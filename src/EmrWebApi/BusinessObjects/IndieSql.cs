﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.BusinessObjects
{
    [System.ComponentModel.DefaultProperty("SqlStatement")]
    public abstract class IndieSql
    {
        public string DatabaseType { get; set; }

        virtual public StmtOracle OracleSql { get; set; }
        virtual public StmtSqlServer SqlServerSql { get; set; }
        virtual public StmtMySql MySql { get; set; }

        static public explicit operator string(IndieSql sql)
        {
            string sqlStatement = "";
            switch (sql.DatabaseType)
            {
                case "PetaPoco.DatabaseTypes.OracleDatabaseType":
                    sqlStatement = sql.OracleSql.SqlStatement;
                    break;
                case "PetaPoco.DatabaseTypes.SqlServerDatabaseType":
                    sqlStatement = sql.SqlServerSql.SqlStatement;
                    break;
                case "PetaPoco.DatabaseTypes.MySqlDatabaseType":
                    sqlStatement = sql.MySql.SqlStatement;
                    break;
            }
            return sqlStatement;
        }

        static public explicit operator StmtOracle(IndieSql sql)
        {
            return sql.OracleSql;
        }
        static public explicit operator StmtSqlServer(IndieSql sql)
        {
            return sql.SqlServerSql;
        }
        static public explicit operator StmtMySql(IndieSql sql)
        {
            return sql.MySql;
        }
    }
}