﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using EmrWebApi.Models;

namespace EmrWebApi.BusinessObjects
{
    public class Template
    {
        private PetaPoco.Database db;

        public Template(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }

        //Insert
        public TESTTABLE InsertTestTable(TESTTABLE infoData)
        {

            using (var transScope = db.GetTransaction())
            {

                //apply processed data
                db.Insert(infoData);

                transScope.Complete();
            }

            return infoData;
        }
        //Update
        public int UpdateTestTable(TESTTABLE infoData)
        {
            //validate data
            if (infoData != null)
            {
                if (infoData.ID == 0)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(infoData, new string[] { "DESCRIPTION", "OTHERDESCRIPTION", "STATUS", "SUPPLIERCODE", "MANUFACTURERCODE", "JOBTYPE"});

                transScope.Complete();
            }

            return 0;
        }
        //ReadById
        public TESTTABLE ReadTestTable(string Id)
        {
            TESTTABLE ret = null;

            var testTableFound = db.SingleOrDefault<TESTTABLE>(PetaPoco.Sql.Builder.Select("*").From("TESTTABLE").Where("ID=@readId", new { readId = Id }));

            if (testTableFound != null)
            {
                ret = testTableFound;
            }
            else
            {
                throw new Exception("Bill Address not found.") { Source = this.GetType().Name };
            }
            return ret;
        }

        //Search
        public List<dynamic> SearchTestTable(string queryString, SearchFilter filter, int maxRows = 500)
        {
            var infoFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchTestTableAdd(db._dbType.ToString(), queryString, filter, maxRows)));

            if (infoFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return infoFound;
        }


    }
    public class IndieSearchTestTableAdd : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "L.ID,L.DESCRIPTION,L.SUPPLIERCODE,L.STATUS, L.SUPPLIER_DESC, L.JOBTYPE_DESC, L.MANUFACTURER_DESC";
        private string columnsSimple = "PO_BILLCODE,BILL_ADDRESS,DESCRIPTION_OTH,DESCRIPTION_SHORT";
        private string from = "testTable_V L";

        public IndieSearchTestTableAdd(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(L.ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(L.DESCRIPTION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(SUPPLIER_DESC) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(L.SUPPLIERCODE) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(L.JOBTYPE_DESC) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(L.MANUFACTURER_DESC) LIKE ('" + query2 + "%')";

            }

            //Additional filter
            //if (!string.IsNullOrWhiteSpace(filter.FromDate))            {
            //    whereClause += "AND " + string.Format(" AM.CREATED_DATE >= '{0}'", filter.FromDate);
            //}
            //if (!string.IsNullOrWhiteSpace(filter.ToDate))
            //{
            //    whereClause += "AND " + string.Format(" AM.CREATED_DATE <= '{0}'", filter.ToDate);
            //}


        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY L.DESCRIPTION ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }


}