﻿using EmrWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EmrWebApi.BusinessObjects
{
    public class Setup
    {
        private PetaPoco.Database db;

        //public Setup()
        //{
        //    db = new PetaPoco.Database("ConStrProdDR");
        //}

        public Setup(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }


        public SYSTEM_DEFAULT GetSystemDefaultByCode(string code)
        {
            return db.SingleOrDefault<SYSTEM_DEFAULT>(PetaPoco.Sql.Builder.Select("*").From("SYSTEM_DEFAULT").Where("SYSDEFAULT=@code", new { code = code }));
        }

        //System Transaction Report
        public SYSTEMTRANSACTION_REPORT CreateSysTransReport(SYSTEMTRANSACTION_REPORT report, List<SYSTEMTRANSACTION_REPORT_SCANDOC> systemTransactionReportScandocs, string userId)
        {
            using (var transScope = db.GetTransaction())
            {
                report.REPORTSTATUS = "REPORTED";
                report.CREATEDBY = userId;
                report.CREATEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                report.REPORTEDBY = userId;
                report.REPORTEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                db.Insert(report);

                var systemTransactionDetails = db.SingleOrDefault<SYSTEM_TRANSACTIONDETAILS>(PetaPoco.Sql.Builder.Select("*").From("SYSTEM_TRANSACTIONDETAILS").Where("SYSTRANSID=@SYSTRANSID", new { SYSTRANSID = report.SYSTRANSID }));
                systemTransactionDetails.REPORTSTATUS = report.REPORTSTATUS;
                db.Update(systemTransactionDetails);

                foreach (var item in systemTransactionReportScandocs)
                {
                    item.SYSREPORTID = report.SYSTRANSID;
                    db.Insert(item);
                }

                transScope.Complete();
            }

            return report;
        }
        public SYSTEMTRANSACTION_REPORT ReadSysTransReport(string transID)
        {
            SYSTEMTRANSACTION_REPORT ret = null;

            var reportFound = db.SingleOrDefault<SYSTEMTRANSACTION_REPORT>(PetaPoco.Sql.Builder.Select("*").From("SYSTEMTRANSACTION_REPORT").Where("SYSTRANSID=@transId", new { transId = transID }));

            if (reportFound != null)
            {
                ret = reportFound;
            }
            else
            {
                throw new Exception("Report not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public List<SYSTEMTRANSACTION_REPORT_SCANDOC> ReadSysTransReportDocs(string transID)
        {
            return db.Fetch<SYSTEMTRANSACTION_REPORT_SCANDOC>(PetaPoco.Sql.Builder.Select("*").From("SYSTEMTRANSACTION_REPORT_SCANDOC").Where("SYSREPORTID=@transId", new { transId = transID }));
        }
        public SYSTEMTRANSACTION_REPORT UpdateSysTransReport(SYSTEMTRANSACTION_REPORT report, List<SYSTEMTRANSACTION_REPORT_SCANDOC> systemTransactionReportScandocs, string userId)
        {
            //validate data
            if (report != null)
            {
                if (report.SYSTRANSID == 0)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {
                if (report.REPORTSTATUS == "APPROVED")
                {
                    report.CLOSEBY = userId;
                    report.CLOSEDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");

                    var systemTransactionDetails = db.SingleOrDefault<SYSTEM_TRANSACTIONDETAILS>(PetaPoco.Sql.Builder.Select("*").From("SYSTEM_TRANSACTIONDETAILS").Where("SYSTRANSID=@SYSTRANSID", new { SYSTRANSID = report.SYSTRANSID }));
                    systemTransactionDetails.REPORTSTATUS = report.REPORTSTATUS;
                    db.Update(systemTransactionDetails);

                    if (report.ACTION == "RPT_ACTION-1" || report.ACTION == "RPT_ACTION-2")
                    {
                        if (systemTransactionDetails.REFTYPE == "Asset")
                        {
                            var amAssetTransaction = db.SingleOrDefault<AM_ASSET_TRANSACTION>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_TRANSACTION").Where("TRANSID=@REFID", new { REFID = systemTransactionDetails.REFTRANSID }));
                            amAssetTransaction.RETURNEDREASON = "RETURN";
                            db.Update(amAssetTransaction);
                        }
                        else if (systemTransactionDetails.REFTYPE == "Consumable" || systemTransactionDetails.REFTYPE == "Parts")
                        {
                            var productStockbatch = db.SingleOrDefault<PRODUCT_STOCKBATCH>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKBATCH").Where("BATCH_ID=@REFID", new { REFID = systemTransactionDetails.REFTRANSID }));
                            productStockbatch.STATUS = "RETURN";
                            db.Update(productStockbatch);
                        }
                    }
                }
                db.Update(report);

                foreach (var item in systemTransactionReportScandocs)
                {
                    item.SYSREPORTID = report.SYSTRANSID;
                    db.Insert(item);
                }

                transScope.Complete();
            }

            return report;
        }

        public SYSTEMTRANSACTION_REPORT UpdateSysTransReportProcessing(SYSTEMTRANSACTION_REPORT report, List<SYSTEMTRANSACTION_REPORT_SCANDOC> systemTransactionReportScandocs, List<SYSTEM_TRANSACTIONDETAILS> systemTransactionDetails, STAFF_LOGON_SESSIONS userSession)
        {
            using (var transScope = db.GetTransaction())
            {
                var arabDateTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                var oldSystemTransactionReport = db.FirstOrDefault<SYSTEMTRANSACTION_REPORT>(PetaPoco.Sql.Builder.Select("*").From("SYSTEMTRANSACTION_REPORT").Where("ID=@ID", new { ID = report.ID }));
                if (oldSystemTransactionReport.REPORTSTATUS != report.REPORTSTATUS)
                {
                    db.Execute("UPDATE SYSTEM_TRANSACTIONDETAILS SET REPORTSTATUS='" + report.REPORTSTATUS + "' WHERE SYSREPORTID='" + report.ID + "'");

                    if (report.REPORTSTATUS == "For Action")
                    {
                        report.REPORTEDBY = userSession.USER_ID;
                        report.REPORTEDDATE = arabDateTime;
                    }
                    else if (report.REPORTSTATUS == "For Closure")
                    {
                        report.ACTIONBY = userSession.USER_ID;
                        report.ACTIONDATE = arabDateTime;

                        if (report.ACTION == "RPT_ACTION-1")
                        {
                            if (report.SEARCHTYPE == "Asset")
                                db.Execute("UPDATE AM_ASSET_TRANSACTION SET RETURNEDREASON='RETURN' WHERE SYSREPORTID='" + report.ID + "'");
                            else if (report.SEARCHTYPE == "Consumable" || report.SEARCHTYPE == "Parts")
                            {
                                var updateProductStockbatchs = db.Fetch<PRODUCT_STOCKBATCH>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKBATCH").Where("SYSREPORTID=@SYSREPORTID", new { SYSREPORTID = report.ID }));
                                foreach (var item in updateProductStockbatchs)
                                {
                                    if (item.PURCHASE_NO == null || item.PURCHASE_NO == "")
                                    {
                                        var amStocktransaction = db.FirstOrDefault<AM_STOCKTRANSACTION>(PetaPoco.Sql.Builder.Select("*").From("AM_STOCKTRANSACTION").Where("TRANS_ID=@TRANS_ID", new { TRANS_ID = item.TRANS_ID }).Where("ID_PARTS=@ID_PARTS", new { ID_PARTS = item.PRODUCT_CODE }));
                                        if (amStocktransaction != null)
                                        {
                                            item.STATUS = "Returned to Main Store";

                                            var updateProductStockonhand = db.FirstOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@PRODUCT_CODE", new { PRODUCT_CODE = item.PRODUCT_CODE }).Where("STORE=@STORE", new { STORE = amStocktransaction.STORE_FROM.ToString() }));
                                            updateProductStockonhand.QTY += Convert.ToInt16(item.RETURN_QTY);
                                            db.Execute("UPDATE PRODUCT_STOCKONHAND SET QTY = " + updateProductStockonhand.QTY + " WHERE PRODUCT_CODE ='" + updateProductStockonhand.PRODUCT_CODE + "' AND STORE = '" + updateProductStockonhand.STORE + "'");
                                        }
                                        else
                                        {
                                            item.STATUS = "RECEIVED";

                                            var updateProductStockonhand = db.FirstOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@PRODUCT_CODE", new { PRODUCT_CODE = item.PRODUCT_CODE }).Where("STORE=@STORE", new { STORE = item.STORE.ToString() }));
                                            updateProductStockonhand.QTY += Convert.ToInt16(item.RETURN_QTY);
                                            db.Execute("UPDATE PRODUCT_STOCKONHAND SET QTY = " + updateProductStockonhand.QTY + " WHERE PRODUCT_CODE ='" + updateProductStockonhand.PRODUCT_CODE + "' AND STORE = '" + updateProductStockonhand.STORE + "'");
                                        }
                                    }
                                    else
                                        item.STATUS = "RECEIVED";

                                    db.Update(item);
                                }
                                db.Execute("UPDATE SYSTEM_TRANSACTIONDETAILS SET REPORTSTATUS='Returned to Main Store' WHERE SYSREPORTID='" + report.ID + "'");
                            }
                        }
                        else if (report.ACTION == "RPT_ACTION-5")
                        {
                            if (report.SEARCHTYPE == "Asset")
                            {
                                var updateAMAssetTransactions = db.Fetch<AM_ASSET_TRANSACTION>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_TRANSACTION").Where("SYSREPORTID=@SYSREPORTID", new { SYSREPORTID = report.ID }));
                                foreach (var item in updateAMAssetTransactions)
                                {
                                    item.RETURNEDREASON = "RETURN";
                                    item.RETURNTOCOSTCENTERBY = userSession.USER_ID;
                                    item.RETURNTOCOSTCENTERDATE = arabDateTime;
                                    db.Update(item);

                                    var updateAMAssetDetails = db.FirstOrDefault<AM_ASSET_DETAILS>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_DETAILS").Where("COSTCENTERTRANSID=@COSTCENTERTRANSID", new { COSTCENTERTRANSID = item.TRANSID }));
                                    if (updateAMAssetDetails != null)
                                    {
                                        updateAMAssetDetails.COST_CENTER = item.COSTCENTERCODE;
                                        db.Update(updateAMAssetDetails);
                                    }
                                }
                            }
                        }
                        else if (report.ACTION == "RPT_ACTION-6")
                        {
                            if (report.SEARCHTYPE == "Consumable" || report.SEARCHTYPE == "Parts")
                            {
                                var updateProductStockbatchs = db.Fetch<PRODUCT_STOCKBATCH>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKBATCH").Where("SYSREPORTID=@SYSREPORTID", new { SYSREPORTID = report.ID }));
                                foreach (var item in updateProductStockbatchs)
                                {
                                    var updateProductStockonhand = db.FirstOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@PRODUCT_CODE", new { PRODUCT_CODE = item.PRODUCT_CODE }).Where("STORE=@STORE", new { STORE = item.STORE.ToString() }));
                                    updateProductStockonhand.QTY += Convert.ToInt16(item.RETURN_QTY);
                                    db.Execute("UPDATE PRODUCT_STOCKONHAND SET QTY = " + updateProductStockonhand.QTY + " WHERE PRODUCT_CODE ='" + updateProductStockonhand.PRODUCT_CODE + "' AND STORE = '" + updateProductStockonhand.STORE + "'");

                                    item.STATUS = "RECEIVED";
                                    db.Update(item);
                                }
                                db.Execute("UPDATE SYSTEM_TRANSACTIONDETAILS SET REPORTSTATUS='RECEIVED' WHERE SYSREPORTID='" + report.ID + "'");
                            }
                        }
                    }
                    else if (report.REPORTSTATUS == "Closed")
                    {
                        report.CLOSEBY = userSession.USER_ID;
                        report.CLOSEDATE = arabDateTime;
                    }
                }
                db.Update(report);

                foreach (var item in systemTransactionReportScandocs)
                {
                    item.SYSREPORTID = report.ID;
                    db.Insert(item);
                }

                foreach (var item in systemTransactionDetails)
                {
                    var updateCommand = "";
                    if (!String.IsNullOrEmpty(item.RECALLBY))
                        updateCommand = "UPDATE SYSTEM_TRANSACTIONDETAILS SET RECALLBY='" + item.RECALLBY + "', RECALLDATE='" + arabDateTime + "'";
                    else
                        updateCommand = "UPDATE SYSTEM_TRANSACTIONDETAILS SET RECALLBY=NULL, RECALLDATE=NULL";

                    if (report.SEARCHTYPE == "Asset")
                        db.Execute(updateCommand + " WHERE SYSREPORTID='" + report.ID + "' and REFCODE='" + item.REFCODE + "'");
                    else
                        db.Execute(updateCommand + " WHERE SYSREPORTID='" + report.ID + "' and REFTRANSID='" + item.REFTRANSID + "'");
                }

                transScope.Complete();
            }

            return report;
        }


        // Group Privileges
        public List<GROUP_PRIVILEGES> CreateNewGroupPrivileges(List<GROUP_PRIVILEGES> requestList, string userId, string createDate, string astionStr)
        {
            using (var transScope = db.GetTransaction())
            {

                DateTime createDt = DateTime.Parse(createDate);


                
                var groupPrivilegesFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchGroupPrivileges(db._dbType.ToString(), requestList[0].STAFF_GROUP_ID, 1)));
                if (groupPrivilegesFound != null)
                {
                    db.Delete<GROUP_PRIVILEGES>(" WHERE STAFF_GROUP_ID ='" + requestList[0].STAFF_GROUP_ID + "'");
                }
               
                
     

                foreach (GROUP_PRIVILEGES i in requestList)
                {
              
             
                    db.Insert(i);


                }
                transScope.Complete();
            }
            return requestList;
        }

        public List<dynamic> SearchGroupPrivileges(string queryString,  int maxRows = 5000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchGroupPrivileges(db._dbType.ToString(), queryString,  maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchPrivileges(string queryString, int maxRows = 5000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPrivileges(db._dbType.ToString(), queryString,  maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        //Update notification
        public int UpdateNotifyStatus(string notifyId, string updateBy, string UpdateDate)
        {

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Execute("UPDATE AM_NOTIFY_MESSAGES SET UPDATED_BY='" + updateBy + "', STATUS='D', UPDATED_DATE='"+ UpdateDate + "' WHERE NOTIFY_ID =" + notifyId );

                transScope.Complete();
            }

            return 0;
        }
        //LOV Lookups Setup
        public LOV_LOOKUPS CreateLovLookup(LOV_LOOKUPS lovlookup)
        {

            using (var transScope = db.GetTransaction())
            {
                if (lovlookup.LOV_LOOKUP_ID == null)
                {
                    lovlookup.LOV_LOOKUP_ID = Guid.NewGuid().ToString("N");
                }
                //apply processed data
                db.Insert(lovlookup);

                transScope.Complete();
            }

            return lovlookup;
        }

        public STAFF_LOGON_SESSIONS insertSessionDr(STAFF_LOGON_SESSIONS sessioncurr, STAFF user)
        {
            
            using (var transScope = db.GetTransaction())
            {
                var userFound = db.SingleOrDefault<STAFF>(PetaPoco.Sql.Builder.Select("*").From("STAFF").Where("STAFF_ID=@userId", new { userId = sessioncurr.STAFF_ID.ToLower() }));

                if (userFound != null)
                {


                    db.Delete("STAFF_LOGON_SESSIONS", "STAFF_ID", sessioncurr);
                    db.Insert(sessioncurr);

                    userFound.USER_ID = ";-D";
                    userFound.PASSWORD = ";-D";
                    transScope.Complete();
                }

               
            }
            return sessioncurr;
        }

        public List<LOV_LOOKUPS> SearchLovLookups(string queryString, string queryStringCategory, int maxRows = 5000)
        {
            var lovlookupsFound = db.Fetch<LOV_LOOKUPS>(PetaPoco.Sql.Builder.Append((string)new IndieSearchLovLookups(db._dbType.ToString(), queryString, queryStringCategory, maxRows)));

            if (lovlookupsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return lovlookupsFound;
        }
        public LOV_LOOKUPS ReadLovLookup(string lovlookupID, string lovlookupCategoryID)
        {
            LOV_LOOKUPS ret = null;

            var lovlookupFound = db.SingleOrDefault<LOV_LOOKUPS>(PetaPoco.Sql.Builder.Select("*").From("LOV_LOOKUPS").Where("LOV_LOOKUP_ID=@lovlookupId", new { lovlookupId = lovlookupID }).Where("CATEGORY=@lovlookupCategoryId", new { lovlookupCategoryId = lovlookupCategoryID })); 

            if (lovlookupFound != null)
            {
                ret = lovlookupFound;
            }
            else
            {
                throw new Exception("Service not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdateLovLookup(LOV_LOOKUPS lovlookup)
        {
            //validate data
            if (lovlookup != null)
            {
                if (lovlookup.LOV_LOOKUP_ID == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //apply processed data
                db.Update(lovlookup, new string[] { "WO_DURATION", "ACTION","DESCRIPTION", "DESCRIPTION_OTH", "DESCRIPTION_SHORT", "STATUS" });

                transScope.Complete();
            }

            return 0;
        }
        public List<dynamic> getLovList(string queryString, int maxRows = 5000)
        {
            var itemsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndiegetLovList(db._dbType.ToString(), queryString, maxRows)));

            if (itemsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return itemsFound;
        }

        public List<dynamic> GetLovListQuery(string queryString, string category, int maxRows = 5000)
        {
            if(category == "'AIMS_COSTCENTER'")
            {
                var itemsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieGetClientCostCenterListQuery(db._dbType.ToString(), queryString, category, maxRows)));

                if (itemsFound == null)
                {
                    throw new Exception("No match found.") { Source = this.GetType().Name };
                }
                return itemsFound;
            }
            else
            {
                var itemsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndiegetLovListQuery(db._dbType.ToString(), queryString, category, maxRows)));

                if (itemsFound == null)
                {
                    throw new Exception("No match found.") { Source = this.GetType().Name };
                }
                return itemsFound;
            }

            
        }

        public List<dynamic> getLovListLookUpId(string queryString, int maxRows = 5000)
		{
			var itemsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndiegetLovListLookUpId(db._dbType.ToString(), queryString, maxRows)));

			if (itemsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return itemsFound;
		}

		//Module Access Attribute Setup
		public List<MODULE_ACCESS_ATTRIBUTE> CreateModuleAccessAttribute(List<MODULE_ACCESS_ATTRIBUTE> NewMFunctionAcess)
        {

 
            using (var transScope = db.GetTransaction())
            {
				db.Delete<MODULE_ACCESS_ATTRIBUTE>(" WHERE GROUP_ID ='" + NewMFunctionAcess[0].GROUP_ID + "'");
				foreach (MODULE_ACCESS_ATTRIBUTE i in NewMFunctionAcess) db.Insert(i);
  
                transScope.Complete();
            }
            return NewMFunctionAcess;
        }
        public List<MODULE_ACCESS_ATTRIBUTE> UpdateModuleAccessAttribute(List<MODULE_ACCESS_ATTRIBUTE> OldMFunctionAccess, List<MODULE_ACCESS_ATTRIBUTE> NewMFunctionAcess)
        {

            var toDelete = NewMFunctionAcess.Where(np => !OldMFunctionAccess.Any(op => op.MODULE_FUNCTION_ID == np.MODULE_FUNCTION_ID && op.GROUP_ID == np.GROUP_ID)).ToArray();
            var toInsert = OldMFunctionAccess.Where(op => !NewMFunctionAcess.Any(np => np.MODULE_FUNCTION_ID == op.MODULE_FUNCTION_ID && op.GROUP_ID == np.GROUP_ID)).ToArray();

            using (var transScope = db.GetTransaction())
            {

                foreach (MODULE_ACCESS_ATTRIBUTE i in toInsert) db.Insert(i);
                foreach (MODULE_ACCESS_ATTRIBUTE i in toDelete) db.Delete<MODULE_ACCESS_ATTRIBUTE>(" WHERE GROUP_ID='" + i.GROUP_ID + "' AND MODULE_FUNCTION_ID='" + i.MODULE_FUNCTION_ID + "'");
 
                transScope.Complete();
            }
            return NewMFunctionAcess;
        }
        public MODULE_ACCESS_ATTRIBUTE DeleteModuleAccessAttribute(MODULE_ACCESS_ATTRIBUTE moduleaccessfunction)
        {

            using (var transScope = db.GetTransaction())
            {

                //apply processed data

                db.Delete("MODULE_ACCESS_ATTRIBUTE", "MODULE_FUNCTION_ID", moduleaccessfunction);


                transScope.Complete();
            }

            return moduleaccessfunction;
        }
        public List<dynamic> SearchModuleAccessAttribute(string queryString,  int maxRows = 500)
        {
            var modulefunctionsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchModuleAccessAttribute(db._dbType.ToString(), queryString, maxRows)));

            if (modulefunctionsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return modulefunctionsFound;
        }
		public int DeleteModuleFunctionAttribute(string userGroupId)
		{

			using (var transScope = db.GetTransaction())
			{

				db.Delete<MODULE_ACCESS_ATTRIBUTE>(" WHERE GROUP_ID ='" + userGroupId + "'");

				transScope.Complete();
			}

			return 0;
		}
		public List<dynamic> SearchMenuAccess(string queryString, string queryGroup, int maxRows = 500)
        {
     
            var modulefunctionsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchMenuAccess(db._dbType.ToString(), queryString, queryGroup, maxRows)));

            if (modulefunctionsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return modulefunctionsFound;
        }

        public int CreateWOPMDue()
        {
            var saudiDateTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
            DateTime modifyDt = saudiDateTime.Date;//DateTime.Today;
            var pmDue = db.Fetch<PM_SCHEDULEDUE_V>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPMScheduleDue(db._dbType.ToString(), "")));
            if (pmDue.Count() > 0)
            {
                using (var transScope = db.GetTransaction())
                {
                    AM_WORK_ORDERS_STATUS_LOG woStatus = new AM_WORK_ORDERS_STATUS_LOG();
                    foreach (PM_SCHEDULEDUE_V i in pmDue)
                    {
                        AM_WORK_ORDERS woDetails = new AM_WORK_ORDERS();
                        db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='AIMS_ORDERNO'");
                        var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "AIMS_ORDERNO" }));
                        woDetails.REQ_NO = Convert.ToString(seq[0].SEQ_VAL);
                        woDetails.ASSET_NO = i.ASSET_NO;
                        //woDetails.ASSET_COSTCENTERCODE = i.ASSET_COSTCENTERCODE;
                        //woDetails.ASSET_COSTCENTER = i.ASSET_COSTCENTER;
                        //UPDATE THE LAST_REQ_NO IN PM SCHEDULE
                        var pmFound = db.SingleOrDefault<AM_ASSET_PMSCHEDULES>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_PMSCHEDULES").Where("ASSET_NO=@assetId", new { assetId = i.ASSET_NO }));
                        if (pmFound != null)
                        {
                            var existingFound = db.SingleOrDefault<ASSET_PMCALNEXTDUEDATE_V>(PetaPoco.Sql.Builder.Select("*").From("ASSET_PMCALNEXTDUEDATE_V").Where("ASSET_NO=@assertNO", new { assertNO = pmFound.ASSET_NO }));
                            if (existingFound != null)
                            {
                                pmFound.NEXT_PM_DUE = existingFound.NEXTDUECAL_DATE;
                                pmFound.LAST_DUE_DATE = existingFound.NEXT_PM_DUE;
                            }
                            pmFound.LAST_REQ_NO = woDetails.REQ_NO;
                            pmFound.CHECKER_UPDATE = "L";
                            db.Update(pmFound, new string[] { "LAST_REQ_NO","NEXT_PM_DUE","CHECKER_UPDATE" ,"LAST_DUE_DATE"});
                        }
                        var assetFound = db.SingleOrDefault<AM_ASSET_DETAILS>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_DETAILS").Where("ASSET_NO=@assetId", new { assetId = i.ASSET_NO }));
                        if(assetFound != null)
                        {
                            if(assetFound.PM_TYPE == "Loan" || assetFound.PM_TYPE == "Warranty" || assetFound.PM_TYPE == "Service Contract")
                            {
                                woDetails.ASSIGN_TYPE = "COMPANY";
                                woDetails.ASSIGN_OUTSOURCE = assetFound.SUPPLIER;
                            }
                        }
                        //var createDate = i.NEXT_PM_DUE.ToString("yyyy/MM/dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                        woDetails.CREATED_BY = "SYSTEM";
                        woDetails.CREATED_DATE = modifyDt;
                        woDetails.REQ_BY = "Preventive Maintenance";
                        woDetails.REQUESTER_CONTACT_NO = "N/A";
                        woDetails.PROBLEM = "Schedule for Preventive Maintenance";
                        woDetails.REQ_TYPE = "PM";
                        woDetails.WO_STATUS = "NE";
                        woDetails.ASSET_COSTCENTERCODE = i.COST_CENTER;
                        woDetails.ASSET_COSTCENTER = i.COSCENTERNAME;
                        woDetails.REQ_DATE = modifyDt;
                        woDetails.JOB_DUE_DATE = i.NEXT_PM_DUE;

                        woStatus.STAFF_ID = woDetails.CREATED_BY;
                        woStatus.RECORDED_DATE = modifyDt;
                        woStatus.REQ_NO = woDetails.REQ_NO;
                        woStatus.WO_STATUS = "NE";

                        db.Insert(woStatus);
                        db.Insert(woDetails);
                    }
                    transScope.Complete();
                }
            }
            else
            {
                return -1;
            }
            return 0;
        }

        //dashboard
        public List<dynamic> SearchDashBoard(string queryString, int maxRows = 50)
        {
            var modulefunctionsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchDashBoard(db._dbType.ToString(), queryString, maxRows)));

            if (modulefunctionsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return modulefunctionsFound;
        }



        //Module Functions Setup
        public MODULE_FUNCTIONS CreateModuleFunction(MODULE_FUNCTIONS modulefunction)
        {
            
            using (var transScope = db.GetTransaction())
            {

                modulefunction.MODULE_FUNCTION_ID = Guid.NewGuid().ToString("N");
                //apply processed data
                db.Insert(modulefunction);

                transScope.Complete();
            }

            return modulefunction;
        }
        public MODULE_FUNCTIONS ReadModuleFunction(string modulefunctionID)
        {
            MODULE_FUNCTIONS ret = null;

            var modulefunctionFound = db.SingleOrDefault<MODULE_FUNCTIONS>(PetaPoco.Sql.Builder.Select("*").From("MODULE_FUNCTIONS").Where("MODULE_FUNCTION_ID=@modulefunctionId", new { modulefunctionId = modulefunctionID }));

            if (modulefunctionFound != null)
            {
                ret = modulefunctionFound;
            }
            else
            {
                throw new Exception("Service not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdateModuleFunction(MODULE_FUNCTIONS modulefunction)
        {
            //validate data
            if (modulefunction != null)
            {
                if (modulefunction.MODULE_ID == null || modulefunction.FUNCTION_ID == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(modulefunction, new string[] { "DESCRIPTION", "DESCRIPTION_OTH","FUNCTION_TYPE","FUNCTION_ID","MODULE_ID","URL" ,"STATUS","APP_ID"});

                transScope.Complete();
            }

            return 0;
        }
        public List<MODULES> GetModuleList()
        {
            var modulesList = db.Fetch<MODULES>(PetaPoco.Sql.Builder.Select("*").From("MODULES"));

            if (modulesList == null)
            {
                throw new Exception("Not Found") { Source = this.GetType().Name };
            }
            return modulesList;
        }
        public List<dynamic> SearchModuleFunctions(string queryString, int maxRows = 500)
        {
            var modulefunctionsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchModuleFunctions(db._dbType.ToString(), queryString, maxRows)));

            if (modulefunctionsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return modulefunctionsFound;
        }

       
        
        //Staff Privileges
        public STAFF_PRIVILEGES CreateStaffPrivileges(STAFF_PRIVILEGES staffprivileges)
        {

            using (var transScope = db.GetTransaction())
            {

                staffprivileges.PRIVILEGE_ID = Guid.NewGuid().ToString("N");
                //apply processed data
                db.Insert(staffprivileges);

                transScope.Complete();
            }

            return staffprivileges;
        }
        public STAFF_PRIVILEGES ReadStaffPrivileges(string privilegeID)
        {
            STAFF_PRIVILEGES ret = null;

            var staffprivilegesFound = db.SingleOrDefault<STAFF_PRIVILEGES>(PetaPoco.Sql.Builder.Select("*").From("STAFF_PRIVILEGES").Where("PRIVILEGE_ID=@privilegeId", new { privilegeId = privilegeID }));
            
            if (staffprivilegesFound != null)
            {
                ret = staffprivilegesFound;
            }
            else
            {
                throw new Exception("Service not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdateStaffPrivileges(STAFF_PRIVILEGES staffprivileges)
        {
            //validate data
            if (staffprivileges != null)
            {
                if (staffprivileges.PRIVILEGE_ID == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(staffprivileges, new string[] { "DESCRIPTION", "DESCRIPTION_OTH" });

                transScope.Complete();
            }

            return 0;
        }
        public List<STAFF_PRIVILEGES> SearchStaffPrivileges(string queryString, int maxRows = 50)
        {
            var staffgroupsFound = db.Fetch<STAFF_PRIVILEGES>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStaffPrivileges(db._dbType.ToString(), queryString, maxRows)));

            if (staffgroupsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffgroupsFound;
        }

        //Locations Setup
        public LOCATIONS CreateLocation(LOCATIONS location)
        {

            using (var transScope = db.GetTransaction())
            {

                location.LOCATION_ID = Guid.NewGuid().ToString("N");
                //apply processed data
                db.Insert(location);

                transScope.Complete();
            }

            return location;
        }
        public LOCATIONS ReadLocation(string locationID)
        {
            LOCATIONS ret = null;

            var locationFound = db.SingleOrDefault<LOCATIONS>(PetaPoco.Sql.Builder.Select("*").From("LOCATIONS").Where("LOCATION_ID=@locationId", new { locationId = locationID }));

            if (locationFound != null)
            {
                ret = locationFound;
            }
            else
            {
                throw new Exception("Service not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdateLocation(LOCATIONS location)
        {
            //validate data
            if (location != null)
            {
                if (location.LOCATION_ID == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(location, new string[] { "DESCRIPTION", "DESCRIPTION_SHORT", "DESCRIPTION_OTH" });

                transScope.Complete();
            }

            return 0;
        }
        public List<LOCATIONS> SearchLocations(string queryString, int maxRows = 50)
        {
            var locationsFound = db.Fetch<LOCATIONS>(PetaPoco.Sql.Builder.Append((string)new IndieSearchLocations(db._dbType.ToString(), queryString, maxRows)));

            if (locationsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return locationsFound;
        }

        //CostCenter Setup
        public COSTCENTER_DETAILS CreateCostCenter(COSTCENTER_DETAILS costcenter, List<CLIENTOTHCONTACT> CLIENTOTHCONTACTS, string userId)
        {

            using (var transScope = db.GetTransaction())
            {
                db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='AIMS_COSTCENTER'");
                var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "AIMS_COSTCENTER" }));
         
                costcenter.CODEID = "CC" + Convert.ToString(seq[0].SEQ_VAL);
                costcenter.CREATEDBY = userId;
                costcenter.CREATEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                //apply processed data
                db.Insert(costcenter);

                LOV_LOOKUPS lookups = new LOV_LOOKUPS();
                lookups.LOV_LOOKUP_ID = "CC" + Convert.ToString(seq[0].SEQ_VAL);
                lookups.DESCRIPTION = costcenter.DESCRIPTION;
                lookups.DESCRIPTION_SHORT = costcenter.SHORTNAME;
                lookups.CREATEDBY = userId;
                lookups.CREATEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                lookups.CATEGORY = "AIMS_COSTCENTER";
                lookups.STATUS = costcenter.STATUS;
                lookups.DESCRIPTION_OTH = costcenter.DESCRIPTIONOTH;
                db.Insert(lookups);

                foreach (var item in CLIENTOTHCONTACTS)
                {
                    item.TYPECODE = "CC";
                    item.REFCODE = costcenter.CODEID;
                    item.CREATEDBY = userId;
                    item.CREATEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                    db.Insert(item);
                }

                transScope.Complete();
            }
            return costcenter;
        }
        public COSTCENTER_DETAILS ReadCostCenter(string codeID)
        {
            COSTCENTER_DETAILS ret = null;

            var costcenterFound = db.SingleOrDefault<COSTCENTER_DETAILS>(PetaPoco.Sql.Builder.Select("*").From("COSTCENTER_DETAILS").Where("CODEID=@codeId", new { codeId = codeID }));

            if (costcenterFound != null)
            {
                ret = costcenterFound;
            }
            else
            {
                throw new Exception("Cost Center not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public List<CLIENTOTHCONTACT> ReadCostCenterClientOthContacts(string codeID, string typeCode)
        {
            List<CLIENTOTHCONTACT> ret = null;

            var CLIENTOTHCONTACT = db.Fetch<CLIENTOTHCONTACT>(PetaPoco.Sql.Builder.Select("*").From("CLIENTOTHCONTACT").Where("RefCode=@codeId", new { codeId = codeID }).Where("TYPECODE=@typeCode", new { typeCode = typeCode }));

            if (CLIENTOTHCONTACT != null)
            {
                ret = CLIENTOTHCONTACT;
            }
            else
            {
                throw new Exception("Other Contact not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public STORES ReadCostCenterStore(string codeID)
        {
            STORES ret = null;

            var costcenterstoreFound = db.SingleOrDefault<STORES>(PetaPoco.Sql.Builder.Select("*").From("STORES").Where("CLIENT_CODE=@codeId", new { codeId = codeID }));

            if (costcenterstoreFound != null)
            {
                ret = costcenterstoreFound;
            }
            else
            {
                return ret;
            }
            return ret;
        }
        public AM_CLIENT_INFO ReadCostCenterClient(string ccodeID)
        {
            AM_CLIENT_INFO ret = null;

            var costcenterclientFound = db.SingleOrDefault<AM_CLIENT_COSTCENTER>(PetaPoco.Sql.Builder.Select("*").From("AM_CLIENT_COSTCENTER").Where("CostcenterCode=@ccodeId", new { ccodeId = ccodeID }));
            

            if (costcenterclientFound != null)
            {
                ret = db.SingleOrDefault<AM_CLIENT_INFO>(PetaPoco.Sql.Builder.Select("*").From("AM_CLIENT_INFO").Where("CLIENT_CODE=@codeId", new { codeId = costcenterclientFound.CLIENTCODE }));
            }
            else
            {
                return ret;
            }
            return ret;
        }
        public int UpdateCostCenter(COSTCENTER_DETAILS costcenter, List<CLIENTOTHCONTACT> CLIENTOTHCONTACTS, string userId)
        {
            //validate data
            if (costcenter != null)
            {
                if (costcenter.CODEID == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data
                costcenter.LASTUPDATEBY = userId;
                costcenter.LASTUPDATEDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                LOV_LOOKUPS lovCostCenterFound = new LOV_LOOKUPS();
                lovCostCenterFound = db.SingleOrDefault<LOV_LOOKUPS>(PetaPoco.Sql.Builder.Select("*").From("LOV_LOOKUPS").Where("LOV_LOOKUP_ID=@reqId", new { reqId = costcenter.CODEID }).Where("CATEGORY=@category", new { category = "AIMS_COSTCENTER" }));

                if (lovCostCenterFound != null)
                {
                    lovCostCenterFound.DESCRIPTION = costcenter.DESCRIPTION;
                    lovCostCenterFound.DESCRIPTION_SHORT = costcenter.SHORTNAME;
                    lovCostCenterFound.DESCRIPTION_OTH = costcenter.DESCRIPTIONOTH;
                    lovCostCenterFound.STATUS = costcenter.STATUS;

                    db.Update(lovCostCenterFound, new string[] { "DESCRIPTION", "DESCRIPTION_OTH", "DESCRIPTION_SHORT", "STATUS" });
                }
               

                //apply processed data
                db.Update(costcenter, new string[] { "DESCRIPTION", "SHORTNAME", "DESCRIPTIONOTH", "STATUS", "ADDRESS", "CITY", "STATE", "EMAILADDRESS", "CONTACTNO", "CONTACTNO2"
                                                     , "CONTACTNO3" , "CONTACTPERSON" , "CONTACTPERSONEMAILADD" , "CONTACTPERSONCONTACTNO" , "LOGOPATH" , "LASTUPDATEBY", "LASTUPDATEDATE", "COSTCENTERTYPE"});

                db.Execute("DELETE ClientOthContact WHERE RefCode='" + costcenter.CODEID + "'");

                foreach (var item in CLIENTOTHCONTACTS)
                {
                    item.TYPECODE = "CC";
                    item.REFCODE = costcenter.CODEID;
                    item.CREATEDBY = userId;
                    item.CREATEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                    db.Insert(item);
                }

                transScope.Complete();
            }

            return 0;
        }
        public List<COSTCENTER_DETAILS> SearchCostCenter(string queryString, int maxRows = 50000)
        {
            var costcenterFound = db.Fetch<COSTCENTER_DETAILS>(PetaPoco.Sql.Builder.Append((string)new IndieSearchCostCenter(db._dbType.ToString(), queryString, maxRows)));

            if (costcenterFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return costcenterFound;
        }
        public List<dynamic> SearchCostCenterDetails(string queryString, int maxRows = 50000)
        {
            var costcenterFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchCostCenterDetails(db._dbType.ToString(), queryString, maxRows)));

            if (costcenterFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return costcenterFound;
        }

        //Approving Officer
        public APPROVING_OFFICER CreateApproveOfficer(APPROVING_OFFICER infoDet)
        {

            using (var transScope = db.GetTransaction())
            {

         
                //apply processed data
                db.Insert(infoDet);

                transScope.Complete();
            }

            return infoDet;
        }
        public APPROVING_OFFICER ReadApproveOfficer(string infoId)
        {
            APPROVING_OFFICER ret = null;

            var locationFound = db.SingleOrDefault<APPROVING_OFFICER>(PetaPoco.Sql.Builder.Select("*").From("APPROVING_OFFICER").Where("ID=@Id", new { Id = infoId }));

            if (locationFound != null)
            {
                ret = locationFound;
            }
            else
            {
                throw new Exception("Service not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdateApproveOfficer(APPROVING_OFFICER infoDet)
        {
            //validate data
            if (infoDet != null)
            {
                if (infoDet.ID == 0)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(infoDet, new string[] { "STATUS", "STORE_ID", "APPROVE_TYPE" });

                transScope.Complete();
            }

            return 0;
        }
        public List<dynamic> SearchApproveOfficer(string queryString, int maxRows = 500)
        {
            var locationsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchApproveOfficer(db._dbType.ToString(), queryString, maxRows)));

            if (locationsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return locationsFound;
        }



        //Purchase Billing Address
        public AM_PURCHASE_BILLADDRESS CreatebillAddress(AM_PURCHASE_BILLADDRESS infoData)
		{

			using (var transScope = db.GetTransaction())
			{

				//apply processed data
				db.Insert(infoData);

				transScope.Complete();
			}

			return infoData;
		}
		public AM_PURCHASE_BILLADDRESS ReadbillAddress(string bAddressID)
		{
			AM_PURCHASE_BILLADDRESS ret = null;

			var locationFound = db.SingleOrDefault<AM_PURCHASE_BILLADDRESS>(PetaPoco.Sql.Builder.Select("*").From("AM_PURCHASE_BILLADDRESS").Where("PO_BILLCODE=@bAddressId", new { bAddressId = bAddressID }));

			if (locationFound != null)
			{
				ret = locationFound;
			}
			else
			{
				throw new Exception("Service not found.") { Source = this.GetType().Name };
			}
			return ret;
		}
		public int UpdatebillAddress(AM_PURCHASE_BILLADDRESS infoData)
		{
			//validate data
			if (infoData != null)
			{
				if (infoData.PO_BILLCODE == 0)
					throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
			}

			using (var transScope = db.GetTransaction())
			{

				//preprocess data

				//apply processed data
				db.Update(infoData, new string[] { "SHIP_TO","BILL_ADDRESS", "BILL_CONTACT_NO1", "BILL_CONTACT_NO2","BILL_CITY","BILL_ZIPCODE","BILL_STATE","STATUS","DESCRIPTION","BILL_EMAIL_ADDRESS" });

				transScope.Complete();
			}

			return 0;
		}
		public List<dynamic> SearchbillAddress(string queryString, int maxRows = 500)
		{
			var infoFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchBillAddress(db._dbType.ToString(), queryString, maxRows)));

			if (infoFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return infoFound;
		}
		public List<dynamic> SearchbillAddressActData(string queryString, int maxRows = 500)
		{
			var infoFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchBillAddressActiveData(db._dbType.ToString(), queryString, maxRows)));

			if (infoFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return infoFound;
		}
		

		//Client Information
		public AM_CLIENT_INFO CreateClientInfo(AM_CLIENT_INFO clientInfo, List<CLIENTOTHCONTACT> CLIENTOTHCONTACTS, string userId)
		{
			using (var transScope = db.GetTransaction())
			{
				//apply processed data
				db.Insert(clientInfo);


                foreach (var item in CLIENTOTHCONTACTS)
                {
                    item.TYPECODE = "CC";
                    item.REFCODE = clientInfo.CLIENT_CODE;
                    item.CREATEDBY = userId;
                    item.CREATEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                    db.Insert(item);
                }

                transScope.Complete();
			}
			return clientInfo;
		}
		public AM_CLIENT_INFO ReadClientInfo(string clientID)
		{
			AM_CLIENT_INFO ret = null;
			var clientFound = db.SingleOrDefault<AM_CLIENT_INFO>(PetaPoco.Sql.Builder.Select("*").From("AM_CLIENT_INFO").Where("CLIENT_CODE=@clientId", new { clientId = clientID }));
			if (clientFound != null)
			{
				ret = clientFound;
			}
			else
			{
				throw new Exception("Service not found.") { Source = this.GetType().Name };
			}
			return ret;
		}
		public List<CLIENT_V> GetCLIENT_V()
		{
            List<CLIENT_V> ret = null;
			var clientList = db.Fetch<CLIENT_V>(PetaPoco.Sql.Builder.Select("*").From("CLIENT_V"));
			if (clientList != null)
			{
				ret = clientList;
			}
			else
			{
				throw new Exception("Service not found.") { Source = this.GetType().Name };
			}
			return ret;
		}
		public int UpdateClientInfo(AM_CLIENT_INFO clientInfo, List<AM_CLIENT_COSTCENTER> clientCostCenter, List<CLIENTOTHCONTACT> CLIENTOTHCONTACTS, string userId)
		{
			//validate data
			if (clientInfo != null)
			{
				if (clientInfo.CLIENT_CODE == null)
					using (var transScope = db.GetTransaction())
					{
						//apply processed data
						db.Insert(clientInfo);
						transScope.Complete();
					}
			}

			using (var transScope = db.GetTransaction())
			{
                //preprocess data

                db.Delete<AM_CLIENT_COSTCENTER>(" WHERE CLIENTCODE='" + clientInfo.CLIENT_CODE + "'");

                if (clientCostCenter.Count() > 0)
                {
                    foreach (AM_CLIENT_COSTCENTER i in clientCostCenter)
                    {
                        i.CLIENTCODE = clientInfo.CLIENT_CODE;
                        i.CREATEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                        i.CREATEDBY = userId;
                        db.Insert(i);

                    }
                }
                else
                {
                   
                }



                //apply processed data
                db.Update(clientInfo, new string[] { "DESCRIPTION", "DESCRIPTION_SHORT", "DESCRIPTION_OTH", "ADDRESS", "CONTACT_NO1", "CONTACT_NO2", "EMAIL_ADDRESS","BILL_ADDRESS","BILL_CONTACT_NO1","BILL_CONTACT_NO2","BILL_EMAIL_ADDRESS",
					"BILL_CITY","BILL_ZIPCODE","BILL_STATE","CLIENT_LOGO","CITY","ZIPCODE","STATE","WEBAPI","HEADING","HEADING2","HEADING3","HEADING_OTH","HEADING_OTH2","HEADING_OTH3" ,"COMPANY_LOGO","CONTACT_PERSON","CONTACTNO_PERSON","CONTACT_EMAILADD","CLIENT_PATH_LOGO", "CLIENT_PATH_PRINT_LOGO", "COMPANY_PATH_LOGO"});

                db.Execute("DELETE ClientOthContact WHERE RefCode='" + clientInfo.CLIENT_CODE + "'");

                foreach (var item in CLIENTOTHCONTACTS)
                {
                    item.TYPECODE = "C";
                    item.REFCODE = clientInfo.CLIENT_CODE;
                    item.CREATEDBY = userId;
                    item.CREATEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                    db.Insert(item);
                }


                transScope.Complete();
			}

			return 0;
		}
        public List<AM_CLIENT_INFO> SearchClientInfo(string queryString, int maxRows = 500)
        {
            var staffgroupsFound = db.Fetch<AM_CLIENT_INFO>(PetaPoco.Sql.Builder.Append((string)new IndieSearchClientInfo(db._dbType.ToString(), queryString, maxRows)));

            if (staffgroupsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffgroupsFound;
        }
        public List<dynamic> SearchClientCostCenter(string queryString, int maxRows = 100)
        {
            var servicesFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchClientCostCenter(db._dbType.ToString(), queryString, maxRows)));

            if (servicesFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return servicesFound;
        }

        public List<LOV_LOOKUPS> SearchClientCostCenterLOV(int maxRows = 500)
        {
            var servicesFound = db.Fetch<LOV_LOOKUPS>(PetaPoco.Sql.Builder.Append((string)new IndieSearchClientCostCenterLOV(db._dbType.ToString(), "N", maxRows)));

            if (servicesFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return servicesFound;
        }

        //Client Building
        public AM_CLIENT_BUILDING CreateClientBuilding(AM_CLIENT_BUILDING clientBuilding)
        {
            using (var transScope = db.GetTransaction())
            {
                //apply processed data
                db.Insert(clientBuilding);
                transScope.Complete();
            }
            return clientBuilding;
        }
     
        //STAFF POSITION SETUP
        public STAFF_POSITIONS CreateStaffPosition(STAFF_POSITIONS staffposition)
        {
            using (var transScope = db.GetTransaction())
            {
                staffposition.STAFF_POSITION_ID = Guid.NewGuid().ToString("N");
                //apply processed data
                db.Insert(staffposition);
                transScope.Complete();
            }
            return staffposition;
        }

        public STAFF_POSITIONS ReadStaffPosition(string staffpositionID)
        {
            STAFF_POSITIONS ret = null;

            var staffpositionFound = db.SingleOrDefault<STAFF_POSITIONS>(PetaPoco.Sql.Builder.Select("*").From("STAFF_POSITIONS").Where("STAFF_POSITION_ID=@staffpositionId", new { staffpositionId = staffpositionID }));

            if (staffpositionFound != null)
            {
                ret = staffpositionFound;
            }
            else
            {
                throw new Exception("Service not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdateStaffPosition(STAFF_POSITIONS staffposition)
        {
            //validate data
            if (staffposition != null)
            {
                if (staffposition.STAFF_POSITION_ID == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(staffposition, new string[] { "DESCRIPTION", "DESCRIPTION_SHORT", "DESCRIPTION_OTH" });

                transScope.Complete();
            }

            return 0;
        }

        public List<STAFF_POSITIONS> SearchStaffPositions(string queryString, int maxRows = 50)
        {
            var staffgroupsFound = db.Fetch<STAFF_POSITIONS>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStaffPositions(db._dbType.ToString(), queryString, maxRows)));

            if (staffgroupsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffgroupsFound;
        }

        //STAFF GROUPS SETUP
        public STAFF_GROUPS CreateStaffGroup(STAFF_GROUPS staffgroup)
        {

            using (var transScope = db.GetTransaction())
            {

                staffgroup.STAFF_GROUP_ID = Guid.NewGuid().ToString("N");
                //apply processed data
                db.Insert(staffgroup);

                transScope.Complete();
            }

            return staffgroup;
        }

        public STAFF_GROUPS ReadStaffGroup(string staffgroupID)
        {
            STAFF_GROUPS ret = null;

            var staffgroupFound = db.SingleOrDefault<STAFF_GROUPS>(PetaPoco.Sql.Builder.Select("*").From("STAFF_GROUPS").Where("STAFF_GROUP_ID=@staffgroupId", new { staffgroupId = staffgroupID }));

            if (staffgroupFound != null)
            {
                ret = staffgroupFound;
            }
            else
            {
                throw new Exception("Service not found.") { Source = this.GetType().Name };
            }
            return ret;
        }

        public int UpdateStaffGroup(STAFF_GROUPS staffgroup)
        {
            //validate data
            if (staffgroup != null)
            {
                if (staffgroup.STAFF_GROUP_ID == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(staffgroup, new string[] { "DESCRIPTION", "DESCRIPTION_SHORT", "DESCRIPTION_OTH","DASHBOARDURL" });


				transScope.Complete();
            }

            return 0;
        }

        public List<STAFF_GROUPS> SearchStaffGroups(string queryString, int maxRows = 50)
        {
            var staffgroupsFound = db.Fetch<STAFF_GROUPS>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStaffGroups(db._dbType.ToString(), queryString, maxRows)));

            if (staffgroupsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffgroupsFound;
        }

        //Service Setup
        public SERVICES CreateService(SERVICES servicesetup)
        {

            using (var transScope = db.GetTransaction())
            {

                servicesetup.SERVICE_ID = Guid.NewGuid().ToString("N");
                //apply processed data
                db.Insert(servicesetup);

                transScope.Complete();
            }

            return servicesetup;
        }

        public SERVICES ReadService(string serviceID)
        {
            SERVICES ret = null;

            var serviceFound = db.SingleOrDefault<SERVICES>(PetaPoco.Sql.Builder.Select("*").From("SERVICES").Where("SERVICE_ID=@serviceId", new { serviceId = serviceID }));

            if (serviceFound != null)
            {
                ret = serviceFound;
            }
            else
            {
                throw new Exception("Service not found.") { Source = this.GetType().Name };
            }
            return ret;
        }

        public int UpdateService(SERVICES servicesetup)
        {
            //validate data
            if (servicesetup != null)
            {
                if (servicesetup.SERVICE_ID == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(servicesetup, new string[] { "DESCRIPTION", "DESCRIPTION_SHORT", "DESCRIPTION_OTH" });

                transScope.Complete();
            }

            return 0;
        }

        public List<SERVICES> SearchServices(string queryString, int maxRows = 50)
        {
            var servicesFound = db.Fetch<SERVICES>(PetaPoco.Sql.Builder.Append((string)new IndieSearchServices(db._dbType.ToString(), queryString, maxRows)));

            if (servicesFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return servicesFound;
        }
   
        
        public List<STAFF> GetStaffList()
        {
            var staffList = db.Fetch<STAFF>(PetaPoco.Sql.Builder.Select("*").From("STAFF"));

            if (staffList == null)
            {
                throw new Exception("Not Found") { Source = this.GetType().Name };
            }
            return staffList;
        }
    
        public List<dynamic> SearchContractLOV(string queryString, int maxRows = 50)
        {
            var clinicDoctorFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchContractLOV(db._dbType.ToString(), queryString, maxRows)));

            if (clinicDoctorFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return clinicDoctorFound;
        }
    
        public List<dynamic> SearchContractServices(string queryString, string systemID, int maxRows = 50)
        {
            var clinicDoctorFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchContractServices(db._dbType.ToString(), queryString, systemID, maxRows)));

            if (clinicDoctorFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return clinicDoctorFound;
        }

       
        //Staff Services
        public STAFF_SERVICES CreateStaffService(STAFF_SERVICES staffservice)
        {

            using (var transScope = db.GetTransaction())
            {

                staffservice.STAFF_SERVICE_ID = Guid.NewGuid().ToString("N");
                //apply processed data
                db.Insert(staffservice);

                transScope.Complete();
            }

            return staffservice;
        }

        public STAFF_SERVICES ReadStaffService(string staffserviceID)
        {
            STAFF_SERVICES ret = null;

            var staffserviceFound = db.SingleOrDefault<STAFF_SERVICES>(PetaPoco.Sql.Builder.Select("*").From("STAFF_SERVICES").Where("STAFF_SERVICE_ID=@staffserviceId", new { staffserviceId = staffserviceID }));

            if (staffserviceFound != null)
            {
                ret = staffserviceFound;
            }
            else
            {
                throw new Exception("Clinic not found.") { Source = this.GetType().Name };
            }
            return ret;
        }

        public int UpdateStaffService(STAFF_SERVICES staffservice)
        {
            //validate data
            if (staffservice != null)
            {
                if (staffservice.STAFF_SERVICE_ID == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(staffservice, new string[] { "STAFF_ID", "SERVICE_ID" });

                transScope.Complete();
            }

            return 0;
        }

        public List<SERVICES> GetServicesList()
        {
            var servicesList = db.Fetch<SERVICES>(PetaPoco.Sql.Builder.Select("*").From("SERVICES ORDER BY DESCRIPTION"));

            if (servicesList == null)
            {
                throw new Exception("Not Found") { Source = this.GetType().Name };
            }
            return servicesList;
        }
        public List<dynamic> SearchStaffServices(string queryString, int maxRows = 50)
        {
            var staffservicesFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStaffServices(db._dbType.ToString(), queryString, maxRows)));

            if (staffservicesFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffservicesFound;
        }

        public SYSTEMTRANSACTION_REPORT GetSysTransReportDocsById(string sysReportId)
        {
            return db.FirstOrDefault<SYSTEMTRANSACTION_REPORT>(PetaPoco.Sql.Builder.Select("*").From("SYSTEMTRANSACTION_REPORT").Where("ID=@sysReportId", new { sysReportId = sysReportId }));
        }

        public List<SYSTEMTRANSACTION_REPORT_SCANDOC> GetSysTransReportDocsByReport(string sysReportId)
        {
            return db.Fetch<SYSTEMTRANSACTION_REPORT_SCANDOC>(PetaPoco.Sql.Builder.Select("*").From("SYSTEMTRANSACTION_REPORT_SCANDOC").Where("SYSREPORTID=@sysReportId", new { sysReportId = sysReportId }));
        }

        
        public List<dynamic> SearchClientV(string queryString, int maxRows = 5000)
        {
            var clientFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchClientV(db._dbType.ToString(), queryString, maxRows)));

            if (clientFound == null)
            {
             //   throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return clientFound;
        }


    }



    public class IndieSearchClientV : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "DESCRIPTION, COSTCENTER AS LOV_LOOKUP_ID";
        private string columnsSimple = "DESCRIPTION,LOV_LOOKUP_ID";
        private string from = "CLIENT_COSTCENTER_V";

        public IndieSearchClientV(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
               

            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }


    public class IndieSearchCostCenter : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "L.CODEID,L.DESCRIPTION,L.SHORTNAME,L.DESCRIPTIONOTH,L.STATUS, L.ADDRESS, L.CONTACTNO, L.CONTACTPERSON, L.LOGOPATH";
        private string columnsSimple = "CODEID,DESCRIPTION,SHORTNAME,DESCRIPTIONOTH,STATUS, ADDRESS, CONTACTNO, CONTACTPERSON, LOGOPATH";
        private string from = "COSTCENTER_DETAILS L";

        public IndieSearchCostCenter(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(L.CODEID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(L.DESCRIPTION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(L.SHORTNAME) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(L.DESCRIPTIONOTH) LIKE ('" + query2 + "%')";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }


    public class IndieSearchCostCenterDetails : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "L.CODEID,L.DESCRIPTION,L.SHORTNAME,L.DESCRIPTIONOTH,L.STATUS, L.ADDRESS,L.CITY,L.STATE,L.EMAILADDRESS, L.CONTACTNO,L.CONTACTNO2,L.CONTACTNO3, " +
            "L.CONTACTPERSON,L.CONTACTPERSONEMAILADD,L.CONTACTPERSONCONTACTNO,L.COSTCENTERTYPE, L.LOGOPATH, ST.STORE_ID, ST.DESCRIPTION AS STORENAME";
        private string columnsSimple = "CODEID,DESCRIPTION,SHORTNAME,DESCRIPTIONOTH,STATUS, ADDRESS, CONTACTNO, CONTACTPERSON, LOGOPATH";
        private string from = "COSTCENTER_DETAILS L LEFT JOIN STORES ST ON L.CODEID = ST.CLIENT_CODE";

        public IndieSearchCostCenterDetails(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(L.CODEID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(L.DESCRIPTION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(L.SHORTNAME) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(L.DESCRIPTIONOTH) LIKE ('" + query2 + "%')";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }



    public class IndieSearchClientCostCenterLOV : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "DESCRIPTION,LOV_LOOKUP_ID";
        private string columnsSimple = "DESCRIPTION,LOV_LOOKUP_ID";
        private string from = "LOV_LOOKUPS";

        public IndieSearchClientCostCenterLOV(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " AND CATEGORY = 'AIMS_COSTCENTER' AND STATUS = 'Y' AND  LOV_LOOKUP_ID NOT IN (SELECT COSTCENTERCODE FROM AM_CLIENT_COSTCENTER)";

            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }

    public class IndieSearchClientCostCenter : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "P.CLIENTCODE,LOV.DESCRIPTION,P.COSTCENTERCODE";
        private string columnsSimple = "P.CLIENTCODE,LOV.DESCRIPTION,P.COSTCENTERCODE";
        private string from = "AM_CLIENT_COSTCENTER P LEFT OUTER JOIN LOV_LOOKUPS LOV ON P.COSTCENTERCODE = LOV.LOV_LOOKUP_ID AND CATEGORY = 'AIMS_COSTCENTER'";

        public IndieSearchClientCostCenter(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(P.CLIENTCODE) LIKE ('" + query2 + "%')";
             
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }
    public class IndieSearchContractLOV : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "CD.CONTRACT_ID, CD.DEBTOR_ID, CD.CONTRACT_NAME, CD.SHORTNAME, CD.IDENTIFICATION,CD.AGE_FROM, CD.AGE_TO, CD.DEBTOR_DEPOSIT, CD.DISCOUNT_PERCENT, CD.START_DATE, CD.END_DATE, CD.DEDUCTIBLE_AMOUNT, CD.DEDUCTIBLE_PERCENT, CD.DISCOUNT_AMOUNT, CD.DEBTOR_LIMIT, CD.STATUS, CD.CLASS, CD.POLICY_ID, CD.PT_LIMIT_PERVISIT, CD.PT_MAX_AMOUNT";
        private string columnsSimple = "CONTRACT_ID, DEBTOR_ID, CONTRACT_NAME, SHORTNAME, IDENTIFICATION,AGE_FROM, AGE_TO, DEBTOR_DEPOSIT, DISCOUNT_PERCENT, START_DATE, END_DATE, DEDUCTIBLE_AMOUNT, DEDUCTIBLE_PERCENT, DISCOUNT_AMOUNT, DEBTOR_LIMIT, STATUS, CLASS, POLICY_ID, PT_LIMIT_PERVISIT, PT_MAX_AMOUNT";
        private string from = "DEBTOR_CONTRACT_TERMS CD";

        public IndieSearchContractLOV(string dbType, string queryString, int maxRows = 1550)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (UPPER(CD.STATUS) ='ACTIVE')";
               

            }
            whereClause += " AND DATE_FORMAT('" + queryString + "', '%Y-%m-%d')";
            whereClause += " BETWEEN DATE_FORMAT(CD.START_DATE, '%Y-%m-%d') AND DATE_FORMAT(CD.END_DATE, '%Y-%m-%d')";


        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY CLINIC_DESCRIPTION " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }


    }

    public class IndieSearchContractServices : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "CD.CONTRACT_ID, CD.CONTRACT_TERM_LINE, CD.DEBTOR_ID, CD.ACTION_CATEGORY, CD.ACTION_TYPE,CD.PRICE_INCLUDED , CD.DEDUCTIBLE_PERCENT, CD.DISCOUNT_PERCENT, CD.STATUS, CD.PRODUCT_CATEGORY, CD.PRODUCT_GROUP, CD.PRODUCT_CODE, CD.CLASS, ST.SHORTNAME";
        private string columnsSimple = "CONTRACT_ID, CONTRACT_TERM_LINE, DEBTOR_ID, ACTION_CATEGORY, ACTION_TYPE,PRICE_INCLUDED, DEDUCTIBLE_PERCENT, DISCOUNT_PERCENT, STATUS, PRODUCT_CAGETORY, PRODUCT_GROUP, PRODUCT_CODE, CLASS, SHORTNAME";
        private string from = "DEBTOR_TERMS_ATTRIBUTE CD INNER JOIN DEBTOR_CONTRACT_TERMS ST ON CD.CONTRACT_ID = ST.CONTRACT_ID";

        public IndieSearchContractServices(string dbType, string queryString, string systemID, int maxRows = 1000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(CD.ACTION_CATEGORY) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(CD.ACTION_TYPE) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(CD.PRODUCT_CODE) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(CD.PRODUCT_GROUP) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(CD.STATUS) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(CD.PRODUCT_CATEGORY) LIKE ('" + query2 + "%'))";

            }

            if (systemID != null && systemID != "")
            {
                whereClause += " AND (CD.CONTRACT_ID ='" + systemID + "' )";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY CLINIC_DESCRIPTION " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndieSearchDebtorContract : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "CD.CONTRACT_ID, CD.DEBTOR_ID, CD.CONTRACT_NAME, CD.SHORTNAME, CD.IDENTIFICATION,CD.AGE_FROM, CD.AGE_TO, CD.DEBTOR_DEPOSIT, CD.DISCOUNT_PERCENT, CD.START_DATE, CD.END_DATE, CD.DEDUCTIBLE_AMOUNT, CD.DEDUCTIBLE_PERCENT, CD.DISCOUNT_AMOUNT, CD.DEBTOR_LIMIT, CD.STATUS, CD.CLASS, CD.POLICY_ID, CD.PT_LIMIT_PERVISIT, CD.PT_MAX_AMOUNT";
        private string columnsSimple = "CONTRACT_ID, DEBTOR_ID, CONTRACT_NAME, SHORTNAME, IDENTIFICATION,AGE_FROM, AGE_TO, DEBTOR_DEPOSIT, DISCOUNT_PERCENT, START_DATE, END_DATE, DEDUCTIBLE_AMOUNT, DEDUCTIBLE_PERCENT, DISCOUNT_AMOUNT, DEBTOR_LIMIT, STATUS, CLASS, POLICY_ID, PT_LIMIT_PERVISIT, PT_MAX_AMOUNT";
        private string from = "DEBTOR_CONTRACT_TERMS CD";

        public IndieSearchDebtorContract(string dbType, string queryString, int maxRows = 1550)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(CD.DEBTOR_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(CD.CONTRACT_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(CD.CONTRACT_NAME) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(CD.SHORTNAME) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(CD.DISCOUNT_PERCENT) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(CD.DISCOUNT_AMOUNT) LIKE ('" + query2 + "%')";

            }


        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY CLINIC_DESCRIPTION " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }

    public class IndieSearchDebtor : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "CD.DEBTOR_ID, CD.DESCRIPTION, CD.ADDRESS, CD.CONTACT_NO, CD.EMAIL_ADDRESS,CD.LIMIT_AMOUNT";
        private string columnsSimple = "DEBTOR_ID, DESCRIPTION, ADDRESS, CONTACT_NO, EMAIL_ADDRESS,LIMIT_AMOUNT";
        private string from = "DEBTOR_DETAILS CD";

        public IndieSearchDebtor(string dbType, string queryString,  int maxRows = 550)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(CD.DEBTOR_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(CD.DESCRIPTION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(CD.ADDRESS) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(CD.CONTACT_NO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(CD.EMAIL_ADDRESS) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(CD.LIMIT_AMOUNT) LIKE ('" + query2 + "%')";

            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY CLINIC_DESCRIPTION " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }

    public class IndiegetLovList : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "(1=2 ";
        private string columns = "LL.LOV_LOOKUP_ID, LL.DESCRIPTION,LL.DESCRIPTION_SHORT, LL.DESCRIPTION_OTH,LL.CATEGORY,LL.ACTION, LL.WO_DURATION";
        private string columnsSimple = "LOV_LOOKUP_ID, DESCRIPTION,DESCRIPTION_SHORT,DESCRIPTION_OTH,CATEGORY,ACTION";
        private string from = "LOV_LOOKUPS LL";

        public IndiegetLovList(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(LL.CATEGORY) IN (" + query2 + "))";
				whereClause += " AND UPPER(LL.STATUS) ='Y'";

			}
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY CLINIC_DESCRIPTION " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + ") ORDER BY  DESCRIPTION " };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + ") ORDER BY  DESCRIPTION " + " LIMIT " + maxRows.ToString() };
            }
        }


    }



    public class IndieGetClientCostCenterListQuery : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "(1=2 ";
        private string columns = "LL.COSTCENTER AS LOV_LOOKUP_ID, LL.DESCRIPTION,LL.SHORTNAME AS DESCRIPTION_SHORT,'AIMS_COSTCENTER' AS CATEGORY";
        private string columnsSimple = "LOV_LOOKUP_ID, DESCRIPTION,DESCRIPTION_SHORT,DESCRIPTION_OTH,CATEGORY,ACTION";
        private string from = "CLIENT_COSTCENTER_V LL";

        public IndieGetClientCostCenterListQuery(string dbType, string queryString, string Category, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(LL.COSTCENTER) LIKE ('%" + query2.ToLower() + "%')";
                whereClause += " OR LOWER(LL.DESCRIPTION) LIKE ('%" + query2.ToLower() + "%'))";


            }

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY CLINIC_DESCRIPTION " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + ") ORDER BY  DESCRIPTION " };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + ") ORDER BY  DESCRIPTION " + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndiegetLovListQuery : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "(1=2 ";
        private string columns = "LL.LOV_LOOKUP_ID, LL.DESCRIPTION,LL.DESCRIPTION_SHORT, LL.DESCRIPTION_OTH,LL.CATEGORY,LL.ACTION";
        private string columnsSimple = "LOV_LOOKUP_ID, DESCRIPTION,DESCRIPTION_SHORT,DESCRIPTION_OTH,CATEGORY,ACTION";
        private string from = "LOV_LOOKUPS LL";

        public IndiegetLovListQuery(string dbType, string queryString, string Category, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(LL.LOV_LOOKUP_ID) LIKE ('%" + query2.ToLower() + "%')";
                whereClause += " OR LOWER(LL.DESCRIPTION) LIKE ('%" + query2.ToLower() + "%'))";


            }

            whereClause += " AND (LOWER(LL.CATEGORY) IN(" + Category.ToLower() + "))";

            whereClause += " AND UPPER(LL.STATUS) ='Y'";
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY CLINIC_DESCRIPTION " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + ") ORDER BY  DESCRIPTION " };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + ") ORDER BY  DESCRIPTION " + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndiegetLovListLookUpId : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "(1=2 ";
		private string columns = "LL.LOV_LOOKUP_ID, LL.DESCRIPTION,LL.DESCRIPTION_SHORT, LL.DESCRIPTION_OTH,LL.CATEGORY,LL.ACTION";
		private string columnsSimple = "LOV_LOOKUP_ID, DESCRIPTION,DESCRIPTION_SHORT,DESCRIPTION_OTH,CATEGORY,ACTION";
		private string from = "LOV_LOOKUPS LL";

		public IndiegetLovListLookUpId(string dbType, string queryString, int maxRows = 50)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

            whereClause += " OR (LOWER(LL.LOV_LOOKUP_ID) IN (" + queryString.ToLower() + "))";
            whereClause += " AND UPPER(LL.STATUS) ='Y'";


        }

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY CLINIC_DESCRIPTION " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + ") ORDER BY  DESCRIPTION " };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + ") ORDER BY  DESCRIPTION " + " LIMIT " + maxRows.ToString() };
			}
		}
	}

	public class IndieSearchMenuAccess : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
       
        private string whereClause = "1=2 ";
        private string columns = "MAA.ATTRIBUTE,MAA.MODULE_ID,MAA.ATTRIBUTE_ID,MAA.DESCRIPTION,MAA.CREATE_BY,MAA.URL,MAA.GROUP_ID,MAA.DESCRIPTION_OTH,MAA.MODULE_FUNCTION_ID,G.DESCRIPTION STAFF_GROUP_DESCRIPTION, MC.APP_ID, MD.APP_ID MOD_APPID";
        private string columnsSimple = "ATTRIBUTE,MODULE_ID,ATTRIBUTE_ID,DESCRIPTION,CREATE_BY,GROUP_ID,URL,DESCRIPTION_OTH,MODULE_FUNCTION_ID,STAFF_GROUP_DESCRIPTION";
        private string from = "MODULE_ACCESS_ATTRIBUTE MAA INNER JOIN STAFF_GROUPS G ON MAA.GROUP_ID = G.STAFF_GROUP_ID  INNER JOIN MODULES MD ON MAA.MODULE_ID = MD.MODULE_ID LEFT OUTER JOIN MODULE_FUNCTIONS   MC ON MAA.MODULE_FUNCTION_ID = MC.MODULE_FUNCTION_ID";

        public IndieSearchMenuAccess(string dbType, string queryString, string queryGroup, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(MAA.ATTRIBUTE) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(MAA.DESCRIPTION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(MAA.GROUP_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(G.DESCRIPTION) LIKE ('" + query2 + "%'))";

            }
            whereClause += " AND (MAA.GROUP_ID ='" + queryGroup + "' )";
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY GROUP_ID,MODULE_ID" };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  MODULE_SEQ,MODULE_ID,DESCRIPTION ASC" };
				}
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  MODULE_SEQ,MODULE_ID,DESCRIPTION ASC LIMIT " + maxRows.ToString() };
            }
        }
    }

    public class IndieSearchPMScheduleDue : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        string NewDate = DateTime.Now.AddMonths(1).ToString("yyyyMM");

        private string whereClause = "1=2 ";
        private string columns = "ASSET_NO,NEXT_PM_DUE,COST_CENTER, COSCENTERNAME";
        private string columnsSimple = "ASSET_NO";
        private string from = "PM_SCHEDULEDUE_V";

        public IndieSearchPMScheduleDue(string dbType, string queryString,  int maxRows = 2000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(ASSET_NO) LIKE ('" + query2 + "%')) And CurrDate='" + NewDate + "'" ;
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY GROUP_ID,MODULE_ID" };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY NEXT_PM_DUE" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  MODULE_SEQ,MODULE_ID,DESCRIPTION ASC LIMIT " + maxRows.ToString() };
            }
        }
    }

    public class IndieSearchModuleAccessAttribute : IndieSql
    {
        private int maxRows = 500;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "MAA.ATTRIBUTE,MAA.MODULE_ID,MAA.ATTRIBUTE_ID,MAA.DESCRIPTION,MAA.CREATE_BY,MAA.URL,MAA.GROUP_ID,MAA.DESCRIPTION_OTH,MAA.MODULE_FUNCTION_ID,G.DESCRIPTION STAFF_GROUP_DESCRIPTION";
        private string columnsSimple = "ATTRIBUTE,MODULE_ID,ATTRIBUTE_ID,DESCRIPTION,CREATE_BY,GROUP_ID,URL,DESCRIPTION_OTH,MODULE_FUNCTION_ID,STAFF_GROUP_DESCRIPTION";
        private string from = "MODULE_ACCESS_ATTRIBUTE MAA INNER JOIN STAFF_GROUPS G ON MAA.GROUP_ID = G.STAFF_GROUP_ID";

        public IndieSearchModuleAccessAttribute(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(MAA.ATTRIBUTE) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(MAA.DESCRIPTION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(MAA.GROUP_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(G.DESCRIPTION) LIKE ('" + query2 + "%')";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString()+ "ORDER BY GROUP_ID,MODULE_ID" };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  MODULE_ID,DESCRIPTION ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  MODULE_ID,DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
            }
        }
    }

    public class IndieSearchLocations : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "L.LOCATION_ID,L.DESCRIPTION,L.DESCRIPTION_OTH,L.DESCRIPTION_SHORT";
        private string columnsSimple = "LOCATION_ID,DESCRIPTION,DESCRIPTION_OTH,DESCRIPTION_SHORT";
        private string from = "LOCATIONS L";

        public IndieSearchLocations(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(L.LOCATION_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(L.DESCRIPTION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(L.DESCRIPTION_SHORT) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(L.DESCRIPTION_OTH) LIKE ('" + query2 + "%')";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }

    public class IndieSearchApproveOfficer : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "L.ID,L.STAFF_ID,L.STATUS,L.STORE_ID, ST.FIRST_NAME, ST.LAST_NAME, SR.DESCRIPTION, L.APPROVE_TYPE";
        private string columnsSimple = "ID,STAFF_ID,STATUS,STORE_ID";
        private string from = "APPROVING_OFFICER L LEFT JOIN STAFF ST ON L.STAFF_ID = ST.STAFF_ID LEFT JOIN STORES SR ON  L.STORE_ID = SR.STORE_ID";

        public IndieSearchApproveOfficer(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(SR.DESCRIPTION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(ST.LAST_NAME) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(ST.FIRST_NAME) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(L.STAFF_ID) LIKE ('" + query2 + "%')";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }

    public class IndieSearchBillAddress : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "L.PO_BILLCODE,L.BILL_ADDRESS,L.BILL_CONTACT_NO1,L.BILL_CONTACT_NO2,L.BILL_CITY,L.BILL_ZIPCODE,L.BILL_STATE,L.STATUS,L.DESCRIPTION";
		private string columnsSimple = "PO_BILLCODE,BILL_ADDRESS,DESCRIPTION_OTH,DESCRIPTION_SHORT";
		private string from = "AM_PURCHASE_BILLADDRESS L";

		public IndieSearchBillAddress(string dbType, string queryString, int maxRows = 50)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			foreach (string query in queryArray)
			{
				var query2 = query.ToLower();
				whereClause += " OR LOWER(L.PO_BILLCODE) LIKE ('" + query2 + "%')";
				whereClause += " OR LOWER(L.BILL_ADDRESS) LIKE ('" + query2 + "%')";
				whereClause += " OR LOWER(L.BILL_CONTACT_NO1) LIKE ('" + query2 + "%')";
				whereClause += " OR LOWER(L.BILL_CONTACT_NO2) LIKE ('" + query2 + "%')";
				whereClause += " OR LOWER(L.BILL_ZIPCODE) LIKE ('" + query2 + "%')";
				whereClause += " OR LOWER(L.DESCRIPTION) LIKE ('" + query2 + "%')";
				whereClause += " OR LOWER(L.STATUS) LIKE ('" + query2 + "%')";
			}
		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY PO_BILLCODE ASC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
			}
		}
	}

	public class IndieSearchBillAddressActiveData : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=1 ";
		private string columns = "L.PO_BILLCODE,L.BILL_ADDRESS,L.BILL_CONTACT_NO1,L.BILL_CONTACT_NO2,L.BILL_CITY,L.BILL_ZIPCODE,L.BILL_STATE,L.STATUS,L.DESCRIPTION, L.BILL_EMAIL_ADDRESS, L.SHIP_TO";
		private string columnsSimple = "PO_BILLCODE,BILL_ADDRESS,DESCRIPTION_OTH,DESCRIPTION_SHORT";
		private string from = "AM_PURCHASE_BILLADDRESS L";

		public IndieSearchBillAddressActiveData(string dbType, string queryString, int maxRows = 50)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

            whereClause += " AND LOWER(L.STATUS) = ('Y')";

            if (!String.IsNullOrEmpty(queryString))
                whereClause += " AND LOWER(L.DESCRIPTION) LIKE ('%" + queryString.ToLower() + "%')";
        }

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY PO_BILLCODE ASC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
			}
		}
	}
	public class IndieSearchStaffPrivileges : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "P.PRIVILEGE_ID,P.DESCRIPTION,P.DESCRIPTION_OTH";
        private string columnsSimple = "PRIVILEGE_ID,DESCRIPTION,DESCRIPTION_OTH";
        private string from = "STAFF_PRIVILEGES P";

        public IndieSearchStaffPrivileges(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(P.PRIVILEGE_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(P.DESCRIPTION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(P.DESCRIPTION_OTH) LIKE ('" + query2 + "%')";

            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }

    public class IndieSearchModuleFunctions : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "MF.MODULE_FUNCTION_ID, MF.MODULE_ID,MF.URL, MF.FUNCTION_ID, MF.FUNCTION_TYPE, MF.DESCRIPTION, MF.DESCRIPTION_OTH, M.DESCRIPTION MODULE_DESCRIPTION,M.DESCRIPTION_OTH MODULE_DESCRIPTION_OTH";
        private string columnsSimple = "MODULE_FUNCTION_ID, MODULE_ID,URL, FUNCTION_ID,FUNCTION_TYPE, DESCRIPTION, DESCRIPTION_OTH, MODULE_DESCRIPTION,MODULE_DESCRIPTION_OTH";
        private string from = "MODULE_FUNCTIONS MF INNER JOIN MODULES M ON MF.MODULE_ID = M.MODULE_ID";

        public IndieSearchModuleFunctions(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(MF.MODULE_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(MF.DESCRIPTION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(M.DESCRIPTION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(MF.FUNCTION_ID) LIKE ('" + query2 + "%')";

            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY MODULE_ID,FUNCTION_TYPE,FUNCTION_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY MODULE_SEQ,FUNCTION_SEQ" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY MODULE_SEQ,FUNCTION_SEQ" };
                }
        }
    }

    public class IndieSearchStaffPositions : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "SP.STAFF_POSITION_ID,SP.DESCRIPTION,SP.DESCRIPTION_OTH,SP.DESCRIPTION_SHORT";
        private string columnsSimple = "STAFF_POSITION_ID,DESCRIPTION,DESCRIPTION_OTH,DESCRIPTION_SHORT";
        private string from = "STAFF_POSITIONS SP";

        public IndieSearchStaffPositions(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(SP.STAFF_POSITION_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(SP.DESCRIPTION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(SP.DESCRIPTION_SHORT) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(SP.DESCRIPTION_OTH) LIKE ('" + query2 + "%')";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }

    public class IndieSearchStaffGroups : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "SG.STAFF_GROUP_ID,SG.DESCRIPTION,SG.DESCRIPTION_OTH,SG.DESCRIPTION_SHORT";
        private string columnsSimple = "STAFF_GROUP_ID,DESCRIPTION,DESCRIPTION_OTH,DESCRIPTION_SHORT";
        private string from = "STAFF_GROUPS SG";

        public IndieSearchStaffGroups(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(SG.STAFF_GROUP_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(SG.DESCRIPTION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(SG.DESCRIPTION_SHORT) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(SG.DESCRIPTION_OTH) LIKE ('" + query2 + "%')";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }
    public class IndieSearchServices : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "P.SERVICE_ID,P.DESCRIPTION,P.DESCRIPTION_OTH,P.DESCRIPTION_SHORT";
        private string columnsSimple = "SERVICE_ID,DESCRIPTION,DESCRIPTION_OTH,DESCRIPTION_SHORT";
        private string from = "SERVICES P";

        public IndieSearchServices(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(P.SERVICE_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(P.DESCRIPTION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(P.DESCRIPTION_SHORT) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(P.DESCRIPTION_OTH) LIKE ('" + query2 + "%')";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }
	public class IndieSearchClientInfo : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "P.CLIENT_CODE,P.DESCRIPTION,P.DESCRIPTION_OTH,P.DESCRIPTION_SHORT, ADDRESS, CLIENT_LOGO, CONTACT_NO1";
		private string columnsSimple = "CLIENTID,DESCRIPTION,DESCRIPTION_OTH,DESCRIPTION_SHORT, ADDRESS, CLIENT_LOGO, CONTACT_NO1";
		private string from = "AM_CLIENT_INFO P";

        public IndieSearchClientInfo(string dbType, string queryString, int maxRows = 50)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			foreach (string query in queryArray)
			{
				var query2 = query.ToLower();
				whereClause += " OR LOWER(P.CLIENT_CODE) LIKE ('" + query2 + "%')";
			}
		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
			}
		}
	}
	public class IndieSearchDashBoard : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "P.PR_REQ,P.RPT_TYPE";
        private string columnsSimple = "PR_REQ,RPT_TYPE";
        private string from = "DASHBOARD_V P";

        public IndieSearchDashBoard(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(P.PR_REQ) LIKE ('" + query2 + "%')";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }

    public class IndieSearchStaffServices : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "SD.STAFF_SERVICE_ID, SD.STAFF_ID, SD.SERVICE_ID, ST.FIRST_NAME, ST.LAST_NAME,SL.DESCRIPTION SERVICE_DESCRIPTION";
        private string columnsSimple = "STAFF_SERVICE_ID, STAFF_ID, SERVICE_ID, FIRST_NAME, LAST_NAME,SERVICE_DESCRIPTION";
        private string from = "STAFF_SERVICES SD INNER JOIN STAFF ST ON SD.STAFF_ID = ST.STAFF_ID INNER JOIN SERVICES SL ON SD.SERVICE_ID = SL.SERVICE_ID";

        public IndieSearchStaffServices(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(SD.STAFF_SERVICE_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(SL.DESCRIPTION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(ST.LAST_NAME) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(ST.FIRST_NAME) LIKE ('" + query2 + "%')";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }

    public class IndieSearchLovLookups : IndieSql
    {
        private int maxRows = 5000;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "LL.LOV_LOOKUP_ID, LL.DESCRIPTION, LL.DESCRIPTION_OTH, LL.DESCRIPTION_SHORT, LL.CATEGORY, LL.STATUS";
        private string columnsSimple = "LOV_LOOKUP_ID, DESCRIPTION, DESCRIPTION_OTH,DESCRIPTION_SHORT";
        private string from = "LOV_LOOKUPS LL";

        public IndieSearchLovLookups(string dbType, string queryString,string queryStringCategory, int maxRows = 5000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(LL.CATEGORY) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(LL.DESCRIPTION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(LL.LOV_LOOKUP_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(LL.DESCRIPTION_OTH) LIKE ('" + query2 + "%'))";
            }
            whereClause += " AND (LL.CATEGORY ='"+queryStringCategory + "' )";
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY DESCRIPTION " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY LL.DESCRIPTION ASC"};
                }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }


    }


    public class IndieSearchGroupPrivileges : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";

        private string whereClause = "1=2 ";
        private string columns = "MAA.STAFF_GROUP_ID,MAA.PRIVILEGES_ID";
        private string columnsSimple = "STAFF_GROUP_ID,PRIVILEGES_ID";
        private string from = "GROUP_PRIVILEGES MAA";

        public IndieSearchGroupPrivileges(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(MAA.PRIVILEGES_ID) is not null )";
            }
            whereClause += " AND (MAA.STAFF_GROUP_ID ='" + queryString + "' )";
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY GROUP_ID,MODULE_ID" };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  PRIVILEGES_ID ASC "};
				}
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  PRIVILEGES_ID ASC LIMIT " + maxRows.ToString() };
            }
        }
    }
    public class IndieSearchPrivileges : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";

        private string whereClause = "1=2 ";
        private string columns = "MAA.PRIVILEGES_ID,MAA.DESCRIPTION";
        private string columnsSimple = "PRIVILEGES_ID,DESCRIPTION";
        private string from = "PRIVILEGES MAA";

        public IndieSearchPrivileges(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(MAA.PRIVILEGES_ID) is not null )";

            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY GROUP_ID,MODULE_ID" };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  PRIVILEGES_ID ASC " };
				}
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  PRIVILEGES_ID ASC LIMIT " + maxRows.ToString() };
            }
        }
    }
}