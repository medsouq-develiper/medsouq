﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Text;

//Project Related
using EmrWebApi.Models;

namespace EmrWebApi.BusinessObjects
{
    public class Security
    {
        private PetaPoco.Database db;
        public Security() {
            db = new PetaPoco.Database("ConStrProdDR");
        }

        //public SecurityDR()
        //{
        //    db = new PetaPoco.Database("ConStrProdDr");
        //}
        public PetaPoco.Database GetSharedDBInstance{
            get { return db; }
        }

        public void Dispose() {
            db.Dispose();
        }

        public Dictionary<string, object> emailSender(STAFF user)
        {
            Dictionary<string, object> ret = null;
            var db = new PetaPoco.Database("ConStrProd");

            var userFound = db.SingleOrDefault<STAFF>(PetaPoco.Sql.Builder.Select("*").From("STAFF").Where("USER_ID=@userId", new { userId = user.USER_ID.ToLower() }));

            if (userFound != null)
            {
                var userEmail = userFound.EMAIL_ADDRESS;






                if (userEmail != null && userEmail != "")
                {
                    var mailBody = "Dear " + userFound.LAST_NAME + ',';
                    mailBody += "\r\n";
                    mailBody += "\r\n";
                    mailBody += "Keep your password private and never share it.";
                    mailBody += "\r\n";
                    mailBody += "\r\n";
                    mailBody += "You or maybe someone else requested your password.";
                    mailBody += "\r\n";
                    mailBody += "\r\n";
                    mailBody += "Your password is:" + userFound.PASSWORD;
                    mailBody += "\r\n";
                    mailBody += "\r\n";
                    mailBody += "\r\n";
                    mailBody += "\r\n";

                    mailBody += " Please do not reply to this message.Replies to this message are routed to an unmonitored mailbox.If you have questions please go to http://www.bsaims.isoftwaresolutions.net/ You may also call us @  | ";
                    mailBody += "\r\n";
                    mailBody += "\r\n";

                    MailMessage mm = new MailMessage("bst.hims.ph@gmail.com", userEmail, "password assistance", mailBody);
                    mm.BodyEncoding = UTF8Encoding.UTF8;
                    mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                    SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                    client.UseDefaultCredentials = false;

                    client.Credentials = new System.Net.NetworkCredential("bst.hims.ph@gmail.com", "bS123456");
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.EnableSsl = true;
                    client.Timeout = 10000;

                    client.Send(mm);

                    ret = new Dictionary<string, object>() { { "EMAILOK", "SUCCESS" } };

                }
                else
                {
                    ret = new Dictionary<string, object>() { { "INVALIDEMAIL", "INVALID" } };
                }

            }
            else {
                ret = new Dictionary<string, object>() { { "INVALIDUSERID", "INVALIDUSER" } };
            }
            return ret;
        }

        /// <summary>
        /// Authenticate a user and return an appropriate STAFF_LOGON_SESSIONS object
        /// </summary>
        /// <param name="user"></param>
        /// <returns>Returns exception if the user authentication failed</returns>
        
        public Dictionary<string, object> AuthenticateUser(STAFF user)
        {
            Dictionary<string, object> ret = null;
            var db = new PetaPoco.Database("ConStrProd");
           
            var userFound = db.SingleOrDefault<STAFF>(PetaPoco.Sql.Builder.Select("*").From("STAFF").Where("USER_ID=@userId", new { userId = user.USER_ID.ToLower() }));
            if (userFound != null){
                if (user.PASSWORD == userFound.PASSWORD) { //implement an encrypt/decrypt
                    STAFF_LOGON_SESSIONS session = new STAFF_LOGON_SESSIONS()
                    {
                        SESSION_ID = Guid.NewGuid().ToString("N"),
                        STAFF_ID = userFound.STAFF_ID.ToUpper(),
                        LOGON_DATE = DateTime.Now,
                        USER_ID = userFound.USER_ID.ToUpper()
                        
                    };
                   
                    db.Delete("STAFF_LOGON_SESSIONS", "STAFF_ID", session);
                    db.Insert(session);

                   
                    userFound.USER_ID = ";-D";
                    userFound.PASSWORD = ";-D";
                    userFound.STAFF_ID = userFound.STAFF_ID.ToUpper();
                    userFound.USER_ID = userFound.USER_ID.ToUpper();
                    ret = new Dictionary<string, object>() { { "STAFF_LOGON_SESSIONS", session }, { "STAFF", userFound } };
                }
                else {
                    throw new Exception("Invalid user credentials.") { Source = this.GetType().Name};
                }
            }
            else{
                throw new Exception("User not found.") { Source = this.GetType().Name };
            }
            return ret;
        }

        public Dictionary<string, object> insertSessionStaffApi(STAFF_LOGON_SESSIONS sessioncurr, STAFF user)
        {
            Dictionary<string, object> ret = null;
            var db = new PetaPoco.Database("ConStrProd");
           
            var userFound = db.SingleOrDefault<STAFF>(PetaPoco.Sql.Builder.Select("*").From("STAFF").Where("USER_ID=@userId", new { userId = user.USER_ID.ToLower() }));

            if (userFound != null)
            {
            
                  
                    db.Delete("STAFF_LOGON_SESSIONS", "STAFF_ID", sessioncurr);
                    db.Insert(sessioncurr);

                    userFound.USER_ID = ";-D";
                    userFound.PASSWORD = ";-D";

                    ret = new Dictionary<string, object>() { { "SESSIONOK", "" } };
                
            }
            else {
                throw new Exception("User not found.") { Source = this.GetType().Name };
            }
            return ret;
        }


        /// <summary>
        /// Check the session validity. This method will use the same db instance and throw an error if session is expired.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="db"></param>
        public void CheckSessionValidity(STAFF_LOGON_SESSIONS session, string asAction)
        {
            bool isSessionExpired = false;
            var currentSession = db.SingleOrDefault<STAFF_LOGON_SESSIONS>(PetaPoco.Sql.Builder.Select("*").From("STAFF_LOGON_SESSIONS").Where("SESSION_ID=@sessionId", new { sessionId = session.SESSION_ID }));

			var endDate = DateTime.Now;
			endDate = endDate.AddDays(30);
			if (endDate <= DateTime.Now && asAction == "UPDATE")
			{
				db.Dispose();
				throw new Exception("License Expired") { Source = this.GetType().Name };
			}

			if (currentSession != null){

                int timeOut = EmrWebApi.Properties.Settings.Default.staffSessionTimeout;

                if (DateTime.Now.Subtract(currentSession.LOGON_DATE.GetValueOrDefault()).TotalMinutes <= timeOut){

					
					currentSession.LOGON_DATE = DateTime.Now;
                    db.Update("STAFF_LOGON_SESSIONS", "SESSION_ID", currentSession);

                }
                else {
                    isSessionExpired = false;
                }
            }
            else {
                isSessionExpired = false;
            }

            if (isSessionExpired) {

                db.Dispose();
                throw new Exception("Session Expired") { Source = this.GetType().Name };
            }

        }

        /// <summary>
        /// Invalidates a logged on staff/user's session.
        /// </summary>
        /// <param name="session"></param>
        /// <returns>Returns 1 if the session was successfully invalidated, returns -1 if it failed.</returns>
        public int InvalidateSession(STAFF_LOGON_SESSIONS session){
            return 1;
        }
    }
}