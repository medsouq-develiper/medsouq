﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using EmrWebApi.Models;

namespace EmrWebApi.BusinessObjects
{
    public class Icd
    {
        #region CLASS
        private PetaPoco.Database db = null;

        public Icd(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }


        public List<dynamic> SearchIcd(string queryString, int maxRows = 50)
        {
            var icdFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchICDDiagnosis(db._dbType.ToString(), queryString, maxRows)));
            return icdFound;
        }

        #endregion


    }

    public class IndieSearchICDDiagnosis : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columnsOracle = "A.REVISION || '|' || A.CODE_ID id, A.ASCII_DESC name";
        private string columnsSql = "A.REVISION || ',' || A.CODE_ID, A.ASCII_DESC";
        private string columnsMySql = "A.REVISION || ',' || A.CODE_ID, A.ASCII_DESC";
        private string columnsSimple = "id, name";
        private string from = "ICD_DIAGNOSIS A";

        public IndieSearchICDDiagnosis(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(A.ASCII_DESC) LIKE ('%" + query2 + "%')";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columnsOracle + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columnsMySql + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }

    }
}