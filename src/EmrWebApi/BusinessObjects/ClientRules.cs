﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using EmrWebApi.Models;
namespace EmrWebApi.BusinessObjects
{
    public class ClientRules
    {
        private PetaPoco.Database db;

        public ClientRules(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }
        //Search
        public CLIENT_RULES GetClientRuleById(string ruleId)
        {
            CLIENT_RULES ret = null;

            var woFound = db.SingleOrDefault<CLIENT_RULES>(PetaPoco.Sql.Builder.Select("*").From("CLIENT_RULES").Where("ID=@ruleId", new { ruleId = ruleId }));

            if (woFound != null)
            {
                ret = woFound;
            }
            else
            {
                throw new Exception("Client rule not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public List<dynamic> GetClientRules(string queryString, int maxRows = 100000)
        {
            var assetListFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieGetClientRules(db._dbType.ToString(), queryString, maxRows)));

            if (assetListFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return assetListFound;
        }

        public class IndieGetClientRules : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=1 ";
            private string columns = "C.ID, C.DESCRIPTION, C.STATUS, C.RULE_VALUE, C.INTERVAL";
            private string columnsSimple = "DESCRIPTION,";
            private string from = "client_rules C";

            public IndieGetClientRules(string dbType, string queryString, int maxRows = 50)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;

                whereClause += " AND ID = 'MSOFFICE'";
            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
                }
            }
        }
    }
}