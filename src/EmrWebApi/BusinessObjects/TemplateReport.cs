﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using EmrWebApi.Models;

namespace EmrWebApi.BusinessObjects
{
    public class TemplateReport
    {
        private PetaPoco.Database db;

        public TemplateReport(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }

        //public List<dynamic> ExampleSearchByAsset(string queryString, SearchFilter filter, int maxRows = 100000)
        //{
        //   // var assetListFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieExampleSearchByAsset(db._dbType.ToString(), queryString, filter, maxRows)));

        //    if (assetListFound == null)
        //    {
        //        throw new Exception("No match found.") { Source = this.GetType().Name };
        //    }
        //    return assetListFound;
        //}

        public List<dynamic> ExampleSearchByWorkOrder(string queryString, SearchFilter filter, int maxRows = 1000)
        {
            var assetListFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieExampleSearchByWorkOrder(db._dbType.ToString(), queryString, filter, maxRows)));

            if (assetListFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return assetListFound;
        }

        public List<dynamic> ExampleSearchByTestData(string queryString, SearchFilter filter, int maxRows = 1000)
        {
            var assetListFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieExampleSearchByTestData(db._dbType.ToString(), queryString, filter, maxRows)));

            if (assetListFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return assetListFound;
        }


    }
    //public class IndieExampleSearchByAsset : IndieSql
    //{
    //    private int maxRows = 50;
    //    private string queryString = "";
    //    private string whereClause = "1=2 ";
    //    private string columns = "A.ASSET_NO, A.DESCRIPTION, A.COSTCENTER_DESC";
    //    private string columnsSimple = "DESCRIPTION,";
    //    private string from = "ASSETINFOREPT_V A";

    //    public IndieExampleSearchByAsset(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
    //    {
    //        this.DatabaseType = dbType;
    //        this.queryString = queryString;
    //        this.maxRows = maxRows;
    //        string[] queryArray = queryString.Split(new char[] { ' ' });

    //        foreach (string query in queryArray)
    //        {
    //            var query2 = query.ToLower();
    //            whereClause += " OR (LOWER(A.ASSET_NO) LIKE ('%" + query2 + "%') OR LOWER(A.COSTCENTER_DESC) LIKE ('%" + query2 + "%') OR LOWER(A.DESCRIPTION) LIKE ('%" + query2 + "%'))";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
    //        {
    //            whereClause += " AND (LOWER(A.COSTCENTER_DESC) LIKE ('%" + filter.CostCenterStr + "%'))";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.assetDescription))
    //        {
    //            whereClause += " AND (LOWER(A.DESCRIPTION) LIKE ('%" + filter.assetDescription + "%'))";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.AssetFilterSuupiler))
    //        {
    //            whereClause += " AND (LOWER(A.SUPPLIER_DESC) LIKE ('%" + filter.AssetFilterSuupiler + "%'))";
    //        }



    //    }

    //    public override StmtOracle OracleSql
    //    {
    //        get
    //        {
    //            return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
    //        }
    //    }

    //    public override StmtSqlServer SqlServerSql
    //    {
    //        get
    //        {
    //            return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY A.ASSET_NO" };
    //        }
    //    }

    //    public override StmtMySql MySql
    //    {
    //        get
    //        {
    //            return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
    //        }
    //    }
    //}

    public class IndieExampleSearchByWorkOrder : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "A.ASSET_NO, A.REQ_NO, A.DESCRIPTION, A.COSTCENTERNAME";
        private string columnsSimple = "DESCRIPTION,";
        private string from = "WORKORDERS_V A";

        public IndieExampleSearchByWorkOrder(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(A.ASSET_NO) LIKE ('%" + query2 + "%') OR LOWER(A.REQ_NO) LIKE ('%" + query2 + "%'))";
            }



            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND (LOWER(A.COSTCENTERNAME) LIKE ('%" + filter.CostCenterStr + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.assetDescription))
            {
                whereClause += " AND (LOWER(A.DESCRIPTION) LIKE ('%" + filter.assetDescription + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.AssetFilterSuupiler))
            {
                whereClause += " AND (LOWER(A.SUPPIERNAME) LIKE ('%" + filter.AssetFilterSuupiler + "%'))";
            }



        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY A.REQ_NO DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }

    public class IndieExampleSearchByTestData : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "A.Id, A.DESCRIPTION, A.SUPPLIER_DESC, A.MANUFACTURER_DESC, A.JOBTYPE_DESC";
        private string columnsSimple = "DESCRIPTION,";
        private string from = "testTable_V A";

        public IndieExampleSearchByTestData(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(A.Id) LIKE ('%" + query2 + "%') OR LOWER(A.DESCRIPTION) LIKE ('%" + query2 + "%') OR LOWER(A.MANUFACTURER_DESC) LIKE ('%" + query2 + "%') OR LOWER(A.JOBTYPE_DESC) LIKE ('%" + query2 + "%'))";
            }

          
            if (!string.IsNullOrWhiteSpace(filter.AssetFilterSuupiler))
            {
                whereClause += " AND (LOWER(A.SUPPLIER_DESC) LIKE ('%" + filter.AssetFilterSuupiler + "%'))";
            }



        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY A.Id" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }
}