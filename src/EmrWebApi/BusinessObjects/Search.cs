﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using EmrWebApi.Models;

namespace EmrWebApi.BusinessObjects
{
    public class Search
    {
        private PetaPoco.Database db;

        public Search(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }

        public string SaveActions(string modifiedBy, string screenid )
        {
            User_Actions session = new User_Actions()
            {
                STAFF_ID = modifiedBy,
                LOGON_DATE = DateTime.Now.AddHours(7.0),
                USER_ID = modifiedBy,
                ScreenId = screenid
            };

            db.Insert(session);
            return "Okey";
        }

        public string UpdateStaffToken(string staffId)
        {
            Guid token = Guid.NewGuid();
            var tokenString = "";
            tokenString = token.ToString();

            using (var transScope = db.GetTransaction())
            {

                //preprocess data


                //apply processed data

                db.Execute("UPDATE STAFF SET TOKEN='" + token + "' WHERE STAFF_ID = '" + staffId + "'");

                transScope.Complete();
            }

            return tokenString;
        }

        public List<dynamic> SearchEngrDashboardCount(string staffId,  int maxRows = 1)
        {
            var infoFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchEngrDashboardCount(db._dbType.ToString(), staffId, maxRows)));

            if (infoFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return infoFound;
        }


    }

    public class IndieSearchEngrDashboardCount : IndieSql
    {
        private int maxRows = 1;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "L.ASSIGN_TO,L.CLOSESTATUS,L.HOLDSTATUS,L.NEWOPENSTATUS";
        private string columnsSimple = "ASSIGN_TO,CLOSESTATUS,HOLDSTATUS,NEWOPENSTATUS";
        private string from = "WORKORDER_DASHBOARDENRG_V L";

        public IndieSearchEngrDashboardCount(string dbType, string staffId,  int maxRows = 1)
        {
            this.DatabaseType = dbType;
            
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            whereClause = " L.ASSIGN_TO = '" +staffId+"'" ;
            


        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause};
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }
}