﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using EmrWebApi.Models;
using System.Data;
using System.Reflection;
using System.ComponentModel;
using System.Net;

namespace EmrWebApi.BusinessObjects
{
   class DocumentLibrary
    {
        #region CLASS
        private PetaPoco.Database db = null;

        public DocumentLibrary(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }

        public void Dispose()
        {
            db.Dispose();
        }
        #endregion

        public List<DOCUMENTLIBRARY> Upload(List<DOCUMENTLIBRARY> doc, string userId, string docNo, string docCategory, string pathFile, string searchKey, string deviceCode, string manufacturerCode, string modelCode)
        {
            var saudiDateTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");

            using (var transScope = db.GetTransaction())
            {

                //db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='DOCUMENTNO'");
                //var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "DOCUMENTNO" }));
                //var nextseq = seq[0].SEQ_VAL.ToString();

                foreach (DOCUMENTLIBRARY i in doc)
                {
                    i.CREATEDBY = userId;
                    i.DOCNO = docNo;
                    i.DEVICETYPECODE = deviceCode;
                    i.MANUFACTURERCODE = manufacturerCode;
                    i.MODELCODE = modelCode;
                    i.DOCCATEGORY = docCategory;
                    i.PATHFILE = pathFile;
                    i.SEARCHKEY = searchKey;
                    i.FILENAME = docNo+i.FILENAME;
                    i.CREATEDDATE = saudiDateTime;
                    db.Insert(i);
                }
                transScope.Complete();
            }
            return doc;
        }


        public string GetLastDocumentNo()
        {
            var nextseq = "";

            using (var transScope = db.GetTransaction())
            {

                db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='DOCUMENTNO'");
                var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "DOCUMENTNO" }));
                nextseq = seq[0].SEQ_VAL.ToString();

                transScope.Complete();
            }
            return nextseq;
        }

        public List<dynamic> SearchDocumentLibrary(string queryString, SearchFilter filter, int maxRows = 100000)
        {

            var docFound = new List<dynamic>();


            var assetFound = db.SingleOrDefault<AM_ASSET_DETAILS>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_DETAILS").Where("ASSET_NO=@assetId", new { assetId = queryString }));

            if (assetFound != null)
            {
                filter.AssetFilterDevice = assetFound.CATEGORY;
                docFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchDocumentLibraryDevice(db._dbType.ToString(), queryString, filter, maxRows)));

                if (docFound == null)
                {
                    throw new Exception("No match found.") { Source = this.GetType().Name };
                }



            }
            else
            {
                docFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchDocumentLibrary(db._dbType.ToString(), queryString, filter, maxRows)));

                if (docFound == null)
                {
                    throw new Exception("No match found.") { Source = this.GetType().Name };
                }
            }
           
            return docFound;
        }

        public List<DOCUMENTLIBRARY> SearchDocumentLibraryDocNo(string docNo)
        {
            List<DOCUMENTLIBRARY> docListFound = db.Fetch<DOCUMENTLIBRARY>(PetaPoco.Sql.Builder.Select("*").From("DOCUMENTLIBRARY").Where("DOCNO=@docNo", new { docNo = docNo }).Where("DELETEDBY is null"));


            return docListFound;
        }


        public int DeleteFileFromLibrary(string docId, string userId)
        {
            

            using (var transScope = db.GetTransaction())
            {
                var docFileFound = db.SingleOrDefault<DOCUMENTLIBRARY>(PetaPoco.Sql.Builder.Select("*").From("DOCUMENTLIBRARY").Where("DOCID=@docId", new { docId = docId }));

                if (docFileFound != null)
                {

                    docFileFound.DELETEDBY = userId;
                    docFileFound.DELETEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");


                    //apply processed data
                    db.Update(docFileFound, new string[] { "DELETEDBY", "DELETEDDATE"});
                }
                transScope.Complete();
            }

            return 0;
        }


        public List<dynamic> GetAllDeviceType(string queryString,  int maxRows = 100000)
        {
            var deviceTypeFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieGetDeviceType(db._dbType.ToString(), queryString, maxRows)));


            return deviceTypeFound;
        }


        public List<dynamic> GetAllManufacturer(string queryString, int maxRows = 100000)
        {
            var manufacturerFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieGetManufacturerModel(db._dbType.ToString(), queryString, maxRows)));


            return manufacturerFound;
        }

        public List<dynamic> GetAllAssetModel(string queryString, int maxRows = 100000)
        {
            var modelFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieGetModel(db._dbType.ToString(), queryString, maxRows)));


            return modelFound;
        }


        public List<DOCUMENTLIBRARY> SearchDocumentLibraryCriteria(string deviceCode, string manufacturerCode, string modelCode, string docCategory)
        {
            List<DOCUMENTLIBRARY> docListFound = db.Fetch<DOCUMENTLIBRARY>(PetaPoco.Sql.Builder.Select("*").From("DOCUMENTLIBRARY").Where("DEVICETYPECODE=@deviceC", new { deviceC = deviceCode }).Where("MANUFACTURERCODE=@manufacturerC", new { manufacturerC = manufacturerCode }).Where("MODELCODE=@modelC", new { modelC = modelCode }).Where("DOCCATEGORY=@docC", new { docC = docCategory }).Where("DELETEDBY is null"));


            return docListFound;
        }


        public class IndieSearchDocumentLibrary : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "P.DOCNO,P.DOCCATEGORY,P.SEARCHKEY,  COUNT(P.DOCID) AS NOOFFILE , DEVICETYPECODE, MANUFACTURERCODE, MODELCODE, LL.DESCRIPTION AS DEVICENAME, MM.DESCRIPTION AS MANUFACTURERNAME";
            private string columnsSimple = "DOCNO,CREATEDDATE,DOCCATEGORY,SEARCHKEY";
            private string from = "DOCUMENTLIBRARY P LEFT OUTER JOIN LOV_LOOKUPS LL ON P.DEVICETYPECODE =LL.LOV_LOOKUP_ID AND LL.CATEGORY = 'AIMS_DTYPE' LEFT OUTER JOIN AM_MANUFACTURER MM ON P.MANUFACTURERCODE = MM.manufacturer_id";

            public IndieSearchDocumentLibrary(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                foreach (string query in queryArray)
                {
                    var query2 = query.ToLower();
                    whereClause += " OR (LOWER(P.DOCNO) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(P.FILENAME) LIKE ('%" + query2 + "%')";
                    whereClause += " OR LOWER(P.DOCCATEGORY) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(P.SEARCHKEY) LIKE ('%" + query2 + "%')";
                    whereClause += " OR LOWER(P.MANUFACTURERCODE) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(P.MODELCODE) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(LL.DESCRIPTION) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(MM.DESCRIPTION) LIKE ('" + query2 + "%'))";
                }

                whereClause += "  AND DELETEDBY IS NULL AND DELETEDDATE IS NULL ";
            }


           

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT DISTINCT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause  + "   GROUP BY P.DOCNO,P.DOCCATEGORY,P.SEARCHKEY , DEVICETYPECODE, MANUFACTURERCODE, MODELCODE, LL.DESCRIPTION, MM.DESCRIPTION ORDER BY P.DOCNO desc" };
                    }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
                }
            }
        }


        public class IndieGetDeviceType : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "AM.CATEGORY,LL.DESCRIPTION";
            private string columnsSimple = "CATEGORY,DESCRIPTION";
            private string from = "AM_ASSET_DETAILS AM LEFT OUTER JOIN LOV_LOOKUPS LL ON AM.CATEGORY = LL.LOV_LOOKUP_ID AND LL.CATEGORY = 'AIMS_DTYPE'";

            public IndieGetDeviceType(string dbType, string queryString,  int maxRows = 50)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                foreach (string query in queryArray)
                {
                    var query2 = query.ToLower();
                    whereClause += " OR LOWER(AM.CATEGORY) LIKE ('" + query2 + "%')";
                   
                }

                whereClause += " AND AM.STATUS IN ('I', 'A', 'N') and LL.DESCRIPTION  is not null ";
            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT DISTINCT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + "   ORDER BY LL.DESCRIPTION" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
                }
            }
        }


        public class IndieGetManufacturerModel : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "AM.CATEGORY,AM.MANUFACTURER, MF.DESCRIPTION AS MANUFACTURERNAME";
            private string columnsSimple = "CATEGORY,MANUFACTURER";
            private string from = "AM_ASSET_DETAILS AM  LEFT OUTER JOIN AM_MANUFACTURER MF ON AM.MANUFACTURER = MF.MANUFACTURER_ID";

            public IndieGetManufacturerModel(string dbType, string queryString, int maxRows = 50)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                foreach (string query in queryArray)
                {
                    var query2 = query.ToLower();
                    whereClause += " OR LOWER(AM.CATEGORY) LIKE ('" + query2 + "%')";

                }

                whereClause += " AND AM.STATUS IN ('I', 'A', 'N') and MF.DESCRIPTION  is not null ";
            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT DISTINCT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + "   ORDER BY MF.DESCRIPTION" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
                }
            }
        }

        public class IndieGetModel : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "AM.CATEGORY, AM.MANUFACTURER, AM.MODEL_NO";
            private string columnsSimple = "CATEGORY,MANUFACTURER";
            private string from = "AM_ASSET_DETAILS AM";

            public IndieGetModel(string dbType, string queryString, int maxRows = 50)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                foreach (string query in queryArray)
                {
                    var query2 = query.ToLower();
                    whereClause += " OR LOWER(AM.CATEGORY) LIKE ('" + query2 + "%')";

                }

                whereClause += " AND AM.STATUS IN ('I', 'A', 'N') ";
            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT DISTINCT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause  };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
                }
            }
        }


        public class IndieSearchDocumentLibraryDevice : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "P.DOCNO,P.DOCCATEGORY,P.SEARCHKEY,  COUNT(P.DOCID) AS NOOFFILE , P.DEVICETYPECODE, P.MANUFACTURERCODE, P.MODELCODE, LL.DESCRIPTION AS DEVICENAME, MM.DESCRIPTION AS MANUFACTURERNAME";
            private string columnsSimple = "DOCNO,CREATEDDATE,DOCCATEGORY,SEARCHKEY";
            private string from = "DOCUMENTLIBRARY P LEFT OUTER JOIN LOV_LOOKUPS LL ON P.DEVICETYPECODE =LL.LOV_LOOKUP_ID AND LL.CATEGORY = 'AIMS_DTYPE' LEFT OUTER JOIN AM_MANUFACTURER MM ON P.MANUFACTURERCODE = MM.manufacturer_id";

            public IndieSearchDocumentLibraryDevice(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                foreach (string query in queryArray)
                {
                    var query2 = query.ToLower();
                    whereClause += " OR (LOWER(P.DOCNO) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(P.FILENAME) LIKE ('%" + query2 + "%')";
                    whereClause += " OR LOWER(P.DOCCATEGORY) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(P.SEARCHKEY) LIKE ('%" + query2 + "%'))";
                }

                if (!string.IsNullOrWhiteSpace(filter.AssetFilterDevice))
                {
                    whereClause += string.Format(" OR (LOWER(P.DEVICETYPECODE) = '" + filter.AssetFilterDevice + "')");

                }

                whereClause += "  AND DELETEDBY IS NULL AND DELETEDDATE IS NULL";


            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT DISTINCT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + "   GROUP BY P.DOCNO,P.DOCCATEGORY,P.SEARCHKEY , DEVICETYPECODE, MANUFACTURERCODE, MODELCODE, LL.DESCRIPTION, MM.DESCRIPTION ORDER BY P.DOCNO desc" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
                }
            }
        }



    }
}