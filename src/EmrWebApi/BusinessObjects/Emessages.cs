﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using EmrSys;
using EmrWebApi.Models;

namespace EmrWebApi.BusinessObjects
{
    public class Emessages
    {
        #region CLASS
        private PetaPoco.Database db = null;

        public Emessages(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }

        public void Dispose()
        {
            db.Dispose();
        }
        #endregion

        //HELP DESK
        public AM_HELPDESK CreateNewHelpDesk(AM_HELPDESK helpdeskDetails)
        {

            using (var transScope = db.GetTransaction())
            {

                helpdeskDetails.CONTACT_PERSON = Regex.Replace(helpdeskDetails.CONTACT_PERSON, "[^\\w\\._]", "");
                db.Insert(helpdeskDetails);

                transScope.Complete();
            }

            return helpdeskDetails;
        }

        public AM_HELPDESK ReadHelpDesk(string tickedID)
        {
            AM_HELPDESK ret = null;

            var productFound = db.SingleOrDefault<AM_HELPDESK>(PetaPoco.Sql.Builder.Select("*").From("AM_HELPDESK").Where("TICKET_ID=@ticketId", new { ticketId = tickedID }));

            if (productFound != null)
            {
                ret = productFound;
            }
            else
            {
                throw new Exception("Product not found.") { Source = this.GetType().Name };
            }
            return ret;
        }

        public int UpdateHelpDesk(AM_HELPDESK helpdeskDetails)
        {
            //validate data
            if (helpdeskDetails != null)
            {
                if (helpdeskDetails.TICKET_ID == 0)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(helpdeskDetails, new string[] { "CONTACT_PERSON", "CONTACT_NO", "LOCATION", "ASSET_NO", "ISSUE_DETAILS", "STEPDETAILS","COST_CENTER"
					, "ASSIGN_TO", "ASSIGN_DATE", "REF_WO", "PRIORITY" ,"CATEGORY", "STATUS","RECEIVED_BY","RECEIVED_DATE","ASSIGN_BY","ASSIGN_BYDATE","ACTION_DETAILS"});

                transScope.Complete();
            }

            return 0;
        }

        public int UpdateHelpDeskAccepted(string ticketId, string updateBy, string UpdateDate)
        {

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
               
                db.Execute("UPDATE AM_HELPDESK SET ACCEPTED_BY='" + updateBy + "', ACCEPTED_DATE='" + UpdateDate + "' WHERE TICKET_ID = " + ticketId);
                db.Execute("UPDATE AM_NOTIFY_MESSAGES SET STATUS='D' WHERE REF_CODE='" + ticketId + "' AND NOTIFY_ID= 9");

                transScope.Complete();
            }

            return 0;
        }

        public List<dynamic> SearchHelpDesk(string queryString, SearchFilter filter, int maxRows = 5000)
        {
            var helpdeskFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchHelpDesk(db._dbType.ToString(), queryString, filter, maxRows)));

            if (helpdeskFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return helpdeskFound;
        }
		public List<dynamic> SearchHelpDeskCostCenter(string queryString, SearchFilter filter, int maxRows = 5000)
		{
			var helpdeskFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchHelpDeskCostCenter(db._dbType.ToString(), queryString, filter, maxRows)));

			if (helpdeskFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return helpdeskFound;
		}

		public List<dynamic> SearchHelpDeskENGR(string queryString, SearchFilter filter, int maxRows = 5000)
		{
			var helpdeskFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchHelpDeskENGR(db._dbType.ToString(), queryString, filter, maxRows)));

			if (helpdeskFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return helpdeskFound;
		}

        public int HelpdeskRequesPendingByCostCenterCount(SearchFilter filter)
        {
            //SEARCH_COUNT inserviceAssets[] = null;
            var totalCount = 0;
            using (var transScope = db.GetTransaction())
            {
                var pendingRequest = db.SingleOrDefault<SEARCH_COUNT>(PetaPoco.Sql.Builder.Append("SELECT COUNT(TICKET_ID) AS COUNTROW FROM AM_HELPDESK WHERE STATUS in('OPEN','PENDING','ONHOLD' ) AND COST_CENTER in (SELECT CODE FROM STAFF_COSTCENTER_BUILDING WHERE UCATEGORY='AIMS_COSTCENTER' AND STAFFCODE ='" + filter.staffId + "')"));
                totalCount = pendingRequest.COUNTROW;


                transScope.Complete();

            }

            return totalCount;
        }

        public int HelpdeskRequesCompletedByCostCenterCount(SearchFilter filter)
        {
            //SEARCH_COUNT inserviceAssets[] = null;
            var totalCount = 0;
            using (var transScope = db.GetTransaction())
            {
                var pendingRequest = db.SingleOrDefault<SEARCH_COUNT>(PetaPoco.Sql.Builder.Append("SELECT COUNT(TICKET_ID) AS COUNTROW FROM AM_HELPDESK WHERE STATUS = 'COMPLETED' AND COST_CENTER in (SELECT CODE FROM STAFF_COSTCENTER_BUILDING WHERE UCATEGORY='AIMS_COSTCENTER' AND STAFFCODE ='" + filter.staffId + "')"));
                totalCount = pendingRequest.COUNTROW;


                transScope.Complete();

            }

            return totalCount;
        }
        public int HelpdeskRequesCancelledByCostCenterCount(SearchFilter filter)
        {
            //SEARCH_COUNT inserviceAssets[] = null;
            var totalCount = 0;
            using (var transScope = db.GetTransaction())
            {
                var pendingRequest = db.SingleOrDefault<SEARCH_COUNT>(PetaPoco.Sql.Builder.Append("SELECT COUNT(TICKET_ID) AS COUNTROW FROM AM_HELPDESK WHERE STATUS = 'CANCELLED' AND COST_CENTER in (SELECT CODE FROM STAFF_COSTCENTER_BUILDING WHERE UCATEGORY='AIMS_COSTCENTER' AND STAFFCODE ='" + filter.staffId + "')"));
                totalCount = pendingRequest.COUNTROW;


                transScope.Complete();

            }

            return totalCount;
        }
        public class IndieSearchHelpDesk : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "L.TICKET_ID,L.CONTACT_PERSON,L.ISSUE_DATE,L.CONTACT_NO, L.LOCATION, L.ASSIGN_TO, L.STATUS, L.PRIORITY, L.CATEGORY, SUBSTRING(L.ISSUE_DETAILS, 1, 100)  ISSUE_DETAILS, L.ASSET_NO, ST.DESCRIPTION ASSET_DESC";
            private string columnsSimple = "TICKET_ID,CONTACT_PERSON,ISSUE_DATE,CONTACT_NO, LOCATION";
            private string from = "AM_HELPDESK L LEFT JOIN AM_ASSET_DETAILS ST ON ST.ASSET_NO = L.ASSET_NO";

            public IndieSearchHelpDesk(string dbType, string queryString, SearchFilter filter, int maxRows = 500)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                foreach (string query in queryArray)
                {
                    var query2 = query.ToLower();
                    whereClause += " OR (LOWER(L.TICKET_ID) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(L.CONTACT_PERSON) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(L.LOCATION) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(L.ASSIGN_TO) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(L.STATUS) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(L.PRIORITY) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(L.CATEGORY) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(L.REF_WO) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(L.ASSET_NO) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(ST.DESCRIPTION) LIKE ('" + query2 + "%')) ";

                }

				if (!string.IsNullOrWhiteSpace(filter.staffId))
				{
					whereClause += " AND (UPPER(L.CREATED_BY) = UPPER('" + filter.staffId + "')";
					whereClause += " OR UPPER(L.ASSIGN_TO) = UPPER('" + filter.staffId + "')) ";

				}
			}

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + "ORDER BY ISSUE_DATE ASC"};
					}
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + "ORDER BY ISSUE_DATE ASC LIMIT " + maxRows.ToString() };
                }
            }
        }
		public class IndieSearchHelpDeskCostCenter : IndieSql
		{
            // search Tickets for an cost center 
            private int maxRows = 50;
			private string queryString = "";
			private string whereClause = "1=2 ";
			private string columns = "L.TICKET_ID,L.CONTACT_PERSON,L.ISSUE_DATE,L.CONTACT_NO, L.LOCATION, L.ASSIGN_TO, L.STATUS, L.PRIORITY, L.CATEGORY, SUBSTRING(L.ISSUE_DETAILS, 1, 100)  ISSUE_DETAILS, L.ASSET_NO, ST.DESCRIPTION ASSET_DESC, ST.COSTCENTER_DESC";
			private string columnsSimple = "TICKET_ID,CONTACT_PERSON,ISSUE_DATE,CONTACT_NO, LOCATION";
			private string from = "AM_HELPDESK L LEFT JOIN ASSET_V ST ON ST.ASSET_NO = L.ASSET_NO";

			public IndieSearchHelpDeskCostCenter(string dbType, string queryString, SearchFilter filter, int maxRows = 500)
			{
				this.DatabaseType = dbType;
				this.queryString = queryString;
				this.maxRows = maxRows;
				string[] queryArray = queryString.Split(new char[] { ' ' });

				foreach (string query in queryArray)
				{
					var query2 = query.ToLower();
					whereClause += " OR (LOWER(L.TICKET_ID) LIKE ('" + query2 + "%')";
					whereClause += " OR LOWER(L.CONTACT_PERSON) LIKE ('" + query2 + "%')";
					whereClause += " OR LOWER(L.LOCATION) LIKE ('" + query2 + "%')";
					whereClause += " OR LOWER(L.ASSIGN_TO) LIKE ('" + query2 + "%')";
					whereClause += " OR LOWER(L.STATUS) LIKE ('" + query2 + "%')";
					whereClause += " OR LOWER(L.PRIORITY) LIKE ('" + query2 + "%')";
					whereClause += " OR LOWER(L.CATEGORY) LIKE ('" + query2 + "%')";
					whereClause += " OR LOWER(L.REF_WO) LIKE ('" + query2 + "%')";
					whereClause += " OR LOWER(L.ASSET_NO) LIKE ('" + query2 + "%')";
					whereClause += " OR LOWER(ST.DESCRIPTION) LIKE ('" + query2 + "%'))";
                }

                if (!string.IsNullOrWhiteSpace(filter.staffId))
				{
					whereClause += string.Format(" AND L.COST_CENTER in (SELECT CODE FROM STAFF_COSTCENTER_BUILDING WHERE UCATEGORY='AIMS_COSTCENTER' AND STAFFCODE ='" + filter.staffId + "')");

				}

                if (!string.IsNullOrWhiteSpace(filter.HelpDeskRequestStatus))
                {
                    whereClause += string.Format(" AND L.STATUS in ( " + filter.HelpDeskRequestStatus + ")");

                }
            }

			public override StmtOracle OracleSql
			{
				get
				{
					return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
				}
			}

			public override StmtSqlServer SqlServerSql
			{
				get
				{
					return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + "ORDER BY TICKET_ID DESC" };
				}
			}

			public override StmtMySql MySql
			{
				get
				{
					return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + "ORDER BY ISSUE_DATE ASC LIMIT " + maxRows.ToString() };
				}
			}
		}

		public class IndieSearchHelpDeskENGR : IndieSql
		{
			private int maxRows = 50;
			private string queryString = "";
			private string whereClause = "1=2 ";
			private string columns = "L.TICKET_ID,L.CONTACT_PERSON,L.ISSUE_DATE,L.CONTACT_NO, L.LOCATION, L.ASSIGN_TO, L.STATUS, L.PRIORITY, L.CATEGORY, SUBSTRING(L.ISSUE_DETAILS, 1, 100)  ISSUE_DETAILS, L.ASSET_NO, ST.DESCRIPTION ASSET_DESC, S.FIRST_NAME,S.LAST_NAME";
			private string columnsSimple = "TICKET_ID,CONTACT_PERSON,ISSUE_DATE,CONTACT_NO, LOCATION";
			private string from = "AM_HELPDESK L LEFT JOIN AM_ASSET_DETAILS ST ON ST.ASSET_NO = L.ASSET_NO LEFT JOIN STAFF S ON S.STAFF_ID = L.REQ_STAFF_ID";

			public IndieSearchHelpDeskENGR(string dbType, string queryString, SearchFilter filter, int maxRows = 500)
			{
				this.DatabaseType = dbType;
				this.queryString = queryString;
				this.maxRows = maxRows;
				string[] queryArray = queryString.Split(new char[] { ' ' });

				foreach (string query in queryArray)
				{
					var query2 = query.ToLower();
					whereClause += " OR (LOWER(L.TICKET_ID) LIKE ('" + query2 + "%')";
					whereClause += " OR LOWER(L.CONTACT_PERSON) LIKE ('" + query2 + "%')";
					whereClause += " OR LOWER(L.LOCATION) LIKE ('" + query2 + "%')";
					whereClause += " OR LOWER(L.ASSIGN_TO) LIKE ('" + query2 + "%')";
					whereClause += " OR LOWER(L.STATUS) LIKE ('" + query2 + "%')";
					whereClause += " OR LOWER(L.PRIORITY) LIKE ('" + query2 + "%')";
					whereClause += " OR LOWER(L.CATEGORY) LIKE ('" + query2 + "%')";
					whereClause += " OR LOWER(L.REF_WO) LIKE ('" + query2 + "%')";
					whereClause += " OR LOWER(L.ASSET_NO) LIKE ('" + query2 + "%')";
					whereClause += " OR LOWER(ST.DESCRIPTION) LIKE ('" + query2 + "%'))";

				}

				if (!string.IsNullOrWhiteSpace(filter.staffId))
				{
					whereClause += " AND UPPER(L.ASSIGN_TO) = UPPER('" + filter.staffId + "')";
					

				}
                whereClause += " AND (L.REF_WO is null OR L.REF_WO = '') ";
            }

			public override StmtOracle OracleSql
			{
				get
				{
					return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
				}
			}

			public override StmtSqlServer SqlServerSql
			{
				get
				{
					return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + "ORDER BY TICKET_ID DESC" };
				}
			}

			public override StmtMySql MySql
			{
				get
				{
					return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + "ORDER BY ISSUE_DATE ASC LIMIT " + maxRows.ToString() };
				}
			}
		}
	}

}
