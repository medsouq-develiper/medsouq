﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using EmrWebApi.Models;

namespace EmrWebApi.BusinessObjects
{
    public class WorkOrderLabour
    {
        private PetaPoco.Database db;

        public WorkOrderLabour(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }

        public List<dynamic> SearchByWorkOrderLabourRpt(string queryString, SearchFilter filter, int maxRows = 1000)
        {
            var assetListFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchByWorkOrderLabourRpt(db._dbType.ToString(), queryString, filter, maxRows)));

            if (assetListFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return assetListFound;
        }

    }

    public class IndieSearchByWorkOrderLabourRpt : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1";
        private string columns = "A.req_no,A.asset_no,A.Description,A.model_name,A.ReqDate,A.DueDate,A.CostCenter,A.manufacturer,A.supplier,A.wo_status,A.PROBLEM_TYPEDESC,A.assign_to,A.serial_no," +
                                 "A.labour_action,A.PM_TYPE";
        private string columnsSimple = "DESCRIPTION,";
        private string from = "WorkOrderLabourAction_V A";

        public IndieSearchByWorkOrderLabourRpt(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " And (LOWER(A.req_no) LIKE ('%" + query2 + "%') and LOWER(A.Description) LIKE ('%" + query2 + "%'))" + " and(LOWER(A.asset_no) LIKE('%" + query2 + "%') ) ";
            }

            if (!string.IsNullOrWhiteSpace(filter.AssetFilterSuupiler))
            {
                whereClause += " AND (LOWER(A.supplier) LIKE ('%" + filter.AssetFilterSuupiler + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.AssetFilterManufacturer))
            {
                whereClause += " AND (LOWER(A.manufacturer) LIKE ('%" + filter.AssetFilterManufacturer + "%'))";
            }
            if(!string.IsNullOrWhiteSpace(filter.Status))
            {
                if (filter.Status== "PSOP") { whereClause += " AND (LOWER(A.wo_status)='op' or (LOWER(A.wo_status)='ne' ))"; } else { 
                whereClause += " AND (LOWER(A.wo_status)) = '" + filter.Status.ToLower() + "'";
                }
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND (LOWER(A.CostCenter) LIKE ('%" + filter.CostCenterStr.ToLower() + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.ProblemType))
            {
                whereClause += " AND (Lower(A.PROBLEM_TYPEDESC)=Lower('" +  filter.ProblemType + "'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.pmtype))
            {
                whereClause += " AND (LOWER(A.PM_TYPE) = Lower('" + filter.pmtype + "'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("convert(date,A.Req_date) >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" AND convert(date,A.Req_date) <= '{0}'", filter.ToDate.Substring(0,10));
                }
                whereClause += " AND (" + whereDate + ")";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY A.Req_No" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }
}
