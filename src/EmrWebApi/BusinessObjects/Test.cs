﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using EmrWebApi.Models;
namespace EmrWebApi.BusinessObjects
{
    public class Test
    {
        private PetaPoco.Database db;

        public Test(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }
        //Search
        public List<dynamic> ExampleSearchByWordOrder(string queryString, SearchFilter filter, int maxRows = 100000)
        {
            var assetListFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieExampleSearchByWorkOrder(db._dbType.ToString(), queryString, filter, maxRows)));

            if (assetListFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return assetListFound;
        }

        public class IndieExampleSearchByWorkOrder : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "A.REQ_NO,A.ASSET_NO, A.PROBLEM_TYPEDESC, A.COSTCENTERNAME";
            private string columnsSimple = "DESCRIPTION,";
            private string from = "WORKORDERREPT_V A";

            public IndieExampleSearchByWorkOrder(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                foreach (string query in queryArray)
                {
                    var query2 = query.ToLower();
                    whereClause += " OR (LOWER(A.COSTCENTERNAME) LIKE ('%" + query2 + "%') OR LOWER(A.REQ_NO) LIKE ('%" + query2 + "%'))";
                }

                if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
                {
                    whereClause += " AND (LOWER(A.COSTCENTER_DESC) LIKE ('%" + filter.CostCenterStr + "%'))";
                }

            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY A.ASSET_NO" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
                }
            }
        }
    }
}