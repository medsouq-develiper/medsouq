﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using EmrSys;
using EmrWebApi.Models;

namespace EmrWebApi.BusinessObjects
{
    class SystemApps
    {
        #region CLASS
        private PetaPoco.Database db = null;

        public SystemApps(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }

        public void Dispose()
        {
            db.Dispose();
        }
        #endregion

        public List<SYSTEM_DICTIONARY> CreateNew(List<SYSTEM_DICTIONARY> dictionaryList)
        {
            using (var transScope = db.GetTransaction())
            {

                //DateTime createDt = DateTime.Parse(createDate);
                foreach (SYSTEM_DICTIONARY i in dictionaryList)
                {
                    
                    if(i.DICTIONARY_ID == 0)
                    {
                        db.Insert(i);
                    }
                    else
                    {
                        var dictionaryFound = db.SingleOrDefault<SYSTEM_DICTIONARY>(PetaPoco.Sql.Builder.Select("*").From("SYSTEM_DICTIONARY").Where("DICTIONARY_ID=@dictionaryId", new { dictionaryId = i.DICTIONARY_ID }));
                        if (dictionaryFound != null)
                        {
                            dictionaryFound.DESCRIPTION = i.DESCRIPTION;
                            dictionaryFound.DESCRIPTION_OTH = i.DESCRIPTION_OTH;
                            dictionaryFound.REMARKS = i.REMARKS;

                            db.Update(dictionaryFound, new string[] { "DESCRIPTION", "DESCRIPTION_OTH", "REMARKS" });
                            // db.Execute("UPDATE SYSTEM_DICTIONARY SET DESCRIPTION_OTH='" + i.DESCRIPTION_OTH + "'  WHERE DICTIONARY_ID =" + i.DICTIONARY_ID);
                        }
                        else
                        {


                            db.Insert(i);
                        }
                    }
                    

                }

                transScope.Complete();
            }
            return dictionaryList;
        }

       
        public SYSTEM_DICTIONARY ReadExisting(string dicID)
        {
            SYSTEM_DICTIONARY ret = null;

            var productFound = db.SingleOrDefault<SYSTEM_DICTIONARY>(PetaPoco.Sql.Builder.Select("*").From("SYSTEM_DICTIONARY").Where("DICTIONARY_ID=@dicId", new { dicId = dicID }));

            if (productFound != null)
            {
                ret = productFound;
            }
            else
            {
                throw new Exception("Product not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdateDictionary(SYSTEM_DICTIONARY dicDetails)
        {
          
            using (var transScope = db.GetTransaction())
            {

                //preprocess data
                //apply processed data
                db.Update(dicDetails, new string[] { "DESCRIPTION", "DESCRIPTION_OTH", "CATEGORY"});

                transScope.Complete();
            }

            return 0;
        }

        public List<dynamic> SearchDictionary(string queryString, string actionStr, int maxRows = 5000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchDictionary(db._dbType.ToString(), queryString, actionStr, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
    }

    public class IndieSearchDictionary : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "CD.DICTIONARY_ID, CD.DESCRIPTION, CD.DESCRIPTION_OTH, CD.REMARKS";
        private string columnsSimple = "DICTIONARY_ID,DESCRIPTION, DESCRIPTION_OTH, REMARKS";
        private string from = "SYSTEM_DICTIONARY CD";

        public IndieSearchDictionary(string dbType, string queryString, string actionStr, int maxRows = 1000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(CD.DICTIONARY_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(CD.DESCRIPTION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(CD.DESCRIPTION_OTH) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(CD.REMARKS) LIKE ('" + query2 + "%')";

            }

      
            if (actionStr != null && actionStr != "")
            {
                if (actionStr == "A")
                {
                    whereClause += " AND (CD.DESCRIPTION_OTH IS NOT NULL )";
                }
                else
                {
                    whereClause += " AND (CD.DESCRIPTION_OTH IS NULL )";
                }
                
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY CLINIC_DESCRIPTION " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC " + " LIMIT " + maxRows.ToString() };
            }
        }


    }
}
