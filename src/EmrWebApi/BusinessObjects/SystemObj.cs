﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using EmrWebApi.Models;

namespace EmrWebApi.BusinessObjects
{
    public class SystemObj
    {
        #region CLASS
        private PetaPoco.Database db;

        public SystemObj(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }

        public void Dispose(){
            db.Dispose();
        }
        #endregion

        #region NATIONALITY
       
        #endregion

        #region COUNTRY
        public List<COUNTRY> GetCountryList()
        {
            var countryList = db.Fetch<COUNTRY>(PetaPoco.Sql.Builder.Select("*").From("COUNTRY").OrderBy("DESCRIPTION"));

            if (countryList == null)
            {
                throw new Exception("Not Found") { Source = this.GetType().Name };
            }
            return countryList;
        }
        #endregion
    }
}