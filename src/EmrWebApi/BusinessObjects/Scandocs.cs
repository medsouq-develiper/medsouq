﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using EmrSys;
using EmrWebApi.Models;

namespace EmrWebApi.BusinessObjects
{
    class Scandocs
    {
        #region CLASS
        private PetaPoco.Database db = null;

        public Scandocs(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }

        public void Dispose()
        {
            db.Dispose();
        }
        #endregion

     
        public List<dynamic> SearchScandocsByPatientCategory(string patientId, int maxRows = 50)
        {
            var itemsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchScandocsByPatientCategory(db._dbType.ToString(), patientId, maxRows)));

            if (itemsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return itemsFound;
        }

        public List<dynamic> SearchScandocsByPatientCategoryID(string patientId, string categoryId, int maxRows = 50)
        {
            var itemsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchScandocsByPatientCategoryID(db._dbType.ToString(), patientId, categoryId, maxRows)));

            if (itemsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return itemsFound;
        }

        public List<dynamic> SearchScandocsByPatientImageID(string patientId, string imageId, int maxRows = 50)
        {
            var itemsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchScandocsByPatientimageID(db._dbType.ToString(), patientId, imageId, maxRows)));

            if (itemsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return itemsFound;
        }

        public List<dynamic> SearchConsultImagesSketchTemp(string quiryStr, int maxRows = 50)
        {
            var itemsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchConsultImagesSketchTemp(db._dbType.ToString(), quiryStr, maxRows)));

            if (itemsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return itemsFound;
        }
        public class IndieSearchConsultImagesSketchTemp : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "(1=2 ";
            private string columns = "RD.SCANDOC_ID, RD.SCANDOCS_DATA,RD.FILE_NAME";
            private string columnsSimple = "SCANDOC_ID, SCANDOCS_DATA,FILE_NAME";
            private string from = " CONSULTATION_IMGSKETCHTEMP RD";

            public IndieSearchConsultImagesSketchTemp(string dbType, string queryString, int maxRows = 15)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                foreach (string query in queryArray)
                {
                    var query2 = query.ToLower();
                    whereClause += " OR (LOWER(RD.FILE_NAME) like ('%" + query2 + "%'))";
                }

                whereClause += ") AND (RD.STATUS = 'A')";



            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY CLINIC_DESCRIPTION " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
                }
            }


        }
        public class IndieSearchScandocsByPatientimageID : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "(1=2 ";
            private string columns = "RD.SCANDOCBKG_DATA, RD.NOTES, RD.CATEGORY,RD.SCANDOCS_DATA,RD.SCANDOC_ID, RD.PATIENT_ID,FILE_NAME, CREATED_DATE";
            private string columnsSimple = "SCANDOCBKG_DATA,NOTES, CATEGORY,SCANDOCS_DATA,SCANDOC_ID, PATIENT_ID,FILE_NAME,CREATED_DATE";
            private string from = " PATIENT_SCANDOCS RD";

            public IndieSearchScandocsByPatientimageID(string dbType, string queryString, string imageId, int maxRows = 6000)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                foreach (string query in queryArray)
                {
                    var query2 = query.ToLower();
                    whereClause += " OR (LOWER(RD.PATIENT_ID) = ('" + query2 + "'))";
                }

                whereClause += ") AND (RD.SCANDOC_ID='" + imageId + "')";

            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY CLINIC_DESCRIPTION " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY CREATED_DATE DESC " };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY CREATED_DATE DESC " + " LIMIT " + maxRows.ToString() };
                }
            }


        }
        public class IndieSearchScandocsByPatientCategoryID : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "(1=2 ";
            private string columns = "RD.CATEGORY,RD.SCANDOC_ID, RD.PATIENT_ID,FILE_NAME, CREATED_DATE";
            private string columnsSimple = "CATEGORY,SCANDOC_ID, PATIENT_ID,FILE_NAME,CREATED_DATE";
            private string from = " PATIENT_SCANDOCS RD";

            public IndieSearchScandocsByPatientCategoryID(string dbType, string queryString,string categoryId, int maxRows = 6000)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                foreach (string query in queryArray)
                {
                    var query2 = query.ToLower();
                    whereClause += " OR (LOWER(RD.PATIENT_ID) = ('" + query2 + "'))";
                }

                whereClause += ") AND (RD.CATEGORY='" + categoryId +"')";

            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY CLINIC_DESCRIPTION " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY CREATED_DATE DESC " };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY CREATED_DATE DESC " + " LIMIT " + maxRows.ToString() };
                }
            }


        }

        public class IndieSearchScandocsByPatientCategory : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "(1=2 ";
            private string columns = "RD.CATEGORY, RD.PATIENT_ID,COUNT(*) TOTALRECORDS";
            private string columnsSimple = "CAGETORY, PATIENT_ID,COUNT(*)";
            private string from = " PATIENT_SCANDOCS RD";

            public IndieSearchScandocsByPatientCategory(string dbType, string queryString, int maxRows = 6000)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                foreach (string query in queryArray)
                {
                    var query2 = query.ToLower();
                    whereClause += " OR LOWER(RD.PATIENT_ID) = ('" + query2 + "'))";
                }



            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY CLINIC_DESCRIPTION " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " GROUP BY  CATEGORY,PATIENT_ID " };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " GROUP BY  CATEGORY,PATIENT_ID " + " LIMIT " + maxRows.ToString() };
                }
            }


        }
    }
}
