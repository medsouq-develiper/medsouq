﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using EmrWebApi.Models;

namespace EmrWebApi.BusinessObjects
{
    public class Hospital
    {
        #region CLASS
        private PetaPoco.Database db;

        public Hospital(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }

        public void Dispose(){
            db.Dispose();
        }
        #endregion

        #region DEPARTMENT
        public DEPARTMENT CreateDepartment(DEPARTMENT department)
        {
            //validate data
            if (department != null)
            {
                if (department.DEPARTMENT_ID != null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }
            else
            {
                throw new Exception("Missing data.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {
                
                //preprocess data
                department.DEPARTMENT_ID = Guid.NewGuid().ToString("N");
                
                //apply processed data
                db.Insert(department);

                transScope.Complete();
            }

            return department;
        }

        public DEPARTMENT ReadDepartment(string departmentID)
        {
            DEPARTMENT ret = null;
            
            var departmentFound = db.SingleOrDefault<DEPARTMENT>(PetaPoco.Sql.Builder.Select("*").From("DEPARTMENT").Where("DEPARTMENT_ID=@departmentId", new { departmentId = departmentID }));

            if (departmentFound != null)
            {
                ret = departmentFound;
            }
            else
            {
                throw new Exception("Department not found.") { Source = this.GetType().Name };
            }
            return ret;
        }

        public int UpdateDepartment(DEPARTMENT department)
        {
            //validate data
            if (department != null)
            {
                if (department.DEPARTMENT_ID == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(department);

                transScope.Complete();
            }

            return 0;
        }

        public List<DEPARTMENT> GetDepartmentList()
        {
            var departmentList = db.Fetch<DEPARTMENT>(PetaPoco.Sql.Builder.Select("*").From("DEPARTMENT"));

            if (departmentList == null)
            {
                throw new Exception("Not Found") { Source = this.GetType().Name };
            }
            return departmentList;
        }
        #endregion

        #region HOSPITAL_DETAIL
        public HOSPITAL_DETAIL CreateHosp(HOSPITAL_DETAIL hospital)
        {
            //validate data
            if (hospital != null)
            {
                if (hospital.HOSPITAL_ID != null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }
            else
            {
                throw new Exception("Missing data.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data
                hospital.HOSPITAL_ID = Guid.NewGuid().ToString("N");

                //apply processed data
                db.Insert(hospital);

                transScope.Complete();
            }

            return hospital;
        }

        public HOSPITAL_DETAIL ReadHospital(string hospitalID)
        {
            HOSPITAL_DETAIL ret = null;

            var hospitalFound = db.SingleOrDefault<HOSPITAL_DETAIL>(PetaPoco.Sql.Builder.Select("*").From("HOSPITAL_DETAIL").Where("HOSPITAL_ID=@hospitalId", new { hospitalId = hospitalID }));

            if (hospitalFound != null)
            {
                ret = hospitalFound;
            }
            else
            {
                throw new Exception("Hospital not found.") { Source = this.GetType().Name };
            }
            return ret;
        }

        public int UpdateHospital(HOSPITAL_DETAIL hospital)
        {
            //validate data
            if (hospital != null)
            {
                if (hospital.HOSPITAL_ID == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(hospital);

                transScope.Complete();
            }

            return 0;
        }

        public List<HOSPITAL_DETAIL> GetHospitalList()
        {
            var hospitalList = db.Fetch<HOSPITAL_DETAIL>(PetaPoco.Sql.Builder.Select("*").From("HOSPITAL_DETAIL"));

            if (hospitalList == null)
            {
                throw new Exception("Not Found") { Source = this.GetType().Name };
            }
            return hospitalList;
        }
        #endregion
    }
}