﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmrWebApi.BusinessObjects
{
    public interface ISql
    {
        string SqlStatement { get; set; }
    }
}
