﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.BusinessObjects
{
    [System.ComponentModel.DefaultProperty("SqlStatement")]
    public class StmtSqlServer : ISql
    {
        public string SqlStatement { get; set; }
    }
}