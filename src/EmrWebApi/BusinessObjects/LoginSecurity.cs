﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
//Project Related
using EmrWebApi.Models;

namespace EmrWebApi.BusinessObjects
{
    public class LoginSecurity
    {
        private PetaPoco.Database db;
        public LoginSecurity()
        {
            db = new PetaPoco.Database("ConStrProd");
        }

        //public SecurityDR()
        //{
        //    db = new PetaPoco.Database("ConStrProdDr");
        //}
        public PetaPoco.Database GetSharedDBInstance
        {
            get { return db; }
        }

        public void Dispose()
        {
            db.Dispose();
        }

        /// <summary>
        /// Authenticate a user and return an appropriate STAFF_LOGON_SESSIONS object
        /// </summary>
        /// <param name="user"></param>
        /// <returns>Returns exception if the user authentication failed</returns>
        public Dictionary<string, object> AuthenticateUser(STAFF user, string passkeystr)
        {
            Dictionary<string, object> ret = null;

			var endDate = DateTime.Now;
			endDate = endDate.AddDays(40);
            if (endDate <= DateTime.Now.AddHours(7.0))
			{				
				throw new Exception("License Expired") { Source = this.GetType().Name };
			}
			var db = new PetaPoco.Database("ConStrProd");

            var userFound = db.SingleOrDefault<STAFF>(PetaPoco.Sql.Builder.Select("*").From("STAFF").Where("USER_ID=@userId", new { userId = user.USER_ID.ToLower() }));

            if (userFound != null)
            {

				//encrypt password
				

				//var ls_ret = "";
				var ls_key = "";

				var li_encrypt_len = 0;
				var li_key_len = 0;

				var as_key1 = "bVyYfGhvoVorazlpxowkbkdxGkByGMSxLSdcc";

				var as_toencrypt = user.PASSWORD;

				li_encrypt_len = as_toencrypt.Length;

				ls_key = as_key1 + user.USER_ID;
				li_key_len = ls_key.Length;


				var ls_key_bin = "";

				var iix = 0;

				for (var i = 0; i < as_toencrypt.Length; ++i)
				{
					var res = as_key1.Substring(+iix);
					iix++;
					var restcode = res.Substring(0, 1);
					var keystr = ((int)as_toencrypt[i]).ToString();
					var keyInt = Convert.ToInt32(keystr) * 2;
					char keystrChar = (char)(keyInt);

					ls_key_bin += keystrChar;
					ls_key_bin += restcode;
					//bytes.push(ls_key_bin);
				}


                 user.PASSWORD = ls_key_bin;
                if (user.PASSWORD == userFound.PASSWORD)
					
				{ //implement an encrypt/decrypt
					//string clientAddress = HttpContext.Current.Request.UserHostAddress;
					String hostName = Dns.GetHostName();
                    IPAddress[] ipaddress = Dns.GetHostAddresses(hostName);

                    string clientAddress = HttpContext.Current.Request.UserHostAddress;
                    //var clientIpAddress = "";
                    //var clientIEIpAddress = "";
                    if (ipaddress.Count() >= 4)
					{
						//clientIpAddress = ipaddress[2].ToString();
						//clientIEIpAddress = ipaddress[4].ToString();
					}
                    //string ip = HttpContext.Current.Request.UserHostAddress;
 
                    //string[] parta = ipaddress[0].ToString().Split('.');
                    //int value1 = Int32.Parse(parta[0]);   // 192
                    //int value2 = Int32.Parse(parta[1]);   // 168
                    //int value3 = Int32.Parse(parta[2]);   // 0
                    //int value4 = Int32.Parse(parta[3]);

                    STAFF_LOGON_SESSIONS session = new STAFF_LOGON_SESSIONS()
                    {
                        SESSION_ID = Guid.NewGuid().ToString("N"),
                        STAFF_ID = userFound.STAFF_ID.ToUpper(),
                        //LOGON_DATE = DateTime.Now,       
                        LOGON_DATE = DateTime.Now.AddHours(7.0),
                        // TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                        USER_ID = userFound.USER_ID.ToUpper(),
                        LOGIN_HOSTNAME = clientAddress,
                        LOGIN_IP = clientAddress,    // value1 + "."+value2+"."+value3+"."+value4,   //  clientAddress
                        LOGINEIIP = "",
                        LOGINEXPIRY = endDate
                    };

                    db.Delete("STAFF_LOGON_SESSIONS", "STAFF_ID", session);  
                    db.Insert(session);
                    //<Mortada>
                    string NewDate = DateTime.Now.AddMonths(1).ToString("yyyyMM");
                    var ppmStatus = "Y";
                    var ppmFound = db.SingleOrDefault<PPM_Tracking>(PetaPoco.Sql.Builder.Select("*").From("PPM_Tracking").Where("PPM_Date=@PPM_DateTm", new { PPM_DateTm = NewDate }));
                    if (ppmFound == null && (userFound.STAFF_GROUP_ID == "AIMS_ADMIN" || userFound.STAFF_GROUP_ID == "aa3fd883ba754442b9bc6ce99162174b"))
                    {
                        ppmStatus = "N";
                        PPM_Tracking ppmtracking = new PPM_Tracking();
                        ppmtracking.Staff_Id = userFound.STAFF_ID.ToUpper();
                        ppmtracking.PPM_Date = NewDate;
                        ppmtracking.PPM_Status = "Done";
                        db.Insert(ppmtracking);
                    }


                    var groupPrivilegesList = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append("SELECT STAFF_GROUP_ID,PRIVILEGES_ID FROM GROUP_PRIVILEGES WHERE STAFF_GROUP_ID ='"+ userFound.STAFF_GROUP_ID+"'"));

					
					var staffDachBoard = db.SingleOrDefault<STAFF_GROUPS>(PetaPoco.Sql.Builder.Select("*").From("STAFF_GROUPS").Where("STAFF_GROUP_ID=@userGroupId", new { userGroupId = userFound.STAFF_GROUP_ID }));
					userFound.HOME_DEFAULT = staffDachBoard.DASHBOARDURL;

                    var userBuildingList = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append("SELECT * FROM USER_BUILDING_V WHERE USER_ID ='" + userFound.USER_ID + "'"));

                    if(userBuildingList.Count() <= 0)
                    {
                        userBuildingList = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append("SELECT * FROM CLIENT_BUILDING_V"));
                        
                    }

                    var userClient = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append("SELECT DISTINCT CLIENT_CODE, ShortName,Description,LogoPath FROM CLIENT_V WHERE COSTCENTER  IN (SELECT CODE FROM STAFF_COSTCENTER_BUILDING WHERE STAFFCODE ='" + userFound.STAFF_ID + "')"));

                    var defaultCostCenter = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append("SELECT CC.CODEID, CC.DESCRIPTION, ST.STORE_ID FROM SYSTEM_DEFAULT AS SD LEFT OUTER JOIN COSTCENTER_DETAILS AS CC ON SD.SYSDEFAULTVAL = CC.CODEID  LEFT OUTER JOIN STORES ST ON CC.CODEID = ST.CLIENT_CODE WHERE SYSDEFAULT ='BSC'"));

                    userFound.USER_ID = ";-D";
                    userFound.PASSWORD = ";-D";
                    userFound.STAFF_ID = userFound.STAFF_ID.ToUpper();
                    userFound.USER_ID = userFound.USER_ID.ToUpper();


                    ret = new Dictionary<string, object>() { { "STAFF_LOGON_SESSIONS", session }, { "STAFF", userFound }, { "GROUP_PRIVILEGES", groupPrivilegesList }, { "PPMSTATUS", ppmStatus }, { "USER_BUILDING", userBuildingList }, { "USER_CLIENT", userClient }, { "SYSTEM_DEFAULT", defaultCostCenter } };
                }
                else {
                    throw new Exception("Invalid user credentials.") { Source = this.GetType().Name };
                }
            }
            else {
                throw new Exception("User not found.") { Source = this.GetType().Name };
            }
            return ret;
        }

		public Dictionary<string, object> updateUser(STAFF user, string passkeystr)
		{
			Dictionary<string, object> ret = null;
			var db = new PetaPoco.Database("ConStrProd");

			var userFound = db.SingleOrDefault<STAFF>(PetaPoco.Sql.Builder.Select("*").From("STAFF").Where("USER_ID=@userId", new { userId = user.USER_ID.ToLower() }));

			if (userFound != null)
			{

				using (var transScope = db.GetTransaction())
				{
					var ls_key = "";

					var li_encrypt_len = 0;
					var li_key_len = 0;

					var as_key1 = "bVyYfGhvoVorazlpxowkbkdxGkByGMSxLSdcc";

					var as_toencrypt = passkeystr;

					li_encrypt_len = as_toencrypt.Length;

					ls_key = as_key1 + user.USER_ID;
					li_key_len = ls_key.Length;


					var ls_key_bin = "";

					var iix = 0;

					for (var i = 0; i < as_toencrypt.Length; ++i)
					{
						var res = as_key1.Substring(+iix);
						iix++;
						var restcode = res.Substring(0, 1);
						var keystr = ((int)as_toencrypt[i]).ToString();
						var keyInt = Convert.ToInt32(keystr) * 2;
						char keystrChar = (char)(keyInt);

						ls_key_bin += keystrChar;
						ls_key_bin += restcode;
						//bytes.push(ls_key_bin);
					}
					userFound.PASSWORD = ls_key_bin;
					userFound.PROMPT_PASSWORD = "N";
					db.Update(userFound, new string[]{"PASSWORD","PROMPT_PASSWORD"});

					transScope.Complete();

					ret = new Dictionary<string, object>() { { "CHANGEPASSED", "CHANGEPASSED" } };
				}
			}
			else
			{
				throw new Exception("User not found.") { Source = this.GetType().Name };
			}
			return ret;
		}


        public Dictionary<string, object> updateUserPasswordChange(SearchFilter filter, string passkeystr)
        {
            Dictionary<string, object> ret = null;
            var db = new PetaPoco.Database("ConStrProd");

            var userFound = db.SingleOrDefault<STAFF>(PetaPoco.Sql.Builder.Select("*").From("STAFF").Where("USER_ID=@userId", new { userId = filter.userid.ToLower() }));

            if (userFound != null)
            {

                //validate password if correct
                var ls_key = "";

                var li_encrypt_len = 0;
                var li_key_len = 0;

                var as_key1 = "bVyYfGhvoVorazlpxowkbkdxGkByGMSxLSdcc";

                var as_toencrypt = filter.curpassword;

                li_encrypt_len = as_toencrypt.Length;

                ls_key = as_key1 + filter.userid;
                li_key_len = ls_key.Length;


                var ls_key_bin = "";

                var iix = 0;

                for (var i = 0; i < as_toencrypt.Length; ++i)
                {
                    var res = as_key1.Substring(+iix);
                    iix++;
                    var restcode = res.Substring(0, 1);
                    var keystr = ((int)as_toencrypt[i]).ToString();
                    var keyInt = Convert.ToInt32(keystr) * 2;
                    char keystrChar = (char)(keyInt);

                    ls_key_bin += keystrChar;
                    ls_key_bin += restcode;
                    //bytes.push(ls_key_bin);
                }

                filter.curpassword = ls_key_bin;
                if(filter.curpassword == userFound.PASSWORD)
                {

                    using (var transScope = db.GetTransaction())
                    {
                        ls_key = "";

                        li_encrypt_len = 0;
                        li_key_len = 0;

                        as_key1 = "bVyYfGhvoVorazlpxowkbkdxGkByGMSxLSdcc";

                        as_toencrypt = filter.newpassword;

                        li_encrypt_len = as_toencrypt.Length;

                        ls_key = as_key1 + filter.userid;
                        li_key_len = ls_key.Length;


                        ls_key_bin = "";

                        iix = 0;

                        for (var i = 0; i < as_toencrypt.Length; ++i)
                        {
                            var res = as_key1.Substring(+iix);
                            iix++;
                            var restcode = res.Substring(0, 1);
                            var keystr = ((int)as_toencrypt[i]).ToString();
                            var keyInt = Convert.ToInt32(keystr) * 2;
                            char keystrChar = (char)(keyInt);

                            ls_key_bin += keystrChar;
                            ls_key_bin += restcode;
                            //bytes.push(ls_key_bin);
                        }
                        userFound.PASSWORD = ls_key_bin;
                        userFound.PROMPT_PASSWORD = "N";
                        db.Update(userFound, new string[] { "PASSWORD", "PROMPT_PASSWORD" });

                        transScope.Complete();

                        ret = new Dictionary<string, object>() { { "PASSWORDUPDATED", "PASSWORDUPDATED" } };
                    }


                }
                else
                {
                    throw new Exception("Wrong Password not found.") { Source = this.GetType().Name };
                }
                
            }
            else
            {
                throw new Exception("User not found.") { Source = this.GetType().Name };
            }
            return ret;
        }


        public Dictionary<string, object> insertSessionStaffApi(STAFF_LOGON_SESSIONS sessioncurr, STAFF user)
        {
            Dictionary<string, object> ret = null;
            var db = new PetaPoco.Database("ConStrProd");

            var userFound = db.SingleOrDefault<STAFF>(PetaPoco.Sql.Builder.Select("*").From("STAFF").Where("USER_ID=@userId", new { userId = user.USER_ID.ToLower() }));

            if (userFound != null)
            {


                db.Delete("STAFF_LOGON_SESSIONS", "STAFF_ID", sessioncurr); //<Mortada>
                db.Insert(sessioncurr);

                userFound.USER_ID = ";-D";
                userFound.PASSWORD = ";-D";

                ret = new Dictionary<string, object>() { { "SESSIONOK", "" } };

            }
            else {
                throw new Exception("User not found.") { Source = this.GetType().Name };
            }
            return ret;
        }

        /// <summary>
        /// Check the session validity. This method will use the same db instance and throw an error if session is expired.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="db"></param>
        public void CheckSessionValidity(STAFF_LOGON_SESSIONS session)
        {
            bool isSessionExpired = false;
            var currentSession = db.SingleOrDefault<STAFF_LOGON_SESSIONS>(PetaPoco.Sql.Builder.Select("*").From("STAFF_LOGON_SESSIONS").Where("SESSION_ID=@sessionId", new { sessionId = session.SESSION_ID }));

            if (currentSession != null)
            {

                int timeOut = EmrWebApi.Properties.Settings.Default.staffSessionTimeout;

                if (DateTime.Now.Subtract(currentSession.LOGON_DATE.GetValueOrDefault()).TotalMinutes <= timeOut)
                {

                    currentSession.LOGON_DATE = DateTime.Now.AddHours(7.0);
                    db.Update("STAFF_LOGON_SESSIONS", "SESSION_ID", currentSession);

                }
                else {
                    isSessionExpired = false;
                }
            }
            else {
                isSessionExpired = false;
            }

            if (isSessionExpired)
            {

                db.Dispose();
                throw new Exception("Session Expired") { Source = this.GetType().Name };
            }

        }

        /// <summary>
        /// Invalidates a logged on staff/user's session.
        /// </summary>
        /// <param name="session"></param>
        /// <returns>Returns 1 if the session was successfully invalidated, returns -1 if it failed.</returns>
        public int InvalidateSession(STAFF_LOGON_SESSIONS session)
        {
            return 1;
        }
    }
}