﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using EmrSys;
using System.Net.Http.Headers;
using System.Globalization;
using System.Threading.Tasks;
using EmrWebApi.Models;

namespace EmrWebApi.BusinessObjects
{

    public class NotifySender
    {
        private PetaPoco.Database db;
        //public NotifySender()
        //{
        //    db = new PetaPoco.Database("ConStrProdDR");
        //}
        static HttpClient client = new HttpClient();
        private const string URL = "http://api.yamamah.com";
        //private string urlParameters = "/966505613305/bste1234";
        public NotifySender(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }       

        public void Dispose()
        {
            db.Dispose();
        }

        public int UpdateNotify(string notifyId, string staffId, string strRef1,string strRef2, string strRef3, DateTime? createdDate)
        {
            DateTime now = DateTime.Now;
            var createDt = now.ToString("yyyy/MM/dd HH:mm:ss", CultureInfo.InvariantCulture);
            DateTime createD = DateTime.Parse(createDt);
            //return 0;
            var notifyExist = "N";
            var nofityMgs = db.SingleOrDefault<AM_NOTIFY_MESSAGES>(PetaPoco.Sql.Builder.Select("*").From("AM_NOTIFY_MESSAGES").Where("STAFF_ID=@staffId", new { staffId = staffId.ToLower() }).Where("NOTIFY_ID=@notifyID", new { notifyID = notifyId }).Where("REF_CODE=@refID", new { refID = strRef1 }));
            if (nofityMgs != null)
            {
                notifyExist = "Y";
            }
            AM_NOTIFY_MESSAGES staffNotify = new AM_NOTIFY_MESSAGES();

          // var notifyFound = db.Fetch<NOTIFY_SETUP_V>(PetaPoco.Sql.Builder.Append((string)new IndieSearchNotifyList(db._dbType.ToString(), notifyId, 100)));

            var notifyFound = db.Fetch<NOTIFY_SETUP_V>(PetaPoco.Sql.Builder.Select("*").From("NOTIFY_SETUP_V").Where("STAFF_ID=@staffId", new { staffId = staffId.ToLower() }).Where("NOTIFY_ID=@notifyID", new { notifyID = notifyId }));

            if (notifyFound != null && notifyFound.Count() > 0)
            {

                var staffExist = "N";

                foreach (NOTIFY_SETUP_V o in notifyFound)
                {
                    var userFound = db.SingleOrDefault<STAFF>(PetaPoco.Sql.Builder.Select("*").From("STAFF").Where("STAFF_ID=@userId", new { userId = o.STAFF_ID.ToLower() }));
                    if (userFound != null)
                    {

                        var nofityMgsExist = db.SingleOrDefault<AM_NOTIFY_MESSAGES>(PetaPoco.Sql.Builder.Select("*").From("AM_NOTIFY_MESSAGES").Where("STAFF_ID=@staffId", new { staffId = userFound.STAFF_ID.ToLower() }).Where("NOTIFY_ID=@notifyID", new { notifyID = notifyId }).Where("REF_CODE=@refID", new { refID = strRef1 }));
                        if (nofityMgsExist != null)
                        {
                            continue;
                        }
                        if (staffId == o.STAFF_ID)
                        {
                            staffExist = "Y";
                        }

                        var emailMsg = o.EMAIL_MESSAGE;
                        var dashMsg = o.DASHBOARD_MESSAGE;
                        var smsMsg = o.SMS_MESSAGE;
                        var refNo = "<refNo>";
                        var lastName = "<lastname>";
                        if (o.EMAIL_FLAG == "Y")
                        {

                            int Place = emailMsg.IndexOf(lastName);
                            if (Place > 0)
                            {
                                emailMsg = emailMsg.Remove(Place, lastName.Length).Insert(Place, userFound.LAST_NAME);
                            }


                            Place = emailMsg.IndexOf(refNo);
                            if (Place > 0)
                            {
                                emailMsg = emailMsg.Remove(Place, refNo.Length).Insert(Place, strRef1);
                                try
                                {
                                    emailSender(userFound.EMAIL_ADDRESS, emailMsg, strRef2);
                                }
                                catch (Exception ex)
                                {

                                }
                            }


                        }
                        if (o.DASHBOARD_FLAG == "Y")
                        {

                            int Place = dashMsg.IndexOf(refNo);
                            if (Place > 0)
                            {
                                dashMsg = dashMsg.Remove(Place, refNo.Length).Insert(Place, strRef1);
                            }
                        }
                        if (o.SMS_FLAG == "Y")
                        {
                            int Place = smsMsg.IndexOf(refNo);
                            if (Place > 0)
                            {
                                smsMsg = smsMsg.Remove(Place, refNo.Length).Insert(Place, strRef1);

                                GetCreditResult smsContentx = new GetCreditResult();

                                smsContentx.Message = smsMsg;
                                if (userFound.CONTACT_PRIMARY != null)
                                {
                                    var prim = userFound.CONTACT_PRIMARY.Length;
                                    if (prim > 6)
                                    {
                                        if (userFound.CONTACT_PRIMARY.Substring(0, 5) == "00966")
                                        {

                                        }
                                        else
                                        {
                                            if (userFound.CONTACT_PRIMARY.Substring(0, 1) == "0")
                                            {
                                                int contactlen = userFound.CONTACT_PRIMARY.Length;
                                                userFound.CONTACT_PRIMARY = "00966" + userFound.CONTACT_PRIMARY.Remove(0, 1);
                                            }
                                        }

                                        smsContentx.RecepientNumber = userFound.CONTACT_PRIMARY;
                                        try
                                        {
                                            smsSend(smsContentx);
                                        }
                                        catch (Exception ex)
                                        {

                                        }
                                        // Supervisor Notification
                                        if (notifyId == "1")
                                        {
                                            smsContentx.Message = "New WO No " + strRef1 + " was assigned to " + userFound.LAST_NAME;
                                            //smsContentx.RecepientNumber = "966542014097";
                                            try
                                            {
                                                smsSend(smsContentx);
                                            }
                                            catch (Exception ex)
                                            {

                                            }
                                        }
                                    }
                                }



                            }
                        }
                        staffNotify.EMAIL_MESSAGE = emailMsg;
                        staffNotify.EMAIL_FLAG = o.EMAIL_FLAG;

                        staffNotify.SMS_MESSAGE = smsMsg;
                        staffNotify.SMS_FLAG = o.SMS_FLAG;
                        staffNotify.NOTIFY_MESSAGE = dashMsg;
                        staffNotify.DASHBOARD_FLAG = o.DASHBOARD_FLAG;
                        staffNotify.EMAIL_ADDRESS = userFound.EMAIL_ADDRESS;
                        staffNotify.MOBILENO = userFound.CONTACT_PRIMARY;
                        staffNotify.NOTIFY_ID = Int32.Parse(notifyId);
                        staffNotify.CREATED_BY = "SYSTEM";

                        staffNotify.CREATED_DATE = createD;
                        staffNotify.STAFF_ID = o.STAFF_ID;
                        staffNotify.STATUS = "A";
                        staffNotify.REF_CODE = strRef1;
                        db.Insert(staffNotify);
                    }
                }

                nofityMgs = db.SingleOrDefault<AM_NOTIFY_MESSAGES>(PetaPoco.Sql.Builder.Select("*").From("AM_NOTIFY_MESSAGES").Where("STAFF_ID=@staffId", new { staffId = staffId.ToLower() }).Where("NOTIFY_ID=@notifyID", new { notifyID = notifyId }).Where("REF_CODE=@refID", new { refID = strRef1 }));
                if (nofityMgs != null)
                {
                    notifyExist = "Y";
                }
                if (staffExist != "Y" && strRef3 == "Y" && notifyExist == "N")
                {                   
                    var userExistFound = db.SingleOrDefault<STAFF>(PetaPoco.Sql.Builder.Select("*").From("STAFF").Where("STAFF_ID=@staffID", new { staffID = staffId.ToLower() }));
                    if (userExistFound != null)
                    {
                        var notifySetup = db.SingleOrDefault<AM_NOTIFYSETUP>(PetaPoco.Sql.Builder.Select("*").From("AM_NOTIFYSETUP").Where("NOTIFY_ID=@notifyID", new { notifyID = notifyId }));
                        if (notifySetup != null)
                        {
                            var emailMsg = notifySetup.EMAIL_MESSAGE;
                            var dashMsg = notifySetup.DASHBOARD_MESSAGE;
                            var smsMsg = notifySetup.SMS_MESSAGE;
                            var refNo = "<refNo>";
                            var lastName = "<lastname>";
                            if (notifySetup.EMAIL_FLAG == "Y")
                            {

                                int Place = emailMsg.IndexOf(lastName);
                                if (Place > 0)
                                {
                                    emailMsg = emailMsg.Remove(Place, lastName.Length).Insert(Place, userExistFound.LAST_NAME);
                                }


                                Place = emailMsg.IndexOf(refNo);
                                if (Place > 0)
                                {
                                    emailMsg = emailMsg.Remove(Place, refNo.Length).Insert(Place, strRef1);
                                
                                    try
                                    {
                                        emailSender(userExistFound.EMAIL_ADDRESS, emailMsg, strRef2);
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }

                            }
                            if (notifySetup.DASHBOARD_FLAG == "Y")
                            {

                                int Place = dashMsg.IndexOf(refNo);
                                if (Place > 0)
                                {
                                    dashMsg = dashMsg.Remove(Place, refNo.Length).Insert(Place, strRef1);
                                }
                            }
                            if (notifySetup.SMS_FLAG == "Y")
                            {
                                int Place = smsMsg.IndexOf(refNo);
                                if (Place > 0)
                                {
                                    if (userExistFound.CONTACT_PRIMARY != null)
                                    {
                                        var prim = userExistFound.CONTACT_PRIMARY.Length;
                                        if (prim > 6)
                                        {
                                            smsMsg = smsMsg.Remove(Place, refNo.Length).Insert(Place, strRef1);

                                            GetCreditResult smsContentx = new GetCreditResult();

                                            smsContentx.Message = smsMsg;
                                            if (userExistFound.CONTACT_PRIMARY.Substring(0, 5) == "00966")
                                            {

                                            }
                                            else
                                            {
                                                if (userExistFound.CONTACT_PRIMARY.Substring(0, 1) == "0")
                                                {
                                                    int contactlen = userExistFound.CONTACT_PRIMARY.Length;
                                                    userExistFound.CONTACT_PRIMARY = "00966" + userExistFound.CONTACT_PRIMARY.Remove(0, 1);
                                                }
                                            }
                                            smsContentx.RecepientNumber = userExistFound.CONTACT_PRIMARY;
                                            try
                                            {
                                                smsSend(smsContentx);
                                            }
                                            catch (Exception ex)
                                            {

                                            }
                                            // Supervisor Notification
                                            if (notifyId == "1")
                                            {
                                                smsContentx.Message = "New WO No " + strRef1 + " was assigned to " + userExistFound.LAST_NAME;
                                                //smsContentx.RecepientNumber = "966542014097";
                                                try
                                                {
                                                    smsSend(smsContentx);
                                                }
                                                catch (Exception ex)
                                                {

                                                }
                                            }

                                        }
                                    }


                                }
                            }
                            staffNotify.EMAIL_MESSAGE = emailMsg;
                            staffNotify.EMAIL_FLAG = notifySetup.EMAIL_FLAG;

                            staffNotify.SMS_MESSAGE = smsMsg;
                            staffNotify.SMS_FLAG = notifySetup.SMS_FLAG;
                            staffNotify.NOTIFY_MESSAGE = dashMsg;
                            staffNotify.DASHBOARD_FLAG = notifySetup.DASHBOARD_FLAG;
                            staffNotify.EMAIL_ADDRESS = userExistFound.EMAIL_ADDRESS;
                            staffNotify.MOBILENO = userExistFound.CONTACT_PRIMARY;
                            staffNotify.NOTIFY_ID = Int32.Parse(notifyId);
                            staffNotify.CREATED_BY = "SYSTEM";

                            staffNotify.CREATED_DATE = createD;
                            staffNotify.STAFF_ID = staffId;
                            staffNotify.STATUS = "A";
                            staffNotify.REF_CODE = strRef1;
                            db.Insert(staffNotify);
                        }
                    }
                }
                else
                {                     
                    if (strRef3 == "Y" && notifyExist == "N")
                    {
                        var userFound = db.SingleOrDefault<STAFF>(PetaPoco.Sql.Builder.Select("*").From("STAFF").Where("STAFF_ID=@userId", new { userId = staffId.ToLower() }));
                        if (userFound != null)
                        {
                            var notifySetup = db.SingleOrDefault<AM_NOTIFYSETUP>(PetaPoco.Sql.Builder.Select("*").From("AM_NOTIFYSETUP").Where("NOTIFY_ID=@notifyID", new { notifyID = notifyId }));
                            if (notifySetup != null)
                            {
                                var emailMsg = notifySetup.EMAIL_MESSAGE;
                                var dashMsg = notifySetup.DASHBOARD_MESSAGE;
                                var smsMsg = notifySetup.SMS_MESSAGE;
                                var refNo = "<refNo>";
                                var lastName = "<lastname>";
                                if (notifySetup.EMAIL_FLAG == "Y")
                                {

                                    int Place = emailMsg.IndexOf(lastName);
                                    if (Place > 0)
                                    {
                                        emailMsg = emailMsg.Remove(Place, lastName.Length).Insert(Place, userFound.LAST_NAME);
                                    }


                                    Place = emailMsg.IndexOf(refNo);
                                    if (Place > 0)
                                    {
                                        emailMsg = emailMsg.Remove(Place, refNo.Length).Insert(Place, strRef1);
                                    }
                                    try
                                    {
                                        emailSender(userFound.EMAIL_ADDRESS, emailMsg, strRef2);
                                    }
                                    catch (Exception ex)
                                    {

                                    }

                                }
                                if (notifySetup.DASHBOARD_FLAG == "Y")
                                {

                                    int Place = dashMsg.IndexOf(refNo);
                                    if (Place > 0)
                                    {
                                        dashMsg = dashMsg.Remove(Place, refNo.Length).Insert(Place, strRef1);
                                    }
                                }
                                if (notifySetup.SMS_FLAG == "Y")
                                {
                                    int Place = smsMsg.IndexOf(refNo);
                                    if (Place > 0)
                                    {
                                        if (userFound.CONTACT_PRIMARY != null)
                                        {
                                            var prim = userFound.CONTACT_PRIMARY.Length;
                                            if (prim > 6)
                                            {
                                                smsMsg = smsMsg.Remove(Place, refNo.Length).Insert(Place, strRef1);

                                                GetCreditResult smsContentx = new GetCreditResult();

                                                smsContentx.Message = smsMsg;
                                                if (userFound.CONTACT_PRIMARY.Substring(0, 5) == "00966")
                                                {

                                                }
                                                else
                                                {
                                                    if (userFound.CONTACT_PRIMARY.Substring(0, 1) == "0")
                                                    {
                                                        int contactlen = userFound.CONTACT_PRIMARY.Length;
                                                        userFound.CONTACT_PRIMARY = "00966" + userFound.CONTACT_PRIMARY.Remove(0, 1);
                                                    }
                                                }
                                                smsContentx.RecepientNumber = userFound.CONTACT_PRIMARY;
                                                try
                                                {
                                                    smsSend(smsContentx);
                                                }
                                                catch (Exception ex)
                                                {

                                                }
                                                // Supervisor Notification
                                                if (notifyId == "1")
                                                {
                                                    smsContentx.Message = "New WO No " + strRef1 + " was assigned to " + userFound.LAST_NAME;
                                                    //smsContentx.RecepientNumber = "966542014097";
                                                    try
                                                    {
                                                        smsSend(smsContentx);
                                                    }
                                                    catch (Exception ex)
                                                    {

                                                    }
                                                }

                                            }
                                        }


                                    }
                                }
                                staffNotify.EMAIL_MESSAGE = emailMsg;
                                staffNotify.EMAIL_FLAG = notifySetup.EMAIL_FLAG;

                                staffNotify.SMS_MESSAGE = smsMsg;
                                staffNotify.SMS_FLAG = notifySetup.SMS_FLAG;
                                staffNotify.NOTIFY_MESSAGE = dashMsg;
                                staffNotify.DASHBOARD_FLAG = notifySetup.DASHBOARD_FLAG;
                                staffNotify.EMAIL_ADDRESS = userFound.EMAIL_ADDRESS;
                                staffNotify.MOBILENO = userFound.CONTACT_PRIMARY;
                                staffNotify.NOTIFY_ID = Int32.Parse(notifyId);
                                staffNotify.CREATED_BY = "SYSTEM";

                                staffNotify.CREATED_DATE = createD;
                                staffNotify.STAFF_ID = staffId;
                                staffNotify.STATUS = "A";
                                staffNotify.REF_CODE = strRef1;
                                db.Insert(staffNotify);
                            }

                        }

                    }

                }             
            }
            else
            {
                //for all in the setup
                notifyFound = db.Fetch<NOTIFY_SETUP_V>(PetaPoco.Sql.Builder.Select("*").From("NOTIFY_SETUP_V").Where("NOTIFY_ID=@notifyID", new { notifyID = notifyId }));

                if (notifyFound != null)
                {

                    foreach (NOTIFY_SETUP_V o in notifyFound)
                    {
                        var userFound = db.SingleOrDefault<STAFF>(PetaPoco.Sql.Builder.Select("*").From("STAFF").Where("STAFF_ID=@userId", new { userId = o.STAFF_ID.ToLower() }));
                        if (userFound != null)
                        {

                            var nofityMgsExist = db.SingleOrDefault<AM_NOTIFY_MESSAGES>(PetaPoco.Sql.Builder.Select("*").From("AM_NOTIFY_MESSAGES").Where("STAFF_ID=@staffId", new { staffId = userFound.STAFF_ID.ToLower() }).Where("NOTIFY_ID=@notifyID", new { notifyID = notifyId }).Where("REF_CODE=@refID", new { refID = strRef1 }));
                            if (nofityMgsExist != null)
                            {
                                continue;
                            }

                            var emailMsg = o.EMAIL_MESSAGE;
                            var dashMsg = o.DASHBOARD_MESSAGE;
                            var smsMsg = o.SMS_MESSAGE;
                            var refNo = "<refNo>";
                            var lastName = "<lastname>";
                            if (o.EMAIL_FLAG == "Y")
                            {

                                int Place = emailMsg.IndexOf(lastName);
                                if (Place > 0)
                                {
                                    emailMsg = emailMsg.Remove(Place, lastName.Length).Insert(Place, userFound.LAST_NAME);
                                }


                                Place = emailMsg.IndexOf(refNo);
                                if (Place > 0)
                                {
                                    emailMsg = emailMsg.Remove(Place, refNo.Length).Insert(Place, strRef1);
                                    try
                                    {
                                        emailSender(userFound.EMAIL_ADDRESS, emailMsg, strRef2);
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }


                            }
                            if (o.DASHBOARD_FLAG == "Y")
                            {

                                int Place = dashMsg.IndexOf(refNo);
                                if (Place > 0)
                                {
                                    dashMsg = dashMsg.Remove(Place, refNo.Length).Insert(Place, strRef1);
                                }
                            }
                            if (o.SMS_FLAG == "Y")
                            {
                                int Place = smsMsg.IndexOf(refNo);
                                if (Place > 0)
                                {
                                    smsMsg = smsMsg.Remove(Place, refNo.Length).Insert(Place, strRef1);

                                    GetCreditResult smsContentx = new GetCreditResult();

                                    smsContentx.Message = smsMsg;
                                    if (userFound.CONTACT_PRIMARY != null)
                                    {
                                        var prim = userFound.CONTACT_PRIMARY.Length;
                                        if (prim > 6)
                                        {
                                            if (userFound.CONTACT_PRIMARY.Substring(0, 5) == "00966")
                                            {

                                            }
                                            else
                                            {
                                                if (userFound.CONTACT_PRIMARY.Substring(0, 1) == "0")
                                                {
                                                    int contactlen = userFound.CONTACT_PRIMARY.Length;
                                                    userFound.CONTACT_PRIMARY = "00966" + userFound.CONTACT_PRIMARY.Remove(0, 1);
                                                }
                                            }

                                            smsContentx.RecepientNumber = userFound.CONTACT_PRIMARY;
                                            try
                                            {
                                                smsSend(smsContentx);
                                            }
                                            catch (Exception ex)
                                            {

                                            }
                                            // Supervisor Notification
                                            if (notifyId == "1")
                                            {
                                                smsContentx.Message = "New WO No " + strRef1 + " was assigned to " + userFound.LAST_NAME;
                                                //smsContentx.RecepientNumber = "966542014097";
                                                try
                                                {
                                                    smsSend(smsContentx);
                                                }
                                                catch (Exception ex)
                                                {

                                                }
                                            }
                                        }
                                    }



                                }
                            }
                            staffNotify.EMAIL_MESSAGE = emailMsg;
                            staffNotify.EMAIL_FLAG = o.EMAIL_FLAG;

                            staffNotify.SMS_MESSAGE = smsMsg;
                            staffNotify.SMS_FLAG = o.SMS_FLAG;
                            staffNotify.NOTIFY_MESSAGE = dashMsg;
                            staffNotify.DASHBOARD_FLAG = o.DASHBOARD_FLAG;
                            staffNotify.EMAIL_ADDRESS = userFound.EMAIL_ADDRESS;
                            staffNotify.MOBILENO = userFound.CONTACT_PRIMARY;
                            staffNotify.NOTIFY_ID = Int32.Parse(notifyId);
                            staffNotify.CREATED_BY = "SYSTEM";

                            staffNotify.CREATED_DATE = createD;
                            staffNotify.STAFF_ID = o.STAFF_ID;
                            staffNotify.STATUS = "A";
                            staffNotify.REF_CODE = strRef1;
                            db.Insert(staffNotify);
                        }
                    }
                }

            }
            return 0;
        }

        public Dictionary<string, object> emailSender(string emailAddress, string mailBody, string strsubject)
        {
            Dictionary<string, object> ret = null;

           
                var userEmail = emailAddress;


                if (userEmail != null && userEmail != "")
                {
                    mailBody += "\r\n";
                    mailBody += "\r\n";
                    mailBody += "\r\n";
                    mailBody += "\r\n";

                    mailBody += " Please do not reply to this message.Replies to this message are routed to an unmonitored mailbox.If you have questions please go to http://www.bsaims.isoftwaresolutions.net/ You may also call us @  | ";
                    mailBody += "\r\n";
                    mailBody += "\r\n";
                
                    MailMessage mm = new MailMessage("bst.hims.ph@gmail.com", userEmail, "password assistance", mailBody);
                    mm.BodyEncoding = UTF8Encoding.UTF8;
                    mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                    SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                    client.UseDefaultCredentials = false;

                    client.Credentials = new System.Net.NetworkCredential("bst.hims.ph@gmail.com", "bS123456");
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.EnableSsl = true;
                    client.Timeout = 10000;

                    client.Send(mm);

                    ret = new Dictionary<string, object>() { { "EMAILOK", "SUCCESS" } };

                }
                else
                {
                    ret = new Dictionary<string, object>() { { "INVALIDEMAIL", "INVALID" } };
                }

           
            return ret;
        }
        public Dictionary<string, object> smsgetBalSender(string action)
        {
           
             var db = new PetaPoco.Database("ConStrProd");
            Dictionary<string, object> ret = null;

                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URL);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync("/GetCredit/966505613305/bste1234").Result;
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                var getcreditresult = response.Content.ReadAsStringAsync().Result;

            }
                return ret;

            }
        public Dictionary<string, object> smsSend(GetCreditResult smsContent)
        {

            var db = new PetaPoco.Database("ConStrProd");
            Dictionary<string, object> ret = null;

            GetCreditResult smsContentx = smsContent;
            
            smsContentx.Username = "966505613305";
            smsContentx.Password = "bste1234";
            smsContentx.Tagname = "BSAIMS";
            //smsContent.RecepientNumber = "00639088928171";
            smsContent.VariableList = "";
            smsContent.ReplacementList = "";
            //smsContent.Message = smsContent.Message;
            smsContent.SendDateTime = "0";
            smsContent.EnableDR = false;


            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.PostAsJsonAsync("/SendSMS", smsContentx).Result;
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                var getcreditresult = response.Content.ReadAsStringAsync().Result;

            }
            return ret;

        }
    }

    public class IndieSearchNotifyList : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "NT.NOTIFY_ID,NT.STAFF_ID,NT.SMS_MESSAGE,NT.EMAIL_MESSAGE,NT.SMS_FLAG, NT.EMAIL_FLAG,NT.DASHBOARD_FLAG,NT.DASHBOARD_MESSAGE, NT.STAFF_ID";
        private string columnsSimple = "STAFF_ID,USER_ID,FIRST_NAME,LAST_NAME,PASSWORD_EXPIRY,STATUS,GROUP_DESCRIPTION,CONTACT_PRIMARY,EMAIL_ADDRESS,POSITION_DESCRIPTION";
        private string from = "NOTIFY_SETUP_V NT";

        public IndieSearchNotifyList(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(NT.NOTIFY_ID) = ('" + query2 + "')";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  NOTIFY_ID ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }

}