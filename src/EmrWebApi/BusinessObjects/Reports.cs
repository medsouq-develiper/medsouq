﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using EmrWebApi.Models;


namespace EmrWebApi.BusinessObjects
{
	public class Reports
	{
		private PetaPoco.Database db;

		public Reports(PetaPoco.Database sharedDBInstance)
		{
			db = sharedDBInstance;
		}

        public int notifyCount(SearchFilter filter)
        {
            //SEARCH_COUNT inserviceAssets[] = null;
            var totalCount = 0;
            using (var transScope = db.GetTransaction())
            {
                if (!string.IsNullOrWhiteSpace(filter.notifyStaffId))
                {
                    var notifyTNo = db.SingleOrDefault<SEARCH_COUNT>(PetaPoco.Sql.Builder.Append("SELECT COUNT(MESSAGE_ID) AS COUNTROW FROM AM_NOTIFY_MESSAGES WHERE STATUS ='A' AND STAFF_ID ='" + filter.notifyStaffId + "'"));
                    totalCount = notifyTNo.COUNTROW;


                }
              

                transScope.Complete();

            }

            return totalCount;
        }

        public List<dynamic> SearchByCostCenterSummary(string queryString, SearchFilter filter, int maxRows = 100000)
		{
			var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchCostCenterSum(db._dbType.ToString(), queryString, filter, maxRows)));

			if (staffuserFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return staffuserFound;
		}

		public List<dynamic> SearchByResponsibleCenterSummary(string queryString, SearchFilter filter, int maxRows = 100000)
		{
			var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchResponsibleCenterSum(db._dbType.ToString(), queryString, filter, maxRows)));

			if (staffuserFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return staffuserFound;
		}

		public List<dynamic> SearchByDeviceTypeSummary(string queryString, SearchFilter filter, int maxRows = 100000)
		{
			var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchDeviceTypeSum(db._dbType.ToString(), queryString, filter, maxRows)));

			if (staffuserFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return staffuserFound;
		}

		public List<dynamic> SearchByManufacturerSummary(string queryString, SearchFilter filter, int maxRows = 100000)
		{
			var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchManufacturerSum(db._dbType.ToString(), queryString, filter, maxRows)));

			if (staffuserFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return staffuserFound;
		}
		public List<dynamic> SearchBySuplierSummary(string queryString, SearchFilter filter, int maxRows = 100000)
		{
			var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchSupplierSum(db._dbType.ToString(), queryString, filter, maxRows)));

			if (staffuserFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return staffuserFound;
		}

        public List<dynamic> SearchBySOHAdjustRept(string queryString, SearchFilter filter, int maxRows = 100000)
        {
            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchSOHAdjustmentRept(db._dbType.ToString(), queryString, filter, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }

        //Word order Report Summary
        public List<dynamic> SearchPMType(string queryString, SearchFilter filter, int maxRows = 100000)
        {
            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPMType(db._dbType.ToString(), queryString, filter, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }
        public List<dynamic> SearchBySupplier(string queryString, SearchFilter filter, int maxRows = 10000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchBySupplier(db._dbType.ToString(), queryString, filter, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchByAsset(string queryString, SearchFilter filter, int maxRows = 10000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchByAsset(db._dbType.ToString(), queryString, filter, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchByCostCenter(string queryString, SearchFilter filter, int maxRows = 10000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchByCostCenter(db._dbType.ToString(), queryString, filter, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchByDeviceType(string queryString, SearchFilter filter, int maxRows = 10000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchByDeviceType(db._dbType.ToString(), queryString, filter, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchByProblemType(string queryString,  SearchFilter filter, int maxRows = 10000)
		{
			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchByProblemType(db._dbType.ToString(), queryString,  filter,  maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}
		public List<dynamic> SearchByJobType(string queryString, SearchFilter filter, int maxRows = 10000)
		{
			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchByJobType(db._dbType.ToString(), queryString, filter, maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}
        //Work Order Engr by Status
        public List<dynamic> SearchByEngrStatusRept(string queryString, SearchFilter filter, int maxRows = 100000)
        {
            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchEngrStatusRept(db._dbType.ToString(), queryString, filter, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }
        public List<dynamic> SearchByAssignToStatusRept(string queryString, SearchFilter filter, int maxRows = 100000)
        {
            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchEngrAssignStatusRept(db._dbType.ToString(), queryString, filter, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }
        public DASHBOARD_CLIENT_V SearchDashBoardClient(SearchFilter filter, int maxRows = 100000)
        {

            DASHBOARD_CLIENT_V dashboardClient = new DASHBOARD_CLIENT_V();

            dashboardClient = db.SingleOrDefault<DASHBOARD_CLIENT_V>(PetaPoco.Sql.Builder.Select("*").From("DASHBOARD_CLIENT_V").Where("CLIENT_CODE=@codeId", new { codeId = filter.clientCode }));


            return dashboardClient;
        }
        public List<dynamic> IndieSearchAssignTypeStatusRept(string queryString, SearchFilter filter, int maxRows = 100000)
        {



            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssignTypeStatusRept(db._dbType.ToString(), queryString, filter, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }
        public List<dynamic> SearchWOTimeRept(string queryString, SearchFilter filter, int maxRows = 100000)
        {
            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchWOTimeRept(db._dbType.ToString(), queryString, filter, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }
        public List<dynamic> WOPartsRequest(string queryString, SearchFilter filter, int maxRows = 10000)
        {

            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieWOPartsRequest(db._dbType.ToString(), queryString, filter, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> WOPartsRequestZero(string queryString, SearchFilter filter, int maxRows = 10000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieWOPartsRequestZero(db._dbType.ToString(), queryString, filter, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchWOPartsRequest(string queryString, SearchFilter filter, int maxRows = 10000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchWOPartsRequest(db._dbType.ToString(), queryString, filter, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchWOPartsRequestRorFR(string queryString, SearchFilter filter, int maxRows = 10000)
        {

            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchWOPartsRequestRorFR(db._dbType.ToString(), queryString, filter, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        
        //Search Transaction
        public List<dynamic> SearchSystemTransaction(string queryString, SearchFilter filter, int maxRows = 100000)
        {
            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchSystemTransaction(db._dbType.ToString(), queryString, filter, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }


        public List<dynamic> SearchSystemTransactionRefCode(string queryString, SearchFilter filter, int maxRows = 100000)
        {
            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchSystemTransactionRefCode(db._dbType.ToString(), queryString, filter, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }


        public List<dynamic> SearchSystemTransactionDashBoard(string queryString, SearchFilter filter, int maxRows = 50)
        {
            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchSystemTransactionDashBoard(db._dbType.ToString(), queryString, filter, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }




        //PM Schedule
        public List<dynamic> SearchByPMSheduleRept(string queryString, SearchFilter filter, int maxRows = 100000)
        {
            if (filter.frequency != null && filter.frequency !="")
            {
                var countdays = 0;
                Int32.TryParse(filter.frequency, out countdays);
                DateTime modifyDt = DateTime.Today;//DateTime.Parse(filter.FromDate);

                if (filter.interval == "D")
                {
                     
                    modifyDt = modifyDt.AddDays(countdays);
                }
                else if (filter.interval == "W")
                {
                    countdays = countdays * 7;
                    modifyDt = modifyDt.AddDays(countdays);

                }
                else if (filter.interval == "M")
                {
                    countdays = countdays * 30;
                    modifyDt = modifyDt.AddDays(countdays);

                }
                else if (filter.interval == "Y")
                {
                    countdays = countdays * 365;
                    modifyDt = modifyDt.AddDays(countdays);

                }
                filter.FromDate = modifyDt.ToString("yyyy/MM/dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                //filter.FromDate =  modifyDt.ToString();
            }




            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPMScheduleRept(db._dbType.ToString(), queryString, filter, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }
        
        //Staff Notification
        public List<dynamic> SearchStaffNotification(string queryString, SearchFilter filter, int maxRows = 100000)
        {
   
            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchNotifyRept(db._dbType.ToString(), queryString, filter, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }

        //search work order in 
        public List<dynamic> SearchWoList(string queryString, SearchFilter filter, int maxRows = 100000)
        {




            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchWOList(db._dbType.ToString(), queryString, filter, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }
        public List<dynamic> SearchWoPartsReqList(string queryString, SearchFilter filter, int maxRows = 100000)
        {




            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieReadWOIdMaterialsList(db._dbType.ToString(), queryString, filter, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }
        public List<dynamic> readWOIdListMaterials(string queryString, SearchFilter filter, int maxRows = 2000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieReadWOIdMaterialsList(db._dbType.ToString(), queryString, filter, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> readWOIdListLabours(string queryString, SearchFilter filter, int maxRows = 2000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchWOLabours(db._dbType.ToString(), queryString, filter, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        //search work order responsible staff
        public List<dynamic> SearchWOByResponsibleStaff(string queryString, int maxRows = 100000)
        {
            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchResponsibleStaff(db._dbType.ToString(), queryString, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }

        //Warranty list
        public List<ASSET_WARRANTY_V> ReadWarrantyListByAssetNoV(string queryString, SearchFilter filter, int maxRows = 100000)
        {
            List<ASSET_WARRANTY_V> warrantyList = new List<ASSET_WARRANTY_V>();

            var woList = db.Fetch<WORKORDER_DET_V>(PetaPoco.Sql.Builder.Append((string)new IndieSearchWOListAssetNO(db._dbType.ToString(), queryString, filter, maxRows)));

            foreach (WORKORDER_DET_V i in woList)
            {

                
                var woNotesFound = db.Fetch<ASSET_WARRANTY_V>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetWarrantyVx(db._dbType.ToString(), i.ASSET_NO, maxRows)));

                if (woNotesFound != null)
                {
                    foreach (ASSET_WARRANTY_V o in woNotesFound)
                    {
                        warrantyList.Add(o);
                    }
                }
            }

           
            return warrantyList;
        }

        public List<dynamic> SearchAssetDPRept(string queryString, SearchFilter filter, int maxRows = 100000)
        {
            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetDepreciation(db._dbType.ToString(), queryString, filter, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }

        public List<dynamic> SearchAssetPartsDPYR(string queryString, int maxRows = 100000)
        {
            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetPartsYR(db._dbType.ToString(), queryString, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }

        //ASSET DEPRECIATION
        public List<dynamic> SearchAssetDPREPT(string queryString, SearchFilter filter, int maxRows = 200000)
        {
            var assetList = new List<dynamic>();

            var assetFound = db.SingleOrDefault<AM_ASSET_DETAILS>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_DETAILS").Where("ASSET_NO=@assetId", new { assetId = queryString }));

            if (assetFound != null)
            {
                filter.Status = null;
                assetList = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetDPREPT(db._dbType.ToString(), queryString, filter, maxRows)));

                if (assetList == null)
                {
                    throw new Exception("No match found.") { Source = this.GetType().Name };
                }
            }
            else
            {
                assetList = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetDPREPT(db._dbType.ToString(), queryString, filter, maxRows)));

                if (assetList == null)
                {
                    throw new Exception("No match found.") { Source = this.GetType().Name };
                }

            }


            return assetList;
        }

        public List<dynamic> SearchTransReports(string queryString, SearchFilter filter, int maxRows = 100000)
        {
            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchTransReports(db._dbType.ToString(), queryString, filter, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }

        public List<dynamic> GetSysTransData(string REFTYPE, string REFCODE, string REFTRANSID)
        {
            if (REFTYPE == "Asset")
                return db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("*").From("ASSET_V").Where("ASSET_NO=@ASSET_NO", new { ASSET_NO = REFCODE }));
            else
                return db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("*").From("ITEM_BATCHES_V").Where("BATCH_ID=@BATCH_ID", new { BATCH_ID = REFTRANSID }));
        }

        public SYSTEMTRANSACTION_REPORT AcknowledgeSysReport(SYSTEMTRANSACTION_REPORT systemTransactionReport, List<SYSTEM_TRANSACTIONDETAILS> transItems1, List<ITEM_BATCHES_V> transItems2, STAFF_LOGON_SESSIONS userSession)
        {
            using (var transScope = db.GetTransaction())
            {
                var arabDateTimeNow = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                systemTransactionReport.ACKNOWLEDGEFLAG = true;
                db.Update(systemTransactionReport, new string[] { "ACKNOWLEDGEFLAG", "ACKNOWLEDGEFLAG1", "ACKNOWLEDGEFLAG2", "ACKNOWLEDGEFLAG3" });

                foreach (var item in transItems1)
                {
                    var updateSystemTransactionDetails = db.FirstOrDefault<SYSTEM_TRANSACTIONDETAILS>(PetaPoco.Sql.Builder.Select("*").From("SYSTEM_TRANSACTIONDETAILS").Where("SYSREPORTID=@SYSREPORTID and SYSTRANSID=@SYSTRANSID", new { SYSREPORTID = item.SYSREPORTID, SYSTRANSID = item.SYSTRANSID }));
                    updateSystemTransactionDetails.ACKNOWLEDGEBY = userSession.USER_ID;
                    updateSystemTransactionDetails.ACKNOWLEDGEDATE = arabDateTimeNow;
                    db.Update(updateSystemTransactionDetails);
                }
                foreach (var item in transItems2)
                {
                    var updateSystemTransactionDetails = db.FirstOrDefault<SYSTEM_TRANSACTIONDETAILS>(PetaPoco.Sql.Builder.Select("*").From("SYSTEM_TRANSACTIONDETAILS").Where("SYSREPORTID=@SYSREPORTID and REFTRANSID=@REFTRANSID", new { SYSREPORTID = item.SYSREPORTID, REFTRANSID = item.BATCH_ID }));
                    updateSystemTransactionDetails.ACKNOWLEDGEBY = userSession.USER_ID;
                    updateSystemTransactionDetails.ACKNOWLEDGEDATE = arabDateTimeNow;
                    db.Update(updateSystemTransactionDetails);
                }
                transScope.Complete();
            }
            return systemTransactionReport;
        }
    }



    public class IndieSearchTransReports : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "";
        private string columnsSimple = "SYSTRANSID, REFCODE, REFDESCRIPTION, MESSAGE, COSTCENTERMESSAGE";
        private string columnsSql = "ID, SEARCHTYPE, SEARCHKEY, ACTION, RECALLCATEGORY, RECALLTYPE, REPORTSTATUS, RECALLTIMEFRAME, HHLEVEL, REASON, CREATEDBY, FORMAT(CREATEDDATE, 'dd/MM/yyyy') as CREATEDDATE, REPORTS, ACTIONREPORTS, CLOSEREPORTS, REPORTEDBY, FORMAT(REPORTEDDATE, 'dd/MM/yyyy') as REPORTEDDATE, ACTIONBY, FORMAT(ACTIONDATE, 'dd/MM/yyyy') as ACTIONDATE, CLOSEBY, FORMAT(CLOSEDATE, 'dd/MM/yyyy') as CLOSEDATE";
        private string from = "SystemTransaction_Report";


        public IndieSearchTransReports(string dbType, string queryString, SearchFilter filter, int maxRows = 10000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(ID) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(SEARCHTYPE) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(SEARCHKEY) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(REPORTSTATUS) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(REASON) LIKE ('%" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("CREATEDDATE >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" and CREATEDDATE <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }
        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause };
            }
        }
        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }
    }

    public class IndieSearchSystemTransactionRefCode : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "";
        private string columnsSimple = "REFCODE, REFDESCRIPTION ";
        private string columnsSql = "REFCODE, REFDESCRIPTION";
        private string from = "SYSTEM_TRANSACTIONDETAILS";


        public IndieSearchSystemTransactionRefCode(string dbType, string queryString, SearchFilter filter, int maxRows = 10000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(REFCODE) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(REFDESCRIPTION) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(REFTYPE) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(REASONTYPE) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(REFBATCHSERIALNO) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(REFMODEL) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(REFUOM) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(MESSAGE) LIKE ('%" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.assignType))
            {
                whereClause += " AND LOWER(REFCODE) LIKE ('%" + filter.assignType + "%')";
            }

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT DISTINCT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + "GROUP BY  REFCODE, REFDESCRIPTION " };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndieSearchSystemTransaction : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "";
        private string columnsSimple = "SYSTRANSID, REFCODE, REFDESCRIPTION, MESSAGE, COSTCENTERMESSAGE";
        private string columnsSql = "ST.SYSTRANSID, ST.REFTRANSID, ST.REFCODE, ST.REFDESCRIPTION, ST.MESSAGE,  ST.REFCOSTCENTERDESCRIPTION, ST.REFCOSTCENTERCODE,ST.REASONTYPE, ST.CREATEDBY,ST.REFCOST,ST.REFQTY,ST.REFBATCHSERIALNO, FORMAT(ST.REFEXPIRY, 'dd/MM/yyyy') as REFEXPIRY,  FORMAT(ST.CREATEDDATE, 'dd/MM/yyyy') as CREATEDDATE, ST.REFTYPE,ST.REFMODEL, ST.REFUOM, ST.REPORTSTATUS, ST.SYSREPORTID, ST.ACKNOWLEDGEBY, ST.ACKNOWLEDGEDATE, S.description AS SUPPLIERNAME, ST.RECALLBY, ST.RECALLDATE";
        private string from = "SYSTEM_TRANSACTIONDETAILS ST LEFT JOIN am_asset_details AD ON ST.RefCode = AD.asset_no LEFT JOIN am_supplier S ON AD.supplier = S.supplier_id";

        public IndieSearchSystemTransaction(string dbType, string queryString, SearchFilter filter, int maxRows = 10000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(ST.REFCODE) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(ST.REFDESCRIPTION) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(ST.REFTYPE) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(ST.REASONTYPE) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(ST.REFBATCHSERIALNO) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(ST.REFMODEL) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(ST.REFUOM) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(ST.MESSAGE) LIKE ('%" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.assignType))
            {
                whereClause += " AND LOWER(ST.REFCODE) LIKE ('%" + filter.assignType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND LOWER(ST.REFCOSTCENTERDESCRIPTION) LIKE ('%" + filter.CostCenterStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.ItemName))
            {
                whereClause += " AND LOWER(REFDESCRIPTION) LIKE ('" + filter.ItemName + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.Category))
            {
                whereClause += " AND LOWER(ST.REFTYPE) = ('" + filter.Category + "')";
            }
            if (!string.IsNullOrWhiteSpace(filter.SerialBatch))
            {
                whereClause += " AND LOWER(ST.REFBATCHSERIALNO) = ('" + filter.SerialBatch + "')";
            }
            if (!string.IsNullOrWhiteSpace(filter.ItemModel))
            {
                whereClause += " AND LOWER(ST.REFMODEL) LIKE ('%" + filter.ItemModel + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.TransType))
            {
                whereClause += " AND LOWER(ST.REASONTYPE) LIKE ('" + filter.TransType + "')";
            }

            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("ST.CREATEDDATE >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" and ST.CREATEDDATE <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }

            if (!string.IsNullOrWhiteSpace(filter.clientCode))
            {
                whereClause += " AND ((ST.RefCostCenterCode IN  (SELECT CostCenter FROM dbo.CLIENT_V AS CC WHERE(CLIENT_CODE ='" + filter.clientCode+ "')) )  OR (ST.RefCostCenterCode IN (SELECT  CLIENT_CODE FROM  dbo.CLIENT_V AS CC WHERE(CLIENT_CODE ='" + filter.clientCode+"'))) )";
            }

            if (!string.IsNullOrWhiteSpace(filter.Reporting))
            {
                whereClause += " AND ST.REASONTYPE = 'RECALL'";
            }
            

            if (!string.IsNullOrWhiteSpace(filter.SysReportId))
            {
                whereClause += " AND ST.SYSREPORTID = '" + filter.SysReportId + "'";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause};
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndieSearchSystemTransactionDashBoard : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "";
        private string columnsSimple = "SYSTRANSID, REFCODE, REFDESCRIPTION, MESSAGE, COSTCENTERMESSAGE";
        private string columnsSql = "SYSTRANSID, REFCODE, REFDESCRIPTION, MESSAGE,  REFCOSTCENTERDESCRIPTION, REFCOSTCENTERCODE,REASONTYPE, CREATEDBY,REFCOST,REFQTY,REFBATCHSERIALNO, FORMAT(REFEXPIRY, 'dd/MM/yyyy') as REFEXPIRY,  FORMAT(CREATEDDATE, 'dd/MM/yyyy') as CREATEDDATE, REFTYPE";
        private string from = "SYSTEM_TRANSACTIONDETAILS";


        public IndieSearchSystemTransactionDashBoard(string dbType, string queryString, SearchFilter filter, int maxRows = 10000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(REFCODE) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(REFDESCRIPTION) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(REFTYPE) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(REASONTYPE) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(REFBATCHSERIALNO) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(REFMODEL) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(REFUOM) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(MESSAGE) LIKE ('%" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.assignType))
            {
                whereClause += " AND LOWER(REFCODE) LIKE ('%" + filter.assignType + "%')";
            }

            if (!string.IsNullOrWhiteSpace(filter.clientCode))
            {
                whereClause += string.Format(" AND REFCOSTCENTERCODE IN (SELECT CostcenterCode from am_client_costcenter where ClientCode = '" + filter.clientCode+ "') OR REFCOSTCENTERCODE = '"+ filter.clientCode + "'");

    }


        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + " ORDER BY SYSTRANSID DESC " };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }




    public class IndieSearchAssetDepreciation : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "A.ASSET_NO, A.DESCRIPTION , A.InService_date, A.PURCHASE_COST,PCOST_YR1,DP_YR1,PCOST_YR2,DP_YR2,PCOST_YR3,DP_YR3,PCOST_YR4,DP_YR4,PCOST_YR5,DP_YR5,PCOST_YR6,DP_YR6,PCOST_YR7,DP_YR7,PCOST_YR8,DP_YR8,PCOST_YR9,DP_YR9,PCOST_YR10,DP_YR10";
        private string columnsSimple = "DESCRIPTION,";
        private string from = "ASSET_DEPRECIATION_V A";

        public IndieSearchAssetDepreciation(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(A.ASSET_NO) LIKE ('%" + query2 + "%') OR LOWER(A.DESCRIPTION) LIKE ('%" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND (LOWER(A.DESCRIPITON) LIKE ('%" + filter.CostCenterStr + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.ManufacturerStr))
            {
                whereClause += " AND (LOWER(A.MANUFACTURER_DESC) LIKE ('%" + filter.ManufacturerStr + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += " AND (LOWER(A.SUPPLIER_DESC) LIKE ('%" + filter.SupplierStr + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.AssetFilterCondition))
            {
                whereClause += " AND (LOWER(A.CONDITION) LIKE ('%" + filter.AssetFilterCondition + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.Status))
            {
                whereClause += " AND (LOWER(A.STATUS) LIKE ('%" + filter.Status + "%'))";
            }

          
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY A.ASSET_NO ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }



    public class IndieSearchAssetWarrantyVx : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.ID, AM.ASSET_NO, AM.FREQUENCY, AM.FREQUENCY_PERIOD, AM.WARRANTY_EXPIRY,AM.WARRANTY_EXTENDED, AM.WARRANTY_INCLUDED, AM.WARRANTY_NOTES, AM.WARRANTYDESC";
        private string columnsSimple = "ID, ASSET_NO, FREQUENCY, FREQUENCY_PERIOD, WARRANTY_EXPIRY, WARRANTY_EXTENDED, WARRANTY_INCLUDED, WARRANTY_NOTES, CREATED_BY, CREATED_DATE";
        private string from = "ASSET_WARRANTY_V AM";

        public IndieSearchAssetWarrantyVx(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR (LOWER(AM.ASSET_NO) = ('" + query2 + "'))";
            whereClause += " AND (AM.ASSET_NO = '" + queryString + "' )";

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ID ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REFID ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }


    public class IndieSearchResponsibleStaff : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "ASSIGN_TO,ASSIGN_NAME,WO_COUNT";
        private string columnsSimple = "ASSIGN_NAME,";
        private string from = "WORKORDER_RESPONSIBLE_V";

        public IndieSearchResponsibleStaff(string dbType, string queryString, int maxRows = 5000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(ASSIGN_NAME) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(ASSIGN_TO) LIKE ('%" + query2 + "%')";
                whereClause += " OR ASSIGN_TO IS NULL ";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY ASSIGN_NAME ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }
    public class IndieSearchCostCenterSum : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "A.COST_CENTER AS CODE, A.COSTCENTER_DESC AS DESCRIPTION, " +
                                 "SUM(CASE WHEN a.STATUS = 'A' THEN 1 ELSE 0 END) AS ACTIVE, SUM(CASE WHEN A.STATUS = 'I' THEN 1 ELSE 0 END) AS INSERVICES, " +
                                 "SUM(CASE WHEN A.STATUS = 'M' THEN 1 ELSE 0 END) AS MISSING, SUM(CASE WHEN A.STATUS = 'N' THEN 1 ELSE 0 END) AS INACTIVE, " +
                                 "SUM(CASE WHEN A.STATUS = 'O' THEN 1 ELSE 0 END) AS OUTSERVICE, SUM(CASE WHEN A.STATUS = 'R' THEN 1 ELSE 0 END) AS RETIRED, " +
                                 "SUM(CASE WHEN A.STATUS = 'T' THEN 1 ELSE 0 END) AS TRANSFERED, SUM(CASE WHEN A.STATUS = 'U' THEN 1 ELSE 0 END) AS UNDIFINED";
        private string columnsSimple = "DESCRIPTION,";
		private string from = "ASSETINFOREPT_V A";

		public IndieSearchCostCenterSum(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			foreach (string query in queryArray)
			{
				var query2 = query.ToLower();
				whereClause += " OR (LOWER(A.COST_CENTER) LIKE ('%" + query2 + "%') OR LOWER(A.COSTCENTER_DESC) LIKE ('%" + query2 + "%'))";
			}

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND (LOWER(A.COSTCENTER_DESC) LIKE ('%" + filter.CostCenterStr + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.ManufacturerStr))
            {
                whereClause += " AND (LOWER(A.MANUFACTURER_DESC) LIKE ('%" + filter.ManufacturerStr + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += " AND (LOWER(A.SUPPLIER_DESC) LIKE ('%" + filter.SupplierStr + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.AssetFilterCondition))
            {
                whereClause += " AND (LOWER(A.CONDITION) LIKE ('%" + filter.AssetFilterCondition + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.Status))
            {
                whereClause += " AND (LOWER(A.STATUS) LIKE ('%" + filter.Status + "%'))";
            }

            //this will not select null values
            whereClause = whereClause.Insert(0, "(") + ") AND (A.COST_CENTER <> '' AND A.COST_CENTER <> 'NULL')";
        }

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " GROUP BY A.COST_CENTER, A.COSTCENTER_DESC ORDER BY A.COSTCENTER_DESC ASC" };
				}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
			}
		}
	}
	public class IndieSearchResponsibleCenterSum : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "A.RESPONSIBLE_CENTER AS CODE, A.RESPCENTER_DESC AS DESCRIPTION, SUM(CASE WHEN a.STATUS = 'A' THEN 1 ELSE 0 END) AS ACTIVE, " +
                                 "SUM(CASE WHEN A.STATUS = 'I' THEN 1 ELSE 0 END) AS INSERVICES, SUM(CASE WHEN A.STATUS = 'M' THEN 1 ELSE 0 END) AS MISSING, " +
                                 "SUM(CASE WHEN A.STATUS = 'N' THEN 1 ELSE 0 END) AS INACTIVE, SUM(CASE WHEN A.STATUS = 'O' THEN 1 ELSE 0 END) AS OUTSERVICE, " +
                                 "SUM(CASE WHEN A.STATUS = 'R' THEN 1 ELSE 0 END) AS RETIRED, SUM(CASE WHEN A.STATUS = 'T' THEN 1 ELSE 0 END) AS TRANSFERED, " +
                                 "SUM(CASE WHEN A.STATUS = 'U' THEN 1 ELSE 0 END) AS UNDIFINED";
        private string columnsSimple = "DESCRIPTION,";
		private string from = "ASSETINFOREPT_V A";

        public IndieSearchResponsibleCenterSum(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			foreach (string query in queryArray)
			{
				var query2 = query.ToLower();
                whereClause += " OR (LOWER(A.RESPONSIBLE_CENTER) LIKE ('%" + query2 + "%') OR LOWER(A.RESPCENTER_DESC) LIKE ('%" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND (LOWER(A.COSTCENTER_DESC) LIKE ('%" + filter.CostCenterStr + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.ManufacturerStr))
            {
                whereClause += " AND (LOWER(A.MANUFACTURER_DESC) LIKE ('%" + filter.ManufacturerStr + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += " AND (LOWER(A.SUPPLIER_DESC) LIKE ('%" + filter.SupplierStr + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.AssetFilterCondition))
            {
                whereClause += " AND (LOWER(A.CONDITION) LIKE ('%" + filter.AssetFilterCondition + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.Status))
            {
                whereClause += " AND (LOWER(A.STATUS) LIKE ('%" + filter.Status + "%'))";
            }

            //this will not select null values
            whereClause = whereClause.Insert(0, "(") + ") AND (A.RESPONSIBLE_CENTER <> '' AND A.RESPONSIBLE_CENTER <> 'NULL')";
        }

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " GROUP BY A.RESPONSIBLE_CENTER,A.RESPCENTER_DESC ORDER BY DESCRIPTION ASC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
			}
		}
	}
	public class IndieSearchDeviceTypeSum : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "A.CATEGORY AS CODE, A.DTYPE_DESC AS DESCRIPTION, SUM(CASE WHEN a.STATUS = 'A' THEN 1 ELSE 0 END) AS ACTIVE, SUM(CASE WHEN A.STATUS = 'I' THEN 1 ELSE 0 END) AS INSERVICES," +
            "SUM(CASE WHEN A.STATUS = 'M' THEN 1 ELSE 0 END) AS MISSING, SUM(CASE WHEN A.STATUS = 'N' THEN 1 ELSE 0 END) AS INACTIVE, SUM(CASE WHEN A.STATUS = 'O' THEN 1 ELSE 0 END) AS OUTSERVICE, " +
            "SUM(CASE WHEN A.STATUS = 'R' THEN 1 ELSE 0 END) AS RETIRED, SUM(CASE WHEN A.STATUS = 'T' THEN 1 ELSE 0 END) AS TRANSFERED, SUM(CASE WHEN A.STATUS = 'U' THEN 1 ELSE 0 END) AS UNDIFINED";
        private string columnsSimple = "DESCRIPTION,";
		private string from = "ASSETINFOREPT_V A";

        public IndieSearchDeviceTypeSum(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			foreach (string query in queryArray)
			{
				var query2 = query.ToLower();
                whereClause += " OR (LOWER(A.CATEGORY) LIKE ('%" + query2 + "%') OR LOWER(A.DTYPE_DESC) LIKE ('%" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND (LOWER(A.COSTCENTER_DESC) LIKE ('%" + filter.CostCenterStr + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.ManufacturerStr))
            {
                whereClause += " AND (LOWER(A.MANUFACTURER_DESC) LIKE ('%" + filter.ManufacturerStr + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += " AND (LOWER(A.SUPPLIER_DESC) LIKE ('%" + filter.SupplierStr + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.AssetFilterCondition))
            {
                whereClause += " AND (LOWER(A.CONDITION) LIKE ('%" + filter.AssetFilterCondition + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.Status))
            {
                whereClause += " AND (LOWER(A.STATUS) LIKE ('%" + filter.Status + "%'))";
            }

            //this will not select null values
            whereClause = whereClause.Insert(0, "(") + ") AND (A.CATEGORY <> '' AND A.CATEGORY <> 'NULL')";
        }

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " GROUP BY A.CATEGORY,A.DTYPE_DESC ORDER BY DESCRIPTION ASC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
			}
		}
	}
	public class IndieSearchManufacturerSum : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
        //SUM(CASE WHEN a.STATUS = 'A' THEN 1 ELSE 0 END) AS ACTIVE,
        private string columns = "A.MANUFACTURER AS CODE, A.MANUFACTURER_DESC AS DESCRIPTION,  " +
            "SUM(CASE WHEN A.STATUS = 'I' THEN 1 ELSE 0 END) AS INSERVICES, SUM(CASE WHEN A.STATUS = 'M' THEN 1 ELSE 0 END) AS MISSING, SUM(CASE WHEN A.STATUS = 'N' THEN 1 ELSE 0 END) AS INACTIVE, " +
            "SUM(CASE WHEN A.STATUS = 'O' THEN 1 ELSE 0 END) AS OUTSERVICE, SUM(CASE WHEN A.STATUS = 'R' THEN 1 ELSE 0 END) AS RETIRED, SUM(CASE WHEN A.STATUS = 'T' THEN 1 ELSE 0 END) AS TRANSFERED, " +
            "SUM(CASE WHEN A.STATUS = 'U' THEN 1 ELSE 0 END) AS UNDIFINED";
        private string columnsSimple = "DESCRIPTION,";
        private string from = "ASSETINFOREPT_V A";

        public IndieSearchManufacturerSum(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(A.MANUFACTURER) LIKE ('%" + query2 + "%') OR LOWER(A.MANUFACTURER_DESC) LIKE ('%" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND (LOWER(A.COSTCENTER_DESC) LIKE ('%" + filter.CostCenterStr + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.ManufacturerStr))
            {
                whereClause += " AND (LOWER(A.MANUFACTURER_DESC) LIKE ('%" + filter.ManufacturerStr + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += " AND (LOWER(A.SUPPLIER_DESC) LIKE ('%" + filter.SupplierStr + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.AssetFilterCondition))
            {
                whereClause += " AND (LOWER(A.CONDITION) LIKE ('%" + filter.AssetFilterCondition + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.Status))
            {
                whereClause += " AND (LOWER(A.STATUS) LIKE ('%" + filter.Status + "%'))";
            }

            //this will not select null values
            whereClause = whereClause.Insert(0, "(") + ") AND (A.MANUFACTURER <> '' AND A.MANUFACTURER <> 'NULL')";
        }

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " GROUP BY A.MANUFACTURER, A.MANUFACTURER_DESC ORDER BY DESCRIPTION ASC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
			}
		}
	}
	public class IndieSearchSupplierSum : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
        private string columns = "A.SUPPLIER AS CODE, A.SUPPLIER_DESC AS DESCRIPTION, SUM(CASE WHEN a.STATUS = 'A' THEN 1 ELSE 0 END) AS ACTIVE, SUM(CASE WHEN A.STATUS = 'I' THEN 1 ELSE 0 END) AS INSERVICES, " +
            "SUM(CASE WHEN A.STATUS = 'M' THEN 1 ELSE 0 END) AS MISSING,SUM(CASE WHEN A.STATUS = 'N' THEN 1 ELSE 0 END) AS INACTIVE, SUM(CASE WHEN A.STATUS = 'O' THEN 1 ELSE 0 END) AS OUTSERVICE," +
            " SUM(CASE WHEN A.STATUS = 'R' THEN 1 ELSE 0 END) AS RETIRED,SUM(CASE WHEN A.STATUS = 'T' THEN 1 ELSE 0 END) AS TRANSFERED, SUM(CASE WHEN A.STATUS = 'U' THEN 1 ELSE 0 END) AS UNDIFINED";
        private string columnsSimple = "DESCRIPTION,";
        private string from = "ASSETINFOREPT_V A";

        public IndieSearchSupplierSum(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(A.SUPPLIER) LIKE ('%" + query2 + "%') OR LOWER(A.SUPPLIER_DESC) LIKE ('%" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND (LOWER(A.COSTCENTER_DESC) LIKE ('%" + filter.CostCenterStr + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.ManufacturerStr))
            {
                whereClause += " AND (LOWER(A.MANUFACTURER_DESC) LIKE ('%" + filter.ManufacturerStr + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += " AND (LOWER(A.SUPPLIER_DESC) LIKE ('%" + filter.SupplierStr + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.AssetFilterCondition))
            {
                whereClause += " AND (LOWER(A.CONDITION) LIKE ('%" + filter.AssetFilterCondition + "%'))";
            }
            if (!string.IsNullOrWhiteSpace(filter.Status))
            {
                whereClause += " AND (LOWER(A.STATUS) LIKE ('%" + filter.Status + "%'))";
            }

            //this will not select null values
            whereClause = whereClause.Insert(0, "(") + ") AND (A.SUPPLIER <> '' AND A.SUPPLIER <> 'NULL')";
        }

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " GROUP BY A.SUPPLIER, A.SUPPLIER_DESC ORDER BY DESCRIPTION ASC" };
            }
        }

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
			}
		}
	}
        
    public class IndieSearchPMType : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "";
        private string columnsSimple = "AM.PM_TYPE, SUM(case when AM.WO_STATUS = 'CL' then 1 end) AS CLOSESTATUS, SUM(case when AM.WO_STATUS = 'HA' then 1 end) AS HOLDSTATUS,SUM(CASE when AM.WO_STATUS = 'NE' then 1 else 0 end) AS NEWSTATUS,SUM(CASE when AM.WO_STATUS = 'OP' then 1 else 0 end) AS OPENSTATUS, SUM(CASE when AM.WO_STATUS = 'PS' then 1 else 0 end) AS POSTEDSTATUS";
        private string columnsSql = "AM.PM_TYPE, SUM(case when AM.WO_STATUS = 'CL' then 1 else 0 end) AS CLOSESTATUS,SUM(case when AM.WO_STATUS = 'HA' then 1 else 0 end) AS HOLDSTATUS, SUM(CASE when AM.WO_STATUS = 'NE' then 1 else 0 end) AS NEWSTATUS,SUM(CASE when AM.WO_STATUS = 'OP' then 1 else 0 end) AS OPENSTATUS, SUM(CASE when AM.WO_STATUS = 'PS' then 1 else 0 end) AS POSTEDSTATUS";
        private string from = "WORKORDERREPT_V AM";


        public IndieSearchPMType(string dbType, string queryString, SearchFilter filter, int maxRows = 10000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();

            whereClause += "  OR (LOWER(AM.PM_TYPE) LIKE ('%" + query2 + "%')) ";

            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("AM.CREATED_DATE >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" and AM.CREATED_DATE <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND LOWER(AM.COSTCENTERNAME) LIKE ('%" + filter.CostCenterStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += " AND LOWER(AM.SUPPLIERNAME) LIKE ('%" + filter.SupplierStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.ProblemType))
            {
                whereClause += " AND LOWER(AM.REQ_TYPE) LIKE ('%" + filter.ProblemType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.pmtype))
            {
                whereClause += " AND LOWER(AM.PM_TYPE) LIKE ('%" + filter.pmtype + "%')";
            }
            if (filter.Status == "PSOP")
            {
                whereClause += " AND (AM.WO_STATUS in ('NE','OP') )";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(filter.Status))
                {
                    whereClause += string.Format(" AND AM.WO_STATUS = ('{0}')", filter.Status);
                }

            }
            if (!string.IsNullOrWhiteSpace(filter.Priority))
            {
                whereClause += " AND LOWER(AM.JOB_URGENCY) LIKE ('%" + filter.Priority + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.JobType))
            {
                whereClause += " AND LOWER(AM.JOB_TYPE) LIKE ('%" + filter.JobType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignTo))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TO) LIKE ('%" + filter.assignTo + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignType))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TYPE) LIKE ('%" + filter.assignType + "%')";
            }

            //this will not select null values
            whereClause = whereClause.Insert(0, "(") + ") AND (AM.PM_TYPE <> '' AND AM.PM_TYPE <> 'NULL')";
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + "GROUP BY  AM.PM_TYPE " };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchBySupplier : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columnsSimple = "REQ_NO, REQ_NO, REQ_BY, REQ_DATE, PROBLEM, STATUS, REQ_TYPE, ASSET_NO,JOB_TYPE, JOB_URGENCY";
        private string columns = "AM.SUPPLIER, AM.SUPPLIERNAME AS DESCRIPTION, SUM(case when AM.WO_STATUS = 'CL' then 1 else 0 end) AS CLOSESTATUS,SUM(case when AM.WO_STATUS = 'HA' then 1 else 0 end) AS HOLDSTATUS, SUM(CASE when AM.WO_STATUS = 'NE' then 1 else 0 end) AS NEWSTATUS,SUM(CASE when AM.WO_STATUS = 'OP' then 1 else 0 end) AS OPENSTATUS, SUM(CASE when AM.WO_STATUS = 'PS' then 1 else 0 end) AS POSTEDSTATUS";
        private string from = "WORKORDERREPT_V AM";


        public IndieSearchBySupplier(string dbType, string queryString, SearchFilter filter, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            if (!string.IsNullOrWhiteSpace(query2))
            {
                whereClause += "  AND (LOWER(AM.SUPPLIER) LIKE ('%" + query2 + "%') " +
                " OR LOWER(AM.SUPPLIERNAME) LIKE ('%" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("AM.REQ_DATE >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" AND AM.REQ_DATE <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND LOWER(AM.COSTCENTERNAME) LIKE ('%" + filter.CostCenterStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += " AND LOWER(AM.SUPPLIERNAME) LIKE ('%" + filter.SupplierStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.ProblemType))
            {
                whereClause += " AND LOWER(AM.REQ_TYPE) LIKE ('%" + filter.ProblemType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.pmtype))
            {
                whereClause += " AND LOWER(AM.PM_TYPE) LIKE ('%" + filter.pmtype + "%')";
            }
            if (filter.Status == "PSOP")
            {
                whereClause += " AND (AM.WO_STATUS in ('NE','OP') )";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(filter.Status))
                {
                    whereClause += string.Format(" AND AM.WO_STATUS = ('{0}')", filter.Status);
                }

            }
            if (!string.IsNullOrWhiteSpace(filter.Priority))
            {
                whereClause += " AND LOWER(AM.JOB_URGENCY) LIKE ('%" + filter.Priority + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.JobType))
            {
                whereClause += " AND LOWER(AM.JOB_TYPE) LIKE ('%" + filter.JobType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignTo))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TO) LIKE ('%" + filter.assignTo + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignType))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TYPE) LIKE ('%" + filter.assignType + "%')";
            }

            //this will not select null values
            whereClause = whereClause.Insert(0, "(") + ") AND (AM.SUPPLIER <> '' AND AM.SUPPLIER <> 'NULL')";
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " GROUP BY SUPPLIER, SUPPLIERNAME" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchByAsset : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columnsSimple = "REQ_NO, REQ_NO, REQ_BY, REQ_DATE, PROBLEM, STATUS, REQ_TYPE, ASSET_NO,JOB_TYPE, JOB_URGENCY";
        private string columns = "AM.ASSET_NO, AM.DESCRIPTION AS DESCRIPTION, SUM(case when AM.WO_STATUS = 'CL' then 1 else 0 end) AS CLOSESTATUS,SUM(case when AM.WO_STATUS = 'HA' then 1 else 0 end) AS HOLDSTATUS, SUM(CASE when AM.WO_STATUS = 'NE' then 1 else 0 end) AS NEWSTATUS,SUM(CASE when AM.WO_STATUS = 'OP' then 1 else 0 end) AS OPENSTATUS, SUM(CASE when AM.WO_STATUS = 'PS' then 1 else 0 end) AS POSTEDSTATUS";
        private string from = "WORKORDERREPT_V AM";


        public IndieSearchByAsset(string dbType, string queryString, SearchFilter filter, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            if (!string.IsNullOrWhiteSpace(query2))
            {
                whereClause += "  AND (LOWER(AM.ASSET_NO) LIKE ('%" + query2 + "%') " +
                " OR LOWER(AM.DESCRIPTION) LIKE ('%" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("AM.REQ_DATE >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" AND AM.REQ_DATE <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND LOWER(AM.COSTCENTERNAME) LIKE ('%" + filter.CostCenterStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += " AND LOWER(AM.SUPPLIERNAME) LIKE ('%" + filter.SupplierStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.ProblemType))
            {
                whereClause += " AND LOWER(AM.REQ_TYPE) LIKE ('%" + filter.ProblemType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.pmtype))
            {
                whereClause += " AND LOWER(AM.PM_TYPE) LIKE ('%" + filter.pmtype + "%')";
            }
            if (filter.Status == "PSOP")
            {
                whereClause += " AND (AM.WO_STATUS in ('NE','OP') )";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(filter.Status))
                {
                    whereClause += string.Format(" AND AM.WO_STATUS = ('{0}')", filter.Status);
                }

            }
            if (!string.IsNullOrWhiteSpace(filter.Priority))
            {
                whereClause += " AND LOWER(AM.JOB_URGENCY) LIKE ('%" + filter.Priority + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.JobType))
            {
                whereClause += " AND LOWER(AM.JOB_TYPE) LIKE ('%" + filter.JobType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignTo))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TO) LIKE ('%" + filter.assignTo + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignType))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TYPE) LIKE ('%" + filter.assignType + "%')";
            }

            //this will not select null values
            whereClause = whereClause.Insert(0, "(") + ") AND (AM.ASSET_NO <> '' AND AM.ASSET_NO <> 'NULL')";
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " GROUP BY ASSET_NO, DESCRIPTION" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchByCostCenter : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columnsSimple = "REQ_NO, REQ_NO, REQ_BY, REQ_DATE, PROBLEM, STATUS, REQ_TYPE, ASSET_NO,JOB_TYPE, JOB_URGENCY";
        private string columns = "AM.COST_CENTER, AM.COSTCENTERNAME AS DESCRIPTION, SUM(case when AM.WO_STATUS = 'CL' then 1 else 0 end) AS CLOSESTATUS,SUM(case when AM.WO_STATUS = 'HA' then 1 else 0 end) AS HOLDSTATUS, SUM(CASE when AM.WO_STATUS = 'NE' then 1 else 0 end) AS NEWSTATUS,SUM(CASE when AM.WO_STATUS = 'OP' then 1 else 0 end) AS OPENSTATUS, SUM(CASE when AM.WO_STATUS = 'PS' then 1 else 0 end) AS POSTEDSTATUS";
        private string from = "WORKORDERREPT_V AM";

        public IndieSearchByCostCenter(string dbType, string queryString, SearchFilter filter, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            if (!string.IsNullOrWhiteSpace(query2))
            {
                whereClause += "  AND (LOWER(AM.COST_CENTER) LIKE ('%" + query2 + "%') " +
                " OR LOWER(AM.COSTCENTERNAME) LIKE ('%" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("AM.REQ_DATE >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" AND AM.REQ_DATE <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND LOWER(AM.COSTCENTERNAME) LIKE ('%" + filter.CostCenterStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += " AND LOWER(AM.SUPPLIERNAME) LIKE ('%" + filter.SupplierStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.ProblemType))
            {
                whereClause += " AND LOWER(AM.REQ_TYPE) LIKE ('%" + filter.ProblemType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.pmtype))
            {
                whereClause += " AND LOWER(AM.PM_TYPE) LIKE ('%" + filter.pmtype + "%')";
            }
            if (filter.Status == "PSOP")
            {
                whereClause += " AND (AM.WO_STATUS in ('NE','OP') )";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(filter.Status))
                {
                    whereClause += string.Format(" AND AM.WO_STATUS = ('{0}')", filter.Status);
                }

            }
            if (!string.IsNullOrWhiteSpace(filter.Priority))
            {
                whereClause += " AND LOWER(AM.JOB_URGENCY) LIKE ('%" + filter.Priority + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.JobType))
            {
                whereClause += " AND LOWER(AM.JOB_TYPE) LIKE ('%" + filter.JobType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignTo))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TO) LIKE ('%" + filter.assignTo + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignType))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TYPE) LIKE ('%" + filter.assignType + "%')";
            }

            //this will not select null values
            whereClause = whereClause.Insert(0, "(") + ") AND (AM.COST_CENTER <> '' AND AM.COST_CENTER <> 'NULL')";
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " GROUP BY COST_CENTER, COSTCENTERNAME" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchByDeviceType : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columnsSimple = "REQ_NO, REQ_NO, REQ_BY, REQ_DATE, PROBLEM, STATUS, REQ_TYPE, ASSET_NO,JOB_TYPE, JOB_URGENCY";
        private string columns = "AM.DTYPE, AM.DTYPE_DESC AS DESCRIPTION, SUM(case when AM.WO_STATUS = 'CL' then 1 else 0 end) AS CLOSESTATUS,SUM(case when AM.WO_STATUS = 'HA' then 1 else 0 end) AS HOLDSTATUS, SUM(CASE when AM.WO_STATUS = 'NE' then 1 else 0 end) AS NEWSTATUS,SUM(CASE when AM.WO_STATUS = 'OP' then 1 else 0 end) AS OPENSTATUS, SUM(CASE when AM.WO_STATUS = 'PS' then 1 else 0 end) AS POSTEDSTATUS";
        private string from = "WORKORDERREPT_V AM";

        public IndieSearchByDeviceType(string dbType, string queryString, SearchFilter filter, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            if (!string.IsNullOrWhiteSpace(query2))
            {
                whereClause += "  AND (LOWER(AM.DTYPE) LIKE ('%" + query2 + "%') " +
                " OR LOWER(AM.DTYPE_DESC) LIKE ('%" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("AM.REQ_DATE >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" AND AM.REQ_DATE <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND LOWER(AM.COSTCENTERNAME) LIKE ('%" + filter.CostCenterStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += " AND LOWER(AM.SUPPLIERNAME) LIKE ('%" + filter.SupplierStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.ProblemType))
            {
                whereClause += " AND LOWER(AM.REQ_TYPE) LIKE ('%" + filter.ProblemType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.pmtype))
            {
                whereClause += " AND LOWER(AM.PM_TYPE) LIKE ('%" + filter.pmtype + "%')";
            }
            if (filter.Status == "PSOP")
            {
                whereClause += " AND (AM.WO_STATUS in ('NE','OP') )";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(filter.Status))
                {
                    whereClause += string.Format(" AND AM.WO_STATUS = ('{0}')", filter.Status);
                }

            }
            if (!string.IsNullOrWhiteSpace(filter.Priority))
            {
                whereClause += " AND LOWER(AM.JOB_URGENCY) LIKE ('%" + filter.Priority + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.JobType))
            {
                whereClause += " AND LOWER(AM.JOB_TYPE) LIKE ('%" + filter.JobType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignTo))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TO) LIKE ('%" + filter.assignTo + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignType))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TYPE) LIKE ('%" + filter.assignType + "%')";
            }

            //this will not select null values
            whereClause = whereClause.Insert(0, "(") + ") AND (AM.DTYPE <> '' AND AM.DTYPE <> 'NULL')";
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " GROUP BY DTYPE, DTYPE_DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchByProblemType : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=1 ";
		//private string columns = " AM.REQ_TYPE, AM.PROBLEM_TYPEDESC AS DESCRIPTION,SUM(WO_CLOSED) AS CLOSED ,SUM(WO_FA) AS WO_FA ,SUM(WO_HELD) AS WO_HELD,SUM(WO_NEW) AS WO_NEW,SUM(WO_OPEN) AS WO_OPEN,SUM(WO_POSTED) AS WO_POSTED";
		private string columnsSimple = "REQ_NO, REQ_NO, REQ_BY, REQ_DATE, PROBLEM, STATUS, REQ_TYPE, ASSET_NO,JOB_TYPE, JOB_URGENCY";
		//private string from = "WO_JOBPROBLEMTYPE_V AM";

        private string columns = "AM.REQ_TYPE, AM.PROBLEM_TYPEDESC AS DESCRIPTION, SUM(case when AM.WO_STATUS = 'CL' then 1 else 0 end) AS CLOSESTATUS,SUM(case when AM.WO_STATUS = 'HA' then 1 else 0 end) AS HOLDSTATUS, SUM(CASE when AM.WO_STATUS = 'NE' then 1 else 0 end) AS NEWSTATUS,SUM(CASE when AM.WO_STATUS = 'OP' then 1 else 0 end) AS OPENSTATUS, SUM(CASE when AM.WO_STATUS = 'PS' then 1 else 0 end) AS POSTEDSTATUS";
        private string from = "WORKORDERREPT_V AM";


        public IndieSearchByProblemType(string dbType, string queryString, SearchFilter filter, int maxRows = 500)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			//foreach (string query in queryArray)
			//{
			var query2 = queryString.ToLower();
			if (!string.IsNullOrWhiteSpace(query2))
			{
				whereClause += "  AND (LOWER(AM.REQ_TYPE) LIKE ('%" + query2 + "%') " +
                " OR LOWER(AM.PROBLEM_TYPEDESC) LIKE ('%" + query2 + "%'))";
			}

			if (!string.IsNullOrWhiteSpace(filter.FromDate))
			{
				var whereDate = string.Format("AM.REQ_DATE >= '{0}'", filter.FromDate);
				if (!string.IsNullOrWhiteSpace(filter.ToDate))
				{
					whereDate += string.Format(" AND AM.REQ_DATE <= '{0}'", filter.ToDate);
				}
				whereClause += " AND (" + whereDate + ")";
			}

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND LOWER(AM.COSTCENTERNAME) LIKE ('%" + filter.CostCenterStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += " AND LOWER(AM.SUPPLIERNAME) LIKE ('%" + filter.SupplierStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.ProblemType))
            {
                whereClause += " AND LOWER(AM.REQ_TYPE) LIKE ('%" +  filter.ProblemType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.pmtype))
            {
                whereClause += " AND LOWER(AM.PM_TYPE) LIKE ('%" + filter.pmtype + "%')";
            }
            if (filter.Status == "PSOP")
            {
                whereClause += " AND (AM.WO_STATUS in ('NE','OP') )";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(filter.Status))
                {
                    whereClause += string.Format(" AND AM.WO_STATUS = ('{0}')", filter.Status);
                }

            }
            if (!string.IsNullOrWhiteSpace(filter.Priority))
            {
                whereClause += " AND LOWER(AM.JOB_URGENCY) LIKE ('%" + filter.Priority + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.JobType))
            {
                whereClause += " AND LOWER(AM.JOB_TYPE) LIKE ('%" + filter.JobType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignTo))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TO) LIKE ('%" + filter.assignTo + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignType))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TYPE) LIKE ('%" + filter.assignType + "%')";
            }

            //this will not select null values
            whereClause = whereClause.Insert(0, "(") + ") AND (AM.REQ_TYPE <> '' AND AM.REQ_TYPE <> 'NULL')";
        }

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " GROUP BY REQ_TYPE, PROBLEM_TYPEDESC" };
				}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
			}
		}


	}
	public class IndieSearchByJobType : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=1 ";
		//private string columns = " AM.JOB_TYPE, ISNULL(AM.JOB_TYPEDESC, AM.JOB_TYPE) AS DESCRIPTION,SUM(WO_CLOSED) AS CLOSED ,SUM(WO_FA) AS WO_FA ,SUM(WO_HELD) AS WO_HELD,SUM(WO_NEW) AS WO_NEW, SUM(WO_OPEN) AS WO_OPEN, SUM(WO_POSTED) AS WO_POSTED";
		private string columnsSimple = "REQ_NO, REQ_NO, REQ_BY, REQ_DATE, PROBLEM, STATUS, REQ_TYPE, ASSET_NO,JOB_TYPE, JOB_URGENCY";
        //private string from = "WO_JOBPROBLEMTYPE_V AM";

        private string columns = "AM.JOB_TYPE, AM.JOB_TYPEDESC AS DESCRIPTION, SUM(case when AM.WO_STATUS = 'CL' then 1 else 0 end) AS CLOSESTATUS,SUM(case when AM.WO_STATUS = 'HA' then 1 else 0 end) AS HOLDSTATUS, SUM(CASE when AM.WO_STATUS = 'NE' then 1 else 0 end) AS NEWSTATUS,SUM(CASE when AM.WO_STATUS = 'OP' then 1 else 0 end) AS OPENSTATUS, SUM(CASE when AM.WO_STATUS = 'PS' then 1 else 0 end) AS POSTEDSTATUS";
        private string from = "WORKORDERREPT_V AM";

        public IndieSearchByJobType(string dbType, string queryString, SearchFilter filter, int maxRows = 500)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			//foreach (string query in queryArray)
			//{
			var query2 = queryString.ToLower();
			if (!string.IsNullOrWhiteSpace(query2))
			{
				whereClause += "  AND (LOWER(AM.JOB_TYPE) LIKE ('" + query2 + "%') " +
				" OR LOWER(AM.JOB_TYPEDESC) LIKE ('" + query2 + "%'))";
			}
			if (!string.IsNullOrWhiteSpace(filter.FromDate))
			{
				var whereDate = string.Format("AM.REQ_DATE >= '{0}'", filter.FromDate);
				if (!string.IsNullOrWhiteSpace(filter.ToDate))
				{
					whereDate += string.Format(" and AM.REQ_DATE <= '{0}'", filter.ToDate);
				}
				whereClause += " AND (" + whereDate + ")";
			}

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND LOWER(AM.COSTCENTERNAME) LIKE ('%" + filter.CostCenterStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += " AND LOWER(AM.SUPPLIERNAME) LIKE ('%" + filter.SupplierStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.ProblemType))
            {
                whereClause += " AND LOWER(AM.REQ_TYPE) LIKE ('%" + filter.ProblemType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.pmtype))
            {
                whereClause += " AND LOWER(AM.PM_TYPE) LIKE ('%" + filter.pmtype + "%')";
            }
            if (filter.Status == "PSOP")
            {
                whereClause += " AND (AM.WO_STATUS in ('NE','OP') )";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(filter.Status))
                {
                    whereClause += string.Format(" AND AM.WO_STATUS = ('{0}')", filter.Status);
                }

            }
            if (!string.IsNullOrWhiteSpace(filter.Priority))
            {
                whereClause += " AND LOWER(AM.JOB_URGENCY) LIKE ('%" + filter.Priority + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.JobType))
            {
                whereClause += " AND LOWER(AM.JOB_TYPE) LIKE ('%" + filter.JobType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignTo))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TO) LIKE ('%" + filter.assignTo + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignType))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TYPE) LIKE ('%" + filter.assignType + "%')";
            }

            //this will not select null values
            whereClause = whereClause.Insert(0, "(") + ") AND (AM.JOB_TYPE <> '' AND AM.JOB_TYPE <> 'NULL')";
        }

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " GROUP BY JOB_TYPE, JOB_TYPEDESC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
			}
		}


	}
    public class IndieWOPartsRequest : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 And AM.Price>0 ";
        private string columns = "AM.REQUEST_ID,  AM.STATUS, AM.REQ_DATE,";
        private string columnsSimple = "AM_PRREQITEMID, DESCRIPTION, QTY, CITY, PRICE, EXTENDED, CONTRACT_NO, REF_WO";
        private string columnsSql = "AM.id_parts, AM.parts_description, AM.req_no, req_date, AM.asset_no, AM.RETURN_QTY, AM.TRANSFER_QTY, AM.qty, AM.used_qty, AM.status, AM.price,AM.price*AM.qty as TotalPrice, AM.asset_costcenter, AM.supplier_description";

        private string from = "WOPARTSREQUESTNotRecieved_V AM";
        //private string groupByClause = "AM.REQUEST_ID ,AM.STATUS, Convert(varchar(11),AM.REQ_DATE,103) ";
        public IndieWOPartsRequest(string dbType, string queryString, SearchFilter filter, int maxRows = 10000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;

            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            if (!string.IsNullOrWhiteSpace(query2))
            {
                whereClause += " AND (LOWER(AM.id_parts) LIKE ('%" + query2 + "%') OR LOWER(AM.parts_description) LIKE ('%" + query2 + "%') OR LOWER(AM.req_no) LIKE ('%" + query2 + "%') OR LOWER(AM.asset_no) LIKE ('%" + query2 + "%') OR LOWER(AM.status) LIKE ('%" + query2 + "%') OR LOWER(AM.price) LIKE ('%" + query2 + "%') OR LOWER(AM.asset_costcenter) LIKE ('%" + query2 + "%') OR LOWER(AM.supplier_description) LIKE ('%" + query2 + "%') OR LOWER(AM.WO_STATUS) LIKE ('%" + query2 + "%') OR LOWER(AM.RETURN_QTY) LIKE ('%" + query2 + "%') OR LOWER(AM.TRANSFER_QTY) LIKE ('%" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("AM.req_date >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" AND AM.req_date <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += string.Format(" AND LOWER(AM.asset_costcenter) LIKE LTrim('%" + filter.CostCenterStr + "%')");

            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += string.Format(" AND LOWER(AM.supplier_description) LIKE LTrim('%" + filter.SupplierStr + "%')");

            }
            if (!string.IsNullOrWhiteSpace(filter.Status))
            {
                whereClause += string.Format(" AND LOWER(AM.status) LIKE LTrim('%" + filter.Status + "%')");

            }
            if (filter.woStatus == "PSOP")
            {
                whereClause += " AND (AM.WO_STATUS in ('NE','OP') )";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(filter.woStatus))
                {
                    whereClause += string.Format(" AND AM.WO_STATUS = ('{0}')", filter.woStatus);
                }

            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + " ORDER BY AM.REQ_DATE DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + "GROUP BY AM.REQUEST_ID ORDER BY  REQ_DATE DESC" + " LIMIT " + maxRows.ToString() };
            }
        }

    }

    public class IndieWOPartsRequestZero : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 And AM.Price=0 ";
        private string columns = "AM.REQUEST_ID,  AM.STATUS, AM.REQ_DATE,";
        private string columnsSimple = "AM_PRREQITEMID, DESCRIPTION, QTY, CITY, PRICE, EXTENDED, CONTRACT_NO, REF_WO";
        private string columnsSql = "AM.id_parts, AM.parts_description, AM.req_no, req_date, AM.asset_no, AM.RETURN_QTY, AM.TRANSFER_QTY, AM.qty, AM.used_qty, AM.status, AM.price,AM.price*AM.qty as TotalPrice, AM.asset_costcenter, AM.supplier_description";

        private string from = "WOPARTSREQUESTNotRecieved_V AM";
        //private string groupByClause = "AM.REQUEST_ID ,AM.STATUS, Convert(varchar(11),AM.REQ_DATE,103) ";
        public IndieWOPartsRequestZero(string dbType, string queryString, SearchFilter filter, int maxRows = 10000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;

            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            if (!string.IsNullOrWhiteSpace(query2))
            {
                whereClause += " AND (LOWER(AM.id_parts) LIKE ('%" + query2 + "%') OR LOWER(AM.parts_description) LIKE ('%" + query2 + "%') OR LOWER(AM.req_no) LIKE ('%" + query2 + "%') OR LOWER(AM.asset_no) LIKE ('%" + query2 + "%') OR LOWER(AM.status) LIKE ('%" + query2 + "%') OR LOWER(AM.price) LIKE ('%" + query2 + "%') OR LOWER(AM.asset_costcenter) LIKE ('%" + query2 + "%') OR LOWER(AM.supplier_description) LIKE ('%" + query2 + "%') OR LOWER(AM.WO_STATUS) LIKE ('%" + query2 + "%') OR LOWER(AM.RETURN_QTY) LIKE ('%" + query2 + "%') OR LOWER(AM.TRANSFER_QTY) LIKE ('%" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("AM.req_date >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" AND AM.req_date <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += string.Format(" AND LOWER(AM.asset_costcenter) LIKE LTrim('%" + filter.CostCenterStr + "%')");

            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += string.Format(" AND LOWER(AM.supplier_description) LIKE LTrim('%" + filter.SupplierStr + "%')");

            }
            if (!string.IsNullOrWhiteSpace(filter.Status))
            {
                whereClause += string.Format(" AND LOWER(AM.status) LIKE LTrim('%" + filter.Status + "%')");

            }
            if (filter.woStatus == "PSOP")
            {
                whereClause += " AND (AM.WO_STATUS in ('NE','OP') )";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(filter.woStatus))
                {
                    whereClause += string.Format(" AND AM.WO_STATUS = ('{0}')", filter.woStatus);
                }

            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + " ORDER BY AM.REQ_DATE DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + "GROUP BY AM.REQUEST_ID ORDER BY  REQ_DATE DESC" + " LIMIT " + maxRows.ToString() };
            }
        }

    }

    public class IndieSearchWOPartsRequest : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "AM.REQUEST_ID,  AM.STATUS, AM.REQ_DATE,";
        private string columnsSimple = "AM_PRREQITEMID, DESCRIPTION, QTY, CITY, PRICE, EXTENDED, CONTRACT_NO, REF_WO";
        private string columnsSql = "AM.id_parts, AM.parts_description, AM.req_no, AM.req_date, AM.asset_no, AM.RETURN_QTY, AM.TRANSFER_QTY, AM.qty, AM.used_qty, AM.status, AM.price, AM.asset_costcenter, AM.supplier_description";

        private string from = "WOPARTSREQUEST_V AM";
        //private string groupByClause = "AM.REQUEST_ID ,AM.STATUS, Convert(varchar(11),AM.REQ_DATE,103) ";
        public IndieSearchWOPartsRequest(string dbType, string queryString, SearchFilter filter, int maxRows = 10000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;

            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            if (!string.IsNullOrWhiteSpace(query2))
            {
                whereClause += " AND (LOWER(AM.id_parts) LIKE ('%" + query2 + "%') OR LOWER(AM.parts_description) LIKE ('%" + query2 + "%') OR LOWER(AM.req_no) LIKE ('%" + query2 + "%') OR LOWER(AM.asset_no) LIKE ('%" + query2 + "%') OR LOWER(AM.status) LIKE ('%" + query2 + "%') OR LOWER(AM.price) LIKE ('%" + query2 + "%') OR LOWER(AM.asset_costcenter) LIKE ('%" + query2 + "%') OR LOWER(AM.supplier_description) LIKE ('%" + query2 + "%') OR LOWER(AM.WO_STATUS) LIKE ('%" + query2 + "%') OR LOWER(AM.RETURN_QTY) LIKE ('%" + query2 + "%') OR LOWER(AM.TRANSFER_QTY) LIKE ('%" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("AM.req_date >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format("AND AM.req_date <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += string.Format(" AND LOWER(AM.asset_costcenter) LIKE LTrim('%" + filter.CostCenterStr + "%')");

            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += string.Format(" AND LOWER(AM.supplier_description) LIKE LTrim('%" + filter.SupplierStr + "%')");

            }
            if (!string.IsNullOrWhiteSpace(filter.Status))
            {
                whereClause += string.Format(" AND LOWER(AM.status) LIKE LTrim('%" + filter.Status + "%')");

            }
            if (filter.woStatus == "PSOP")
            {
                whereClause += " AND (AM.WO_STATUS in ('NE','OP') )";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(filter.woStatus))
                {
                    whereClause += string.Format(" AND AM.WO_STATUS = ('{0}')", filter.woStatus);
                }

            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + " ORDER BY AM.REQ_DATE DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + "GROUP BY AM.REQUEST_ID ORDER BY  REQ_DATE DESC" + " LIMIT " + maxRows.ToString() };
            }
        }

    }
    public class IndieSearchWOPartsRequestRorFR : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "AM.REQUEST_ID,  AM.STATUS, AM.REQ_DATE,";
        private string columnsSimple = "AM_PRREQITEMID, DESCRIPTION, QTY, CITY, PRICE, EXTENDED, CONTRACT_NO, REF_WO";
        private string columnsSql = "AM.id_parts, AM.parts_description, AM.req_no, AM.req_date, AM.asset_no, AM.RETURN_QTY, AM.FORRETURN_QTY, AM.TRANSFER_QTY, AM.qty, AM.used_qty, AM.status, AM.price, AM.asset_costcenter, AM.supplier_description";

        private string from = "WOPARTSREQUEST_V AM";
        //private string groupByClause = "AM.REQUEST_ID ,AM.STATUS, Convert(varchar(11),AM.REQ_DATE,103) ";
        public IndieSearchWOPartsRequestRorFR(string dbType, string queryString, SearchFilter filter, int maxRows = 10000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;

            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            if (!string.IsNullOrWhiteSpace(query2))
            {
                whereClause += " AND (LOWER(AM.id_parts) LIKE ('%" + query2 + "%') OR LOWER(AM.parts_description) LIKE ('%" + query2 + "%') OR LOWER(AM.req_no) LIKE ('%" + query2 + "%') OR LOWER(AM.asset_no) LIKE ('%" + query2 + "%') OR LOWER(AM.status) LIKE ('%" + query2 + "%') OR LOWER(AM.price) LIKE ('%" + query2 + "%') OR LOWER(AM.asset_costcenter) LIKE ('%" + query2 + "%') OR LOWER(AM.supplier_description) LIKE ('%" + query2 + "%') OR LOWER(AM.WO_STATUS) LIKE ('%" + query2 + "%') OR LOWER(AM.RETURN_QTY) LIKE ('%" + query2 + "%') OR LOWER(AM.FORRETURN_QTY) LIKE ('%" + query2 + "%') OR LOWER(AM.TRANSFER_QTY) LIKE ('%" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("AM.req_date >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format("AND AM.req_date <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += string.Format(" AND LOWER(AM.asset_costcenter) LIKE LTrim('%" + filter.CostCenterStr + "%')");

            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += string.Format(" AND LOWER(AM.supplier_description) LIKE LTrim('%" + filter.SupplierStr + "%')");

            }
            if (!string.IsNullOrWhiteSpace(filter.Status))
            {
                whereClause += string.Format(" AND LOWER(AM.status) LIKE LTrim('%" + filter.Status + "%')");

            }
            if (filter.woStatus == "PSOP")
            {
                whereClause += " AND (AM.WO_STATUS in ('NE','OP') )";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(filter.woStatus))
                {
                    whereClause += string.Format(" AND AM.WO_STATUS = ('{0}')", filter.woStatus);
                }

            }

            whereClause = whereClause.Insert(0, "(") + ") AND (AM.RETURN_QTY > 0 OR AM.FORRETURN_QTY > 0)";

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + " ORDER BY AM.REQ_DATE DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + "GROUP BY AM.REQUEST_ID ORDER BY  REQ_DATE DESC" + " LIMIT " + maxRows.ToString() };
            }
        }

    }
    
    public class IndieSearchSOHAdjustmentRept : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "";
        private string columnsSimple = "REQ_NO, REQ_NO, REQ_BY, REQ_DATE, PROBLEM, STATUS, REQ_TYPE, ASSET_NO,JOB_TYPE, JOB_URGENCY";
        private string columnsSql = "AM.REASON,  AM.ID_PARTS, Convert(varchar(11),AM.CREATED_DATE,103) AS CREATED_DATE, ST.DESCRIPTION, AM.QTY_ADJUST, AM.QTY_ACTUAL, AM.MIN_ADJUST, AM.MAX_ADJUST, AM.STATUS_ACTUAL, AM.MIN_ACTUAL, AM.MAX_ACTUAL, AM.STATUS_ADJUST, SF.FIRST_NAME +' '+ SF.LAST_NAME DONEDESC ";
        private string from = "STOCK_ONHAND_ADJUSTMENT AM LEFT JOIN AM_PARTS ST ON AM.ID_PARTS = ST.ID_PARTS LEFT JOIN STAFF SF ON AM.CREATED_BY = SF.STAFF_ID  LEFT JOIN STORES SM ON AM.STORE_ID = SM.STORE_ID";

        public IndieSearchSOHAdjustmentRept(string dbType, string queryString, SearchFilter filter, int maxRows = 10000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
     
                whereClause += "  OR (LOWER(AM.ID_PARTS) LIKE ('%" + query2 + "%') ";
                whereClause += "  OR LOWER(SM.DESCRIPTION) LIKE ('%" + query2 + "%') ";
                whereClause += "  OR LOWER(ST.DESCRIPTION) LIKE ('%" + query2 + "%') ";
                whereClause += "  OR LOWER(AM.REASON) LIKE ('%" + query2 + "%') ";
                whereClause += "  OR LOWER(SF.FIRST_NAME) LIKE ('%" + query2 + "%')) ";


         

            if (filter.FromStore > 0)
            {
                whereClause += " AND ( STORE_ID =" + filter.FromStore + ")";
            }
            if (filter.FromStore > 0)
            {
                whereClause += " AND ( STORE_ID =" + filter.FromStore + ")";
            }
            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("AM.CREATED_DATE >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" and AM.CREATED_DATE <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }



        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + "ORDER BY  AM.ID DESC " };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchPMScheduleRept : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "";
        private string columnsSimple = "REQ_NO, REQ_NO, REQ_BY, REQ_DATE, PROBLEM, STATUS, REQ_TYPE, ASSET_NO,JOB_TYPE, JOB_URGENCY";
        private string columnsSql = "AM.ASSET_NO, Convert(varchar(11),AM.NEXT_PM_DUE,103) AS NEXT_PM_DUE, ST.DESCRIPTION, AM.INTERVAL, AM.FREQUENCY_PERIOD ,DATEDIFF( day,  GETDATE(),NEXT_PM_DUE  ) as DAYCOUNT, AM.LAST_REQ_NO, Convert(varchar(11),WO.REQ_DATE,103) AS LAST_WOREQDATE, ST.PM_TYPE";
        private string from = "AM_ASSET_PMSCHEDULES AM LEFT JOIN AM_ASSET_DETAILS ST ON AM.ASSET_NO = ST.ASSET_NO LEFT JOIN AM_WORK_ORDERS WO ON AM.LAST_REQ_NO = WO.REQ_NO LEFT OUTER JOIN AM_MANUFACTURER MF ON ST.MANUFACTURER = MF.MANUFACTURER_ID LEFT OUTER JOIN AM_SUPPLIER SP ON ST.SUPPLIER = SP.SUPPLIER_ID LEFT OUTER JOIN LOV_LOOKUPS LL ON ST.COST_CENTER = LL.LOV_LOOKUP_ID AND LL.CATEGORY ='AIMS_COSTCENTER'";

        public IndieSearchPMScheduleRept(string dbType, string queryString,  SearchFilter filter,  int maxRows = 10000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();

            whereClause += "  OR (LOWER(ST.DESCRIPTION) LIKE ('%" + query2 + "%') ";
            whereClause += "  OR LOWER(AM.ASSET_NO) LIKE ('%" + query2 + "%')) ";
                       
            //var whereDate = string.Format("AM.NEXT_PM_DUE <= ", fromDate);
            if (!string.IsNullOrWhiteSpace(filter.FromDate) && (filter.frequency != "null" && filter.frequency != ""))
            {
                whereClause += " AND (AM.NEXT_PM_DUE <='" + filter.FromDate + "')";
            }
            
                whereClause += " AND (ST.STATUS ='I')";
            if ((!string.IsNullOrWhiteSpace(filter.ToDate) ||!string.IsNullOrWhiteSpace(filter.FromDate)) && (filter.frequency == "null" || filter.frequency == ""))
            {
               // var whereDate = "";
                if (!string.IsNullOrWhiteSpace(filter.FromDate))
                {
                    whereClause += "AND "+string.Format(" AM.NEXT_PM_DUE >= '{0}'", filter.FromDate);
                }
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereClause += "AND " + string.Format(" AM.NEXT_PM_DUE <= '{0}'", filter.ToDate);
                }
                //whereClause += " AND (" + whereDate + ")";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += string.Format(" AND (LOWER(LL.DESCRIPTION) LIKE LTrim('%" + filter.CostCenterStr + "%'))");

            }

            if (!string.IsNullOrWhiteSpace(filter.ManufacturerStr))
            {
                whereClause += string.Format(" AND (LOWER(MF.description) LIKE LTrim('%" + filter.ManufacturerStr + "%'))");

            }

            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += string.Format(" AND (LOWER(SP.description) LIKE LTrim('%" + filter.SupplierStr + "%'))");

            }

            if (!string.IsNullOrWhiteSpace(filter.pmtype))
            {
                whereClause += string.Format(" AND (LOWER(ST.PM_TYPE) LIKE LTrim('%" + filter.pmtype + "%'))");

            }

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + "ORDER BY  Convert(varchar(11),AM.NEXT_PM_DUE,103) DESC " };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchNotifyRept : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "";
        private string columnsSimple = "REQ_NO, REQ_NO, REQ_BY, REQ_DATE, PROBLEM, STATUS, REQ_TYPE, ASSET_NO,JOB_TYPE, JOB_URGENCY";
        private string columnsSql = "AM.MESSAGE_ID, Convert(varchar(11),AM.CREATED_DATE,103) AS CREATED_DATE,Convert(varchar(11),AM.DELIVERED_DATE,103) AS DELIVERED_DATE, AM.REF_CODE, AM.NOTIFY_MESSAGE, AM.NOTIFY_ID, AM.SMS_MESSAGE, AM.EMAIL_MESSAGE,AM.MOBILENO,ST.FIRST_NAME,ST.LAST_NAME,AM.STATUS";
        private string from = "AM_NOTIFY_MESSAGES AM LEFT JOIN STAFF ST ON AM.STAFF_ID = ST.STAFF_ID";

        public IndieSearchNotifyRept(string dbType, string queryString, SearchFilter filter, int maxRows = 10000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();

            whereClause += "  OR (LOWER(ST.FIRST_NAME) LIKE ('%" + query2 + "%') ";
            whereClause += "  OR LOWER(ST.LAST_NAME) LIKE ('%" + query2 + "%')) ";

            if (!string.IsNullOrWhiteSpace(filter.notifyStaffId))
            {
                whereClause += " AND ( AM.STAFF_ID ='" + filter.notifyStaffId + "')";
            }
            if (!string.IsNullOrWhiteSpace(filter.notifyStatus))
            {
                whereClause += " AND ( AM.STATUS ='" + filter.notifyStatus + "')";
            }


        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + "ORDER BY  Convert(varchar(11),AM.CREATED_DATE,103) DESC " };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchEngrStatusRept : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "";
        //private string columnsSimple = "AM.CREATED_BY, SF.FIRST_NAME,SF.LAST_NAME, SUM(case when AM.WO_STATUS = 'CL' then 1 end) AS CLOSESTATUS, SUM(case when AM.WO_STATUS = 'HA' then 1 end) AS HOLDSTATUS,SUM(CASE when AM.WO_STATUS = 'NE' then 1 else 0 end) AS NEWSTATUS,SUM(CASE when AM.WO_STATUS = 'OP' then 1 else 0 end) AS OPENSTATUS, SUM(CASE when AM.WO_STATUS = 'PS' then 1 else 0 end) AS POSTEDSTATUS ";
        private string columnsSimple = "AM.CREATED_BY, AM.CREATEDBY_FULLNAME,sum(case when AM.CLOSED_DATE > AM.job_due_date then 1 else 0 end) AS OverDue,sum(case when (AM.WO_STATUS = 'HA' And AM.REQ_TYPE='PM') then 1 else 0 end) AS PMHold," +
                    "sum(case when AM.WO_STATUS = 'CL' then 1 else 0 end)-sum(case when (AM.WO_STATUS = 'CL' And AM.REQ_TYPE= 'PM') then 1 else 0 end) as CRClose," +
                    "sum(case When (getDate()>AM.job_due_date and AM.WO_STATUS in ('OP','NE','HE')) then 1 else 0 end) as OverDueOpen,sum(case when(AM.WO_STATUS = 'OP' And AM.REQ_TYPE = 'PM') then 1 " +
                    "else 0 end) AS PMOpen,sum(CASE when(AM.WO_STATUS = 'NE' And AM.REQ_TYPE = 'PM') then 1 else 0 end) AS PMNew,sum(case when (AM.WO_STATUS = 'CL' And AM.REQ_TYPE = 'PM') then 1 else " +
                    "0 end) AS PMClose,(sum(case when AM.WO_STATUS = 'NE' then 1 else 0 end) - sum(case when(AM.WO_STATUS = 'OP' And AM.REQ_TYPE = 'PM') then 1 else 0 end)) AS CRNew,SUM(case when " +
                    "AM.WO_STATUS = 'OP' then 1 else 0 end)-sum(case when (AM.WO_STATUS = 'OP' And AM.REQ_TYPE= 'PM') then 1 else 0 end) AS CROpen," +
                    "sum(case when AM.WO_STATUS = 'HA' and AM.Req_Type='CM' then 1 else 0 end) as CRHold,SUM(case when (AM.WO_STATUS = 'FC' And AM.REQ_TYPE= 'PM') then 1 else 0 end) AS PMFClose" +
                    ",SUM(case when(AM.WO_STATUS = 'FC' And AM.REQ_TYPE <> 'PM') then 1 else 0 end) AS CMFClose";

        //private string columnsSql = "AM.CREATED_BY, SF.FIRST_NAME,SF.LAST_NAME, SUM(case when AM.WO_STATUS = 'CL' then 1 end) AS CLOSESTATUS, SUM(case when AM.WO_STATUS = 'HA' then 1 end) AS HOLDSTATUS,SUM(CASE when AM.WO_STATUS = 'NE' then 1 else 0 end) AS NEWSTATUS,SUM(CASE when AM.WO_STATUS = 'OP' then 1 else 0 end) AS OPENSTATUS, SUM(CASE when AM.WO_STATUS = 'PS' then 1 else 0 end) AS POSTEDSTATUS ";
        //private string from = "AM_WORK_ORDERS AM LEFT JOIN STAFF SF ON AM.CREATED_BY = SF.STAFF_ID";

        //private string columnsSql = "AM.CREATED_BY, AM.CREATEDBY_FULLNAME, SUM(case when AM.WO_STATUS = 'CL' then 1 else 0 end) AS CLOSESTATUS,SUM(case when AM.WO_STATUS = 'HA' then 1 else 0 end) AS HOLDSTATUS, SUM(CASE when AM.WO_STATUS = 'NE' then 1 else 0 end) AS NEWSTATUS,SUM(CASE when AM.WO_STATUS = 'OP' then 1 else 0 end) AS OPENSTATUS, SUM(CASE when AM.WO_STATUS = 'PS' then 1 else 0 end) AS POSTEDSTATUS";
        private string columnsSql = "AM.CREATED_BY, AM.CREATEDBY_FULLNAME,sum(case when AM.CLOSED_DATE > AM.job_due_date then 1 else 0 end) AS OverDue,sum(case when (AM.WO_STATUS = 'HA' And AM.REQ_TYPE='PM') then 1 else 0 end) AS PMHold," +
                            "sum(case when AM.WO_STATUS = 'CL' then 1 else 0 end)-sum(case when (AM.WO_STATUS = 'CL' And AM.REQ_TYPE= 'PM') then 1 else 0 end) as CRClose," +
                            "sum(case When (getDate()>AM.job_due_date and AM.WO_STATUS in ('OP','NE','HE')) then 1 else 0 end) as OverDueOpen,sum(case when(AM.WO_STATUS = 'OP' And AM.REQ_TYPE = 'PM') then 1 " +
                            "else 0 end) AS PMOpen,sum(CASE when(AM.WO_STATUS = 'NE' And AM.REQ_TYPE = 'PM') then 1 else 0 end) AS PMNew,sum(case when (AM.WO_STATUS = 'CL' And AM.REQ_TYPE = 'PM') then 1 else " +
                            "0 end) AS PMClose,(sum(case when AM.WO_STATUS = 'NE' then 1 else 0 end) - sum(case when(AM.WO_STATUS = 'OP' And AM.REQ_TYPE = 'PM') then 1 else 0 end)) AS CRNew,SUM(case when " +
                            "AM.WO_STATUS = 'OP' then 1 else 0 end)-sum(case when (AM.WO_STATUS = 'OP' And AM.REQ_TYPE= 'PM') then 1 else 0 end) AS CROpen," +
                            "sum(case when AM.WO_STATUS = 'HA' and AM.Req_Type='CM' then 1 else 0 end) as CRHold,SUM(case when (AM.WO_STATUS = 'FC' And AM.REQ_TYPE= 'PM') then 1 else 0 end) AS PMFClose" +
                            ",SUM(case when(AM.WO_STATUS = 'FC' And AM.REQ_TYPE <> 'PM') then 1 else 0 end) AS CMFClose";
        private string from = "dbo.WORKORDERREPT_V AM";

        public IndieSearchEngrStatusRept(string dbType, string queryString, SearchFilter filter, int maxRows = 10000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();

            whereClause += "  OR (LOWER(AM.REQ_NO) LIKE ('%" + query2 + "%') ";
            whereClause += "  OR LOWER(AM.CREATEDBY_FULLNAME) LIKE ('%" + query2 + "%') ";
            whereClause += "  OR LOWER(AM.CREATED_BY) LIKE ('%" + query2 + "%')) ";


            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("AM.CREATED_DATE >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" and AM.CREATED_DATE <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND LOWER(AM.COSTCENTERNAME) LIKE ('%" + filter.CostCenterStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += " AND LOWER(AM.SUPPLIERNAME) LIKE ('%" + filter.SupplierStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.ProblemType))
            {
                whereClause += " AND LOWER(AM.REQ_TYPE) LIKE ('%" + filter.ProblemType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.pmtype))
            {
                whereClause += " AND LOWER(AM.PM_TYPE) LIKE ('%" + filter.pmtype + "%')";
            }
            if (filter.Status == "PSOP")
            {
                whereClause += " AND (AM.WO_STATUS in ('NE','OP') )";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(filter.Status))
                {
                    whereClause += string.Format(" AND AM.WO_STATUS = ('{0}')", filter.Status);
                }

            }
            if (!string.IsNullOrWhiteSpace(filter.Priority))
            {
                whereClause += " AND LOWER(AM.JOB_URGENCY) LIKE ('%" + filter.Priority + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.JobType))
            {
                whereClause += " AND LOWER(AM.JOB_TYPE) LIKE ('%" + filter.JobType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignTo))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TO) LIKE ('%" + filter.assignTo + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignType))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TYPE) LIKE ('%" + filter.assignType + "%')";
            }
            // adjust result
            //{
            //    whereClause += " AND LOWER(AM.status) ='Active' And Lower(AM.System_Lookup)='y' ";
            //}

            //this will not select null values
            whereClause = whereClause.Insert(0, "(") + ") AND (AM.CREATED_BY <> '' AND AM.CREATED_BY <> 'NULL') And AM.REQ_TYPE<>'SR'";
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + "GROUP BY  AM.CREATED_BY, AM.CREATEDBY_FULLNAME" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchEngrAssignStatusRept : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "";
        private string columnsSimple = "AM.ASSIGN_TO, AM.STAFF_FULLNAME,sum(case when AM.CLOSED_DATE > AM.job_due_date then 1 else 0 end) AS OverDue,sum(case when (AM.WO_STATUS = 'HA' And AM.REQ_TYPE='PM') then 1 else 0 end) AS PMHold," +
                                    "sum(case when AM.WO_STATUS = 'CL' then 1 else 0 end)-sum(case when (AM.WO_STATUS = 'CL' And AM.REQ_TYPE= 'PM') then 1 else 0 end) as CRClose," +
                                    "sum(case When (getDate()>AM.job_due_date and AM.WO_STATUS in ('OP','NE','HE')) then 1 else 0 end) as OverDueOpen,sum(case when(AM.WO_STATUS = 'OP' And AM.REQ_TYPE = 'PM') then 1 " +
                                    "else 0 end) AS PMOpen,sum(CASE when(AM.WO_STATUS = 'NE' And AM.REQ_TYPE = 'PM') then 1 else 0 end) AS PMNew,sum(case when (AM.WO_STATUS = 'CL' And AM.REQ_TYPE = 'PM') then 1 else " +
                                    "0 end) AS PMClose,(sum(case when AM.WO_STATUS = 'NE' then 1 else 0 end) - sum(case when(AM.WO_STATUS = 'OP' And AM.REQ_TYPE = 'PM') then 1 else 0 end)) AS CRNew,SUM(case when " +
                                    "AM.WO_STATUS = 'OP' then 1 else 0 end)-sum(case when (AM.WO_STATUS = 'OP' And AM.REQ_TYPE= 'PM') then 1 else 0 end) AS CROpen," +
                                    "sum(case when AM.WO_STATUS = 'HA' and AM.Req_Type='CM' then 1 else 0 end) as CRHold,SUM(case when (AM.WO_STATUS = 'FC' And AM.REQ_TYPE= 'PM') then 1 else 0 end) AS PMFClose" +
                                    ",SUM(case when(AM.WO_STATUS = 'FC' And AM.REQ_TYPE <> 'PM') then 1 else 0 end) AS CMFClose";
        //private string columnsSql = "AM.ASSIGN_TO, SF.FIRST_NAME,SF.LAST_NAME, SUM(case when AM.WO_STATUS = 'CL' then 1 end) AS CLOSESTATUS,SUM(case when AM.WO_STATUS = 'HA' then 1 end) AS HOLDSTATUS, SUM(CASE when AM.WO_STATUS = 'NE' then 1 else 0 end) AS NEWSTATUS,SUM(CASE when AM.WO_STATUS = 'OP' then 1 else 0 end) AS OPENSTATUS, SUM(CASE when AM.WO_STATUS = 'PS' then 1 else 0 end) AS POSTEDSTATUS ";
        //private string from = "AM_WORK_ORDERS AM LEFT JOIN STAFF SF ON AM.ASSIGN_TO = SF.STAFF_ID SUM(CASE when AM.WO_STATUS = 'PS' then 1 else 0 end) AS POSTEDSTATUS,";
        private string columnsSql = "AM.ASSIGN_TO, AM.STAFF_FULLNAME,sum(case when AM.CLOSED_DATE > AM.job_due_date then 1 else 0 end) AS OverDue,sum(case when (AM.WO_STATUS = 'HA' And AM.REQ_TYPE='PM') then 1 else 0 end) AS PMHold," +
                                    "sum(case when AM.WO_STATUS = 'CL' then 1 else 0 end)-sum(case when (AM.WO_STATUS = 'CL' And AM.REQ_TYPE= 'PM') then 1 else 0 end) as CRClose," +
                                    "sum(case When (getDate()>AM.job_due_date and AM.WO_STATUS in ('OP','NE','HE')) then 1 else 0 end) as OverDueOpen,sum(case when(AM.WO_STATUS = 'OP' And AM.REQ_TYPE = 'PM') then 1 " +
                                    "else 0 end) AS PMOpen,sum(CASE when(AM.WO_STATUS = 'NE' And AM.REQ_TYPE = 'PM') then 1 else 0 end) AS PMNew,sum(case when (AM.WO_STATUS = 'CL' And AM.REQ_TYPE = 'PM') then 1 else " +
                                    "0 end) AS PMClose,(sum(case when AM.WO_STATUS = 'NE' then 1 else 0 end) - sum(case when(AM.WO_STATUS = 'OP' And AM.REQ_TYPE = 'PM') then 1 else 0 end)) AS CRNew,SUM(case when " +
                                    "AM.WO_STATUS = 'OP' then 1 else 0 end)-sum(case when (AM.WO_STATUS = 'OP' And AM.REQ_TYPE= 'PM') then 1 else 0 end) AS CROpen," +
                                    "sum(case when AM.WO_STATUS = 'HA' and AM.Req_Type='CM' then 1 else 0 end) as CRHold,SUM(case when (AM.WO_STATUS = 'FC' And AM.REQ_TYPE= 'PM') then 1 else 0 end) AS PMFClose" +
                                    ",SUM(case when(AM.WO_STATUS = 'FC' And AM.REQ_TYPE <> 'PM') then 1 else 0 end) AS CMFClose";
        private string from = "dbo.WORKORDERREPT_V AM";

        public IndieSearchEngrAssignStatusRept(string dbType, string queryString, SearchFilter filter, int maxRows = 100000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();

            whereClause += "  OR (LOWER(AM.REQ_NO) LIKE ('%" + query2 + "%') ";
            whereClause += "  OR LOWER(AM.STAFF_FULLNAME) LIKE ('%" + query2 + "%') ";
            whereClause += "  OR LOWER(AM.CREATED_BY) LIKE ('%" + query2 + "%')) ";


            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("AM.REQ_DATE >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" and AM.REQ_DATE <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND LOWER(AM.COSTCENTERNAME) LIKE ('%" + filter.CostCenterStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += " AND LOWER(AM.SUPPLIERNAME) LIKE ('%" + filter.SupplierStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.ProblemType))
            {
                whereClause += " AND LOWER(AM.REQ_TYPE) LIKE ('%" + filter.ProblemType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.pmtype))
            {
                whereClause += " AND LOWER(AM.PM_TYPE) LIKE ('%" + filter.pmtype + "%')";
            }
            if (filter.Status == "PSOP")
            {
                whereClause += " AND (AM.WO_STATUS in ('NE','OP') )";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(filter.Status))
                {
                    whereClause += string.Format(" AND AM.WO_STATUS = ('{0}')", filter.Status);
                }

            }
            if (!string.IsNullOrWhiteSpace(filter.Priority))
            {
                whereClause += " AND LOWER(AM.JOB_URGENCY) LIKE ('%" + filter.Priority + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.JobType))
            {
                whereClause += " AND LOWER(AM.JOB_TYPE) LIKE ('%" + filter.JobType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignTo))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TO) LIKE ('%" + filter.assignTo + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignType))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TYPE) LIKE ('%" + filter.assignType + "%')";
            }
            // To Adjust Report
            //{
            //    whereClause += " AND LOWER(AM.Status) ='Active' And Lower(AM.System_Lookup)='y' ";
            //}
            //this will not select null values
            whereClause = whereClause.Insert(0, "(") + ") AND (AM.ASSIGN_TO <> '' AND AM.ASSIGN_TO <> 'NULL') And AM.REQ_TYPE<>'SR'";
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + "GROUP BY  AM.ASSIGN_TO, AM.STAFF_FULLNAME " + " ORDER BY AM.STAFF_FULLNAME" };
                }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchAssignTypeStatusRept : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "";
        private string columnsSimple = "AM.ASSIGN_TYPE, SUM(case when AM.WO_STATUS = 'CL' then 1 end) AS CLOSESTATUS, SUM(case when AM.WO_STATUS = 'HA' then 1 end) AS HOLDSTATUS,SUM(CASE when AM.WO_STATUS = 'NE' then 1 else 0 end) AS NEWSTATUS,SUM(CASE when AM.WO_STATUS = 'OP' then 1 else 0 end) AS OPENSTATUS, SUM(CASE when AM.WO_STATUS = 'PS' then 1 else 0 end) AS POSTEDSTATUS";
        //private string columnsSql = "AM.ASSIGN_TYPE, SUM(case when AM.WO_STATUS = 'CL' then 1 end) AS CLOSESTATUS, SUM(case when AM.WO_STATUS = 'HA' then 1 end) AS HOLDSTATUS,SUM(CASE when AM.WO_STATUS = 'NE' then 1 else 0 end) AS NEWSTATUS,SUM(CASE when AM.WO_STATUS = 'OP' then 1 else 0 end) AS OPENSTATUS, SUM(CASE when AM.WO_STATUS = 'PS' then 1 else 0 end) AS POSTEDSTATUS ";
        //private string from = "AM_WORK_ORDERS AM";

        private string columnsSql = "AM.ASSIGN_TYPE, SUM(case when AM.WO_STATUS = 'CL' then 1 else 0 end) AS CLOSESTATUS,SUM(case when AM.WO_STATUS = 'HA' then 1 else 0 end) AS HOLDSTATUS, SUM(CASE when AM.WO_STATUS = 'NE' then 1 else 0 end) AS NEWSTATUS,SUM(CASE when AM.WO_STATUS = 'OP' then 1 else 0 end) AS OPENSTATUS, SUM(CASE when AM.WO_STATUS = 'PS' then 1 else 0 end) AS POSTEDSTATUS";
        private string from = "WORKORDERREPT_V AM";


        public IndieSearchAssignTypeStatusRept(string dbType, string queryString, SearchFilter filter, int maxRows = 10000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();

            whereClause += "  OR (LOWER(AM.REQ_NO) LIKE ('%" + query2 + "%') ";
            whereClause += "  OR LOWER(AM.ASSIGN_TYPE) LIKE ('%" + query2 + "%') ";
            whereClause += "  OR LOWER(AM.CREATED_BY) LIKE ('%" + query2 + "%')) ";


            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("AM.CREATED_DATE >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" and AM.CREATED_DATE <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND LOWER(AM.COSTCENTERNAME) LIKE ('%" + filter.CostCenterStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += " AND LOWER(AM.SUPPLIERNAME) LIKE ('%" + filter.SupplierStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.ProblemType))
            {
                whereClause += " AND LOWER(AM.REQ_TYPE) LIKE ('%" + filter.ProblemType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.pmtype))
            {
                whereClause += " AND LOWER(AM.PM_TYPE) LIKE ('%" + filter.pmtype + "%')";
            }
            if (filter.Status == "PSOP")
            {
                whereClause += " AND (AM.WO_STATUS in ('NE','OP') )";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(filter.Status))
                {
                    whereClause += string.Format(" AND AM.WO_STATUS = ('{0}')", filter.Status);
                }

            }
            if (!string.IsNullOrWhiteSpace(filter.Priority))
            {
                whereClause += " AND LOWER(AM.JOB_URGENCY) LIKE ('%" + filter.Priority + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.JobType))
            {
                whereClause += " AND LOWER(AM.JOB_TYPE) LIKE ('%" + filter.JobType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignTo))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TO) LIKE ('%" + filter.assignTo + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignType))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TYPE) LIKE ('%" + filter.assignType + "%')";
            }

            //this will not select null values
            //whereClause = whereClause.Insert(0, "(") + ") AND (AM.ASSIGN_TYPE <> '' AND AM.ASSIGN_TYPE <> 'NULL')";
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + "GROUP BY  AM.ASSIGN_TYPE " };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchWOTimeRept : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "";
        private string columnsSimple = "AM.REQ_NO, AM.REQ_BY, AM.REQ_DATE, AM.PROBLEM,AM.WO_STATUS, AM.ASSET_NO,  AM.DESCRIPTION, AM.AM_INSPECTION_ID, AM.INSPECTION_TYPE, AM.CLOSED_DATE";
        private string columnsSql = "AM.REQ_NO, AM.REQ_BY, AM.REQ_DATE, AM.PROBLEM,AM.WO_STATUS,  AM.ASSET_NO,  AM.DESCRIPTION ,AM.CLOSED_DATE, AM.CREATED_NAME, AM.PROBTYPEDESC,AM.ASSIGNED_NAME";
        private string from = "WORK_ORDER_REPT_V AM";

        public IndieSearchWOTimeRept(string dbType, string queryString, SearchFilter filter, int maxRows = 10000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();

            whereClause += "  OR (LOWER(AM.REQ_NO) LIKE ('%" + query2 + "%') ";
            whereClause += "  OR LOWER(AM.ASSET_NO) LIKE ('%" + query2 + "%') ";
            whereClause += "  OR LOWER(AM.PROBTYPEDESC) LIKE ('%" + query2 + "%') ";
            whereClause += "  OR LOWER(AM.CREATED_NAME) LIKE ('%" + query2 + "%') ";
            whereClause += "  OR LOWER(AM.ASSET_NO) LIKE ('%" + query2 + "%') ";
            whereClause += "  OR LOWER(AM.CREATED_BY) LIKE ('%" + query2 + "%')) ";


            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("AM.REQ_DATE >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" and AM.REQ_DATE <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND LOWER(AM.COSTCENTERNAME) LIKE ('%" + filter.CostCenterStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += " AND LOWER(AM.SUPPLIERNAME) LIKE ('%" + filter.SupplierStr + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.ProblemType))
            {
                whereClause += " AND LOWER(AM.REQ_TYPE) LIKE ('%" + filter.ProblemType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.pmtype))
            {
                whereClause += " AND LOWER(AM.PM_TYPE) LIKE ('%" + filter.pmtype + "%')";
            }
            if (filter.Status == "PSOP")
            {
                whereClause += " AND (AM.WO_STATUS in ('NE','OP') )";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(filter.Status))
                {
                    whereClause += string.Format(" AND AM.WO_STATUS = ('{0}')", filter.Status);
                }

            }
            if (!string.IsNullOrWhiteSpace(filter.Priority))
            {
                whereClause += " AND LOWER(AM.JOB_URGENCY) LIKE ('%" + filter.Priority + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.JobType))
            {
                whereClause += " AND LOWER(AM.JOB_TYPE) LIKE ('%" + filter.JobType + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignTo))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TO) LIKE ('%" + filter.assignTo + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.assignType))
            {
                whereClause += " AND LOWER(AM.ASSIGN_TYPE) LIKE ('%" + filter.assignType + "%')";
            }

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + "ORDER BY  AM.REQ_DATE DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndieSearchWOListAssetNO : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "";
        private string columnsSimple = "AM.REQ_NO, AM.ASSET_NO";
        private string columnsSql = "AM.REQ_NO, AM.ASSET_NO";
        private string from = "WORKORDER_DET_V AM";

        public IndieSearchWOListAssetNO(string dbType, string queryString, SearchFilter filter, int maxRows = 10000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();

            whereClause += "  OR (LOWER(AM.REQ_NO) LIKE ('%" + query2 + "%') ";
            whereClause += "  OR LOWER(AM.REQ_BY) LIKE ('%" + query2 + "%')) ";


            if (!string.IsNullOrWhiteSpace(filter.woIdList))
            {

                whereClause += " AND AM.REQ_NO IN (" + filter.woIdList + ")";
            }



        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + "ORDER BY  AM.REQ_DATE DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndieSearchWOList : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "";
        private string columnsSimple = "AM.REQ_NO, AM.ASSET_NO, AM.REQ_BY,AM.REQ_DATE,AM.TICKET_ID,AM.PROBLEM,AM.JOB_DUE_DATE,AM.REQUESTER_CONTACT_NO,AM.WO_REFERENCE_NOTES,AM.WO_STATUS,AM.ASSETDESCRIPTION,AM.LOCATION,AM.WARRANTY_NOTES,AM.WARRANTY_EXPIRYDATE,AM.WARRANTY_FREQUNIT,AM.TERM_PERIOD,AM.MODEL_NO,AM.SERIAL_NO,AM.STATUS,AM.COSTCENTER,AM.RESPCENTER,AM.WARRANTYINCLUDED,AM.PROBLEMTYPE,AM.JOBTYPEDESC,AM.SERVDEPARTMENT,AM.STAFFNAME,AM.DEVICETYPE,AM.CLOSED_DATE";
        private string columnsSql = "AM.REQ_NO, AM.ASSET_NO, AM.REQ_BY,AM.REQ_DATE,AM.TICKET_ID,AM.PROBLEM,AM.JOB_DUE_DATE,AM.REQUESTER_CONTACT_NO,AM.WO_REFERENCE_NOTES,AM.WO_STATUS,AM.ASSETDESCRIPTION,AM.LOCATION,AM.WARRANTY_NOTES,AM.WARRANTY_EXPIRYDATE,AM.WARRANTY_FREQUNIT,AM.TERM_PERIOD,AM.MODEL_NO,AM.SERIAL_NO,AM.STATUS,AM.COSTCENTER,AM.RESPCENTER,AM.WARRANTYINCLUDED,AM.PROBLEMTYPE,AM.JOBTYPEDESC,AM.SERVDEPARTMENT,AM.STAFFNAME,AM.DEVICETYPE,AM.CLOSED_DATE";
        private string from = "WORKORDER_DET_V AM";

        public IndieSearchWOList(string dbType, string queryString, SearchFilter filter, int maxRows = 10000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();

            whereClause += "  OR (LOWER(AM.REQ_NO) LIKE ('%" + query2 + "%') ";
            whereClause += "  OR LOWER(AM.REQ_BY) LIKE ('%" + query2 + "%')) ";


            if (!string.IsNullOrWhiteSpace(filter.woIdList))
            {
                
                whereClause += " AND AM.REQ_NO IN (" + filter.woIdList + ")";
            }



        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + "ORDER BY  AM.REQ_DATE DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieReadWOIdMaterialsList : IndieSql
    {
        private int maxRows = 5000;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.AM_WORKORDER_LABOUR_ID, AM.AM_PARTREQUESTID, AM.DESCRIPTION, AM.QTY, AM.PRICE,AM.EXTENDED,AM.BILLABLE, AM.REF_WO, AM.STATUS,AM.WO_LABOURTYPE,AM.ID_PARTS,AM.REF_ASSETNO,AM.STORE_ID,AM.REQ_DATE, ST.DESCRIPTION PRODUCT_DESC, PX.DESCRIPTION STORE_DESC, AM.TRANSFER_QTY,AM.QTY_RETURN, AM.QTY_FORRETURN, AM.TRANSFER_FLAG";
        private string columnsSimple = "AM_WORKORDER_LABOUR_ID, AM_PARTREQUESTID, DESCRIPTION, QTY, PRICE, EXTENDED, BILLABLE, REF_WO,STATUS";
        private string from = "AM_PARTSREQUEST AM LEFT JOIN AM_PARTS ST ON AM.ID_PARTS = ST.ID_PARTS LEFT JOIN STORES PX ON PX.STORE_ID = AM.STORE_ID";

        public IndieReadWOIdMaterialsList(string dbType, string queryString, SearchFilter filter,  int maxRows = 5000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR AM.REF_WO in (" + filter.woIdList + ")";



        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PARTREQUESTID ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PARTREQUESTID ASC" + " LIMIT 5000" };
            }
        }


    }
    public class IndieSearchWOLabours : IndieSql
    {
        private int maxRows = 5000;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "LL.AM_WORKORDER_LABOUR_ID, LL.RESPONSE, LL.LABOUR_ACTION, LL.EMPLOYEE, LL.SUPPLIER, LL.HOURS, LL.REQ_NO, LL.LABOUR_DATE , LL.BILLABLE";
        private string columnsSimple = "AM_WORKORDER_LABOUR_ID, ASSET_NO, REQ_NO,EMPLOYEE,LABOUR_TYPE,RESPONSE";
        private string from = "WORKORDER_ACT_V LL";

        public IndieSearchWOLabours(string dbType, string queryString, SearchFilter filter, int maxRows = 5000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(LL.AM_WORKORDER_LABOUR_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(LL.RESPONSE) LIKE ('%" + query2 + "%'))";

            }
          
             whereClause += " AND (LL.REQ_NO IN (" + filter.woIdList + ") )";


        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY DESCRIPTION " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndieSearchAssetPartsYR : IndieSql
    {
        private int maxRows = 5000;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "LL.ASSET_NO, LL.COSTBYPARTS, LL.REQ_YEAR";
        private string columnsSimple = "ASSET_NO, COSTBYPARTS, REQ_YEAR";
        private string from = "PARTS_GROUPBYASSETYR_V LL";

        public IndieSearchAssetPartsYR(string dbType, string queryString, int maxRows = 5000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(LL.ASSET_NO) LIKE ('" + query2 + "%'))";

            }



        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY DESCRIPTION " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY LL.REQ_YEAR"};
                }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndieSearchAssetDPREPT : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "   (ROW_NUMBER() OVER (ORDER BY AM.description ASC)) AS ROWNUMID,AM.DESCRIPTION, AM.ASSET_NO, AM.SERIAL_NO,AM.MODEL_NO,  AM.STATUS, AM.CLONE_ID, AM.SUPPLIERNAME, AM.MANUFACTURERNAME,AM.Inservice_date,AM.PURCHASE_COST, AM.DEPRECIATION_PERCENT";
        private string columnsSimple = "ASSET_NO, DESCRIPTION, CATEGORY, MANUFACTURER, SERIAL_NO, MODEL_NO, MODEL_NAME, BUILDING,COST_CENTER, LOCATION, CONDITION,STATUS";
        private string from = "ASSETDP_V AM";

        public IndieSearchAssetDPREPT(string dbType, string queryString, SearchFilter filter, int maxRows = 5000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            if (!string.IsNullOrWhiteSpace(query2))
            {

                whereClause += " AND (LOWER(AM.ASSET_NO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(AM.SERIAL_NO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.MODEL_NO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.SERIAL_NO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.MODEL_NAME) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.BUILDING) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.LOCATION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.CONDITION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.STATUS) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.COSTCENTER_DESC) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(AM.SUPPLIERNAME) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(AM.RESPCENTER_DESC) LIKE ('%" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.Status))
            {
                whereClause += string.Format(" AND AM.STATUS = ('{0}')", filter.Status);

            }

            if (!string.IsNullOrWhiteSpace(filter.CloneId))
            {
                whereClause += string.Format(" AND AM.CLONE_ID = ('{0}')", filter.CloneId);

            }
            if (!string.IsNullOrWhiteSpace(filter.PO_NO))
            {
                whereClause += string.Format(" AND AM.PURCHASE_NO = ('{0}')", filter.PO_NO);

            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += string.Format(" AND LOWER(AM.COSTCENTER_DESC) LIKE LTrim('%" + filter.CostCenterStr + "%')");

            }

            if (!string.IsNullOrWhiteSpace(filter.ManufacturerStr))
            {
                whereClause += string.Format(" AND LOWER(AM.MANUFACTURERNAME) LIKE LTrim('%" + filter.ManufacturerStr + "%')");

            }

            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += string.Format(" AND LOWER(AM.SUPPLIERNAME) LIKE LTrim('%" + filter.SupplierStr + "%')");

            }

            if (!string.IsNullOrWhiteSpace(filter.assetNo))
            {
                whereClause += string.Format(" AND AM.ASSET_NO = ('{0}')", filter.assetNo);

            }
            if (!string.IsNullOrWhiteSpace(filter.AssetFilterCondition))
            {
                whereClause += string.Format(" AND AM.CONDITION = ('{0}')", filter.AssetFilterCondition);

            }
            if (!string.IsNullOrWhiteSpace(filter.staffId))
            {
                whereClause += string.Format(" AND AM.COST_CENTER in (SELECT CODE FROM STAFF_COSTCENTER_BUILDING WHERE UCATEGORY='AIMS_COSTCENTER' AND STAFFCODE ='" + filter.staffId + "')");

            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause }; //" WHERE " + whereClause + +  " ORDER BY  DESCRIPTION ASC"
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ASSET_NO, DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }

}