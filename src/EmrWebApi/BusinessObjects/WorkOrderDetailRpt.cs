﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using EmrWebApi.Models;

namespace EmrWebApi.BusinessObjects
{
    public class WorkOrderDetailRpt
    {
        private PetaPoco.Database db;

        public WorkOrderDetailRpt(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }

        //public List<dynamic> ExampleSearchByAsset(string queryString, SearchFilter filter, int maxRows = 100000)
        //{
        //    var assetListFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieExampleSearchByAsset(db._dbType.ToString(), queryString, filter, maxRows)));

        //    if (assetListFound == null)
        //    {
        //        throw new Exception("No match found.") { Source = this.GetType().Name };
        //    }
        //    return assetListFound;
        //}

        //public List<dynamic> ExampleSearchByWorkOrder(string queryString, SearchFilter filter, int maxRows = 1000)
        //{
        //    var assetListFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieExampleSearchByWorkOrder(db._dbType.ToString(), queryString, filter, maxRows)));

        //    if (assetListFound == null)
        //    {
        //        throw new Exception("No match found.") { Source = this.GetType().Name };
        //    }
        //    return assetListFound;
        //}

        //public List<dynamic> ExampleSearchByTestData(string queryString, SearchFilter filter, int maxRows = 1000)
        //{
        //    var assetListFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieExampleSearchByTestData(db._dbType.ToString(), queryString, filter, maxRows)));

        //    if (assetListFound == null)
        //    {
        //        throw new Exception("No match found.") { Source = this.GetType().Name };
        //    }
        //    return assetListFound;
        //}

        public List<dynamic> ExampleSearchByWorkOrderRpt(string queryString, SearchFilter filter, int maxRows = 1000)
        {
            var assetListFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieExampleSearchByWorkOrderRpt(db._dbType.ToString(), queryString, filter, maxRows)));

            if (assetListFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return assetListFound;
        }

    }
    //public class IndieExampleSearchByAsset : IndieSql
    //{
    //    private int maxRows = 50;
    //    private string queryString = "";
    //    private string whereClause = "1=1 ";
    //    private string columns = "A.ASSET_NO, A.DESCRIPTION, A.COSTCENTER_DESC,A.MANUFACTURER_DESC,A.SUPPLIER_DESC,A.INSERVICE_DATE,PM_TYPE";
    //    private string columnsSimple = "DESCRIPTION,";
    //    private string from = "ASSETINFOREPT_V A";

    //    public IndieExampleSearchByAsset(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
    //    {
    //        this.DatabaseType = dbType;
    //        this.queryString = queryString;
    //        this.maxRows = maxRows;
    //        string[] queryArray = queryString.Split(new char[] { ' ' });

    //        foreach (string query in queryArray)
    //        {
    //            var query2 = query.ToLower();
    //            whereClause += " And (LOWER(A.ASSET_NO) LIKE ('%" + query2 + "%') And LOWER(A.COSTCENTER_DESC) LIKE ('%" + query2 + "%'))";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
    //        {
    //            whereClause += " AND (LOWER(A.COSTCENTER_DESC) LIKE ('%" + filter.CostCenterStr + "%'))";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.assetDescription))
    //        {
    //            whereClause += " AND (LOWER(A.DESCRIPTION) LIKE ('%" + filter.assetDescription + "%'))";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.AssetFilterSuupiler))
    //        {
    //            whereClause += " AND (LOWER(A.SUPPLIER_DESC) LIKE ('%" + filter.AssetFilterSuupiler + "%'))";
    //        }
    //        if (!string.IsNullOrWhiteSpace(filter.pmtype))
    //        {
    //            whereClause += " AND (LOWER(A.PM_TYPE) LIKE ('%" + filter.pmtype + "%'))";
    //        }
    //        if (!string.IsNullOrWhiteSpace(filter.AssetFilterManufacturer))
    //        {
    //            whereClause += " AND (LOWER(A.MANUFACTURER_DESC) LIKE ('%" + filter.AssetFilterManufacturer + "%'))";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.FromDate))
    //        {
    //            var whereDate = string.Format("convert(date,A.INSERVICE_DATE) >= '{0}'", filter.FromDate);
    //            if (!string.IsNullOrWhiteSpace(filter.ToDate))
    //            {
    //                whereDate += string.Format(" AND convert(date,A.INSERVICE_DATE) <= '{0}'", filter.ToDate.Substring(0, 10));
    //            }
    //            whereClause += " AND (" + whereDate + ")";
    //        }
    //    }

    //    public override StmtOracle OracleSql
    //    {
    //        get
    //        {
    //            return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
    //        }
    //    }

    //    public override StmtSqlServer SqlServerSql
    //    {
    //        get
    //        {
    //            return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY A.ASSET_NO" };
    //        }
    //    }

    //    public override StmtMySql MySql
    //    {
    //        get
    //        {
    //            return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
    //        }
    //    }
    //}

    //public class IndieExampleSearchByWorkOrder : IndieSql
    //{
    //    private int maxRows = 50;
    //    private string queryString = "";
    //    private string whereClause = "1=1 ";
    //    private string columns = "A.ASSET_NO,A.REQ_NO,A.ReqDate, A.description, A.COSTCENTERNAME,A.SUPPIERNAME,A.WO_STATUS";
    //    private string columnsSimple = "DESCRIPTION,";
    //    private string from = "WORKORDERS_V A";

    //    public IndieExampleSearchByWorkOrder(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
    //    {
    //        this.DatabaseType = dbType;
    //        this.queryString = queryString;
    //        this.maxRows = maxRows;
    //        string[] queryArray = queryString.Split(new char[] { ' ' });

    //        foreach (string query in queryArray)
    //        {
    //            var query2 = query.ToLower();
    //            whereClause += " And (LOWER(A.ASSET_NO) LIKE ('%" + query2 + "%') And LOWER(A.COSTCENTERNAME) LIKE ('%" + query2 + "%')) And (LOWER(A.REQ_NO) LIKE ('%" + query2 + "%') And LOWER(A.description) LIKE ('%" + query2 + "%'))";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
    //        {
    //            whereClause += " AND (LOWER(A.COSTCENTERNAME) LIKE ('%" + filter.CostCenterStr + "%'))";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.assetDescription))
    //        {
    //            whereClause += " AND (LOWER(A.description) LIKE ('%" + filter.assetDescription + "%'))";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.AssetFilterSuupiler))
    //        {
    //            whereClause += " AND (LOWER(A.SUPPIERNAME) LIKE ('%" + filter.AssetFilterSuupiler + "%'))";
    //        }
    //        if (!string.IsNullOrWhiteSpace(filter.FromDate))
    //        {
    //            var whereDate = string.Format("convert(date,A.Req_date) >= '{0}'", filter.FromDate);
    //            if (!string.IsNullOrWhiteSpace(filter.ToDate))
    //            {
    //                whereDate += string.Format(" AND convert(date,A.Req_date) <= '{0}'", filter.ToDate.Substring(0, 10));
    //            }
    //            whereClause += " AND (" + whereDate + ")";
    //        }
    //    }

    //    public override StmtOracle OracleSql
    //    {
    //        get
    //        {
    //            return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
    //        }
    //    }

    //    public override StmtSqlServer SqlServerSql
    //    {
    //        get
    //        {
    //            return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY A.ReqDate desc" };
    //        }
    //    }

    //    public override StmtMySql MySql
    //    {
    //        get
    //        {
    //            return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
    //        }
    //    }
    //}

    //public class IndieExampleSearchByTestData : IndieSql
    //{
    //    private int maxRows = 50;
    //    private string queryString = "";
    //    private string whereClause = "1=1 ";
    //    private string columns = "A.Code, A.Description, A.Supplier,A.MANUFACTURER_DESC";
    //    private string columnsSimple = "DESCRIPTION,";
    //    private string from = "TestTableView A";

    //    public IndieExampleSearchByTestData(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
    //    {
    //        this.DatabaseType = dbType;
    //        this.queryString = queryString;
    //        this.maxRows = maxRows;
    //        string[] queryArray = queryString.Split(new char[] { ' ' });

    //        foreach (string query in queryArray)
    //        {
    //            var query2 = query.ToLower();
    //            whereClause += " And (LOWER(A.Code) LIKE ('%" + query2 + "%') And LOWER(A.Description) LIKE ('%" + query2 + "%')) ";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.AssetFilterSuupiler))
    //        {
    //            whereClause += " AND (LOWER(A.Supplier) LIKE ('%" + filter.AssetFilterSuupiler + "%'))";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.AssetFilterManufacturer))
    //        {
    //            whereClause += " AND (LOWER(A.MANUFACTURER_DESC) LIKE ('%" + filter.AssetFilterManufacturer + "%'))";
    //        }
    //    }

    //    public override StmtOracle OracleSql
    //    {
    //        get
    //        {
    //            return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
    //        }
    //    }

    //    public override StmtSqlServer SqlServerSql
    //    {
    //        get
    //        {
    //            return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY A.Code" };
    //        }
    //    }

    //    public override StmtMySql MySql
    //    {
    //        get
    //        {
    //            return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
    //        }
    //    }
    //}

    public class IndieExampleSearchByWorkOrderRpt: IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1";
        private string columns = "A.req_no, A.asset_no,A.Description,A.model_no,A.ReqDate,A.JobDueDate,A.CostCenter,A.manufacturer,A.Supplier,A.Current_Agent,A.wo_status,A.PROBLEM_TYPEDESC,A.USER_ID,A.serial_no,A.PM_TYPE,A.ClosedDate"; //,A.model_name
        private string columnsSimple = "DESCRIPTION,";
        private string from = "WorkOrderForReport_V A";

        public IndieExampleSearchByWorkOrderRpt(string dbType, string queryString, SearchFilter filter, int maxRows = 2000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " And (LOWER(A.req_no) LIKE ('%" + query2 + "%') and (LOWER(A.Description) LIKE LOWER('%" + query2 + "%')" + " and LOWER(A.asset_no) LIKE Lower('%" + query2 + "%')";
                whereClause += " and LOWER(A.serial_no) LIKE ('%" + query2 + "%') and ISNULL(LOWER(A.model_no), '') LIKE LOWER('%%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.AssetFilterSuupiler))
            {
                whereClause += " and (LOWER(A.Supplier) LIKE ('%" + filter.AssetFilterSuupiler + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.AssetFilterManufacturer))
            {
                whereClause += " and (LOWER(A.manufacturer) LIKE ('%" + filter.AssetFilterManufacturer + "%'))";
            }
            if(!string.IsNullOrWhiteSpace(filter.Status))
            {
                if (filter.Status== "PSOP") { whereClause += " and (LOWER(A.wo_status) in (Lower('op'),LOWER('ne')))"; } else { 
                whereClause += " And LOWER(A.wo_status) = '" + filter.Status.ToLower() + "'";
                }
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " and LOWER(A.CostCenter) LIKE ('%" + filter.CostCenterStr.ToLower() + "%')";
            }

            if (!string.IsNullOrWhiteSpace(filter.ProblemType))
            {
                whereClause += " And Lower(A.PROBLEM_TYPEDESC)=Lower('" +  filter.ProblemType + "')";
            }
            if (!string.IsNullOrWhiteSpace(filter.pmtype))
            {
                whereClause += " And LOWER(A.PM_TYPE) = Lower('" + filter.pmtype + "')";
            }

            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("Convert(Date,A.Req_date) >=Convert(Date,'{0}'", filter.FromDate) + ")";
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" AND Convert(Date,A.Req_date) <=Convert(Date,'{0}'", filter.ToDate.Substring(0, 10));
                }
                whereClause += ") AND (" + whereDate + ")";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                // + " ORDER BY A.Req_No" 
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + ")" + " ORDER BY cast(A.req_no as int)" };
                //return new StmtSqlServer() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + ")" + " ORDER BY A.Req_No" };

            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }
}
