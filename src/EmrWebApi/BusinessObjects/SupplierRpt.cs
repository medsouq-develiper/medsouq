﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using EmrWebApi.Models;

namespace EmrWebApi.BusinessObjects
{
    public class SupplierRpt
    {
        private PetaPoco.Database db;

        public SupplierRpt(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }

        public List<dynamic> ExampleSearchBySupplierRpt(string queryString, SearchFilter filter, int maxRows = 1000)
        {
            var assetListFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieExampleSearchBySupplierRpt(db._dbType.ToString(), queryString, filter, maxRows)));

            if (assetListFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return assetListFound;
        }

    }

    public class IndieExampleSearchBySupplierRpt: IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1";
        private string columns = "A.supplier_id, A.description, A.address, A.city, A.state, A.zip, A.contact,A.contact_no2 , A.email_address,A.additional_info";
        private string columnsSimple = "DESCRIPTION,";
        private string from = "SupplierForRpt_V A";

        public IndieExampleSearchBySupplierRpt(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " And (LOWER(A.supplier_id) LIKE ('%" + query2 + "%') Or LOWER(A.Description) LIKE LOWER('%" + query2 + "%')" + " Or LOWER(A.address)  LIKE Lower('%" + query2 + "%')";
                whereClause += " Or LOWER(A.contact) LIKE ('%" + query2 + "%') or LOWER(A.contact_no2) LIKE ('%" + query2 + "%') Or LOWER(A.email_address) LIKE LOWER('%" + query2 + "%')" + " Or LOWER(A.city) LIKE Lower('%" + query2 + "%')";
                whereClause += " Or LOWER(A.state) LIKE ('%" + query2 + "%') Or LOWER(A.additional_info) LIKE ('%" + query2 + "%'))";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY A.supplier_id" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }
}
