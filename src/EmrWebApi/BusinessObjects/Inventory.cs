﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;

using EmrSys;
using EmrWebApi.Models;

namespace EmrWebApi.BusinessObjects
{
    class Inventory
    {
        #region CLASS
        private PetaPoco.Database db = null;

        public Inventory(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }

        public void Dispose()
        {
            db.Dispose();
        }
        #endregion
        //Stock Request/Transfer
        public List<AM_STOCKTRANSACTION> CreateNewStockTransaction(List<AM_STOCKTRANSACTION> requestList, AM_STOCKTRANSACTION_MASTER amStocktransactionMaster, string userId, string createDate, string astionStr)
        {
            using (var transScope = db.GetTransaction())
            {

                DateTime createDt = DateTime.Parse(createDate);

                var reqId = requestList.Find(item => !String.IsNullOrWhiteSpace(item.TRANS_ID));
                SEQ seq = null;
                var seqId = "";
                if (reqId == null)
                {
                    db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='STOCKTRANSACTION'");

                    seq = db.SingleOrDefault<SEQ>(PetaPoco.Sql.Builder.Select("*").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "STOCKTRANSACTION" }));
                    seqId = seq.SEQ_VAL.ToString();
                }
                else
                {
                    seqId = reqId.TRANS_ID;
                    db.Delete<AM_STOCKTRANSACTION>(" WHERE TRANS_ID ='" + seqId + "'");

                    if(requestList[0].TRANS_STATUS == "TRANSFER")
                    {
                        foreach (AM_STOCKTRANSACTION i in requestList)
                        {

                            var committedQty = db.SingleOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@prodId", new { prodId = i.ID_PARTS }).Where("STORE=@storeId", new { storeId = i.STORE_FROM }));
                            if (committedQty != null)
                            {
                                var currCommitted = committedQty.COMMITTED_QTY;
                                var toUpdateqty = currCommitted + i.QTY_TRANSFER;
                                db.Execute("UPDATE PRODUCT_STOCKONHAND SET COMMITTED_QTY=" + toUpdateqty + " WHERE PRODUCT_CODE='" + i.ID_PARTS + "' AND STORE='" + i.STORE_FROM + "'");
                            }
                        }
                        
                    }else if (requestList[0].TRANS_STATUS == "RECEIVED" || requestList[0].TRANS_STATUS == "PARTIAL RECEIVED")
                    {
                        foreach (AM_STOCKTRANSACTION i in requestList)
                        {

                            var currQty = db.SingleOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@prodId", new { prodId = i.ID_PARTS }).Where("STORE=@storeId", new { storeId = i.STORE_TO }));
                            if (currQty != null)
                            {
                                var trQty = currQty.QTY;
                                var toUpdateqty = trQty + i.QTY_TRANSFER;
                                db.Execute("UPDATE PRODUCT_STOCKONHAND SET QTY=" + toUpdateqty + " WHERE PRODUCT_CODE='" + i.ID_PARTS + "' AND STORE='" + i.STORE_TO + "'");
                            }else
                            {
                                PRODUCT_STOCKONHAND stockOnHand = new PRODUCT_STOCKONHAND();

                                stockOnHand.PRODUCT_CODE = i.ID_PARTS;
                                stockOnHand.STORE = i.STORE_TO.ToString();
                                stockOnHand.QTY = i.QTY_TRANSFER;
                                stockOnHand.MIN = 0;
                                stockOnHand.MAX = 0;
                                stockOnHand.COMMITTED_QTY = 0;
                                stockOnHand.PR_QTY = 0;
                                stockOnHand.STATUS = "Y";
                                db.Insert(stockOnHand);
                            }


                            //Update QTY and COMMITTED
                            var storeFromQty = db.SingleOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@prodId", new { prodId = i.ID_PARTS }).Where("STORE=@storeId", new { storeId = i.STORE_FROM }));
                            if (storeFromQty != null)
                            {
                                var currCommitted = storeFromQty.COMMITTED_QTY;
                                var toUpdateCommitted = currCommitted - i.QTY_TRANSFER;
                                var currQTY = storeFromQty.QTY;
                                var toUpdateQty = currQTY - i.QTY_TRANSFER;
                                db.Execute("UPDATE PRODUCT_STOCKONHAND SET COMMITTED_QTY=" + toUpdateCommitted + ",QTY ="+ toUpdateQty+ "  WHERE PRODUCT_CODE='" + i.ID_PARTS + "' AND STORE='" + i.STORE_FROM + "'");
                            }
                        }

						DateTime cToday = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");

                        List<ITEM_BATCHES_V> receiveBatches = new List<ITEM_BATCHES_V>();
                        receiveBatches = db.Fetch<ITEM_BATCHES_V>(PetaPoco.Sql.Builder.Select("*").From("ITEM_BATCHES_V").Where("TRANS_ID=@requestId", new { requestId = reqId.TRANS_ID }));



                        foreach (ITEM_BATCHES_V i in receiveBatches)
                        {

                            if (i.STATUS == "TRANSFER")
                            {
                                SYSTEM_TRANSACTIONDETAILS systemTrans = new SYSTEM_TRANSACTIONDETAILS();

                                systemTrans = db.SingleOrDefault<SYSTEM_TRANSACTIONDETAILS>(PetaPoco.Sql.Builder.Select("*").From("SYSTEM_TRANSACTIONDETAILS").Where("REFTRANSID=@batchId", new { batchId = i.BATCH_ID }).Where("REFCODE=@prodId", new { prodId = i.PRODUCT_CODE }));

                                if (systemTrans != null)
                                {
                                    systemTrans.MESSAGE = i.STORE_DESC + " purchased " + i.STOCK_ONHAND + " " + i.UO_DESC + " of " + i.PRODUCT_CODE + " " + i.PRODUCT_NAME + " with batch number (" + i.BARCODE_ID + ") and invoice number " + (i.INVOICENO) + " on " + String.Format("{0:dd/MM/yyyy}", cToday) + ".";
                                    systemTrans.COSTCENTERMESSAGE = "New batch number (" + i.BARCODE_ID + ") was received on " + i.CREATED_DATE;
                                    systemTrans.REASONTYPE = "Purchased";
                                    systemTrans.REFQTY = i.STOCK_ONHAND;
                                    db.Update(systemTrans, new string[] { "MESSAGE", "COSTCENTERMESSAGE", "REASONTYPE", "REFQTY" });

                                }
                                else
                                {
                                    var systemTransactionDetails = new SYSTEM_TRANSACTIONDETAILS()
                                    {																								   

                                        REFTRANSID = i.BATCH_ID,
                                        REFCODE = i.PRODUCT_CODE,
                                        REFDESCRIPTION = i.PRODUCT_NAME,
                                        MESSAGE = i.STORE_DESC + " purchased " + i.STOCK_ONHAND + " " + i.UO_DESC + " of " + i.PRODUCT_CODE + " " + i.PRODUCT_NAME + " with batch number (" + i.BARCODE_ID + ") and invoice number " + (i.INVOICENO) + " on " + String.Format("{0:dd/MM/yyyy}", cToday) + ".",
                                        COSTCENTERMESSAGE = "New batch number (" + i.BARCODE_ID + ") was received on " + i.CREATED_DATE,
                                        STATUS = "Y",
                                        REFEXPIRY = i.EXPIRY_DATE,
                                        REFBATCHSERIALNO = i.BARCODE_ID,
                                        REFINVOICENO = i.INVOICENO,
                                        REFCOST = 0,
                                        REFQTY = i.STOCK_ONHAND,
                                        REFUOM = i.UO_DESC,
                                        REFCOSTCENTERCODE = i.CLIENT_CODE,
                                        REFCOSTCENTERDESCRIPTION = i.CLIENT_NAME,
                                        REASONTYPE = "Purchased",
                                        REFTYPE = i.ITEMTYPE,
                                        CREATEDBY = userId,
                                        CREATEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time")
                                    };
                                    db.Insert(systemTransactionDetails);

                    }

                }

                            db.Execute("UPDATE PRODUCT_STOCKBATCH SET STATUS='RECEIVED' WHERE TRANS_ID ='" + reqId.TRANS_ID + "' AND BATCH_ID = '" + i.BATCH_ID + "'");
                        }
                    }
                }

                if (String.IsNullOrWhiteSpace(amStocktransactionMaster.TRANS_ID))
                {
                    amStocktransactionMaster.TRANS_ID = seqId;
                    if (amStocktransactionMaster.TRANS_STATUS == "POSTED")
                    {
                        //i.REQUEST_DATE = createDt;
                        amStocktransactionMaster.REQUEST_BY = userId;
                    }
                    if (amStocktransactionMaster.TRANS_STATUS == "TRANSFER")
                    {
                        amStocktransactionMaster.DELIVER_DATE = createDt;
                        amStocktransactionMaster.DELIVER_BY = userId;
                    }
                    if (amStocktransactionMaster.TRANS_STATUS == "RECEIVED")
                    {
                        amStocktransactionMaster.RECEIVE_DATE = createDt;
                        amStocktransactionMaster.RECEIVE_BY = userId;
                    }
                    db.Insert(amStocktransactionMaster);
                }
                else
                {
                    if (amStocktransactionMaster.TRANS_STATUS == "POSTED")
                    {
                        //i.REQUEST_DATE = createDt;
                        amStocktransactionMaster.REQUEST_BY = userId;
                    }
                    if (amStocktransactionMaster.TRANS_STATUS == "TRANSFER")
                    {
                        amStocktransactionMaster.DELIVER_DATE = createDt;
                        amStocktransactionMaster.DELIVER_BY = userId;
                    }
                    if (amStocktransactionMaster.TRANS_STATUS == "RECEIVED")
                    {
                        amStocktransactionMaster.RECEIVE_DATE = createDt;
                        amStocktransactionMaster.RECEIVE_BY = userId;
                    }
                    db.Update(amStocktransactionMaster);
                }

                foreach (AM_STOCKTRANSACTION i in requestList)
                {
                    i.TRANS_ID = seqId;
                    if (i.TRANS_STATUS == "POSTED")
                    {
                        //i.REQUEST_DATE = createDt;
                        i.REQUEST_BY = userId;
                    }
                    if (i.TRANS_STATUS == "TRANSFER")
                    {
                        i.DELIVER_DATE = createDt;
                        i.DELIVER_BY = userId;
                    }
                    if (i.TRANS_STATUS == "RECEIVED")
                    {
                        i.RECEIVE_DATE = createDt;
                        i.RECEIVE_BY = userId;
                    }
                    db.Insert(i);
                }
                transScope.Complete();
            }
            return requestList;
        }
        public int UpdateStockReqStatus(string requestID, string prStatus, string reqIDLine, string userId)
        {
            var arabDateTimeNow = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");

            using (var transScope = db.GetTransaction())
            {

                //preprocess data
                if(prStatus == "RECEIVED")
                {
                    List<ITEM_BATCHES_V> receiveBatches = new List<ITEM_BATCHES_V>();
                    receiveBatches = db.Fetch<ITEM_BATCHES_V>(PetaPoco.Sql.Builder.Select("*").From("ITEM_BATCHES_V").Where("TRANS_ID=@requestId", new { requestId = requestID }));

                    foreach (ITEM_BATCHES_V i in receiveBatches)
                    {
                        var systemTransactionDetails = new SYSTEM_TRANSACTIONDETAILS()
                        {


                            REFTRANSID = requestID,
                            REFCODE = i.PRODUCT_CODE,
                            REFDESCRIPTION = i.PRODUCT_NAME,
                            MESSAGE = i.STORE_DESC + " purchased " + i.PRODUCT_CODE + " " + i.PRODUCT_NAME + " with batch number (" + i.BARCODE_ID + ") on " + i.CREATED_DATE,
                            COSTCENTERMESSAGE = "New batch number (" + i.BARCODE_ID + ") was received on " + i.CREATED_DATE,
                            STATUS = "Y",
                            REFEXPIRY = i.EXPIRY_DATE,
                            REFBATCHSERIALNO = i.BARCODE_ID,
                            REFINVOICENO = i.INVOICENO,
                            REFCOST = 0,
                            REFQTY = i.STOCK_ONHAND,
                            REFUOM = i.UO_DESC,
                            REFCOSTCENTERCODE = i.CLIENT_CODE,
                            REFCOSTCENTERDESCRIPTION = i.CLIENT_NAME,
                            REASONTYPE = "Purchased",
                            REFTYPE = i.ITEMTYPE,
                            CREATEDBY = userId,
                            CREATEDDATE = arabDateTimeNow

                        };
                        db.Insert(systemTransactionDetails);
                        db.Execute("UPDATE PRODUCT_STOCKBATCH SET STATUS='" + prStatus + "' WHERE TRANS_ID ='" + requestID + "' AND BATCH_ID = '" + i.BATCH_ID + "'");
                    }

                }

                //apply processed data
                db.Execute("UPDATE AM_STOCKTRANSACTION SET TRANS_STATUS='" + prStatus + "' WHERE TRANS_ID ='" + requestID + "'");
                db.Execute("UPDATE AM_STOCKTRANSACTION_MASTER SET TRANS_STATUS='" + prStatus + "' WHERE TRANS_ID ='" + requestID + "'");

                transScope.Complete();
            }

            return 0;
        }
        public List<dynamic> readStockReq(string queryString, int maxRows = 2000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieReadStockReq(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public dynamic readStocktransactionMaster(string TRANS_ID)
        {
            var itemPOFound = db.SingleOrDefault<dynamic>(PetaPoco.Sql.Builder.Append("SELECT " +
                "ST.AM_STOCKTRANSACTION_MASTERID, ST.TRANS_ID, ST.TRANS_TYPE, ST.STORE_FROM, ST.STORE_TO, ST.TRANS_STATUS, ST.DELIVER_BY, ST.DELIVER_DATE, ST.REQUEST_BY, ST.REQUEST_DATE, ST.RECEIVE_BY, ST.RECEIVE_DATE, ST.TRANSDETFLAG, ST.TRANSFER_REMARKS, ST.RECEIVE_REMARKS, " +
                "ST.REMARKS, ST.FOREIGN_CURRENCY, ST.SHIP_VIA, ST.TOTAL_AMOUNT, ST.TOTAL_NOITEMS, ST.SUPPLIER_ID, ST.SUPPLIER_ADDRESS1, ST.SUPPLIER_CITY, ST.SUPPLIER_STATE, ST.SUPPLIER_ZIPCODE, ST.SUPPLIER_CONTACT_NO1," +
                "ST.SUPPLIER_CONTACT_NO2, ST.SUPPLIER_CONTACT, ST.SUPPLIER_EMAIL_ADDRESS, ST.OTHER_INFO, ST.SHIP_DATE, ST.EST_SHIP_DATE, ST.HANDLING_FEE, ST.TRACKING_NO, ST.EST_FREIGHT, ST.FREIGHT, ST.FREIGHT_PAIDBY," +
                "ST.FREIGHT_CHARGETO, ST.TAXABLE, ST.TAX_CODE, ST.TAX_AMOUNT, ST.DISCOUNT, ST.FREIGHT_POLINE, ST.FREIGHT_SEPARATE_INV, ST.INSURANCE_VALUE, ST.PO_BILLCODE_ADDRESS, ST.PURCHASE_NO, ST.POINVOICENO, " +
                "CC.DESCRIPTION AS SUPPLIERDESC FROM AM_STOCKTRANSACTION_MASTER ST LEFT JOIN COSTCENTER_DETAILS CC ON CC.CODEID = ST.SUPPLIER_ID WHERE ST.TRANS_ID = " + TRANS_ID + ""));

            if (itemPOFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return itemPOFound;
        }
        public List<dynamic> SearchStockTransaction(string queryString, string reqAction, string transStatus, string queryPage, SearchFilter filter, int maxRows = 5000)
        {

            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStockTransaction(db._dbType.ToString(), queryString, reqAction, transStatus, queryPage, filter, maxRows)));
            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        //Purchase Received Return
        public PRODUCT_BATCH_V UpdatePurchaseOrder(PRODUCT_BATCH_V poDetails)
        {
            if (poDetails != null)
            {
                if (poDetails.PURCHASE_ID == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }
            using (var transScope = db.GetTransaction())
            {
                var itemPOFound = db.SingleOrDefault<AM_PURCHASEORDER_ITEMS>(PetaPoco.Sql.Builder.Select("*").From("AM_PURCHASEORDER_ITEMS").Where("ID_PARTS=@productId", new { productId = poDetails.ID_PARTS }).Where("PURCHASE_NO=@reqId", new { reqId = poDetails.PURCHASE_NO }));
                if (itemPOFound != null)
                {
                    var returnQty = itemPOFound.QTY_RETURN;
                    var newreturnQty = poDetails.QTY_RETURN - returnQty;
                    if(newreturnQty > 0)
                    {
                        var itemStockFound = db.SingleOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@productId", new { productId = poDetails.ID_PARTS }).Where("STORE=@storeId", new { storeId = poDetails.STOREID }));
                        if (itemStockFound != null)
                        {
                            var newQty = itemStockFound.QTY - decimal.ToInt32(newreturnQty);
                            db.Execute("UPDATE PRODUCT_STOCKONHAND SET  QTY = " + newQty + " WHERE PRODUCT_CODE ='" + poDetails.ID_PARTS + "' AND STORE = '" + poDetails.STOREID + "'");
                        }
                    }          
                }
                    db.Execute("UPDATE AM_PURCHASEORDER_ITEMS SET QTY_RETURN='"+ poDetails.QTY_RETURN+ "',RETURN_DATE ='" + poDetails.RETURN_DATE + "' ,RETURN_AMEND_DATE ='" + poDetails.RETURN_DATE + "',RETURN_REASON ='" + poDetails.RETURN_REASON + "',RETURN_BY ='" + poDetails.RETURN_BY + "' WHERE PURCHASE_NO='" + poDetails.PURCHASE_NO + "' AND ID_PARTS='"+ poDetails.ID_PARTS+"'");
             
                transScope.Complete();
            }

            return poDetails;
        }

        //Purchase Information
        public PURCHASEORDER_V ReadPurchaseInfo(string purchaseID)
        {
            PURCHASEORDER_V ret = null;

            var purchaseFound = db.SingleOrDefault<PURCHASEORDER_V>(PetaPoco.Sql.Builder.Select("*").From("PURCHASEORDER_V").Where("PURCHASE_NO=@puchaseId", new { puchaseId = purchaseID }));

            if (purchaseFound != null)
            {
                ret = purchaseFound;
            }
            else
            {
                throw new Exception("Purchase not found.") { Source = this.GetType().Name };
            }
            return ret;
        }

        //Work Order Labour Materials
        public List<dynamic> UpdatePartRequestForCollection(AM_PARTSREQUEST partsRequestDetail)
        {
           
            //validate data
            if (partsRequestDetail != null)
            {
                if (partsRequestDetail.ID_PARTS == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //check if there are request for collection
                var forCollection = 0;

                var partsForCollectionFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieReadWOLMaterialsCollect(db._dbType.ToString(), partsRequestDetail.ID_PARTS, 100)));
                if(partsForCollectionFound != null)
                {
                    foreach (dynamic i in partsForCollectionFound)
                    {
                        forCollection = forCollection + i.FOR_COLLECTION_QTY;

                    }
                }
               
                var currQty = db.SingleOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@prodId", new { prodId = partsRequestDetail.ID_PARTS }).Where("STORE=@storeId", new { storeId = partsRequestDetail.STORE_ID }));
                if (currQty != null)
                {
                    forCollection = forCollection + partsRequestDetail.FOR_COLLECTION_QTY;
                    if (forCollection > currQty.QTY)
                    {
                        return partsForCollectionFound;
                    }


                    var trQty = currQty.COMMITTED_WOQTY;
                    var toUpdateqty = trQty + partsRequestDetail.FOR_COLLECTION_QTY;
                 
                    db.Execute("UPDATE PRODUCT_STOCKONHAND SET COMMITTED_WOQTY=" + toUpdateqty + " WHERE PRODUCT_CODE='" + partsRequestDetail.ID_PARTS + "' AND STORE='" + partsRequestDetail.STORE_ID + "'");
                }
                partsRequestDetail.TRANSFER_FLAG = "F";

                if (partsRequestDetail.QTY == partsRequestDetail.FOR_COLLECTION_QTY)
                {
                    partsRequestDetail.STATUS = "For Collection QTY " + partsRequestDetail.FOR_COLLECTION_QTY;
                }
                else
                {

                    var dataExist = db.SingleOrDefault<AM_PARTSREQUEST>(PetaPoco.Sql.Builder.Select("*").From("AM_PARTSREQUEST").Where("ID_PARTS=@prodId", new { prodId = partsRequestDetail.ID_PARTS }).Where("REF_WO=@refWoId", new { refWoId = partsRequestDetail.REF_WO }));
                    if(dataExist != null)
                    {
                        var pendingQty = partsRequestDetail.QTY - (partsRequestDetail.FOR_COLLECTION_QTY + dataExist.FOR_COLLECTION_QTY + dataExist.TRANSFER_QTY);
                        partsRequestDetail.STATUS = "For Collection QTY " + partsRequestDetail.FOR_COLLECTION_QTY + " Pending Qty " + pendingQty;
                    }
                    
                }
                
                //preprocess data
                //apply processed data
                db.Update(partsRequestDetail, new string[] { "FOR_COLLECTION_QTY", "TRANSFER_FLAG", "STATUS"});
                db.Execute("UPDATE AM_WORK_ORDERS SET MATERIALS_REMARKS='Ready for Collection', PARTS_CONFIRMED = 'N' WHERE REQ_NO=" + partsRequestDetail.REF_WO);
                transScope.Complete();
            }

            return null;
        }
        public int UpdatePartRequestDispatch(AM_PARTSREQUEST partsRequestDetail)
        {
            //validate data
            if (partsRequestDetail != null)
            {
                if (partsRequestDetail.ID_PARTS == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                var currQty = db.SingleOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@prodId", new { prodId = partsRequestDetail.ID_PARTS }).Where("STORE=@storeId", new { storeId = partsRequestDetail.STORE_ID }));
                if (currQty != null)
                {
                    var trQty = currQty.QTY;
                    var toUpdateqty = trQty - partsRequestDetail.FOR_COLLECTION_QTY;

                    db.Execute("UPDATE PRODUCT_STOCKONHAND SET QTY=" + toUpdateqty + " WHERE PRODUCT_CODE='" + partsRequestDetail.ID_PARTS + "' AND STORE='" + partsRequestDetail.STORE_ID + "'");
                }
                partsRequestDetail.TRANSFER_FLAG = "D";
                partsRequestDetail.TRANSFER_QTY = partsRequestDetail.TRANSFER_QTY + partsRequestDetail.FOR_COLLECTION_QTY;


                if (partsRequestDetail.QTY == partsRequestDetail.TRANSFER_QTY)
                {
                    partsRequestDetail.STATUS = "Dispatch";
                }
                else
                {
                    //var dataExist = db.SingleOrDefault<AM_PARTSREQUEST>(PetaPoco.Sql.Builder.Select("*").From("AM_PARTSREQUEST").Where("ID_PARTS=@prodId", new { prodId = partsRequestDetail.ID_PARTS }).Where("REF_WO=@refWoId", new { refWoId = partsRequestDetail.REF_WO }));
                    //if(dataExist != null)
                    //{
                        var pendingQty = partsRequestDetail.QTY - partsRequestDetail.TRANSFER_QTY;
                   // }

                    

                    partsRequestDetail.STATUS = "Dispatch QTY "+ partsRequestDetail.TRANSFER_QTY + " Pending Qty " + pendingQty;
                }
                partsRequestDetail.FOR_COLLECTION_QTY = 0;

                //preprocess data
                //apply processed data
                db.Update(partsRequestDetail, new string[] { "TRANSFER_QTY", "TRANSFER_FLAG", "STATUS", "DISPATCH_BY", "DISPATCH_DATE","FOR_COLLECTION_QTY" });
                
                db.Execute("UPDATE AM_WORK_ORDERS SET MATERIALS_REMARKS='Dispatched' WHERE REQ_NO=" + partsRequestDetail.REF_WO);
                transScope.Complete();
            }

            return 0;
        }


        public List<AM_PARTSREQUEST> CreateWOLMaterial(List<AM_PARTSREQUEST> requestList, string userId, string createDate, string astionStr)
        {
            using (var transScope = db.GetTransaction())
            {

                DateTime createDt = DateTime.Parse(createDate);

                var reqId = requestList.Find(item => item.REF_WO != null && item.REF_WO != "");
             
                if (reqId != null)
                
                {
                    db.Delete<AM_PARTSREQUEST>(" WHERE REF_WO ='" + reqId.REF_WO + "' AND STATUS='Request'");
                    if (astionStr == "CONFIRM")
                    {
                        db.Execute("UPDATE AM_WORK_ORDERS SET PARTS_CONFIRMED='Y', WO_STATUS ='FC' WHERE REQ_NO=" + reqId.REF_WO);
                    }
                    else
                    {
                        db.Execute("UPDATE AM_WORK_ORDERS SET PARTS_CONFIRMED='N', WO_STATUS ='OP'  WHERE REQ_NO=" + reqId.REF_WO);
                    }

                    if (astionStr == "DELETE")
                    {
                        return requestList;
                    }

                }
                var cci = 0;
                var existoldRecord = 0;
                foreach (AM_PARTSREQUEST i in requestList)
                {
                    var dataExist = db.SingleOrDefault<AM_PARTSREQUEST>(PetaPoco.Sql.Builder.Select("*").From("AM_PARTSREQUEST").Where("ID_PARTS=@prodId", new { prodId = i.ID_PARTS }).Where("REF_WO=@refWoId", new { refWoId = i.REF_WO }));
                   
                    if (dataExist != null)
                    {
                        existoldRecord = 1;

                        if (astionStr == "RECEIVED" || astionStr == "CONFIRM")
                        {
                            //if(dataExist.STATUS == "Dispatch")
                            //{
                            if(i.TRANSFER_QTY > 0 && i.TRANSFER_FLAG != "F")
                            {
                                dataExist.STATUS = "Received";
                            }
                            
                            //}
                            if (i.TRANSFER_QTY > 0)
                            {
                                if (i.USED_QTY > 0)
                                {

                                    var qtyreturn = dataExist.TRANSFER_QTY - i.USED_QTY;

                                    if (qtyreturn > dataExist.QTY_FORRETURN && dataExist.TRANSFER_QTY > 0)
                                    {
                                        //i.QTY_FORRETURN = i.TRANSFER_QTY - i.QTY;
                                        dataExist.QTY_FORRETURN = (i.TRANSFER_QTY - (i.USED_QTY + i.QTY_RETURN));
                                        if(dataExist.QTY_FORRETURN > 0 && astionStr == "CONFIRM")
                                        {
                                            if (cci == 0)
                                            {
                                                var statusText = "Qty for Return";
                                                db.Execute("UPDATE AM_WORK_ORDERS SET MATERIALS_REMARKS='" + statusText + "' WHERE REQ_NO=" + i.REF_WO);
                                                cci++;
                                            }
                                        }
                                        else
                                        {
                                            if (cci == 0)
                                            {
                                                var statusText = "Dispatched";
                                                db.Execute("UPDATE AM_WORK_ORDERS SET MATERIALS_REMARKS='" + statusText + "' WHERE REQ_NO=" + i.REF_WO);

                                            }
                                        }

                                    }

                                    if(dataExist.TRANSFER_QTY == i.USED_QTY)
                                    {
                                        dataExist.QTY_FORRETURN = 0;
                                    }
                                }else if(i.USED_QTY == 0)
                                {
                                   
                                    if(i.TRANSFER_QTY > i.QTY_RETURN)
                                    {

                                        dataExist.QTY_FORRETURN = (i.TRANSFER_QTY - i.QTY_RETURN);
                                        if(dataExist.QTY_FORRETURN > 0 && astionStr == "CONFIRM")
                                        {
                                            if (cci == 0)
                                            {
                                                var statusText = "Qty for Return";
                                                db.Execute("UPDATE AM_WORK_ORDERS SET MATERIALS_REMARKS='" + statusText + "' WHERE REQ_NO=" + i.REF_WO);
                                                cci++;
                                            }
                                        }
                                        else
                                        {
                                            if (cci == 0)
                                            {
                                                var statusText = "Dispatched";
                                                db.Execute("UPDATE AM_WORK_ORDERS SET MATERIALS_REMARKS='" + statusText + "' WHERE REQ_NO=" + i.REF_WO);
                                          
                                            }
                                        }
                                    }
                                    else if (i.TRANSFER_QTY > 0)
                                    {
                                        if (cci == 0)
                                        {
                                            var statusText = "Dispatched";
                                            db.Execute("UPDATE AM_WORK_ORDERS SET MATERIALS_REMARKS='" + statusText + "' WHERE REQ_NO=" + i.REF_WO);

                                        }
                                    }
                                }   
                            }
                            dataExist.USED_QTY = i.USED_QTY;
                            if(astionStr == "CONFIRM")
                            {
                                if (dataExist.QTY_FORRETURN > 0)
                                {
                                    if (cci == 0)
                                    {
                                        var statusText = "Qty for Return";
                                        db.Execute("UPDATE AM_WORK_ORDERS SET MATERIALS_REMARKS='" + statusText + "' WHERE REQ_NO=" + i.REF_WO);
                                        cci++;
                                    }
                                }
                                else
                                {
                                    if (cci == 0)
                                    {
                                        var statusText = "Dispatched";
                                        db.Execute("UPDATE AM_WORK_ORDERS SET MATERIALS_REMARKS='" + statusText + "' WHERE REQ_NO=" + i.REF_WO);

                                    }
                                }
                            }
                            else
                            {
                                dataExist.QTY_FORRETURN = 0;
                            }
                            dataExist.BILLABLE = i.BILLABLE;
                            dataExist.EXTENDED = i.EXTENDED;
                            db.Update(dataExist, new string[] { "QTY", "QTY_FORRETURN", "STATUS", "TRANSFER_FLAG", "USED_QTY" ,"BILLABLE","EXTENDED"});
                        }
                        else
                        {

                            if (i.TRANSFER_FLAG == "CFC")
                            {
                                if(dataExist.TRANSFER_FLAG == "F")
                                {
                                    if(dataExist.TRANSFER_QTY > 0)
                                    {

                                        if (cci == 0)
                                        {
                                            var statusText = "Dispatched";
                                            db.Execute("UPDATE AM_WORK_ORDERS SET MATERIALS_REMARKS='" + statusText + "' WHERE REQ_NO=" + i.REF_WO);
                                        }
                                        dataExist.STATUS = "Dispatch";
                                        dataExist.TRANSFER_FLAG = "D";
                                        dataExist.FOR_COLLECTION_CANCELBY = i.FOR_COLLECTION_CANCELBY;
                                        dataExist.FOR_COLLECTION_CANCELDATE = i.FOR_COLLECTION_CANCELDATE;
                                        dataExist.QTY = i.QTY;
                                        dataExist.FOR_COLLECTION_QTY = 0;
                                        dataExist.BILLABLE = i.BILLABLE;
                                        dataExist.EXTENDED = i.EXTENDED;
                                        db.Update(dataExist, new string[] { "QTY","FOR_COLLECTION_QTY", "FOR_COLLECTION_CANCELBY", "STATUS", "TRANSFER_FLAG", "FOR_COLLECTION_CANCELDATE","BILLABLE", "EXTENDED"});

                                    }
                                    else
                                    {
                                        
                                        if (cci == 0)
                                        {
                                            var statusText = "Pending";
                                            db.Execute("UPDATE AM_WORK_ORDERS SET MATERIALS_REMARKS='" + statusText + "' WHERE REQ_NO=" + i.REF_WO);
                                        }
                                        dataExist.STATUS = "Request";
                                        dataExist.TRANSFER_FLAG = null;
                                        dataExist.FOR_COLLECTION_CANCELBY = i.FOR_COLLECTION_CANCELBY;
                                        dataExist.FOR_COLLECTION_CANCELDATE = i.FOR_COLLECTION_CANCELDATE;
                                        dataExist.QTY = i.QTY;
                                        dataExist.FOR_COLLECTION_QTY = 0;
                                        dataExist.BILLABLE = i.BILLABLE;
                                        dataExist.EXTENDED = i.EXTENDED;
                                        db.Update(dataExist, new string[] { "QTY", "FOR_COLLECTION_QTY", "FOR_COLLECTION_CANCELBY", "STATUS", "TRANSFER_FLAG", "FOR_COLLECTION_CANCELDATE", "BILLABLE", "EXTENDED" });

                                    }
                                }
                                

                            }
                            else
                            {
                                if (i.USED_QTY > 0)
                                {
                                    if (dataExist.TRANSFER_QTY == i.USED_QTY)
                                    {
                                        dataExist.QTY_FORRETURN = 0;
                                    }
                                    else
                                    {
                                        dataExist.QTY_FORRETURN = (i.TRANSFER_QTY - (i.USED_QTY + i.QTY_RETURN));

                                    }

                                }
                                dataExist.USED_QTY = i.USED_QTY;
                                db.Update(dataExist, new string[] { "USED_QTY" });
                                if (i.USED_QTY == 0 && astionStr == "CONFIRM")
                                {
                                    if (cci == 0)
                                    {
                                        var statusText = "Qty for Return";
                                        db.Execute("UPDATE AM_WORK_ORDERS SET MATERIALS_REMARKS='" + statusText + "' WHERE REQ_NO=" + i.REF_WO);
                                        cci++;
                                    }


                                    if (i.TRANSFER_QTY > 0)
                                    {
                                        dataExist.QTY_FORRETURN = (i.TRANSFER_QTY - i.USED_QTY);
                                    }
                                    dataExist.USED_QTY = i.USED_QTY;
                                    dataExist.BILLABLE = i.BILLABLE;
                                    dataExist.EXTENDED = i.EXTENDED;
                                    db.Update(dataExist, new string[] { "QTY_FORRETURN", "TRANSFER_FLAG", "USED_QTY","BILLABLE","EXTENDED" });


                                }
                                if (i.QTY > dataExist.QTY && (i.STATUS == "Dispatch" || i.STATUS == "Received"))
                                {
                                    if (i.QTY > i.TRANSFER_QTY)
                                    {
                                        db.Execute("UPDATE AM_WORK_ORDERS SET MATERIALS_REMARKS='Additional Spare Parts QTY Requested' WHERE REQ_NO=" + i.REF_WO);
                                        cci++;
                                        var pendingQTY = i.QTY - i.TRANSFER_QTY;
                                        dataExist.STATUS = "Pending QTY " + pendingQTY;
                                        dataExist.TRANSFER_FLAG = "P";
                                        dataExist.QTY = i.QTY;
                                        dataExist.BILLABLE = i.BILLABLE;
                                        dataExist.EXTENDED = i.EXTENDED;
                                        db.Update(dataExist, new string[] { "QTY", "STATUS", "TRANSFER_FLAG","BILLABLE","EXTENDED" });
                                    }
                                }
                                else if (i.QTY == dataExist.QTY && (i.STATUS == "Dispatch" || i.STATUS == "Received"))

                                {
                                    if (i.QTY > i.TRANSFER_QTY)
                                    {
                                        db.Execute("UPDATE AM_WORK_ORDERS SET MATERIALS_REMARKS='Pending  QTY Requested for Spare Parts' WHERE REQ_NO=" + i.REF_WO);
                                        cci++;
                                        var pendingQTY = i.QTY - i.TRANSFER_QTY;
                                        dataExist.STATUS = "Pending QTY " + pendingQTY;
                                        dataExist.TRANSFER_FLAG = "P";
                                        dataExist.QTY = i.QTY;
                                        dataExist.BILLABLE = i.BILLABLE;
                                        dataExist.EXTENDED = i.EXTENDED;
                                        db.Update(dataExist, new string[] { "QTY", "STATUS", "TRANSFER_FLAG" ,"BILLABLE","EXTENDED"});
                                    }




                                }
                                else if (i.QTY < dataExist.QTY && i.TRANSFER_QTY > 0)
                                {
                                    dataExist.STATUS = "Dispatch";
                                    dataExist.TRANSFER_FLAG = "D";
                                    dataExist.QTY = i.QTY;
                                    dataExist.FOR_COLLECTION_QTY = 0;
                                    dataExist.BILLABLE = i.BILLABLE;
                                    dataExist.EXTENDED = i.EXTENDED;
                                    db.Update(dataExist, new string[] { "QTY", "STATUS", "TRANSFER_FLAG", "FOR_COLLECTION_QTY" ,"BILLABLE","EXTENDED"});

                                }
                                else if (i.QTY < dataExist.QTY && i.TRANSFER_QTY == 0)
                                {
                                    dataExist.STATUS = "Request";
                                    dataExist.TRANSFER_FLAG = null;
                                    dataExist.QTY = i.QTY;
                                    dataExist.BILLABLE = i.BILLABLE;
                                    dataExist.EXTENDED = i.EXTENDED;
                                    db.Update(dataExist, new string[] { "QTY", "STATUS", "TRANSFER_FLAG","BILLABLE","EXTENDED" });

                                }
                            }
                        }
                    }
                    else
                    {
                        
                        if(existoldRecord > 0)
                        {
                            db.Execute("UPDATE AM_WORK_ORDERS SET MATERIALS_REMARKS='Additional Spare Parts Requested' WHERE REQ_NO=" + i.REF_WO);
                        }
                        i.TRANSFER_FLAG = "P";
                        i.CREATED_DATE = createDt;
                        i.CREATED_BY = userId;
                        db.Insert(i);
                    }

                }
                

                transScope.Complete();
            }
            return requestList;
        }


        public List<AM_PARTSREQUEST> CreateWordDetailMaterial(List<AM_PARTSREQUEST> requestList, string userId, string createDate, string astionStr)
        {
            using (var transScope = db.GetTransaction())
            {

                DateTime createDt = DateTime.Parse(createDate);

                var reqId = requestList.Find(item => item.REF_WO != null && item.REF_WO != "");

                if (reqId != null)

                {
                    db.Delete<AM_PARTSREQUEST>(" WHERE REF_WO ='" + reqId.REF_WO + "'");
                   
                    if (astionStr == "DELETE")
                    {
                        return requestList;
                    }

                }
               
                foreach (AM_PARTSREQUEST i in requestList)
                {
                        db.Insert(i);
                }


                transScope.Complete();
            }
            return requestList;
        }

        public int deleteWOSpartParts (string ref_wono)
        {

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Delete<AM_PARTSREQUEST>(" WHERE REF_WO ='" + ref_wono + "' AND STATUS='Request'");

                transScope.Complete();
            }

            return 0;
        }
        public int deleteWorkDetailsSpartParts(string ref_wono)
        {

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Delete<AM_PARTSREQUEST>(" WHERE REF_WO ='" + ref_wono + "'");

                transScope.Complete();
            }

            return 0;
        }

        //Dispatch Work Order Labour Parts
        public List<dynamic> readWOLPartsReq(string queryString, int maxRows = 2000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieReadWOLPartsReq(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> readWOLMaterials(string queryString, int maxRows = 2000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieReadWOLMaterials(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<AM_PARTSREQUEST> UpdateWOLPartsRequest(List<AM_PARTSREQUEST> requestList, string userId, string createDate, string astionStr)
        {
            using (var transScope = db.GetTransaction())
            {

                DateTime createDt = DateTime.Parse(createDate);



                var cci = 0;
                foreach (AM_PARTSREQUEST i in requestList)
                {

                    if (i.TRANSFER_QTY == i.QTY && astionStr != "RETURN" && (i.STATUS == "Dispatch" || i.STATUS == "Received"))
                    {
                        continue;
                    }

                    var dataExist = db.SingleOrDefault<AM_PARTSREQUEST>(PetaPoco.Sql.Builder.Select("*").From("AM_PARTSREQUEST").Where("ID_PARTS=@prodId", new { prodId = i.ID_PARTS }).Where("REF_WO=@refWoId", new { refWoId = i.REF_WO }));
                    
                    if (i.TRANSFER_FLAG == "D" || i.TRANSFER_FLAG == "F")
                    {
                       
                        if(i.QTY_FORRETURN > 0 && astionStr == "RETURN")
                        {
                            
                            var currQty = db.SingleOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@prodId", new { prodId = i.ID_PARTS }).Where("STORE=@storeId", new { storeId = i.STORE_ID }));
                            if (currQty != null)
                            {
                                var trQty = currQty.QTY;
                                var toUpdateqty = trQty + i.QTY_FORRETURN;
                                i.QTY_RETURN = i.QTY_RETURN + i.QTY_FORRETURN;
                                i.TRANSFER_QTY = i.TRANSFER_QTY - i.QTY_FORRETURN;
                                i.QTY_FORRETURN = 0;
                                db.Execute("UPDATE PRODUCT_STOCKONHAND SET QTY=" + toUpdateqty + " WHERE PRODUCT_CODE='" + i.ID_PARTS + "' AND STORE='" + i.STORE_ID + "'");
                            }
                            i.QTY_FORRETURN = 0;
                            db.Update(i, new string[] { "QTY_RETURN","QTY_FORRETURN" }); //, "QTY_FORRETURN","TRANSFER_QTY"
                            db.Execute("UPDATE AM_WORK_ORDERS SET MATERIALS_REMARKS='Dispatched' WHERE REQ_NO=" + i.REF_WO);
                        }
                        else
                        {
                            if(astionStr != "RETURN")
                            {
                                i.TRANSFER_FLAG = "D";
                                i.STATUS = "Dispatch";
                                i.TRANSFER_QTY = i.TRANSFER_QTY + i.PENDING_QTY;
                                var currDispatchQty = i.PENDING_QTY + i.TRANSFER_QTY;
                                if (i.QTY > currDispatchQty)
                                {
                                    if(i.TRANSFER_QTY == 0)
                                    {
                                        i.TRANSFER_FLAG = null;
                                        i.STATUS = "Request";
                                    }
                                    else
                                    {
                                        i.TRANSFER_FLAG = "P";
                                        i.STATUS = " Pending QTY " + (i.QTY - (i.TRANSFER_QTY + i.PENDING_QTY)).ToString();
                                       
                                    }
                                    db.Execute("UPDATE AM_WORK_ORDERS SET MATERIALS_REMARKS='Partial Spare Parts Dispatched', PARTS_CONFIRMED = 'N'  WHERE REQ_NO=" + i.REF_WO);
                                    cci++;
                                }
                                i.DISPATCH_BY = userId;
                                i.DISPATCH_DATE = createDt;
                                db.Update(i, new string[] { "TRANSFER_QTY", "TRANSFER_FLAG", "STATUS","DISPATCH_BY", "DISPATCH_DATE" });
                                if (cci == 0)
                                {
                                    db.Execute("UPDATE AM_WORK_ORDERS SET MATERIALS_REMARKS='Dispatched', PARTS_CONFIRMED = 'N' WHERE REQ_NO=" + i.REF_WO);
                                    cci++;
                                }
                                if(i.PENDING_QTY > 0)
                                {
                                    var currQty = db.SingleOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@prodId", new { prodId = i.ID_PARTS }).Where("STORE=@storeId", new { storeId = i.STORE_ID }));
                                    if (currQty != null)
                                    {
                                        var trQty = currQty.QTY;
                                        var toUpdateqty = trQty - i.PENDING_QTY;

                                        db.Execute("UPDATE PRODUCT_STOCKONHAND SET QTY=" + toUpdateqty + " WHERE PRODUCT_CODE='" + i.ID_PARTS + "' AND STORE='" + i.STORE_ID + "'");
                                    }
                                }
                                


                            }
                            
                           
                        }
                        

                    }else
                    {

                        if(astionStr == "RETURN")
                        {


                        }
                        else
                        {
                            var currStatusText = "";
                            i.TRANSFER_FLAG = "F";

                            if (i.PENDING_QTY > 0)
                            {
                                if (astionStr != "RETURN")
                                {
                                    currStatusText = "For Collection QTY " + (i.PENDING_QTY).ToString();
                                }

                            }

                            //  if(dataExist.TRANSFER_QTY = 0)

                            var currQty = db.SingleOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@prodId", new { prodId = i.ID_PARTS }).Where("STORE=@storeId", new { storeId = i.STORE_ID }));
                            if (currQty != null)
                            {
                                var trQty = currQty.QTY;
                                var toUpdateqty = trQty - i.PENDING_QTY;

                                db.Execute("UPDATE PRODUCT_STOCKONHAND SET QTY=" + toUpdateqty + " WHERE PRODUCT_CODE='" + i.ID_PARTS + "' AND STORE='" + i.STORE_ID + "'");
                            }
                            var currDispatchQty = i.PENDING_QTY + i.TRANSFER_QTY;
                            if (i.QTY > currDispatchQty)
                            {
                                //i.TRANSFER_FLAG = "P";
                                if(i.TRANSFER_QTY == 0 && i.PENDING_QTY == 0)
                                {
                                    currStatusText = "Request";
                                }
                                else
                                {
                                    currStatusText = currStatusText + " Pending QTY " + (i.QTY - (i.TRANSFER_QTY + i.PENDING_QTY)).ToString();
                                }

                                
                            }
                            i.STATUS = currStatusText;
                            i.TRANSFER_QTY = i.TRANSFER_QTY + i.PENDING_QTY;
                           
                            db.Update(i, new string[] { "TRANSFER_QTY", "TRANSFER_FLAG", "STATUS" });
                            if (cci == 0)
                            {
                                db.Execute("UPDATE AM_WORK_ORDERS SET MATERIALS_REMARKS='Ready for Collection', PARTS_CONFIRMED = 'N' WHERE REQ_NO=" + i.REF_WO);
                                cci++;
                            }

                        }

                        
                    }
                    
                    

                }
                transScope.Complete();
            }
            return requestList;
        }

        public List<dynamic> readWOIdMaterials(string queryString, int maxRows = 2000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieReadWOIdMaterials(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> readWOIdPRMaterials(string queryString, int maxRows = 2000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieReadWOIdPRMaterials(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        public List<dynamic> readWOIdPRMaterialsUse(string queryString, int maxRows = 2000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieReadWOIdPRMaterialsUse(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        //PRODUCTS
        public PRODUCT_DETAILS CreateNewProduct(PRODUCT_DETAILS productDetails)
        {

            using (var transScope = db.GetTransaction())
            {

                //productDetails.PRODUCT_ID = Guid.NewGuid().ToString("N");
                //apply processed data
                db.Insert(productDetails);

                transScope.Complete();
            }

            return productDetails;
        }
        public PRODUCT_DETAILS ReadProduct(string productID)
        {
            PRODUCT_DETAILS ret = null;

            var productFound = db.SingleOrDefault<PRODUCT_DETAILS>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_DETAILS").Where("PRODUCT_ID=@productId", new { productId = productID }));

            if (productFound != null)
            {
                ret = productFound;
            }
            else
            {
                throw new Exception("Product not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdateProduct(PRODUCT_DETAILS productsetup)
        {
            //validate data
            if (productsetup != null)
            {
                if (productsetup.PRODUCT_ID == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(productsetup, new string[] { "DESCRIPTION", "SHORT_DESCRIPTION", "CATEGORY_ID", "CURRENT_PRICE", "GROUP_ID", "ASSET_FLAG"
                    , "ORDER_FLAG", "STATUS", "ORDER_DETAILS_FLAG", "SALE_UNIT" ,"BILL_FLAG", "STOCK_FLAG","DISCOUNT","SERVICE_ID","DEPARTMENT_ID","STOCK_UNIT"});

                transScope.Complete();
            }

            return 0;
        }


		//STOCK ON HAND ADJUSTMENT
		public STOCK_ONHAND_ADJUSTMENT CreateNewSOHAdjustment(STOCK_ONHAND_ADJUSTMENT infoDetails)
		{

			using (var transScope = db.GetTransaction())
			{

				var itemFound = db.SingleOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@idPart", new { idPart = infoDetails.ID_PARTS }).Where("STORE=@storeId", new { storeId = infoDetails.STORE_ID }));
				if (itemFound != null)
				{
					//if (itemFound.STATUS != infoDetails.STATUS_ADJUST)
					//{
					//	itemFound.STATUS = infoDetails.STATUS_ADJUST;
					//}
					//if (itemFound.MIN != infoDetails.MIN_ADJUST)
					//{
					//	itemFound.MIN = infoDetails.MIN_ADJUST;
					//}
					//if (itemFound.MAX != infoDetails.MAX_ADJUST)
					//{
					//	itemFound.MAX = infoDetails.MAX_ADJUST;
					//}
					//if (itemFound.QTY != infoDetails.QTY_ADJUST)
					//{
					//	itemFound.QTY = infoDetails.QTY_ADJUST;
					//}
                    db.Execute("UPDATE PRODUCT_STOCKONHAND SET  STATUS = '" + infoDetails.STATUS_ADJUST + "' , MIN ="+ infoDetails.MIN_ADJUST + ", MAX="+ infoDetails.MAX_ADJUST + ", QTY="+ infoDetails.QTY_ADJUST + "  WHERE PRODUCT_CODE ='" + infoDetails.ID_PARTS + "' AND STORE = '" + infoDetails.STORE_ID + "'");
                   // db.Update(itemFound, new string[] { "QTY", "MIN", "MAX","STATUS"});
					}
                else
                {
                    PRODUCT_STOCKONHAND woInfo = new PRODUCT_STOCKONHAND();
                    if(infoDetails.STATUS_ADJUST == null || infoDetails.STATUS_ADJUST == "")
                    {
                        infoDetails.STATUS_ADJUST = "Y";
                    }
                    woInfo.PRODUCT_CODE = infoDetails.ID_PARTS;
                    woInfo.MIN = infoDetails.MIN_ADJUST;
                    woInfo.MAX = infoDetails.MAX_ADJUST;
                    woInfo.QTY = infoDetails.QTY_ADJUST;
                    woInfo.STATUS = infoDetails.STATUS_ADJUST;
                    woInfo.STORE = infoDetails.STORE_ID.ToString();
                    db.Insert(woInfo);
                }
					//apply processed data
					db.Insert(infoDetails);

				transScope.Complete();
			}

			return infoDetails;
		}
		public List<dynamic> SearchSOHAdjustment(string queryString, string idPart, string storeId, SearchFilter filter, int maxRows = 5000)
		{

			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchSOHAdjustment(db._dbType.ToString(), queryString, idPart, storeId, filter,  maxRows)));
			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}



        //STORE BATCHES
        public List<dynamic> SearchStoreBatches(string queryString, SearchFilter filter, int maxRows = 5000)
        {

            var storeBatchesFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStoreBatches(db._dbType.ToString(), queryString, filter, maxRows)));
            if (storeBatchesFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return storeBatchesFound;
        }





		//SUPPLIERS
		public PRODUCT_SUPPLIERS CreateNewSuppliers(PRODUCT_SUPPLIERS supplierDetails)
        {

            using (var transScope = db.GetTransaction())
            {

                //productDetails.PRODUCT_ID = Guid.NewGuid().ToString("N");
                //apply processed data
                db.Insert(supplierDetails);

                transScope.Complete();
            }

            return supplierDetails;
        }
        public PRODUCT_SUPPLIERS ReadSupplier(string supplierID)
        {
            PRODUCT_SUPPLIERS ret = null;

            var productFound = db.SingleOrDefault<PRODUCT_SUPPLIERS>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_SUPPLIERS").Where("SUPPLIER_ID=@supplierId", new { supplierId = supplierID }));

            if (productFound != null)
            {
                ret = productFound;
            }
            else
            {
                throw new Exception("Product not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdateSupplier(PRODUCT_SUPPLIERS supplierDetails)
        {
            //validate data
            if (supplierDetails != null)
            {
                if (supplierDetails.SUPPLIER_ID == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(supplierDetails, new string[] { "DESCRIPTION", "ADDRESS", "CONTACT_NO", "CONTACT_PERSON", "CONTACT_OTH"
                    , "STATUS"});

                transScope.Complete();
            }

            return 0;
        }

        //STORES
        public STORES CreateNewStore(STORES storeDetails, string userId)
        {

            using (var transScope = db.GetTransaction())
            {

                //productDetails.PRODUCT_ID = Guid.NewGuid().ToString("N");
                //apply processed data
                storeDetails.CREATED_BY = userId;
                storeDetails.CREATED_DATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                db.Insert(storeDetails);

                transScope.Complete();
            }

            return storeDetails;
        }
        public STORES ReadStore(string storeID)
        {
            STORES ret = null;

            var productFound = db.SingleOrDefault<STORES>(PetaPoco.Sql.Builder.Select("*").From("STORES").Where("STORE_ID=@storeId", new { storeId = storeID }));

            if (productFound != null)
            {
                ret = productFound;
            }
            else
            {
                throw new Exception("Product not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdateStore(STORES storeDetails, List<STORE_BUILDING> storeBuilding, string userId)
        {
            //validate data
            if (storeDetails != null)
            {
                if (storeDetails.STORE_ID == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                db.Delete<STORE_BUILDING>(" WHERE STORE_ID='" + storeDetails.STORE_ID + "'");

                if (storeBuilding.Count() > 0)
                {
                    foreach (STORE_BUILDING i in storeBuilding)
                    {
                        var itemFound = db.SingleOrDefault<STORE_BUILDING>(PetaPoco.Sql.Builder.Select("*").From("STORE_BUILDING").Where("BUILDING_CODE=@refId", new { refId = i.BUILDING_CODE }).Where("STORE_ID=@refClient", new { refClient = storeDetails.STORE_ID }));
                        if (itemFound != null)
                        {

                        }
                        else
                        {
                            i.STORE_ID = storeDetails.STORE_ID;
                            i.CREATED_DATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                            i.CREATED_BY = userId;
                            db.Insert(i);
                        }

                    }
                }
                else
                {

                }


                //apply processed data
                db.Update(storeDetails, new string[] { "DESCRIPTION", "LOCATION", "CONTACT_NO", "COSTCENTERFLAG", "STATUS","ALLOWREQUEST_FLAG"});

                transScope.Complete();
            }

            return 0;
        }
        public List<dynamic> SearchstoreBuilding(string queryString, int maxRows = 100)
        {
            var servicesFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchstoreBuilding(db._dbType.ToString(), queryString, maxRows)));

            if (servicesFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return servicesFound;
        }

        public List<LOV_LOOKUPS> SearchStoreBuildingLOV(int maxRows = 500)
        {
            var servicesFound = db.Fetch<LOV_LOOKUPS>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStoreBuildingLOV(db._dbType.ToString(), "N", maxRows)));

            if (servicesFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return servicesFound;
        }


        //STOCK ON HAND
        public int UpdateStockOnHandItemLocation(string storeId, string partid, string location, string location2 , string location3, string location4, string updateBy, string updateDate)
        {
            //validate data
           

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Execute("UPDATE PRODUCT_STOCKONHAND SET  LOCATION_UPDATEDDATE = '"+ updateDate + "', LOCATION_UPDATEDBY = '" + updateBy + "', LOCATION = '" + location + "', LOCATION2 ='"+location2+ "', LOCATION3 = '"+ location3+ "', LOCATION4='"+ location4+"' WHERE PRODUCT_CODE ='" + partid + "' AND STORE = '" + storeId + "'");

                transScope.Complete();
            }

            return 0;
        }
        public List<dynamic> SearchStockOnHand(string queryString, string queryStatus, string queryCagetory,string queryStore, int maxRows = 10000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStockOnHand(db._dbType.ToString(), queryString, queryStatus, queryCagetory, queryStore, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        public List<dynamic> SearchStockOnHandByUserStore(string queryString, string queryStatus, string queryCagetory, string queryStore, string userId,int maxRows = 10000)
        {

           // var itemFound = db.SingleOrDefault<USER_STORE>(PetaPoco.Sql.Builder.Select("*").From("USER_STORE").Where("USER_ID=@refId", new { refId = userId }));

            var itemFound = db.Fetch<USER_STORE>(PetaPoco.Sql.Builder.Select("*").From("USER_STORE").Where("USER_ID=@refId", new { refId = userId }));

            if (itemFound.Count() == 0)
            {
                userId = null;
            }


            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStockOnHandByUserStore(db._dbType.ToString(), queryString, queryStatus, queryCagetory, queryStore, userId, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        public List<dynamic> SearchNStockOnHand(string queryString, string queryStore, int maxRows = 5000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchProductsNotStockOnHand(db._dbType.ToString(), queryString,  queryStore, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }


        public List<dynamic> SearchUserStoreStockOnHand(string queryString, string queryStatus, string queryCagetory, string queryStore, string userId, int maxRows = 10000)
        {

            // var itemFound = db.SingleOrDefault<USER_STORE>(PetaPoco.Sql.Builder.Select("*").From("USER_STORE").Where("USER_ID=@refId", new { refId = userId }));

            var itemFound = db.Fetch<USER_STORE>(PetaPoco.Sql.Builder.Select("*").From("USER_STORE").Where("USER_ID=@refId", new { refId = userId }));

            if (itemFound.Count() == 0)
            {
                userId = null;
            }


            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchUserStoreStockOnHand(db._dbType.ToString(), queryString, queryStatus, queryCagetory, queryStore, userId, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }


        public List<dynamic> GetStockBatchByProductAndRequest(string requestId, string productCode, string barCodeId)
        {
            var productStockBatches = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieGetStockBatchByProductAndRequest(db._dbType.ToString(), requestId, productCode, barCodeId)));
            if (productStockBatches == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productStockBatches;
        }

        public List<PRODUCT_STOCKBATCH> CreateNewStockBatchV2(AM_PURCHASEREQUEST amPurchaseRequest, List<dynamic> AMPurchaseRequesItems, List<PRODUCT_STOCKBATCH> productStockBatches, STAFF_LOGON_SESSIONS userSession)
        {
            using (var transScope = db.GetTransaction())
            {
                var arabDateTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");

                var purchaseOrderStatus = "RECEIVED";
                int totalQuantityReiceved = 0;
                foreach (var prItem in AMPurchaseRequesItems)
                {
                    totalQuantityReiceved = prItem.TOTAL_QTY_RECEIVED == null ? 0 : (int)prItem.TOTAL_QTY_RECEIVED;
                    var filteredProductStockBatches = productStockBatches.FindAll(x => x.PRODUCT_CODE == prItem.ID_PARTS);
                    foreach (var item in filteredProductStockBatches)
                    {
                        totalQuantityReiceved += item.QTY_RECEIVED;
                        var qtyReceived = 0;
                        var totalPrice = 0;
                        var itemStockBatchFound = db.FirstOrDefault<PRODUCT_STOCKBATCH>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKBATCH").Where("BATCH_ID=@batchId", new { batchId = item.BATCH_ID }));
                        if (itemStockBatchFound != null)
                        {
                            itemStockBatchFound.TRANS_DATE = arabDateTime;
                            itemStockBatchFound.QTY_RECEIVED += item.QTY_RECEIVED;
                            itemStockBatchFound.BOX_QTY_RECEIVED += item.BOX_QTY_RECEIVED;
                            itemStockBatchFound.STOCK_ONHAND += item.QTY_RECEIVED;
                            db.Update(itemStockBatchFound);

                            qtyReceived = itemStockBatchFound.QTY_RECEIVED;
                            totalPrice = itemStockBatchFound.QTY_RECEIVED * (int)itemStockBatchFound.PRICE;

                            var defaultCostCenter = db.FirstOrDefault<SYSTEM_DEFAULT>(PetaPoco.Sql.Builder.Select("*").From("SYSTEM_DEFAULT").Where("SYSDEFAULT=@reqId", new { reqId = "BSC" }));
                            var costcenterdescriptionFound = db.FirstOrDefault<COSTCENTER_DETAILS>(PetaPoco.Sql.Builder.Select("Description").From("COSTCENTER_DETAILS").Where("CODEID=@reqId", new { reqId = defaultCostCenter.SYSDEFAULTVAL }));
                            var costcennterName = "";
                            if (costcenterdescriptionFound != null)
                                costcennterName = costcenterdescriptionFound.DESCRIPTION;

                            var productInfoFound = db.FirstOrDefault<AM_PARTS>(PetaPoco.Sql.Builder.Select("*").From("AM_PARTS").Where("ID_PARTS=@reqId", new { reqId = item.PRODUCT_CODE }));
                            var productTypeFound = db.FirstOrDefault<LOV_LOOKUPS>(PetaPoco.Sql.Builder.Select("DESCRIPTION").From("LOV_LOOKUPS").Where("LOV_LOOKUP_ID=@reqId", new { reqId = productInfoFound.TYPE_ID }).Where("CATEGORY=@catId", new { catId = "AIMS_MSTYPE" }));
                            var productUOMFound = db.FirstOrDefault<LOV_LOOKUPS>(PetaPoco.Sql.Builder.Select("DESCRIPTION").From("LOV_LOOKUPS").Where("LOV_LOOKUP_ID=@reqId", new { reqId = productInfoFound.UOM_ID }).Where("CATEGORY=@catId", new { catId = "AIMS_MSUOM" }));

                            var systemTrans = db.FirstOrDefault<SYSTEM_TRANSACTIONDETAILS>(PetaPoco.Sql.Builder.Select("*").From("SYSTEM_TRANSACTIONDETAILS").Where("REFTRANSID=@refTransId", new { refTransId = item.BATCH_ID }));
                            if (systemTrans != null)
                            {
                                systemTrans.REFCOST = totalPrice;
                                systemTrans.REFQTY = qtyReceived;
                                systemTrans.MESSAGE = "New " + qtyReceived + " " + productUOMFound.DESCRIPTION.ToLower() + " with batch number (" + item.BARCODE_ID + ")  was received on " + String.Format("{0:dd/MM/yyyy}", arabDateTime);
                                db.Update(systemTrans);

                                qtyReceived = item.QTY_RECEIVED;
                                totalPrice = item.QTY_RECEIVED * (int)item.PRICE;
                            }
                        }
                        else
                        {
                            var productInfoFound = db.FirstOrDefault<AM_PARTS>(PetaPoco.Sql.Builder.Select("*").From("AM_PARTS").Where("ID_PARTS=@reqId", new { reqId = item.PRODUCT_CODE }));

                            item.TRANS_DATE = arabDateTime;
                            item.CREATED_BY = userSession.STAFF_ID;
                            item.TRANS_ID = amPurchaseRequest.REQUEST_ID;
                            item.STOCK_ONHAND = item.QTY_RECEIVED;
                            item.CREATED_DATE = arabDateTime;
                            if (productInfoFound.TYPE_ID == "AIMS_MSTYPE-3")
                                item.STATUS = "ASSET";
                            else
                                item.STATUS = "RECEIVED";

                            db.Insert(item);

                            qtyReceived = item.QTY_RECEIVED;
                            totalPrice = item.QTY_RECEIVED * (int)item.PRICE;

                            if (productInfoFound.TYPE_ID != "AIMS_MSTYPE-3")
                            {
                                var defaultCostCenter = db.FirstOrDefault<SYSTEM_DEFAULT>(PetaPoco.Sql.Builder.Select("*").From("SYSTEM_DEFAULT").Where("SYSDEFAULT=@reqId", new { reqId = "BSC" }));
                                var costcenterdescriptionFound = db.FirstOrDefault<COSTCENTER_DETAILS>(PetaPoco.Sql.Builder.Select("Description").From("COSTCENTER_DETAILS").Where("CODEID=@reqId", new { reqId = defaultCostCenter.SYSDEFAULTVAL }));
                                var costcennterName = "";
                                if (costcenterdescriptionFound != null)
                                    costcennterName = costcenterdescriptionFound.DESCRIPTION;

                                var productTypeFound = db.FirstOrDefault<LOV_LOOKUPS>(PetaPoco.Sql.Builder.Select("DESCRIPTION").From("LOV_LOOKUPS").Where("LOV_LOOKUP_ID=@reqId", new { reqId = productInfoFound.TYPE_ID }).Where("CATEGORY=@catId", new { catId = "AIMS_MSTYPE" }));
                                var productUOMFound = db.FirstOrDefault<LOV_LOOKUPS>(PetaPoco.Sql.Builder.Select("DESCRIPTION").From("LOV_LOOKUPS").Where("LOV_LOOKUP_ID=@reqId", new { reqId = productInfoFound.UOM_ID }).Where("CATEGORY=@catId", new { catId = "AIMS_MSUOM" }));

                                var systemTrans = new SYSTEM_TRANSACTIONDETAILS();
                                systemTrans.REFTRANSID = item.BATCH_ID;
                                systemTrans.REFCODE = item.PRODUCT_CODE;
                                systemTrans.REFDESCRIPTION = productInfoFound.DESCRIPTION;
                                systemTrans.REFCOSTCENTERCODE = costcenterdescriptionFound.CODEID;
                                systemTrans.REFCOST = totalPrice;
                                systemTrans.REFCOSTCENTERDESCRIPTION = costcennterName;
                                systemTrans.REFMODEL = item.MODELCODE;
                                systemTrans.REFQTY = qtyReceived;
                                systemTrans.REFBATCHSERIALNO = item.BARCODE_ID;
                                systemTrans.REFINVOICENO = item.INVOICENO;
                                systemTrans.REFEXPIRY = item.EXPIRY_DATE;

                                if (productUOMFound != null) { systemTrans.REFUOM = productUOMFound.DESCRIPTION; }
                                systemTrans.REASONTYPE = "Received";
                                if (productUOMFound != null) { systemTrans.REFTYPE = productTypeFound.DESCRIPTION; }
                                systemTrans.REFTYPE = productTypeFound.DESCRIPTION;
                                systemTrans.MESSAGE = "New " + qtyReceived + " " + productUOMFound.DESCRIPTION.ToLower() + " with batch number (" + item.BARCODE_ID + ")  was received on " + String.Format("{0:dd/MM/yyyy}", arabDateTime);

                                systemTrans.STATUS = "Y";
                                systemTrans.CREATEDBY = userSession.STAFF_ID;
                                systemTrans.CREATEDDATE = arabDateTime;
                                db.Insert(systemTrans);
                            }
                        }

                        if (item.PURCHASE_NO != null && item.PURCHASE_NO != "0")
                        {
                            //Update Purchase Request / Purchase Order
                            var itemPOFound = db.FirstOrDefault<AM_PURCHASEORDER_ITEMS>(PetaPoco.Sql.Builder.Select("*").From("AM_PURCHASEORDER_ITEMS").Where("ID_PARTS=@productId", new { productId = item.PRODUCT_CODE }).Where("PURCHASE_NO=@reqId", new { reqId = item.PURCHASE_NO }));
                            if (itemPOFound != null)
                            {
                                itemPOFound.QTY_RECEIVED += qtyReceived;
                                itemPOFound.PO_TOTALUNITPRICEITEM += totalPrice;
                                db.Update(itemPOFound);
                            }
                            else
                            {
                                AM_PURCHASEORDER_ITEMS poItem = new AM_PURCHASEORDER_ITEMS();
                                poItem.ID_PARTS = item.PRODUCT_CODE;
                                poItem.PURCHASE_NO = Int32.Parse(item.PURCHASE_NO);
                                poItem.QTY_RECEIVED = qtyReceived;
                                poItem.PO_UNITPRICE = item.PRICE;
                                poItem.QTY_PO_REQUEST = item.QTY;
                                poItem.PR_REQUEST_ID = item.PURCHASE_NO;
                                poItem.PO_TOTALUNITPRICEITEM = totalPrice;
                                poItem.QTY_RETURN = 0;
                                db.Insert(poItem);
                            }
                        }
                        var productInfoFound2 = db.FirstOrDefault<AM_PARTS>(PetaPoco.Sql.Builder.Select("*").From("AM_PARTS").Where("ID_PARTS=@reqId", new { reqId = item.PRODUCT_CODE }));
                        if (productInfoFound2.TYPE_ID != "AIMS_MSTYPE-3")
                        {
                            //Update Stock on Hand
                            var itemStockFound = db.FirstOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@productId", new { productId = item.PRODUCT_CODE }).Where("STORE=@storeId", new { storeId = item.STORE }));
                            if (itemStockFound != null)
                            {
                                int stPRQty = itemStockFound.PR_QTY;
                                int sbrQty = qtyReceived;
                                int newPRQty = stPRQty - sbrQty;
                                var newQty = itemStockFound.QTY + qtyReceived;
                                if (newPRQty < 0)
                                    newPRQty = 0;

                                db.Execute("UPDATE PRODUCT_STOCKONHAND SET PR_QTY=" + newPRQty + " , QTY = " + newQty + " WHERE PRODUCT_CODE ='" + item.PRODUCT_CODE + "' AND STORE = '" + item.STORE + "'");
                            }
                            else
                            {
                                PRODUCT_STOCKONHAND stockonHandDet = new PRODUCT_STOCKONHAND();
                                stockonHandDet.PRODUCT_CODE = item.PRODUCT_CODE;
                                stockonHandDet.STORE = item.STORE;
                                stockonHandDet.QTY = qtyReceived;
                                stockonHandDet.UOM = item.UOM;
                                stockonHandDet.MIN = item.MIN;
                                stockonHandDet.MAX = item.MAX;
                                stockonHandDet.STATUS = "Y";
                                db.Insert(stockonHandDet);
                            }
                        }
                    }

                    if ((int)prItem.PR_REQUEST_QTY > (int)totalQuantityReiceved)
                        purchaseOrderStatus = "PARTIAL RECEIVED";
                }
                db.Execute("UPDATE AM_PURCHASEREQUEST SET STATUS='" + purchaseOrderStatus + "', POINVOICENO='" + amPurchaseRequest.POINVOICENO + "' WHERE REQUEST_ID ='" + amPurchaseRequest.REQUEST_ID + "'");
                transScope.Complete();

                return productStockBatches;
            }
        }

        //STOCK BATCH RECEIVE
        public List<PRODUCT_STOCKBATCH> CreateNewStockBatch(List<PRODUCT_STOCKBATCH> NewStockBatch, string userId, string createDate, string action, string transId)
		{
			using (var transScope = db.GetTransaction())
			{
				DateTime createDt = DateTime.Parse(createDate);

                var currDate =  TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");


				var transIdCurr = "";
				//if (action != "POST") {
					if (transId == "0")
					{
						db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='STOCKBATCH_ID'");
						var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "STOCKBATCH_ID" }));
						transIdCurr = seq[0].SEQ_VAL.ToString();
					}
					else
					{
						db.Delete<PRODUCT_STOCKBATCH>(" WHERE TRANS_ID ='" + transId + "'");
						transIdCurr = transId;
					}
                //}

                AM_PURCHASEORDER_ITEMS poItem = new AM_PURCHASEORDER_ITEMS();
                var stockBatch = NewStockBatch.Count();
				var cx = 0;
				var purchaseOrderStatus = "FOR POSTING";
				foreach (PRODUCT_STOCKBATCH i in NewStockBatch)
				{
					if (action == "POST")
					{
						cx++;
						i.STATUS = "RECEIVED";
						if (i.PURCHASE_NO != null && i.PURCHASE_NO != "0")
						{
							//Update Purchase Request / Purchase Order
							var itemPOFound = db.SingleOrDefault<AM_PURCHASEORDER_ITEMS>(PetaPoco.Sql.Builder.Select("*").From("AM_PURCHASEORDER_ITEMS").Where("ID_PARTS=@productId", new { productId = i.PRODUCT_CODE }).Where("PURCHASE_NO=@reqId", new { reqId = i.PURCHASE_NO }));
							if (itemPOFound != null)
							{
								
                               
                                if (i.QTY_RECEIVED < itemPOFound.QTY_PO_REQUEST)
                                {
                                   
                                    purchaseOrderStatus = "PARTIAL RECEIVED";
                                }
                                else
                                {
                                    purchaseOrderStatus = "RECEIVED";

                                }
                                db.Execute("UPDATE AM_PURCHASEORDER_ITEMS SET QTY_RECEIVED='" + i.QTY_RECEIVED + "', PO_UNITPRICE='"+ i.PRICE + "', PO_TOTALUNITPRICEITEM='"+ i.TOTAL_PRICE +  "'  WHERE PURCHASE_NO ='" + i.PURCHASE_NO + "' AND ID_PARTS = '" + i.PRODUCT_CODE + "'");

                            }
                            else
                            {

                                if (i.QTY_RECEIVED < i.QTY)
                                {

                                    purchaseOrderStatus = "PARTIAL RECEIVED";
                                }
                                else
                                {
                                    purchaseOrderStatus = "RECEIVED";

                                }


                                poItem.ID_PARTS = i.PRODUCT_CODE;
                                poItem.PURCHASE_NO = Int32.Parse(i.PURCHASE_NO);
                                poItem.QTY_RECEIVED = i.QTY_RECEIVED;
                                poItem.PO_UNITPRICE = i.PRICE;
                                poItem.QTY_PO_REQUEST = i.QTY;
                                poItem.PR_REQUEST_ID = i.PURCHASE_NO;
                                poItem.PO_TOTALUNITPRICEITEM = i.TOTAL_PRICE;
                                poItem.QTY_RETURN = 0;
                                db.Insert(poItem);


                            }

							//Update Stock on Hand
							var itemStockFound = db.SingleOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@productId", new { productId = i.PRODUCT_CODE }).Where("STORE=@storeId", new { storeId = i.STORE }));
							if (itemStockFound != null)
							{
								int stPRQty = itemStockFound.PR_QTY;
								int sbrQty = decimal.ToInt32(i.QTY);
								int newPRQty = stPRQty - sbrQty;
								var newQty = itemStockFound.QTY + i.QTY;
								if (newPRQty < 0) {
									newPRQty = 0;
									};
								db.Execute("UPDATE PRODUCT_STOCKONHAND SET PR_QTY="+ newPRQty + " , QTY = " + newQty + " WHERE PRODUCT_CODE ='" + i.PRODUCT_CODE + "' AND STORE = '" + i.STORE + "'");
							}
							else
							{
								PRODUCT_STOCKONHAND stockonHandDet = new PRODUCT_STOCKONHAND();
								stockonHandDet.PRODUCT_CODE = i.PRODUCT_CODE;
								stockonHandDet.STORE = i.STORE;
								stockonHandDet.QTY = i.QTY;
								stockonHandDet.UOM = i.UOM;
								stockonHandDet.MIN = i.MIN;
								stockonHandDet.MAX = i.MAX;
								db.Insert(stockonHandDet);
							}

						}
						else
						{
							//Update Stock on Hand
							var itemStockFound = db.SingleOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@productId", new { productId = i.PRODUCT_CODE }).Where("STORE=@storeId", new { storeId = i.STORE }));
							if (itemStockFound != null)
							{
								int stPRQty = itemStockFound.PR_QTY;
								int sbrQty = decimal.ToInt32(i.QTY);
								int newPRQty = stPRQty - sbrQty;
								var newQty = itemStockFound.QTY + i.QTY;
								if (newPRQty < 0)
								{
									newPRQty = 0;
								};
								db.Execute("UPDATE PRODUCT_STOCKONHAND SET PR_QTY=" + newPRQty + " , QTY = " + newQty + " WHERE PRODUCT_CODE ='" + i.PRODUCT_CODE + "' AND STORE = '" + i.STORE + "'");
							}
							else
							{
								PRODUCT_STOCKONHAND stockonHandDet = new PRODUCT_STOCKONHAND();
								stockonHandDet.PRODUCT_CODE = i.PRODUCT_CODE;
								stockonHandDet.STORE = i.STORE;
								stockonHandDet.QTY = i.QTY;
								stockonHandDet.UOM = i.UOM;
								stockonHandDet.MIN = i.MIN;
								stockonHandDet.MAX = i.MAX;
								db.Insert(stockonHandDet);
							}
						}

						//Update Stock Batch Receipt Status
						
							var itemStockBatchFound = db.SingleOrDefault<PRODUCT_STOCKBATCH>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKBATCH").Where("BATCH_ID=@batchId", new { batchId = i.BATCH_ID }));
							if (itemStockBatchFound != null)
							{

								
								itemStockBatchFound.STORE = i.STORE;
								itemStockBatchFound.QTY = i.QTY;
								itemStockBatchFound.UOM = i.UOM;
								itemStockBatchFound.MIN = i.MIN;
								itemStockBatchFound.MAX = i.MAX;
								itemStockBatchFound.BARCODE_ID = i.BARCODE_ID;
								itemStockBatchFound.STATUS = i.STATUS;
                                itemStockBatchFound.EXPIRY_DATE = i.EXPIRY_DATE;
                                itemStockBatchFound.INVOICENO = i.INVOICENO;
                                itemStockBatchFound.SUPPLIER = i.SUPPLIER;

                            db.Update(itemStockBatchFound, new string[] { "STORE", "QTY", "UOM", "MIN", "MAX", "STATUS", "TRANS_DATE", "EXPIRY_DATE","INVOICENO", "SUPPLIER" });
								

							}else
                            {


                                i.TRANS_DATE = createDt;
                                i.CREATED_DATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                                i.CREATED_BY = userId;
                                i.STOCK_ONHAND = i.QTY_RECEIVED;
                                i.TRANS_ID = transIdCurr;
                                db.Insert(i);

                            }
                        if (stockBatch == cx)
                        {
                            db.Execute("UPDATE AM_PURCHASEREQUEST SET STATUS='" + purchaseOrderStatus + "' WHERE REQUEST_ID ='" + i.PURCHASE_NO + "'");
                        }
                        SYSTEM_DEFAULT defaultCostCenter = new SYSTEM_DEFAULT();

                        defaultCostCenter = db.SingleOrDefault<SYSTEM_DEFAULT>(PetaPoco.Sql.Builder.Select("*").From("SYSTEM_DEFAULT").Where("SYSDEFAULT=@reqId", new { reqId = "BSC" }));
                      

                        
                        var costcenterdescriptionFound = db.SingleOrDefault<COSTCENTER_DETAILS>(PetaPoco.Sql.Builder.Select("Description").From("COSTCENTER_DETAILS").Where("CODEID=@reqId", new { reqId = defaultCostCenter.SYSDEFAULTVAL }));

                        var costcennterName = "";
                        if (costcenterdescriptionFound != null)
                        {
                            costcennterName = costcenterdescriptionFound.DESCRIPTION;
                    }

                        var productInfoFound = db.SingleOrDefault<AM_PARTS>(PetaPoco.Sql.Builder.Select("*").From("AM_PARTS").Where("ID_PARTS=@reqId", new { reqId = i.PRODUCT_CODE }));


                        var productTypeFound = db.SingleOrDefault<LOV_LOOKUPS>(PetaPoco.Sql.Builder.Select("DESCRIPTION").From("LOV_LOOKUPS").Where("LOV_LOOKUP_ID=@reqId", new { reqId = productInfoFound .TYPE_ID}).Where("CATEGORY=@catId", new { catId = "AIMS_MSTYPE" }));

                        var productUOMFound = db.SingleOrDefault<LOV_LOOKUPS>(PetaPoco.Sql.Builder.Select("DESCRIPTION").From("LOV_LOOKUPS").Where("LOV_LOOKUP_ID=@reqId", new { reqId = productInfoFound.UOM_ID }).Where("CATEGORY=@catId", new { catId = "AIMS_MSUOM" }));


                        //var currDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                        DateTime cToday = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                        SYSTEM_TRANSACTIONDETAILS systemTrans = new SYSTEM_TRANSACTIONDETAILS();

                        systemTrans.REFTRANSID = i.BATCH_ID;
                        systemTrans.REFCODE = i.PRODUCT_CODE;
                        systemTrans.REFDESCRIPTION = productInfoFound.DESCRIPTION;
                        systemTrans.REFCOSTCENTERCODE = costcenterdescriptionFound.CODEID;
                        systemTrans.REFCOST = i.TOTAL_PRICE;
                        systemTrans.REFCOSTCENTERDESCRIPTION = costcennterName;
                        systemTrans.REFMODEL = i.MODELCODE;
                        systemTrans.REFQTY = i.QTY_RECEIVED;
                        systemTrans.REFBATCHSERIALNO = i.BARCODE_ID;
                        systemTrans.REFINVOICENO = i.INVOICENO;

                        systemTrans.REFEXPIRY = i.EXPIRY_DATE;

                        if (productUOMFound != null) { systemTrans.REFUOM = productUOMFound.DESCRIPTION; }
                        
                        systemTrans.REASONTYPE = "Received";
                        if(productUOMFound != null) { systemTrans.REFTYPE = productTypeFound.DESCRIPTION; }
                        systemTrans.REFTYPE = productTypeFound.DESCRIPTION;
                        systemTrans.MESSAGE = "New " + i.QTY_RECEIVED +" "+ productUOMFound.DESCRIPTION.ToLower() +   " with batch number ("+i.BARCODE_ID+")  was received on " + String.Format("{0:dd/MM/yyyy}", cToday);
                      
                        systemTrans.STATUS = "Y";
                        systemTrans.CREATEDBY = userId;
                        systemTrans.CREATEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                        db.Insert(systemTrans);


                    }
					else {

                        db.Execute("UPDATE AM_PURCHASEREQUEST SET STATUS='" + purchaseOrderStatus + "' WHERE REQUEST_ID ='" + i.PURCHASE_NO + "'");

                        var itemStockBatchFound = db.SingleOrDefault<PRODUCT_STOCKBATCH>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKBATCH").Where("BATCH_ID=@batchId", new { batchId = i.BATCH_ID }));
						if (itemStockBatchFound != null)
						{
							itemStockBatchFound.TRANS_DATE = createDt;
							itemStockBatchFound.STORE = i.STORE;
							itemStockBatchFound.QTY = i.QTY;
							itemStockBatchFound.UOM = i.UOM;
							itemStockBatchFound.MIN = i.MIN;
							itemStockBatchFound.MAX = i.MAX;
							itemStockBatchFound.BARCODE_ID = i.BARCODE_ID;
							itemStockBatchFound.STATUS = i.STATUS;
							itemStockBatchFound.EXPIRY_DATE = i.EXPIRY_DATE;
                            itemStockBatchFound.SUPPLIER = i.SUPPLIER;
                            itemStockBatchFound.INVOICENO = i.INVOICENO;
                            db.Update(itemStockBatchFound, new string[] { "STORE", "QTY", "UOM", "MIN", "MAX", "STATUS", "TRANS_DATE", "EXPIRY_DATE","INVOICENO", "SUPPLIER" });
						}
						else
						{
				

							i.TRANS_DATE = createDt;
							i.CREATED_BY = userId;
							i.TRANS_ID = transIdCurr;
                            i.STOCK_ONHAND = i.QTY_RECEIVED;
                            i.CREATED_DATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
							db.Insert(i);
		
						}



					}

					
				}
				transScope.Complete();

			}
			return NewStockBatch;
		}
        public PRODUCT_SUPPLIERS ReadStockBatch(string supplierID)
        {
            PRODUCT_SUPPLIERS ret = null;

            var productFound = db.SingleOrDefault<PRODUCT_SUPPLIERS>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_SUPPLIERS").Where("SUPPLIER_ID=@supplierId", new { supplierId = supplierID }));

            if (productFound != null)
            {
                ret = productFound;
            }
            else
            {
                throw new Exception("Product not found.") { Source = this.GetType().Name };
            }
            return ret;
        }

        public PRODUCT_STOCKBATCH CreateProductBatch(PRODUCT_STOCKBATCH batch)
        {
            using (var transScope = db.GetTransaction())
            {
                batch.CREATED_DATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                db.Insert(batch);
                transScope.Complete();
            }
            var itemStockOnHandFound = db.SingleOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@prodcode", new { prodcode = batch.PRODUCT_CODE }).Where("STORE=@store", new { store = batch.STORE }));
            float newQTY = itemStockOnHandFound.QTY + batch.QTY;
            db.Execute("UPDATE PRODUCT_STOCKONHAND SET QTY=" + newQTY + " WHERE PRODUCT_CODE='" + itemStockOnHandFound.PRODUCT_CODE + "' AND STORE = '" + itemStockOnHandFound.STORE + "'");
            return batch;
        }

        public PRODUCT_STOCKBATCH UpdateProductBatch(PRODUCT_STOCKBATCH batch)
        {
            var rawBatch = db.SingleOrDefault<PRODUCT_STOCKBATCH>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKBATCH").Where("BATCH_ID=@batchid", new { batchid = batch.BATCH_ID }));
            using (var transScope = db.GetTransaction())
            {
                db.Update(batch);
                transScope.Complete();
            }
            var itemStockOnHandFound = db.SingleOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@prodcode", new { prodcode = batch.PRODUCT_CODE }).Where("STORE=@store", new { store = batch.STORE }));
            float newQTY = itemStockOnHandFound.QTY + (batch.STOCK_ONHAND - rawBatch.STOCK_ONHAND);
            db.Execute("UPDATE PRODUCT_STOCKONHAND SET QTY=" + newQTY + " WHERE PRODUCT_CODE='" + itemStockOnHandFound.PRODUCT_CODE + "' AND STORE = '" + itemStockOnHandFound.STORE + "'");
            return batch;
        }

        public bool RecalculateProductBatch(List<dynamic> batches)
        {
            try {
                dynamic batch = batches[0];
                int sum = 0;
                foreach (dynamic item in batches)
                {
                    if (item.GetType().GetProperty("EXPIRY_DATE") != null)
                    {
                        if (item.EXPIRY_DATE > TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time")) {
                            sum += item.STOCK_ONHAND;
                        }
                    }
                    else {
                        sum += item.STOCK_ONHAND;
                    }
                }
                var itemStockOnHandFound = db.SingleOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@prodcode", new { prodcode = batch.PRODUCT_CODE }).Where("STORE=@store", new { store = batch.STORE }));
                db.Execute("UPDATE PRODUCT_STOCKONHAND SET QTY=" + sum + " WHERE PRODUCT_CODE='" + itemStockOnHandFound.PRODUCT_CODE + "' AND STORE = '" + itemStockOnHandFound.STORE + "'");
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        public int UpdateStockBatch(PRODUCT_SUPPLIERS supplierDetails)
        {
            //validate data
            if (supplierDetails != null)
            {
                if (supplierDetails.SUPPLIER_ID == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(supplierDetails, new string[] { "DESCRIPTION", "ADDRESS", "CONTACT_NO", "CONTACT_PERSON", "CONTACT_OTH"
                    , "STATUS"});

                transScope.Complete();
            }

            return 0;
        }

        public List<dynamic> SearchStockBatchReceive(string queryString, SearchFilter filter, int maxRows = 5000)
        {
			//var a = PetaPoco.Sql.Builder.Append((string)new IndieSearchStockBatchReceive(db._dbType.ToString(), queryString, filter, maxRows));


			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStockBatchReceive(db._dbType.ToString(), queryString, filter, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        public List<dynamic> SearchStockBatchReceiveItems(string queryString, string productCode, int maxRows = 5000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStockBatchReceiveItems(db._dbType.ToString(), queryString, productCode, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        public List<dynamic> SearchProducts(string queryString, string queryStatus, string queryCagetory, int maxRows = 5000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchProducts(db._dbType.ToString(), queryString, queryStatus, queryCagetory, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        public List<dynamic> SearchProdServiceCat(string queryString, string serviceStr, string queryCagetory, int maxRows = 5000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchProdServiceCat(db._dbType.ToString(), queryString, serviceStr, queryCagetory, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        public List<dynamic> SearchStore(string queryString,  int maxRows = 5000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStore(db._dbType.ToString(), queryString,  maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        public List<dynamic> SearchStoreByUser(string queryString, string userId, int maxRows = 5000)
        {
            //List<USER_STORE> itemFound = new List<USER_STORE>; 
            var itemFound = db.Fetch<USER_STORE>(PetaPoco.Sql.Builder.Select("*").From("USER_STORE").Where("USER_ID=@refId", new { refId = userId }));

            if (itemFound.Count() == 0)
            {
                userId = null;
            }

            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStoreByUserId(db._dbType.ToString(), queryString,  userId, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }


        public List<dynamic> SearchSupplier(string queryString, int maxRows = 5000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchSupplier(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        public List<dynamic> SearchPartsOnhand(string queryString, int maxRows = 5000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPartsOnHand(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        public List<dynamic> SearchPartsOnhandListActive(string queryString, int maxRows = 5000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPartsOnHandActive(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }


        public List<dynamic> SearchPartsOnhandByStoreActive(string queryString, string woNo,  int maxRows = 5000)
        {

            var storeId = "";
            var itemBuildingFound = db.SingleOrDefault<WORKORDER_DET_V>(PetaPoco.Sql.Builder.Select("BUILDING").From("WORKORDER_DET_V").Where("req_no=@refId", new { refId = woNo }));

            if(itemBuildingFound != null)
            {
                var itemStoreBuildingFound = db.SingleOrDefault<STORE_BUILDING>(PetaPoco.Sql.Builder.Select("STORE_ID").From("STORE_BUILDING").Where("BUILDING_CODE=@refId", new { refId = itemBuildingFound.BUILDING }));
                if(itemStoreBuildingFound != null)
                {
                    storeId = itemStoreBuildingFound.STORE_ID;
                }
                else
                {
                    storeId = "1";
                }
                
            }
            else
            {
                storeId = "1";
            }
           

            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPartsOnHandByStoreActive(db._dbType.ToString(), queryString, storeId, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }


        public List<dynamic> SearchStockItems(string queryString,string storeStr, string actionStr, int maxRows = 5000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStockItems(db._dbType.ToString(), queryString, actionStr, maxRows),storeStr));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        public List<dynamic> SearchStockItemsOverall(string queryString, string storeStr, string actionStr, int maxRows = 5000)
        {


			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAllStock(db._dbType.ToString(), queryString, actionStr, maxRows), storeStr));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        public List<dynamic> GetStockBatchByProdStore(string queryStore, string queryProductCode, int maxRows = 5000)
        {
            var PRODUCT_STOCKBATCHS = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("PSB.*, SP.description AS SUPPLIERNAME").From("PRODUCT_STOCKBATCH PSB LEFT JOIN AM_SUPPLIER SP ON PSB.SUPPLIER = SP.supplier_id").Where("PSB.STATUS='RECEIVED' and PSB.STORE=@STORE", new { STORE = queryStore }).Where("PSB.PRODUCT_CODE=@PRODUCT_CODE", new { PRODUCT_CODE = queryProductCode }));

            if (PRODUCT_STOCKBATCHS == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return PRODUCT_STOCKBATCHS;
        }

        public AM_STOCKTRANSACTION SearchStockTransactionByProd(string TRANS_ID, string PRODUCT_CODE)
        {
            var amStocktransactions = db.SingleOrDefault<AM_STOCKTRANSACTION>(PetaPoco.Sql.Builder.Select("*").From("AM_STOCKTRANSACTION").Where("TRANS_ID=@TRANS_ID", new { TRANS_ID = TRANS_ID }).Where("ID_PARTS=@PRODUCT_CODE", new { PRODUCT_CODE = PRODUCT_CODE }));

            if (amStocktransactions == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return amStocktransactions;
        }

        public AM_STOCKTRANSACTION_MASTER TransferCustomerRequest(AM_STOCKTRANSACTION_MASTER amStocktransaction, List<PRODUCT_STOCKBATCH> productStockbatchs, List<AM_ASSET_DETAILS> assetDetails,  STAFF_LOGON_SESSIONS userSession)
        {
            using (var transScope = db.GetTransaction())
            {
                if (productStockbatchs.Count() > 0)
                {
                    var amStocktransactions = db.Fetch<AM_STOCKTRANSACTION>(PetaPoco.Sql.Builder.Select("*").From("AM_STOCKTRANSACTION").Where("TRANS_ID=@TRANS_ID AND TRANS_TYPE='SOH'", new { TRANS_ID = amStocktransaction.TRANS_ID }));
                    foreach (var stockTransaction in amStocktransactions)
                    {
                        var filteredproductStockbatchs = productStockbatchs.Where(x => x.PRODUCT_CODE == stockTransaction.ID_PARTS);
                        foreach (var productStockbatch in filteredproductStockbatchs)
                        {
                            var existProductStockbatch = db.FirstOrDefault<PRODUCT_STOCKBATCH>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKBATCH")
                                .Where("PRODUCT_CODE=@PRODUCT_CODE", new { PRODUCT_CODE = productStockbatch.PRODUCT_CODE })
                                .Where("STORE=@STORE", new { STORE = amStocktransaction.STORE_TO.ToString() })
                                .Where("BARCODE_ID=@BARCODE_ID", new { BARCODE_ID = productStockbatch.BARCODE_ID })
                                .Where("TRANS_ID=@TRANS_ID", new { TRANS_ID = amStocktransaction.TRANS_ID })
                                );
                            if (existProductStockbatch != null)
                            {
                                existProductStockbatch.INVOICENO = amStocktransaction.POINVOICENO;
                                existProductStockbatch.QTY += productStockbatch.QTY;
                                existProductStockbatch.QTY_RECEIVED += productStockbatch.QTY;
                                existProductStockbatch.STOCK_ONHAND += productStockbatch.QTY;

                                stockTransaction.QTY_TRANSFER += productStockbatch.QTY;
                                db.Update(existProductStockbatch);
                            }
                            else
                            {
                                var newProductStockbatch = new PRODUCT_STOCKBATCH()
                                {
                                    PRODUCT_CODE = productStockbatch.PRODUCT_CODE,
                                    SUPPLIER = productStockbatch.SUPPLIER,
                                    STORE = amStocktransaction.STORE_TO.ToString(),
                                    EXPIRY_DATE = productStockbatch.EXPIRY_DATE,
                                    QTY = productStockbatch.QTY,
                                    STATUS = "TRANSFER",
                                    CREATED_BY = userSession.USER_ID,
                                    CREATED_DATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time"),
                                    BARCODE_ID = productStockbatch.BARCODE_ID,
                                    TRANS_ID = amStocktransaction.TRANS_ID,
                                    TRANS_DATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time"),
                                    QTY_RECEIVED = productStockbatch.QTY,
                                    INVOICENO = amStocktransaction.POINVOICENO,
                                    STOCK_ONHAND = productStockbatch.QTY
                                };

                                stockTransaction.QTY_TRANSFER += newProductStockbatch.QTY;
                                db.Insert(newProductStockbatch);

                                DateTime cToday = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                                List<ITEM_BATCHES_V> receiveBatches = new List<ITEM_BATCHES_V>();
                                receiveBatches = db.Fetch<ITEM_BATCHES_V>(PetaPoco.Sql.Builder.Select("*").From("ITEM_BATCHES_V").Where("BATCH_ID=@batchId", new { batchId = newProductStockbatch.BATCH_ID }));


                                if(receiveBatches.Count == 1)
                                {
                                    var systemTransactionDetails = new SYSTEM_TRANSACTIONDETAILS()
                                    {
                                   
                                        REFTRANSID = receiveBatches[0].BATCH_ID,
                                        REFCODE = productStockbatch.PRODUCT_CODE,
                                        REFDESCRIPTION = receiveBatches[0].PRODUCT_NAME,
                                        MESSAGE = receiveBatches[0].STOCK_ONHAND + " " + receiveBatches[0].UO_DESC + " of " + receiveBatches[0].PRODUCT_CODE + " " + receiveBatches[0].PRODUCT_NAME + " with invoice number (" + receiveBatches[0].INVOICENO + ") and batch number (" + receiveBatches[0].BARCODE_ID + ") is already transfer on " + String.Format("{0:dd/MM/yyyy}", cToday) + ",   waiting   " + receiveBatches[0].STORE_DESC + " to received.",
                                        COSTCENTERMESSAGE = "",
                                        STATUS = "Y",
                                        REFEXPIRY = receiveBatches[0].EXPIRY_DATE,
                                        REFBATCHSERIALNO = receiveBatches[0].BARCODE_ID,
                                        REFINVOICENO = receiveBatches[0].INVOICENO,
                                        REFCOST = 0,
                                        REFQTY = productStockbatch.QTY,
                                        REFUOM = receiveBatches[0].UO_DESC,
                                        REFCOSTCENTERCODE = receiveBatches[0].CLIENT_CODE,
                                        REFCOSTCENTERDESCRIPTION = receiveBatches[0].CLIENT_NAME,
                                        REASONTYPE = "Transfer",
                                        REFTYPE = receiveBatches[0].ITEMTYPE,
                                        CREATEDBY = userSession.USER_ID,
                                        CREATEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time")

                                    };
                                    db.Insert(systemTransactionDetails);
                                }
								
								
								
                            }

                            var updateProductStockbatch = db.FirstOrDefault<PRODUCT_STOCKBATCH>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKBATCH").Where("BATCH_ID=@BATCH_ID", new { BATCH_ID = productStockbatch.BATCH_ID }));
                            updateProductStockbatch.STOCK_ONHAND -= productStockbatch.QTY;
                            db.Update(updateProductStockbatch);

                            var updateProductStockonhand = db.FirstOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@PRODUCT_CODE", new { PRODUCT_CODE = productStockbatch.PRODUCT_CODE }).Where("STORE=@STORE", new { STORE = amStocktransaction.STORE_FROM.ToString() }));
                            updateProductStockonhand.QTY -= productStockbatch.QTY;
                            if (updateProductStockonhand.QTY < 0) updateProductStockonhand.QTY = 0;
                            db.Execute("UPDATE PRODUCT_STOCKONHAND SET QTY = " + updateProductStockonhand.QTY + " WHERE PRODUCT_CODE ='" + updateProductStockonhand.PRODUCT_CODE + "' AND STORE = '" + updateProductStockonhand.STORE + "'");
                        }

                        stockTransaction.INVOICENO = amStocktransaction.POINVOICENO;
                        stockTransaction.TRANSFER_REMARKS = amStocktransaction.TRANSFER_REMARKS;
                        db.Update(stockTransaction);
                    }
                }

                if (assetDetails.Count() > 0)
                {
                    var stockTransaction = db.FirstOrDefault<AM_STOCKTRANSACTION>(PetaPoco.Sql.Builder.Select("*").From("AM_STOCKTRANSACTION").Where("TRANS_ID=@TRANS_ID", new { TRANS_ID = amStocktransaction.TRANS_ID }).Where("ID_PARTS=@ID_PARTS AND TRANS_TYPE='ASSET'", new { ID_PARTS = assetDetails[0].PRODUCT_CODE }));

                    stockTransaction.QTY_TRANSFER += assetDetails.Count();

                    stockTransaction.INVOICENO = amStocktransaction.POINVOICENO;
                    stockTransaction.TRANSFER_REMARKS = amStocktransaction.TRANSFER_REMARKS;
                    db.Update(stockTransaction);
                }

                var hasPartialTransfer = false;
                var stockTransactionList = db.Fetch<AM_STOCKTRANSACTION>(PetaPoco.Sql.Builder.Select("*").From("AM_STOCKTRANSACTION").Where("TRANS_ID=@TRANS_ID", new { TRANS_ID = amStocktransaction.TRANS_ID }));
                for (int i = 0; i < stockTransactionList.Count(); i++)
                {
                    if (stockTransactionList[i].QTY_TRANSFER < stockTransactionList[i].QTY)
                    {
                        hasPartialTransfer = true;
                        break;
                    }
                }

                if (hasPartialTransfer)
                {
                    db.Execute("UPDATE AM_STOCKTRANSACTION SET TRANS_STATUS='PARTIAL TRANSFER' WHERE TRANS_ID=" + amStocktransaction.TRANS_ID);
                    db.Execute("UPDATE AM_STOCKTRANSACTION_MASTER SET TRANS_STATUS='PARTIAL TRANSFER', POINVOICENO='" + amStocktransaction.POINVOICENO + "', TRANSFER_REMARKS='" + amStocktransaction.TRANSFER_REMARKS + "' WHERE TRANS_ID=" + amStocktransaction.TRANS_ID);
                }
                else
                {
                    db.Execute("UPDATE AM_STOCKTRANSACTION SET TRANS_STATUS='TRANSFER' WHERE TRANS_ID=" + amStocktransaction.TRANS_ID);
                    db.Execute("UPDATE AM_STOCKTRANSACTION_MASTER SET TRANS_STATUS='TRANSFER', POINVOICENO='" + amStocktransaction.POINVOICENO + "', TRANSFER_REMARKS='" + amStocktransaction.TRANSFER_REMARKS + "' WHERE TRANS_ID=" + amStocktransaction.TRANS_ID);
                }

                transScope.Complete();
            }
            return amStocktransaction;
        }

        public PRODUCT_STOCKBATCH ReturnRecallStockBatch(PRODUCT_STOCKBATCH productStockbatch, STAFF_LOGON_SESSIONS userSession)
        {
            var updateProductStockbatch = new PRODUCT_STOCKBATCH();
            using (var transScope = db.GetTransaction())
            {
                updateProductStockbatch = db.FirstOrDefault<PRODUCT_STOCKBATCH>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKBATCH").Where("BATCH_ID=@BATCH_ID", new { BATCH_ID = productStockbatch.BATCH_ID }));
                if (productStockbatch.STATUS == "RETURNED")
                {
                    updateProductStockbatch.STATUS = productStockbatch.STATUS;
                    if (updateProductStockbatch.PURCHASE_NO == null || updateProductStockbatch.PURCHASE_NO == "")
                    {
                        var amStocktransaction = db.FirstOrDefault<AM_STOCKTRANSACTION>(PetaPoco.Sql.Builder.Select("*").From("AM_STOCKTRANSACTION").Where("TRANS_ID=@TRANS_ID", new { TRANS_ID = updateProductStockbatch.TRANS_ID }).Where("ID_PARTS=@ID_PARTS", new { ID_PARTS = updateProductStockbatch.PRODUCT_CODE }));

                        var updateProductStockonhand = db.FirstOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@PRODUCT_CODE", new { PRODUCT_CODE = productStockbatch.PRODUCT_CODE }).Where("STORE=@STORE", new { STORE = amStocktransaction.STORE_FROM.ToString() }));
                        updateProductStockonhand.QTY += Convert.ToInt16(updateProductStockbatch.RETURN_QTY);
                        db.Execute("UPDATE PRODUCT_STOCKONHAND SET QTY = " + updateProductStockonhand.QTY + " WHERE PRODUCT_CODE ='" + updateProductStockonhand.PRODUCT_CODE + "' AND STORE = '" + updateProductStockonhand.STORE + "'");
                    }

                    db.Update(updateProductStockbatch);
                }
                else
                {
                    updateProductStockbatch.SUPPLIER = productStockbatch.SUPPLIER;
                    updateProductStockbatch.RETURN_REASON = productStockbatch.RETURN_REASON;
                    updateProductStockbatch.RETURN_TYPE = productStockbatch.RETURN_TYPE;
                    updateProductStockbatch.STATUS = productStockbatch.STATUS;
                    updateProductStockbatch.RETURN_QTY = updateProductStockbatch.STOCK_ONHAND;
                    db.Update(updateProductStockbatch);

                    var updateProductStockonhand = db.FirstOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@PRODUCT_CODE", new { PRODUCT_CODE = updateProductStockbatch.PRODUCT_CODE }).Where("STORE=@STORE", new { STORE = updateProductStockbatch.STORE }));
                    updateProductStockonhand.QTY -= Convert.ToInt16(updateProductStockbatch.RETURN_QTY);
                    if (updateProductStockonhand.QTY < 0) updateProductStockonhand.QTY = 0;
                    db.Execute("UPDATE PRODUCT_STOCKONHAND SET QTY = " + updateProductStockonhand.QTY + " WHERE PRODUCT_CODE ='" + updateProductStockonhand.PRODUCT_CODE + "' AND STORE = '" + updateProductStockonhand.STORE + "'");

                    var store = db.FirstOrDefault<STORES>(PetaPoco.Sql.Builder.Select("*").From("STORES").Where("STORE_ID=@STORE_ID", new { STORE_ID = updateProductStockonhand.STORE.ToString() }));
                    var clientV = db.FirstOrDefault<CLIENT_V>(PetaPoco.Sql.Builder.Select("*").From("CLIENT_V").Where("CLIENT_CODE=@CLIENT_CODE", new { CLIENT_CODE = store.CLIENT_CODE }));
                    var amParts = db.FirstOrDefault<AM_PARTS>(PetaPoco.Sql.Builder.Select("*").From("AM_PARTS").Where("ID_PARTS=@PRODUCT_CODE", new { PRODUCT_CODE = updateProductStockbatch.PRODUCT_CODE }));
                    var lovLookupsProductType = db.FirstOrDefault<LOV_LOOKUPS>(PetaPoco.Sql.Builder.Select("DESCRIPTION").From("LOV_LOOKUPS").Where("LOV_LOOKUP_ID=@TYPE_ID", new { TYPE_ID = amParts.TYPE_ID }).Where("CATEGORY=@CATEGORY", new { CATEGORY = "AIMS_MSTYPE" }));
                    var lovLookupsUOM = db.FirstOrDefault<LOV_LOOKUPS>(PetaPoco.Sql.Builder.Select("DESCRIPTION").From("LOV_LOOKUPS").Where("LOV_LOOKUP_ID=@UOM_ID", new { UOM_ID = amParts.UOM_ID }).Where("CATEGORY=@catId", new { catId = "AIMS_MSUOM" }));

                    var arabDateTimeNow = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                    var invoiceNo = String.IsNullOrEmpty(updateProductStockbatch.INVOICENO) ? "N/A" : updateProductStockbatch.INVOICENO;
                    var statusMessage = updateProductStockbatch.STATUS == "RECALL" ? "Recalled" : updateProductStockbatch.STATUS;
                    var systemTransactionDetails = new SYSTEM_TRANSACTIONDETAILS()
                    {
                        REFTRANSID = updateProductStockbatch.BATCH_ID,
                        REFCODE = updateProductStockbatch.PRODUCT_CODE,
                        REFDESCRIPTION = amParts.DESCRIPTION,
                        MESSAGE = $"{updateProductStockbatch.RETURN_QTY} {lovLookupsUOM.DESCRIPTION} with batch no. ({updateProductStockbatch.BARCODE_ID}) and invoice no. ({invoiceNo}) was {statusMessage} from {store.DESCRIPTION} on {arabDateTimeNow}",
                        COSTCENTERMESSAGE = $"{updateProductStockbatch.RETURN_QTY} {lovLookupsUOM.DESCRIPTION} with batch no. ({updateProductStockbatch.BARCODE_ID}) and invoice no. ({invoiceNo}) was {statusMessage} on your store on {arabDateTimeNow}",
                        STATUS = "Y",
                        REFEXPIRY = updateProductStockbatch.EXPIRY_DATE,
                        REFBATCHSERIALNO = updateProductStockbatch.BARCODE_ID,
                        REFINVOICENO = updateProductStockbatch.INVOICENO,
                        REFCOST = updateProductStockbatch.TOTAL_PRICE,
                        REFQTY = updateProductStockbatch.RETURN_QTY,
                        REFUOM = lovLookupsUOM != null ? lovLookupsUOM.DESCRIPTION : "",
                        REFCOSTCENTERCODE = store.CLIENT_CODE,
                        REFCOSTCENTERDESCRIPTION = clientV.Description,
                        REASONTYPE = updateProductStockbatch.STATUS,
                        REFTYPE = lovLookupsProductType != null ? lovLookupsProductType.DESCRIPTION : "",
                        CREATEDBY = userSession.USER_ID,
                        CREATEDDATE = arabDateTimeNow
                    };
                    db.Insert(systemTransactionDetails);
                }

                transScope.Complete();
            }
            return updateProductStockbatch;
        }

        public List<ITEM_BATCHES_V> SearchItemBatchesV(string searchKey, int maxRows = 5000)
        {
            if (String.IsNullOrEmpty(searchKey))
                return new List<ITEM_BATCHES_V>();

            return db.Fetch<ITEM_BATCHES_V>(PetaPoco.Sql.Builder.Select("*").From("ITEM_BATCHES_V").Where("SYSREPORTID is null and BARCODE_ID='"+ searchKey+ "' OR MODELCODE ='"+ searchKey+"'"));
        }

        public List<dynamic> GetItemBatchesVByReport(string sysReportId, SearchFilter filter, int maxRows = 5000)
        {
            var ItemBatchesV = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieGetItemBatchesVByReport(db._dbType.ToString(), "", filter, maxRows)));

            if (ItemBatchesV == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return ItemBatchesV;
        }

        public SYSTEMTRANSACTION_REPORT CreatSysTransReport(SYSTEMTRANSACTION_REPORT systemTransactionReport, List<ASSET_V> transItems1, List<ITEM_BATCHES_V> transItems2, List<SYSTEMTRANSACTION_REPORT_SCANDOC> systemTransactionReportScandocs, STAFF_LOGON_SESSIONS userSession, string codeId)
        {
            using (var transScope = db.GetTransaction())
            {
                var arabDateTimeNow = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
				DateTime? cToday = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                if (systemTransactionReport.ID != 0)
                {
                    if (systemTransactionReport.REPORTSTATUS == "For Action")
                    {
                        systemTransactionReport.REPORTEDBY = userSession.USER_ID;
                        systemTransactionReport.REPORTEDDATE = arabDateTimeNow;

                        db.Execute("UPDATE SYSTEM_TRANSACTIONDETAILS SET REPORTSTATUS='" + systemTransactionReport.REPORTSTATUS + "' WHERE SYSREPORTID='" + systemTransactionReport.ID + "'");
                    }

                    db.Update(systemTransactionReport);
                }
                else
                {
                    if (systemTransactionReport.REPORTSTATUS == "For Action")
                    {
                        systemTransactionReport.REPORTEDBY = userSession.USER_ID;
                        systemTransactionReport.REPORTEDDATE = arabDateTimeNow;

                        db.Execute("UPDATE SYSTEM_TRANSACTIONDETAILS SET REPORTSTATUS='" + systemTransactionReport.REPORTSTATUS + "' WHERE SYSREPORTID='" + systemTransactionReport.ID + "'");
                    }
                    else
                        systemTransactionReport.REPORTSTATUS = "For Reporting";

                    systemTransactionReport.CREATEDBY = userSession.USER_ID;
                    systemTransactionReport.CREATEDDATE = arabDateTimeNow;

                    db.Insert(systemTransactionReport);

                    var recallType = db.FirstOrDefault<string>(PetaPoco.Sql.Builder.Select("DESCRIPTION").From("LOV_LOOKUPS").Where("LOV_LOOKUP_ID=@LOV_LOOKUP_ID", new { LOV_LOOKUP_ID = systemTransactionReport.RECALLTYPE }).Where("CATEGORY=@CATEGORY", new { CATEGORY = "STOCKBATCH_STATUSTYPE" }));
                    var severity = db.FirstOrDefault<string>(PetaPoco.Sql.Builder.Select("DESCRIPTION").From("LOV_LOOKUPS").Where("LOV_LOOKUP_ID=@LOV_LOOKUP_ID", new { LOV_LOOKUP_ID = systemTransactionReport.HHLEVEL }).Where("CATEGORY=@CATEGORY", new { CATEGORY = "HH-Level" }));
                    var QSA = db.FirstOrDefault<string>(PetaPoco.Sql.Builder.Select("DESCRIPTION").From("LOV_LOOKUPS").Where("LOV_LOOKUP_ID=@LOV_LOOKUP_ID", new { LOV_LOOKUP_ID = systemTransactionReport.QSA }).Where("CATEGORY=@CATEGORY", new { CATEGORY = "QSA" }));
                    var DDSA = db.FirstOrDefault<string>(PetaPoco.Sql.Builder.Select("DESCRIPTION").From("LOV_LOOKUPS").Where("LOV_LOOKUP_ID=@LOV_LOOKUP_ID", new { LOV_LOOKUP_ID = systemTransactionReport.DDSA }).Where("CATEGORY=@CATEGORY", new { CATEGORY = "DDSA" }));
                    var recallBy = db.FirstOrDefault<string>(PetaPoco.Sql.Builder.Select("DESCRIPTION").From("LOV_LOOKUPS").Where("LOV_LOOKUP_ID=@LOV_LOOKUP_ID", new { LOV_LOOKUP_ID = systemTransactionReport.RECALLCATEGORY }).Where("CATEGORY=@CATEGORY", new { CATEGORY = "RECALLBY" }));
                    var staff = db.FirstOrDefault<STAFF>(PetaPoco.Sql.Builder.Select("*").From("STAFF").Where("USER_ID=@USER_ID", new { USER_ID = userSession.USER_ID }));
                    var staffPosition = db.FirstOrDefault<STAFF_POSITIONS>(PetaPoco.Sql.Builder.Select("*").From("STAFF_POSITIONS").Where("STAFF_POSITION_ID=@STAFF_POSITION_ID", new { STAFF_POSITION_ID = staff.STAFF_POSITION_ID }));
                    var clientInfo = db.FirstOrDefault<AM_CLIENT_INFO>(PetaPoco.Sql.Builder.Select("*").From("AM_CLIENT_INFO").Where("CLIENT_CODE=@CLIENT_CODE", new { CLIENT_CODE = staff.CLIENT_CODE }));

                    var costCenters = new List<string>();
                    var products = new List<string>();
                    var TDOPD = "N/A";
                    foreach (var item in transItems1)
                    {
                        var updateAMAssetDetails = db.FirstOrDefault<AM_ASSET_DETAILS>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_DETAILS").Where("ASSET_NO=@ASSET_NO", new { ASSET_NO = item.ASSET_NO }));
                        updateAMAssetDetails.SYSREPORTID = systemTransactionReport.ID;
                        updateAMAssetDetails.RECALLFLAG = true;
                        updateAMAssetDetails.COST_CENTER = codeId;
                        db.Update(updateAMAssetDetails);

                        
						
                        var updateAMAssetTransaction = db.FirstOrDefault<AM_ASSET_TRANSACTION>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_TRANSACTION").Where("ASSET_NO=@ASSET_NO", new { ASSET_NO = item.ASSET_NO }).Where("TRANSID=@COSTCENTERTRANSID", new { COSTCENTERTRANSID = updateAMAssetDetails.COSTCENTERTRANSID }));
                         var costcenterdescriptionFound = new CLIENT_COSTCENTER_V();
                        if (updateAMAssetTransaction != null)
                        {
                            updateAMAssetTransaction.RETURNEDREASON = "RECALL";
                            updateAMAssetTransaction.RETURNEDREMARKS = systemTransactionReport.REASON;
                            updateAMAssetTransaction.RETURNCREATEDBY = userSession.USER_ID;
                            updateAMAssetTransaction.RETURNCREATEDDATE = arabDateTimeNow;
                            updateAMAssetTransaction.RETURNEDTYPE = systemTransactionReport.RECALLTYPE;
                            updateAMAssetTransaction.SYSREPORTID = systemTransactionReport.ID;
                            db.Update(updateAMAssetTransaction);

                            //cToday = updateAMAssetTransaction.COSTCENTERPURCHASEDATE;
                            TDOPD = updateAMAssetTransaction.COSTCENTERPURCHASEDATE.HasValue ? updateAMAssetTransaction.COSTCENTERPURCHASEDATE.Value.ToString("dd/MM/yyyy") : "N/A";

                            costcenterdescriptionFound = db.FirstOrDefault<CLIENT_COSTCENTER_V>(PetaPoco.Sql.Builder.Select("Description").From("CLIENT_COSTCENTER_V").Where("CostCenter=@reqId", new { reqId = updateAMAssetTransaction.COSTCENTERCODE }));
                        }

                        SYSTEM_TRANSACTIONDETAILS systemTrans = new SYSTEM_TRANSACTIONDETAILS();
                        var costcennterName = "";
                        if (costcenterdescriptionFound != null)
                        {
                            costcennterName = costcenterdescriptionFound.Description;
                        }
                        var COSTCENTERCODE = "";
                        if (updateAMAssetTransaction != null)
                        {
                            COSTCENTERCODE = updateAMAssetTransaction.COSTCENTERCODE;
                        }
                        decimal? COSTCENTERPURCHASECOST = 0;
                        if (updateAMAssetTransaction != null)
                        {
                            COSTCENTERPURCHASECOST = updateAMAssetTransaction.COSTCENTERPURCHASECOST;
                        }
                        var COSTCENTERINVOICENO = "";
                        if (updateAMAssetTransaction != null)
                        {
                            COSTCENTERINVOICENO = updateAMAssetTransaction.COSTCENTERINVOICENO;
                        }

                        systemTrans.REFCODE = updateAMAssetDetails.ASSET_NO;
                        systemTrans.REFDESCRIPTION = updateAMAssetDetails.DESCRIPTION;
                        systemTrans.REFCOSTCENTERCODE = COSTCENTERCODE;
                        systemTrans.REFCOST = COSTCENTERPURCHASECOST;
                        systemTrans.REFCOSTCENTERDESCRIPTION = costcennterName;
                        systemTrans.REFMODEL = updateAMAssetDetails.MODEL_NO;
                        systemTrans.REFBATCHSERIALNO = updateAMAssetDetails.SERIAL_NO;
                        systemTrans.REFINVOICENO = COSTCENTERINVOICENO;
                        systemTrans.REASONTYPE = "RECALL";
                        systemTrans.REFTYPE = "Asset";
                        systemTrans.REFQTY = 1;
                        systemTrans.REFUOM = "Piece";
                        systemTrans.REPORTSTATUS = systemTransactionReport.REPORTSTATUS;
                        systemTrans.COSTCENTERMESSAGE = "This equipment was RECALL from " + costcennterName + " on " + String.Format("{0:dd/MM/yyyy}", cToday) + ".";
                        systemTrans.MESSAGE = "This equipment was RECALL from " + costcennterName + " on " + String.Format("{0:dd/MM/yyyy}", cToday) + ".";
                        systemTrans.STATUS = "Y";
                        systemTrans.CREATEDBY = userSession.USER_ID;
                        systemTrans.CREATEDDATE = arabDateTimeNow;
                        systemTrans.SYSREPORTID = systemTransactionReport.ID;
                        db.Insert(systemTrans);

                        var found = costCenters.Find(x => x == item.COST_CENTER);
                        if (found == null)
                            costCenters.Add(item.COST_CENTER);

                        var foundProduct = products.Find(x => x == item.DESCRIPTION);
                        if (found == null)
                            products.Add(item.DESCRIPTION);
                    }
                    foreach (var item in transItems2)
                    {
                        var updateProductStockbatch = db.FirstOrDefault<PRODUCT_STOCKBATCH>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKBATCH").Where("BATCH_ID=@BATCH_ID", new { BATCH_ID = item.BATCH_ID }));
                        updateProductStockbatch.RETURN_REASON = systemTransactionReport.REASON;
                        updateProductStockbatch.RETURN_TYPE = systemTransactionReport.RECALLTYPE;
                        updateProductStockbatch.STATUS = "RECALL";
                        updateProductStockbatch.RETURN_QTY = updateProductStockbatch.STOCK_ONHAND;
                        updateProductStockbatch.SYSREPORTID = systemTransactionReport.ID;
                        db.Update(updateProductStockbatch);

                        TDOPD = updateProductStockbatch.TRANS_DATE.HasValue ? updateProductStockbatch.TRANS_DATE.Value.ToString("dd/MM/yyyy") : "N/A";

                        var store = db.FirstOrDefault<STORES>(PetaPoco.Sql.Builder.Select("*").From("STORES").Where("STORE_ID=@STORE_ID", new { STORE_ID = updateProductStockbatch.STORE.ToString() }));
                        var clientV = db.FirstOrDefault<CLIENT_V>(PetaPoco.Sql.Builder.Select("*").From("CLIENT_V").Where("CLIENT_CODE=@CLIENT_CODE", new { CLIENT_CODE = store.CLIENT_CODE }));
                        var amParts = db.FirstOrDefault<AM_PARTS>(PetaPoco.Sql.Builder.Select("*").From("AM_PARTS").Where("ID_PARTS=@PRODUCT_CODE", new { PRODUCT_CODE = updateProductStockbatch.PRODUCT_CODE }));
                        var lovLookupsProductType = db.FirstOrDefault<LOV_LOOKUPS>(PetaPoco.Sql.Builder.Select("DESCRIPTION").From("LOV_LOOKUPS").Where("LOV_LOOKUP_ID=@TYPE_ID", new { TYPE_ID = amParts.TYPE_ID }).Where("CATEGORY=@CATEGORY", new { CATEGORY = "AIMS_MSTYPE" }));
                        var lovLookupsUOM = db.FirstOrDefault<LOV_LOOKUPS>(PetaPoco.Sql.Builder.Select("DESCRIPTION").From("LOV_LOOKUPS").Where("LOV_LOOKUP_ID=@UOM_ID", new { UOM_ID = amParts.UOM_ID }).Where("CATEGORY=@catId", new { catId = "AIMS_MSUOM" }));

                        var invoiceNo = String.IsNullOrEmpty(updateProductStockbatch.INVOICENO) ? "N/A" : updateProductStockbatch.INVOICENO;
                        var statusMessage = updateProductStockbatch.STATUS == "RECALL" ? "Recalled" : updateProductStockbatch.STATUS;
                        var systemTransactionDetails = new SYSTEM_TRANSACTIONDETAILS()
                        {
                            REFTRANSID = updateProductStockbatch.BATCH_ID,
                            REFCODE = updateProductStockbatch.PRODUCT_CODE,
                            REFDESCRIPTION = amParts.DESCRIPTION,
                            MESSAGE = $"{updateProductStockbatch.RETURN_QTY} {lovLookupsUOM.DESCRIPTION} with batch no. ({updateProductStockbatch.BARCODE_ID}) and invoice no. ({invoiceNo}) was {statusMessage} from {store.DESCRIPTION} on" + String.Format("{0:dd/MM/yyyy}", cToday) + ".",
                            COSTCENTERMESSAGE = $"{updateProductStockbatch.RETURN_QTY} {lovLookupsUOM.DESCRIPTION} with batch no. ({updateProductStockbatch.BARCODE_ID}) and invoice no. ({invoiceNo}) was {statusMessage} on your store on {arabDateTimeNow}",
                            STATUS = "Y",
                            REFEXPIRY = updateProductStockbatch.EXPIRY_DATE,
                            REFBATCHSERIALNO = updateProductStockbatch.BARCODE_ID,
                            REFINVOICENO = updateProductStockbatch.INVOICENO,
                            REFMODEL = updateProductStockbatch.MODELCODE,
                            REFCOST = updateProductStockbatch.TOTAL_PRICE,
                            REFQTY = updateProductStockbatch.RETURN_QTY,
                            REFUOM = lovLookupsUOM != null ? lovLookupsUOM.DESCRIPTION : "",
                            REFCOSTCENTERCODE = store.CLIENT_CODE,
                            REFCOSTCENTERDESCRIPTION = clientV.Description,
                            REASONTYPE = updateProductStockbatch.STATUS,
                            REFTYPE = lovLookupsProductType != null ? lovLookupsProductType.DESCRIPTION : "",
                            CREATEDBY = userSession.USER_ID,
                            CREATEDDATE = arabDateTimeNow,
                            REPORTSTATUS = systemTransactionReport.REPORTSTATUS,
                            SYSREPORTID = systemTransactionReport.ID
                        };
                        db.Insert(systemTransactionDetails);

                        var found = costCenters.Find(x => x == item.CLIENT_CODE);
                        if (found == null)
                            costCenters.Add(item.CLIENT_CODE);

                        var foundProduct = products.Find(x => x == item.PRODUCT_NAME);
                        if (found == null)
                            products.Add(item.PRODUCT_NAME);
                    }

                    foreach (var item in costCenters)
                    {
                        var clientV = new CLIENT_COSTCENTER_V();
                        if (transItems1.Count() > 0)
                            clientV = db.FirstOrDefault<CLIENT_COSTCENTER_V>(PetaPoco.Sql.Builder.Select("*").From("CLIENT_COSTCENTER_V").Where("CostCenter=@COSTCENTER", new { COSTCENTER = item }));
                        else if (transItems2.Count() > 0)
                            clientV = db.FirstOrDefault<CLIENT_COSTCENTER_V>(PetaPoco.Sql.Builder.Select("*").From("CLIENT_COSTCENTER_V").Where("CLIENT_CODE=@CLIENT_CODE", new { CLIENT_CODE = item }));

                        var emailMessage = "";
                        var smsMessage = "";
                        var dashboardMessage = "";
                        var clientContactNo = clientV.ContactNo != null ? clientV.ContactNo : clientV.ContactNo2;
                        var clientEmail = clientV.EmailAddress;

                        var notifySetup = db.FirstOrDefault<AM_NOTIFYSETUP>(PetaPoco.Sql.Builder.Select("*").From("AM_NOTIFYSETUP").Where("NOTIFY_ID=10"));
                        if (clientV != null && notifySetup != null && notifySetup.STATUS == "Y")
                        {
                            if (notifySetup.EMAIL_FLAG == "Y" && !String.IsNullOrEmpty(clientEmail))
                            {
                                var mailFrom = "bst.hims.ph@gmail.com";
                                var mailTo = clientEmail;
                                var mailSubject = notifySetup.DESCRIPTION;
                                var mailBody = notifySetup.EMAIL_MESSAGE;

                                //mailBody = replaceValue(mailBody, "<Company Letter Head>", "");
                                mailBody = replaceValue(mailBody, "<FOOD, DRUG, MEDICAL DEVICE, BIOLOGIC, COSMETIC, etc.>", String.Join(", ", products));

                                mailBody = replaceValue(mailBody, "<Recall Date>", systemTransactionReport.CREATEDDATE.ToString("dd/MM/yyyy"));

                                mailBody = replaceValue(mailBody, "<Contact Name or Department>", clientInfo.CONTACT_PERSON);
                                mailBody = replaceValue(mailBody, "<Company Name>", clientInfo.DESCRIPTION);
                                mailBody = replaceValue(mailBody, "<Address>", $"{clientInfo.ADDRESS} {clientInfo.CITY} {clientInfo.ZIPCODE}, {clientInfo.STATE}");

                                mailBody = replaceValue(mailBody, "<ItemCode, Name, Model Name, Serial No>", $"{String.Join(", ", products)}, {systemTransactionReport.SEARCHKEY}");

                                mailBody = replaceValue(mailBody, "<Recall Type>", recallType);
                                mailBody = replaceValue(mailBody, "<Reason>", systemTransactionReport.REASON);
                                mailBody = replaceValue(mailBody, "<Severity>", severity);
                                mailBody = replaceValue(mailBody, "<TransferedDate or PurchasedDate>", TDOPD);
                                mailBody = replaceValue(mailBody, "<Quantity in Saudi Arabia>", QSA);
                                mailBody = replaceValue(mailBody, "<Depth of Distribution in Saudi Arabia>", DDSA);
                                mailBody = replaceValue(mailBody, "<Recall By>", recallBy);
                                mailBody = replaceValue(mailBody, "<Response Time Frame>", systemTransactionReport.RECALLTIMEFRAME);
                                mailBody = replaceValue(mailBody, "<Recall ID>", systemTransactionReport.ID);

                                mailBody = replaceValue(mailBody, "<Staff Name>", $"{staff.FIRST_NAME} {staff.LAST_NAME}");
                                mailBody = replaceValue(mailBody, "<Contact No>", staff.CONTACT_PRIMARY);
                                mailBody = replaceValue(mailBody, "<Position>", staffPosition != null ? staffPosition.DESCRIPTION : "N/A");
                                mailBody = replaceValue(mailBody, "<Service Department>", staff.SRVC_DEPT);

                                emailMessage = mailBody;

                                MailMessage mm = new MailMessage(mailFrom, mailTo);
                                mm.BodyEncoding = UTF8Encoding.UTF8;
                                mm.IsBodyHtml = true;
                                mm.AlternateViews.Add(getEmbeddedImage(clientV.LogoPath, mailBody));
                                mm.Subject = mailSubject;
                                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                                client.UseDefaultCredentials = false;

                                client.Credentials = new System.Net.NetworkCredential("bst.hims.ph@gmail.com", "bS123456");
                                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                                client.EnableSsl = true;
                                client.Timeout = 10000;

                                client.Send(mm);
                            }

                            if (notifySetup.SMS_FLAG == "Y" && clientContactNo != null)
                            {
                                var message = notifySetup.SMS_MESSAGE;

                                message = replaceValue(message, "<ItemCode, Name, Model Name, Serial No>", $"{String.Join(", ", products)}, {systemTransactionReport.SEARCHKEY}");
                                message = replaceValue(message, "<Recall Date>", systemTransactionReport.CREATEDDATE.ToString("dd/MM/yyyy"));
                                message = replaceValue(message, "<Response Time Frame>", systemTransactionReport.RECALLTIMEFRAME);
                                message = replaceValue(message, "<Recall ID>", systemTransactionReport.ID);

                                smsMessage = message;

                                GetCreditResult smsContentx = new GetCreditResult();

                                smsContentx.Username = "966505613305";
                                smsContentx.Password = "bste1234";
                                smsContentx.Tagname = "BSAIMS";
                                smsContentx.VariableList = "";
                                smsContentx.ReplacementList = "";
                                smsContentx.SendDateTime = "0";
                                smsContentx.EnableDR = false;
                                smsContentx.RecepientNumber = clientContactNo;
                                smsContentx.Message = message;

                                HttpClient client = new HttpClient();
                                client.BaseAddress = new Uri("http://api.yamamah.com");

                                // Add an Accept header for JSON format.
                                client.DefaultRequestHeaders.Accept.Clear();
                                client.DefaultRequestHeaders.Accept.Add(
                                new MediaTypeWithQualityHeaderValue("application/json"));

                                // List data response.
                                HttpResponseMessage response = client.PostAsJsonAsync("/SendSMS", smsContentx).Result;
                                if (response.IsSuccessStatusCode)
                                    response.Content.ReadAsStringAsync();
                            }

                            if (notifySetup.DASHBOARD_FLAG == "Y")
                            {
                                dashboardMessage = notifySetup.DASHBOARD_MESSAGE;

                                dashboardMessage = replaceValue(dashboardMessage, "<ItemCode, Name, Model Name, Serial No>", $"{String.Join(", ", products)}, {systemTransactionReport.SEARCHKEY}");
                                dashboardMessage = replaceValue(dashboardMessage, "<Recall Date>", systemTransactionReport.CREATEDDATE.ToString("dd/MM/yyyy"));
                                dashboardMessage = replaceValue(dashboardMessage, "<Recall Type>", recallType);
                                dashboardMessage = replaceValue(dashboardMessage, "<Reason>", systemTransactionReport.REASON);
                                dashboardMessage = replaceValue(dashboardMessage, "<Response Time Frame>", systemTransactionReport.RECALLTIMEFRAME);
                                dashboardMessage = replaceValue(dashboardMessage, "<Recall ID>", systemTransactionReport.ID);
                            }

                            var staffCostcenterBuildings = db.Fetch<STAFF_COSTCENTER_BUILDING>(PetaPoco.Sql.Builder.Select("*").From("STAFF_COSTCENTER_BUILDING").Where("UCATEGORY='AIMS_COSTCENTER' and CODE=@COSTCENTER", new { COSTCENTER = clientV.CostCenter }));
                            foreach (var staffCCB in staffCostcenterBuildings)
                            {
                                var notifyMessage = new AM_NOTIFY_MESSAGES()
                                {
                                    NOTIFY_ID = notifySetup.NOTIFY_ID,
                                    SMS_MESSAGE = smsMessage,
                                    EMAIL_MESSAGE = emailMessage,
                                    MOBILENO = clientContactNo,
                                    EMAIL_ADDRESS = clientEmail,
                                    SMS_FLAG = notifySetup.SMS_FLAG,
                                    EMAIL_FLAG = notifySetup.EMAIL_FLAG,
                                    STAFF_ID = staffCCB.STAFFCODE,
                                    NOTIFY_MESSAGE = dashboardMessage,
                                    DASHBOARD_FLAG = notifySetup.DASHBOARD_FLAG,
                                    STATUS = "A",
                                    CREATED_BY = "SYSTEM",
                                    CREATED_DATE = arabDateTimeNow,
                                    REF_CODE = ""
                                };
                                db.Insert(notifyMessage);
                            }
                        }
                    }

                    foreach (var item in systemTransactionReportScandocs)
                    {
                        item.SYSREPORTID = systemTransactionReport.ID;
                        item.DOCPATH = systemTransactionReport.ID + "_" + item.DOCPATH;
                        db.Insert(item);
                    }

                    transScope.Complete();
                }
                return systemTransactionReport;
            }
        }

        private AlternateView getEmbeddedImage(string filePath, string mailBody)
        {
            string image = "http://localhost:51732/" + filePath;
            var webClient = new WebClient();
            byte[] imageBytes = webClient.DownloadData(image);
            MemoryStream ms = new MemoryStream(imageBytes);
            LinkedResource res = new LinkedResource(ms, MediaTypeNames.Image.Jpeg);
            res.ContentId = Guid.NewGuid().ToString();
            string htmlBody = @"<img style='width: 120px; height: 100px' src='cid:" + res.ContentId + @"'/>" + mailBody;
            AlternateView alternateView = AlternateView.CreateAlternateViewFromString(htmlBody, null, MediaTypeNames.Text.Html);
            alternateView.LinkedResources.Add(res);
            return alternateView;
        }

        public string replaceValue(string str, dynamic oldValue, dynamic newValue)
        {
            string oldVal = Convert.ToString(oldValue);
            string newVal = !String.IsNullOrWhiteSpace(Convert.ToString(newValue)) ? Convert.ToString(newValue) : "N/A";

            return str.Replace(oldVal, newVal.Trim());
        }

        public class IndieGetItemBatchesVByReport : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=1 ";
            private string columns = "";
            private string columnsSimple = "REQ_NO, REQ_NO, REQ_BY, REQ_DATE, PROBLEM, STATUS, REQ_TYPE, ASSET_NO,JOB_TYPE, JOB_URGENCY";
            private string columnsSql = "IBV.BATCH_ID, IBV.PRODUCT_CODE, IBV.STORE, IBV.EXPIRY_DATE_STR, IBV.CREATED_DATE, IBV.EXPIRY_DATE, IBV.PRODUCT_NAME, IBV.STOCK_ONHAND, IBV.INVOICENO, IBV.STORE_DESC, IBV.BARCODE_ID, IBV.SUPPLIER, IBV.RETURN_REASON, IBV.RETURN_TYPE, IBV.STATUS, IBV.ITEMTYPE, IBV.CLIENT_CODE, IBV.UO_DESC, IBV.CLIENT_NAME, IBV.TRANS_ID, IBV.SYSREPORTID, IBV.MODELCODE, STD.ACKNOWLEDGEBY, STD.ACKNOWLEDGEDATE, STD.RECALLBY, STD.RECALLDATE";
            private string from = "ITEM_BATCHES_V IBV INNER JOIN SYSTEM_TRANSACTIONDETAILS STD ON IBV.BATCH_ID = STD.REFTRANSID AND IBV.SYSREPORTID = STD.SYSREPORTID";

            public IndieGetItemBatchesVByReport(string dbType, string queryString, SearchFilter filter, int maxRows = 5000)
            {
                this.DatabaseType = dbType;
                this.maxRows = maxRows;

                if (!string.IsNullOrWhiteSpace(filter.clientCode))
                {
                    whereClause += " AND IBV.STORE IN (SELECT STORE_ID FROM STORES WHERE CLIENT_CODE = '" + filter.clientCode + "')";
                }

                if (!string.IsNullOrWhiteSpace(filter.SysReportId))
                {
                    whereClause += " AND IBV.SYSREPORTID = '" + filter.SysReportId + "'";
                }
            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + " ORDER BY  IBV.CREATED_DATE DESC " };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
                }
            }
        }

        public class IndieSearchStoreBatches : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "";
            private string columnsSimple = "REQ_NO, REQ_NO, REQ_BY, REQ_DATE, PROBLEM, STATUS, REQ_TYPE, ASSET_NO,JOB_TYPE, JOB_URGENCY";
            private string columnsSql = "AM.BATCH_ID, AM.PRODUCT_CODE, Convert(varchar(11),AM.EXPIRY_DATE,103) AS EXPIRY_DATE, Convert(varchar(11),AM.CREATED_DATE,103) AS CREATED_DATE, ST.DESCRIPTION PRODUCT_NAME, AM.STOCK_ONHAND, AM.INVOICENO, SS.DESCRIPTION STORE_DESC, AM.BARCODE_ID, AM.SUPPLIER, AM.RETURN_REASON, AM.RETURN_TYPE, AM.STATUS, LL.DESCRIPTION ITEMTYPE, SS.CLIENT_CODE, STR.ACTION";
            private string from = "PRODUCT_STOCKBATCH AM LEFT JOIN AM_PARTS ST ON AM.PRODUCT_CODE = ST.ID_PARTS LEFT OUTER JOIN STORES SS ON AM.STORE = SS.STORE_ID LEFT OUTER JOIN LOV_LOOKUPS LL ON ST.TYPE_ID = LL.LOV_LOOKUP_ID AND LL.CATEGORY = 'AIMS_MSTYPE' LEFT JOIN SystemTransaction_Report STR ON STR.Id = AM.SYSREPORTID";

            public IndieSearchStoreBatches(string dbType, string queryString,  SearchFilter filter, int maxRows = 500)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });


                var query2 = queryString.ToLower();
                whereClause += " OR (LOWER(AM.PRODUCT_CODE) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(ST.DESCRIPTION) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(AM.BARCODE_ID) LIKE ('" + query2 + "%'))";


                if (filter.StoreId != null && filter.StoreId != "")
                {
                    whereClause += " AND (AM.STORE ='" + filter.StoreId + "' )";
                }


                if (filter.ItemType != null && filter.ItemType != "")
                {
                    whereClause += " AND (ST.TYPE_ID ='" + filter.ItemType + "' )";
                }

                if (filter.InvoiceNo != null && filter.InvoiceNo != "")
                {
                    whereClause += " AND (AM.INVOICENO ='" + filter.InvoiceNo + "' )";
                }

                if (filter.SerialBatch != null && filter.SerialBatch != "")
                {
                    whereClause += " AND (AM.BARCODE_ID ='" + filter.SerialBatch + "' )";
                }
                if (filter.Status != null && filter.Status != "")
                {
                    whereClause += " AND (AM.STATUS ='" + filter.Status + "' )";
                }
                if (!string.IsNullOrWhiteSpace(filter.FromDate))
                {
                    var whereDate = string.Format("AM.TRANS_DATE >= '{0}'", filter.FromDate);
                    if (!string.IsNullOrWhiteSpace(filter.ToDate))
                    {
                        whereDate += string.Format(" and AM.TRANS_DATE <= '{0}'", filter.ToDate);
                    }
                    whereClause += " AND (" + whereDate + ")";
                }

                
                whereClause += " AND (AM.STATUS IN ('RECEIVED','RECALL','RETURNED', 'RETURN'))";



            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM.CREATED_DATE DESC " };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
                }
            }


        }


        public class IndieGetStockBatchByProductAndRequest : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=1 ";
            private string columns = "PSB.BATCH_ID, PSB.PRODUCT_CODE, PSB.SUPPLIER, SP.description AS SUPPLIERNAME, PSB.STORE, PSB.UOM, PSB.EXPIRY_DATE, PSB.QTY, PSB.PRICE, PSB.PRICE_DISCOUNT, PSB.STATUS, PSB.CREATED_BY, PSB.CREATED_DATE, PSB.TOTAL_PRICE, PSB.BARCODE_ID, PSB.TRANS_ID, PSB.TRANS_DATE, PSB.MIN, PSB.MAX, PSB.PURCHASE_NO, PSB.QTY_RECEIVED, PSB.BOX_QTY_RECEIVED, PSB.INVOICENO, PSB.MODELCODE, PSB.STOCK_ONHAND, PSB.ADJUST_REASON, PSB.ADDED_STOCK, PSB.RETURN_QTY, PSB.RETURN_TYPE, PSB.RETURN_REASON, PSB.SYSREPORTID";
            private string columnsSimple = "PRODUCT_ID, DESCRIPTION, SHORT_DESCRIPTION, CATEGORY_ID, CURRENT_PRICE, ORDER_FLAG, STATUS, ORDER_DETAILS_FLAG, SALE_UNIT";
            private string from = "PRODUCT_STOCKBATCH PSB LEFT JOIN AM_SUPPLIER SP ON PSB.SUPPLIER = SP.supplier_id";

            public IndieGetStockBatchByProductAndRequest(string dbType, string requestId, string productCode, string barCodeId, int maxRows = 10000)
            {
                this.DatabaseType = dbType;
                this.maxRows = maxRows;
                whereClause += " AND PSB.TRANS_ID='" + requestId + "' AND PSB.PRODUCT_CODE='" + productCode + "'";
                if (!String.IsNullOrEmpty(barCodeId))
                    whereClause += " AND PSB.BARCODE_ID='" + barCodeId + "'";
            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION " + " LIMIT " + maxRows.ToString() };
                }
            }


        }


        public class IndieSearchUserStoreStockOnHand : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "PX.ROWNUMID,PX.STORE, PX.COMMITTED_QTY,PX.PR_QTY,PX.UOM, PX.MIN, PX.MAX, PX.QTY, PX.PRODUCT_CODE, PX.DESCRIPTION, PX.CATEGORY,PX.STATUS, PX.STORE_DESC, PX.PRODUCT_CODE ID_PARTS, PX.LOCATION,PX.LOCATION2,PX.LOCATION3,PX.LOCATION4, PX.LOCATION5, PX.PART_NUMBERS, PX.UOM_DESC, PX.TYPE_DESC";
            private string columnsSimple = "PRODUCT_ID, DESCRIPTION, SHORT_DESCRIPTION, CATEGORY_ID, CURRENT_PRICE, ORDER_FLAG, STATUS, ORDER_DETAILS_FLAG, SALE_UNIT";
            private string from = "STORESTOCK_ONHAND_V PX";

            public IndieSearchUserStoreStockOnHand(string dbType, string queryString, string queryStatus, string queryCategoryId, string queryStore, string userId, int maxRows = 10000)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                
                var query2 = queryString.ToLower();
                whereClause += " OR (LOWER(PX.ID_PARTS) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(PX.DESCRIPTION) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(PX.PART_NUMBERS) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(PX.CATEGORY) LIKE ('" + query2 + "%'))";

                //   }

                if (queryCategoryId != "" && queryCategoryId != null && queryCategoryId != "ALL")
                {
                    whereClause += " AND (PX.CATEGORY = '" + queryCategoryId + "' )";

                }

                if (queryStatus != "" && queryStatus != null && queryStatus != "ALL")
                {
                    whereClause += " AND (PX.STATUS = '" + queryStatus + "' )";

                }
                if (queryStore != "" && queryStore != null && queryStore != "ALL")
                {
                    whereClause += " AND (PX.STORE = '" + queryStore + "' )";

                }

                if (userId != "" && userId != null)
                {
                    whereClause += " AND (PX.STORE IN (SELECT STORE_ID FROM USER_STORE WHERE USER_ID = '" + userId + "') )";

                }


            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  PX.DESCRIPTION ASC" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION " + " LIMIT " + maxRows.ToString() };
                }
            }


        }



        public class IndieSearchStockOnHand : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = " PX.ROWNUMID,PX.STORE, PX.COMMITTED_QTY,PX.PR_QTY,PX.UOM, PX.MIN, PX.MAX, PX.QTY, PX.PRODUCT_CODE, PX.DESCRIPTION, PX.CATEGORY,PX.STATUS, PX.DESCRIPTION STORE_DESC, PX.PRODUCT_CODE ID_PARTS, PX.PRICE, PX.LOCATION,PX.LOCATION2,PX.LOCATION3,PX.LOCATION4, PX.LOCATION5, PX.PART_NUMBERS, PX.UOM_DESC AS AM_UOM";
            private string columnsSimple = "PRODUCT_ID, DESCRIPTION, SHORT_DESCRIPTION, CATEGORY_ID, CURRENT_PRICE, ORDER_FLAG, STATUS, ORDER_DETAILS_FLAG, SALE_UNIT";
            private string from = " STORESTOCK_ONHAND_V PX";

            public IndieSearchStockOnHand(string dbType, string queryString, string queryStatus, string queryCategoryId, string queryStore, int maxRows = 10000)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                //foreach (string query in queryArray)
                //{
                var query2 = queryString.ToLower();
                whereClause += " OR (LOWER(PX.ID_PARTS) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(PX.DESCRIPTION) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(PX.PART_NUMBERS) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(PX.CATEGORY) LIKE ('" + query2 + "%'))";

                //   }

                if (queryCategoryId != "" && queryCategoryId != null && queryCategoryId != "ALL")
                {
                    whereClause += " AND (PX.CATEGORY = '" + queryCategoryId + "' )";

                }

                if (queryStatus != "" && queryStatus != null && queryStatus != "ALL")
                {
                    whereClause += " AND (PX.STATUS = '" + queryStatus + "' )";

            }
                if (queryStore != "" && queryStore != null && queryStore != "ALL")
                {
                    whereClause += " AND (PX.STORE = '" + queryStore + "' )";

                }




            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  PX.DESCRIPTION ASC" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION " + " LIMIT " + maxRows.ToString() };
                }
            }


        }


        public class IndieSearchPartsOnHandByStoreActive : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "PR.PRODUCT_CODE, PR.QTY, PR.COMMITTED_QTY, PR.AVAILABLE_STOCK, PR.STORE,PR.PRODUCT_DESC,PR.STORE_DESC,PR.PART_COST, PS.PART_NUMBERS, PS.PART_MODELS";
            private string columnsSimple = "PRODUCT_CODE, QTY, COMMITTED_QTY, AVAILABLE_STOCK, STORE, PRODUCT_DESC";
            private string from = "PARTS_STOCKONHAND_V PR LEFT OUTER JOIN PARTNO_BYPARTID_V PS ON PR.PRODUCT_CODE = PS.ID_PARTS";

            public IndieSearchPartsOnHandByStoreActive(string dbType, string queryString, string storeId, int maxRows = 5000)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                foreach (string query in queryArray)
                {
                    var query2 = query.ToLower();
                    whereClause += " OR (LOWER(PR.PRODUCT_CODE) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(PS.PART_NUMBERS) LIKE ('%" + query2 + "%')";
                    whereClause += " OR LOWER(PR.PRODUCT_DESC) LIKE ('%" + query2 + "%'))";

                    if (storeId != "" && storeId != null)
                    {
                        whereClause += " AND (PR.STORE = '" + storeId + "' )";

                    }


                }



            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
        {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  PRODUCT_DESC ASC" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  PRODUCT_DESC ASC" + " LIMIT " + maxRows.ToString() };
                }
            }


        }


        public class IndieSearchStockOnHandByUserStore : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "(ROW_NUMBER() OVER (ORDER BY PR.DESCRIPTION ASC)) AS ROWNUMID,PX.STORE, PX.COMMITTED_QTY,PX.PR_QTY,PX.UOM, PX.MIN, PX.MAX, PX.QTY, PX.PRODUCT_CODE, PR.DESCRIPTION, PR.CATEGORY,PX.STATUS, ST.DESCRIPTION STORE_DESC, PX.PRODUCT_CODE ID_PARTS, PX.LOCATION,PX.LOCATION2,PX.LOCATION3,PX.LOCATION4, PX.LOCATION5, PS.PART_NUMBERS";
            private string columnsSimple = "PRODUCT_ID, DESCRIPTION, SHORT_DESCRIPTION, CATEGORY_ID, CURRENT_PRICE, ORDER_FLAG, STATUS, ORDER_DETAILS_FLAG, SALE_UNIT";
            private string from = "PRODUCT_STOCKONHAND PX INNER JOIN AM_PARTS PR ON PX.PRODUCT_CODE = PR.ID_PARTS  INNER JOIN STORES ST ON PX.STORE = ST.STORE_ID LEFT OUTER JOIN PARTNO_BYPARTID_V PS ON PX.PRODUCT_CODE = PS.ID_PARTS";

            public IndieSearchStockOnHandByUserStore(string dbType, string queryString, string queryStatus, string queryCategoryId, string queryStore, string userId,int maxRows = 10000)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                //foreach (string query in queryArray)
                //{
                var query2 = queryString.ToLower();
                whereClause += " OR (LOWER(PR.ID_PARTS) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(PR.DESCRIPTION) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(PS.PART_NUMBERS) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(PR.CATEGORY) LIKE ('" + query2 + "%'))";

                //   }

                if (queryCategoryId != "" && queryCategoryId != null && queryCategoryId != "ALL")
                {
                    whereClause += " AND (PR.CATEGORY = '" + queryCategoryId + "' )";

                }

                if (queryStatus != "" && queryStatus != null && queryStatus != "ALL")
                {
                    whereClause += " AND (PR.STATUS = '" + queryStatus + "' )";

                }
                if (queryStore != "" && queryStore != null && queryStore != "ALL")
                {
                    whereClause += " AND (PX.STORE = '" + queryStore + "' )";

                }

                if (userId != "" && userId != null)
                {
                    whereClause += " AND (PX.STORE IN (SELECT STORE_ID FROM USER_STORE WHERE USER_ID = '"+ userId + "') )";

            }


            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  PR.DESCRIPTION ASC" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION " + " LIMIT " + maxRows.ToString() };
                }
            }


        }
        public class IndieSearchStoreByUserId : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=1 ";
            private string columns = "PR.STORE_ID, PR.DESCRIPTION, PR.LOCATION, PR.CONTACT_NO, PR.STATUS,PR.COSTCENTERFLAG, PR.ALLOWREQUEST_FLAG";
            private string columnsSimple = "STORE_ID, DESCRIPTION, LOCATION, CONTACT_NO, STATUS, COSTCENTERFLAG, ALLOWREQUEST_FLAG";
            private string from = "STORES PR";

            public IndieSearchStoreByUserId(string dbType, string queryString, string userId, int maxRows = 5000)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                //foreach (string query in queryArray)
                //{
                var query2 = queryString.ToLower();

                if (userId != null && userId != "")
                {
                    whereClause += " AND (PR.STORE_ID IN (SELECT STORE_ID FROM USER_STORE WHERE USER_ID = '"+ userId+ "'))";
                }

            }




        public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
                }
            }


        }


        public class IndieSearchStoreBuildingLOV : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=1 ";
            private string columns = "DESCRIPTION,LOV_LOOKUP_ID";
            private string columnsSimple = "DESCRIPTION,LOV_LOOKUP_ID";
            private string from = "LOV_LOOKUPS";

            public IndieSearchStoreBuildingLOV(string dbType, string queryString, int maxRows = 50)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                foreach (string query in queryArray)
                {
                    var query2 = query.ToLower();
                    whereClause += " AND CATEGORY = 'AIMS_BUILDING' AND STATUS = 'Y' AND  LOV_LOOKUP_ID NOT IN (SELECT BUILDING_CODE FROM STORE_BUILDING)";

                }
            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
                }
        }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
                }
            }
        }

        public class IndieSearchstoreBuilding : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "P.STORE_ID,LOV.DESCRIPTION,P.BUILDING_CODE";
            private string columnsSimple = "P.STORE_ID,LOV.DESCRIPTION,P.BUILDING_CODE";
            private string from = "STORE_BUILDING P LEFT OUTER JOIN LOV_LOOKUPS LOV ON P.BUILDING_CODE = LOV.LOV_LOOKUP_ID AND CATEGORY = 'AIMS_BUILDING'";

            public IndieSearchstoreBuilding(string dbType, string queryString, int maxRows = 50)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                foreach (string query in queryArray)
                {
                    var query2 = query.ToLower();
                    whereClause += " OR LOWER(P.STORE_ID) = ('" + query2 + "')";

                }
            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
                }
            }
        }


        public class IndieReadWOLMaterialsCollect : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "AM.AM_WORKORDER_LABOUR_ID, AM.AM_PARTREQUESTID, AM.DESCRIPTION, AM.QTY, AM.PRICE,AM.EXTENDED,AM.BILLABLE, AM.REF_WO, AM.STATUS,AM.WO_LABOURTYPE,AM.ID_PARTS,AM.REF_ASSETNO,AM.STORE_ID,AM.REQ_DATE, ST.DESCRIPTION PRODUCT_DESC, PX.DESCRIPTION STORE_DESC, AM.TRANSFER_QTY, AM.FOR_COLLECTION_QTY, AM.TRANSFER_FLAG, PS.QTY STOCK_ONHAND";
            private string columnsSimple = "AM_WORKORDER_LABOUR_ID, AM_PARTREQUESTID, DESCRIPTION, QTY, PRICE, EXTENDED, BILLABLE, REF_WO,STATUS";
            private string from = "AM_PARTSREQUEST AM LEFT JOIN AM_PARTS ST ON AM.ID_PARTS = ST.ID_PARTS LEFT JOIN STORES PX ON PX.STORE_ID = AM.STORE_ID LEFT JOIN PRODUCT_STOCKONHAND PS ON AM.STORE_ID = PS.STORE AND PS.PRODUCT_CODE = AM.ID_PARTS";

            public IndieReadWOLMaterialsCollect(string dbType, string queryString, int maxRows = 6)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                //foreach (string query in queryArray)
                //{
                var query2 = queryString.ToLower();
                whereClause += " OR (LOWER(AM.ID_PARTS) = ('" + query2 + "'))";
                whereClause += " AND FOR_COLLECTION_QTY > 0 ";


            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PARTREQUESTID ASC" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PARTREQUESTID ASC" + " LIMIT 5000" };
                }
            }


        }

       
        public class IndieSearchStockBatchReceiveItems : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = " PR.TRANS_ID,PR.TRANS_DATE, PR.STORE, PR.BATCH_ID, PR.PRODUCT_CODE, PR.PRODUCT_CODE ID_PARTS, PR.SUPPLIER, PR.UOM, PR.MIN,PR.MAX,PR.STATUS, PR.PRICE, PR.PRICE_DISCOUNT, PR.QTY, PR.TOTAL_PRICE, PR.BARCODE_ID, PR.EXPIRY_DATE, PR.PURCHASE_NO, PD.DESCRIPTION PRODUCT_DESC, ST.DESCRIPTION STOREDESC, PR.QTY_PO_REQUEST,PR.QTY_RECEIVED,PR.QTY_RETURN, PR.RETURN_DATE,PR.RETURN_REASON, UO.DESCRIPTION UOM_DESC, PR.MODELCODE, PR.INVOICENO";
            private string columnsSimple = "STORE_ID, DESCRIPTION, LOCATION, CONTACT_NO, STATUS, COSTCENTERFLAG";
            private string from = "PRODUCT_BATCH_V PR INNER JOIN AM_PARTS PD ON PR.PRODUCT_CODE = PD.ID_PARTS INNER JOIN STORES ST ON PR.STORE = ST.STORE_ID LEFT OUTER JOIN [dbo].[lov_lookups] AS UO ON PD.uom_id = UO.LOV_LOOKUP_ID AND UO.CATEGORY ='AIMS_MSUOM'"; 

            public IndieSearchStockBatchReceiveItems(string dbType, string queryString, string productCode, int maxRows = 500)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                //foreach (string query in queryArray)
                //{
                var query2 = queryString.ToLower();
               // whereClause += " OR LOWER(PR.TRANS_ID) LIKE ('" + query2 + "%')";

                if (queryString != "" && queryString != null && queryString != "ALL")
                {
                    whereClause += " OR PR.TRANS_ID = ('" + queryString + "')";
                    //whereClause += " AND (PR.TRANS_ID = '" + queryString + "' )";

                }

                if (productCode != "" && productCode != null && productCode != "ALL")
                {
                    whereClause += " OR PR.PRODUCT_CODE = ('" + productCode + "')";

                }


            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY TRANS_DATE ASC" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY TRANS_DATE ASC" + " LIMIT " + maxRows.ToString() };
                }
            }


        }

        public class IndieSearchStockBatchReceive : IndieSql
        {           
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = " 1=1 ";
            private string columns = "DISTINCT PR.TRANS_ID, PR.TRANS_DATE TRANS_DATE, PR.STORE, COUNT(PR.BATCH_ID) NOITEMS, PR.STATUS, PX.DESCRIPTION STOREDESC, PO.DESCRIPTION AS SUPPLIER_DESC, PR.REF_WO";
            private string columnsSimple = "STORE_ID, DESCRIPTION, LOCATION, CONTACT_NO, STATUS, STORE_TYPE";
            private string columnsSql = "CAST(PR.TRANS_ID AS INT) AS TRANS_ID , Convert(varchar(11),PR.TRANS_DATE,103) AS TRANS_DATE, COUNT(PR.TRANS_ID) as NOITEMS, PR.STATUS , PO.DESCRIPTION AS SUPPLIER_DESC, PQ.REF_WO,PR.PURCHASE_NO";
            private string groupByClause = String.Empty;

            private string from = "PRODUCT_STOCKBATCH PR INNER JOIN STORES PX ON PX.STORE_ID = PR.STORE LEFT OUTER JOIN PURCHASEORDER_V PO ON PR.PURCHASE_NO = PO.PURCHASE_NO LEFT OUTER JOIN AM_PURCHASEREQUEST PQ ON PR.PURCHASE_NO = PQ.REQUEST_ID";

            public IndieSearchStockBatchReceive(string dbType, string queryString, SearchFilter filter, int maxRows = 500)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                groupByClause = "CAST(PR.TRANS_ID AS INT),Convert(varchar(11),PR.TRANS_DATE,103),pr.STATUS,PO.DESCRIPTION,PQ.REF_WO, PR.PURCHASE_NO ";
                //foreach (string query in queryArray)
                //{
                var query2 = queryString.ToLower();
                if (!string.IsNullOrWhiteSpace(query2))
                {
                    whereClause += " AND (LOWER(PR.STORE) LIKE ('" + query2 + "%') OR LOWER(PR.TRANS_ID) LIKE ('" + query2 + "%') OR LOWER(PQ.REF_WO) LIKE ('" + query2 + "%') OR LOWER(PR.PURCHASE_NO) LIKE ('" + query2 + "%'))";
                }


                if (!string.IsNullOrWhiteSpace(filter.FromDate))
                {
                    var whereDate = string.Format("PR.TRANS_DATE >= '{0}'", filter.FromDate);
                    if (!string.IsNullOrWhiteSpace(filter.ToDate))
                    {
                        whereDate += string.Format(" and PR.TRANS_DATE <= '{0}'", filter.ToDate);
                    }
                    whereClause += " AND (" + whereDate + ")";
                }

                if (!string.IsNullOrWhiteSpace(filter.Status))
                {
                    whereClause += string.Format(" AND PR.STATUS Like ('{0}%')", filter.Status);

                }


            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT distinct TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + " GROUP BY " + groupByClause + " ORDER BY CAST(PR.TRANS_ID AS INT) DESC" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " GROUP BY TRANS_ID, DATE_FORMAT(PR.TRANS_DATE,'%m-%d-%Y') ORDER BY TRANS_DATE DESC" + " LIMIT " + maxRows.ToString() };
                }
            }


        }

        public class IndieSearchStockItems : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "CD.ID_PARTS, CD.DESCRIPTION, CD.CATEGORY, ST.PRODUCT_CODE, ST.QTY, ST.MIN, ST.MAX";
            private string columnsSimple = "";
            private string from = "AM_PARTS CD LEFT JOIN PRODUCT_STOCKONHAND ST ON CD.ID_PARTS = ST.PRODUCT_CODE AND ST.STORE =@0";

            public IndieSearchStockItems(string dbType, string queryString, string actionStr, int maxRows = 1000)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                foreach (string query in queryArray)
                {
                    var query2 = query.ToLower();
                    whereClause += " OR (LOWER(CD.ID_PARTS) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(CD.DESCRIPTION) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(CD.CATEGORY) LIKE ('" + query2 + "%')";

                }

                whereClause += ") AND (CD.STATUS = 'Active')";
                if (actionStr != null && actionStr != "")
                {
                    whereClause += " AND (ST.PRODUCT_CODE IS NOT NULL )";
                }
            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY CLINIC_DESCRIPTION " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  CD.DESCRIPTION " };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  CD.DESCRIPTION " + " LIMIT " + maxRows.ToString() };
                }
            }


        }

        public class IndieSearchAllStock : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "CD.ID_PARTS, CD.DESCRIPTION, CD.CATEGORY, SUM(ST.QTY) QTY";
            private string columnsSimple = "";
            private string from = "AM_PARTS CD LEFT JOIN PRODUCT_STOCKONHAND ST ON CD.ID_PARTS = ST.PRODUCT_CODE";

            public IndieSearchAllStock(string dbType, string queryString, string actionStr, int maxRows = 1000)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                foreach (string query in queryArray)
                {
                    var query2 = query.ToLower();
                    whereClause += " OR (LOWER(CD.ID_PARTS) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(CD.DESCRIPTION) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(CD.CATEGORY) LIKE ('" + query2 + "%')";

                }

                whereClause += ") AND (CD.STATUS = 'Active')";
                if (actionStr != null && actionStr != "")
                {
                    whereClause += " AND (ST.PRODUCT_CODE IS NOT NULL )";
                }
            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY CLINIC_DESCRIPTION " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " GROUP BY CD.ID_PARTS, CD.DESCRIPTION, CD.CATEGORY ORDER BY  CD.DESCRIPTION ASC" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " GROUP BY CD.ID_PARTS, CD.DESCRIPTION ORDER BY  CD.DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
                }
            }


        }
        public class IndieSearchPartsOnHand : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "PR.PRODUCT_CODE, PR.QTY, PR.COMMITTED_QTY, PR.AVAILABLE_STOCK, PR.STORE,PR.PRODUCT_DESC,PR.STORE_DESC,PR.PART_COST";
            private string columnsSimple = "PRODUCT_CODE, QTY, COMMITTED_QTY, AVAILABLE_STOCK, STORE, PRODUCT_DESC";
            private string from = "PARTS_STOCKONHAND_V PR";

            public IndieSearchPartsOnHand(string dbType, string queryString, int maxRows = 5000)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                //foreach (string query in queryArray)
                //{
                var query2 = queryString.ToLower();
                whereClause += " OR LOWER(PR.PRODUCT_CODE) LIKE ('" + query2 + "%')";



            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  PRODUCT_DESC ASC" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  PRODUCT_DESC ASC" + " LIMIT " + maxRows.ToString() };
                }
            }


        }

        public class IndieSearchPartsOnHandActive : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "PR.PRODUCT_CODE, PR.QTY, PR.COMMITTED_QTY, PR.AVAILABLE_STOCK, PR.STORE,PR.PRODUCT_DESC,PR.STORE_DESC,PR.PART_COST, PS.PART_NUMBERS, PS.PART_MODELS";
            private string columnsSimple = "PRODUCT_CODE, QTY, COMMITTED_QTY, AVAILABLE_STOCK, STORE, PRODUCT_DESC";
            private string from = "PARTS_STOCKONHAND_V PR LEFT OUTER JOIN PARTNO_BYPARTID_V PS ON PR.PRODUCT_CODE = PS.ID_PARTS";

            public IndieSearchPartsOnHandActive(string dbType, string queryString, int maxRows = 5000)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                foreach (string query in queryArray)
                {
                    var query2 = query.ToLower();
                    whereClause += " OR (LOWER(PR.PRODUCT_CODE) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(PS.PART_NUMBERS) LIKE ('%" + query2 + "%')";
                    whereClause += " OR LOWER(PR.PRODUCT_DESC) LIKE ('%" + query2 + "%'))";

                }



            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  PRODUCT_DESC ASC" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  PRODUCT_DESC ASC" + " LIMIT " + maxRows.ToString() };
                }
            }


        }
        public class IndieSearchStore : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "PR.STORE_ID, PR.DESCRIPTION, PR.LOCATION, PR.CONTACT_NO, PR.STATUS,PR.CLIENT_CODE, PR.ALLOWREQUEST_FLAG";
            private string columnsSimple = "STORE_ID, DESCRIPTION, LOCATION, CONTACT_NO, STATUS, CLIENT_CODE, ALLOWREQUEST_FLAG";
            private string from = "STORES PR";

            public IndieSearchStore(string dbType, string queryString, int maxRows = 5000)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                //foreach (string query in queryArray)
                //{
                var query2 = queryString.ToLower();
                whereClause += " OR LOWER(PR.STORE_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(PR.DESCRIPTION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(PR.LOCATION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(PR.CLIENT_CODE) LIKE ('" + query2 + "%')";

              

            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
                }
            }


        }
        public class IndieSearchProdServiceCat : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "PR.PRODUCT_ID, PR.DESCRIPTION, PR.SHORT_DESCRIPTION, PR.CATEGORY_ID, PR.CURRENT_PRICE,PR.SALE_UNIT";
            private string columnsSimple = "PRODUCT_ID, DESCRIPTION, SHORT_DESCRIPTION, CATEGORY_ID, CURRENT_PRICE, SALE_UNIT";
            private string from = "PRODUCT_DETAILS PR";

            public IndieSearchProdServiceCat(string dbType, string queryString, string serviceStr, string queryCategoryId, int maxRows = 5000)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });
               
                var query2 = queryString.ToLower();
                whereClause += " OR LOWER(PR.PRODUCT_ID) LIKE ('" + query2 + "%')";
               

                if (queryCategoryId != "" && queryCategoryId != null && queryCategoryId != "ALL")
                {
                    whereClause += " AND (PR.CATEGORY_ID = '" + queryCategoryId + "' )";

                }

                if (serviceStr != "" && serviceStr != null && serviceStr != "ALL")
                {
                    whereClause += " AND (PR.SERVICE_ID = '" + serviceStr + "' )";

                }

                whereClause += " AND (PR.STATUS = 'Y' )";
            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  PRODUCT_ID,DESCRIPTION " };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  PRODUCT_ID,DESCRIPTION " + " LIMIT " + maxRows.ToString() };
                }
            }


        }
        public class IndieSearchProducts : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "PR.PRODUCT_ID, PR.DESCRIPTION, PR.SHORT_DESCRIPTION, PR.CATEGORY_ID, PR.CURRENT_PRICE,PR.ORDER_FLAG, PR.STATUS, PR.ORDER_DETAILS_FLAG,PR.SALE_UNIT";
            private string columnsSimple = "PRODUCT_ID, DESCRIPTION, SHORT_DESCRIPTION, CATEGORY_ID, CURRENT_PRICE, ORDER_FLAG, STATUS, ORDER_DETAILS_FLAG, SALE_UNIT";
            private string from = "PRODUCT_DETAILS PR";

            public IndieSearchProducts(string dbType, string queryString, string queryStatus, string queryCategoryId, int maxRows = 5000)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                //foreach (string query in queryArray)
                //{
                    var query2 = queryString.ToLower();
                    whereClause += " OR (LOWER(PR.PRODUCT_ID) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(PR.DESCRIPTION) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(PR.SHORT_DESCRIPTION) LIKE ('" + query2 + "%')";
                    whereClause += " OR LOWER(PR.CATEGORY_ID) LIKE ('" + query2 + "%'))";

             //   }

                if (queryCategoryId != "" && queryCategoryId != null && queryCategoryId != "ALL")
                {
                    whereClause += " AND (PR.CATEGORY_ID = '" + queryCategoryId + "' )";

                }

                if (queryStatus != "" && queryStatus != null && queryStatus != "ALL")
                {
                    whereClause += " AND (PR.STATUS = '" + queryStatus + "' )";

                }
            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  CATEGORY_ID,DESCRIPTION " };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  CATEGORY_ID,DESCRIPTION " + " LIMIT " + maxRows.ToString() };
                }
            }


        }

        public class IndieSearchProductsNotStockOnHand : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "PR.ID_PARTS, PR.DESCRIPTION, PR.PART_COST ";
            private string columnsSimple = "ID_PARTS, DESCRIPTION, PART_COST";
            private string from = "AM_PARTS PR";

            public IndieSearchProductsNotStockOnHand(string dbType, string queryString, string storeId, int maxRows = 15000)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                //foreach (string query in queryArray)
                //{
                var query2 = queryString.ToLower();
                whereClause += " OR (LOWER(PR.ID_PARTS) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(PR.DESCRIPTION) LIKE ('" + query2 + "%'))";

                //   }

                if (storeId != "" && storeId != null && storeId != "ALL")
                {
                    whereClause += " AND PR.ID_PARTS NOT IN (SELECT PRODUCT_CODE FROM PRODUCT_STOCKONHAND WHERE STORE ='" + storeId+"' )";
                    whereClause += " AND (PR.STATUS = 'Active' )";

                }


            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC " };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  CATEGORY_ID,DESCRIPTION " + " LIMIT " + maxRows.ToString() };
                }
            }


        }

        public class IndieSearchStockTransaction : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=1";
            private string columns = "AM.AM_STOCKTRANSACTIONID, AM.TRANS_ID,  AM.TRANS_STATUS, AM.REQUEST_DATE, COUNT(AM.ID_PARTS) ITEMREQ, PX.DESCRIPTION FROM_STOREDESC, PY.DESCRIPTION TO_STOREDESC";
            private string columnsSimple = "AM_PRREQITEMID, DESCRIPTION, QTY, CITY, PRICE, EXTENDED, CONTRACT_NO, REF_WO";
			private string columnsSql = " AM.TRANS_ID,  AM.TRANS_STATUS, Convert(varchar(11),AM.REQUEST_DATE,103) as REQUEST_DATE, COUNT(AM.ID_PARTS) ITEMREQ, PX.DESCRIPTION FROM_STOREDESC, PY.DESCRIPTION TO_STOREDESC";

			private string from = "AM_STOCKTRANSACTION AM INNER JOIN STORES PX ON PX.STORE_ID = AM.STORE_FROM INNER JOIN STORES PY ON PY.STORE_ID = AM.STORE_TO";

			private string groupByClause = " AM.TRANS_ID ,AM.TRANS_STATUS, Convert(varchar(11),AM.REQUEST_DATE,103) ,PX.DESCRIPTION ,PY.DESCRIPTION  ";

            public IndieSearchStockTransaction(string dbType, string queryString, string retAction, string transStatus, string queryPage, SearchFilter filter, int maxRows = 500)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                //foreach (string query in queryArray)
                //{
                var query2 = queryString.ToLower();
                if (!string.IsNullOrWhiteSpace(query2))
                {
                    whereClause += "AND (LOWER(PX.DESCRIPTION) LIKE ('" + query2 + "%') OR LOWER(PY.DESCRIPTION) LIKE ('" + query2 + "%') OR LOWER(AM.TRANS_ID) LIKE ('" + query2 + "%'))";
                }
                
                if (!string.IsNullOrWhiteSpace(filter.FromDate))
                {
                    var whereDate = string.Format("AM.REQUEST_DATE >= '{0}'",filter.FromDate);
                    if (!string.IsNullOrWhiteSpace(filter.ToDate))
                    {
                        whereDate += string.Format(" and AM.REQUEST_DATE <= '{0}'", filter.ToDate);
                    }
                    whereClause +=" AND (" + whereDate+")";
                }

                if (filter.FromStore > 0)
                {
                    whereClause += string.Format(" AND AM.STORE_FROM = {0}", filter.FromStore);
                    
                }
                if (filter.ToStore > 0)
                {
                    whereClause += string.Format(" AND AM.STORE_TO = {0}", filter.ToStore);
                }
                if (!string.IsNullOrWhiteSpace(filter.Status))
                {
                    whereClause += string.Format(" AND AM.TRANS_STATUS Like ('{0}%')", filter.Status);
                  
                }

                //if (transStatus != null && transStatus != "")
                //{
                //    whereClause += " AND (AM.TRANS_STATUS = '" + transStatus + "' )";
                //}


                if (queryPage == "TRANSFER")
                {
                    whereClause += "AND (AM.TRANS_STATUS = 'POSTED' or AM.TRANS_STATUS = 'TRANSFER' or AM.TRANS_STATUS = 'PARTIAL TRANSFER' or AM.TRANS_STATUS = 'PARTIAL RECEIVED')";
                }
                else if (queryPage == "RECEIVE")
                {
                    whereClause += "AND (AM.TRANS_STATUS = 'TRANSFER' or AM.TRANS_STATUS = 'PARTIAL TRANSFER' or AM.TRANS_STATUS = 'RECEIVED' or AM.TRANS_STATUS = 'PARTIAL RECEIVED')";
                }
            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause+ "GROUP BY " + groupByClause + " ORDER BY  AM.TRANS_ID DESC, Convert(varchar(11),AM.REQUEST_DATE,103) DESC" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + "GROUP BY AM.TRANS_ID ORDER BY  REQUEST_DATE DESC" + " LIMIT " + maxRows.ToString() };
                }
            }


        }

        public class IndieReadStockReq : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "AM.AM_STOCKTRANSACTIONID, AM.TRANS_ID, AM.TRANS_TYPE, AM.ID_PARTS, AM.STORE_FROM,AM.STORE_TO,AM.TRANS_STATUS, AM.QTY, AM.REQUEST_BY," +
                "AM.REQUEST_DATE,AM.DELIVER_BY,AM.DELIVER_DATE,AM.RECEIVE_BY,AM.TRANSDETFLAG,AM.RECEIVE_DATE,AM.QTY_TRANSFER, AM.REMARKS, AM.INVOICENO, AM.TRANSFER_REMARKS, " +
                "AM.RECEIVE_REMARKS, AM.PRICE, AM.VAT_FLAG, AM.VAT, ST.DESCRIPTION, LL.DESCRIPTION AS AM_UOM, SOH.QTY STOCK_QTY, SOH.MAX STOCK_MAX, SOH.MIN STOCK_MIN";
            private string columnsSimple = "AM_PRREQITEMID, QTY, REMARKS, REF_WO, STATUS, REF_ASSETNO, REQUEST_ID, STATUS_BY,STATUS_DATE";
            private string from = "AM_STOCKTRANSACTION AM LEFT JOIN AM_PARTS ST ON AM.ID_PARTS = ST.ID_PARTS LEFT JOIN LOV_LOOKUPS LL ON LL.LOV_LOOKUP_ID = ST.UOM_ID LEFT JOIN PRODUCT_STOCKONHAND SOH ON AM.ID_PARTS = SOH.PRODUCT_CODE AND AM.STORE_TO = SOH.STORE";

            public IndieReadStockReq(string dbType, string queryString, int maxRows = 6)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                //foreach (string query in queryArray)
                //{
                var query2 = queryString.ToLower();
                whereClause += " OR LOWER(AM.TRANS_ID) = ('" + query2 + "')";



            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_STOCKTRANSACTIONID ASC" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_STOCKTRANSACTIONID ASC" + " LIMIT 5000" };
                }
            }


        }

        public class IndieReadWOLMaterials : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "AM.AM_WORKORDER_LABOUR_ID, AM.AM_PARTREQUESTID, AM.DESCRIPTION, AM.QTY, AM.PRICE,AM.EXTENDED,AM.BILLABLE, AM.REF_WO, AM.STATUS,AM.WO_LABOURTYPE,AM.ID_PARTS,AM.REF_ASSETNO,AM.STORE_ID,AM.REQ_DATE, ST.DESCRIPTION PRODUCT_DESC, PX.DESCRIPTION STORE_DESC, AM.TRANSFER_QTY,AM.QTY_RETURN, AM.QTY_FORRETURN, AM.TRANSFER_FLAG";
            private string columnsSimple = "AM_WORKORDER_LABOUR_ID, AM_PARTREQUESTID, DESCRIPTION, QTY, PRICE, EXTENDED, BILLABLE, REF_WO,STATUS";
            private string from = "AM_PARTSREQUEST AM LEFT JOIN AM_PARTS ST ON AM.ID_PARTS = ST.ID_PARTS LEFT JOIN STORES PX ON PX.STORE_ID = AM.STORE_ID";

            public IndieReadWOLMaterials(string dbType, string queryString, int maxRows = 6)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                //foreach (string query in queryArray)
                //{
                var query2 = queryString.ToLower();
                whereClause += " OR LOWER(AM.AM_WORKORDER_LABOUR_ID) = ('" + query2 + "')";



            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PARTREQUESTID ASC" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PARTREQUESTID ASC" + " LIMIT 5000" };
                }
            }


        }
        public class IndieReadWOIdPRMaterials : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "AM.AM_WORKORDER_LABOUR_ID, AM.AM_PARTREQUESTID, AM.DESCRIPTION, AM.QTY, AM.PRICE,AM.EXTENDED,AM.BILLABLE, AM.REF_WO, AM.STATUS,AM.WO_LABOURTYPE,AM.ID_PARTS,AM.REF_ASSETNO,AM.STORE_ID,AM.REQ_DATE, ST.DESCRIPTION PRODUCT_DESC, PX.DESCRIPTION STORE_DESC, AM.TRANSFER_QTY,AM.QTY_RETURN, AM.QTY_FORRETURN, AM.TRANSFER_FLAG,AM.PR_REQUEST_ID,AM.PR_REQUEST_QTY, PS.PART_NUMBERS";
            private string columnsSimple = "AM_WORKORDER_LABOUR_ID, AM_PARTREQUESTID, DESCRIPTION, QTY, PRICE, EXTENDED, BILLABLE, REF_WO,STATUS";
            private string from = "AM_PARTSREQUEST AM LEFT JOIN AM_PARTS ST ON AM.ID_PARTS = ST.ID_PARTS LEFT JOIN STORES PX ON PX.STORE_ID = AM.STORE_ID LEFT OUTER JOIN PARTNO_BYPARTID_V PS ON AM.ID_PARTS = PS.ID_PARTS";

            public IndieReadWOIdPRMaterials(string dbType, string queryString, int maxRows = 6)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                //foreach (string query in queryArray)
                //{
                var query2 = queryString.ToLower();
                whereClause += " OR (LOWER(AM.REF_WO) = ('" + query2 + "'))";
                whereClause += " AND LOWER(AM.PR_REQUEST_ID) = (0)";



            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PARTREQUESTID ASC" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PARTREQUESTID ASC" + " LIMIT 5000" };
                }
            }


        }

        public class IndieReadWOIdPRMaterialsUse : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "AM.AM_WORKORDER_LABOUR_ID, AM.AM_PARTREQUESTID, AM.DESCRIPTION, AM.QTY, AM.PRICE,AM.EXTENDED,AM.BILLABLE, AM.REF_WO, AM.STATUS,AM.WO_LABOURTYPE,AM.ID_PARTS,AM.REF_ASSETNO,AM.STORE_ID,AM.REQ_DATE, ST.DESCRIPTION PRODUCT_DESC, PX.DESCRIPTION STORE_DESC, AM.TRANSFER_QTY,AM.QTY_RETURN, AM.QTY_FORRETURN, AM.TRANSFER_FLAG,AM.PR_REQUEST_ID,AM.PR_REQUEST_QTY, AM.USED_QTY,PS.PART_NUMBERS, AM.DISPATCH_DATE,PS.PART_MODELS";
            private string columnsSimple = "AM_WORKORDER_LABOUR_ID, AM_PARTREQUESTID, DESCRIPTION, QTY, PRICE, EXTENDED, BILLABLE, REF_WO,STATUS";
            private string from = "AM_PARTSREQUEST AM LEFT JOIN AM_PARTS ST ON AM.ID_PARTS = ST.ID_PARTS LEFT JOIN STORES PX ON PX.STORE_ID = AM.STORE_ID LEFT OUTER JOIN PARTNO_BYPARTID_V PS ON AM.ID_PARTS = PS.ID_PARTS";

            public IndieReadWOIdPRMaterialsUse(string dbType, string queryString, int maxRows = 6)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                //foreach (string query in queryArray)
                //{
                var query2 = queryString.ToLower();
                whereClause += " OR (LOWER(AM.REF_WO) = ('" + query2 + "'))";



            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PARTREQUESTID ASC" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PARTREQUESTID ASC" + " LIMIT 5000" };
                }
            }


        }
        public class IndieReadWOIdMaterials : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "AM.AM_WORKORDER_LABOUR_ID, AM.AM_PARTREQUESTID, AM.DESCRIPTION, AM.QTY, AM.PRICE,AM.EXTENDED,AM.BILLABLE, AM.REF_WO, AM.STATUS,AM.WO_LABOURTYPE,AM.ID_PARTS,AM.REF_ASSETNO,AM.STORE_ID,AM.REQ_DATE, AM.PRODUCT_DESC, AM.STORE_DESC, AM.TRANSFER_QTY,AM.QTY_RETURN, AM.QTY_FORRETURN, AM.TRANSFER_FLAG,AM.PR_REQUEST_ID,AM.PR_REQUEST_QTY, AM.USED_QTY, AM.PART_NUMBERS, AM.FOR_COLLECTION_QTY, AM.FOR_COLLECTION_CANCELBY, AM.FOR_COLLECTION_CANCELDATE, AM.LAST_MODIFIEDBY, AM.LAST_MODIFIEDDATE,AM.PART_MODELS";
            private string columnsSimple = "AM_WORKORDER_LABOUR_ID, AM_PARTREQUESTID, DESCRIPTION, QTY, PRICE, EXTENDED, BILLABLE, REF_WO,STATUS";
            private string from = "WORKORDER_PARTSREQUEST_V AM";

            public IndieReadWOIdMaterials(string dbType, string queryString, int maxRows = 6)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                //foreach (string query in queryArray)
                //{
                var query2 = queryString.ToLower();
                whereClause += " OR LOWER(AM.REF_WO) = ('" + query2 + "')";
                //whereClause += " OR LOWER(AM.PR_REQUEST_ID) = (0)";



            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PARTREQUESTID ASC" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PARTREQUESTID ASC" + " LIMIT 5000" };
                }
            }


        }
        public class IndieReadWOLPartsReq : IndieSql
        {
            private int maxRows = 50;
            private string queryString = "";
            private string whereClause = "1=2 ";
            private string columns = "AM.REF_WO,AM.ID_PARTS, AM.AM_PARTREQUESTID, AM.DESCRIPTION, AM.REQ_DATE,AM.QTY, AM.PRICE, AM.EXTENDED,AM.CONTRACT_NO,AM.STATUS, AM.STORE_ID, ST.DESCRIPTION PART_DESC, PS.QTY STOCK_ONHAND, PS.COMMITTED_QTY, STS.DESCRIPTION STORE_DESC, AM.TRANSFER_FLAG, AM.TRANSFER_QTY, AM.QTY_RETURN, AM.PENDING_QTY, AM.QTY_FORRETURN, PW.PART_NUMBERS, AM.FOR_COLLECTION_QTY, AM.DISPATCH_BY, AM.DISPATCH_DATE";
            private string columnsSimple = "AM_PARTREQUESTID, DESCRIPTION, QTY, PRICE, EXTENDED, CONTRACT_NO, STATUS, STATUS_BY,STATUS_DATE";
            private string from = "AM_PARTSREQUEST AM LEFT JOIN AM_PARTS ST ON AM.ID_PARTS = ST.ID_PARTS LEFT JOIN PRODUCT_STOCKONHAND PS ON AM.STORE_ID = PS.STORE AND PS.PRODUCT_CODE = AM.ID_PARTS LEFT JOIN STORES STS ON AM.STORE_ID = STS.STORE_ID LEFT OUTER JOIN PARTNO_BYPARTID_V PW ON AM.ID_PARTS = PW.ID_PARTS";

            public IndieReadWOLPartsReq(string dbType, string queryString, int maxRows = 6)
            {
                this.DatabaseType = dbType;
                this.queryString = queryString;
                this.maxRows = maxRows;
                string[] queryArray = queryString.Split(new char[] { ' ' });

                //foreach (string query in queryArray)
                //{
                var query2 = queryString.ToLower();
                whereClause += " OR LOWER(AM.REF_WO) = ('" + query2 + "')";



            }

            public override StmtOracle OracleSql
            {
                get
                {
                    return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
                }
            }

            public override StmtSqlServer SqlServerSql
            {
                get
                {
                    return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PARTREQUESTID ASC" };
                }
            }

            public override StmtMySql MySql
            {
                get
                {
                    return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PARTREQUESTID ASC" + " LIMIT 5000" };
                }
            }


        }

		public class IndieSearchSOHAdjustment : IndieSql
		{
			private int maxRows = 50;
			private string queryString = "";
			private string whereClause = "1=1 ";
			private string columns = "";
			private string columnsSimple = "REQ_NO, REQ_NO, REQ_BY, REQ_DATE, PROBLEM, STATUS, REQ_TYPE, ASSET_NO,JOB_TYPE, JOB_URGENCY";
			private string columnsSql = "AM.REASON,  AM.ID_PARTS, Convert(varchar(11),AM.CREATED_DATE,103) AS CREATED_DATE, ST.DESCRIPTION";
			private string from = "STOCK_ONHAND_ADJUSTMENT AM LEFT JOIN AM_PARTS ST ON AM.ID_PARTS = ST.ID_PARTS";

			public IndieSearchSOHAdjustment(string dbType, string queryString, string idPart, string storeId, SearchFilter filter, int maxRows = 500)
			{
				this.DatabaseType = dbType;
				this.queryString = queryString;
				this.maxRows = maxRows;
				string[] queryArray = queryString.Split(new char[] { ' ' });

				//foreach (string query in queryArray)
				//{
				var query2 = queryString.ToLower();
				if (!string.IsNullOrWhiteSpace(query2))
				{
					whereClause += "  AND (LOWER(AM.ID_PARTS) LIKE ('" + query2 + "%')) ";


				}

				if (idPart != null && idPart != "")
				{
					whereClause += " AND (AM.ID_PARTS ='" + idPart + "' )";
				}
				if (storeId != null && storeId != "")
				{
					whereClause += " AND (AM.STORE_ID ='" + idPart + "' )";
				}

				if (!string.IsNullOrWhiteSpace(filter.FromDate))
				{
					var whereDate = string.Format("AM.CREATED_DATE >= '{0}'", filter.FromDate);
					if (!string.IsNullOrWhiteSpace(filter.ToDate))
					{
						whereDate += string.Format(" and AM.CREATED_DATE <= '{0}'", filter.ToDate);
					}
					whereClause += " AND (" + whereDate + ")";
				}



			}

			public override StmtOracle OracleSql
			{
				get
				{
					return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
				}
			}

			public override StmtSqlServer SqlServerSql
			{
				get
				{
					return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + "ORDER BY  AM.ID DESC "};
				}
			}

			public override StmtMySql MySql
			{
				get
				{
					return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
				}
			}


		}
	}
}
