﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using EmrWebApi.Models;

namespace EmrWebApi.BusinessObjects
{
    public class PartsWorkOrderRpt
    {
        private PetaPoco.Database db;

        public PartsWorkOrderRpt(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }

        public List<dynamic> ParetsSearchByAsset(string queryString, SearchFilter filter, int maxRows = 1000)
        {
            var assetListFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndiePartSearchByAsset(db._dbType.ToString(), queryString, filter, maxRows)));

            if (assetListFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return assetListFound;
        }

        public List<dynamic> PartsSearchByWorkOrder(string queryString, SearchFilter filter, int maxRows = 1000)
        {
            var assetListFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndiePartSearchByWorkOrder(db._dbType.ToString(), queryString, filter, maxRows)));

            if (assetListFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return assetListFound;
        }

    }
    public class IndiePartSearchByAsset : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "A.Req_No,A.Id_Parts,A.ASSET_NO,A.Description,A.PartDesc,A.ReqDate,A.Qty,A.UsedQty,A.Part_Cost,A.DispatchDate,A.part_numbers,A.part_models";
        private string columnsSimple = "DESCRIPTION,";
        private string from = "PartsUsedForReport_V A";

        public IndiePartSearchByAsset(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " And (LOWER(A.ASSET_NO) LIKE Lower('%" + query2 + "%') Or LOWER(A.Description) LIKE Lower('%" + query2 + "%')" + " Or LOWER(A.Req_No) LIKE Lower('%" + query2 + "%'))";
            }
            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " Or (LOWER(A.id_parts) LIKE Lower('%" + query2 + "%') Or LOWER(A.PartDesc) LIKE Lower('%" + query2 + "%'))";
            }
            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " Or (LOWER(A.part_models) LIKE Lower('%" + query2 + "%') Or LOWER(A.part_numbers) LIKE Lower('%" + query2 + "%'))" ;
            }
            //if (!string.IsNullOrWhiteSpace(filter.assetDescription))
            //{
            //    whereClause += " AND (LOWER(A.Description) LIKE ('%" + filter.assetDescription + "%'))";
            //}

            //if (!string.IsNullOrWhiteSpace(filter.AssetFilterSuupiler))
            //{
            //    whereClause += " AND (LOWER(A.SUPPLIER_DESC) LIKE ('%" + filter.AssetFilterSuupiler + "%'))";
            //}
            //if (!string.IsNullOrWhiteSpace(filter.pmtype))
            //{
            //    whereClause += " AND (LOWER(A.PM_TYPE) LIKE ('%" + filter.pmtype + "%'))";
            //}
            //if (!string.IsNullOrWhiteSpace(filter.AssetFilterManufacturer))
            //{
            //    whereClause += " AND (LOWER(A.MANUFACTURER_DESC) LIKE ('%" + filter.AssetFilterManufacturer + "%'))";
            //}

            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format(" A.Dispatched_date >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" AND A.Dispatched_date <= '{0}'", filter.ToDate.Substring(0, 10));
                }
                whereClause += " AND (" + whereDate + ")";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY A.Dispatch_date" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }


    public class IndiePartSearchByWorkOrder : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "A.Req_No,A.Id_Parts,A.ASSET_NO,A.Description,A.PartDesc,A.ReqDate,A.Qty,A.UsedQty,A.Part_Cost,A.DispatchDate,A.part_numbers,A.part_models";
        private string columnsSimple = "DESCRIPTION,";
        private string from = "PartsUsedForReport_V A";

        public IndiePartSearchByWorkOrder(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " And (LOWER(A.ASSET_NO) LIKE Lower('%" + query2 + "%') Or LOWER(A.Description) LIKE Lower('%" + query2 + "%')" + " Or LOWER(A.Req_No) LIKE Lower('%" + query2 + "%'))";
            }
            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " Or (LOWER(A.id_parts) LIKE Lower('%" + query2 + "%') Or LOWER(A.PartDesc) LIKE Lower('%" + query2 + "%'))";
            }
            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " Or (LOWER(A.part_models) LIKE Lower('%" + query2 + "%') Or LOWER(A.part_numbers) LIKE Lower('%" + query2 + "%'))";
            }

            //if (!string.IsNullOrWhiteSpace(filter.AssetFilterSuupiler))
            //{
            //    whereClause += " AND (LOWER(A.SUPPIERNAME) LIKE ('%" + filter.AssetFilterSuupiler + "%'))";
            //}

            //if (!string.IsNullOrWhiteSpace(filter.AssetFilterManufacturer))
            //{
            //    whereClause += " AND (LOWER(A.manufacturer) LIKE ('%" + filter.AssetFilterManufacturer + "%'))";
            //}
            //if(!string.IsNullOrWhiteSpace(filter.Status))
            //{
            //    if (filter.Status== "PSOP") { whereClause += " AND (LOWER(A.wo_status)='op' or (LOWER(A.wo_status)='ne' ))"; } else { 
            //    whereClause += " AND (LOWER(A.wo_status) LIKE ('%" + filter.Status + "%'))";}
            //}

            //if (!string.IsNullOrWhiteSpace(filter.ProblemType))
            //{
            //    whereClause += " AND (Lower(A.WO_Desc)=Lower('" +  filter.ProblemType + "'))";
            //}
            //if (!string.IsNullOrWhiteSpace(filter.pmtype))
            //{
            //    whereClause += " AND (LOWER(A.PM_TYPE) = Lower('" + filter.pmtype + "'))";
            //}

            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format(" A.Dispatched_date >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" AND A.Dispatched_date <= '{0}'", filter.ToDate.Substring(0,10));
                }
                whereClause += " AND (" + whereDate + ")";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY A.Dispatch_date" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }
}
