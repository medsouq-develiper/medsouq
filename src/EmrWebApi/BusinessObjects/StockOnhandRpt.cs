﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using EmrWebApi.Models;

namespace EmrWebApi.BusinessObjects
{
    public class StockOnHandRpt
    {
        private PetaPoco.Database db;

        public StockOnHandRpt(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }

        public List<dynamic> ExampleStockOnHandRpt(string queryString, int maxRows = 1000)
        {
            var assetListFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieExamplStockOnHandRpt(db._dbType.ToString(), queryString,  maxRows)));

            if (assetListFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return assetListFound;
        }

    }
    public class IndieExamplStockOnHandRpt : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1";
        private string columns = "A.Product_Code,A.Description,A.Category,A.Part_id,A.Qty,A.part_numbers,A.part_models,A.Part_Cost,A.location,A.ModelNames";
        private string columnsSimple = "DESCRIPTION,";
        private string from = "StockOnHand_V A";

        public IndieExamplStockOnHandRpt(string dbType, string queryString,  int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " And (LOWER(A.Product_Code) LIKE (Lower('%" + query2 + "%')) Or LOWER(A.Description) LIKE (Lower('%" + query2 + "%')))" ;
            }
            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " Or (LOWER(A.part_numbers) LIKE Lower(('%" + query2 + "%')) Or LOWER(A.part_models) LIKE (Lower('%" + query2 + "%')))"  ;
            }
            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " Or (LOWER(A.location) LIKE Lower(('%" + query2 + "%')))";
            }
            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " Or (LOWER(A.ModelNames) LIKE Lower(('%" + query2 + "%')))";
            }
            //if (!string.IsNullOrWhiteSpace(filter.AssetFilterSuupiler))
            //{
            //    whereClause += " AND (LOWER(A.SUPPIERNAME) LIKE ('%" + filter.AssetFilterSuupiler + "%'))";
            //}

            //if (!string.IsNullOrWhiteSpace(filter.AssetFilterManufacturer))
            //{
            //    whereClause += " AND (LOWER(A.manufacturer) LIKE ('%" + filter.AssetFilterManufacturer + "%'))";
            //}
            //if(!string.IsNullOrWhiteSpace(filter.Status))
            //{
            //    if (filter.Status== "PSOP") { whereClause += " AND (LOWER(A.wo_status)='op' or (LOWER(A.wo_status)='ne' ))"; } else { 
            //    whereClause += " AND (LOWER(A.wo_status) LIKE ('%" + filter.Status + "%'))";}
            //}

            //if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            //{
            //    whereClause += " AND (LOWER(A.COSTCENTERNAME) LIKE ('%" + filter.CostCenterStr + "%'))";
            //}

            //if (!string.IsNullOrWhiteSpace(filter.ProblemType))
            //{
            //    whereClause += " AND (Lower(A.PROBLEM_TYPEDESC)=Lower('" +  filter.ProblemType + "'))";
            //}
            //if (!string.IsNullOrWhiteSpace(filter.pmtype))
            //{
            //    whereClause += " AND (LOWER(A.PM_TYPE) LIKE ('%" + filter.pmtype + "%'))";
            //}

            //if (!string.IsNullOrWhiteSpace(filter.FromDate))
            //{
            //    var whereDate = string.Format("convert(date,A.Req_date) >= '{0}'", filter.FromDate);
            //    if (!string.IsNullOrWhiteSpace(filter.ToDate))
            //    {
            //        whereDate += string.Format(" AND convert(date,A.Req_date) <= '{0}'", filter.ToDate.Substring(0,10));
            //    }
            //    whereClause += " AND (" + whereDate + ")";
            //}
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT " + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY A.Product_Code" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }
}
