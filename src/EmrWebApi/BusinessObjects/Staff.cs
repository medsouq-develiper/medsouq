﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using EmrSys;
using EmrWebApi.Models;

namespace EmrWebApi.BusinessObjects
{

    public class Staff
    {
        private PetaPoco.Database db;

        public Staff(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }

		//create staff Cost Center
		public List<STAFF_COSTCENTER_BUILDING> CreateNewStaffCB(List<STAFF_COSTCENTER_BUILDING> infoList)
		{
			using (var transScope = db.GetTransaction())
			{


				db.Delete<STAFF_COSTCENTER_BUILDING>(" WHERE STAFFCODE ='" + infoList[0].STAFFCODE + "' AND UCATEGORY ='"+infoList[0].UCATEGORY+"'");



				foreach (STAFF_COSTCENTER_BUILDING i in infoList)
				{
					
					db.Insert(i);


				}
				transScope.Complete();
			}
			return infoList;
		}
		public int DeleteStaffCB(string staffId, string uCategory) {
			db.Delete<STAFF_COSTCENTER_BUILDING>(" WHERE STAFFCODE ='" + staffId + "' AND UCATEGORY ='"+ uCategory+"'");
			return 0;
		}
		public List<dynamic> SearchStaffCB(string queryString, string reqAction, string staffId, int maxRows = 1000)
		{

			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStaffCB(db._dbType.ToString(), queryString, reqAction, staffId,  maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}

		//create Staff Store
		public List<STAFF_STORE> CreateNewStaffStore(List<STAFF_STORE> infoList)
		{
			using (var transScope = db.GetTransaction())
			{


				db.Delete<STAFF_STORE>(" WHERE STAFFCODE ='" + infoList[0].STAFFCODE + "'" );



				foreach (STAFF_STORE i in infoList)
				{

					db.Insert(i);


				}
				transScope.Complete();
			}
			return infoList;
		}
		public List<dynamic> SearchStaffStore(string queryString, string staffId, int maxRows = 1000)
		{

			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStaffStore(db._dbType.ToString(), queryString, staffId, maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}
		public STAFF CreateStaffUser(STAFF staffuser)
        {
           
            using (var transScope = db.GetTransaction())
            {


				//apply processed data
				var staffuserFound = db.SingleOrDefault<STAFF>(PetaPoco.Sql.Builder.Select("*").From("STAFF").Where("USER_ID=@userId", new { userId = staffuser.USER_ID }));
				if (staffuserFound != null)
				{
					throw new Exception("USERIDEXIST") { Source = this.GetType().Name };
				}
				else
				{
					staffuserFound = db.SingleOrDefault<STAFF>(PetaPoco.Sql.Builder.Select("*").From("STAFF").Where("STAFF_ID=@staffId", new { staffId = staffuser.STAFF_ID }));
					if (staffuserFound != null)
					{
						throw new Exception("USERIDEXIST") { Source = this.GetType().Name };
					}
				}
					//encrypt password

					var ls_key = "";

				var li_encrypt_len = 0;
				var li_key_len = 0;

				var as_key1 = "bVyYfGhvoVorazlpxowkbkdxGkByGMSxLSdcc";

				var as_toencrypt = staffuser.PASSWORD;

				li_encrypt_len = as_toencrypt.Length;

				ls_key = as_key1 + staffuser.USER_ID;
				li_key_len = ls_key.Length;


				var ls_key_bin = "";

				var iix = 0;

				for (var i = 0; i < as_toencrypt.Length; ++i)
				{
					var res = as_key1.Substring(+iix);
					iix++;
					var restcode = res.Substring(0, 1);
					var keystr = ((int)as_toencrypt[i]).ToString();
					var keyInt = Convert.ToInt32(keystr) * 2;
					char keystrChar = (char)(keyInt);

					ls_key_bin += keystrChar;
					ls_key_bin += restcode;
					//bytes.push(ls_key_bin);
				}
				staffuser.PASSWORD = ls_key_bin;

                staffuser.CLIENT_CODE = "BS1";

                db.Insert(staffuser);

				transScope.Complete();
            }

            return staffuser;
        }

        //notification setup
        public int UpdateNotifySetup(AM_NOTIFYSETUP infoDet)
        {
            //validate data
            if (infoDet != null)
            {
                if (infoDet.NOTIFY_ID == 0)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(infoDet, new string[] { "STATUS", "SMS_MESSAGE", "SMS_MESSAGEOTH","EMAIL_MESSAGE","EMAIL_MESSAGEOTH","SMS_FLAG","EMAIL_FLAG" , "DASHBOARD_FLAG" ,"DASHBOARD_MESSAGE","CREATED_BY","CREATED_DATE"});

                transScope.Complete();
            }

            return 0;
        }
        public AM_NOTIFYSETUP ReadNotifySetup(string infoId)
        {
            AM_NOTIFYSETUP ret = null;

            var notifyFound = db.SingleOrDefault<AM_NOTIFYSETUP>(PetaPoco.Sql.Builder.Select("*").From("AM_NOTIFYSETUP").Where("NOTIFY_ID=@Id", new { Id = infoId }));

            if (notifyFound != null)
            {
                ret = notifyFound;
            }
            else
            {
                throw new Exception("Notify Setup not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public List<dynamic> SearchNotifySetup(string queryString, int maxRows = 50)
        {
            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchNotifySetup(db._dbType.ToString(), queryString, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }

        //notication setup assigned
        public List<AM_NOTIFYSETUPASSIGNED> CreateNewNotificationAssigned(List<AM_NOTIFYSETUPASSIGNED> infoList)
        {
            using (var transScope = db.GetTransaction())
            {


                db.Delete<AM_NOTIFYSETUPASSIGNED>(" WHERE NOTIFY_ID ='" + infoList[0].NOTIFY_ID + "'");



                foreach (AM_NOTIFYSETUPASSIGNED i in infoList)
                {

                    db.Insert(i);


                }
                transScope.Complete();
            }
            return infoList;
        }
        public int DeleteStaffNotify(string notifyId)
        {
            db.Delete<AM_NOTIFYSETUPASSIGNED>(" WHERE NOTIFY_ID ='" + notifyId + "'");
            return 0;
        }
        public List<dynamic> SearchNotifyAssigned(string queryString, string notifyId, int maxRows = 1000)
        {

            var infoFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchNotifySetupAssigned(db._dbType.ToString(), queryString, notifyId, maxRows)));

            if (infoFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return infoFound;
        }

        public STAFF ReadStaffUser(string staffID)
        {
            STAFF ret = null;

            var staffuserFound = db.SingleOrDefault<STAFF>(PetaPoco.Sql.Builder.Select("*").From("STAFF").Where("STAFF_ID=@staffId", new { staffId = staffID }));

            if (staffuserFound != null)
            {
                ret = staffuserFound;
            }
            else
            {
                throw new Exception("Staff not found.") { Source = this.GetType().Name };
            }
            return ret;
        }

        public int UpdateStaffUser(STAFF staffuser, List<USER_BUILDING> userBuilding, List<USER_STORE> userStore, string userId)
        {
            //validate data
            if (staffuser != null)
            {
                if (staffuser.STAFF_ID == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {
                db.Delete<USER_BUILDING>(" WHERE USER_ID='" + staffuser.USER_ID + "'");

                if (userBuilding.Count() > 0)
                {
                    foreach (USER_BUILDING i in userBuilding)
                    {
                        i.USER_ID = staffuser.USER_ID;
                        i.CREATED_DATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                        i.CREATED_BY = userId;
                        db.Insert(i);

                    }
                }


                db.Delete<USER_STORE>(" WHERE USER_ID='" + staffuser.USER_ID + "'");

                if (userStore.Count() > 0)
                {
                    foreach (USER_STORE i in userStore)
                    {
                        i.USER_ID = staffuser.USER_ID;
                        i.CREATED_DATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                        i.CREATED_BY = userId;
                        db.Insert(i);

                    }
                }



                var staffuserFound = db.SingleOrDefault<STAFF>(PetaPoco.Sql.Builder.Select("*").From("STAFF").Where("STAFF_ID=@staffId", new { staffId = staffuser.STAFF_ID }));
				//encrypt password

				if (staffuserFound.PASSWORD != staffuser.PASSWORD)
				{
					var ls_key = "";

					var li_encrypt_len = 0;
					var li_key_len = 0;

					var as_key1 = "bVyYfGhvoVorazlpxowkbkdxGkByGMSxLSdcc";

					var as_toencrypt = staffuser.PASSWORD;

					li_encrypt_len = as_toencrypt.Length;

					ls_key = as_key1 + staffuser.USER_ID;
					li_key_len = ls_key.Length;


					var ls_key_bin = "";

					var iix = 0;

					for (var i = 0; i < as_toencrypt.Length; ++i)
					{
						var res = as_key1.Substring(+iix);
						iix++;
						var restcode = res.Substring(0, 1);
						var keystr = ((int)as_toencrypt[i]).ToString();
						var keyInt = Convert.ToInt32(keystr) * 2;
						char keystrChar = (char)(keyInt);

						ls_key_bin += keystrChar;
						ls_key_bin += restcode;
						//bytes.push(ls_key_bin);
					}
					staffuser.PASSWORD = ls_key_bin;
				}
              
                db.Update(staffuser, new string[]{"FIRST_NAME","LAST_NAME","PASSWORD_EXPIRY","CONTACT_3","LOCATION"
					,"STAFF_GROUP_ID","EMAIL_ADDRESS","STATUS","CONTACT_PRIMARY","CONTACT_SECONDARY","STAFF_POSITION_ID","PAY_ST_DAY","PROMPT_PASSWORD","REQUESTER","HIRE_DATETIME","TERM_DATETIME"
					,"PASSWORD","SYSTEM_ID","DOCTOR_APPS","API","HOME_DEFAULT","SYSTEM_LOOKUP","SRVC_DEPT","CHRG_RATE","PAY_RATE","BEEPER_PH","SCHED_HRS","PAY_PERIOD"});


				transScope.Complete();
            }

            return 0;
        }

        public List<dynamic> SearchUserBuilding(string queryString, int maxRows = 100)
        {
            var servicesFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchUserBuilding(db._dbType.ToString(), queryString, maxRows)));

            if (servicesFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return servicesFound;
        }

        public List<LOV_LOOKUPS> SearchUserBuildingLOV(string userId, int maxRows = 500)
        {
            var servicesFound = db.Fetch<LOV_LOOKUPS>(PetaPoco.Sql.Builder.Append((string)new IndieSearchUserBuildingLOV(db._dbType.ToString(), userId, maxRows)));

            if (servicesFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return servicesFound;
        }

        public List<dynamic> SearchUserStore(string queryString, int maxRows = 100)
        {


            var servicesFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchUserStore(db._dbType.ToString(), queryString, maxRows)));

            if (servicesFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return servicesFound;
        }

        public List<STORES> SearchUserStoreLOV(string userId, int maxRows = 500)
        {
            var servicesFound = db.Fetch<STORES>(PetaPoco.Sql.Builder.Append((string)new IndieSearchUserStoreLOV(db._dbType.ToString(), userId, maxRows)));

            if (servicesFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return servicesFound;
        }


        public List<STAFF_GROUPS> GetStaffGroupList()
        {
            var staffgroupList = db.Fetch<STAFF_GROUPS>(PetaPoco.Sql.Builder.Select("*").From("STAFF_GROUPS").Where("RESTRICTEDFLAG <>'Y'"));

            if (staffgroupList == null)
            {
                throw new Exception("Not Found") { Source = this.GetType().Name };
            }
            return staffgroupList;
        }

        public List<STAFF_POSITIONS> GetStaffPositionsList()
        {
            var staffpositionsList = db.Fetch<STAFF_POSITIONS>(PetaPoco.Sql.Builder.Select("*").From("STAFF_POSITIONS"));

            if (staffpositionsList == null)
            {
                throw new Exception("Not Found") { Source = this.GetType().Name };
            }
            return staffpositionsList;
        }

        public List<dynamic> SearchStaffUser(string queryString, int maxRows = 5000)
        {
            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStaffUser(db._dbType.ToString(), queryString, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }

        public List<dynamic> SearchStaffUserWO(string queryString, int maxRows = 5000)
        {
            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStaffUserWO(db._dbType.ToString(), queryString, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }
        public List<dynamic> SearchStaffUserActive(string queryString, int maxRows = 5000)
        {
            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStaffUserActive(db._dbType.ToString(), queryString, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }

        public List<dynamic> SearchStaffLookUp(string queryString, int maxRows = 10000)
        {
            var staffuserFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStaffLookUp(db._dbType.ToString(), queryString, maxRows)));

            if (staffuserFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return staffuserFound;
        }

        public List<dynamic> SearchStaffById(string queryString, int maxRows = 1000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchStaffById(db._dbType.ToString(), queryString, maxRows)));

            //if (productsFound == null)
            //{
            //    throw new Exception("No match found.") { Source = this.GetType().Name };
            //}
            return productsFound;
        }

        public void Dispose()
        {
            db.Dispose();
        }

    }

    public class IndieSearchUserStore : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "P.USER_ID,S.DESCRIPTION,P.STORE_ID";
        private string columnsSimple = "P.CLIENT_CODE,LOV.DESCRIPTION,P.STORE_ID";
        private string from = "USER_STORE P LEFT OUTER JOIN STORES S ON P.STORE_ID = S.STORE_ID";

        public IndieSearchUserStore(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(P.USER_ID) LIKE ('" + query2 + "%')";

            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }
    public class IndieSearchUserStoreLOV : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "DESCRIPTION,STORE_ID";
        private string columnsSimple = "DESCRIPTION,STORE_ID";
        private string from = "STORES";

        public IndieSearchUserStoreLOV(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " AND  STATUS = 'ACTIVE' AND  STORE_ID NOT IN (SELECT STORE_ID FROM USER_STORE WHERE USER_ID = '" + query2 + "')";

            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }

    public class IndieSearchUserBuildingLOV : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "DESCRIPTION,LOV_LOOKUP_ID";
        private string columnsSimple = "DESCRIPTION,LOV_LOOKUP_ID";
        private string from = "LOV_LOOKUPS";

        public IndieSearchUserBuildingLOV(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " AND CATEGORY = 'AIMS_BUILDING' AND STATUS = 'Y' AND  LOV_LOOKUP_ID NOT IN (SELECT BUILDING_CODE FROM USER_BUILDING WHERE USER_ID = '"+ query2 + "')";

            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }
    public class IndieSearchUserBuilding : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "P.USER_ID,LOV.DESCRIPTION,P.BUILDING_CODE";
        private string columnsSimple = "P.CLIENT_CODE,LOV.DESCRIPTION,P.BUILDING_CODE";
        private string from = "USER_BUILDING P LEFT OUTER JOIN LOV_LOOKUPS LOV ON P.BUILDING_CODE = LOV.LOV_LOOKUP_ID AND CATEGORY = 'AIMS_BUILDING'";

        public IndieSearchUserBuilding(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(P.USER_ID) LIKE ('" + query2 + "%')";

            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }

    public class IndieSearchStaffById : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.STAFF_ID, AM.FIRST_NAME, AM.LAST_NAME, AM.FIRST_NAME+' '+ AM.LAST_NAME AS STAFF_FULLNAME";
        private string columnsSimple = "SUPPLIER_ID, DESCRIPTION, ADDRESS1, CITY, CONTACT, CONTACT_NO1, CONTACT_NO2, EMAIL_ADDRESS";
        private string from = "STAFF AM";

        public IndieSearchStaffById(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
           // string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            if(queryString == null)
            {
                queryString = "";
            }
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.STAFF_ID) = ('" + query2 + "')";




        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause  };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }


    public class IndieSearchStaffUser : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "S.STAFF_ID,S.USER_ID,S.FIRST_NAME,S.LAST_NAME,S.PASSWORD_EXPIRY,S.STATUS,N.DESCRIPTION GROUP_DESCRIPTION,S.CONTACT_PRIMARY,S.EMAIL_ADDRESS,P.DESCRIPTION POSITION_DESCRIPTION, S.SYSTEM_LOOKUP, S.FIRST_NAME +' '+S.LAST_NAME STAFF_NAME";
        private string columnsSimple = "STAFF_ID,USER_ID,FIRST_NAME,LAST_NAME,PASSWORD_EXPIRY,STATUS,GROUP_DESCRIPTION,CONTACT_PRIMARY,EMAIL_ADDRESS,POSITION_DESCRIPTION";
        private string from = "STAFF S  LEFT OUTER JOIN STAFF_GROUPS N ON S.STAFF_GROUP_ID=N.STAFF_GROUP_ID LEFT OUTER JOIN STAFF_POSITIONS P ON S.STAFF_POSITION_ID = P.STAFF_POSITION_ID ";

        public IndieSearchStaffUser(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(S.STAFF_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(S.USER_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(S.CONTACT_PRIMARY) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(S.EMAIL_ADDRESS) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(S.FIRST_NAME) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(S.LAST_NAME) LIKE ('%" + query2 + "%'))";
                //New
                if ( query2 == "" )
                {
                    whereClause += " AND S.STATUS ='ACTIVE'";
                }
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }

    public class IndieSearchStaffUserWO : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "S.ASSIGN_TO,S.STAFF_FULLNAME,S.STATUS";
        private string columnsSimple = "ASSIGN_TO,STAFF_FULLNAME, STATUS";
        private string from = "WORK_ORDER_STAFF_V S";

        public IndieSearchStaffUserWO(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(S.ASSIGN_TO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(S.STAFF_FULLNAME) LIKE ('" + query2 + "%')";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  STAFF_FULLNAME ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }
    public class IndieSearchNotifySetup : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "NT.NOTIFY_ID,NT.DESCRIPTION,NT.SMS_MESSAGE,NT.SMS_MESSAGEOTH,NT.EMAIL_MESSAGE,NT.EMAIL_MESSAGEOTH,NT.SMS_FLAG, NT.EMAIL_FLAG,NT.STATUS,NT.DASHBOARD_FLAG,NT.DASHBOARD_MESSAGE, NT.CREATED_BY,NT.CREATED_DATE";
        private string columnsSimple = "STAFF_ID,USER_ID,FIRST_NAME,LAST_NAME,PASSWORD_EXPIRY,STATUS,GROUP_DESCRIPTION,CONTACT_PRIMARY,EMAIL_ADDRESS,POSITION_DESCRIPTION";
        private string from = "AM_NOTIFYSETUP NT";

        public IndieSearchNotifySetup(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR LOWER(NT.NOTIFY_ID) LIKE ('" + query2 + "%')";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  NOTIFY_ID ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }
    public class IndieSearchNotifySetupAssigned : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.ID, AM.NOTIFY_ID, AM.STAFF_ID, AM.STAFF_GROUP_ID, AM.CREATED_BY, AM.CREATED_DATE";
        private string columnsSimple = "STAFFCODE, CODE, UCATEGORY, CREATED_BY, CREATED_DATE, DESCRIPTION";
        private string from = "AM_NOTIFYSETUPASSIGNED AM";

        public IndieSearchNotifySetupAssigned(string dbType, string queryString, string notifyId, int maxRows = 1500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });


            var query2 = queryString.ToLower();

            if (notifyId != null & notifyId != "")
            {
                whereClause += " OR (AM.NOTIFY_ID ='" + notifyId + "')";
            }


        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + "GROUP BY AM.REQUEST_ID ORDER BY  REQ_DATE DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchStaffUserActive : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "S.STAFF_ID,S.USER_ID,S.FIRST_NAME,S.LAST_NAME";
        private string columnsSimple = "STAFF_ID,USER_ID,FIRST_NAME,LAST_NAME,PASSWORD_EXPIRY,STATUS,GROUP_DESCRIPTION,CONTACT_PRIMARY,EMAIL_ADDRESS,POSITION_DESCRIPTION";
        private string from = "STAFF S";

        public IndieSearchStaffUserActive(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(S.STAFF_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(S.USER_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(S.CONTACT_PRIMARY) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(S.EMAIL_ADDRESS) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(S.FIRST_NAME) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(S.LAST_NAME) LIKE ('%" + query2 + "%'))";
            }
                whereClause += " AND S.STATUS ='ACTIVE'";
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  S.FIRST_NAME ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }
    public class IndieSearchStaffLookUp : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "S.STAFF_ID,S.USER_ID,S.FIRST_NAME,S.LAST_NAME,S.STATUS,N.DESCRIPTION GROUP_DESCRIPTION,S.CONTACT_PRIMARY,S.EMAIL_ADDRESS,P.DESCRIPTION POSITION_DESCRIPTION, P.DESCRIPTION_SHORT POSITION_SHORTDESC, S.FIRST_NAME +' '+ S.LAST_NAME AS STAFF_NAME";
        private string columnsSimple = "STAFF_ID,USER_ID,FIRST_NAME,LAST_NAME,PASSWORD_EXPIRY,STATUS,GROUP_DESCRIPTION,CONTACT_PRIMARY,EMAIL_ADDRESS,POSITION_DESCRIPTION";
        private string from = "STAFF S  LEFT OUTER JOIN STAFF_GROUPS N ON S.STAFF_GROUP_ID=N.STAFF_GROUP_ID LEFT OUTER JOIN STAFF_POSITIONS P ON S.STAFF_POSITION_ID = P.STAFF_POSITION_ID ";

        public IndieSearchStaffLookUp(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(S.SYSTEM_LOOKUP) = ('Y')";
                whereClause += " AND LOWER(S.STATUS) LIKE ('ACTIVE'))";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  FIRST_NAME ASC"};
				}
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  FIRST_NAME ASC LIMIT " + maxRows.ToString() };
            }
        }
    }
	public class IndieSearchStaffCB : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "AM.STAFFCODE, AM.CODE, AM.UCATEGORY, AM.CREATED_BY, AM.CREATED_DATE, LL.DESCRIPTION";
		private string columnsSimple = "STAFFCODE, CODE, UCATEGORY, CREATED_BY, CREATED_DATE, DESCRIPTION";
		private string from = "STAFF_COSTCENTER_BUILDING AM LEFT JOIN CLIENT_COSTCENTER_V LL ON LL.COSTCENTER = AM.CODE";

		public IndieSearchStaffCB(string dbType, string queryString, string retAction, string staffId, int maxRows = 1500)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			
			var query2 = queryString.ToLower();

            if(staffId != null & staffId != "")
            {
                whereClause += " OR (AM.STAFFCODE ='" + staffId + "')";
            }
			

			whereClause += " AND (AM.UCATEGORY ='" + retAction + "')";




		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause  };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + "GROUP BY AM.REQUEST_ID ORDER BY  REQ_DATE DESC" + " LIMIT " + maxRows.ToString() };
			}
		}


	}
	public class IndieSearchStaffStore : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "AM.STAFFCODE, AM.CODE,  AM.CREATED_BY, AM.CREATED_DATE, LL.DESCRIPTION";
		private string columnsSimple = "STAFFCODE, CODE,  CREATED_BY, CREATED_DATE, DESCRIPTION";
		private string from = "STAFF_STORE AM LEFT JOIN STORES LL ON LL.STORE_ID = AM.CODE";

		public IndieSearchStaffStore(string dbType, string queryString,  string staffId, int maxRows = 1500)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });


			var query2 = queryString.ToLower();


			whereClause += " OR (AM.STAFFCODE ='" + staffId + "')";




		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + "GROUP BY AM.REQUEST_ID ORDER BY  REQ_DATE DESC" + " LIMIT " + maxRows.ToString() };
			}
		}


	}
}