﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using EmrWebApi.Models;

namespace EmrWebApi.BusinessObjects
{
    public class AssetReport
    {
        private PetaPoco.Database db;

        public AssetReport(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }

        public List<dynamic> ExampleSearchByAsset(string queryString, SearchFilter filter, int maxRows = 100000)
        {
            var assetListFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieExampleSearchByAsset(db._dbType.ToString(), queryString, filter, maxRows)));

            if (assetListFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return assetListFound;
        }

        //public List<dynamic> ExampleSearchByWorkOrder(string queryString, SearchFilter filter, int maxRows = 1000)
        //{
        //    var assetListFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieExampleSearchByWorkOrder(db._dbType.ToString(), queryString, filter, maxRows)));

        //    if (assetListFound == null)
        //    {
        //        throw new Exception("No match found.") { Source = this.GetType().Name };
        //    }
        //    return assetListFound;
        //}

        //public List<dynamic> ExampleSearchByTestData(string queryString, SearchFilter filter, int maxRows = 1000)
        //{
        //    var assetListFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieExampleSearchByTestData(db._dbType.ToString(), queryString, filter, maxRows)));

        //    if (assetListFound == null)
        //    {
        //        throw new Exception("No match found.") { Source = this.GetType().Name };
        //    }
        //    return assetListFound;
        //}

        //public List<dynamic> ExampleSearchByWorkOrderCM(string queryString, SearchFilter filter, int maxRows = 1000)
        //{
        //    var assetListFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieExampleSearchByWorkOrderCM(db._dbType.ToString(), queryString, filter, maxRows)));

        //    if (assetListFound == null)
        //    {
        //        throw new Exception("No match found.") { Source = this.GetType().Name };
        //    }
        //    return assetListFound;
        //}
    
    }
    public class IndieExampleSearchByAsset : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "A.ASSET_NO, A.DESCRIPTION,A.MODEL_NO,A.MODEL_NAME,A.COSTCENTER_DESC,A.MANUFACTURER_DESC,A.SUPPLIER_DESC,A.INSERVICE_DATE,A.PM_TYPE,A.STATUSDESC,A.SERIAL_NO,"+
                                 "A.FREQUENCY,A.NEXT_PM_DUE,A.PURCHASE_COST, A.WARRANTY_EXPIRY, A.CURRENT_AGENT";
        private string columnsSimple = "DESCRIPTION,";
        private string from = "ASSETINFOREPT_V A";

        public IndieExampleSearchByAsset(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " And (LOWER(A.ASSET_NO) LIKE ('%" + query2 + "%') Or LOWER(A.COSTCENTER_DESC) LIKE Lower('%" + query2 + "%') Or LOWER(A.DESCRIPTION) LIKE LOWER('%" + query2 + "%')";
                whereClause += " Or LOWER(A.MODEL_NO) LIKE ('%" + query2 + "%') Or LOWER(A.MANUFACTURER_DESC) LIKE Lower('%" + query2 + "%') Or LOWER(A.SUPPLIER_DESC) LIKE LOWER('%" + query2 + "%')";
                whereClause += " Or Lower(A.SERIAL_NO) like ('%" + query2 + "%') Or Lower(A.FREQUENCY) like Lower('%" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += " AND (LOWER(A.COSTCENTER_DESC) LIKE '%" + filter.CostCenterStr.ToLower() + "%')";
            }

            if (!string.IsNullOrWhiteSpace(filter.assetDescription))
            {
                whereClause += " AND (LOWER(A.DESCRIPTION) LIKE '%" + filter.assetDescription.ToLower() + "%')";
            }

            if (!string.IsNullOrWhiteSpace(filter.AssetFilterSuupiler))
            {
                whereClause += " AND (LOWER(A.SUPPLIER_DESC) LIKE '%" + filter.AssetFilterSuupiler.ToLower() + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.pmtype))
            {
                whereClause += " AND (LOWER(A.PM_TYPE)='" + filter.pmtype.ToLower() + "')";
            }
            if (!string.IsNullOrWhiteSpace(filter.AssetFilterManufacturer))
            {
                whereClause += " AND (LOWER(A.MANUFACTURER_DESC) LIKE '%" + filter.AssetFilterManufacturer.ToLower() + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.AssetFilterStatus))
            {
                whereClause += " AND (LOWER(A.STATUS) LIKE '%" + filter.AssetFilterStatus.ToLower() + "%')";
            }
            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("Convert(Date,A.INSERVICEDate) >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" AND Convert(Date,A.INSERVICEDate) <= '{0}'", filter.ToDate.Substring(0, 10));
                }
                whereClause += " AND (" + whereDate + ")";
            }
            if (!string.IsNullOrWhiteSpace(filter.warrantyFrom))
            {
                var whereDate = string.Format("Convert(Date,A.WarrantyExpiry) >= '{0}'", filter.warrantyFrom);
                if (!string.IsNullOrWhiteSpace(filter.warrantyTo))
                {
                    whereDate += string.Format(" AND Convert(Date,A.WarrantyExpiry) <= '{0}'", filter.warrantyTo.Substring(0, 10));
                }
                whereClause += " AND (" + whereDate + ")";
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY A.ASSET_NO" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }

    //public class IndieExampleSearchByWorkOrder : IndieSql
    //{
    //    private int maxRows = 50;
    //    private string queryString = "";
    //    private string whereClause = "1=1 ";
    //    private string columns = "A.ASSET_NO,A.REQ_NO,A.ReqDate, A.description, A.COSTCENTERNAME,A.SUPPIERNAME,A.WO_STATUS";
    //    private string columnsSimple = "DESCRIPTION,";
    //    private string from = "WORKORDERS_V A";

    //    public IndieExampleSearchByWorkOrder(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
    //    {
    //        this.DatabaseType = dbType;
    //        this.queryString = queryString;
    //        this.maxRows = maxRows;
    //        string[] queryArray = queryString.Split(new char[] { ' ' });

    //        foreach (string query in queryArray)
    //        {
    //            var query2 = query.ToLower();
    //            whereClause += " And (LOWER(A.ASSET_NO) LIKE ('%" + query2 + "%') And LOWER(A.COSTCENTERNAME) LIKE ('%" + query2 + "%')) And (LOWER(A.REQ_NO) LIKE ('%" + query2 + "%') And LOWER(A.description) LIKE ('%" + query2 + "%'))";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
    //        {
    //            whereClause += " AND (LOWER(A.COSTCENTERNAME) LIKE ('%" + filter.CostCenterStr + "%'))";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.assetDescription))
    //        {
    //            whereClause += " AND (LOWER(A.description) LIKE ('%" + filter.assetDescription + "%'))";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.AssetFilterSuupiler))
    //        {
    //            whereClause += " AND (LOWER(A.SUPPIERNAME) LIKE ('%" + filter.AssetFilterSuupiler + "%'))";
    //        }
    //        if (!string.IsNullOrWhiteSpace(filter.FromDate))
    //        {
    //            var whereDate = string.Format("convert(date,A.Req_date) >= '{0}'", filter.FromDate);
    //            if (!string.IsNullOrWhiteSpace(filter.ToDate))
    //            {
    //                whereDate += string.Format(" AND convert(date,A.Req_date) <= '{0}'", filter.ToDate.Substring(0, 10));
    //            }
    //            whereClause += " AND (" + whereDate + ")";
    //        }
    //    }

    //    public override StmtOracle OracleSql
    //    {
    //        get
    //        {
    //            return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
    //        }
    //    }

    //    public override StmtSqlServer SqlServerSql
    //    {
    //        get
    //        {
    //            return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY A.ReqDate desc" };
    //        }
    //    }

    //    public override StmtMySql MySql
    //    {
    //        get
    //        {
    //            return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
    //        }
    //    }
    //}

    //public class IndieExampleSearchByTestData : IndieSql
    //{
    //    private int maxRows = 50;
    //    private string queryString = "";
    //    private string whereClause = "1=1 ";
    //    private string columns = "A.Code, A.Description, A.Supplier,A.MANUFACTURER_DESC";
    //    private string columnsSimple = "DESCRIPTION,";
    //    private string from = "TestTableView A";

    //    public IndieExampleSearchByTestData(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
    //    {
    //        this.DatabaseType = dbType;
    //        this.queryString = queryString;
    //        this.maxRows = maxRows;
    //        string[] queryArray = queryString.Split(new char[] { ' ' });

    //        foreach (string query in queryArray)
    //        {
    //            var query2 = query.ToLower();
    //            whereClause += " And (LOWER(A.Code) LIKE ('%" + query2 + "%') And LOWER(A.Description) LIKE ('%" + query2 + "%')) ";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.AssetFilterSuupiler))
    //        {
    //            whereClause += " AND (LOWER(A.Supplier) LIKE ('%" + filter.AssetFilterSuupiler + "%'))";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.AssetFilterManufacturer))
    //        {
    //            whereClause += " AND (LOWER(A.MANUFACTURER_DESC) LIKE ('%" + filter.AssetFilterManufacturer + "%'))";
    //        }
    //    }

    //    public override StmtOracle OracleSql
    //    {
    //        get
    //        {
    //            return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
    //        }
    //    }

    //    public override StmtSqlServer SqlServerSql
    //    {
    //        get
    //        {
    //            return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY A.Code" };
    //        }
    //    }

    //    public override StmtMySql MySql
    //    {
    //        get
    //        {
    //            return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
    //        }
    //    }
    //}

    //public class IndieExampleSearchByWorkOrderCM
    //    : IndieSql
    //{
    //    private int maxRows = 50;
    //    private string queryString = "";
    //    private string whereClause = "1=1";
    //    private string columns = "A.req_no, A.asset_no, A.ReqDate,A.Description,A.job_due_date,A.SUPPIERNAME,A.manufacturer,A.wo_status,A.PROBLEM_TYPEDESC,A.assign_to,A.serial_no,A.model_name,A.COSTCENTERNAME,A.PM_TYPE";
    //    private string columnsSimple = "DESCRIPTION,";
    //    private string from = "WorkOrderForReport A";

    //    public IndieExampleSearchByWorkOrderCM(string dbType, string queryString, SearchFilter filter, int maxRows = 50)
    //    {
    //        this.DatabaseType = dbType;
    //        this.queryString = queryString;
    //        this.maxRows = maxRows;
    //        string[] queryArray = queryString.Split(new char[] { ' ' });

    //        foreach (string query in queryArray)
    //        {
    //            var query2 = query.ToLower();
    //            whereClause += " And (LOWER(A.req_no) LIKE ('%" + query2 + "%') and LOWER(A.Description) LIKE ('%" + query2 + "%'))" + " and(LOWER(A.asset_no) LIKE('%" + query2 + "%') ) ";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.AssetFilterSuupiler))
    //        {
    //            whereClause += " AND (LOWER(A.SUPPIERNAME) LIKE ('%" + filter.AssetFilterSuupiler + "%'))";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.AssetFilterManufacturer))
    //        {
    //            whereClause += " AND (LOWER(A.manufacturer) LIKE ('%" + filter.AssetFilterManufacturer + "%'))";
    //        }
    //        if(!string.IsNullOrWhiteSpace(filter.Status))
    //        {
    //            if (filter.Status== "PSOP") { whereClause += " AND (LOWER(A.wo_status)='op' or (LOWER(A.wo_status)='ne' ))"; } else { 
    //            whereClause += " AND (LOWER(A.wo_status) LIKE ('%" + filter.Status + "%'))";}
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
    //        {
    //            whereClause += " AND (LOWER(A.COSTCENTERNAME) LIKE ('%" + filter.CostCenterStr + "%'))";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.ProblemType))
    //        {
    //            whereClause += " AND (Lower(A.PROBLEM_TYPEDESC)=Lower('" +  filter.ProblemType + "'))";
    //        }
    //        if (!string.IsNullOrWhiteSpace(filter.pmtype))
    //        {
    //            whereClause += " AND (LOWER(A.PM_TYPE) LIKE ('%" + filter.pmtype + "%'))";
    //        }

    //        if (!string.IsNullOrWhiteSpace(filter.FromDate))
    //        {
    //            var whereDate = string.Format("convert(date,A.Req_date) >= '{0}'", filter.FromDate);
    //            if (!string.IsNullOrWhiteSpace(filter.ToDate))
    //            {
    //                whereDate += string.Format(" AND convert(date,A.Req_date) <= '{0}'", filter.ToDate.Substring(0,10));
    //            }
    //            whereClause += " AND (" + whereDate + ")";
    //        }
    //    }

    //    public override StmtOracle OracleSql
    //    {
    //        get
    //        {
    //            return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
    //        }
    //    }

    //    public override StmtSqlServer SqlServerSql
    //    {
    //        get
    //        {
    //            return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY A.Req_No" };
    //        }
    //    }

    //    public override StmtMySql MySql
    //    {
    //        get
    //        {
    //            return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
    //        }
    //    }
    //}
}
