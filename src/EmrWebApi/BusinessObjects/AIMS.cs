﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using EmrWebApi.Models;
using System.Data;
using System.Reflection;
using System.ComponentModel;
using System.Net;


namespace EmrWebApi.BusinessObjects
{
    class AIMS
    { 
        #region CLASS
        private PetaPoco.Database db = null;

        public AIMS(PetaPoco.Database sharedDBInstance)
        {
            db = sharedDBInstance;
        }

        public void Dispose()
        {
            db.Dispose();
        }
        #endregion
        //QUICK VIEW
        public WORK_ORDER_QUICKVIEW_V ReadWorkOrderQuickView(string reqID)
        {
            WORK_ORDER_QUICKVIEW_V ret = null;

            var woFound = db.SingleOrDefault<WORK_ORDER_QUICKVIEW_V>(PetaPoco.Sql.Builder.Select("*").From("WORK_ORDER_QUICKVIEW_V").Where("QVWONO=@reqId", new { reqId = reqID }));

            if (woFound != null)
            {
                ret = woFound;
            }
            else
            {
                throw new Exception("Work Order not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public ASSET_QUICKVIEW_V ReadAssetQuickView(string assetNO)
        {
            ASSET_QUICKVIEW_V ret = null;

            var woFound = db.SingleOrDefault<ASSET_QUICKVIEW_V>(PetaPoco.Sql.Builder.Select("*").From("ASSET_QUICKVIEW_V").Where("AQV_ASSET_NO=@reqId", new { reqId = assetNO }));

            if (woFound != null)
            {
                ret = woFound;
            }
            
            return ret;
        }

        //Work Order Information
        public AM_WORK_ORDERS CreateNewWordOrder(AM_WORK_ORDERS woDetails)
        {
			AM_WORK_ORDERS_STATUS_LOG woStatus = new AM_WORK_ORDERS_STATUS_LOG();

            using (var transScope = db.GetTransaction())
            {
                db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='AIMS_ORDERNO'");
                var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "AIMS_ORDERNO" }));

                woDetails.REQ_NO = Convert.ToString(seq[0].SEQ_VAL);
               
				//apply processed data
				if (woDetails.TICKET_ID > 0) {
					db.Execute("UPDATE AM_HELPDESK SET REF_WO=" + woDetails.REQ_NO + ", WO_STATUS='PENDING' WHERE TICKET_ID='" + woDetails.TICKET_ID + "'");
                    db.Execute("UPDATE AM_NOTIFY_MESSAGES SET STATUS='D' WHERE REF_CODE='" + woDetails.TICKET_ID + "' AND NOTIFY_ID= 7");
                }


                //UPDATE THE LAST_REQ_NO IN PM SCHEDULE
                if(woDetails.REQ_TYPE == "PM")
                {
                    var pmFound = db.SingleOrDefault<AM_ASSET_PMSCHEDULES>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_PMSCHEDULES").Where("ASSET_NO=@assetId", new { assetId = woDetails.ASSET_NO }));
                    if (pmFound != null)
                    {
                        pmFound.LAST_REQ_NO = woDetails.REQ_NO;
                        db.Update(pmFound, new string[] { "LAST_REQ_NO" });
                    }
                }

                
                    woStatus.STAFF_ID = woDetails.CREATED_BY;
				woStatus.RECORDED_DATE = woDetails.CREATED_DATE;
				woStatus.REQ_NO = woDetails.REQ_NO;
                woStatus.WO_STATUS = woDetails.WO_STATUS;
                db.Insert(woStatus);
                if (woDetails.ASSIGN_TO !=null && woDetails.ASSIGN_TO != "")
                {
                    woDetails.ASSIGN_BY = woDetails.CREATED_BY;
                    woDetails.ASSIGN_DATE = woDetails.CREATED_DATE;
                }


				db.Insert(woDetails);

                transScope.Complete();
            }

            return woDetails;
        }
        public AM_WORK_ORDERS ReadWorkOrder(string reqID)
        {
            AM_WORK_ORDERS ret = null;

            var woFound = db.SingleOrDefault<AM_WORK_ORDERS>(PetaPoco.Sql.Builder.Select("*").From("AM_WORK_ORDERS").Where("REQ_NO=@reqId", new { reqId = reqID }));

            if (woFound != null)
            {
                ret = woFound;
            }
            else
            {
                throw new Exception("Work Order not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public AM_WORK_ORDERS ReadWorkOrderByTicketNo(int ticketNo)
        {
            return  db.SingleOrDefault<AM_WORK_ORDERS>(PetaPoco.Sql.Builder.Select("*").From("AM_WORK_ORDERS").Where("TICKET_ID=@ticketId", new { ticketId = ticketNo }));

        }

        public int UpdateWorkOrder(AM_WORK_ORDERS woDetails,string modifyDate, string modifyBy)
        {

            AM_WORK_ORDERS_STATUS_LOG woStatus = new AM_WORK_ORDERS_STATUS_LOG();

            AM_WORK_ORDER_ASSIGN_LOG woAssign = new AM_WORK_ORDER_ASSIGN_LOG();
            AM_WORK_ORDERS_AUDIT woAudit = new AM_WORK_ORDERS_AUDIT();

            //validate data
            if (woDetails != null)
            {
                if (woDetails.ASSET_NO == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data


                DateTime modifyDt = DateTime.Parse(modifyDate);


                if (woDetails.TICKET_ID > 0 && woDetails.WO_STATUS == "HA")
				{
					db.Execute("UPDATE AM_HELPDESK SET REF_WO=" + woDetails.REQ_NO + ", WO_STATUS='" + woDetails.WO_STATUS + "' , STATUS='ONHOLD' WHERE TICKET_ID='" + woDetails.TICKET_ID + "'");
				}
				else if (woDetails.TICKET_ID > 0 && woDetails.WO_STATUS == "CL")
				{
					db.Execute("UPDATE AM_HELPDESK SET REF_WO=" + woDetails.REQ_NO + ", WO_STATUS='" + woDetails.WO_STATUS + "' , STATUS='COMPLETED'  WHERE TICKET_ID='" + woDetails.TICKET_ID + "'");
				}
				else if(woDetails.TICKET_ID > 0)
				{
					db.Execute("UPDATE AM_HELPDESK SET REF_WO='" + woDetails.REQ_NO + "', WO_STATUS='"+ woDetails.WO_STATUS+"' WHERE TICKET_ID='" + woDetails.TICKET_ID + "'");
				}


                var woFound = db.SingleOrDefault<AM_WORK_ORDERS>(PetaPoco.Sql.Builder.Select("*").From("AM_WORK_ORDERS").Where("REQ_NO=@reqId", new { reqId = woDetails.REQ_NO }));
                
                woAudit.REQ_NO = woFound.REQ_NO;
                woAudit.REQ_BY = woFound.REQ_BY;
                woAudit.REQUESTER_CONTACT_NO = woFound.REQUESTER_CONTACT_NO;
                woAudit.REQ_DATE = woFound.REQ_DATE;
                woAudit.PROBLEM = woFound.PROBLEM;
                woAudit.WO_STATUS = woFound.WO_STATUS;
                woAudit.REQ_REMARKS = woFound.REQ_REMARKS;
                woAudit.REQ_TYPE = woFound.REQ_TYPE;
                woAudit.ASSET_NO = woFound.ASSET_NO;
                woAudit.JOB_TYPE = woFound.JOB_TYPE;
                woAudit.SERV_DEPT = woFound.SERV_DEPT;
                woAudit.ASSIGN_TYPE = woFound.ASSIGN_TYPE;
                woAudit.ASSIGN_TO = woFound.ASSIGN_TO;
                woAudit.ASSIGN_BY = woFound.ASSIGN_BY;
                woAudit.ASSIGN_DATE = woFound.ASSIGN_DATE;
                woAudit.SPECIALTY = woFound.SPECIALTY;
                woAudit.AM_INSPECTION_ID = woFound.AM_INSPECTION_ID;
                woAudit.TICKET_ID = woFound.TICKET_ID;
                woAudit.REQ_STAFF_ID = woFound.REQ_STAFF_ID;
                woAudit.JOB_URGENCY = woFound.JOB_URGENCY;
                woAudit.PARTS_CONFIRMED = woFound.PARTS_CONFIRMED;
                woAudit.JOB_DUE_DATE = woFound.JOB_DUE_DATE;
                woAudit.JOB_DURATION = woFound.JOB_DURATION;
                woAudit.WO_REFERENCE_NOTES = woFound.WO_REFERENCE_NOTES;
                woAudit.WONOTE_RECORDED_BY = woFound.WONOTE_RECORDED_BY;
                woAudit.WONOTE_RECORDED_DATE = woFound.WONOTE_RECORDED_DATE;
                woAudit.ASSET_LOCATION = woFound.ASSET_LOCATION;
                woAudit.ASSET_CONDITION = woFound.ASSET_CONDITION;
                woAudit.ASSET_COSTCENTER = woFound.ASSET_COSTCENTER;
                woAudit.CREATED_BY = woFound.CREATED_BY;
                woAudit.CREATED_DATE = woFound.CREATED_DATE;
                woAudit.ASSET_COSTCENTERCODE = woFound.ASSET_COSTCENTERCODE;
                woAudit.ASSIGN_OUTSOURCE = woFound.ASSIGN_OUTSOURCE;
                woAudit.FAULT_FINDING = woFound.FAULT_FINDING;
                woAudit.LAST_MODIFY_BY = modifyBy;
                woAudit.LAST_MODIFY_BY = modifyBy;
                woAudit.LAST_MODIFY_DATE = modifyDt;
                db.Insert(woAudit);
                if (woFound.WO_STATUS != woDetails.WO_STATUS)
                {

                    //DateTime modifyDt = DateTime.Parse(modifyDate);
                    woStatus.STAFF_ID = modifyBy;
                    woStatus.RECORDED_DATE = modifyDt;
                    woStatus.REQ_NO = woDetails.REQ_NO;
                    woStatus.WO_STATUS = woDetails.WO_STATUS;
                    
                    if (woDetails.WO_STATUS == "CL")
                    {  
                        woDetails.CLOSED_BY = modifyBy;
                        //woDetails.CLOSED_DATE = modifyDt;
                        woStatus.RECORDED_DATE = woDetails.CLOSED_DATE;
                    };
                    db.Insert(woStatus);
                }

                if(woFound.ASSIGN_TO != woDetails.ASSIGN_TO && woFound.ASSIGN_TO != null)
                {
                    //DateTime modifyDt = DateTime.Parse(modifyDate);
                    woAssign.ASSIGN_TO = woFound.ASSIGN_TO;
                    woAssign.ASSIGN_BY = woFound.ASSIGN_BY;
                    woAssign.REQ_NO = woDetails.REQ_NO;
                    woAssign.ASSIGN_DATE = woFound.ASSIGN_DATE;
                    db.Insert(woAssign);
                    woDetails.ASSIGN_BY = modifyBy;
                    woDetails.ASSIGN_DATE = modifyDt;
                }




                if (woFound.WO_REFERENCE_NOTES != woDetails.WO_REFERENCE_NOTES && woDetails.WO_REFERENCE_NOTES != null && woDetails.WO_REFERENCE_NOTES != "")
                {
                    AM_WORK_ORDER_REFNOTES woNotes = new AM_WORK_ORDER_REFNOTES();
                    if(woFound.WONOTE_RECORDED_DATE != null && woFound.WONOTE_RECORDED_BY != null)
                    {
                        woNotes.ASSET_NO = woFound.ASSET_NO;
                        woNotes.WO_NOTES = woFound.WO_REFERENCE_NOTES;
                        woNotes.REQ_NO = woFound.REQ_NO;
                        woNotes.CREATED_BY = woFound.WONOTE_RECORDED_BY;
                        woNotes.CREATED_DATE = woFound.WONOTE_RECORDED_DATE;
                        db.Insert(woNotes);
                    }

                    if (woDetails.ASSET_NO != null && woDetails.ASSET_NO != "")
                    {
                        var woAssetFound = db.SingleOrDefault<AM_ASSET_DETAILS>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_DETAILS").Where("LAST_REQ_NO=@reqId", new { reqId = woDetails.REQ_NO }));

                        if (woAssetFound != null)
                        {
                            if(woAssetFound.LAST_REQ_NO != woDetails.REQ_NO)
                            {
                                woNotes.ASSET_NO = woAssetFound.ASSET_NO;
                                woNotes.WO_NOTES = woAssetFound.WO_REFERENCE_NOTES;
                                woNotes.REQ_NO = woAssetFound.LAST_REQ_NO;
                                woNotes.CREATED_BY = woAssetFound.WONOTE_RECORDED_BY;
                                woNotes.CREATED_DATE = woAssetFound.WONOTE_RECORDED_DATE;
                                db.Insert(woNotes);
                            }
                           

                        }
                        woAssetFound = db.SingleOrDefault<AM_ASSET_DETAILS>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_DETAILS").Where("ASSET_NO=@assetId", new { assetId = woDetails.ASSET_NO }));

                        woAssetFound.WO_REFERENCE_NOTES = woDetails.WO_REFERENCE_NOTES;
                        woAssetFound.WONOTE_RECORDED_BY = woDetails.WONOTE_RECORDED_BY;
                        woAssetFound.WONOTE_RECORDED_DATE = woDetails.WONOTE_RECORDED_DATE;
                        woAssetFound.LAST_REQ_NO = woDetails.REQ_NO;
                        db.Update(woAssetFound, new string[] { "WO_REFERENCE_NOTES", "WONOTE_RECORDED_BY", "WONOTE_RECORDED_DATE", "LAST_REQ_NO" });

                        //db.Execute("UPDATE AM_ASSET_DETAILS SET WO_REFERENCE_NOTES='" + woDetails.WO_REFERENCE_NOTES + "', WONOTE_RECORDED_BY = '" + woDetails.WONOTE_RECORDED_BY + "' , WONOTE_RECORDED_DATE='" + Convert(nvarchar(20), Cast(woDetails.WONOTE_RECORDED_DATE as datetime), 101) + "' , LAST_REQ_NO ='" + woDetails.REQ_NO+ "' WHERE ASSET_NO='" +woDetails.ASSET_NO + "'");

                    }
                }


                // Update PM Schedule
                if (woDetails.REQ_TYPE == "PM" && woDetails.WO_STATUS == "CL" && woDetails.ASSET_NO != null){
                   // DateTime modifyDt = DateTime.Parse(modifyDate);
                    var pmFound = db.SingleOrDefault<AM_ASSET_PMSCHEDULES>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_PMSCHEDULES").Where("ASSET_NO=@assetId", new { assetId = woDetails.ASSET_NO }));

                    if (pmFound != null)
                    {
                        var pmwoFound = db.SingleOrDefault<AM_ASSET_PMSCHEDULES>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_PMSCHEDULES").Where("ASSET_NO=@assetId", new { assetId = woDetails.ASSET_NO }));//.Where("LAST_REQ_NO=@reqId", new { reqId = woDetails.REQ_NO })

                        if (pmwoFound != null)
                        {


                            //var nextDueDate = pmwoFound.NEXT_PM_DUE;
                            //DateTime nextDue = DateTime.Parse(nextDueDate.ToString());

                            //var countdays = 0;
                            //if (pmFound.INTERVAL == "D")
                            //{
                            //    countdays = Int32.Parse(pmFound.FREQUENCY_PERIOD);
                            //    nextDue = nextDue.AddDays(countdays);
                            //}
                            //else if (pmFound.INTERVAL == "W")
                            //{
                            //    countdays = Int32.Parse(pmFound.FREQUENCY_PERIOD) * 7;
                            //    nextDue = nextDue.AddDays(countdays);

                            //}
                            //else if (pmFound.INTERVAL == "M")
                            //{
                            //    countdays = Int32.Parse(pmFound.FREQUENCY_PERIOD) * 30;
                            //    nextDue = nextDue.AddDays(countdays);

                            //}
                            //else if (pmFound.INTERVAL == "Y")
                            //{
                            //    countdays = Int32.Parse(pmFound.FREQUENCY_PERIOD) * 365;
                            //    nextDue = nextDue.AddDays(countdays);

                            //}
                            //pmFound.NEXT_PM_DUE = nextDue;
                            //pmFound.LAST_REQ_NO = woDetails.REQ_NO;
                            //db.Update(pmFound, new string[] { "NEXT_PM_DUE", "LAST_REQ_NO" });

                        }
        
                    }
                }

               


                db.Update(woDetails, new string[] { "FAULT_FINDING", "ASSIGN_OUTSOURCE","ASSET_COSTCENTERCODE", "ASSET_COSTCENTER", "ASSET_LOCATION", "REQ_BY", "REQ_DATE", "PROBLEM", "WO_STATUS", "REQ_REMARKS","WO_REFERENCE_NOTES","WONOTE_RECORDED_BY","WONOTE_RECORDED_DATE"
                    , "REQ_TYPE", "ASSET_NO","JOB_TYPE","SERV_DEPT","SPECIALTY","JOB_URGENCY","JOB_DUE_DATE","JOB_DURATION",
                "ASSIGN_TYPE","ASSIGN_TO","REQUESTER_CONTACT_NO","AM_INSPECTION_ID","CLOSED_BY","CLOSED_DATE", "ASSIGN_BY","ASSIGN_DATE"});




                transScope.Complete();
            }

            return 0;
        }
        public int UpdateWOBuilk(List<AM_WORK_ORDER_BULKUPATES> woList, string updateAction, string staffId)
        {

            using (var transScope = db.GetTransaction())
            {
                AM_WORK_ORDERS_STATUS_LOG woStatus = new AM_WORK_ORDERS_STATUS_LOG();
                AM_WORK_ORDER_ASSIGN_LOG woAssign = new AM_WORK_ORDER_ASSIGN_LOG();
                foreach (AM_WORK_ORDER_BULKUPATES i in woList)
                {


                    var itemFound = db.SingleOrDefault<AM_WORK_ORDERS>(PetaPoco.Sql.Builder.Select("*").From("AM_WORK_ORDERS").Where("REQ_NO=@refId", new { refId = i.REQ_NO }));
                    if (itemFound != null)
                    {
                        if (updateAction == "CLOSE")
                        {
                            itemFound.WO_STATUS = i.WO_STATUS;
                            itemFound.CLOSED_BY = staffId;
                            itemFound.CLOSED_DATE = i.CLOSED_DATE;
                            db.Update(itemFound, new string[] { "WO_STATUS", "CLOSED_BY", "CLOSED_DATE" });

                            woStatus.STAFF_ID = staffId;
                            woStatus.RECORDED_DATE = i.CLOSED_DATE;
                            woStatus.REQ_NO = i.REQ_NO;
                            woStatus.WO_STATUS = i.WO_STATUS;
                            db.Insert(woStatus);

                        }
                        else
                        {

                            if (itemFound.ASSIGN_TO != i.ASSIGN_TO && itemFound.ASSIGN_TO != null)
                            {


                                woAssign.ASSIGN_TO = itemFound.ASSIGN_TO;
                                woAssign.ASSIGN_BY = itemFound.ASSIGN_BY;
                                woAssign.REQ_NO = itemFound.REQ_NO;
                                woAssign.ASSIGN_DATE = itemFound.ASSIGN_DATE;
                                db.Insert(woAssign);

                            }

                            itemFound.ASSIGN_TO = i.ASSIGN_TO;
                            itemFound.ASSIGN_BY = i.ASSIGN_BY;
                            itemFound.ASSIGN_DATE = i.ASSIGN_DATE;
                            db.Update(itemFound, new string[] { "ASSIGN_TO", "ASSIGN_BY", "ASSIGN_DATE" });
                        }

                    }
                }

                transScope.Complete();
            }

            return 0;
        }
        public int UpdateWorkDetails(AM_WORK_ORDERS woDetails, string modifyDate, string modifyBy)
        {

            // AM_WORK_ORDERS_STATUS_LOG woStatus = new AM_WORK_ORDERS_STATUS_LOG();

            //  AM_WORK_ORDER_ASSIGN_LOG woAssign = new AM_WORK_ORDER_ASSIGN_LOG();
            // AM_WORK_ORDERS_AUDIT woAudit = new AM_WORK_ORDERS_AUDIT();

            //validate data
            if (woDetails != null)
            {
                if (woDetails.ASSET_NO == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data


                DateTime modifyDt = DateTime.Parse(modifyDate);


                //if (woDetails.TICKET_ID > 0 && woDetails.WO_STATUS == "HA")
                //{
                //    db.Execute("UPDATE AM_HELPDESK SET REF_WO=" + woDetails.REQ_NO + ", WO_STATUS='" + woDetails.WO_STATUS + "' , STATUS='ONHOLD' WHERE TICKET_ID='" + woDetails.TICKET_ID + "'");
                //}
                //else if (woDetails.TICKET_ID > 0 && woDetails.WO_STATUS == "CL")
                //{
                //    db.Execute("UPDATE AM_HELPDESK SET REF_WO=" + woDetails.REQ_NO + ", WO_STATUS='" + woDetails.WO_STATUS + "' , STATUS='COMPLETED'  WHERE TICKET_ID='" + woDetails.TICKET_ID + "'");
                //}
                //else if (woDetails.TICKET_ID > 0)
                //{
                //    db.Execute("UPDATE AM_HELPDESK SET REF_WO='" + woDetails.REQ_NO + "', WO_STATUS='" + woDetails.WO_STATUS + "' WHERE TICKET_ID='" + woDetails.TICKET_ID + "'");
                //}


                //var woFound = db.SingleOrDefault<AM_WORK_ORDERS>(PetaPoco.Sql.Builder.Select("*").From("AM_WORK_ORDERS").Where("REQ_NO=@reqId", new { reqId = woDetails.REQ_NO }));

                //woAudit.REQ_NO = woFound.REQ_NO;
                //woAudit.REQ_BY = woFound.REQ_BY;
                //woAudit.REQUESTER_CONTACT_NO = woFound.REQUESTER_CONTACT_NO;
                //woAudit.REQ_DATE = woFound.REQ_DATE;
                //woAudit.PROBLEM = woFound.PROBLEM;
                //woAudit.WO_STATUS = woFound.WO_STATUS;
                //woAudit.REQ_REMARKS = woFound.REQ_REMARKS;
                //woAudit.REQ_TYPE = woFound.REQ_TYPE;
                //woAudit.ASSET_NO = woFound.ASSET_NO;
                //woAudit.JOB_TYPE = woFound.JOB_TYPE;
                //woAudit.SERV_DEPT = woFound.SERV_DEPT;
                //woAudit.ASSIGN_TYPE = woFound.ASSIGN_TYPE;
                //woAudit.ASSIGN_TO = woFound.ASSIGN_TO;
                //woAudit.ASSIGN_BY = woFound.ASSIGN_BY;
                //woAudit.ASSIGN_DATE = woFound.ASSIGN_DATE;
                //woAudit.SPECIALTY = woFound.SPECIALTY;
                //woAudit.AM_INSPECTION_ID = woFound.AM_INSPECTION_ID;
                //woAudit.TICKET_ID = woFound.TICKET_ID;
                //woAudit.REQ_STAFF_ID = woFound.REQ_STAFF_ID;
                //woAudit.JOB_URGENCY = woFound.JOB_URGENCY;
                //woAudit.PARTS_CONFIRMED = woFound.PARTS_CONFIRMED;
                //woAudit.JOB_DUE_DATE = woFound.JOB_DUE_DATE;
                //woAudit.JOB_DURATION = woFound.JOB_DURATION;
                //woAudit.WO_REFERENCE_NOTES = woFound.WO_REFERENCE_NOTES;
                //woAudit.WONOTE_RECORDED_BY = woFound.WONOTE_RECORDED_BY;
                //woAudit.WONOTE_RECORDED_DATE = woFound.WONOTE_RECORDED_DATE;
                //woAudit.ASSET_LOCATION = woFound.ASSET_LOCATION;
                //woAudit.ASSET_CONDITION = woFound.ASSET_CONDITION;
                //woAudit.ASSET_COSTCENTER = woFound.ASSET_COSTCENTER;
                //woAudit.CREATED_BY = woFound.CREATED_BY;
                //woAudit.CREATED_DATE = woFound.CREATED_DATE;
                //woAudit.ASSET_COSTCENTERCODE = woFound.ASSET_COSTCENTERCODE;
                //woAudit.ASSIGN_OUTSOURCE = woFound.ASSIGN_OUTSOURCE;
                //woAudit.FAULT_FINDING = woFound.FAULT_FINDING;
                //woAudit.LAST_MODIFY_BY = modifyBy;
                //woAudit.LAST_MODIFY_BY = modifyBy;
                //woAudit.LAST_MODIFY_DATE = modifyDt;
                //db.Insert(woAudit);
                //if (woFound.WO_STATUS != woDetails.WO_STATUS)
                //{

                //    woStatus.STAFF_ID = modifyBy;
                //    woStatus.RECORDED_DATE = modifyDt;
                //    woStatus.REQ_NO = woDetails.REQ_NO;
                //    woStatus.WO_STATUS = woDetails.WO_STATUS;

                //    if (woDetails.WO_STATUS == "CL")
                //    {
                //        woDetails.CLOSED_BY = modifyBy;
                //        woStatus.RECORDED_DATE = woDetails.CLOSED_DATE;
                //    };
                //    db.Insert(woStatus);
                //}

                //if (woFound.ASSIGN_TO != woDetails.ASSIGN_TO && woFound.ASSIGN_TO != null)
                //{
                //    //DateTime modifyDt = DateTime.Parse(modifyDate);
                //    woAssign.ASSIGN_TO = woFound.ASSIGN_TO;
                //    woAssign.ASSIGN_BY = woFound.ASSIGN_BY;
                //    woAssign.REQ_NO = woDetails.REQ_NO;
                //    woAssign.ASSIGN_DATE = woFound.ASSIGN_DATE;
                //    db.Insert(woAssign);
                //    woDetails.ASSIGN_BY = modifyBy;
                //    woDetails.ASSIGN_DATE = modifyDt;
                //}




                //if (woFound.WO_REFERENCE_NOTES != woDetails.WO_REFERENCE_NOTES && woDetails.WO_REFERENCE_NOTES != null && woDetails.WO_REFERENCE_NOTES != "")
                //{
                //    AM_WORK_ORDER_REFNOTES woNotes = new AM_WORK_ORDER_REFNOTES();
                //    if (woFound.WONOTE_RECORDED_DATE != null && woFound.WONOTE_RECORDED_BY != null)
                //    {
                //        woNotes.ASSET_NO = woFound.ASSET_NO;
                //        woNotes.WO_NOTES = woFound.WO_REFERENCE_NOTES;
                //        woNotes.REQ_NO = woFound.REQ_NO;
                //        woNotes.CREATED_BY = woFound.WONOTE_RECORDED_BY;
                //        woNotes.CREATED_DATE = woFound.WONOTE_RECORDED_DATE;
                //        db.Insert(woNotes);
                //    }

                //    //if (woDetails.ASSET_NO != null && woDetails.ASSET_NO != "")
                //    //{
                //    //    var woAssetFound = db.SingleOrDefault<AM_ASSET_DETAILS>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_DETAILS").Where("LAST_REQ_NO=@reqId", new { reqId = woDetails.REQ_NO }));

                //    //    if (woAssetFound != null)
                //    //    {
                //    //        if (woAssetFound.LAST_REQ_NO != woDetails.REQ_NO)
                //    //        {
                //    //            woNotes.ASSET_NO = woAssetFound.ASSET_NO;
                //    //            woNotes.WO_NOTES = woAssetFound.WO_REFERENCE_NOTES;
                //    //            woNotes.REQ_NO = woAssetFound.LAST_REQ_NO;
                //    //            woNotes.CREATED_BY = woAssetFound.WONOTE_RECORDED_BY;
                //    //            woNotes.CREATED_DATE = woAssetFound.WONOTE_RECORDED_DATE;
                //    //            db.Insert(woNotes);
                //    //        }


                //    //    }
                //    //    woAssetFound = db.SingleOrDefault<AM_ASSET_DETAILS>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_DETAILS").Where("ASSET_NO=@assetId", new { assetId = woDetails.ASSET_NO }));

                //    //    woAssetFound.WO_REFERENCE_NOTES = woDetails.WO_REFERENCE_NOTES;
                //    //   // woAssetFound.LAST_REQ_NO = woDetails.REQ_NO;
                //    //    db.Update(woAssetFound, new string[] { "WO_REFERENCE_NOTES" });

                //    //}
                //}

                db.Update(woDetails, new string[] { "FAULT_FINDING", "ASSIGN_OUTSOURCE","ASSET_COSTCENTERCODE", "ASSET_COSTCENTER", "ASSET_LOCATION", "REQ_BY", "REQ_DATE", "PROBLEM", "WO_STATUS", "REQ_REMARKS","WO_REFERENCE_NOTES","WONOTE_RECORDED_BY","WONOTE_RECORDED_DATE"
                    , "REQ_TYPE", "ASSET_NO","JOB_TYPE","SERV_DEPT","SPECIALTY","JOB_URGENCY","JOB_DUE_DATE","JOB_DURATION",
                "ASSIGN_TYPE","ASSIGN_TO","REQUESTER_CONTACT_NO","AM_INSPECTION_ID","CLOSED_BY","CLOSED_DATE"});


                transScope.Complete();
            }

            return 0;
        }
        public List<dynamic> SearchWorkOrder(string queryString, string queryAssetNo,SearchFilter filter, string searchStatus, string searchbyStaffId,  string userId, int maxRows = 20000)
        {

            var userBuildingFound = db.Fetch<USER_BUILDING>(PetaPoco.Sql.Builder.Select("*").From("USER_BUILDING").Where("USER_ID=@refId", new { refId = userId }));

            if (userBuildingFound.Count() == 0)
            {
                userId = null;
            }

            var productsFound = new List<dynamic>();

            var woFound = db.SingleOrDefault<AM_WORK_ORDERS>(PetaPoco.Sql.Builder.Select("*").From("AM_WORK_ORDERS").Where("REQ_NO=@reqId", new { reqId = queryString }));
            if (woFound != null)
            {
                filter.Status = null;
                productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchWorkOrder(db._dbType.ToString(), queryString, queryAssetNo, filter, searchStatus, searchbyStaffId, userId, maxRows)));

                if (productsFound == null)
                {
                    throw new Exception("No match found.") { Source = this.GetType().Name };
                }
            }
            else
            {

                productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchWorkOrder(db._dbType.ToString(), queryString, queryAssetNo, filter, searchStatus, searchbyStaffId, userId, maxRows)));

                if (productsFound == null)
                {
                    throw new Exception("No match found.") { Source = this.GetType().Name };
                }

            }


            return productsFound;
        }
        public List<dynamic> SearchWorkOrderStatus(string queryString, string reqId, int maxRows = 10000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchWorkOrderStatus(db._dbType.ToString(), queryString, reqId, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchWorkOrderAssign(string queryString, SearchFilter filter,  int maxRows = 20000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchWorkOrderAssign(db._dbType.ToString(), queryString ,filter, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        
        //add/Update Warranty
        public List<AM_ASSET_WARRANTY> CreateUpdateAssetWarranty(List<AM_ASSET_WARRANTY> warrantyList, string assetNo, string modifyBy)
        {
            using (var transScope = db.GetTransaction())
            {

                var saudiDateTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");

                string[] UPDATE_AAM_ASSET_WARRANTY = new string[]
                    { "LAST_MODIFIED_DATE","LAST_MODIFIED_BY","FREQUENCY_PERIOD", "FREQUENCY", "WARRANTY_EXPIRY", "WARRANTY_EXTENDED" ,"WARRANTY_INCLUDED","WARRANTY_NOTES"};


                foreach (AM_ASSET_WARRANTY i in warrantyList)
                {


                    AM_SYSTEM_AUDIT systemAudit = new AM_SYSTEM_AUDIT();
                    var itemFound = db.SingleOrDefault<AM_ASSET_WARRANTY>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_WARRANTY").Where("ID=@refId", new { refId = i.ID }).Where("ASSET_NO=@refAsset", new { refAsset = i.ASSET_NO }));
                    if (itemFound != null)
                    {
 
                        if (itemFound.WARRANTY_EXPIRY != i.WARRANTY_EXPIRY || itemFound.WARRANTY_INCLUDED != i.WARRANTY_INCLUDED || itemFound.WARRANTY_NOTES != i.WARRANTY_NOTES || itemFound.FREQUENCY != i.FREQUENCY || itemFound.FREQUENCY_PERIOD != i.FREQUENCY_PERIOD)
                        {

                            var properties = TypeDescriptor.GetProperties(typeof(AM_ASSET_WARRANTY));
                            foreach (PropertyDescriptor property in properties)
                            {
                                var fieldname = property.Name;
                                var prop = UPDATE_AAM_ASSET_WARRANTY.Where(x => x.ToString() == fieldname.ToString() && x.ToString() != "LAST_MODIFIED_DATE" && x.ToString() != "LAST_MODIFIED_BY").Count() > 0;

                                if (prop)
                                {
                                    var newdata = property.GetValue(i);
                                    var olddata = property.GetValue(itemFound);
                                   
                                    if (newdata == null)
                                    {
                                        newdata = "";
                                    }
                                    if (olddata == null)
                                    {
                                        olddata = "";
                                    }
                                    if (newdata.ToString() != olddata.ToString())
                                    {
                                        systemAudit.REF_ID = i.ID.ToString();
                                        systemAudit.FIELD_NAME = fieldname.ToString();
                                        systemAudit.ACTION_CODE = "AM_ASSET_WARRANTY";
                                        systemAudit.OLDDATA = olddata.ToString();
                                        systemAudit.NEWDATA = newdata.ToString();
                                        systemAudit.RECORDED_BY = modifyBy;
                                        systemAudit.RECORDED_DATE = saudiDateTime;
                                        db.Insert(systemAudit);
                                    }
                                }

                            }



                            itemFound.LAST_MODIFIED_BY = modifyBy;
                            itemFound.LAST_MODIFIED_DATE = saudiDateTime;
                        }
                        itemFound.WARRANTY_EXPIRY = i.WARRANTY_EXPIRY;
                        itemFound.WARRANTY_EXTENDED = i.WARRANTY_EXTENDED;
                        itemFound.WARRANTY_INCLUDED = i.WARRANTY_INCLUDED;
                        itemFound.WARRANTY_NOTES = i.WARRANTY_NOTES;
                        itemFound.FREQUENCY = i.FREQUENCY;
                        itemFound.FREQUENCY_PERIOD = i.FREQUENCY_PERIOD;
                        
                        db.Update(itemFound, UPDATE_AAM_ASSET_WARRANTY);
                    }
                    else
                    {
                        i.ASSET_NO = assetNo;
                        db.Insert(i);
                    }


                    
                    
                }
                //apply processed data


                transScope.Complete();
            }

            var woWarrantyFound = db.Fetch<AM_ASSET_WARRANTY>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetWarranty(db._dbType.ToString(), assetNo, 1000)));

            return woWarrantyFound;
        }
        public List<AM_ASSET_WARRANTY> ReadWarrantyListByAssetNo(string queryString, int maxRows = 1000)
        {
            var woNotesFound = db.Fetch<AM_ASSET_WARRANTY>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetWarranty(db._dbType.ToString(), queryString, maxRows)));

            if (woNotesFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return woNotesFound;
        }
        public List<dynamic> ReadWarrantyListByAssetNoV(string queryString, int maxRows = 1000)
        {
            var warrantyFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetWarrantyV(db._dbType.ToString(), queryString, maxRows)));

            if (warrantyFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return warrantyFound;
        }

        //Cost Center Warranty
        public List<AM_ASSET_COSTCENTER_WARRANTY> CreateUpdateAssetCostCenterWarranty(List<AM_ASSET_COSTCENTER_WARRANTY> ccwarrantyList, string assetNo,  string modifyBy, int transId)
        {
            using (var transScope = db.GetTransaction())
            {
                if(transId > 0 && assetNo !=null){
                    var saudiDateTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");

                    string[] UPDATE_AAM_ASSET_WARRANTY = new string[]
                        { "LAST_MODIFIED_DATE","LAST_MODIFIED_BY","CCFREQUENCY_PERIOD", "CCFREQUENCY", "CCWARRANTY_EXPIRY", "CCWARRANTY_EXTENDED" ,"CCWARRANTY_INCLUDED","CCWARRANTY_NOTES"};


                    foreach (AM_ASSET_COSTCENTER_WARRANTY i in ccwarrantyList)
                    {


                        var itemFound = db.SingleOrDefault<AM_ASSET_COSTCENTER_WARRANTY>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_COSTCENTER_WARRANTY").Where("CCID=@refId", new { refId = i.CCID }).Where("ASSET_NO=@refAsset", new { refAsset = i.ASSET_NO }).Where("TRANSID=@refTransId", new { refTransId = transId }));

                        if (itemFound != null)
                        {
                            itemFound.CCWARRANTY_EXPIRY = i.CCWARRANTY_EXPIRY;
                            itemFound.CCWARRANTY_EXTENDED = i.CCWARRANTY_EXTENDED;
                            itemFound.CCWARRANTY_INCLUDED = i.CCWARRANTY_INCLUDED;
                            itemFound.CCWARRANTY_NOTES = i.CCWARRANTY_NOTES;
                            itemFound.CCFREQUENCY = i.CCFREQUENCY;
                            itemFound.CCFREQUENCY_PERIOD = i.CCFREQUENCY_PERIOD;
                            itemFound.LAST_MODIFIED_BY = modifyBy;

                            itemFound.LAST_MODIFIED_DATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");

                            db.Update(itemFound, UPDATE_AAM_ASSET_WARRANTY);

                        }
                        else
                        {
                            i.TRANSID = transId;
                            i.ASSET_NO = assetNo;
                            i.CREATED_BY = modifyBy;
                            i.CREATED_DATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                            db.Insert(i);
                        }




                    }

                    transScope.Complete();
                   
                }
                //apply processed data

            }
            var ccWarrantyFound = db.Fetch<AM_ASSET_COSTCENTER_WARRANTY>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetCostCenterWarranty(db._dbType.ToString(), assetNo, 1000)));
            return ccWarrantyFound;
        }
        public List<AM_ASSET_COSTCENTER_WARRANTY> ReadCostCenterWarrantyListAssetNo(string assetNo, int transId, int maxRows = 1000)
        {
            var woNotesFound = db.Fetch<AM_ASSET_COSTCENTER_WARRANTY>(PetaPoco.Sql.Builder.Append((string)new IndieSearchCostCenterAssetWarranty(db._dbType.ToString(), assetNo, transId, maxRows)));

            if (woNotesFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return woNotesFound;
        }


        public List<AM_ASSET_TRANSACTION> ReadCostCenterTransactionAssetNo(string assetNo, int transId, int maxRows = 1000)
        {
            var transFound = db.Fetch<AM_ASSET_TRANSACTION>(PetaPoco.Sql.Builder.Append((string)new IndieSearchCostCenterTransactionAsset(db._dbType.ToString(), assetNo, transId, maxRows)));

            if (transFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return transFound;
        }
        public int UpdateAssetTransaction(AM_ASSET_TRANSACTION assettransaction, AM_ASSET_DETAILS assetDetails, string costCenter, string modifiedBy)
        {
            //validate data
            if (assettransaction != null)
            {
                if (assettransaction.TRANSID == 0)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {
                var currDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                DateTime cToday = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");


                //preprocess data
                var assetTransFound = db.SingleOrDefault<AM_ASSET_TRANSACTION>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_TRANSACTION").Where("TRANSID=@reqId", new { reqId = assettransaction.TRANSID }));
                if(assetTransFound != null)
                {
                    SYSTEM_TRANSACTIONDETAILS systemTrans = new SYSTEM_TRANSACTIONDETAILS();
                    var costcenterdescriptionFound = db.SingleOrDefault<COSTCENTER_DETAILS>(PetaPoco.Sql.Builder.Select("Description").From("COSTCENTER_DETAILS").Where("CODEID=@reqId", new { reqId = assettransaction.COSTCENTERCODE }));

                    var costcennterName = "";
                    if (costcenterdescriptionFound != null)
                    {
                        costcennterName = costcenterdescriptionFound.DESCRIPTION;
                    }

                    var existcostcentername = "";

                    var existcostcenterdescriptionFound = db.SingleOrDefault<COSTCENTER_DETAILS>(PetaPoco.Sql.Builder.Select("Description").From("COSTCENTER_DETAILS").Where("CODEID=@reqId", new { reqId = assetDetails.COST_CENTER }));

                    existcostcentername = existcostcenterdescriptionFound.DESCRIPTION;


                    systemTrans.REFCODE = assetDetails.ASSET_NO;
                    systemTrans.REFDESCRIPTION = assetDetails.DESCRIPTION;
                    systemTrans.REFCOSTCENTERCODE = assetTransFound.COSTCENTERCODE;
                    systemTrans.REFCOST = assetTransFound.COSTCENTERPURCHASECOST;
                    systemTrans.REFCOSTCENTERDESCRIPTION = existcostcentername;
                    systemTrans.REFMODEL = assetDetails.MODEL_NO;
                    systemTrans.REFBATCHSERIALNO = assetDetails.SERIAL_NO;
                    systemTrans.REFINVOICENO = assetTransFound.COSTCENTERINVOICENO;
                    systemTrans.REASONTYPE = assettransaction.RETURNEDREASON;
                    systemTrans.REFTYPE = "Asset";
                    systemTrans.REFQTY = 1;
                    systemTrans.REFUOM = "Piece";
                    systemTrans.REPORTSTATUS = "For Reporting";

                    if (assettransaction.RETURNEDREASON == "Recall")
                    {
                        systemTrans.COSTCENTERMESSAGE = "This equipment was " + assettransaction.RETURNEDREASON + "  by " + costcennterName + " on " + String.Format("{0:dd/MM/yyyy}", cToday) + ".";
                        systemTrans.MESSAGE = "This equipment was " + assettransaction.RETURNEDREASON + "  by " + costcennterName + " on " + String.Format("{0:dd/MM/yyyy}", cToday) + ".";
                    }
                    else
                    {
                        systemTrans.COSTCENTERMESSAGE = "This equipment was " + assettransaction.RETURNEDREASON + "  to " + costcennterName + " on " + String.Format("{0:dd/MM/yyyy}", cToday) + ".";
                        systemTrans.MESSAGE = "This equipment was " + assettransaction.RETURNEDREASON + "  to " + costcennterName + " on " + String.Format("{0:dd/MM/yyyy}", cToday) + ".";
                    }

                   

                    systemTrans.STATUS = "Y";
                    systemTrans.CREATEDBY = modifiedBy;
                    systemTrans.CREATEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                    db.Insert(systemTrans);


                    assettransaction.RETURNCREATEDBY = modifiedBy;
                    assettransaction.RETURNCREATEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time"); ;

                    //apply processed data
                    db.Update(assettransaction, new string[] { "RETURNEDREASON", "RETURNEDREMARKS", "RETURNCREATEDDATE", "RETURNCREATEDBY", "STATUS" });

                    db.Execute("UPDATE AM_ASSET_DETAILS SET COST_CENTER='" + costCenter + "' WHERE ASSET_NO='" + assettransaction.ASSET_NO + "'");


                    transScope.Complete();
                }
                
            }

            return 0;
        }


        //Asset Notes
        public AM_ASSET_NOTES CreateNewAssetNotes(AM_ASSET_NOTES rfDetails)
        {

            using (var transScope = db.GetTransaction())
            {


                //apply processed data
                db.Insert(rfDetails);

                transScope.Complete();
            }

            return rfDetails;
        }
        public List<dynamic> SearchAssetNotes(string queryString, int maxRows = 1000)
        {
            var woNotesFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetNotes(db._dbType.ToString(), queryString, maxRows)));

            if (woNotesFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return woNotesFound;
        }
        //Work Order Ref Notes
        public AM_WORK_ORDER_REFNOTES CreateNewWORefNotes(AM_WORK_ORDER_REFNOTES rfDetails)
        {

            using (var transScope = db.GetTransaction())
            {


                //apply processed data
                db.Insert(rfDetails);

                transScope.Complete();
            }

            return rfDetails;
        }
        public List<dynamic> SearchWORefNotes(string queryString, int maxRows = 1000)
        {
            var woNotesFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchWORefNotes(db._dbType.ToString(), queryString, maxRows)));

            if (woNotesFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return woNotesFound;
        }
        public List<dynamic> SearchAssetWORefNotes(string queryString, int maxRows = 10000)
        {
            var woNotesFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetWORefNotes(db._dbType.ToString(), queryString, maxRows)));

            if (woNotesFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return woNotesFound;
        }

        //Manufacturer Model
        public AM_MANUFACTURER_MODEL CreateNewModel(AM_MANUFACTURER_MODEL rfDetails)
        {

            using (var transScope = db.GetTransaction())
            {

                db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='MODEL_ID'");

                var seq = db.SingleOrDefault<SEQ>(PetaPoco.Sql.Builder.Select("*").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "MODEL_ID" }));
                var seqId = seq.SEQ_VAL.ToString();
                rfDetails.MODEL_ID = seq.SEQ_VAL;

                //apply processed data
                db.Insert(rfDetails);

                transScope.Complete();
            }

            return rfDetails;
        }
        public AM_MANUFACTURER_MODEL ReadModel(string reqID)
        {
            AM_MANUFACTURER_MODEL ret = null;

            var woFound = db.SingleOrDefault<AM_MANUFACTURER_MODEL>(PetaPoco.Sql.Builder.Select("*").From("AM_MANUFACTURER_MODEL").Where("MODEL_ID=@reqId", new { reqId = reqID }));

            if (woFound != null)
            {
                ret = woFound;
            }
            else
            {
                throw new Exception("Work Order not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdateModel(AM_MANUFACTURER_MODEL rfDetails)
        {
            //validate data
            if (rfDetails != null)
            {
                if (rfDetails.MODEL_ID == 0)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(rfDetails, new string[] { "DESCRIPTION", "VENDOR", "TYPE", "MODEL", "LIST_PRICE"
                    , "FACILITY", "MODEL_NAME","SALVAGE_VALUE","TEST_EQU","REPLACEMENT_COST","CRITICAL","COVERAGE_ID","FREQUENCY","FREQ_UNIT","NOTE","EXPIRE_DATETIME"});

                transScope.Complete();
            }

            return 0;
        }
        public List<dynamic> SearchModel(string queryString, int maxRows = 10000)
        {
            var woNotesFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchModel(db._dbType.ToString(), queryString, maxRows)));

            if (woNotesFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return woNotesFound;
        }
        public List<dynamic> SearchModelManufacturer(string queryString,string manufacturerId, int maxRows = 10000)
        {
            var woNotesFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchModelManufacturerId(db._dbType.ToString(), queryString, manufacturerId, maxRows)));

            if (woNotesFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return woNotesFound;
        }
        //ASSET Risk Factor Setup
        public RISK_CATEGORYFACTOR CreateNewRiskFactorSetup(RISK_CATEGORYFACTOR rfDetails)
		{

			using (var transScope = db.GetTransaction())
			{
				

				//apply processed data
				db.Insert(rfDetails);

				transScope.Complete();
			}

			return rfDetails;
		}
		public RISK_CATEGORYFACTOR ReadRiskFactorSetup(string reqID)
		{
			RISK_CATEGORYFACTOR ret = null;

			var woFound = db.SingleOrDefault<RISK_CATEGORYFACTOR>(PetaPoco.Sql.Builder.Select("*").From("RISK_CATEGORYFACTOR").Where("RISK_FACTORCODE=@reqId", new { reqId = reqID }));

			if (woFound != null)
			{
				ret = woFound;
			}
			else
			{
				throw new Exception("Work Order not found.") { Source = this.GetType().Name };
			}
			return ret;
		}
		public int UpdateRiskFactorSetup(RISK_CATEGORYFACTOR rfDetails)
		{
			//validate data
			if (rfDetails != null)
			{
				if (rfDetails.RISK_FACTORCODE == 0)
					throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
			}

			using (var transScope = db.GetTransaction())
			{

				//preprocess data

				//apply processed data
				db.Update(rfDetails, new string[] { "RISK_DESCRIPTION", "RISK_CATEGORYCODE", "RISK_SCORE"});

				transScope.Complete();
			}

			return 0;
		}
		public List<dynamic> SearchRiskFactorSetup(string queryString, string reftype, int maxRows = 1000)
		{
			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchRiskFactorSetup(db._dbType.ToString(), queryString, reftype, maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}
		public List<dynamic> SearchRiskFactorGroupSetup(string queryString, int maxRows = 1000)
		{
			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchRiskFactorGroup(db._dbType.ToString(), queryString, maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}

		//ASSET PARTS
		public AM_PARTS CreateNewAssetPart(AM_PARTS partDetails)
        {

            using (var transScope = db.GetTransaction())
            {
                var partsFound = db.SingleOrDefault<AM_PARTS>(PetaPoco.Sql.Builder.Select("*").From("AM_PARTS").Where("DESCRIPTION=@Id", new { Id = partDetails.DESCRIPTION }));
                if (partsFound != null)
                {
                    throw new Exception("Duplicate") { Source = this.GetType().Name };
                }

                var nextseq = "";
                if (partDetails.TYPE_ID == "AIMS_MSTYPE-1")
                {
                    db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='PRODUCTCODEPR'");
                    var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "PRODUCTCODEPR" }));
                    nextseq = seq[0].SEQ_VAL.ToString();

                    nextseq = "CS" + nextseq;
                }
                else if (partDetails.TYPE_ID == "AIMS_MSTYPE-2")
                {
                    db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='PRODUCTCODECS'");
                    var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "PRODUCTCODECS" }));
                    nextseq = seq[0].SEQ_VAL.ToString();
                    nextseq = "PR" + nextseq;
                }
                else if (partDetails.TYPE_ID == "AIMS_MSTYPE-3")
                {
                    db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='PRODUCTCODEEQ'");
                    var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "PRODUCTCODEEQ" }));
                    nextseq = seq[0].SEQ_VAL.ToString();
                    nextseq = "EQ" + nextseq;
                }
                else
                {
                    db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='PRODUCTCODEOT'");
                    var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "PRODUCTCODEOT" }));
                    nextseq = seq[0].SEQ_VAL.ToString();
                    nextseq = "OT" + nextseq;
                }

                partDetails.ID_PARTS = nextseq;
                //apply processed data
                db.Insert(partDetails);

                transScope.Complete();
            }

            return partDetails;
        }
        public AM_PROD_CUSTOMER_DISCOUNT CreateOrUpdateCustomerDiscount(AM_PROD_CUSTOMER_DISCOUNT customerDiscount)
        {
            using (var transScope = db.GetTransaction())
            {
                if (customerDiscount.id == 0)
                {
                    if (customerDiscount.status == "Active")
                    {
                        var customerDiscounts = db.Fetch<AM_PROD_CUSTOMER_DISCOUNT>(PetaPoco.Sql.Builder.Select("*").From("am_prod_customer_discount").Where("am_part_id = @am_part_id and status = 'Active'", new { am_part_id = customerDiscount.am_part_id }));
                        foreach (var record in customerDiscounts)
                        {
                            record.status = "Inactive";
                            db.Update(record);
                        }
                    }
                    db.Insert(customerDiscount);
                }
                else
                {
                    if (customerDiscount.status == "Active")
                    {
                        var customerDiscounts = db.Fetch<AM_PROD_CUSTOMER_DISCOUNT>(PetaPoco.Sql.Builder.Select("*").From("am_prod_customer_discount").Where("am_part_id = @am_part_id and id <> @id and status = 'Active'", new { am_part_id = customerDiscount.am_part_id, id = customerDiscount.id }));
                        foreach (var record in customerDiscounts)
                        {
                            record.status = "Inactive";
                            db.Update(record);
                        }
                    }
                    db.Update(customerDiscount);
                }
                //apply processed data
                transScope.Complete();
            }

            return customerDiscount;
        }
        public AM_PARTS ReadAssetPart(string reqID)
        {
            AM_PARTS ret = null;

            var woFound = db.SingleOrDefault<AM_PARTS>(PetaPoco.Sql.Builder.Select("*").From("AM_PARTS").Where("ID_PARTS=@reqId", new { reqId = reqID }));

            if (woFound != null)
            {
                ret = woFound;
            }
            else
            {
                throw new Exception("Work Order not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public PARTNO_BYPARTID_V ReadAssetPartSupplier(string reqID)
        {
            PARTNO_BYPARTID_V ret = null;

            var woFound = db.SingleOrDefault<PARTNO_BYPARTID_V>(PetaPoco.Sql.Builder.Select("*").From("PARTNO_BYPARTID_V").Where("ID_PARTS=@reqId", new { reqId = reqID }));

            if (woFound != null)
            {
                ret = woFound;
            }
           
            return ret;
        }
        public int UpdateAssetPart(AM_PARTS partDetails, List<AM_PARTS_MODELDETAILS> partModelsList)
        {
            //validate data
            if (partDetails != null)
            {
                if (partDetails.ID_PARTS == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data
                if(partModelsList.Count()> 0)
                {
                    foreach (AM_PARTS_MODELDETAILS i in partModelsList)
                    {
                        var itemFound = db.SingleOrDefault<AM_PARTS_MODELDETAILS>(PetaPoco.Sql.Builder.Select("*").From("AM_PARTS_MODELDETAILS").Where("MODEL_ID=@refId", new { refId = i.MODEL_ID }).Where("ID_PARTS=@refPart", new { refPart = partDetails.ID_PARTS }));
                        if(itemFound != null)
                        {

                        }
                        else
                        {
                            i.ID_PARTS = partDetails.ID_PARTS;
                            db.Insert(i);
                        }

                    }
                }
                else
                {
                    var itemModelsFound = db.SingleOrDefault<AM_PARTS_MODELDETAILS>(PetaPoco.Sql.Builder.Select("*").From("AM_PARTS_MODELDETAILS").Where("ID_PARTS=@refPart", new { refPart = partDetails.ID_PARTS }));
                    if(itemModelsFound != null)
                    {
                        db.Delete<AM_PARTS_MODELDETAILS>(" WHERE ID_PARTS='" + partDetails.ID_PARTS + "'");
                    }
                }

                //apply processed data
                db.Update(partDetails, new string[] { "DESCRIPTION", "ANCILLARY", "CATEGORY", "COMMITTABLE", "EXPENSE_RECEIVED","PART_COST","PART_KIT","MODEL_ID"
                    , "ACCOUNT", "FACILITY","REMARKS","ALTERNATIVE_ITEM","STATUS","STOCK_ITEM_FLAG", "TYPE_ID", "UOM_ID"});

                transScope.Complete();
            }

            return 0;
        }
        public List<dynamic> SearchAmPartsActive2(string queryString, int maxRows = 1000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAmPartsActive2(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchAssetParts(string queryString, int maxRows = 1000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchParts(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchAssetPartsActive(string queryString, int maxRows = 1000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPartsActive(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        //ASSET PARTS MODELS
        public List<dynamic> SearchAssetPartsModels(string queryString, int maxRows = 1000)
        {
            var partsModelsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPartModels(db._dbType.ToString(), queryString, maxRows)));
            return partsModelsFound;
        }

        public List<dynamic> SearchCustomerDiscount(string queryString, int maxRows = 1000)
        {
            var customerDiscount = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchCustomerDiscount(db._dbType.ToString(), queryString, maxRows)));
            return customerDiscount;
        }

        //INSPECTION ASSETS
        public AM_INSPECTIONASSET CreateNewWSAsset(AM_INSPECTIONASSET infoDetails)
        {

            using (var transScope = db.GetTransaction())
            {

                //apply processed data
                db.Insert(infoDetails);

                transScope.Complete();
            }

            return infoDetails;
        }
        public List<AM_INSPECTIONASSET> CreateNewInspectionAsset(List<AM_INSPECTIONASSET> InspectionAssetList, string userId, string createDate)
        {
            using (var transScope = db.GetTransaction())
            {

                DateTime createDt = DateTime.Parse(createDate);

                db.Delete<AM_INSPECTIONASSET>(" WHERE AM_INSPECTION_ID='" + InspectionAssetList[0].REF_INSPECTION_ID + "'");

                foreach (AM_INSPECTIONASSET i in InspectionAssetList)
                {

                   

                    i.CREATED_DATE = createDt;
                    i.CREATED_BY = userId;
                    db.Insert(i);
                    //        }


                }
                transScope.Complete();
            }
            return InspectionAssetList;
        }

        public AM_INSPECTIONASSET DeleteWSAsset(string refId)
        {
            AM_INSPECTIONASSET ret = null;

            var imageFound = db.SingleOrDefault<AM_INSPECTIONASSET>(PetaPoco.Sql.Builder.Select("*").From("AM_INSPECTIONASSET").Where("AM_INSPECTIONASSETID=@refID", new { refID = refId }));

            if (imageFound != null)
            {
                ret = imageFound;
                using (var transScope = db.GetTransaction())
                {

                    //apply processed data
                    db.Delete(imageFound);

                    transScope.Complete();
                }
            }
            else
            {
                throw new Exception("Equipment not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public List<dynamic> SearchInspectionAsset(string queryString, int maxRows = 1000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchInspectionAsset(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        //WORK ORDER PM PARTS 
        public List<AM_WOPM_PARTS> CreateNewWOPMPart(List<AM_WOPM_PARTS> WOPMPartsList, string userId, string createDate)
        {
            using (var transScope = db.GetTransaction())
            {

                DateTime createDt = DateTime.Parse(createDate);

                db.Delete<AM_WOPM_PARTS>(" WHERE REFID='" + WOPMPartsList[0].REFID + "' AND REF_TYPE='" + WOPMPartsList[0].REF_TYPE + "'");

                foreach (AM_WOPM_PARTS i in WOPMPartsList)
                {

                    //var itemFound = db.SingleOrDefault<AM_WOPM_PARTS>(PetaPoco.Sql.Builder.Select("*").From("AM_WOPM_PARTS").Where("REF_TYPE=@batchId", new { batchId = i.REF_TYPE }).Where("REFID=@refId", new { refId = i.REFID }).Where("ID_PARTS=@partId", new { partId = i.ID_PARTS }));
                    //if (itemFound != null)
                    //{
                        
                    //    //itemFound.DESCRIPTION = i.DESCRIPTION;
                    //    //itemFound.QTY = i.QTY;
                    //    //db.Update(itemFound, new string[] { "DESCRIPTION", "QTY" });
                    //}
                    //else
                    //{


                        i.CREATED_DATE = createDt;
                    i.CREATED_BY = userId;
                        db.Insert(i);
            //        }

                   
                }
                transScope.Complete();
            }
            return WOPMPartsList;
        }
        public AM_WOPM_PARTS ReadWOPMPart(string reqID)
        {
            AM_WOPM_PARTS ret = null;

            var woFound = db.SingleOrDefault<AM_WOPM_PARTS>(PetaPoco.Sql.Builder.Select("*").From("AM_WOPM_PARTS").Where("WOPM_PARTS_ID=@reqId", new { reqId = reqID }));

            if (woFound != null)
            {
                ret = woFound;
            }
            else
            {
                throw new Exception("Work Order not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdateWOPMPart(AM_WOPM_PARTS wopmpartDetails)
        {
            //validate data
            if (wopmpartDetails != null)
            {
                if (wopmpartDetails.WOPM_PARTS_ID == 0)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(wopmpartDetails, new string[] { "DESCRIPTION", "ID_PARTS", "REFID", "REF_TYPE", "QTY"
                    , "PRICE_ITEM", "EXTENDED","REMARKS","BILLABLE"});

                transScope.Complete();
            }

            return 0;
        }
        public List<dynamic> SearchWOPMParts(string queryString,string refId, int maxRows = 1000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchWOPMParts(db._dbType.ToString(), queryString, refId, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchWOPMPartsById(string queryString, string refType, string refId, int maxRows = 1000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchWOPMPartsById(db._dbType.ToString(), queryString, refType, refId,  maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }


        //PARTS REQUEST
        public List<AM_PARTSREQUEST> CreateNewPartRequest(List<AM_PARTSREQUEST> requestList, string userId, string[]tempInfo, string createDate)
        {
            using (var transScope = db.GetTransaction())
            {

                DateTime createDt = DateTime.Parse(createDate);

                //db.Delete<AM_WOPM_PARTS>(" WHERE AM_PARTREQUESTID='" + WOPMPartsList[0].AM_PARTREQUESTID + "' AND REF_TYPE='" + WOPMPartsList[0].REF_TYPE + "'");

                foreach (AM_PARTSREQUEST i in requestList)
                {

                    var itemFound = db.SingleOrDefault<AM_PARTSREQUEST>(PetaPoco.Sql.Builder.Select("*").From("AM_PARTSREQUEST").Where("AM_ASSET_PMSCHEDULE_ID=@refId", new { refId = i.AM_PARTREQUESTID }).Where("REF_WO=@requestId", new { requestId = i.REF_WO }));
                    if (itemFound != null)
                    {

                        //itemFound.DESCRIPTION = i.DESCRIPTION;
                        //itemFound.QTY = i.QTY;
                        //db.Update(itemFound, new string[] { "DESCRIPTION", "QTY" });
                    }
                    else
                    {


                        i.CREATED_DATE = createDt;
                        i.CREATED_BY = userId;
                        db.Insert(i);
                    }


                }
                transScope.Complete();
            }
            return requestList;
        }

        //PURCHASE REQUEST
        public AM_PURCHASEREQUEST CreateNewPurchaseRequest(AM_PURCHASEREQUEST prDetails,List<AM_PARTSREQUEST> requestList, string userId, string createDate, string astionStr)
        {
            using (var transScope = db.GetTransaction())
            {

                DateTime createDt = DateTime.Parse(createDate);
				
				SEQ seq = null;
                var seqId = "";
                if (prDetails.REQUEST_ID == null || prDetails.REQUEST_ID == "")
                {
                    db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='PURCHASE_REQUEST'");

                    seq = db.SingleOrDefault<SEQ>(PetaPoco.Sql.Builder.Select("*").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "PURCHASE_REQUEST" }));
                    seqId = seq.SEQ_VAL.ToString();
                }else
                {
                    seqId = prDetails.REQUEST_ID;
                   //gaspar db.Delete<AM_PURCHASEREQUEST_ITEMS>(" WHERE REQUEST_ID ='" + seqId + "'");
                }

                //var approveOfficerFound = db.Fetch<APPROVING_OFFICER>(PetaPoco.Sql.Builder.Append((string)new IndieSearchApproveOfficerCat(db._dbType.ToString(), "PR", 1000)));

                //if (approveOfficerFound != null)
                //{
                //    AM_NOTIFY_MESSAGES woNotify = new AM_NOTIFY_MESSAGES();
                    
                //    foreach (APPROVING_OFFICER i in approveOfficerFound)
                //    {
                //        woNotify.NOTIFY_ID =1;
                //        woNotify.CREATED_BY = "SYSTEM";
                //        woNotify.NOTIFY_MESSAGE = "Purchase Request No." + seqId +" waiting for your approval.";
                //        woNotify.CREATED_DATE = createDt;
                //        woNotify.STAFF_ID = i.STAFF_ID;
                //        woNotify.STATUS = "A";
                //        woNotify.REF_CODE = seqId;
                //        db.Insert(woNotify);


                //    }
                //}
                
                foreach (AM_PARTSREQUEST i in requestList)
                {

                    var itemFound = db.SingleOrDefault<AM_PARTSREQUEST>(PetaPoco.Sql.Builder.Select("*").From("AM_PARTSREQUEST").Where("ID_PARTS=@refId", new { refId = i.ID_PARTS }).Where("REF_WO=@refWO", new { refWO = prDetails.REF_WO }));
                    if (itemFound != null)
                    {
                        itemFound.PR_REQUEST_ID = seqId;
                        itemFound.PR_REQUEST_QTY = i.PR_REQUEST_QTY;
                        db.Update(itemFound, new string[] { "PR_REQUEST_ID","PR_REQUEST_QTY" });
                    }


                }
                //apply processed data
                prDetails.REQUEST_ID = seqId;
                db.Insert(prDetails);

                transScope.Complete();
            }
            return prDetails;
        }

        //Company PURCHASE REQUEST
        public AM_PURCHASEREQUEST CreateOrUpdateCompanyPR(AM_PURCHASEREQUEST prDetails, List<AM_PARTSREQUEST> requestList, string mode)
        {
            using (var transScope = db.GetTransaction())
            {
                if (mode == "Create")
                {
                    SEQ seq = null;
                    var seqId = "";
                    if (prDetails.REQUEST_ID == null || prDetails.REQUEST_ID == "")
                    {
                        db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='PURCHASE_REQUEST'");

                        seq = db.SingleOrDefault<SEQ>(PetaPoco.Sql.Builder.Select("*").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "PURCHASE_REQUEST" }));
                        seqId = seq.SEQ_VAL.ToString();
                    }
                    else
                    {
                        seqId = prDetails.REQUEST_ID;
                    }

                    foreach (AM_PARTSREQUEST item in requestList)
                    {
                        item.PR_REQUEST_ID = seqId;
                        db.Insert(item);
                    }
                    //apply processed data
                    prDetails.REQUEST_ID = seqId;
                    db.Insert(prDetails);
                }
                else
                {
                    var list = db.Fetch<AM_PARTSREQUEST>(PetaPoco.Sql.Builder.Select("*").From("AM_PARTSREQUEST").Where("PR_REQUEST_ID=@PR_REQUEST_ID", new { PR_REQUEST_ID = prDetails.REQUEST_ID }));
                    foreach (var item in list)
                    {
                        var found = requestList.Find(x => x.ID_PARTS == item.ID_PARTS);
                        if (found == null)
                            db.Delete("AM_PARTSREQUEST", "AM_PARTREQUESTID", item);
                    }

                    foreach (AM_PARTSREQUEST i in requestList)
                    {
                        var itemFound = list.Find(x => x.ID_PARTS == i.ID_PARTS);
                        if (itemFound != null)
                        {
                            itemFound.REQ_DEPT = prDetails.REQ_DEPT;
                            itemFound.STORE_ID = prDetails.STORE_ID;
                            itemFound.REMARKS = prDetails.REMARKS;
                            itemFound.PR_REQUEST_QTY = i.PR_REQUEST_QTY;
                            db.Update(itemFound);
                        }
                        else
                        {
                            i.PR_REQUEST_ID = prDetails.REQUEST_ID;
                            db.Insert(i);
                        }
                    }
                    db.Update(prDetails);
                }
                transScope.Complete();
            }
            return prDetails;
        }

        public List<dynamic> SearchCompanyPR(string queryString, SearchFilter filter, int maxRows = 1000)
        {

            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchCompanyPR(db._dbType.ToString(), queryString, filter, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }


        public AM_PURCHASEREQUEST UpdatePurchaseRequest(List<AM_PARTSREQUEST> poItemList, AM_PURCHASEREQUEST poDetails)
        {
            if (poDetails != null)
            {
                if (poDetails.REQUEST_ID == "")
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }
            using (var transScope = db.GetTransaction())
            {

                db.Update(poDetails, new string[] { "REMARKS", "REQ_DEPT", "REQ_CONTACTNO", "STORE_ID"});

                db.Execute("UPDATE AM_PARTSREQUEST SET PR_REQUEST_ID=0,PR_REQUEST_QTY=NULL WHERE PR_REQUEST_ID='" + poDetails.REQUEST_ID + "'");
                foreach (AM_PARTSREQUEST i in poItemList)
                {
                    var itemFound = db.SingleOrDefault<AM_PARTSREQUEST>(PetaPoco.Sql.Builder.Select("*").From("AM_PARTSREQUEST").Where("ID_PARTS=@refId", new { refId = i.ID_PARTS }).Where("REF_WO=@refWO", new { refWO = poDetails.REF_WO }));
                    if (itemFound != null)
                    {
                        itemFound.PR_REQUEST_ID = poDetails.REQUEST_ID;
                        itemFound.PR_REQUEST_QTY = i.PR_REQUEST_QTY;
                        db.Update(itemFound, new string[] { "PR_REQUEST_ID", "PR_REQUEST_QTY" });
                    }
                }

                transScope.Complete();
            }

            return poDetails;
        }

        public dynamic ReadPurchaseRequest(string reqID)
        {
            dynamic ret = null;

            var woFound = db.SingleOrDefault<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieReadPurchaseRequest(db._dbType.ToString(), reqID)));

            if (woFound != null)
            {
                ret = woFound;
            }
            else
            {
                throw new Exception("Purchase Request not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public List<dynamic> ReadPurchaseRequestItems(string queryString, int maxRows = 1000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieReadPurchaseReqItems(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public int UpdatePurchaseReqStatus(AM_PURCHASEREQUEST prDetails)
        {
           
            using (var transScope = db.GetTransaction())
            {


                //apply processed data
                db.Execute("UPDATE AM_NOTIFY_MESSAGES SET STATUS='D' WHERE REF_CODE='" + prDetails.REQUEST_ID + "' AND NOTIFY_ID= 3");
                if(prDetails.STATUS == "APPROVED")
                {
                    db.Update(prDetails, new string[] { "STATUS","APPROVED_BY", "APPROVED_DATE" });
                   
                }
                else if (prDetails.STATUS == "CANCELLED")
                {
                    db.Update(prDetails, new string[] { "STATUS", "CANCELLED_BY", "CANCELLED_DATE" , "CANCELLED_REASON"});
                }


                var reqFound = db.SingleOrDefault<AM_PURCHASEREQUEST>(PetaPoco.Sql.Builder.Select("*").From("AM_PURCHASEREQUEST").Where("REQUEST_ID=@reqId", new { reqId = prDetails.REQUEST_ID }).Where("STATUS=@cancelledStatus", new { cancelledStatus = "CANCELLED" }));
                if (reqFound != null)
                {
                    reqFound.STATUS = prDetails.STATUS;
                    reqFound.RETURNSTATUS_BY = prDetails.RETURNSTATUS_BY;
                    reqFound.RETURNSTATUS_DATE = prDetails.RETURNSTATUS_DATE;
                    reqFound.RETURNSTATUS_REASON = prDetails.RETURNSTATUS_REASON;
                    db.Update(reqFound, new string[] { "STATUS", "RETURNSTATUS_BY", "RETURNSTATUS_DATE", "RETURNSTATUS_REASON" });
                }

                transScope.Complete();


                //var productsFound = db.Fetch<AM_PURCHASEREQUEST>(PetaPoco.Sql.Builder.Append((string)new IndieReadPurchaseReq(db._dbType.ToString(), requestID, 500)));
                //if (productsFound == null)
                //{

                //}else
                //{
                //    var storeInfoId = productsFound.Find(item => item.STORE_ID > 0);
                //if(storeInfoId != null)
                //{
                //    var storeID = storeInfoId.STORE_ID;
                //    foreach (AM_PURCHASEREQUEST i in productsFound)
                //    {
                //        if (prStatus == "APPROVED")
                //        {

                //                if(i.STATUS == "APPROVED")
                //                {
                //                    var currPRQty = db.SingleOrDefault<AM_PURCHASEREQUEST>(PetaPoco.Sql.Builder.Select("*").From("AM_PURCHASEREQUEST").Where("AM_PRREQITEMID=@reqlineId", new { reqlineId = i.AM_PRREQITEMID }));
                //                        if(currPRQty != null) {
                //                            var currQty = db.SingleOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@prodId", new { prodId = i.ID_PARTS }).Where("STORE=@storeId", new { storeId = storeID }));
                //                            var prQTY = 0;
                //                            if (currQty != null)
                //                            {
                //                                var trQty = Convert.ToInt32(currQty.PR_QTY);

                //                                if (Convert.ToInt32(i.QTY) > currPRQty.QTY) {
                //                                    var qtyUpdated = Convert.ToInt32(i.QTY) - Convert.ToInt32(currPRQty.QTY);
                //                                    prQTY = trQty + qtyUpdated;

                //                                    db.Execute("UPDATE PRODUCT_STOCKONHAND SET PR_QTY=" + prQTY + " WHERE PRODUCT_CODE='" + i.ID_PARTS + "' AND STORE='" + storeID + "'");
                //                                }else if(Convert.ToInt32(i.QTY) < currPRQty.QTY) { 
                //                                    var qtyUpdated = Convert.ToInt32(currPRQty.QTY) - Convert.ToInt32(i.QTY) ;
                //                                    prQTY = trQty - qtyUpdated;

                //                                    db.Execute("UPDATE PRODUCT_STOCKONHAND SET PR_QTY=" + prQTY + " WHERE PRODUCT_CODE='" + i.ID_PARTS + "' AND STORE='" + storeID + "'");
                //                                }

                //                            }
                //                        }
                //                }else
                //                {
                //                    var currQty = db.SingleOrDefault<PRODUCT_STOCKONHAND>(PetaPoco.Sql.Builder.Select("*").From("PRODUCT_STOCKONHAND").Where("PRODUCT_CODE=@prodId", new { prodId = i.ID_PARTS }).Where("STORE=@storeId", new { storeId = storeID }));
                //                    var prQTY = 0;
                //                    if (currQty != null)
                //                    {
                //                        var trQty = Convert.ToInt32(currQty.PR_QTY);
                //                        prQTY = trQty + Convert.ToInt32(i.QTY);
                //                        db.Execute("UPDATE PRODUCT_STOCKONHAND SET PR_QTY=" + prQTY + " WHERE PRODUCT_CODE='" + i.ID_PARTS + "' AND STORE='" + storeID + "'");
                //                    }
                //                }

                //                db.Execute("UPDATE AM_PURCHASEREQUEST SET STATUS='" + prStatus + "', APPROVED_BY='"+ modifyInfo.statusBy+"', APPROVED_DATE='"+ modifyInfo.StatusDate+"' WHERE REQUEST_ID ='" + requestID + "'");
                //        }else
                //        {
                //            db.Execute("UPDATE AM_PURCHASEREQUEST SET STATUS='" + prStatus + "', CANCELLED_BY='" + modifyInfo.statusBy + "', CANCELLED_DATE='" + modifyInfo.StatusDate + "' WHERE REQUEST_ID ='" + requestID + "'");
                //        }
                //    }



                //     db.Execute("UPDATE AM_NOTIFY_MESSAGES SET STATUS='D' WHERE REF_CODE='" + requestID + "' AND NOTIFY_ID= 1");
                //}

                //}



            }

            return 0;
        }       

        public List<dynamic> SearchPurchaseRequest(string queryString, string reqAction, SearchFilter filter, int maxRows = 1000)
        {

			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPurchaseRequest(db._dbType.ToString(), queryString, reqAction,filter, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchPurchaseRequestRept(string queryString,  SearchFilter filter, int maxRows = 10000)
        {

            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPurchaseRequestRept(db._dbType.ToString(), queryString, filter, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        
        public List<dynamic> SearchPurchaseRequestApprove(string queryString, string reqAction, int maxRows = 1000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPurchaseRequestApproved(db._dbType.ToString(), queryString, reqAction, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

		public List<dynamic> SearchPRApprovedSummary(string queryString, int maxRows = 1000)
		{
			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPurchaseReq(db._dbType.ToString(), queryString, maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}

		public List<dynamic> readPurchaseReq(string queryString, int maxRows = 1000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieReadPurchaseReq(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

		public List<dynamic> SearchPurchaseRequestApproveByProdId(string queryString, string prodId, string purchaseNo, int maxRows = 1000)
		{
			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPurchaseRequestApprovedByProdId(db._dbType.ToString(), queryString, prodId, purchaseNo, maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}
        public List<dynamic> SearchPurchaseRequestItemInfo(string queryString, string prodId, string prNo, int maxRows = 1000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPurchaseRequestItemsInfo(db._dbType.ToString(), queryString, prodId, prNo, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchPurchaseRequestItemApprove(string queryString, int maxRows = 1000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPurchaseRequestItemsApproved(db._dbType.ToString(), queryString,  maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        public List<dynamic> SearchPurchaseRequestWOParts(string queryString, int maxRows = 1000)
        {

            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPRWOMaterial(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        //PURCHASE ORDER
        public AM_PURCHASEORDER CreateNewPurchaseOrder(List<AM_PURCHASEORDER_ITEMS> poItemList, AM_PURCHASEORDER poDetails)
        {

            using (var transScope = db.GetTransaction())
            {
                
                if (poDetails.PURCHASE_NO == 0)
                {
                    db.Delete("AM_PURCHASEORDER", "PURCHASE_NO", poDetails);
                    db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='PURCHASE_ORDER'");
                    var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "PURCHASE_ORDER" }));
                    //apply processed data
                    poDetails.PURCHASE_NO = seq[0].SEQ_VAL;

                    //var approveOfficerFound = db.Fetch<APPROVING_OFFICER>(PetaPoco.Sql.Builder.Append((string)new IndieSearchApproveOfficerCat(db._dbType.ToString(), "PO", 1000)));

                    //if (approveOfficerFound != null)
                    //{
                    //    AM_NOTIFY_MESSAGES woNotify = new AM_NOTIFY_MESSAGES();

                    //    foreach (APPROVING_OFFICER i in approveOfficerFound)
                    //    {
                    //        woNotify.NOTIFY_ID = 2;
                    //        woNotify.CREATED_BY = "SYSTEM";
                    //        woNotify.NOTIFY_MESSAGE = "Purchase Order No." + poDetails.PURCHASE_NO + " waiting for your approval.";
                    //        woNotify.CREATED_DATE = poDetails.CREATED_DATE;
                    //        woNotify.STAFF_ID = i.STAFF_ID;
                    //        woNotify.STATUS = "A";
                    //        woNotify.REF_CODE = poDetails.PURCHASE_NO.ToString();
                    //        db.Insert(woNotify);


                    //    }
                    //}



                }
                db.Insert(poDetails);

                foreach (AM_PURCHASEORDER_ITEMS i in poItemList)
                {
                    i.PURCHASE_NO = poDetails.PURCHASE_NO;
                    db.Insert(i);
                }

                transScope.Complete();
            }

            return poDetails;
        }
        public AM_PURCHASEORDER UpdatePurchaseOrder(List<AM_PURCHASEORDER_ITEMS> poItemList, AM_PURCHASEORDER poDetails)
        {
            if (poDetails != null)
            {
                if (poDetails.PURCHASE_NO == 0)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }
            using (var transScope = db.GetTransaction())
            {

                db.Update(poDetails, new string[] { "SUPPLIER_ID", "BILL_ADDRESS", "BILL_EMAIL_ADDRESS", "BILL_CONTACT_NO1", "BILL_CONTACT_NO2" ,"BILL_ZIPCODE","BILL_CITY","BILL_STATE","BILL_SHIPTO"
                    , "SHIP_VIA", "TOTAL_AMOUNT","TOTAL_NOITEMS","SUPPLIER_ADDRESS1","SUPPLIER_CITY","SUPPLIER_STATE","SUPPLIER_ZIPCODE","SUPPLIER_CONTACT_NO1","SUPPLIER_CONTACT_NO2","SUPPLIER_CONTACT"
                    ,"SUPPLIER_EMAIL_ADDRESS","OTHER_INFO","SHIP_DATE","EST_SHIP_DATE","HANDLING_FEE","TRACKING_NO"
                    ,"EST_FREIGHT","FREIGHT","FREIGHT_PAIDBY","FREIGHT_CHARGETO","TAXABLE","TAX_CODE","TAX_AMOUNT"
                    ,"FREIGHT_POLINE","FREIGHT_SEPARATE_INV","INSURANCE_VALUE","PO_BILLCODE_ADDRESS","REF_WO"});

                db.Execute("DELETE AM_PURCHASEORDER_ITEMS WHERE PURCHASE_NO='"+ poDetails.PURCHASE_NO+"'");
                foreach (AM_PURCHASEORDER_ITEMS i in poItemList)
                {
                    i.PURCHASE_NO = poDetails.PURCHASE_NO;
                    db.Insert(i);
                }

                transScope.Complete();
            }

            return poDetails;
        }
        public AM_PURCHASEORDER CreateNewPO(List<AM_PURCHASEORDER_ITEMS> poItemList, AM_PURCHASEORDER poDetails)
		{

			using (var transScope = db.GetTransaction())
			{
				var purchaseNo = "";
			
				
				if (poDetails.PURCHASE_NO == 0)
				{
					db.Delete("AM_PURCHASEORDER", "PURCHASE_NO", poDetails);
					db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='PURCHASE_ORDER'");
					var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "PURCHASE_ORDER" }));
					//apply processed data
					poDetails.PURCHASE_NO = seq[0].SEQ_VAL;

                    var approveOfficerFound = db.Fetch<APPROVING_OFFICER>(PetaPoco.Sql.Builder.Append((string)new IndieSearchApproveOfficerCat(db._dbType.ToString(), "PO", 1000)));

                    //if (approveOfficerFound != null)
                    //{
                    //    AM_NOTIFY_MESSAGES woNotify = new AM_NOTIFY_MESSAGES();

                    //    foreach (APPROVING_OFFICER i in approveOfficerFound)
                    //    {
                    //        woNotify.NOTIFY_ID = 2;
                    //        woNotify.CREATED_BY = "SYSTEM";
                    //        woNotify.NOTIFY_MESSAGE = "Purchase Order No." + poDetails.PURCHASE_NO + " waiting for your approval.";
                    //        woNotify.CREATED_DATE = poDetails.CREATED_DATE;
                    //        woNotify.STAFF_ID = i.STAFF_ID;
                    //        woNotify.STATUS = "A";
                    //        woNotify.REF_CODE = poDetails.PURCHASE_NO.ToString();
                    //        db.Insert(woNotify);


                    //    }
                    //}



                }
				else
				{
					db.Delete("AM_PURCHASEORDER", "PURCHASE_NO", poDetails);
					purchaseNo = poDetails.PURCHASE_NO.ToString();
				}
				

				db.Insert(poDetails);

				//foreach (AM_PURCHASEREQUEST_ITEMS i in poItemList)
				//{
					
				//	var productsFound = db.Fetch<AM_PURCHASEREQUEST>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPurchaseRequestApprovedByProdId(db._dbType.ToString(), "", i.ID_PARTS, purchaseNo, 500)));
				//	if (productsFound.Count <= 0)
				//	{
				//		productsFound = db.Fetch<AM_PURCHASEREQUEST>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPurchaseRequestApprovedByProdId(db._dbType.ToString(), "", i.ID_PARTS, "", 500)));
				//	}
				//	var prItems = productsFound.Count;
				//	//gasparint purchaseTotalQty = i.PURCHASE_QTY;
				//	int curr = 1;
				//	foreach (AM_PURCHASEREQUEST y in productsFound)
				//	{
    //                    int reqQty = 0;//gaspar (int)y.QTY;
				//		//gasparvar currPurchaseQty = purchaseTotalQty;
				//		//gasparpurchaseTotalQty = purchaseTotalQty - reqQty;
				//		if (productsFound.Count() == 1)
				//		{
				//			db.Execute("UPDATE AM_PURCHASEREQUEST SET PURCHASE_NO =" + poDetails.PURCHASE_NO + ", PURCHASE_UNITPRICE =" + i.PO_PRICE + ", PURCHASE_QTY =" + currPurchaseQty + ", SUPPLIER_ID =" + poDetails.SUPPLIER_ID + ", PO_STATUS ='" + poDetails.STATUS + "' WHERE ID_PARTS='" + i.ID_PARTS + "' AND STATUS = 'APPROVED' AND AM_PRREQITEMID =" + y.AM_PRREQITEMID);
				//		}
				//		else
				//		{
				//			if (productsFound.Count() == curr)
				//			{
				//				if (currPurchaseQty < 0) {
				//					currPurchaseQty = 0;
				//						}
				//				db.Execute("UPDATE AM_PURCHASEREQUEST SET PURCHASE_NO =" + poDetails.PURCHASE_NO + ", PURCHASE_UNITPRICE =" + i.PO_PRICE + ", PURCHASE_QTY =" + currPurchaseQty + ", SUPPLIER_ID =" + poDetails.SUPPLIER_ID + ", PO_STATUS ='" + poDetails.STATUS + "' WHERE ID_PARTS='" + i.ID_PARTS + "' AND STATUS = 'APPROVED' AND AM_PRREQITEMID =" + y.AM_PRREQITEMID);
				//			}
				//			else
				//			{
				//				if (purchaseTotalQty > 0)
				//				{
				//					db.Execute("UPDATE AM_PURCHASEREQUEST SET PURCHASE_NO =" + poDetails.PURCHASE_NO + ", PURCHASE_UNITPRICE =" + i.PO_PRICE + ", PURCHASE_QTY =" + reqQty + ", SUPPLIER_ID =" + poDetails.SUPPLIER_ID + ", PO_STATUS ='" + poDetails.STATUS +  "' WHERE ID_PARTS='" + i.ID_PARTS + "' AND STATUS = 'APPROVED' AND AM_PRREQITEMID =" + y.AM_PRREQITEMID);
				//				}
				//				else
				//				{
				//					if (currPurchaseQty > 0)
				//					{
				//						db.Execute("UPDATE AM_PURCHASEREQUEST SET PURCHASE_NO =" + poDetails.PURCHASE_NO + ", PURCHASE_UNITPRICE =" + i.PO_PRICE + ", PURCHASE_QTY =" + currPurchaseQty + ", SUPPLIER_ID =" + poDetails.SUPPLIER_ID + ", PO_STATUS ='" + poDetails.STATUS +  "' WHERE ID_PARTS='" + i.ID_PARTS + "' AND STATUS = 'APPROVED' AND AM_PRREQITEMID =" + y.AM_PRREQITEMID);
				//					}
				//					else
				//					{
				//						db.Execute("UPDATE AM_PURCHASEREQUEST SET PURCHASE_NO =" + poDetails.PURCHASE_NO + ", PURCHASE_UNITPRICE =" + i.PO_PRICE + ", PURCHASE_QTY =" + 0 + ", SUPPLIER_ID =" + poDetails.SUPPLIER_ID + ", PO_STATUS ='" + poDetails.STATUS +  "' WHERE ID_PARTS='" + i.ID_PARTS + "' AND STATUS = 'APPROVED' AND AM_PRREQITEMID =" + y.AM_PRREQITEMID);
				//					}
				//				}
				//			}

				//		}



						
				//	}



				//}

				



				transScope.Complete();
			}

			return poDetails;
		}

        public AM_PURCHASEORDER UpdateStatusPurchaseOrder(AM_PURCHASEORDER poDetails)
        {

            using (var transScope = db.GetTransaction())
            {   

                db.Execute("UPDATE AM_NOTIFY_MESSAGES SET STATUS='D' WHERE REF_CODE='" + poDetails.PURCHASE_NO + "' AND NOTIFY_ID= 4");
                if (poDetails.STATUS == "APPROVED")
                {
                    db.Update(poDetails, new string[] { "STATUS", "APPROVED_BY", "APPROVED_DATE" });
                }
                else if (poDetails.STATUS == "CANCELLED")
                {
                    db.Update(poDetails, new string[] { "STATUS", "CANCELLED_BY", "CANCELLED_DATE" });
                }

                transScope.Complete();
            }

            return poDetails;
        }

        public AM_PURCHASEORDER ApprovePO(List<AM_PURCHASEORDERLIST> poItemList, AM_PURCHASEORDER poDetails)
		{

			using (var transScope = db.GetTransaction())
			{
				db.Delete("AM_PURCHASEORDER", "PURCHASE_NO", poDetails);
				poDetails.STATUS = "APPROVED";


				db.Insert(poDetails);
                db.Execute("UPDATE AM_NOTIFY_MESSAGES SET STATUS='D' WHERE REF_CODE='" + poDetails.PURCHASE_NO + "' AND NOTIFY_ID= 2");
                foreach (AM_PURCHASEORDERLIST i in poItemList)
				{
					var productsFound = db.Fetch<AM_PURCHASEREQUEST>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPurchaseRequestApprovedByProdId(db._dbType.ToString(), "", i.ID_PARTS, poDetails.PURCHASE_NO.ToString(), 5000)));
					var prItems = productsFound.Count;
					int purchaseTotalQty = i.PURCHASE_QTY;
					int curr = 1;
					foreach (AM_PURCHASEREQUEST y in productsFound)
					{
                        int reqQty = 0;//gaspar (int)y.QTY;
						var currPurchaseQty = purchaseTotalQty;
						//gasparpurchaseTotalQty = purchaseTotalQty - reqQty;
						if (productsFound.Count() == 1)
						{
							db.Execute("UPDATE AM_PURCHASEREQUEST SET PURCHASE_NO =" + poDetails.PURCHASE_NO + ", PURCHASE_UNITPRICE =" + i.PO_PRICE + ", PURCHASE_QTY =" + currPurchaseQty + ", SUPPLIER_ID =" + poDetails.SUPPLIER_ID + ", PO_STATUS ='" + poDetails.STATUS + "' WHERE ID_PARTS='" + i.ID_PARTS + "' AND STATUS = 'APPROVED' AND AM_PRREQITEMID =" + y.AM_PRREQITEMID);
						}
						else
						{
							if (productsFound.Count() == curr)
							{
								if (currPurchaseQty < 0)
								{
									currPurchaseQty = 0;
								}
								db.Execute("UPDATE AM_PURCHASEREQUEST SET PURCHASE_NO =" + poDetails.PURCHASE_NO + ", PURCHASE_UNITPRICE =" + i.PO_PRICE + ", PURCHASE_QTY =" + currPurchaseQty + ", SUPPLIER_ID =" + poDetails.SUPPLIER_ID + ", PO_STATUS ='" + poDetails.STATUS + "' WHERE ID_PARTS='" + i.ID_PARTS + "' AND STATUS = 'APPROVED' AND AM_PRREQITEMID =" + y.AM_PRREQITEMID);
							}
							else
							{
								if (purchaseTotalQty > 0)
								{
									db.Execute("UPDATE AM_PURCHASEREQUEST SET PURCHASE_NO =" + poDetails.PURCHASE_NO + ", PURCHASE_UNITPRICE =" + i.PO_PRICE + ", PURCHASE_QTY =" + reqQty + ", SUPPLIER_ID =" + poDetails.SUPPLIER_ID + ", PO_STATUS ='" + poDetails.STATUS + "' WHERE ID_PARTS='" + i.ID_PARTS + "' AND STATUS = 'APPROVED' AND AM_PRREQITEMID =" + y.AM_PRREQITEMID);
								}
								else
								{
									if (currPurchaseQty > 0)
									{
										db.Execute("UPDATE AM_PURCHASEREQUEST SET PURCHASE_NO =" + poDetails.PURCHASE_NO + ", PURCHASE_UNITPRICE =" + i.PO_PRICE + ", PURCHASE_QTY =" + currPurchaseQty + ", SUPPLIER_ID =" + poDetails.SUPPLIER_ID + ", PO_STATUS ='" + poDetails.STATUS + "' WHERE ID_PARTS='" + i.ID_PARTS + "' AND STATUS = 'APPROVED' AND AM_PRREQITEMID =" + y.AM_PRREQITEMID);
									}
									else
									{
										db.Execute("UPDATE AM_PURCHASEREQUEST SET PURCHASE_NO =" + poDetails.PURCHASE_NO + ", PURCHASE_UNITPRICE =" + i.PO_PRICE + ", PURCHASE_QTY =" + 0 + ", SUPPLIER_ID =" + poDetails.SUPPLIER_ID + ", PO_STATUS ='" + poDetails.STATUS + "' WHERE ID_PARTS='" + i.ID_PARTS + "' AND STATUS = 'APPROVED' AND AM_PRREQITEMID =" + y.AM_PRREQITEMID);
									}
								}
							}

						}
					}
				}





				transScope.Complete();
			}

			return poDetails;
		}
		public List<dynamic> SearchPurchaseOrder(string queryString, string reqAction, int maxRows = 1000)
		{

			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPurchaseOrder(db._dbType.ToString(), queryString, reqAction, maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}
		public AM_PURCHASEORDER ReadPO(string reqID)
		{
			AM_PURCHASEORDER ret = null;

			var woFound = db.SingleOrDefault<AM_PURCHASEORDER>(PetaPoco.Sql.Builder.Select("*").From("AM_PURCHASEORDER").Where("PURCHASE_NO=@reqId", new { reqId = reqID }));

			if (woFound != null)
			{
				ret = woFound;
			}
			else
			{
				throw new Exception("Purchase Order not found.") { Source = this.GetType().Name };
			}
			return ret;
		}
		public List<dynamic> readPOItems(string queryString, int maxRows = 1000)
		{
			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieReadPurchaseOrderItems(db._dbType.ToString(), queryString, maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}
		public List<dynamic> SearchPOItemsApprove(string queryString, string reqAction, int maxRows = 1000)
		{
			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPOItemsApproved(db._dbType.ToString(), queryString, reqAction, maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}
		public List<dynamic> readPurchaseOrderItems(string queryString, int maxRows = 1000)
		{
			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieReadPurchaseOrderItemsBatch(db._dbType.ToString(), queryString, maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}


		//PARTS SUPPLIER DETAILS
		public AM_PARTSUPPLIERDETAILS CreateNewPartSP(AM_PARTSUPPLIERDETAILS partSPDetails)
        {

            using (var transScope = db.GetTransaction())
            {

                //apply processed data
                db.Insert(partSPDetails);

                transScope.Complete();
            }

            return partSPDetails;
        }
        public AM_PARTSUPPLIERDETAILS ReadPartSP(string reqID)
        {
            AM_PARTSUPPLIERDETAILS ret = null;

            var woFound = db.SingleOrDefault<AM_PARTSUPPLIERDETAILS>(PetaPoco.Sql.Builder.Select("*").From("AM_PARTSUPPLIERDETAILS").Where("PARTSP_ID=@reqId", new { reqId = reqID }));

            if (woFound != null)
            {
                ret = woFound;
            }
            else
            {
                throw new Exception("Work Order not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdatePartSP(AM_PARTSUPPLIERDETAILS partSPDetails)
        {
            //validate data
            if (partSPDetails != null)
            {
                if (partSPDetails.PARTSP_ID == 0)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(partSPDetails, new string[] { "SUPPLIER_ID", "CUSTOMER_NO", "PREFERENCE", "PART_NUMBER", "PART_DESCRIPTION"
                    , "EXCHANGEABLE", "RETURN_ADDRESS","REMARKS","RETURN_PARTDESC","RETURN_NODAYS","CREDIT","UNIT_QTY","QTY_PERUNIT","MIN_ORDERQTY","DELIVERY_LOADTIME","STATUS"});

                transScope.Complete();
            }

            return 0;
        }
       
        public List<dynamic> SearchPartSP(string queryString, string partId, int maxRows = 1000)
        {
            var partSupplierFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPartSP(db._dbType.ToString(), queryString, partId, maxRows)));

            if (partSupplierFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return partSupplierFound;
        }

        //ASSET Information
        public AM_ASSET_DETAILS CreateNewAsset(AM_ASSET_DETAILS assetDetails)
        {

            using (var transScope = db.GetTransaction())
            {
                db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='AIMS_ASSETNO'");
                var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "AIMS_ASSETNO" }));
                var nextseq = seq[0].SEQ_VAL.ToString();
                //productDetails.PRODUCT_ID = Guid.NewGuid().ToString("N");
                //apply processed data
                var assetFound = db.SingleOrDefault<AM_ASSET_DETAILS>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_DETAILS").Where("ASSET_NO=@assetId", new { assetId = nextseq }));

                if (assetFound != null)
                {

                    var assetDesc = assetFound.DESCRIPTION;
                    var assetID = assetDetails.ASSET_NO;

                    throw new Exception("ASSETNOEXIST " + assetDesc) { Source = this.GetType().Name };

                }
                assetDetails.ASSET_NO = nextseq;

                if (assetDetails.PO_DATE != null)
                {
                    var indexRev = assetDetails.PO_DATE.IndexOf("00:00:00");
                    if (indexRev > 0)
                    {
                        assetDetails.PO_DATE = assetDetails.PO_DATE.Remove(indexRev, 8);
                    }
                    //assetDetails.PO_DATE.Remove("00:00:00");
                }


                db.Insert(assetDetails);
                // insert to system transaction
                DateTime? cToday = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");

                SYSTEM_TRANSACTIONDETAILS systemTrans = new SYSTEM_TRANSACTIONDETAILS();
                var costcenterdescriptionFound = db.SingleOrDefault<COSTCENTER_DETAILS>(PetaPoco.Sql.Builder.Select("Description").From("COSTCENTER_DETAILS").Where("CODEID=@reqId", new { reqId = assetDetails.COST_CENTER }));

                var costcennterName = "";
                if (costcenterdescriptionFound != null)
                {
                    costcennterName = costcenterdescriptionFound.DESCRIPTION;
                }
                var assetModel = "";
                if (assetDetails.MODEL_NO == "" || assetDetails.MODEL_NO == null)
                {
                    assetModel = assetDetails.MODEL_NAME;
                }
                else
                {
                    assetModel = assetDetails.MODEL_NO;
                }
                systemTrans.REFCODE = assetDetails.ASSET_NO;
                systemTrans.REFTRANSID = "NEWASSET";
                systemTrans.REFDESCRIPTION = assetDetails.DESCRIPTION;
                systemTrans.REFCOSTCENTERCODE = assetDetails.COST_CENTER;
                systemTrans.REFCOST = assetDetails.PURCHASE_COST;
                systemTrans.REFCOSTCENTERDESCRIPTION = costcennterName;
                systemTrans.REFMODEL = assetModel;
                systemTrans.REFBATCHSERIALNO = assetDetails.SERIAL_NO;
                systemTrans.REFINVOICENO = assetDetails.PURCHASE_NO;
                systemTrans.REFQTY = 1;
                systemTrans.REFUOM = "Piece";
                systemTrans.REASONTYPE = "Purchased";
                systemTrans.REFTYPE = "Asset";
                systemTrans.COSTCENTERMESSAGE = "This new Asset/Equipment with serial no.(" + assetDetails.SERIAL_NO + ") was added on Asset Onhand. Purchased Date: " + String.Format("{0:dd/MM/yyyy}", assetDetails.PURCHASE_DATE) + ".";
                systemTrans.MESSAGE = "This new Asset/Equipment with serial no.(" + assetDetails.SERIAL_NO + ") was added on Asset Onhand. Purchased Date: " + String.Format("{0:dd/MM/yyyy}", assetDetails.PURCHASE_DATE) + ".";
                systemTrans.STATUS = "Y";
                systemTrans.CREATEDBY = assetDetails.CREATED_BY;
                systemTrans.CREATEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                db.Insert(systemTrans);




                //if (assetDetails.CONDITION_DATE != null && (assetDetails.CONDITION_DATE).ToString() != "01/01/1900 00:00:00")
                //{
                    db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='AIMS_ORDERNO'");
                    var Woseq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "AIMS_ORDERNO" }));

                    AM_WORK_ORDERS wOrder = new AM_WORK_ORDERS();
                    wOrder.REQ_NO = Convert.ToString(Woseq[0].SEQ_VAL);
                    wOrder.ASSET_NO = nextseq;
                    wOrder.REQ_BY = "Initial Acceptance";
                    wOrder.REQUESTER_CONTACT_NO = "N/A";
                    wOrder.ASSIGN_TYPE = "PERSON";
                    wOrder.REQ_DATE = assetDetails.CREATED_DATE;
                    wOrder.CREATED_BY = assetDetails.CREATED_BY;
                    wOrder.CREATED_DATE = assetDetails.CREATED_DATE;
                    wOrder.REQ_TYPE = "IA";
                    wOrder.WO_STATUS = "NE";
                    wOrder.JOB_TYPE = "034";
                    wOrder.ASSET_COSTCENTER = assetDetails.COST_CENTER;
                    wOrder.PROBLEM = "For Initial Acceptance";
                    wOrder.ASSET_LOCATION = assetDetails.LOCATION;

                    db.Insert(wOrder);
             //   }
               


                transScope.Complete();
            }

            
            return assetDetails;
        }

        public List<AM_ASSET_DETAILS_CLONE> CreateAssetClone(AM_ASSET_DETAILS assetDetails, List<AM_ASSET_DETAILS_CLONE> cloneList, List<AM_ASSET_WARRANTY> warrantyList, string assetNo)
        {
            List<AM_ASSET_DETAILS_CLONE> list = new List<AM_ASSET_DETAILS_CLONE>();
            //AM_ASSET_DETAILS itemFound = new AM_ASSET_DETAILS();


            var nextseqClone = "";
            var nextseq = "";
            var newAssetNo = "";
            if (assetDetails.ASSET_NO == null || assetDetails.ASSET_NO == "")
            {
                foreach (AM_ASSET_DETAILS_CLONE i in cloneList)
                {
                    using (var transScope = db.GetTransaction())
                    {
                        if(assetDetails.CLONE_ID == null || assetDetails.CLONE_ID == "")
                        {
                            if (nextseqClone == null || nextseqClone == "")
                            {
                                db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='AIMS_CLONEID'");
                                var seqClone = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "AIMS_CLONEID" }));
                                nextseqClone = seqClone[0].SEQ_VAL.ToString();
                            }
                        }
                        else
                        {
                            nextseqClone = assetDetails.CLONE_ID;
                        }
                        

                        db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='AIMS_ASSETNO'");
                        var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "AIMS_ASSETNO" }));
                        nextseq = seq[0].SEQ_VAL.ToString();
                        newAssetNo = nextseq;
                        assetDetails.ASSET_NO = nextseq;
                        i.ASSET_NO = nextseq;
                        i.CLONE_ID = nextseqClone;
                        assetDetails.CLONE_ID = nextseqClone;
                        assetDetails.BUILDING = i.BUILDING;
                        assetDetails.SERIAL_NO = i.SERIAL_NO;
                        assetDetails.COST_CENTER = i.COST_CENTER;
                        assetDetails.LOCATION_DATE = i.LOCATION_DATE;
                        assetDetails.CONDITION_DATE = i.CONDITION_DATE;
                        assetDetails.LOCATION = i.LOCATION;
                        assetDetails.CREATED_BY = i.CREATED_BY;
                        assetDetails.CREATED_DATE = i.CREATED_DATE;
                        assetDetails.RESPONSIBLE_CENTER = i.RESPONSIBLE_CENTER;
                        db.Insert(assetDetails);

                        // insert to system transaction
                        DateTime? cToday = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");

                        SYSTEM_TRANSACTIONDETAILS systemTrans = new SYSTEM_TRANSACTIONDETAILS();
                        var costcenterdescriptionFound = db.SingleOrDefault<COSTCENTER_DETAILS>(PetaPoco.Sql.Builder.Select("Description").From("COSTCENTER_DETAILS").Where("CODEID=@reqId", new { reqId = assetDetails.COST_CENTER }));

                        var costcennterName = "";
                        if (costcenterdescriptionFound != null)
                        {
                            costcennterName = costcenterdescriptionFound.DESCRIPTION;
                        }
                        var assetModel = "";
                        if (assetDetails.MODEL_NO == "" || assetDetails.MODEL_NO == null)
                        {
                            assetModel = assetDetails.MODEL_NAME;
                        }
                        else
                        {
                            assetModel = assetDetails.MODEL_NO;
                        }
                        systemTrans.REFCODE = assetDetails.ASSET_NO;
                        systemTrans.REFTRANSID = "NEWASSET";
                        systemTrans.REFDESCRIPTION = assetDetails.DESCRIPTION;
                        systemTrans.REFCOSTCENTERCODE = assetDetails.COST_CENTER;
                        systemTrans.REFCOST = assetDetails.PURCHASE_COST;
                        systemTrans.REFCOSTCENTERDESCRIPTION = costcennterName;
                        systemTrans.REFMODEL = assetModel;
                        systemTrans.REFBATCHSERIALNO = assetDetails.SERIAL_NO;
                        systemTrans.REFINVOICENO = assetDetails.PURCHASE_NO;
                        systemTrans.REFQTY = 1;
                        systemTrans.REFUOM = "Piece";
                        systemTrans.REASONTYPE = "Purchased";
                        systemTrans.REFTYPE = "Asset";
                        systemTrans.COSTCENTERMESSAGE = "This new Asset/Equipment with serial no.(" + assetDetails.SERIAL_NO + ") was added on Asset Onhand. Purchased Date: " + String.Format("{0:dd/MM/yyyy}", assetDetails.PURCHASE_DATE) + ".";
                        systemTrans.MESSAGE = "This new Asset/Equipment with serial no.(" + assetDetails.SERIAL_NO + ") was added on Asset Onhand. Purchased Date: " + String.Format("{0:dd/MM/yyyy}", assetDetails.PURCHASE_DATE) + ".";
                        systemTrans.STATUS = "Y";
                        systemTrans.CREATEDBY = assetDetails.CREATED_BY;
                        systemTrans.CREATEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                        db.Insert(systemTrans);


                        //if (i.CONDITION_DATE != null && (i.CONDITION_DATE).ToString() != "01/01/1900 00:00:00")
                        //{
                            db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='AIMS_ORDERNO'");
                            var Woseq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "AIMS_ORDERNO" }));

                            AM_WORK_ORDERS wOrder = new AM_WORK_ORDERS();
                            wOrder.REQ_NO = Convert.ToString(Woseq[0].SEQ_VAL);
                            wOrder.ASSET_NO = nextseq;
                            wOrder.REQ_BY = "Initial Acceptance";
                            wOrder.REQ_DATE = i.CREATED_DATE;
                            wOrder.CREATED_BY = i.CREATED_BY;
                            wOrder.CREATED_DATE = i.CREATED_DATE;
                            wOrder.REQ_TYPE = "IA";
                            wOrder.WO_STATUS = "NE";
                            wOrder.JOB_TYPE = "034";
                            wOrder.PROBLEM = "For Initial Acceptance";
                            wOrder.ASSET_LOCATION = i.LOCATION;

                            db.Insert(wOrder);
                       // }
                        if(warrantyList.Count() > 0)
                        {
                            foreach (AM_ASSET_WARRANTY o in warrantyList)
                            {
                                o.ASSET_NO = newAssetNo;
                                db.Insert(o);
                            }
                        }


                        //apply processed data
                        transScope.Complete();
                    }
                }
            }
            else
            {

                foreach (AM_ASSET_DETAILS_CLONE i in cloneList)
                {

                    using (var transScope = db.GetTransaction())
                    {
                        //if (itemFound.DESCRIPTION == null)
                        //{
                        //    itemFound = db.SingleOrDefault<AM_ASSET_DETAILS>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_DETAILS").Where("ASSET_NO=@refAsset", new { refAsset = assetNo }));
                        //}

                        if (assetDetails.CLONE_ID == null || assetDetails.CLONE_ID == "")
                        {
                            if (nextseqClone == null || nextseqClone == "")
                            {
                                db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='AIMS_CLONEID'");
                                var seqClone = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "AIMS_CLONEID" }));
                                nextseqClone = seqClone[0].SEQ_VAL.ToString();

                                if (assetNo != null && assetNo != "")
                                {
                                    var itemAssetFound = db.SingleOrDefault<AM_ASSET_DETAILS>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_DETAILS").Where("ASSET_NO=@refAsset", new { refAsset = assetNo }));
                                    if(itemAssetFound != null)
                                    {
                                        if(itemAssetFound.CLONE_ID == null || itemAssetFound.CLONE_ID == "")
                                        {
                                            itemAssetFound.CLONE_ID = nextseqClone;
                                            db.Update(itemAssetFound, new string[] { "CLONE_ID" });
                                        }
                                        
                                    }
                                }


                            }
                        }
                        else
                        {
                            nextseqClone = assetDetails.CLONE_ID;
                        }

                        

                        if (assetDetails.DESCRIPTION != null)
                        {
                            db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='AIMS_ASSETNO'");
                            var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "AIMS_ASSETNO" }));
                            nextseq = seq[0].SEQ_VAL.ToString();
                            newAssetNo = nextseq;
                            assetDetails.ASSET_NO = nextseq;
                            i.ASSET_NO = nextseq;
                            i.CLONE_ID = nextseqClone;
                            assetDetails.CLONE_ID = nextseqClone;
                            assetDetails.BUILDING = i.BUILDING;
                            assetDetails.SERIAL_NO = i.SERIAL_NO;
                            assetDetails.COST_CENTER = i.COST_CENTER;
                            assetDetails.LOCATION_DATE = i.LOCATION_DATE;
                            assetDetails.CONDITION_DATE = i.CONDITION_DATE;
                            assetDetails.LOCATION = i.LOCATION;
                            assetDetails.CREATED_BY = i.CREATED_BY;
                            assetDetails.CREATED_DATE = i.CREATED_DATE;
                            assetDetails.RESPONSIBLE_CENTER = i.RESPONSIBLE_CENTER;
                            db.Insert(assetDetails);

                            // insert to system transaction
                            DateTime? cToday = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");

                            SYSTEM_TRANSACTIONDETAILS systemTrans = new SYSTEM_TRANSACTIONDETAILS();
                            var costcenterdescriptionFound = db.SingleOrDefault<COSTCENTER_DETAILS>(PetaPoco.Sql.Builder.Select("Description").From("COSTCENTER_DETAILS").Where("CODEID=@reqId", new { reqId = assetDetails.COST_CENTER }));

                            var costcennterName = "";
                            if (costcenterdescriptionFound != null)
                            {
                                costcennterName = costcenterdescriptionFound.DESCRIPTION;
                            }
                            var assetModel = "";
                            if (assetDetails.MODEL_NO == "" || assetDetails.MODEL_NO == null)
                            {
                                assetModel = assetDetails.MODEL_NAME;
                            }
                            else
                            {
                                assetModel = assetDetails.MODEL_NO;
                            }
                            systemTrans.REFCODE = assetDetails.ASSET_NO;
                            systemTrans.REFDESCRIPTION = assetDetails.DESCRIPTION;
                            systemTrans.REFCOSTCENTERCODE = assetDetails.COST_CENTER;
                            systemTrans.REFCOST = assetDetails.PURCHASE_COST;
                            systemTrans.REFCOSTCENTERDESCRIPTION = costcennterName;
                            systemTrans.REFMODEL = assetModel;
                            systemTrans.REFBATCHSERIALNO = assetDetails.SERIAL_NO;
                            systemTrans.REFINVOICENO = assetDetails.PURCHASE_NO;
                            systemTrans.REFQTY = 1;
                            systemTrans.REFUOM = "Piece";
                            systemTrans.REASONTYPE = "Purchased";
                            systemTrans.REFTYPE = "Asset";
                            systemTrans.COSTCENTERMESSAGE = "This new Asset/Equipment with serial no.(" + assetDetails.SERIAL_NO + ") was added on Asset Onhand. Purchased Date: " + String.Format("{0:dd/MM/yyyy}", assetDetails.PURCHASE_DATE) + ".";
                            systemTrans.MESSAGE = "This new Asset/Equipment with serial no.(" + assetDetails.SERIAL_NO + ") was added on Asset Onhand. Purchased Date: " + String.Format("{0:dd/MM/yyyy}", assetDetails.PURCHASE_DATE) + ".";
                            systemTrans.STATUS = "Y";
                            systemTrans.CREATEDBY = assetDetails.CREATED_BY;
                            systemTrans.CREATEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                            db.Insert(systemTrans);


                            //if (i.CONDITION_DATE != null && (i.CONDITION_DATE).ToString() != "01/01/1900 00:00:00")
                            //{
                            db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='AIMS_ORDERNO'");
                            var Woseq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "AIMS_ORDERNO" }));

                            AM_WORK_ORDERS wOrder = new AM_WORK_ORDERS();
                            wOrder.REQ_NO = Convert.ToString(Woseq[0].SEQ_VAL);
                            wOrder.ASSET_NO = nextseq;
                            wOrder.REQ_BY = "Initial Acceptance";
                            wOrder.REQ_DATE = i.CREATED_DATE;
                            wOrder.CREATED_BY = i.CREATED_BY;
                            wOrder.CREATED_DATE = i.CREATED_DATE;
                            wOrder.REQ_TYPE = "IA";
                            wOrder.WO_STATUS = "NE";
                            wOrder.JOB_TYPE = "034";
                            wOrder.PROBLEM = "For Initial Acceptance";
                            wOrder.ASSET_LOCATION = i.LOCATION;
                            db.Insert(wOrder);
                          //  }

                            var itemPMFound = db.SingleOrDefault<AM_ASSET_PMSCHEDULES>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_PMSCHEDULES").Where("ASSET_NO=@refAsset", new { refAsset = assetNo }));
                            if (itemPMFound != null)
                            {

                                db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='AIMS_PMSCHENO'");
                                var seqPm = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "AIMS_PMSCHENO" }));
                                itemPMFound.AM_ASSET_PMSCHEDULE_ID = Convert.ToString(seqPm[0].SEQ_VAL);
                                itemPMFound.ASSET_NO = newAssetNo;
                                //apply processed data
                                db.Insert(itemPMFound);

                            }

                            var wrListFound = db.Fetch<AM_ASSET_WARRANTY>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetWarranty(db._dbType.ToString(), assetNo, 1000)));
                            if (wrListFound != null)
                            {
                                foreach (AM_ASSET_WARRANTY o in wrListFound)
                                {
                                    o.ASSET_NO = newAssetNo;
                                    db.Insert(o);
                                }
                            }

                        }
                        //apply processed data
                        transScope.Complete();


                    }
                }
               
            }
            list = cloneList;

            return list;
        }

        public List<AM_ASSET_DETAILS_CLONE> UpdateAssetClone(List<AM_ASSET_DETAILS_CLONE> cloneList)
        {
            AM_WORK_ORDERS wOrder = new AM_WORK_ORDERS();

            using (var transScope = db.GetTransaction())
            {

               

                foreach (AM_ASSET_DETAILS_CLONE i in cloneList)
                {

                    var itemFound = db.SingleOrDefault<AM_ASSET_DETAILS>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_DETAILS").Where("ASSET_NO=@refAsset", new { refAsset = i.ASSET_NO }));
                    if (itemFound != null)
                    {
                        //if (i.CONDITION_DATE != null && (i.CONDITION_DATE).ToString() != "01/01/1900 00:00:00" && (itemFound.CONDITION_DATE == null || (itemFound.CONDITION_DATE).ToString() == "01/01/1900 00:00:00"))
                        //{
                            //db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='AIMS_ORDERNO'");
                            //var Woseq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "AIMS_ORDERNO" }));

                            
                            //wOrder.REQ_NO = Convert.ToString(Woseq[0].SEQ_VAL);
                            //wOrder.ASSET_NO = i.ASSET_NO;
                            //wOrder.REQ_BY = "Initial Acceptance";
                            //wOrder.REQ_DATE = i.CONDITION_DATE;
                            //wOrder.CREATED_BY = itemFound.CREATED_BY;
                            //wOrder.CREATED_DATE = itemFound.CREATED_DATE;
                            //wOrder.REQ_TYPE = "IA";
                            //wOrder.WO_STATUS = "NE";
                            //wOrder.JOB_TYPE = "034";
                            //wOrder.PROBLEM = "For Initial Acceptance";

                            //db.Insert(wOrder);
                       // }
                        

                        itemFound.BUILDING = i.BUILDING;
                        itemFound.SERIAL_NO = i.SERIAL_NO;
                        itemFound.COST_CENTER = i.COST_CENTER;
                        itemFound.LOCATION_DATE = i.LOCATION_DATE;
                        itemFound.LOCATION = i.LOCATION;
                        itemFound.CONDITION_DATE = i.CONDITION_DATE;
                        itemFound.RESPONSIBLE_CENTER = i.RESPONSIBLE_CENTER;
                        



                        db.Update(itemFound, new string[] { "BUILDING", "SERIAL_NO", "COST_CENTER","LOCATION_DATE", "LOCATION", "RESPONSIBLE_CENTER","CONDITION_DATE" });


                    }
                    
                }
                //apply processed data


                transScope.Complete();
            }
            return cloneList;
        }

        public List<dynamic> ReadAssetsByProductCode(string productCode)
        {
            List<dynamic> ret = null;

            var assetFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieReadAssetsByProductCode(db._dbType.ToString(), productCode)));

            if (assetFound != null)
            {
                ret = assetFound;
            }
            else
            {
                throw new Exception("Equipment not found.") { Source = this.GetType().Name };
            }
            return ret;
        }

        public AM_ASSET_DETAILS ReadAssetInfo(string assetID)
        {
            AM_ASSET_DETAILS ret = null;

            var assetFound = db.SingleOrDefault<AM_ASSET_DETAILS>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_DETAILS").Where("ASSET_NO=@assetId", new { assetId = assetID }));

            if (assetFound != null)
            {
                ret = assetFound;
            }
            else
            {
				if (assetID == "WOWORKSHEET")
				{
					ret = null;
				}
				else
				{
					throw new Exception("Equipment not found.") { Source = this.GetType().Name };
				}

				
            }
            return ret;
        }
        public dynamic ReadAssetInfoV(string assetID)
        {

            var assetFound = db.SingleOrDefault<dynamic>(PetaPoco.Sql.Builder.Select("*").From("ASSETINFO_V").Where("ASSET_NO=@assetId", new { assetId = assetID }));


            if (assetFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return assetFound;
        }
        public dynamic ReadAssetInfoVWO(string assetID)
        {

            var assetFound = db.SingleOrDefault<dynamic>(PetaPoco.Sql.Builder.Select("*").From("ASSETINFO_V").Where("ASSET_NO=@assetId", new { assetId = assetID }));

            //if (assetFound == null)
            //{
            //    throw new Exception("No match found.") { Source = this.GetType().Name };
            //}
            return assetFound;
        }
        public AM_ASSET_DETAILS UpdateAssetInfo(AM_ASSET_DETAILS assetDetails, AM_ASSET_TRANSACTION assetTransaction, string modifiedBy)
        {
            AM_ASSET_NOTES assetNotes = new AM_ASSET_NOTES();

            //validate data
            if (assetDetails != null)
            {
                if (assetDetails.ASSET_NO == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {
                //preprocess data
                //Update Notes
                var assetFound = db.SingleOrDefault<AM_ASSET_DETAILS>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_DETAILS").Where("ASSET_NO=@reqId", new { reqId = assetDetails.ASSET_NO }));

                //Insert Asset Transaction
                if (assetFound.COST_CENTER != assetDetails.COST_CENTER)
                {
                    assetDetails.STATUS ="I";
                    var assetTransactionFound = db.SingleOrDefault<AM_ASSET_TRANSACTION>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_TRANSACTION").Where("TRANSID=@reqId", new { reqId = assetTransaction.TRANSID }).Where("ASSET_NO=@reqId", new { reqId = assetDetails.ASSET_NO }));

                    if (assetTransactionFound == null)
                    {
                        assetTransaction.CREATEDBY = modifiedBy;
                        assetTransaction.CREATEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                        assetTransaction.STATUS = "R";
                        db.Insert(assetTransaction);

                        DateTime? cToday = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");

                        //if (assetTransaction.COSTCENTERPURCHASEDATE != null)
                        //{
                        //    cToday = assetTransaction.COSTCENTERPURCHASEDATE;
                        //}
                       


                        SYSTEM_TRANSACTIONDETAILS systemTrans = new SYSTEM_TRANSACTIONDETAILS();
                        var costcenterdescriptionFound = db.SingleOrDefault<COSTCENTER_DETAILS>(PetaPoco.Sql.Builder.Select("Description").From("COSTCENTER_DETAILS").Where("CODEID=@reqId", new { reqId = assetTransaction.COSTCENTERCODE }));
                        
                        var costcennterName = "";
                        if (costcenterdescriptionFound != null)
                        {
                            costcennterName = costcenterdescriptionFound.DESCRIPTION;
                        }

                        systemTrans.REFCODE = assetDetails.ASSET_NO;
                        systemTrans.REFDESCRIPTION = assetDetails.DESCRIPTION;
                        systemTrans.REFCOSTCENTERCODE = assetTransaction.COSTCENTERCODE;
                        systemTrans.REFCOST = assetTransaction.COSTCENTERPURCHASECOST;
                        systemTrans.REFCOSTCENTERDESCRIPTION = costcennterName;
                        systemTrans.REFMODEL = assetDetails.MODEL_NO;
                        systemTrans.REFBATCHSERIALNO = assetDetails.SERIAL_NO;
                        systemTrans.REFINVOICENO = assetTransaction.COSTCENTERINVOICENO;
                        systemTrans.REFQTY = 1;
                        systemTrans.REFUOM = "Piece";
                        systemTrans.REASONTYPE = "Purchased";
                        systemTrans.REFTYPE = "Asset";
                        systemTrans.COSTCENTERMESSAGE = "This equipment was transferred to your location on "+ String.Format("{0:dd/MM/yyyy}", cToday) + " in good condition.";
                        systemTrans.MESSAGE = "This equipment was transferred to "+ costcennterName+" on "+ String.Format("{0:dd/MM/yyyy}", cToday) + " in good condition.";
                        systemTrans.STATUS = "Y"; 
                        systemTrans.CREATEDBY = modifiedBy;
                        systemTrans.CREATEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                        db.Insert(systemTrans);



                        assetDetails.COSTCENTERTRANSID = assetTransaction.TRANSID;

                    }
                    else
                    {

                        var systransFound = db.SingleOrDefault<SYSTEM_TRANSACTIONDETAILS>(PetaPoco.Sql.Builder.Select("*").From("SYSTEM_TRANSACTIONDETAILS").Where("REFTRANSID=@reqId", new { reqId = assetTransactionFound.TRANSID }).Where("REFCODE=@reqId", new { reqId = assetDetails.ASSET_NO }));

                        DateTime? cToday = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");

                        if (systransFound != null)
                        {
                            var costcenterdescriptionFound = db.SingleOrDefault<COSTCENTER_DETAILS>(PetaPoco.Sql.Builder.Select("Description").From("COSTCENTER_DETAILS").Where("CODEID=@reqId", new { reqId = assetTransaction.COSTCENTERCODE }));

                            var costcennterName = "";
                            if (costcenterdescriptionFound != null)
                            {
                                costcennterName = costcenterdescriptionFound.DESCRIPTION;
                            }

                           
                            systransFound.REFDESCRIPTION = assetDetails.DESCRIPTION;
                            systransFound.REFCOSTCENTERCODE = assetTransaction.COSTCENTERCODE;
                            systransFound.REFCOST = assetTransaction.COSTCENTERPURCHASECOST;
                            systransFound.REFCOSTCENTERDESCRIPTION = costcennterName;
                            systransFound.REFMODEL = assetDetails.MODEL_NO;
                            systransFound.REFBATCHSERIALNO = assetDetails.SERIAL_NO;
                            systransFound.REFINVOICENO = assetTransaction.COSTCENTERINVOICENO;
                            systransFound.REFQTY = 1;
                            systransFound.REFUOM = "Piece";
                            systransFound.COSTCENTERMESSAGE = "This equipment was transferred to your location on " + String.Format("{0:dd/MM/yyyy}", cToday) + " in good condition.";
                            systransFound.MESSAGE = "This equipment was transferred to " + costcennterName + " on " + String.Format("{0:dd/MM/yyyy}", cToday) + " in good condition.";
                            db.Update(systransFound, new string[] { "REFDESCRIPTION", "REFCOSTCENTERCODE", "REFCOST", "REFCOSTCENTERDESCRIPTION", "REFMODEL" , "COSTCENTERMESSAGE", "MESSAGE", "REFBATCHSERIALNO","REFINVOICENO" });
                        }

                        assetTransactionFound.COSTCENTERINSTALLDATE = assetTransaction.COSTCENTERINSTALLDATE;
                        assetTransactionFound.COSTCENTERCODE = assetTransaction.COSTCENTERCODE;
                        assetTransactionFound.COSTCENTERPURCHASEDATE = assetTransaction.COSTCENTERPURCHASEDATE;
                        assetTransactionFound.COSTCENTERPURCHASECOST = assetTransaction.COSTCENTERPURCHASECOST;
                        assetTransactionFound.COSTCENTERASSETCOND = assetTransaction.COSTCENTERASSETCOND;

                        assetTransactionFound.COSTCENTERINVOICENO = assetTransaction.COSTCENTERINVOICENO;


                        db.Update(assetTransactionFound, new string[] { "COSTCENTERINSTALLDATE", "COSTCENTERCODE", "COSTCENTERPURCHASEDATE", "COSTCENTERPURCHASECOST", "COSTCENTERASSETCOND","COSTCENTERINVOICENO","MESSAGE"});
                    }

                }
                else
                {
                    //update systemtransaction
                    if(assetFound.MODEL_NO != assetDetails.MODEL_NO || assetFound.SERIAL_NO != assetDetails.SERIAL_NO || assetFound.DESCRIPTION != assetDetails.DESCRIPTION || assetFound.PURCHASE_DATE != assetDetails.PURCHASE_DATE)
                    {
                        var systransFound = db.SingleOrDefault<SYSTEM_TRANSACTIONDETAILS>(PetaPoco.Sql.Builder.Select("*").From("SYSTEM_TRANSACTIONDETAILS").Where("REFTRANSID=@reqId", new { reqId = "NEWASSET" }).Where("REFCODE=@reqId", new { reqId = assetDetails.ASSET_NO }));

                        if (systransFound != null)
                        {
                            systransFound.REFDESCRIPTION = assetDetails.DESCRIPTION;
                            systransFound.REFMODEL = assetDetails.MODEL_NO;
                            systransFound.REFBATCHSERIALNO = assetDetails.SERIAL_NO;
                            systransFound.COSTCENTERMESSAGE = "This new Asset/Equipment with serial no.(" + assetDetails.SERIAL_NO + ") was added on Asset Onhand. Purchased Date: " + String.Format("{0:dd/MM/yyyy}", assetDetails.PURCHASE_DATE) + ".";
                            systransFound.MESSAGE = "This new Asset/Equipment with serial no.(" + assetDetails.SERIAL_NO + ") was added on Asset Onhand. Purchased Date: " + String.Format("{0:dd/MM/yyyy}", assetDetails.PURCHASE_DATE) + ".";
                            db.Update(systransFound, new string[] { "REFDESCRIPTION", "REFMODEL", "REFBATCHSERIALNO", "COSTCENTERMESSAGE", "MESSAGE" });
                        }

                    }

                }

                if (assetFound.ERECORD_GENERAL_NOTES != assetDetails.ERECORD_GENERAL_NOTES)
                {
                    //DateTime modifyDt = DateTime.Parse(modifyDate);
                    assetNotes.ASSET_NO = assetFound.ASSET_NO;
                    assetNotes.CREATED_DATE = assetFound.NOTE_RECORDED_DATE;
                    assetNotes.NOTES = assetFound.ERECORD_GENERAL_NOTES;
                    assetNotes.CREATED_BY = assetFound.NOTE_RECORDED_BY;
                    db.Insert(assetNotes);
                }

                var pmFound = db.SingleOrDefault<AM_ASSET_PMSCHEDULES>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_PMSCHEDULES").Where("ASSET_NO=@assetId", new { assetId = assetDetails.ASSET_NO }));

                if (pmFound != null)
                {
                    pmFound.STATUS = assetDetails.PM_SCHEDULEFLAG;
                    db.Update(pmFound, new string[] { "STATUS" });
                }

                if(assetDetails.PO_DATE != null)
                {
                    var indexRev = assetDetails.PO_DATE.IndexOf("00:00:00");
                    if(indexRev > 0)
                    {
                        assetDetails.PO_DATE = assetDetails.PO_DATE.Remove(indexRev, 8);
                    }

                    var indexRev2 = assetDetails.PO_DATE.IndexOf("@");
                    if (indexRev2 > 0)
                    {
                        assetDetails.PO_DATE = assetDetails.PO_DATE.Remove(indexRev2, 1);
                    }
                    if (assetDetails.PO_DATE == "@")
                    {
                        assetDetails.PO_DATE = null;
                    }

                    //assetDetails.PO_DATE.Remove("00:00:00");
                }

                var saudiDateTime  = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                assetDetails.LAST_MODIFIEDDATE = saudiDateTime;
                //,"EST_ACQUISITIONCOST", ,"STATUS_DATE",711994,,"Current_Agent"

                string[] UPDATE_AM_ASSET_DETAILS = new string[]
                   {"GROUP_CODE","DEPRECIATION_PERCENT", "LAST_MODIFIEDDATE", "LAST_MODIFIEDBY", "PO_DATE","VOLTAGE", "PM_TYPE","PM_SCHEDULEFLAG", "DESCRIPTION", "CATEGORY", "MANUFACTURER", "SERIAL_NO", "MODEL_NO" ,"LOCATIONFR","WARRANTY_FREQUNIT","NOTE_RECORDED_BY","NOTE_RECORDED_DATE"
                    , "MODEL_NAME", "BUILDING","COST_CENTER","RISK_INCLUSIONFACTOR","LOCATION","LOCATION_DATE","INSERVICE_DATE","STATUS","SUPPLIER"
                    ,"PURCHASE_NO","PURCHASE_COST","CONDITION","CONDITION_DATE","WARRANTY_EXPIRYDATE","Current_Agent"
                    ,"WARRANTY_NOTES","TERM_PERIOD","WARRANTY_INCLUDE","PURCHASE_DATE","REPLACEMENT_COST","REPLACEMENT_DATE","SALVAGE_VALUE"
                    ,"SALVAGE_DATE","SUN_AM","SUN_PM","MON_AM","MON_PM","TUE_AM","TUE_PM","WED_AM","WED_PM","THUR_AM","THUR_PM","FRI_AM","FRI_PM"
                    ,"SAT_AM","SAT_PM","WO_REFERENCE_NOTES","ERECORD_GENERAL_NOTES","SERV_DEPT","SPECIALTY","INHOUSE_OUTSOURCE","PRIMARY_EMPLOYEE"
                    ,"SECONDARY_EMPLOYEE","ASSET_CLASSIFICATION","DEPRECIATED_LIFE","USEFULL_LIFE","RESPONSIBLE_CENTER","RISKGROUP","RISKCLA","RISKCLS"
                    ,"RISKCMT","RISKEQF","RISKESE","RISKEVS","RISKEVT","RISKFPF","RISKLIS","RISKMAR","RISKPCA","FACILITY", "OUTREQFLAG","COSTCENTERTRANSID", "VATRATIO","VATFLAG","MDMANO", "CLASS_TYPE", "INVENTORY_DATE", "INVENTORY_REMARK"};

                var i = assetFound;
                if (i != null)
                {
                    AM_SYSTEM_AUDIT systemAudit = new AM_SYSTEM_AUDIT();
                   
                    var properties = TypeDescriptor.GetProperties(typeof(AM_ASSET_DETAILS));
                    foreach (PropertyDescriptor property in properties)
                    {
                        var fieldname = property.Name;
                        var prop = UPDATE_AM_ASSET_DETAILS.Where(x => x.ToString() == fieldname.ToString() && x.ToString() != "LAST_MODIFIEDDATE" && x.ToString() != "LAST_MODIFIEDBY").Count() > 0;

                        if (prop)
                        {
                            var newdata = property.GetValue(assetDetails);
                            var olddata = property.GetValue(i);
                            if(newdata != null)
                            {
                                if(newdata.ToString() == "01/01/1900 00:00:00" || newdata.ToString() == "01/01/1900 00:00")
                                {
                                    continue;
                                }
                            }
                            if (fieldname.ToString() == "SUN_AM" || fieldname.ToString() == "SUN_PM" || fieldname.ToString() == "MON_AM" || fieldname.ToString() == "MON_PM" || fieldname.ToString() == "TUE_AM" || fieldname.ToString() == "TUE_PM" || fieldname.ToString() == "WED_AM" || fieldname.ToString() == "WED_PM"
                                 || fieldname.ToString() == "THUR_AM" || fieldname.ToString() == "THUR_PM" || fieldname.ToString() == "FRI_AM" || fieldname.ToString() == "FRI_PM" || fieldname.ToString() == "SAT_AM" || fieldname.ToString() == "SAT_PM")
                            {
                                if(olddata == null || olddata.ToString() == "")
                                {
                                    olddata = "01/01/1900 00:00";
                                }
                                if(newdata != null && newdata.ToString() != "")
                                {
                                DateTime dt = DateTime.Parse(newdata.ToString());
                                newdata = dt.ToString("HH:mm");
                                }
                                DateTime dtold = DateTime.Parse(olddata.ToString());
                                olddata = dtold.ToString("HH:mm");

                            }
                          
                            if (fieldname.ToString() == "DEPRECIATION_PERCENT" || fieldname.ToString() == "PURCHASE_COST") {
                                bool dec = newdata.ToString().Contains(".");
                                decimal decData;
                                if (dec)
                                {
                                    decData = decimal.Parse(newdata.ToString() + "00");
                                }
                                else
                                {
                                    decData = decimal.Parse(newdata.ToString() + ".00");
                                } 
                                newdata = decimal.Round(decData, 2).ToString();
                            }

                            if (newdata == null)
                            {
                                newdata = "";
                            }
                            if (olddata == null)
                            {
                                olddata = "";
                            }
                            if (newdata.ToString() != olddata.ToString())
                            {
                                systemAudit.REF_ID = i.ASSET_NO;
                                systemAudit.FIELD_NAME = fieldname.ToString();
                                systemAudit.ACTION_CODE = "AM_ASSET_DETAILS";
                                systemAudit.OLDDATA = olddata.ToString();
                                systemAudit.NEWDATA = newdata.ToString();
                                systemAudit.RECORDED_BY = modifiedBy;
                                systemAudit.RECORDED_DATE = saudiDateTime;
                                db.Insert(systemAudit);
                            }
                        }

                    }
                }


                db.Update(assetDetails, UPDATE_AM_ASSET_DETAILS);

                transScope.Complete();
            }

            return assetDetails;
        }
        public List<dynamic> SearchAsset(string queryString, SearchFilter filter, string userId, int maxRows = 2000000)
        {
            var assetList = new List<dynamic>();

            var userBuildingFound = db.Fetch<USER_BUILDING>(PetaPoco.Sql.Builder.Select("*").From("USER_BUILDING").Where("USER_ID=@refId", new { refId = userId }));

            if(userBuildingFound.Count() == 0)
            {
                userId = null;
            }

            var assetFound = db.SingleOrDefault<AM_ASSET_DETAILS>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_DETAILS").Where("ASSET_NO=@assetId", new { assetId = queryString }));

            if (assetFound != null)
            {
                filter.Status = null;
                assetList = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAsset(db._dbType.ToString(), queryString, filter, userId, maxRows)));

                if (assetList == null)
                {
                    throw new Exception("No match found.") { Source = this.GetType().Name };
                }
            }
            else
            {
                assetList = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAsset(db._dbType.ToString(), queryString, filter, userId, maxRows)));

                if (assetList == null)
                {
                    throw new Exception("No match found.") { Source = this.GetType().Name };
                }

            }
            return assetList;
        }


        public List<dynamic> SearchAssetOnHand(string queryString, SearchFilter filter, string userId, int maxRows = 2000000)
        {
            var assetList = new List<dynamic>();
            assetList = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetOnHand(db._dbType.ToString(), queryString, filter, userId, maxRows)));

            if (assetList == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }


            return assetList;
        }
       




        public List<dynamic> SearchAssetCloneId(string queryString, SearchFilter filter, int maxRows = 200000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetCloneId(db._dbType.ToString(), queryString, filter, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchAssetCostCenter(string queryString, SearchFilter filter, int maxRows = 100000)
		{
			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetCostCenter(db._dbType.ToString(), queryString, filter, maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}
		public List<dynamic> SearchWOSummaryById(string queryString, int maxRows = 500)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetWOSummaryById(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchWOCostSummaryById(string queryString, int maxRows = 500)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetWOCostSummaryById(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchAssetActive(string queryString, SearchFilter filter, int maxRows = 100000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetActive(db._dbType.ToString(), queryString, filter, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchAssetWOActive(string queryString, SearchFilter filter, string userId, int maxRows = 100000)
        {

            var userBuildingFound = db.Fetch<USER_BUILDING>(PetaPoco.Sql.Builder.Select("*").From("USER_BUILDING").Where("USER_ID=@refId", new { refId = userId }));

            if (userBuildingFound.Count() == 0)
        {
                userId = null;
            }

            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetWOActive(db._dbType.ToString(), queryString,  filter, userId, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchAssetBIOActive(string queryString, SearchFilter filter, int maxRows = 100000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetBIOMEDActive(db._dbType.ToString(), queryString, filter, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchWOContractCostSummaryById(string queryString, int maxRows = 1000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetWOContractCostSummaryById(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchWOMaterialCostSummaryById(string queryString, int maxRows = 1000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetWOMaterialCostSummaryById(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
		public int InServiceAssetCount(SearchFilter filter)
		{
			//SEARCH_COUNT inserviceAssets[] = null;
			var totalCount = 0;
			using (var transScope = db.GetTransaction())
			{
				if (!string.IsNullOrWhiteSpace(filter.staffId))
				{
					var inserviceAssets = db.SingleOrDefault<SEARCH_COUNT>(PetaPoco.Sql.Builder.Append("SELECT COUNT(ASSET_NO) AS COUNTROW FROM AM_ASSET_DETAILS WHERE STATUS ='I' AND COST_CENTER in (SELECT CODE FROM STAFF_COSTCENTER_BUILDING WHERE UCATEGORY = 'AIMS_COSTCENTER' AND STAFFCODE ='"+ filter.staffId + "')"));
						totalCount = inserviceAssets.COUNTROW;
				

				}
				else
				{
                    var assetStatus = "I";
					var inserviceAssets = db.SingleOrDefault<SEARCH_COUNT>(PetaPoco.Sql.Builder.Append("SELECT COUNT(ASSET_NO) AS COUNTROW FROM AM_ASSET_DETAILS WHERE STATUS ='" + assetStatus+"'"));
					totalCount = inserviceAssets.COUNTROW;
				}


				transScope.Complete();

			}

			return totalCount;
		}
		

		//ASSET WORK ORDER LABOUR
		public AM_WORKORDER_LABOUR CreateNewWOLabour(AM_WORKORDER_LABOUR infoDetails)
        {

            using (var transScope = db.GetTransaction())
            {

                db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='AIMS_LABOURNO'");
                var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "AIMS_LABOURNO" }));
                infoDetails.AM_WORKORDER_LABOUR_ID = seq[0].SEQ_VAL;
                //apply processed data
                db.Insert(infoDetails);

                transScope.Complete();
            }

            return infoDetails;
        }
        public AM_WORKORDER_LABOUR ReadWOLabour(string refID)
        {
            AM_WORKORDER_LABOUR ret = null;

            var woFound = db.SingleOrDefault<AM_WORKORDER_LABOUR>(PetaPoco.Sql.Builder.Select("*").From("AM_WORKORDER_LABOUR").Where("AM_WORKORDER_LABOUR_ID=@refId", new { refId = refID }));

            if (woFound != null)
            {
                ret = woFound;
            }
            else
            {
                throw new Exception("Work Order not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdateWOLabour(AM_WORKORDER_LABOUR infoDetails)
        {
            //validate data
            if (infoDetails != null)
            {
                if (infoDetails.ASSET_NO == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(infoDetails, new string[] { "ASSET_NO", "REQ_NO", "EMPLOYEE", "RESPONSE", "HOURS"
                    , "LABOUR_DATE", "LABOUR_ACTION","LABOUR_COST","LABOUR_CHARGE","LABOUR_RATE","LABOUR_TYPE","CONTRACT_NO","FLAT_FEE", "BILLABLE", "TOTAL_COST",
                "TRAVEL","RATE_TYPE", "SUPPLIER_ID", "REF_ID"});

                transScope.Complete();
            }

            return 0;
        }
        public List<AM_WORKORDER_LABOUR> SearchWOLabour(string queryString, string queryReqNo, int maxRows = 1000)
        {
            var pmscheduleFound = db.Fetch<AM_WORKORDER_LABOUR>(PetaPoco.Sql.Builder.Append((string)new IndieSearchWOLabour(db._dbType.ToString(), queryString, queryReqNo, maxRows)));

            if (pmscheduleFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return pmscheduleFound;
        }

        public List<dynamic> SearchWOLabourLOV(string queryString, string queryReqNo, int maxRows = 1000)
        {
            var pmscheduleFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchWOLabourLOV(db._dbType.ToString(), queryString, queryReqNo, maxRows)));

            if (pmscheduleFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return pmscheduleFound;
        }
        public List<dynamic> SearchWOLabourAct(string queryString, string queryReqNo, int maxRows = 1000)
        {
            var pmscheduleFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchWOLabourAct(db._dbType.ToString(), queryString, queryReqNo, maxRows)));

            if (pmscheduleFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return pmscheduleFound;
        }
        

        public List<dynamic> SearchOTHById(string queryString,  int maxRows = 1000)
        {
    
                var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchothById(db._dbType.ToString(), queryString, maxRows)));
                if (productsFound == null)
                {
                    throw new Exception("No match found.") { Source = this.GetType().Name };
                }
                
  

            return productsFound;

        }

        public List<dynamic> SearchRefType(string queryString, int maxRows = 1000)
        {

            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchrefType(db._dbType.ToString(), queryString, maxRows)));
            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }



            return productsFound;

        }

        //ASSET Supplier
        public AM_SUPPLIER CreateNewSupplier(AM_SUPPLIER supplierDetails)
        {

            using (var transScope = db.GetTransaction())
            {


                db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='SUPPLIER_ID'");

                var seq = db.SingleOrDefault<SEQ>(PetaPoco.Sql.Builder.Select("*").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "SUPPLIER_ID" }));
                var seqId = seq.SEQ_VAL.ToString();
                supplierDetails.SUPPLIER_ID = seqId;
                //apply processed data
                db.Insert(supplierDetails);

                transScope.Complete();
            }

            return supplierDetails;
        }
        public AM_SUPPLIER ReadSupplierInfo(string supplierID)
        {
            AM_SUPPLIER ret = null;

            var supplierFound = db.SingleOrDefault<AM_SUPPLIER>(PetaPoco.Sql.Builder.Select("*").From("AM_SUPPLIER").Where("SUPPLIER_ID=@supplierId", new { supplierId = supplierID }));

            if (supplierFound != null)
            {
                ret = supplierFound;
            }
            else
            {
                throw new Exception("Equipment not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdateSupplierInfo(AM_SUPPLIER supplierDetails)
        {
            //validate data
            if (supplierDetails != null)
            {
                if (supplierDetails.SUPPLIER_ID == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(supplierDetails, new string[] { "DESCRIPTION", "ADDRESS1", "ADDRESS2", "ADDRESS3", "CITY"
                    , "STATE", "ZIP","SUPPLIER_NO","TERM","MIN_ORDERAMOUNT","REMARKS","STATUS"
                    ,"ADDITIONAL_INFO","CONTACT","CONTACT_NO1" ,"CONTACT_NO2","EMAIL_ADDRESS","EQUIPMENT_SUPPLIER","PART_SUPPLIER","SERVICE_PROVIDER"
                    ,"PROVIDER_TYPE"});

                transScope.Complete();
            }

            return 0;
        }
        public List<dynamic> SearchSupplier(string queryString, int maxRows = 1000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchSupplier(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        public List<dynamic> SearchSupplierById(string queryString, int maxRows = 1000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchSupplierById(db._dbType.ToString(), queryString, maxRows)));

            //if (productsFound == null)
            //{
            //    throw new Exception("No match found.") { Source = this.GetType().Name };
            //}
            return productsFound;
        }
		public List<dynamic> SearchSupplierLookup(string queryString, int maxRows = 10000)
		{
			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchSupplierActiveLookup(db._dbType.ToString(), queryString, maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}
		public List<dynamic> SearchSupplierLookupData(string queryString, int maxRows = 10000)
		{
			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchSupplierActiveLOVData(db._dbType.ToString(), queryString, maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}
		

		//PM PRUCEDURE
		public AM_PROCEDURES CreateNewProcedure(AM_PROCEDURES infoDetails)
		{

			using (var transScope = db.GetTransaction())
			{
				db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='AIMS_PMPROCEDURE'");
				var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "AIMS_PMPROCEDURE" }));
				infoDetails.PROCEDURE_ID = Convert.ToString(seq[0].SEQ_VAL);

				//apply processed data
				db.Insert(infoDetails);

				transScope.Complete();
			}

			return infoDetails;
		}
		public AM_PROCEDURES ReadProcedureInfo(string procedureID)
		{
			AM_PROCEDURES ret = null;

			var procedureFound = db.SingleOrDefault<AM_PROCEDURES>(PetaPoco.Sql.Builder.Select("*").From("AM_PROCEDURES").Where("PROCEDURE_ID=@procedureId", new { procedureId = procedureID }));

			if (procedureFound != null)
			{
				ret = procedureFound;
			}
			else
			{
				throw new Exception("Procedure not found.") { Source = this.GetType().Name };
			}
			return ret;
		}
		public int UpdateProcedureInfo(AM_PROCEDURES infoDetails)
		{
			//validate data
			if (infoDetails != null)
			{
				if (infoDetails.PROCEDURE_ID == null)
					throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
			}

			using (var transScope = db.GetTransaction())
			{

				//preprocess data

				//apply processed data
				db.Update(infoDetails, new string[] { "DESCRIPTION", "SERV_DEPT", "ASSIGN_TO", "AVERAGE_TIME", "PROCEDURE_TYPE"
					, "REMARKS", "STATUS","TEST_DVICE","INC_TASKRESULT","ROUND","ALLOW_OVERRIDE"});

				transScope.Complete();
			}

			return 0;
		}
		public List<dynamic> SearchProcedure(string queryString, int maxRows = 1000)
		{
			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchProcedure(db._dbType.ToString(), queryString, maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}
		public List<dynamic> SearchProcedureActiveLookup(string queryString, int maxRows = 1000)
		{
			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchProcedureActiveLookup(db._dbType.ToString(), queryString, maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}

		//Work Order Dispatch Parts
		public List<dynamic> SearchWorkOrderParts(string queryString,SearchFilter filter, int maxRows = 1000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchWorkOrderParts(db._dbType.ToString(), queryString, filter, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        //METER READINGS
        public AM_METERREADING CreateNewMeterReading(AM_METERREADING infoDetails)
        {

            using (var transScope = db.GetTransaction())
            {

                //apply processed data
                db.Insert(infoDetails);

                transScope.Complete();
            }

            return infoDetails;
        }
        public AM_METERREADING ReadMeterReadingInfo(string infoID)
        {
            AM_METERREADING ret = null;

            var supplierFound = db.SingleOrDefault<AM_METERREADING>(PetaPoco.Sql.Builder.Select("*").From("AM_METERREADING").Where("AM_METERREADINGID=@infoId", new { infoId = infoID }));

            if (supplierFound != null)
            {
                ret = supplierFound;
            }
            else
            {
                throw new Exception("Meter Reading not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdateMeterReadingInfo(AM_METERREADING infoDetails)
        {
            //validate data
            if (infoDetails != null)
            {
                if (infoDetails.AM_METERREADINGID == 0)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(infoDetails, new string[] { "ASSET_NO", "AM_INSPECTION_ID", "REQ_NO", "READING_DATE", "NEXT_PMDUE", "CURRENT_READING","REMARKS"});

                transScope.Complete();
            }

            return 0;
        }
        public List<dynamic> SearchMeterReading(string queryString, int maxRows = 20)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchMeterReadingAssetNo(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        //INSPECTIONTRANS
        public AM_INSPECTIONTRANS CreateNewInspectionTrans(AM_INSPECTIONTRANS infoDetails)
        {

            using (var transScope = db.GetTransaction())
            {

                //apply processed data
                db.Insert(infoDetails);

                transScope.Complete();
            }

            return infoDetails;
        }
        public AM_INSPECTIONTRANS ReadMeterInspectionTransInfo(string infoID)
        {
            AM_INSPECTIONTRANS ret = null;

            var supplierFound = db.SingleOrDefault<AM_INSPECTIONTRANS>(PetaPoco.Sql.Builder.Select("*").From("AM_INSPECTIONTRANS").Where("AM_INSPECTIONTRANSID=@infoId", new { infoId = infoID }));

            if (supplierFound != null)
            {
                ret = supplierFound;
            }
            else
            {
                throw new Exception("Inspection Transaction not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdateInspectionTransInfo(AM_INSPECTIONTRANS infoDetails)
        {
            //validate data
            if (infoDetails != null)
            {
                if (infoDetails.AM_INSPECTIONTRANSID == 0)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(infoDetails, new string[] { "REQ_NO", "ASSET_NO", "INSPECTION_DATE", "INSPECTION_TIME", "EMPLOYEE"
                ,"INSPECTION_RESULT","TOTAL_HOURWS","RATE","COST_CENTER","INSP_TYPE","FAILURE","INSPROCEDURE","REMARKS"});

                transScope.Complete();
            }

            return 0;
        }
        public List<dynamic> SearchInspectionTrans(string queryString, int maxRows = 1000)
        {

			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchInspectionTransWS(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
        
        //WORKSHEET/INSPECTION
        public AM_INSPECTION CreateNewWorkSheet(AM_INSPECTION infoDetails)
        {

            using (var transScope = db.GetTransaction())
            {

                //apply processed data
                db.Insert(infoDetails);

                transScope.Complete();
            }

            return infoDetails;
        }
        public AM_INSPECTION ReadWorkSheetInfo(string infoID)
        {
            AM_INSPECTION ret = null;

            var wsFound = db.SingleOrDefault<AM_INSPECTION>(PetaPoco.Sql.Builder.Select("*").From("AM_INSPECTION").Where("AM_INSPECTION_ID=@infoId", new { infoId = infoID }));

            if (wsFound != null)
            {
                ret = wsFound;
            }
            else
            {
                throw new Exception("Work Sheet not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdateWorkSheetInfo(AM_INSPECTION infoDetails)
        {
            //validate data
            if (infoDetails != null)
            {
                if (infoDetails.AM_INSPECTION_ID == 0)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(infoDetails, new string[] { "DESCRIPTION", "PROCESS", "INSPECTION_TYPE", "ASSET_TYPECOVERED", "REMARKS"});

                transScope.Complete();
            }

            return 0;
        }
        public List<dynamic> SearchWorkSheet(string queryString, int maxRows = 1000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchWorkSheet(db._dbType.ToString(), queryString, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }

        //ASSET PM SCHEDULE
        public AM_ASSET_PMSCHEDULES CreateNewPMSchedule(AM_ASSET_PMSCHEDULES pmscheduleDetails)
        {

            using (var transScope = db.GetTransaction())
            {
                db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='AIMS_PMSCHENO'");
                var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "AIMS_PMSCHENO" }));
                pmscheduleDetails.AM_ASSET_PMSCHEDULE_ID = Convert.ToString(seq[0].SEQ_VAL);
                //apply processed data
                db.Insert(pmscheduleDetails);

                transScope.Complete();
            }

            return pmscheduleDetails;
        }
        public AM_ASSET_PMSCHEDULES readPMSchedule(string pmscheduleID)
        {
            AM_ASSET_PMSCHEDULES ret = null;

            var supplierFound = db.SingleOrDefault<AM_ASSET_PMSCHEDULES>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_PMSCHEDULES").Where("AM_ASSET_PMSCHEDULE_ID=@pmscheduleId", new { pmscheduleId = pmscheduleID }));

            if (supplierFound != null)
            {
                ret = supplierFound;
            }
            else
            {
                throw new Exception("Equipment not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdatePMSchedule(AM_ASSET_PMSCHEDULES pmscheduleDetails, string modifiedBy)
        {
            //validate data
            if (pmscheduleDetails != null)
            {
                if (pmscheduleDetails.AM_ASSET_PMSCHEDULE_ID == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {
                string[] UPDATE_AM_ASSET_PMSCHEDULES = new string[]
                     {
                        "PM_PROCEDURE", "SERVICE_DEPARTMENT", "EMPLOYEE_TYPE", "EMPLOYEE_INFO", "FREQUENCY_PERIOD"
                    , "INTERVAL", "NEXT_PM_DUE", "RANDOM_PMSCHEDULE", "FLOATING_PMSCHEDULE", "GRACE_PERIOD_TYPE", "GRACE_PERIOD", "TIME_PERFORM"
                    , "PM_PRIORITY", "SESSION_START", "SESSION_END", "LAST_MODIFIED_BY", "LAST_MODIFIED_DATE", "REMARKS", "PM_PROCESS", "PM_STATUS", "STATUS" };

                var i = db.SingleOrDefault<AM_ASSET_PMSCHEDULES>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_PMSCHEDULES").Where("AM_ASSET_PMSCHEDULE_ID=@pmId", new { pmId = pmscheduleDetails.AM_ASSET_PMSCHEDULE_ID }));
                    if (i != null)
                    {
                        AM_SYSTEM_AUDIT systemAudit = new AM_SYSTEM_AUDIT();
                        var saudiDateTime = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                        var properties = TypeDescriptor.GetProperties(typeof(AM_ASSET_PMSCHEDULES));
                        foreach (PropertyDescriptor property in properties)
                        {
                            var fieldname = property.Name;
                            var prop = UPDATE_AM_ASSET_PMSCHEDULES.Where(x => x.ToString() == fieldname.ToString() && x.ToString() != "LAST_MODIFIED_DATE").Count() > 0;

                            if(prop) {
                                var newdata = property.GetValue(pmscheduleDetails);
                                var olddata = property.GetValue(i);
                                if (fieldname.ToString() == "TIME_PERFORM")
                                {
                                    if(newdata.ToString() == "0")
                                    {
                                        newdata = "0.00";
                                    }
                                }
                                if (newdata == null)
                                {
                                    newdata = "";
                                }
                                if (olddata == null)
                                {
                                    olddata = "";
                                }
                                if (newdata.ToString() != olddata.ToString())
                                {
                                    systemAudit.REF_ID = i.AM_ASSET_PMSCHEDULE_ID;
                                    systemAudit.FIELD_NAME = fieldname.ToString();
                                    systemAudit.ACTION_CODE = "AM_ASSET_PMSCHEDULES";
                                    systemAudit.OLDDATA = olddata.ToString();
                                    systemAudit.NEWDATA = newdata.ToString();
                                    systemAudit.RECORDED_BY = modifiedBy;
                                    systemAudit.RECORDED_DATE = saudiDateTime;
                                    db.Insert(systemAudit);
                                }
                            }
                            
                        }
                    }
             
                //preprocess data
                //apply processed data
                db.Update(pmscheduleDetails, UPDATE_AM_ASSET_PMSCHEDULES);
                transScope.Complete();
            }

            return 0;
        }
        public List<dynamic> SearchPMSchedule(string queryString, string queryAssetNo, int maxRows = 1000)
        {
            var pmscheduleFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchPMSchedule(db._dbType.ToString(), queryString, queryAssetNo, maxRows)));

            if (pmscheduleFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return pmscheduleFound;
        }
        public AM_ASSET_PMSCHEDULES readPMScheduleByAsset(string assetNo)
        {
            AM_ASSET_PMSCHEDULES ret = null;

            var pmScheduleFound = db.SingleOrDefault<AM_ASSET_PMSCHEDULES>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_PMSCHEDULES").Where("ASSET_NO=@assetNO", new { assetNO= assetNo }));
            ret = pmScheduleFound;
            
            return ret;
        }

        //ASSET INFORMATION SERVICES
        public AM_ASSET_INFOSERVICES CreateNewInfoServices(AM_ASSET_INFOSERVICES infoDetails)
        {

            using (var transScope = db.GetTransaction())
            {
                db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='AIMS_INFOSERVNO'");
                var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "AIMS_INFOSERVNO" }));
                infoDetails.ASSET_INFOSERVICES_ID = seq[0].SEQ_VAL;

                //apply processed data
                db.Insert(infoDetails);

                transScope.Complete();
            }

            return infoDetails;
        }
        public AM_ASSET_INFOSERVICES readInfoServices(string infoID)
        {
            AM_ASSET_INFOSERVICES ret = null;
            //Dictionary<string, object> cret = null;
            var supplierFound = db.SingleOrDefault<AM_ASSET_INFOSERVICES>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_INFOSERVICES").Where("ASSET_NO=@infoId", new { infoId = infoID }));

            if (supplierFound != null)
            {
                ret = supplierFound;
            }
            else
            {
                throw new Exception("Information Services not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdateInfoServices(AM_ASSET_INFOSERVICES infoDetails)
        {
            //validate data
            if (infoDetails != null)
            {
                if (infoDetails.ASSET_INFOSERVICES_ID == 0)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(infoDetails, new string[] { "ASSET_NO", "PURPOSE", "SOFTWARE_VERSION", "REVISION_LEVEL", "AVAILABLE_MODULE"
                    , "CONNECTED_NETWORK", "SUPPORT_HIPAA","SUPPORT_LASTUPDATED","SUPPORT_DIAGCONNECTION","SUPPORT_CHARGE","SUPPORT_COST","SUPPORT_SERVSOFTWARE"
                    ,"SUPPORT_VERSION","SUPPORT_CRITICALUPDATE","LAST_UPDATED_BY","LAST_UPDATED_DATE","SUPPORT_REMOTE","BACKUP_METHOD","BACKUP_SCHEDULE","RECOVERY_POINT","RECOVERY_TIME"
					,"RECOVERY_PRIORITY","RECOVERY_OFFSITE"});


				transScope.Complete();
            }

            return 0;
        }
        
        //ASSET CONTRACT SERVICES
        public AM_ASSET_CONTRACTSERVICES CreateNewContractService(AM_ASSET_CONTRACTSERVICES contractserviceDetails)
        {

            using (var transScope = db.GetTransaction())
            {

                var supplierFound = db.SingleOrDefault<AM_ASSET_CONTRACTSERVICES>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_CONTRACTSERVICES").Where("CONTRACT_ID=@contractserviceId", new { contractserviceId = contractserviceDetails.CONTRACT_ID }));

                if (supplierFound != null) { 

                    throw new Exception("EXISTCODE "+ supplierFound.PROVIDER+" "+ supplierFound.TYPE_COVERAGE) { Source = this.GetType().Name };
            }

            db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='AIMS_CONTRACTNO'");
                var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "AIMS_CONTRACTNO" }));
                contractserviceDetails.ASSET_CONTRACTSERVICE_ID = seq[0].SEQ_VAL;

                //apply processed data
                db.Insert(contractserviceDetails);

                transScope.Complete();
            }

            return contractserviceDetails;
        }
        public AM_ASSET_CONTRACTSERVICES readContractService(string contractserviceID)
        {
            AM_ASSET_CONTRACTSERVICES ret = null;

            var supplierFound = db.SingleOrDefault<AM_ASSET_CONTRACTSERVICES>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_CONTRACTSERVICES").Where("ASSET_CONTRACTSERVICE_ID=@contractserviceId", new { contractserviceId = contractserviceID }));

            if (supplierFound != null)
            {
                ret = supplierFound;
            }
            else
            {
                throw new Exception("Equipment not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdateContractService(AM_ASSET_CONTRACTSERVICES contractserviceDetails)
        {
            //validate data
            if (contractserviceDetails != null)
            {
                if (contractserviceDetails.ASSET_CONTRACTSERVICE_ID == 0)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(contractserviceDetails, new string[] { "CONTRACT_ID", "PROVIDER", "TYPE_COVERAGE", "TERM_EFFECTIVE", "TERM_TERMINATION"
                    , "REMARKS", "COVERAGE_LABOR","COVERAGE_PARTSMATERIALS","ASSET_NO","PURCHASE_ORDER","TERM_REVIEW","COST_TOTAL"
                    ,"COST_PERPIECE","COST_LASTPERIOD","LABOR_RATEPERHOUR","LABOR_HOURS" ,"LAST_UPDATED_BY","LAST_UPDATED_DATE"});

                transScope.Complete();
            }

            return 0;
        }
        public List<AM_ASSET_CONTRACTSERVICES> SearchAssetContractService(string queryString, string queryAssetNo, int maxRows = 1000)
        {
            var pmscheduleFound = db.Fetch<AM_ASSET_CONTRACTSERVICES>(PetaPoco.Sql.Builder.Append((string)new IndieSearchAssetContractService(db._dbType.ToString(), queryString, queryAssetNo, maxRows)));

            if (pmscheduleFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return pmscheduleFound;
        }


		//ASSET INFORSERVICE DEVICE
		public AM_ASSET_INFOSERVICES_DEVICE CreateNewINFOSRVDvice(AM_ASSET_INFOSERVICES_DEVICE infoDetails)
		{

			using (var transScope = db.GetTransaction())
			{
				
				//apply processed data
				db.Insert(infoDetails);

				transScope.Complete();
			}

			return infoDetails;
		}
		public AM_ASSET_INFOSERVICES_DEVICE readINFOSRVDvice(string dviceID, string assetNo)
		{
			AM_ASSET_INFOSERVICES_DEVICE ret = null;

			var supplierFound = db.SingleOrDefault<AM_ASSET_INFOSERVICES_DEVICE>(PetaPoco.Sql.Builder.Select("*").From("AM_ASSET_INFOSERVICES_DEVICE").Where("DEVICE_ID=@dviceId", new { dviceId = dviceID }).Where("ASSET_NO=@assetNum", new { assetNum = assetNo }));

			if (supplierFound != null)
			{
				ret = supplierFound;
			}
			else
			{
				throw new Exception("Equipment Information Service Device and Network not found.") { Source = this.GetType().Name };
			}
			return ret;
		}
		public int UpdateINFOSRVDvice(AM_ASSET_INFOSERVICES_DEVICE infoDetails)
		{
			//validate data
			if (infoDetails != null)
			{
				if (infoDetails.DEVICE_ID == null)
					throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
			}

			using (var transScope = db.GetTransaction())
			{

				//preprocess data

				//apply processed data
				db.Update(infoDetails, new string[] { "COMPUTER_BASE", "COMPUTER_TYPE", "MODEL", "OPERATING_SYSTEM", "FRIMWARE_VERSION"
					, "PROCESSOR", "MEMORY","HARD_DRIVE","BIOS_VERSION","ADDITIONAL_DRIVE","WIRELESS_FREQ","SOFTWARE_PACKAGE"
					,"NET_DROP_LOCATION","NET_IP_ADDRESS","NET_SUB_NET","NET_SUB_MASK" ,"NET_ROUTER_TYPE","NET_MAC"
					,"NET_ROUTER_LOC4","NET_ROUTER_LOC5","NET_SPEED","STATUS"
				,"NET_AE_TITLE","NET_MODEM_PHONE","NET_VIRTUAL_LAN","NET_ROUTER_LOC1" ,"NET_ROUTER_LOC2","NET_ROUTER_LOC3","SERIAL_NO"});

				transScope.Complete();
			}

			return 0;
		}
		public List<dynamic> SearchINFOSRVDvice(string queryString, string assetNo, int maxRows = 1000)
		{
			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchInfoServiceDvice(db._dbType.ToString(), queryString, assetNo, maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}

		//ASSET Manufacturer
		public AM_MANUFACTURER CreateNewManufacturer(AM_MANUFACTURER manufacturerDetails, List<CLIENTOTHCONTACT> CLIENTOTHCONTACTS, string userId)
        {

            using (var transScope = db.GetTransaction())
            {

                db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='MANUFACTURER_ID'");

                var seq = db.SingleOrDefault<SEQ>(PetaPoco.Sql.Builder.Select("*").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "MANUFACTURER_ID" }));
                var seqId = seq.SEQ_VAL.ToString();

                manufacturerDetails.MANUFACTURER_ID = seqId;

                //apply processed data
                //db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='MANUFACTURERCODE'");
                //var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "MANUFACTURERCODE" }));
                //manufacturerDetails.MANUFACTURER_ID = Convert.ToString(seq[0].SEQ_VAL);
                db.Insert(manufacturerDetails);

                foreach (var item in CLIENTOTHCONTACTS)
                {
                    item.TYPECODE = "MV";
                    item.REFCODE = manufacturerDetails.MANUFACTURER_ID;
                    item.CREATEDBY = userId;
                    item.CREATEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                    db.Insert(item);
                }

                transScope.Complete();
            }

            return manufacturerDetails;
        }
        public AM_MANUFACTURER ReadManufacturerInfo(string manufacturerID)
        {
            AM_MANUFACTURER ret = null;

            var manufacturerFound = db.SingleOrDefault<AM_MANUFACTURER>(PetaPoco.Sql.Builder.Select("*").From("AM_MANUFACTURER").Where("MANUFACTURER_ID=@manufacturerId", new { manufacturerId = manufacturerID }));

            if (manufacturerFound != null)
            {
                ret = manufacturerFound;
            }
            else
            {
                throw new Exception("Equipment not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public int UpdateManufacturerInfo(AM_MANUFACTURER manufacturerDetails, List<CLIENTOTHCONTACT> CLIENTOTHCONTACTS, string userId)
        {
            //validate data
            if (manufacturerDetails != null)
            {
                if (manufacturerDetails.MANUFACTURER_ID == null)
                    throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {

                //preprocess data

                //apply processed data
                db.Update(manufacturerDetails, new string[] { "DESCRIPTION", "ADDRESS1", "ADDRESS2", "ADDRESS3", "TYPE", "CITY"
                    , "STATE", "ZIP","TERM","MIN_ORDERAMOUNT","REMARKS","STATUS"
                    ,"ADDITIONAL_INFO","CONTACT","CONTACT_NO1" ,"CONTACT_NO2","EMAIL_ADDRESS","CONTACT_NO3","PROVIDER_TYPE"});

                db.Execute("DELETE ClientOthContact WHERE RefCode='" + manufacturerDetails.MANUFACTURER_ID + "'");

                foreach (var item in CLIENTOTHCONTACTS)
                {
                    item.TYPECODE = "MV";
                    item.REFCODE = manufacturerDetails.MANUFACTURER_ID;
                    item.CREATEDBY = userId;
                    item.CREATEDDATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                    db.Insert(item);
                }

                transScope.Complete();
            }

            return 0;
        }
        public int DeleteManufacturerInfo(string manufacturerId)
        {
            //validate data
            if (manufacturerId == null)
            {
                throw new Exception("Some values supplied were invalid.") { Source = this.GetType().Name };
            }

            using (var transScope = db.GetTransaction())
            {
                //apply processed data
                db.Execute("UPDATE AM_MANUFACTURER SET STATUS = 'D' WHERE MANUFACTURER_ID='" + manufacturerId + "'");

                transScope.Complete();
            }

            return 0;
        }
        public List<dynamic> SearchManufacturer(string queryString, SearchFilter filter, int maxRows = 10000)
        {
            var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchManufacturer(db._dbType.ToString(), queryString, filter, maxRows)));

            if (productsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return productsFound;
        }
		public List<dynamic> SearchManufacturerLookUp(string queryString, int maxRows = 10000)
		{
			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchManufacturerLookUpActive(db._dbType.ToString(), queryString, maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}
		public List<dynamic> SearchMSVLookUp(string queryString, string type, int maxRows = 10000)
		{
			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchMSVLookUp(db._dbType.ToString(), queryString, type, maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}
		public List<dynamic> SearchManufacturerId(string queryString, int maxRows = 1000)
		{
			var productsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchManufacturerId(db._dbType.ToString(), queryString, maxRows)));

			if (productsFound == null)
			{
				throw new Exception("No match found.") { Source = this.GetType().Name };
			}
			return productsFound;
		}
		//ASSET IMAGES
		public List<SCANDOCS> CreateAssetImages(List<SCANDOCS> scandocImages)
        {


            using (var transScope = db.GetTransaction())
            {

                foreach (SCANDOCS i in scandocImages) db.Insert(i);

                transScope.Complete();
            }
            return scandocImages;
        }
        public SCANDOCS CreateAssetPMImages(SCANDOCS inforeturn , MODIFYINFO info)
        {


            using (var transScope = db.GetTransaction())
            {

                DateTime modifyDt = DateTime.Today;
                //var modifyDtst = modifyDt.ToString("yyyy/MM/dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
                if (info.filename != null && info.filename != "")
                {
                    SCANDOCS scandocInfo = new SCANDOCS();
                    db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='IMAGECOUNT'");
                    var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "IMAGECOUNT" }));
                    scandocInfo.CATEGORY = info.category;
                    scandocInfo.FILE_NAME = "PM"+info.refid+'_'+ seq[0].SEQ_VAL + '_'+ info.filename;
                    scandocInfo.FILE_TYPE = info.filetype;
                    scandocInfo.REF_ID = info.refid;
                    scandocInfo.CREATED_BY = info.statusBy;
                    scandocInfo.CREATED_DATE = modifyDt;
                    inforeturn = scandocInfo;
                    db.Insert(scandocInfo);
                }

                db.Execute("UPDATE AM_ASSET_PMSCHEDULES SET PM_PROCESS='" + info.process + "' WHERE AM_ASSET_PMSCHEDULE_ID =" + info.refid);
                transScope.Complete();
            }
            return inforeturn;
        }
        public SCANDOCS CreateAssetWODocs(SCANDOCS inforeturn, MODIFYINFO info)
        {


            using (var transScope = db.GetTransaction())
            {
                DateTime modifyDt = DateTime.Parse(info.StatusDate);

                if (info.filename != null && info.filename != "")
                {
                    SCANDOCS scandocInfo = new SCANDOCS();
                    db.Execute("UPDATE SEQ SET SEQ_VAL=SEQ_VAL+1 WHERE SEQ_ID='IMAGECOUNT'");
                    var seq = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Select("SEQ_VAL").From("SEQ").Where("SEQ_ID=@seqId", new { seqId = "IMAGECOUNT" }));
                    scandocInfo.CATEGORY = info.category;
                    scandocInfo.FILE_NAME = "WO" + info.refid + '_' + seq[0].SEQ_VAL + '_' + info.filename;
                    scandocInfo.FILE_TYPE = info.filetype;
                    scandocInfo.REF_ID = info.refid;
                    scandocInfo.CREATED_BY = info.statusBy;
                    scandocInfo.CREATED_DATE = modifyDt;
                    inforeturn = scandocInfo;
                    db.Insert(scandocInfo);
                }

    
                transScope.Complete();
            }
            return inforeturn;
        }
        public SCANDOCS DeleteAssetImage(string imageId)
        {
            SCANDOCS ret = null;

            var imageFound = db.SingleOrDefault<SCANDOCS>(PetaPoco.Sql.Builder.Select("*").From("SCANDOCS").Where("SCANDOC_ID=@imageID", new { imageID = imageId }));

            if (imageFound != null)
            {
                ret = imageFound;
                using (var transScope = db.GetTransaction())
                {

                    //apply processed data
                    db.Delete(imageFound);

                    transScope.Complete();
                }
            }
            else
            {
                throw new Exception("Equipment not found.") { Source = this.GetType().Name };
            }
            return ret;
        }
        public List<dynamic> SearchScandocsByAssetImageID(string assetId, string imageId, int maxRows = 50)
        {
            var itemsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchScandocsByAssetimageID(db._dbType.ToString(), assetId, imageId, maxRows)));

            if (itemsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return itemsFound;
        }

        public List<dynamic> SearchScandocsByAssetId(string assetId, int maxRows = 50)
        {
            var itemsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchScandocsByAssetId(db._dbType.ToString(), assetId, maxRows)));

            if (itemsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return itemsFound;
        }
        public List<dynamic> SearchScandocsByCategoryId(string assetId, string categoryId, int maxRows = 500)
        {
            var itemsFound = db.Fetch<dynamic>(PetaPoco.Sql.Builder.Append((string)new IndieSearchScandocsByCategoryId(db._dbType.ToString(), assetId, categoryId, maxRows)));

            if (itemsFound == null)
            {
                throw new Exception("No match found.") { Source = this.GetType().Name };
            }
            return itemsFound;
        }

        public List<ASSET_V> SearchAssetV(string searchKey, int maxRows = 5000)
        {
            if (string.IsNullOrEmpty(searchKey))
                return new List<ASSET_V>();

            return db.Fetch<ASSET_V>(PetaPoco.Sql.Builder.Select("*").From("ASSET_V").Where("(SYSREPORTID is null or SYSREPORTID = 0) and STATUS IN ('I', 'A', 'N') and (MODEL_NO=@searchKey or SERIAL_NO=@searchKey)", new { searchKey = searchKey }));
        }

        public List<ASSET_V> GetAssetVByReport(string sysReportId, int maxRows = 5000)
        {
            return db.Fetch<ASSET_V>(PetaPoco.Sql.Builder.Select("*").From("ASSET_V").Where("SYSREPORTID=@SYSREPORTID", new { SYSREPORTID = sysReportId }));
        }

        public AM_MANUFACTURER CheckDuplicateManufacturer(string manufacturerId, string oldManufacturerId)
        {
            return db.SingleOrDefault<AM_MANUFACTURER>(PetaPoco.Sql.Builder.Select("*").From("AM_MANUFACTURER").Where("MANUFACTURER_ID=@manufacturerId and MANUFACTURER_ID<>@oldManufacturerId", new { manufacturerId = manufacturerId, oldManufacturerId = oldManufacturerId }));
        }
    }

    public class IndieSearchCompanyPR : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 and (am.ref_wo is null or am.ref_wo = '')";
        private string columns = "";
        private string columnsSimple = "";
        private string columnsSql = "AM.REQUEST_ID, AM.STATUS, Convert(varchar(11),AM.REQ_DATE,103) AS REQ_DATE, Convert(varchar(11),AM.PURCHASE_DATE,103) AS PURCHASE_DATE, (SELECT COUNT(ID_PARTS) FROM AM_PARTSREQUEST PR WHERE PR_REQUEST_ID > 0 AND PR_REQUEST_ID = AM.REQUEST_ID ) ITEMREQ, PURCHASE_NO, MF.DESCRIPTION AS SUPPLIER_NAME";
        private string from = "AM_PURCHASEREQUEST AM LEFT OUTER JOIN AM_MANUFACTURER MF ON AM.SUPPLIER_ID = MF.manufacturer_id";

        public IndieSearchCompanyPR(string dbType, string queryString, SearchFilter filter, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;

            string[] queryArray = queryString.Split(new char[] { ' ' });
            var query2 = queryString.ToLower();

            if (!string.IsNullOrWhiteSpace(query2))
            {
                whereClause += " AND (LOWER(AM.STATUS) LIKE ('" + query2 + "%') OR LOWER(AM.PURCHASE_NO) LIKE ('" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("AM.REQ_DATE >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" and AM.REQ_DATE <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }

            if (!string.IsNullOrWhiteSpace(filter.Status))
            {
                whereClause += string.Format(" AND AM.STATUS Like ('{0}%')", filter.Status);

            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + " ORDER BY AM.REQUEST_ID DESC, Convert(varchar(11),AM.REQ_DATE,103) DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + "GROUP BY AM.REQUEST_ID ORDER BY  REQ_DATE DESC" + " LIMIT " + maxRows.ToString() };
            }
        }
    }

    public class IndieSearchCustomerDiscount : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "id, am_part_id, discount_type, discount, validity_date_from, validity_date_to, status";
        private string columnsSimple = "id, am_part_id, discount_type, discount, validity_date_from, validity_date_to, status";
        private string from = "am_prod_customer_discount";

        public IndieSearchCustomerDiscount(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(am_part_id) = ('" + query2 + "')";

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  id desc" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchPartModels : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.ID, AM.ID_PARTS, AM.MODEL_ID, PS.MODEL_NAME, PS.MODEL, AM.CREATED_BY, AM.CREATED_DATE";
        private string columnsSimple = "ID, ID_PARTS, MODEL_ID, MODEL_NAME, MODEL";
        private string from = "AM_PARTS_MODELDETAILS AM LEFT OUTER JOIN AM_MANUFACTURER_MODEL PS ON AM.MODEL_ID = PS.MODEL_ID";

        public IndieSearchPartModels(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.ID_PARTS) = ('" + query2 + "')";
          
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  PS.MODEL_ID ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }


    public class IndieSearchAssetCostCenterWarranty : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.CCID, AM.ASSET_NO, AM.CCFREQUENCY, AM.CCFREQUENCY_PERIOD, AM.CCWARRANTY_EXPIRY,AM.CCWARRANTY_EXTENDED, AM.CCWARRANTY_INCLUDED, AM.CCWARRANTY_NOTES, AM.CREATED_BY, AM.CREATED_DATE";
        private string columnsSimple = "";
        private string from = "AM_ASSET_COSTCENTER_WARRANTY AM";

        public IndieSearchAssetCostCenterWarranty(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR (LOWER(AM.ASSET_NO) = ('" + query2 + "'))";
            whereClause += " AND (AM.ASSET_NO = '" + queryString + "' )";

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  CCID DESC " };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REFID ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndieSearchCostCenterTransactionAsset : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.TRANSID, AM.ASSET_NO, AM.COSTCENTERINSTALLDATE, AM.COSTCENTERCODE, AM.COSTCENTERINVOICENO,AM.COSTCENTERPURCHASEDATE, AM.COSTCENTERPURCHASECOST, AM.COSTCENTERASSETCOND, AM.RETURNEDDATE, AM.RETURNEDREASON,AM.RETURNEDREMARKS, AM.STATUS, AM.RETURNCREATEDBY,AM.RETURNCREATEDDATE";
        private string columnsSimple = "ID, ASSET_NO, FREQUENCY, FREQUENCY_PERIOD, WARRANTY_EXPIRY, WARRANTY_EXTENDED, WARRANTY_INCLUDED, WARRANTY_NOTES, CREATED_BY, CREATED_DATE";
        private string from = "AM_ASSET_TRANSACTION AM";

        public IndieSearchCostCenterTransactionAsset(string dbType, string assetNo, int transId, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            //this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            //  var query2 = queryString.ToLower();
            whereClause += " OR (LOWER(AM.ASSET_NO) = ('" + assetNo + "'))";
            whereClause += " AND (AM.TRANSID = '" + transId + "' )";

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause};
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REFID ASC" + " LIMIT " + maxRows.ToString() };
            }
    }


    }

    public class IndieSearchCostCenterAssetWarranty : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.CCID, AM.TRANSID, AM.ASSET_NO, AM.CCFREQUENCY, AM.CCFREQUENCY_PERIOD, AM.CCWARRANTY_EXPIRY,AM.CCWARRANTY_EXTENDED, AM.CCWARRANTY_INCLUDED, AM.CCWARRANTY_NOTES, AM.CREATED_BY, AM.CREATED_DATE";
        private string columnsSimple = "ID, ASSET_NO, FREQUENCY, FREQUENCY_PERIOD, WARRANTY_EXPIRY, WARRANTY_EXTENDED, WARRANTY_INCLUDED, WARRANTY_NOTES, CREATED_BY, CREATED_DATE";
        private string from = "AM_ASSET_COSTCENTER_WARRANTY AM";

        public IndieSearchCostCenterAssetWarranty(string dbType, string assetNo, int transId, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            //this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
          //  var query2 = queryString.ToLower();
            whereClause += " OR (LOWER(AM.ASSET_NO) = ('" + assetNo + "'))";
            whereClause += " AND (AM.TRANSID = '" + transId + "' )";

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM.CCID DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REFID ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchAssetWarranty : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.ID, AM.ASSET_NO, AM.FREQUENCY, AM.FREQUENCY_PERIOD, AM.WARRANTY_EXPIRY,AM.WARRANTY_EXTENDED, AM.WARRANTY_INCLUDED, AM.WARRANTY_NOTES, AM.CREATED_BY, AM.CREATED_DATE";
        private string columnsSimple = "ID, ASSET_NO, FREQUENCY, FREQUENCY_PERIOD, WARRANTY_EXPIRY, WARRANTY_EXTENDED, WARRANTY_INCLUDED, WARRANTY_NOTES, CREATED_BY, CREATED_DATE";
        private string from = "AM_ASSET_WARRANTY AM";

        public IndieSearchAssetWarranty(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR (LOWER(AM.ASSET_NO) = ('" + query2 + "'))";
            whereClause += " AND (AM.ASSET_NO = '" + queryString + "' )";

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ID ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REFID ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchAssetWarrantyV : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.ID, AM.ASSET_NO, AM.FREQUENCY, AM.FREQUENCY_PERIOD, AM.WARRANTY_EXPIRY,AM.WARRANTY_EXTENDED, AM.WARRANTY_INCLUDED, AM.WARRANTY_NOTES, AM.WARRANTYDESC";
        private string columnsSimple = "ID, ASSET_NO, FREQUENCY, FREQUENCY_PERIOD, WARRANTY_EXPIRY, WARRANTY_EXTENDED, WARRANTY_INCLUDED, WARRANTY_NOTES, CREATED_BY, CREATED_DATE";
        private string from = "ASSET_WARRANTY_V AM";

        public IndieSearchAssetWarrantyV(string dbType, string queryString,  int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR (LOWER(AM.ASSET_NO) = ('" + query2 + "'))";
            whereClause += " AND (AM.ASSET_NO = '" + queryString + "' )";
          
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ID ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REFID ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }


    public class IndieSearchModel : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.MODEL_ID, AM.TYPE, AM.MODEL, AM.MODEL_NAME,AM.CRITICAL,AM.DESCRIPTION,ST.DESCRIPTION DESCRIPTION_MFTR";
        private string columnsSimple = "ID, WO_NOTES, CREATED_DATE";
        private string from = "AM_MANUFACTURER_MODEL AM LEFT JOIN AM_MANUFACTURER ST ON AM.VENDOR = ST.MANUFACTURER_ID";

        public IndieSearchModel(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.MODEL_ID) = LOWER('" + query2 + "')";
            whereClause += " OR LOWER(AM.TYPE) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(AM.MODEL) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(AM.CRITICAL) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(ST.DESCRIPTION) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(ST.MANUFACTURER_ID) LIKE ('" + query2 + "%')";

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  MODEL ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  CREATED_DATE DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchModelManufacturerId : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.MODEL_ID, AM.TYPE, AM.MODEL, AM.MODEL_NAME DESCRIPTION,AM.CRITICAL,ST.DESCRIPTION DESCRIPTION_MFTR";
        private string columnsSimple = "ID, WO_NOTES, CREATED_DATE";
        private string from = "AM_MANUFACTURER_MODEL AM LEFT JOIN AM_MANUFACTURER ST ON AM.VENDOR = ST.MANUFACTURER_ID";

        public IndieSearchModelManufacturerId(string dbType, string queryString, string manufacturerId, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR (LOWER(AM.MODEL_ID) = LOWER('" + query2 + "')";
            whereClause += " OR LOWER(AM.TYPE) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(AM.MODEL) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(AM.CRITICAL) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(ST.DESCRIPTION) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(ST.MANUFACTURER_ID) LIKE ('" + query2 + "%'))";

            whereClause += " AND LOWER(ST.MANUFACTURER_ID) LIKE ('" + manufacturerId + "%')";
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  MODEL ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  CREATED_DATE DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchPurchaseRequest : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "AM.REQUEST_ID,  AM.STATUS, AM.REQ_DATE, COUNT(AM.ID_PARTS) ITEMREQ, REF_WO";
        private string columnsSimple = "AM_PRREQITEMID, DESCRIPTION, QTY, CITY, PRICE, EXTENDED, CONTRACT_NO, REF_WO";
		private string columnsSql = "AM.REQUEST_ID,  AM.STATUS, Convert(varchar(11),AM.REQ_DATE,103) AS REQ_DATE, (SELECT COUNT(ID_PARTS) FROM AM_PARTSREQUEST PR WHERE AM.REF_WO =PR.REF_WO  AND PR_REQUEST_ID > 0 AND PR_REQUEST_ID = AM.REQUEST_ID ) ITEMREQ, AM.REF_WO, LL.DESCRIPTION PROBLEM_TYPE";

		private string from = "AM_PURCHASEREQUEST AM LEFT JOIN AM_WORK_ORDERS WO ON AM.REF_WO = WO.REQ_NO LEFT JOIN LOV_LOOKUPS LL ON WO.REQ_TYPE = LL.LOV_LOOKUP_ID AND LL.CATEGORY ='AIMS_PROBLEMTYPE'";
		//private string groupByClause = "AM.REQUEST_ID ,AM.STATUS, Convert(varchar(11),AM.REQ_DATE,103) ";
        public IndieSearchPurchaseRequest(string dbType, string queryString, string retAction,SearchFilter filter,int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;

			string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
          
            if (!string.IsNullOrWhiteSpace(query2))
            {
                whereClause += " AND (LOWER(AM.STATUS) LIKE ('" + query2 + "%') OR LOWER(AM.REQUEST_ID) LIKE ('" + query2 + "%') OR LOWER(AM.REF_WO) LIKE LOWER('" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("AM.REQ_DATE >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" and AM.REQ_DATE <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }

            if (!string.IsNullOrWhiteSpace(filter.Status))
            {
                whereClause += string.Format(" AND AM.STATUS Like ('{0}%')", filter.Status);

            }





        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause +  " ORDER BY AM.REQUEST_ID DESC, Convert(varchar(11),AM.REQ_DATE,103) DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + "GROUP BY AM.REQUEST_ID ORDER BY  REQ_DATE DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndieSearchApproveOfficerCat : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "L.ID,L.STAFF_ID,L.STATUS,L.STORE_ID, L.APPROVE_TYPE";
        private string columnsSimple = "ID,STAFF_ID,STATUS,STORE_ID";
        private string from = "APPROVING_OFFICER L";

        public IndieSearchApproveOfficerCat(string dbType, string queryString, int maxRows = 50)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });
            
            whereClause += " OR L.APPROVE_TYPE = ('" + queryString + "')";
            whereClause += " AND L.STATUS = ('Y')";
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }
    }



    public class IndieSearchPurchaseRequestRept : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "AM.REQUEST_ID,  AM.STATUS, AM.REQ_DATE,";
        private string columnsSimple = "AM_PRREQITEMID, DESCRIPTION, QTY, CITY, PRICE, EXTENDED, CONTRACT_NO, REF_WO";
        private string columnsSql = "AM.ID_PARTS,AM.PR_REQUEST_QTY, AM.PR_REQUEST_ID, AM.DESCRIPTION, AM.REQ_DATE,AM.PR_REQDATE, AM.REF_WO, AM.PR_STATUS,AM.PO_UNITPRICE,AM.PO_TOTALUNITPRICEITEM,AM.PURCHASE_NO,AM.QTY_PO_REQUEST, AM.PO_STATUS, AM.SUPPLIER, AM.PO_REQDATE";

        private string from = "PURCHASE_REQUEST_V AM";
        //private string groupByClause = "AM.REQUEST_ID ,AM.STATUS, Convert(varchar(11),AM.REQ_DATE,103) ";
        public IndieSearchPurchaseRequestRept(string dbType, string queryString,  SearchFilter filter, int maxRows = 10000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;

            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            if (!string.IsNullOrWhiteSpace(query2))
            {
                whereClause += " AND (LOWER(AM.PR_STATUS) LIKE ('" + query2 + "%') OR LOWER(AM.PR_REQUEST_ID) LIKE ('" + query2 + "%') OR LOWER(AM.DESCRIPTION) LIKE ('" + query2 + "%') OR LOWER(AM.SUPPLIER) LIKE ('" + query2 + "%') OR LOWER(AM.REF_WO) LIKE ('" + query2 + "%') OR LOWER(AM.ID_PARTS) LIKE ('" + query2 + "%') OR LOWER(AM.PURCHASE_NO) LIKE ('" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("AM.REQ_DATE >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" and AM.REQ_DATE <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }

            if (!string.IsNullOrWhiteSpace(filter.Status))
            {
                whereClause += string.Format(" AND AM.PR_STATUS Like ('{0}')", filter.Status);

            }
            if (!string.IsNullOrWhiteSpace(filter.POStatus))
            {
                whereClause += string.Format(" AND AM.PO_STATUS Like ('{0}')", filter.POStatus);

            }
            if (!string.IsNullOrWhiteSpace(filter.REF_WO))
            {
                whereClause += string.Format(" AND AM.REF_WO Like ('{0}')", filter.REF_WO);

            }
            if (!string.IsNullOrWhiteSpace(filter.PR_NO))
            {
                whereClause += string.Format(" AND AM.PR_REQUEST_ID Like ('{0}')", filter.PR_NO);

            }
            if (!string.IsNullOrWhiteSpace(filter.PO_NO))
            {
                whereClause += string.Format(" AND AM.PURCHASE_NO Like ('{0}')", filter.PO_NO);

            }




        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + " ORDER BY AM.REQ_DATE DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + "GROUP BY AM.REQUEST_ID ORDER BY  REQ_DATE DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchPurchaseOrder : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "AM.PURCHASEID,  AM.PURCHASE_NO, AM.STATUS,PURCHASE_DATE, TOTAL_NOITEMS, AM.STATUS, AM.TOTAL_AMOUNT,AM.REF_WO";
		private string columnsSimple = "PURCHASEID, PURCHASE_NO, PURCHASE_DATE, TOTAL_NOITEMS, AM.STATUS, AM.TOTAL_AMOUNT";
		private string columnsSql = "AM.PURCHASEID,  AM.PURCHASE_NO, Convert(varchar(11),AM.PURCHASE_DATE,103) AS PURCHASE_DATE, TOTAL_NOITEMS, AM.STATUS, AM.TOTAL_AMOUNT,AM.REF_WO";

		private string from = "AM_PURCHASEORDER AM";
		
		public IndieSearchPurchaseOrder(string dbType, string queryString, string retAction, int maxRows = 500)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;

			string[] queryArray = queryString.Split(new char[] { ' ' });

			//foreach (string query in queryArray)
			//{
			var query2 = queryString.ToLower();

			whereClause += " OR LOWER(AM.PURCHASE_NO) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.REF_WO) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.STATUS) LIKE ('" + query2 + "%')";

			if (retAction != null && retAction != "")
			{

			}





		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + " ORDER BY AM.PURCHASE_NO DESC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + "GROUP BY AM.REQUEST_ID ORDER BY  REQ_DATE DESC" + " LIMIT " + maxRows.ToString() };
			}
		}


	}
	public class IndieSearchPurchaseRequestApproved : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.REQUEST_ID,  AM.STATUS, AM.REQ_DATE, COUNT(AM.ID_PARTS) ITEMREQ";
        private string columnsSimple = "AM_PRREQITEMID, DESCRIPTION, QTY, CITY, PRICE, EXTENDED, CONTRACT_NO, REF_WO";
        private string from = "AM_PURCHASEREQUEST AM";

        public IndieSearchPurchaseRequestApproved(string dbType, string queryString, string retAction, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();

           
            whereClause += " OR LOWER(AM.STATUS ='APPROVED')";

            if (retAction != null && retAction != "")
            {

            }





        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + "GROUP BY AM.REQUEST_ID ORDER BY  REQ_DATE DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + "GROUP BY AM.REQUEST_ID ORDER BY  REQ_DATE DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }

	public class IndieSearchPOItemsApproved : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "AM.PURCHASE_NO,  AM.REQDATE, AM.REF_WO,AM.STATUS,AM.ITEMSREQ,AM.DESCRIPTION,AM.SUPPLIER_ID";
		private string columnsSimple = "PURCHASE_NO, REQDATE, REF_WO, STATUS, ITEMSREQ, DESCRIPTION, SUPPLIER_ID";
		private string from = "PURCHASEORDER_FORRECEIVE_V AM";

		public IndieSearchPOItemsApproved(string dbType, string queryString, string retAction, int maxRows = 500)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			//foreach (string query in queryArray)
			//{
			var query2 = queryString.ToLower();
			whereClause += " OR AM.STATUS IS NOT NULL";
			if (retAction != null && retAction != "")
			{

			}





		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM.REQDATE ASC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + "GROUP BY AM.REQUEST_ID ORDER BY  REQ_DATE DESC" + " LIMIT " + maxRows.ToString() };
			}
		}


	}

	public class IndieSearchPurchaseRequestApprovedByProdId : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "AM.AM_PRREQITEMID, AM.REQUEST_ID,  AM.STATUS, AM.REQ_DATE, AM.ID_PARTS, AM.QTY, AM.PURCHASE_UNITPRICE, AM.PURCHASE_QTY, ISNULL(AM.QTY_RECEIVED,0) AS QTY_RECEIVED";
		private string columnsSimple = "AM.AM_PRREQITEMID, REQUEST_ID, STATUS, REQ_DATE, ID_PARTS, QTY, PURCHASE_UNITPRICE, PURCHASE_QTY";
		private string from = "AM_PURCHASEREQUEST AM";

		public IndieSearchPurchaseRequestApprovedByProdId(string dbType, string queryString, string prodId, string purchaseNo, int maxRows = 500)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			//foreach (string query in queryArray)
			//{
			var query2 = queryString.ToLower();


			whereClause += " OR (LOWER(AM.ID_PARTS) ='"+ prodId +"')";

			if (purchaseNo != null && purchaseNo != "")
			{
				whereClause += " AND LOWER(AM.PURCHASE_NO) ='" + purchaseNo + "'";
				whereClause += " AND (PO_STATUS ='Order' OR PO_STATUS ='PARTIAL RECEIVED')";
			}
			whereClause += " AND AM.STATUS ='APPROVED' ";





		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_DATE  ASC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + "GROUP BY AM.REQUEST_ID ORDER BY  REQ_DATE DESC" + " LIMIT " + maxRows.ToString() };
			}
		}


	}
    public class IndieSearchPurchaseRequestItemsApproved : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.REF_WO, AM.REF_ASSETNO,  AM.STATUS, AM.ID_PARTS, AM.PR_REQUEST_QTY, AM.DESCRIPTION, AM.REQ_DATE, AM.REQUEST_ID, AM.STORE_ID, AM.UOM_DESC";
        private string columnsSimple = "AM.REF_WO, REF_ASSETNO, STATUS, ID_PARTS, PR_REQUEST_QTY, DESCRIPTION, REQ_DATE, REQUEST_ID";
        private string from = "PURCHASE_REQUEST_ITEMS_V AM";

        public IndieSearchPurchaseRequestItemsApproved(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();

            whereClause += " OR LOWER(AM.REF_WO) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.ID_PARTS) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.REQUEST_ID) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('%" + query2 + "%')";





        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_DATE  ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + "GROUP BY AM.REQUEST_ID ORDER BY  REQ_DATE DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchPurchaseRequestItemsInfo : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.REF_WO, AM.REF_ASSETNO,  AM.STATUS, AM.ID_PARTS, AM.PR_REQUEST_QTY, AM.DESCRIPTION, AM.REQ_DATE, AM.REQUEST_ID";
        private string columnsSimple = "AM.REF_WO, REF_ASSETNO, STATUS, ID_PARTS, PR_REQUEST_QTY, DESCRIPTION, REQ_DATE, REQUEST_ID";
        private string from = "PURCHASE_REQUEST_ITEMS_V AM";

        public IndieSearchPurchaseRequestItemsInfo(string dbType, string queryString, string prodId, string prNo, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();


            whereClause += " OR (LOWER(AM.ID_PARTS) ='" + prodId + "')";
            whereClause += " AND LOWER(AM.REQUEST_ID) ='" + prNo + "'";
         




        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_DATE  ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + "GROUP BY AM.REQUEST_ID ORDER BY  REQ_DATE DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchWOPMPartsById : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.DESCRIPTION, AM.ID_PARTS, AM.QTY, AM.PRICE_ITEM, AM.REFID,AM.REF_TYPE";
        private string columnsSimple = "DESCRIPTION, ID_PARTS, QTY, PRICE_ITEM, REFID, REF_TYPE";
        private string from = "AM_WOPM_PARTS AM";

        public IndieSearchWOPMPartsById(string dbType, string queryString, string reftype,string refid, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR (LOWER(AM.REFID) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.REF_TYPE) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(AM.ID_PARTS) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.QTY) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('%" + query2 + "%'))";

            if (reftype != null && reftype != "")
            {
                whereClause += " AND (AM.REF_TYPE in  ('" + reftype + "') )";
                whereClause += " AND (AM.REFID =" + refid + " )";
            }


        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REFID ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REFID ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchWOPMParts : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.DESCRIPTION, AM.ID_PARTS, AM.QTY, AM.PRICE_ITEM, AM.REFID,AM.REF_TYPE";
        private string columnsSimple = "DESCRIPTION, ID_PARTS, QTY, PRICE_ITEM, REFID, REF_TYPE";
        private string from = "AM_WOPM_PARTS AM";

        public IndieSearchWOPMParts(string dbType, string queryString, string reftype, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " (OR LOWER(AM.REFID) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.REF_TYPE) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(AM.ID_PARTS) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.QTY) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('%" + query2 + "%'))";

            if (reftype != null && reftype != "")
            {
                whereClause += " AND (LL.REF_TYPE in'" + reftype + "' )";
            }


        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REFID ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REFID ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchPartSP : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.PARTSP_ID, AM.CUSTOMER_NO, AM.PREFERENCE, AM.PART_NUMBER, AM.PART_DESCRIPTION,AM.RETURN_ADDRESS, ST.DESCRIPTION,AM.EXCHANGEABLE,AM.SUPPLIER_ID";
        private string columnsSimple = "PARTSP_ID, CUSTOMER_NO, PREFERENCE, PART_NUMBER, PART_DESCRIPTION, RETURN_ADDRESS";
        private string from = "AM_PARTSUPPLIERDETAILS AM INNER JOIN AM_SUPPLIER ST ON AM.SUPPLIER_ID = ST.SUPPLIER_ID";

        public IndieSearchPartSP(string dbType, string queryString, string partId, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR (LOWER(AM.PARTSP_ID) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.PART_DESCRIPTION) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(ST.DESCRIPTION) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(AM.CUSTOMER_NO) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.PART_NUMBER) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.RETURN_ADDRESS) LIKE ('%" + query2 + "%'))";

            if (partId != null && partId != "")
            {
                whereClause += " AND (AM.ID_PARTS ='" + partId + "' )";
            }


        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  PREFERENCE ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  PREFERENCE ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchAmPartsActive2 : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "AM.ID_PARTS as ID_PARTS, AM.ID_PARTS as PRODUCT_CODE, AM.DESCRIPTION as DESCRIPTION, L.DESCRIPTION as AM_UOM, AM.PART_COST as PRICE";
        private string columnsSimple = "ID_PARTS, DESCRIPTION, ANCILLARY, CATEGORY, ALTERNATIVE_ITEM, FACILITY";
        private string from = "AM_PARTS AM LEFT JOIN LOV_LOOKUPS L ON AM.UOM_ID = L.LOV_LOOKUP_ID";

        public IndieSearchAmPartsActive2(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            var query2 = queryString.ToLower();
            whereClause += " AND AM.STATUS = 'Active' AND (LOWER(AM.ID_PARTS) LIKE ('" + query2 + "%') OR LOWER(AM.DESCRIPTION) LIKE ('%" + query2 + "%'))";
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchParts : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.PART_ID, AM.ID_PARTS, AM.DESCRIPTION, AM.ANCILLARY, AM.CATEGORY, AM.ALTERNATIVE_ITEM,AM.FACILITY,AM.STATUS, PS.PART_NUMBERS";
        private string columnsSimple = "ID_PARTS, DESCRIPTION, ANCILLARY, CATEGORY, ALTERNATIVE_ITEM, FACILITY";
        private string from = "AM_PARTS AM LEFT OUTER JOIN PARTNO_BYPARTID_V PS ON AM.ID_PARTS = PS.ID_PARTS";

        public IndieSearchParts(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.ID_PARTS) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(PS.PART_NUMBERS) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(AM.ANCILLARY) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(AM.CATEGORY) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(AM.ALTERNATIVE_ITEM) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.FACILITY) LIKE ('" + query2 + "%')";



        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }

	public class IndieSearchRiskFactorSetup : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "AM.RISK_FACTORCODE, AM.RISK_DESCRIPTION, AM.RISK_CATEGORYCODE, AM.RISK_SCORE, ST.RISK_CATEGORY";
		private string columnsSimple = "RISK_FACTORCODE, RISK_DESCRIPTION, RISK_CATEGORYCODE, RISK_SCORE, RISK_CATEGORY, RISK_CATEGORYCODE";
		private string from = "RISK_CATEGORYFACTOR AM LEFT JOIN RISK_CATEGORYGROUP ST ON AM.RISK_CATEGORYCODE = ST.RISK_CATEGORYCODE";

		public IndieSearchRiskFactorSetup(string dbType, string queryString, string reftype, int maxRows = 500)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			//foreach (string query in queryArray)
			//{
			var query2 = queryString.ToLower();
			whereClause += " OR (LOWER(AM.RISK_FACTORCODE) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.RISK_DESCRIPTION) LIKE ('%" + query2 + "%')";
			whereClause += " OR LOWER(AM.RISK_CATEGORYCODE) LIKE ('%" + query2 + "%')";
			whereClause += " OR LOWER(AM.RISK_SCORE) LIKE ('%" + query2 + "%')";
			whereClause += " OR LOWER(ST.RISK_CATEGORY) LIKE ('%" + query2 + "%')";
			whereClause += " OR LOWER(ST.RISK_CATEGORYCODE) LIKE ('" + query2 + "%'))";


			if (reftype != null && reftype != "")
			{
				whereClause += " AND (AM.RISK_CATEGORYCODE = '" + reftype + "' )";
			}

		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  RISK_DESCRIPTION ASC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  RISK_DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
			}
		}


	}
	public class IndieSearchRiskFactorGroup : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "AM.RISK_CATEGORYCODE, AM.RISK_CATEGORY, AM.RISK_GROUP";
		private string columnsSimple = "RISK_CATEGORYCODE, RISK_CATEGORY, RISK_GROUP";
		private string from = "RISK_CATEGORYGROUP AM";

		public IndieSearchRiskFactorGroup(string dbType, string queryString, int maxRows = 500)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			//foreach (string query in queryArray)
			//{
			var query2 = queryString.ToLower();
			whereClause += " OR LOWER(AM.RISK_CATEGORYCODE) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.RISK_CATEGORY) LIKE ('%" + query2 + "%')";
			whereClause += " OR LOWER(AM.RISK_GROUP) LIKE ('%" + query2 + "%')";




		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  RISK_CATEGORY ASC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  RISK_CATEGORY ASC" + " LIMIT " + maxRows.ToString() };
			}
		}


	}

	public class IndieSearchPartsActive : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.PART_ID, AM.ID_PARTS, AM.DESCRIPTION, AM.ANCILLARY, AM.CATEGORY, AM.ALTERNATIVE_ITEM,AM.FACILITY";
        private string columnsSimple = "ID_PARTS, DESCRIPTION, ANCILLARY, CATEGORY, ALTERNATIVE_ITEM, FACILITY";
        private string from = "AM_PARTS AM";

        public IndieSearchPartsActive(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR (LOWER(AM.ID_PARTS) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(AM.ANCILLARY) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(AM.CATEGORY) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(AM.ALTERNATIVE_ITEM) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.FACILITY) LIKE ('" + query2 + "%'))";

            whereClause += " AND (LL.STATUS ='Active' )";

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchScandocsByAssetId : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "(1=2 ";
        private string columns = "RD.SCANDOCBKG_DATA, RD.NOTES, RD.CATEGORY,RD.SCANDOC_ID, RD.REF_ID,FILE_NAME, CREATED_DATE,FILE_TYPE";
        private string columnsSimple = "SCANDOCBKG_DATA,NOTES, CATEGORY,SCANDOC_ID, PATIENT_ID,FILE_NAME,CREATED_DATE";
        private string from = " SCANDOCS RD";

        public IndieSearchScandocsByAssetId(string dbType, string queryString, int maxRows = 6000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });
            var assetId = queryString;
            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(RD.REF_ID) = ('" + query2 + "'))";
            }

            whereClause += ") AND (RD.REF_ID='" + assetId + "')";

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY CLINIC_DESCRIPTION " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY CREATED_DATE DESC " };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY CREATED_DATE DESC " + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndieSearchScandocsByCategoryId : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "(1=2 ";
        private string columns = "RD.SCANDOCBKG_DATA, RD.NOTES, RD.CATEGORY,RD.SCANDOC_ID, RD.REF_ID,FILE_NAME, CREATED_DATE";
        private string columnsSimple = "SCANDOCBKG_DATA,NOTES, CATEGORY,SCANDOC_ID, PATIENT_ID,FILE_NAME,CREATED_DATE";
        private string from = " SCANDOCS RD";

        public IndieSearchScandocsByCategoryId(string dbType, string queryString,string categoryId, int maxRows = 6000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });
            var assetId = queryString;
            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(RD.REF_ID) = ('" + query2 + "'))";
            }

            whereClause += ") AND (RD.REF_ID='" + assetId + "')";
            if (categoryId != null || categoryId!= "")
            {
                whereClause += " AND (RD.CATEGORY='" + categoryId + "')";
            }
           

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY CLINIC_DESCRIPTION " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY CREATED_DATE DESC " };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY CREATED_DATE DESC " + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchScandocsByAssetimageID : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "(1=2 ";
        private string columns = "RD.SCANDOCBKG_DATA, RD.NOTES, RD.CATEGORY,RD.SCANDOCS_DATA,RD.SCANDOC_ID, RD.REF_ID,FILE_NAME, CREATED_DATE";
        private string columnsSimple = "SCANDOCBKG_DATA,NOTES, CATEGORY,SCANDOCS_DATA,SCANDOC_ID, PATIENT_ID,FILE_NAME,CREATED_DATE";
        private string from = " SCANDOCS RD";

        public IndieSearchScandocsByAssetimageID(string dbType, string queryString, string imageId, int maxRows = 6000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(RD.REF_ID) = ('" + query2 + "'))";
            }

            whereClause += ") AND (RD.SCANDOC_ID='" + imageId + "')";

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY CLINIC_DESCRIPTION " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY CREATED_DATE DESC " };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY CREATED_DATE DESC " + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchAssetContractService : IndieSql
    {
        private int maxRows = 5000;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "LL.ASSET_CONTRACTSERVICE_ID, LL.ASSET_NO, LL.CONTRACT_ID, LL.PROVIDER, LL.TYPE_COVERAGE,LL.TERM_EFFECTIVE, LL.TERM_TERMINATION, LL.REMARKS, LL.COVERAGE_LABOR, LL.COVERAGE_PARTSMATERIALS, LL.PURCHASE_ORDER, LL.TERM_REVIEW, LL.COST_TOTAL, LL.COST_PERPIECE, LL.COST_LASTPERIOD, LL.LABOR_RATEPERHOUR, LL.LABOR_HOURS";
        private string columnsSimple = "ASSET_CONTRACTSERVICE_ID, ASSET_NO, CONTRACT_ID,PROVIDER,TYPE_COVERAGE,TERM_EFFECTIVE,TERM_TERMINATION,REMARKS,COVERAGE_LABOR,COVERAGE_PARTSMATERIALS,PURCHASE_ORDER, TERM_REVIEW";
        private string from = "AM_ASSET_CONTRACTSERVICES LL";

        public IndieSearchAssetContractService(string dbType, string queryString, string queryAssetNo, int maxRows = 5000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(LL.ASSET_NO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(LL.PROVIDER) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(LL.TYPE_COVERAGE) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(LL.ASSET_CONTRACTSERVICE_ID) LIKE ('" + query2 + "%'))";

            }
            if (queryAssetNo != null && queryAssetNo != "")
            {
                whereClause += " AND (LL.ASSET_NO ='" + queryAssetNo + "' )";
            }

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY DESCRIPTION " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchPMSchedule : IndieSql
    {
        private int maxRows = 5000;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "LL.AM_ASSET_PMSCHEDULE_ID, LL.ASSET_NO, LL.PM_PROCEDURE, LL.PM_DESCRIPTION, LL.SERVICE_DEPARTMENT, LL.SRVDEPT_DESC, LL.FREQUENCY_PERIOD, LL.INTERVAL, LL.NEXT_PM_DUE, LL.GRACE_PERIOD_TYPE, LL.GRACE_PERIOD, LL.SESSION_START, LL.SESSION_END, LL.PM_STATUS, LL.STATUS";
        private string columnsSimple = "AM_ASSET_PMSCHEDULE_ID, ASSET_NO, PM_PROCEDURE,SERVICE_DEPARTMENT,EMPLOYEE_TYPE,EMPLOYEE_INFO,FREQUENCY_PERIOD,INTERVAL,NEXT_PM_DUE,GRACE_PERIOD_TYPE,GRACE_PERIOD, PM_PRIORITY, PM_STATUS";
        private string from = "PM_SCHEDULE_V LL";

        public IndieSearchPMSchedule(string dbType, string queryString, string queryAssetNo, int maxRows = 5000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(LL.PM_PROCEDURE) LIKE ('" + query2 + "%')";
				whereClause += " OR LOWER(LL.PM_DESCRIPTION) LIKE ('" + query2 + "%')";
				whereClause += " OR LOWER(LL.SERVICE_DEPARTMENT) LIKE ('" + query2 + "%')";
				whereClause += " OR LOWER(LL.SRVDEPT_DESC) LIKE ('" + query2 + "%'))";

            }
            if (queryAssetNo != null && queryAssetNo != "")
            {
                whereClause += " AND (LL.ASSET_NO ='" + queryAssetNo + "' )";
            }
            
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY DESCRIPTION " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndieSearchWOLabourAct : IndieSql
    {
        private int maxRows = 5000;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "LL.AM_WORKORDER_LABOUR_ID, LL.RESPONSE, LL.LABOUR_ACTION, LL.EMPLOYEE, LL.SUPPLIER, LL.HOURS, LL.REQ_NO, LL.LABOUR_DATE , LL.BILLABLE";
        private string columnsSimple = "AM_WORKORDER_LABOUR_ID, ASSET_NO, REQ_NO,EMPLOYEE,LABOUR_TYPE,RESPONSE";
        private string from = "WORKORDER_ACT_V LL";

        public IndieSearchWOLabourAct(string dbType, string queryString, string queryReqNo, int maxRows = 5000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(LL.AM_WORKORDER_LABOUR_ID) LIKE ('" + query2 + "%')";             
                whereClause += " OR LOWER(LL.RESPONSE) LIKE ('%" + query2 + "%'))";

            }
            if (queryReqNo != null && queryReqNo != "")
            {
                whereClause += " AND (LL.REQ_NO ='" + queryReqNo + "' )";
            }

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY DESCRIPTION " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY LL.AM_WORKORDER_LABOUR_ID DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndieSearchWOLabourLOV : IndieSql
    {
        private int maxRows = 5000;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "LL.AM_WORKORDER_LABOUR_ID, LL.ASSET_NO, LL.REQ_NO, LL.EMPLOYEE, LL.LABOUR_TYPE, LL.RESPONSE,  LL.LABOUR_ACTION ,LL.RESPONSE_NAME, LL.REF_DESC";
        private string columnsSimple = "AM_WORKORDER_LABOUR_ID, ASSET_NO, REQ_NO,EMPLOYEE,LABOUR_TYPE,RESPONSE";
        private string from = "WORK_ORDER_LABOUR_V LL";

        public IndieSearchWOLabourLOV(string dbType, string queryString, string queryReqNo, int maxRows = 5000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(LL.AM_WORKORDER_LABOUR_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(LL.REQ_NO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(LL.EMPLOYEE) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(LL.LABOUR_TYPE) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(LL.RESPONSE) LIKE ('%" + query2 + "%'))";

            }
            if (queryReqNo != null && queryReqNo != "")
            {
                whereClause += " AND (LL.REQ_NO ='" + queryReqNo + "' )";
            }

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY DESCRIPTION " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY LL.AM_WORKORDER_LABOUR_ID" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndieSearchWOLabour : IndieSql
    {
        private int maxRows = 5000;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "LL.AM_WORKORDER_LABOUR_ID, LL.ASSET_NO, LL.REQ_NO, LL.EMPLOYEE, LL.LABOUR_TYPE, LL.RESPONSE, LL.CONTRACT_NO, LL.SUPPLIER_ID, LL.LABOUR_ACTION";
        private string columnsSimple = "AM_WORKORDER_LABOUR_ID, ASSET_NO, REQ_NO,EMPLOYEE,LABOUR_TYPE,RESPONSE";
        private string from = "AM_WORKORDER_LABOUR LL";

        public IndieSearchWOLabour(string dbType, string queryString, string queryReqNo, int maxRows = 5000)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            foreach (string query in queryArray)
            {
                var query2 = query.ToLower();
                whereClause += " OR (LOWER(LL.AM_WORKORDER_LABOUR_ID) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(LL.REQ_NO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(LL.EMPLOYEE) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(LL.LABOUR_TYPE) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(LL.RESPONSE) LIKE ('%" + query2 + "%'))";

            }
            if (queryReqNo != null && queryReqNo != "")
            {
                whereClause += " AND (LL.REQ_NO ='" + queryReqNo + "' )";
            }

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY DESCRIPTION " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndieSearchWorkOrderAssign : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "(ROW_NUMBER() OVER (ORDER BY AM.REQ_DATE DESC)) AS ROWNUMID, AM.REQ_NO, AM.REQ_BY, AM.REQ_DATE, AM.PROBLEM,AM.WO_STATUS, AM.PROBTYPEDESC, AM.ASSET_NAME,   AM.ASSIGN_TO, AM.ASSIGN_DATE,AM.COSTCENTER_NAME, AM.ASSIGN_TYPE";
        private string columnsSimple = "REQ_NO, REQ_NO, REQ_BY, REQ_DATE, PROBLEM, STATUS, REQ_TYPE, ASSET_NO,JOB_TYPE, JOB_URGENCY";
        private string from = "WORKORDER_PENDING_V AM";

        public IndieSearchWorkOrderAssign(string dbType, string queryString,  SearchFilter filter,  int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            if (string.IsNullOrWhiteSpace(filter.assignTo))
            {
                whereClause += "  AND (AM.ASSIGN_TO IS NULL " + " OR AM.ASSIGN_TO = '') ";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(filter.assignTo))
                {
                    whereClause += string.Format(" AND AM.ASSIGN_TO = '{0}'", filter.assignTo);

                }
            }
                

          
                if (!string.IsNullOrWhiteSpace(filter.FromDate))
                {
                    var whereDate = string.Format("AM.REQ_DATE >= '{0}'", filter.FromDate);
                    if (!string.IsNullOrWhiteSpace(filter.ToDate))
                    {
                        whereDate += string.Format(" and AM.REQ_DATE <= '{0}'", filter.ToDate);
                    }
                    whereClause += " AND (" + whereDate + ")";
                }
               
                if (!string.IsNullOrWhiteSpace(filter.ProblemType))
                {
                    whereClause += string.Format(" AND AM.REQ_TYPE = '{0}'", filter.ProblemType);

                }
               
                if (!string.IsNullOrWhiteSpace(filter.assignType))
                {
                    whereClause += string.Format(" AND AM.ASSIGN_TYPE = '{0}'", filter.assignType);

                }


        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY AM.REQ_DATE DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }


    public class IndieSearchWorkOrder : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "(ROW_NUMBER() OVER (ORDER BY AM.REQ_DATE DESC)) AS ROWNUMID, AM.REQ_NO, AM.REQ_BY, AM.REQ_DATE, AM.PROBLEM,AM.WO_STATUS, AM.REQ_TYPE, AM.ASSET_NO, AM.JOB_TYPE, AM.JOB_URGENCY, AM.DESCRIPTION, AM.AM_INSPECTION_ID, AM.INSPECTION_TYPE,AM.SPAREPARTS, AM.MATERIALS_REMARKS, AM.JOB_DUE_DATE, AM.COSTCENTERNAME, AM.SUPPIERNAME,AM.STAFF_FULLNAME, AM.SERIAL_NO";
        private string columnsSimple = "REQ_NO, REQ_NO, REQ_BY, REQ_DATE, PROBLEM, STATUS, REQ_TYPE, ASSET_NO,JOB_TYPE, JOB_URGENCY";
        private string from = "WORKORDERS_V AM";

        public IndieSearchWorkOrder(string dbType, string queryString, string queryAssetNo, SearchFilter filter, string searchStatus, string searchbyStaffId, string userId, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            if (!string.IsNullOrWhiteSpace(query2))
            {
				whereClause += "  AND (LOWER(AM.REQ_NO) LIKE ('" + query2 + "%') " +
				" OR LOWER(AM.REQ_BY) LIKE ('" + query2 + "%')" +
				" OR LOWER(AM.PROBLEM) LIKE ('" + query2 + "%')" +
				" OR LOWER(AM.REQ_TYPE) LIKE ('" + query2 + "%')" +
				" OR LOWER(AM.ASSET_NO) LIKE ('" + query2 + "%')" +
                " OR LOWER(AM.COSTCENTERNAME) LIKE ('%" + query2 + "%')" +
                " OR LOWER(AM.SUPPIERNAME) LIKE ('%" + query2 + "%')" +
                " OR LOWER(AM.SERIAL_NO) LIKE ('%" + query2 + "%')" +
                " OR LOWER(AM.DESCRIPTION) LIKE ('%" + query2 + "%'))";
            }
          
            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("AM.REQ_DATE >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" and AM.REQ_DATE <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }

            if (!string.IsNullOrWhiteSpace(filter.overDue))
            {

                //  var whereDate =

                whereClause += string.Format("  AND AM.JOB_DUE_DATE < '" + filter.overDue + "' AND (CONVERT(varchar(11), AM.JOB_DUE_DATE, 105) <> '01-01-1900') ");
            }
            if (filter.unAssigned == true)
            {
                whereClause += " AND (AM.ASSIGN_TO IS NULL OR AM.ASSIGN_TO = '') ";
            }

           
            if (filter.Status == "PSOP")
            {
                whereClause += " AND (AM.WO_STATUS in ('NE','OP') )";
            }else if(filter.Status == "PSOPFC")
            {
                whereClause += " AND (AM.WO_STATUS in ('NE','OP','FC') )";
            }
            else
             if (filter.Status== "OpHa")
            {
                whereClause += " AND (AM.WO_STATUS in ('NE','OP','HA') )";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(filter.Status))
                {
                    whereClause += string.Format(" AND AM.WO_STATUS = ('{0}')", filter.Status);
                }
                   
            }

            if(!string.IsNullOrWhiteSpace(filter.ProblemType))
            {
                if (filter.ProblemType  == "Oth")
                    whereClause += string.Format(" AND (AM.REQ_TYPE not in ('pm','cm','in'))");
                else
                {
                    whereClause += string.Format(" AND AM.REQ_TYPE = '{0}'", filter.ProblemType);
                }
            }
            if (!string.IsNullOrWhiteSpace(filter.assignTo))
            {
                whereClause += string.Format(" AND LOWER(AM.STAFF_FULLNAME) LIKE LOWER('%" + filter.assignTo+"%')");

            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += string.Format(" AND LOWER(AM.COSTCENTERNAME) LIKE LOWER('%" + filter.CostCenterStr + "%')");

            }

            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += string.Format(" AND LOWER(AM.SUPPIERNAME) LIKE LOWER('%" + filter.SupplierStr + "%')");

            }

            if (!string.IsNullOrWhiteSpace(filter.assignType))
            {
                whereClause += string.Format(" AND AM.ASSIGN_TYPE = '{0}'", filter.assignType);

            }
            if (!string.IsNullOrWhiteSpace(filter.Priority))
            {
                whereClause += string.Format(" AND AM.JOB_URGENCY = '{0}'", filter.Priority);

            }
            if (!string.IsNullOrWhiteSpace(filter.JobType))
            {
                whereClause += string.Format(" AND AM.JOB_TYPE = '{0}'", filter.JobType);

            }




            if (queryAssetNo != null && queryAssetNo != "")
			{
				whereClause += " AND (AM.ASSET_NO ='" + queryAssetNo + "' )";
			}

            if (!string.IsNullOrWhiteSpace(filter.pmtype))
            {
                whereClause += " AND (AM.PM_TYPE ='" + filter.pmtype + "' )";
            }
            if (searchbyStaffId != null && searchbyStaffId != "")
			{
				whereClause += " AND (AM.REQ_BY ='" + searchbyStaffId + "' OR AM.CREATED_BY ='" + searchbyStaffId + "' OR AM.ASSIGN_TO ='" + searchbyStaffId + "')";
			}


            if (!string.IsNullOrWhiteSpace(filter.userConstCenter))
            {
                if (filter.userConstCenter != null)
                {
                    whereClause += string.Format(" AND AM.COST_CENTER in (SELECT CODE FROM STAFF_COSTCENTER_BUILDING WHERE UCATEGORY='AIMS_COSTCENTER' AND STAFFCODE ='" + filter.userConstCenter + "')");
                }
    

            }


            if (!string.IsNullOrWhiteSpace(filter.staffStatus))
            {
                if(filter.staffStatus == "A")
                {
                    whereClause += string.Format(" AND AM.ASSIGN_TO in (SELECT STAFF_ID FROM STAFF WHERE STATUS = 'ACTIVE')");
                }
                else if (filter.staffStatus == "IA")
                {
                    whereClause += string.Format(" AND AM.ASSIGN_TO in (SELECT STAFF_ID FROM STAFF WHERE STATUS = 'INACTIVE')");
                }
                

            }

            if (userId != null && userId != "")
            {
                whereClause += string.Format(" AND AM.BUILDING in (SELECT BUILDING_CODE FROM USER_BUILDING WHERE USER_ID ='" + userId + "')");
            }


        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY AM.REQ_DATE DESC" };
				}
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchWorkOrderStatus : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = " CONVERT(varchar(16), AM.RECORDED_DATE, 103) + ' ' + CONVERT(varchar(5),AM.RECORDED_DATE, 114) AS RECORDED_DATE, AM.WO_STATUS, ST.FIRST_NAME, ST.LAST_NAME";
        private string columnsSimple = "REQ_NO, REQ_NO, REQ_BY, REQ_DATE, PROBLEM, STATUS, REQ_TYPE, ASSET_NO,JOB_TYPE, JOB_URGENCY";
        private string from = "AM_WORK_ORDERS_STATUS_LOG AM LEFT JOIN STAFF ST ON AM.STAFF_ID = ST.STAFF_ID";

        public IndieSearchWorkOrderStatus(string dbType, string queryString, string reqId, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            if (!string.IsNullOrWhiteSpace(query2))
            {
                whereClause += "  AND (LOWER(AM.WO_STATUS) LIKE ('" + query2 + "%')) ";


            }

            if (reqId != null && reqId != "")
            {
                whereClause += " AND (AM.REQ_NO ='" + reqId + "' )";
            }
         
         
        

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY AM.RECORDED_DATE DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_NO DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieReadAssetsByProductCode : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "AM.ASSET_NO, AM.DESCRIPTION, AM.PRODUCT_CODE, AM.MANUFACTURER, M.DESCRIPTION AS MANUFACTURERNAME, AM.MODEL_NAME";
        private string columnsSimple = "ASSET_NO, DESCRIPTION, CATEGORY, MANUFACTURER, SERIAL_NO, MODEL_NO, MODEL_NAME, BUILDING,COST_CENTER, LOCATION, CONDITION,STATUS";
        private string from = "AM_ASSET_DETAILS AM LEFT JOIN AM_MANUFACTURER M ON AM.MANUFACTURER = M.MANUFACTURER_ID";

        public IndieReadAssetsByProductCode(string dbType, string productCode, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.maxRows = maxRows;

            whereClause += "  AND AM.PRODUCT_CODE = '" + productCode + "' ";

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause }; //" WHERE " + whereClause + +  " ORDER BY  DESCRIPTION ASC"
    }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ASSET_NO, DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchAsset : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "   (ROW_NUMBER() OVER (ORDER BY AM.description ASC)) AS ROWNUMID,AM.DESCRIPTION, AM.ASSET_NO, AM.CATEGORY, AM.CATEGORY_DESC, AM.MANUFACTURER, AM.SERIAL_NO,AM.MODEL_NO, AM.MODEL_NAME, AM.BUILDING, AM.COST_CENTER, AM.LOCATION, AM.CONDITION, AM.STATUS, AM.RESPCENTER_DESC, AM.COSTCENTER_DESC, AM.CLONE_ID, AM.SUPPLIERNAME, AM.MANUFACTURERNAME, AM.BUILDING_CODE,AM.PURCHASE_COST,AM.RETURNEDREASON, AM.PRODUCT_CODE";
        private string columnsSimple = "ASSET_NO, DESCRIPTION, CATEGORY, MANUFACTURER, SERIAL_NO, MODEL_NO, MODEL_NAME, BUILDING,COST_CENTER, LOCATION, CONDITION,STATUS";
        private string from = "ASSET_V AM";

        public IndieSearchAsset(string dbType, string queryString, SearchFilter filter, string userId, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            if (!string.IsNullOrWhiteSpace(query2))
            {
                
                whereClause += " AND (LOWER(AM.ASSET_NO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(AM.SERIAL_NO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.MODEL_NO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.SERIAL_NO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.MODEL_NAME) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.BUILDING) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.LOCATION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.CONDITION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.STATUS) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.COSTCENTER_DESC) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(AM.SUPPLIERNAME) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(AM.RESPCENTER_DESC) LIKE ('%" + query2 + "%'))";
            }
           
            if (!string.IsNullOrWhiteSpace(filter.Status))
            {
                whereClause += string.Format(" AND AM.STATUS = ('{0}')", filter.Status);

            }

            if (!string.IsNullOrWhiteSpace(filter.groupCode))
            {
                whereClause += string.Format(" AND AM.GROUP_CODE = ('{0}')", filter.groupCode);

            }

            if (!string.IsNullOrWhiteSpace(filter.CloneId))
            {
                whereClause += string.Format(" AND AM.CLONE_ID = ('{0}')", filter.CloneId);

            }
            if (!string.IsNullOrWhiteSpace(filter.PO_NO))
            {
                //whereClause += string.Format(" AND AM.PURCHASE_NO = ('{0}')", filter.PO_NO);
                whereClause += " AND AM.PURCHASE_NO like '%"+ filter.PO_NO +"%'";
            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += string.Format(" AND LOWER(AM.COSTCENTER_DESC) LIKE LTrim('%" + filter.CostCenterStr + "%')");

            }

            if (!string.IsNullOrWhiteSpace(filter.ManufacturerStr))
            {
                whereClause += string.Format(" AND LOWER(AM.MANUFACTURERNAME) LIKE LTrim('%" + filter.ManufacturerStr + "%')");

            }

            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += string.Format(" AND LOWER(AM.SUPPLIERNAME) LIKE LTrim('%" + filter.SupplierStr + "%')");

            }

            if (!string.IsNullOrWhiteSpace(filter.assetNo))
            {
                whereClause += string.Format(" AND AM.ASSET_NO = ('{0}')", filter.assetNo);

            }
            if (!string.IsNullOrWhiteSpace(filter.AssetFilterCondition))
			{
				whereClause += string.Format(" AND AM.CONDITION = ('{0}')", filter.AssetFilterCondition);

			}
			if (!string.IsNullOrWhiteSpace(filter.staffId))
			{
				whereClause += string.Format(" AND AM.COST_CENTER in (SELECT CODE FROM STAFF_COSTCENTER_BUILDING WHERE UCATEGORY='AIMS_COSTCENTER' AND STAFFCODE ='" + filter.staffId + "')");

			}

            if (!string.IsNullOrWhiteSpace(filter.AssetFilterCostCenter))
            {
                whereClause += string.Format(" AND AM.COST_CENTER = ('{0}')", filter.AssetFilterCostCenter);

			}

            if (!string.IsNullOrWhiteSpace(filter.assetDescription))
            {
                whereClause += string.Format(" AND AM.DESCRIPTION = ('{0}')", filter.assetDescription);

            }

            if (!string.IsNullOrWhiteSpace(filter.companyAccess) && filter.companyAccess != "BSCADMIN" && filter.companyAccess != "BSC")
            {
                whereClause += string.Format(" AND AM.COST_CENTER <> ('{0}')", filter.companyAccess);

            }
            if (!string.IsNullOrWhiteSpace(filter.RecallReturn))
            {
                if(filter.RecallReturn == "ReturnRecall")
                {
                    whereClause += string.Format(" AND AM.RETURNEDREASON IN ('Recall','Return')");
			}
               
                else
                {
                    whereClause += string.Format(" AND AM.RETURNEDREASON = ('{0}')", filter.RecallReturn);
                }
                

            }




		}

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause }; //" WHERE " + whereClause + +  " ORDER BY  DESCRIPTION ASC"
			}
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ASSET_NO, DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndieSearchAssetOnHand : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "   DISTINCT AM.DESCRIPTION,PRODUCT_CODE, COUNT(AM.ASSET_NO) AS ONHANDCNT";
        private string columnsSimple = "ASSET_NO, DESCRIPTION, CATEGORY, MANUFACTURER, SERIAL_NO, MODEL_NO, MODEL_NAME, BUILDING,COST_CENTER, LOCATION, CONDITION,STATUS";
        private string from = "ASSET_V AM";

        public IndieSearchAssetOnHand(string dbType, string queryString, SearchFilter filter, string userId, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            if (!string.IsNullOrWhiteSpace(query2))
            {

                whereClause += " AND (LOWER(AM.ASSET_NO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(AM.SERIAL_NO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.MODEL_NO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.SERIAL_NO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.MODEL_NAME) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.BUILDING) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.LOCATION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.CONDITION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.STATUS) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.COSTCENTER_DESC) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(AM.SUPPLIERNAME) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(AM.RESPCENTER_DESC) LIKE ('%" + query2 + "%'))";
            }

            if (!string.IsNullOrWhiteSpace(filter.Status))
            {
                whereClause += string.Format(" AND AM.STATUS = ('{0}')", filter.Status);

            }

            if (!string.IsNullOrWhiteSpace(filter.groupCode))
            {
                whereClause += string.Format(" AND AM.GROUP_CODE = ('{0}')", filter.groupCode);

            }

            if (!string.IsNullOrWhiteSpace(filter.CloneId))
            {
                whereClause += string.Format(" AND AM.CLONE_ID = ('{0}')", filter.CloneId);

            }
            if (!string.IsNullOrWhiteSpace(filter.PO_NO))
            {
                whereClause += string.Format(" AND AM.PURCHASE_NO = ('{0}')", filter.PO_NO);

            }

            if (!string.IsNullOrWhiteSpace(filter.AssetFilterCostCenter))
            {
                whereClause += string.Format(" AND AM.COST_CENTER = ('{0}')", filter.AssetFilterCostCenter);

            }

            if (!string.IsNullOrWhiteSpace(filter.ManufacturerStr))
            {
                whereClause += string.Format(" AND LOWER(AM.MANUFACTURERNAME) LIKE LTrim('%" + filter.ManufacturerStr + "%')");

            }

            if (!string.IsNullOrWhiteSpace(filter.SupplierStr))
            {
                whereClause += string.Format(" AND LOWER(AM.SUPPLIERNAME) LIKE LTrim('%" + filter.SupplierStr + "%')");

            }

            if (!string.IsNullOrWhiteSpace(filter.assetNo))
            {
                whereClause += string.Format(" AND AM.ASSET_NO = ('{0}')", filter.assetNo);

            }
            if (!string.IsNullOrWhiteSpace(filter.AssetFilterCondition))
            {
                whereClause += string.Format(" AND AM.CONDITION = ('{0}')", filter.AssetFilterCondition);

            }

            if (!string.IsNullOrWhiteSpace(filter.RecallReturn))
            {
                if (filter.RecallReturn == "ReturnRecall")
                {
                    whereClause += string.Format(" AND AM.RETURNEDREASON IN ('Recall','Return')");
                }

                else
                {
                    whereClause += string.Format(" AND AM.RETURNEDREASON = ('{0}')", filter.RecallReturn);
                }


            }
            else
            {
                whereClause += string.Format(" AND (AM.RETURNEDREASON <> 'Recall' OR AM.RETURNEDREASON IS NULL)");
            }





        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " GROUP BY AM.DESCRIPTION, PRODUCT_CODE" + " ORDER BY AM.DESCRIPTION " };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ASSET_NO, DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }




    public class IndieSearchAssetCloneId : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "   AM.ASSET_NO, AM. DESCRIPTION, AM.CATEGORY,  AM.SERIAL_NO, AM.BUILDING, AM.COST_CENTER, AM.LOCATION_DATE, AM.LOCATION, AM.CONDITION_DATE, AM.STATUS, AM.CLONE_ID , AM.PURCHASE_DATE, AM.RESPONSIBLE_CENTER, AM.CREATED_BY, AM.CREATED_DATE";
        private string columnsSimple = "ASSET_NO, DESCRIPTION, CATEGORY, MANUFACTURER, SERIAL_NO, MODEL_NO, MODEL_NAME, BUILDING,COST_CENTER, LOCATION, CONDITION,STATUS";
        private string from = "AM_ASSET_DETAILS AM";

        public IndieSearchAssetCloneId(string dbType, string queryString, SearchFilter filter, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            if (!string.IsNullOrWhiteSpace(query2))
            {

                whereClause += " AND (LOWER(AM.ASSET_NO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.SERIAL_NO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.MODEL_NO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.SERIAL_NO) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.MODEL_NAME) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.BUILDING) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.LOCATION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.CONDITION) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.STATUS) LIKE ('" + query2 + "%')";
                whereClause += " OR LOWER(AM.RESPCENTER_DESC) LIKE ('" + query2 + "%'))";
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(filter.Status))
                {
                    whereClause += string.Format(" AND AM.STATUS = ('{0}')", filter.Status);

                }

            }

            if (!string.IsNullOrWhiteSpace(filter.CloneId))
            {
                whereClause += string.Format(" AND AM.CLONE_ID = ('{0}')", filter.CloneId);

            }

            if (!string.IsNullOrWhiteSpace(filter.CostCenterStr))
            {
                whereClause += string.Format(" AND LOWER(AM.COSTCENTER_DESC) LIKE ('" + filter.CostCenterStr + "%')");

            }

            if (!string.IsNullOrWhiteSpace(filter.assetNo))
            {
                whereClause += string.Format(" AND AM.ASSET_NO = ('{0}')", filter.assetNo);

            }
            if (!string.IsNullOrWhiteSpace(filter.AssetFilterCondition))
            {
                whereClause += string.Format(" AND AM.CONDITION = ('{0}')", filter.AssetFilterCondition);

            }
            if (!string.IsNullOrWhiteSpace(filter.staffId))
            {
                whereClause += string.Format(" AND AM.COST_CENTER in (SELECT CODE FROM STAFF_COSTCENTER_BUILDING WHERE UCATEGORY='AIMS_COSTCENTER' AND STAFFCODE ='" + filter.staffId + "')");

            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause }; //" WHERE " + whereClause + +  " ORDER BY  DESCRIPTION ASC"
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ASSET_NO, DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchAssetWOActive : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "   AM.DESCRIPTION, AM.ASSET_NO,  AM.SERIAL_NO, AM.STATUS, AM.COSTCENTER_DESC, AM.SUPPLIERNAME";
        private string columnsSimple = "ASSET_NO, DESCRIPTION, CATEGORY, MANUFACTURER, SERIAL_NO, MODEL_NO, MODEL_NAME, BUILDING,COST_CENTER, LOCATION, CONDITION,STATUS";
        private string from = "ASSET_V AM";

        public IndieSearchAssetWOActive(string dbType, string queryString, SearchFilter filter, string userId, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            if (!string.IsNullOrWhiteSpace(query2))
            {

                whereClause += " AND (LOWER(AM.ASSET_NO) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(AM.SERIAL_NO) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(AM.RESPCENTER_DESC) LIKE ('" + query2 + "%'))";
            }

           
                whereClause += string.Format(" AND AM.STATUS = 'I'");



            if (!string.IsNullOrWhiteSpace(filter.assetNo))
            {
                whereClause += string.Format(" AND AM.ASSET_NO = ('{0}')", filter.assetNo);

            }
            if (!string.IsNullOrWhiteSpace(filter.groupCode))
            {
                whereClause += string.Format(" AND AM.GROUP_CODE = ('{0}')", filter.groupCode);

            }
            if (!string.IsNullOrWhiteSpace(filter.AssetFilterCondition))
            {
                whereClause += string.Format(" AND AM.CONDITION = ('{0}')", filter.AssetFilterCondition);

            }
            if (!string.IsNullOrWhiteSpace(filter.staffId))
            {
                whereClause += string.Format(" AND AM.COST_CENTER in (SELECT CODE FROM STAFF_COSTCENTER_BUILDING WHERE UCATEGORY='AIMS_COSTCENTER' AND STAFFCODE ='" + filter.staffId + "')");

            }

            if (userId != null && userId != "")
            {
                whereClause += string.Format(" AND AM.BUILDING_CODE in (SELECT BUILDING_CODE FROM USER_BUILDING WHERE USER_ID ='" + userId + "')");
            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + "ORDER BY AM.DESCRIPTION"}; //" WHERE " + whereClause + +  " ORDER BY  DESCRIPTION ASC"
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ASSET_NO, DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndieSearchAssetBIOMEDActive : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "   AM.DESCRIPTION, AM.ASSET_NO,  AM.SERIAL_NO, AM.STATUS, AM.COSTCENTER_DESC, AM.SUPPLIERNAME";
        private string columnsSimple = "ASSET_NO, DESCRIPTION, CATEGORY, MANUFACTURER, SERIAL_NO, MODEL_NO, MODEL_NAME, BUILDING,COST_CENTER, LOCATION, CONDITION,STATUS";
        private string from = "ASSET_BIOMED_V AM";

        public IndieSearchAssetBIOMEDActive(string dbType, string queryString, SearchFilter filter, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            if (!string.IsNullOrWhiteSpace(query2))
            {

                whereClause += " AND (LOWER(AM.ASSET_NO) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('%" + query2 + "%')";

                whereClause += " OR LOWER(AM.RESPCENTER_DESC) LIKE ('" + query2 + "%'))";
            }


            whereClause += string.Format(" AND AM.STATUS = 'I'");



            if (!string.IsNullOrWhiteSpace(filter.assetNo))
            {
                whereClause += string.Format(" AND AM.ASSET_NO = ('{0}')", filter.assetNo);

            }
            if (!string.IsNullOrWhiteSpace(filter.AssetFilterCondition))
            {
                whereClause += string.Format(" AND AM.CONDITION = ('{0}')", filter.AssetFilterCondition);

            }
            if (!string.IsNullOrWhiteSpace(filter.staffId))
            {
                whereClause += string.Format(" AND AM.COST_CENTER in (SELECT CODE FROM STAFF_COSTCENTER_BUILDING WHERE UCATEGORY='AIMS_COSTCENTER' AND STAFFCODE ='" + filter.staffId + "')");

            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + "ORDER BY AM.DESCRIPTION" }; //" WHERE " + whereClause + +  " ORDER BY  DESCRIPTION ASC"
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ASSET_NO, DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchAssetActive : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "   (ROW_NUMBER() OVER (ORDER BY AM.description ASC)) AS ROWNUMID,AM.DESCRIPTION, AM.ASSET_NO, AM.CATEGORY, AM.MANUFACTURER, AM.SERIAL_NO,AM.MODEL_NO, AM.MODEL_NAME, AM.BUILDING, AM.COST_CENTER, AM.LOCATION, AM.CONDITION, AM.STATUS, AM.RESPCENTER_DESC, AM.COSTCENTER_DESC";
        private string columnsSimple = "ASSET_NO, DESCRIPTION, CATEGORY, MANUFACTURER, SERIAL_NO, MODEL_NO, MODEL_NAME, BUILDING,COST_CENTER, LOCATION, CONDITION,STATUS";
        private string from = "ASSET_V AM";

        public IndieSearchAssetActive(string dbType, string queryString, SearchFilter filter, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            if (!string.IsNullOrWhiteSpace(query2))
            {

                whereClause += " AND (LOWER(AM.ASSET_NO) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(AM.SERIAL_NO) LIKE ('%" + query2 + "%')";
                whereClause += " OR LOWER(AM.RESPCENTER_DESC) LIKE ('" + query2 + "%'))";
            }
          
                if (!string.IsNullOrWhiteSpace(filter.Status))
                {
                    whereClause += string.Format(" AND AM.STATUS = ('{0}')", filter.Status);

                }
        

            if (!string.IsNullOrWhiteSpace(filter.assetNo))
            {
                whereClause += string.Format(" AND AM.ASSET_NO = ('{0}')", filter.assetNo);

            }
            if (!string.IsNullOrWhiteSpace(filter.AssetFilterCondition))
            {
                whereClause += string.Format(" AND AM.CONDITION = ('{0}')", filter.AssetFilterCondition);

            }
            if (!string.IsNullOrWhiteSpace(filter.staffId))
            {
                whereClause += string.Format(" AND AM.COST_CENTER in (SELECT CODE FROM STAFF_COSTCENTER_BUILDING WHERE UCATEGORY='AIMS_COSTCENTER' AND STAFFCODE ='" + filter.staffId + "')");

            }
        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause }; //" WHERE " + whereClause + +  " ORDER BY  DESCRIPTION ASC"
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ASSET_NO, DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchAssetCostCenter : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "   (ROW_NUMBER() OVER (ORDER BY AM.description ASC)) AS ROWNUMID,AM.DESCRIPTION, AM.ASSET_NO, AM.CATEGORY, AM.MANUFACTURER, AM.SERIAL_NO,AM.MODEL_NO, AM.MODEL_NAME, AM.BUILDING, AM.COST_CENTER, AM.LOCATION, AM.CONDITION, AM.STATUS, AM.RESPCENTER_DESC";
		private string columnsSimple = "ASSET_NO, DESCRIPTION, CATEGORY, MANUFACTURER, SERIAL_NO, MODEL_NO, MODEL_NAME, BUILDING,COST_CENTER, LOCATION, CONDITION,STATUS";
		private string from = "ASSET_V AM";

		public IndieSearchAssetCostCenter(string dbType, string queryString, SearchFilter filter, int maxRows = 500)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			//foreach (string query in queryArray)
			//{
			var query2 = queryString.ToLower();
			whereClause += " OR (LOWER(AM.ASSET_NO) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.SERIAL_NO) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.MODEL_NO) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.SERIAL_NO) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.MODEL_NAME) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.BUILDING) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.LOCATION) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.CONDITION) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.STATUS) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.RESPCENTER_DESC) LIKE ('" + query2 + "%'))";

			if (!string.IsNullOrWhiteSpace(filter.Status))
			{
				whereClause += string.Format(" AND AM.STATUS = ('{0}')", filter.Status);

			}
			if (!string.IsNullOrWhiteSpace(filter.AssetFilterCondition))
			{
				whereClause += string.Format(" AND AM.CONDITION = ('{0}')", filter.AssetFilterCondition);

			}
			if (!string.IsNullOrWhiteSpace(filter.staffId))
			{
				whereClause += string.Format(" AND AM.COST_CENTER in (SELECT CODE FROM STAFF_COSTCENTER_BUILDING WHERE UCATEGORY='AIMS_COSTCENTER' AND STAFFCODE ='"+ filter.staffId+"')");

			}
		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause }; //" WHERE " + whereClause + +  " ORDER BY  DESCRIPTION ASC"
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ASSET_NO, DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
			}
		}


	}

	public class IndieSearchSupplier : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.SUPPLIER_ID, AM.DESCRIPTION, AM.ADDRESS1, AM.CITY, AM.CONTACT,AM.CONTACT_NO1, AM.CONTACT_NO2, AM.EMAIL_ADDRESS, AM.STATUS, AM.STATE, AM.ZIP";
        private string columnsSimple = "SUPPLIER_ID, DESCRIPTION, ADDRESS1, CITY, CONTACT, CONTACT_NO1, CONTACT_NO2, EMAIL_ADDRESS";
        private string from = "AM_SUPPLIER AM";

        public IndieSearchSupplier(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.SUPPLIER_ID) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(AM.ADDRESS1) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.CONTACT) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.CONTACT_NO1) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.CONTACT_NO2) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.EMAIL_ADDRESS) LIKE ('" + query2 + "%')";



        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
	public class IndieSearchSupplierActiveLOVData : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = " ";
		private string columns = "AM.SUPPLIER_ID, AM.DESCRIPTION, AM.ADDRESS1, AM.CITY, AM.CONTACT,AM.CONTACT_NO1, AM.CONTACT_NO2, AM.EMAIL_ADDRESS, AM.STATUS, AM.STATE, AM.ZIP";
		private string columnsSimple = "SUPPLIER_ID, DESCRIPTION, ADDRESS1, CITY, CONTACT, CONTACT_NO1, CONTACT_NO2, EMAIL_ADDRESS";
		private string from = "AM_SUPPLIER AM";

		public IndieSearchSupplierActiveLOVData(string dbType, string queryString, int maxRows = 500)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			//foreach (string query in queryArray)
			//{
			var query2 = queryString.ToLower();
			//whereClause += " OR (LOWER(AM.SUPPLIER_ID) LIKE ('" + query2 + "%'))";


			whereClause += " AM.STATUS = 'Y' ";



		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
			}
		}


	}
	public class IndieSearchProcedure : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "AM.PROCEDURE_ID, AM.DESCRIPTION, AM.SERV_DEPT, AM.ASSIGN_TO, AM.REMARKS,AM.STATUS";
		private string columnsSimple = "PROCEDURE_ID, DESCRIPTION, SERV_DEPT, ASSIGN_TO, REMARKS, STATUS";
		private string from = "AM_PROCEDURES AM";

		public IndieSearchProcedure(string dbType, string queryString, int maxRows = 500)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			//foreach (string query in queryArray)
			//{
			var query2 = queryString.ToLower();
			whereClause += " OR LOWER(AM.PROCEDURE_ID) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.SERV_DEPT) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.ASSIGN_TO) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.STATUS) LIKE ('" + query2 + "%')";



		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
			}
		}


	}
	public class IndieSearchProcedureActiveLookup : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "AM.PROCEDURE_ID, AM.DESCRIPTION, AM.SERV_DEPT, AM.ASSIGN_TO, AM.REMARKS,AM.STATUS";
		private string columnsSimple = "PROCEDURE_ID, DESCRIPTION, SERV_DEPT, ASSIGN_TO, REMARKS, STATUS";
		private string from = "AM_PROCEDURES AM";

		public IndieSearchProcedureActiveLookup(string dbType, string queryString, int maxRows = 500)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			//foreach (string query in queryArray)
			//{
			var query2 = queryString.ToLower();
			whereClause += " OR (LOWER(AM.PROCEDURE_ID) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.SERV_DEPT) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.ASSIGN_TO) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.STATUS) LIKE ('" + query2 + "%'))";

			whereClause += " AND (AM.STATUS) = 'Y' ";

		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
			}
		}


	}
	public class IndieSearchSupplierActiveLookup : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "AM.SUPPLIER_ID, AM.DESCRIPTION";
		private string columnsSimple = "SUPPLIER_ID, DESCRIPTION";
		private string from = "AM_SUPPLIER AM";

		public IndieSearchSupplierActiveLookup(string dbType, string queryString, int maxRows = 10000)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			//foreach (string query in queryArray)
			//{
			var query2 = queryString.ToLower();
			whereClause += " OR (LOWER(AM.SUPPLIER_ID) LIKE ('" + query2 + "%'))";
			

			whereClause += " AND AM.STATUS = 'Y' ";

		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
			}
		}


	}
    public class IndieSearchAssetNotes : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.NOTE_ID, AM.NOTES, Convert(varchar(11),AM.CREATED_DATE ,103) as CREATED_DATE , ST.FIRST_NAME,ST.LAST_NAME,AM.CREATED_BY";
        private string columnsSimple = "ID, WO_NOTES, CREATED_DATE";
        private string from = "AM_ASSET_NOTES AM LEFT JOIN STAFF ST ON AM.CREATED_BY = ST.STAFF_ID";

        public IndieSearchAssetNotes(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.ASSET_NO) = LOWER('" + query2 + "')";



        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM.NOTE_ID DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  CREATED_DATE DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchAssetWORefNotes : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.REQ_NO, AM.ID, AM.WO_NOTES, AM.CREATED_DATE,ST.FIRST_NAME,ST.LAST_NAME,AM.CREATED_BY";
        private string columnsSimple = "ID, WO_NOTES, CREATED_DATE";
        private string from = "AM_WORK_ORDER_REFNOTES AM LEFT JOIN STAFF ST ON AM.CREATED_BY = ST.STAFF_ID";

        public IndieSearchAssetWORefNotes(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.ASSET_NO) = LOWER('" + query2 + "')";



        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  CREATED_DATE DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  CREATED_DATE DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchWORefNotes : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.ID, AM.WO_NOTES, AM.CREATED_DATE,ST.FIRST_NAME,ST.LAST_NAME,AM.CREATED_BY";
        private string columnsSimple = "ID, WO_NOTES, CREATED_DATE";
        private string from = "AM_WORK_ORDER_REFNOTES AM LEFT JOIN STAFF ST ON AM.CREATED_BY = ST.STAFF_ID";

        public IndieSearchWORefNotes(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.REQ_NO) = LOWER('" + query2 + "')";



        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  CREATED_DATE DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  CREATED_DATE DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchInspectionAsset : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.AM_INSPECTIONASSETID, AM.REF_INSPECTION_ID, AM.ASSET_NO, AM.CREATED_BY,AM.CREATED_DATE, ST.DESCRIPTION";
        private string columnsSimple = "AM_INSPECTIONASSETID, AM_INSPECTION_ID, ASSET_NO,CREATED_DATE";
        private string from = "AM_INSPECTIONASSET AM INNER JOIN AM_ASSET_DETAILS ST ON AM.ASSET_NO = ST.ASSET_NO";

        public IndieSearchInspectionAsset(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.REF_INSPECTION_ID) = (" + query2 + ")";



        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  CREATED_DATE DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  CREATED_DATE DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchMeterReadingAssetNo : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.AM_METERREADINGID, AM.ASSET_NO, AM.AM_INSPECTION_ID, AM.REQ_NO, AM.NEXT_PMDUE,AM.CURRENT_READING,AM.READING_DATE, AM.REMARKS, AM.CREATED_BY,CREATED_DATE";
        private string columnsSimple = "AM_METERREADINGID, ASSET_NO, AM_INSPECTION_ID, REQ_NO, NEXT_PMDUE, CURRENT_READING, REMARKS, EMAIL_ADDRESS,CREATED_DATE";
        private string from = "AM_METERREADING AM";

        public IndieSearchMeterReadingAssetNo(string dbType, string queryString, int maxRows = 6)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.ASSET_NO) = ('" + query2 + "')";



        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  READING_DATE DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  READING_DATE DESC" + " LIMIT 6" };
            }
        }


    }
    public class IndieReadPurchaseReq : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.AM_PRREQITEMID, AM.QTY, AM.REMARKS, AM.REF_WO, AM.STATUS,AM.REF_ASSETNO,AM.REQUEST_ID, AM.STATUS_BY, AM.STATUS_DATE,AM.REQ_BY,AM.REQ_CONTACTNO,AM.REQ_DEPT,AM.REQ_DATE,AM.STORE_ID,AM.REQ_DETFLAG,AM.ID_PARTS, ST.DESCRIPTION";
        private string columnsSimple = "AM_PRREQITEMID, QTY, REMARKS, REF_WO, STATUS, REF_ASSETNO, REQUEST_ID, STATUS_BY,STATUS_DATE";
        private string from = "AM_PURCHASEREQUEST AM LEFT JOIN AM_PARTS ST ON AM.ID_PARTS = ST.ID_PARTS";

        public IndieReadPurchaseReq(string dbType, string queryString, int maxRows = 6)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.REQUEST_ID) = ('" + query2 + "')";



        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PRREQITEMID ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PRREQITEMID ASC" + " LIMIT 5000" };
            }
        }


    }
   
    public class IndieReadPurchaseRequest : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AP.AM_PRREQITEMID, AP.REMARKS, AP.REF_WO, AP.STATUS, AP.REF_ASSETNO, AP.REQ_BY, AP.REQ_CONTACTNO, AP.REQ_DEPT, AP.REQ_DATE, AP.CREATED_BY, AP.CREATED_DATE, AP.STORE_ID, AP.REQUEST_ID, AP.APPROVED_BY, AP.APPROVED_DATE, AP.CANCELLED_BY, AP.CANCELLED_DATE, " +
            "AP.PO_STATUS, AP.STATUS_REMARKS, AP.CANCELLED_REASON, AP.RETURNSTATUS_BY, AP.RETURNSTATUS_DATE, AP.RETURNSTATUS_REASON, AP.REJECTREF, AP.SUPPLIER_ID, AP.QTY, AP.PURCHASE_DATE, AP.BILL_ADDRESS, AP.BILL_EMAIL_ADDRESS, AP.BILL_CONTACT_NO1, " +
            "AP.BILL_CONTACT_NO2, AP.BILL_ZIPCODE, AP.BILL_CITY, AP.BILL_STATE, AP.BILL_SHIPTO, AP.FOREIGN_CURRENCY, AP.SHIP_VIA, AP.TOTAL_AMOUNT, AP.TOTAL_NOITEMS, AP.SUPPLIER_ADDRESS1, AP.SUPPLIER_CITY, AP.SUPPLIER_STATE, " +
            "AP.SUPPLIER_ZIPCODE, AP.SUPPLIER_CONTACT_NO1, AP.SUPPLIER_CONTACT_NO2, AP.SUPPLIER_CONTACT, AP.SUPPLIER_EMAIL_ADDRESS, AP.OTHER_INFO, AP.SHIP_DATE, AP.EST_SHIP_DATE, AP.HANDLING_FEE, AP.TRACKING_NO, AP.EST_FREIGHT, " +
            "AP.FREIGHT, AP.FREIGHT_PAIDBY, AP.FREIGHT_CHARGETO, AP.TAXABLE, AP.TAX_CODE, AP.TAX_AMOUNT, AP.FREIGHT_POLINE, AP.FREIGHT_SEPARATE_INV, AP.INSURANCE_VALUE, AP.PO_BILLCODE_ADDRESS, AP.PURCHASE_NO, AP.POINVOICENO, AM.DESCRIPTION AS SUPPLIERDESC";
        private string columnsSimple = "AM_PRREQITEMID, QTY, REMARKS, REF_WO, STATUS, REF_ASSETNO, REQUEST_ID, STATUS_BY,STATUS_DATE";
        private string from = "AM_PURCHASEREQUEST AP LEFT JOIN AM_MANUFACTURER AM ON AP.SUPPLIER_ID = AM.MANUFACTURER_ID";

        public IndieReadPurchaseRequest(string dbType, string reqID)
        {
            this.DatabaseType = dbType;


            whereClause += " OR LOWER(AP.REQUEST_ID) = ('" + reqID + "')";

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PRREQITEMID ASC" + " LIMIT 5000" };
            }
        }


    }


    public class IndieReadPurchaseReqItems : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.AM_WORKORDER_LABOUR_ID, AM.AM_PARTREQUESTID, AM.DESCRIPTION, AM.QTY, AM.PRICE,AM.EXTENDED,AM.BILLABLE, AM.REF_WO, AM.STATUS,AM.WO_LABOURTYPE,AM.ID_PARTS,AM.REF_ASSETNO,AM.STORE_ID,AM.REQ_DATE, ST.DESCRIPTION PRODUCT_DESC, PX.DESCRIPTION STORE_DESC, AM.TRANSFER_QTY,AM.QTY_RETURN, AM.QTY_FORRETURN, AM.TRANSFER_FLAG,AM.PR_REQUEST_ID,AM.PR_REQUEST_QTY, PS.PART_NUMBERS, LL.DESCRIPTION AS AM_UOM, SOH.QTY STOCK_QTY, SOH.MAX STOCK_MAX, SOH.MIN STOCK_MIN, ST.TYPE_ID as AM_TYPE_ID, LLT.DESCRIPTION as AM_TYPE, (SELECT SUM(PSB.QTY_RECEIVED) FROM PRODUCT_STOCKBATCH PSB WHERE PSB.TRANS_ID = AM.PR_REQUEST_ID AND PSB.PRODUCT_CODE = AM.ID_PARTS) AS TOTAL_QTY_RECEIVED";
        private string columnsSimple = "AM_WORKORDER_LABOUR_ID, AM_PARTREQUESTID, DESCRIPTION, QTY, PRICE, EXTENDED, BILLABLE, REF_WO,STATUS";
        private string from = "AM_PARTSREQUEST AM LEFT JOIN AM_PARTS ST ON AM.ID_PARTS = ST.ID_PARTS LEFT JOIN STORES PX ON PX.STORE_ID = AM.STORE_ID LEFT OUTER JOIN PARTNO_BYPARTID_V PS ON AM.ID_PARTS = PS.ID_PARTS LEFT JOIN lov_lookups LL ON LL.LOV_LOOKUP_ID = ST.uom_id LEFT JOIN lov_lookups LLT ON LLT.LOV_LOOKUP_ID = ST.type_id LEFT JOIN product_stockonhand SOH ON AM.id_parts = SOH.PRODUCT_CODE AND AM.store_id = SOH.STORE";

        public IndieReadPurchaseReqItems(string dbType, string queryString, int maxRows = 6)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.PR_REQUEST_ID) = ('" + query2 + "')";



        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PARTREQUESTID ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PARTREQUESTID ASC" + " LIMIT 5000" };
            }
        }


    }
    public class IndieReadPurchaseOrderItemsBatch : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "AM.ID_PARTS, AM.PURCHASE_NO, AM.QTY_RECEIVED, AM.QTY_PO_REQUEST, AM.DESCRIPTION,AM.RECEIVING_STORE, AM.SUPPLIER_ID,AM.AM_REQITEMID,AM.PO_UNITPRICE";
		private string columnsSimple = "AM_PRREQITEMID, QTY, REMARKS, REF_WO, STATUS, REF_ASSETNO, REQUEST_ID, STATUS_BY,STATUS_DATE";
		private string from = "PURCHASE_ORDERFORRECITEMS_V AM";

		public IndieReadPurchaseOrderItemsBatch(string dbType, string queryString, int maxRows = 6)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			//foreach (string query in queryArray)
			//{
			var query2 = queryString.ToLower();
			whereClause += " OR LOWER(AM.PURCHASE_NO) = ('" + query2 + "')";



		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_REQITEMID ASC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PRREQITEMID ASC" + " LIMIT 5000" };
			}
		}


	}
	public class IndieReadPurchaseOrderItems : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "AM.PURCHASE_NO, AM.ID_PARTS, AM.DESCRIPTION, AM.PO_UNITPRICE, AM.QTY_PO_REQUEST,AM.PR_REQUEST_QTY,AM.PO_TOTALUNITPRICEITEM, AM.PR_REQUEST_ID";
		private string columnsSimple = "PURCHASE_NO, ID_PARTS, DESCRIPTION, PURCHASE_UNITPRICE, PURCHASE_QTY,AM.QTY";
		private string from = "PRPO_V AM";

		public IndieReadPurchaseOrderItems(string dbType, string queryString, int maxRows = 6)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			//foreach (string query in queryArray)
			//{
			var query2 = queryString.ToLower();
			whereClause += " OR LOWER(AM.PURCHASE_NO) = ('" + query2 + "')";



		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM.AM_REQITEMID ASC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_PRREQITEMID ASC" + " LIMIT 5000" };
			}
		}


	}
	public class IndieSearchPurchaseReq : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = " AM.PR_REQUEST_QTY,AM.ID_PARTS, AM.DESCRIPTION,AM.REF_WO,AM.REF_ASSETNO,AM.STATUS,AM.REQDATE,AM.REQUEST_ID";
		private string columnsSimple = "AM.QTY,AM.ID_PARTS,ST.DESCRIPTION";
		private string from = "PURCHASE_REQUEST_FORPO_V AM";

		public IndieSearchPurchaseReq(string dbType, string queryString, int maxRows = 6)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			//foreach (string query in queryArray)
			//{
			var query2 = queryString.ToLower();
			whereClause += " OR (UPPER(AM.STATUS) = ('APPROVED'))";



		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = " SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM.REQDATE ASC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " GROUP BY AM.ID_PARTS ST.DESCRIPTION, ORDER BY  ST.DESCRIPTION " + " LIMIT 5000" };
			}
		}


	}
	public class IndieSearchInspectionTransWS : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.AM_INSPECTIONTRANSID, AM.AM_INSPECTION_ID, AM.REQ_NO, AM.ASSET_NO, AM.INSPECTION_DATE,AM.INSPECTION_TIME, AM.EMPLOYEE, AM.INSPECTION_RESULT,AM.TOTAL_HOURWS,RATE,AM.COST_CENTER,AM.INSP_TYPE,AM.FAILURE,AM.INSPROCEDURE,AM.REMARKS,AM.CREATED_BY,AM.CREATED_DATE";
        private string columnsSimple = "AM_INSPECTIONTRANSID, AM_INSPECTION_ID, REQ_NO, ASSET_NO, INSPECTION_DATE, INSPECTION_TIME, EMPLOYEE, INSPECTION_RESULT,TOTAL_HOURWS,RATE,COST_CENTER,INSP_TYPE,FAILURE,INSPROCEDURE ,REMARKS,CREATED_BY,CREATED_DATE";
		private string columnsSql = "AM.AM_INSPECTIONTRANSID, AM.AM_INSPECTION_ID, AM.REQ_NO, AM.ASSET_NO, AM.INSPECTION_DATE,AM.INSPECTION_TIME, AM.EMPLOYEE, AM.INSPECTION_RESULT,AM.TOTAL_HOURWS,RATE,AM.COST_CENTER,AM.INSP_TYPE,AM.FAILURE,AM.INSPROCEDURE,AM.REMARKS,AM.CREATED_BY,AM.CREATED_DATE";
		private string from = "AM_INSPECTIONTRANS AM";

        public IndieSearchInspectionTransWS(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.AM_INSPECTION_ID) = (" + query2 + ")";



        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columnsSql + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_INSPECTIONTRANSID DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  AM_INSPECTIONTRANSID DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchWorkSheet : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.AM_INSPECTION_ID, AM.DESCRIPTION, AM.PROCESS, AM.INSPECTION_TYPE, AM.ASSET_TYPECOVERED";
        private string columnsSimple = "AM_INSPECTION_ID, DESCRIPTION, PROCESS, INSPECTION_TYPE, ASSET_TYPECOVERED";
        private string from = "AM_INSPECTION AM";

        public IndieSearchWorkSheet(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.AM_INSPECTION_ID) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(AM.PROCESS) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(AM.INSPECTION_TYPE) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(AM.ASSET_TYPECOVERED) LIKE ('" + query2 + "%')";


        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchSupplierById : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.SUPPLIER_ID, AM.DESCRIPTION, AM.ADDRESS1, AM.CITY, AM.CONTACT,AM.CONTACT_NO1, AM.CONTACT_NO2, AM.EMAIL_ADDRESS";
        private string columnsSimple = "SUPPLIER_ID, DESCRIPTION, ADDRESS1, CITY, CONTACT, CONTACT_NO1, CONTACT_NO2, EMAIL_ADDRESS";
        private string from = "AM_SUPPLIER AM";

        public IndieSearchSupplierById(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            //string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            if(queryString == null)
            {
                queryString = "";
            }
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.SUPPLIER_ID) = ('" + query2 + "')";
           



        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndieSearchManufacturer : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.MANUFACTURER_ID, AM.DESCRIPTION, AM.ADDRESS1, AM.CITY, AM.CONTACT,AM.CONTACT_NO1, AM.CONTACT_NO2, AM.EMAIL_ADDRESS, AM.STATUS";
        private string columnsSimple = "MANUFACTURER_ID, DESCRIPTION, ADDRESS1, CITY, CONTACT, CONTACT_NO1, CONTACT_NO2, EMAIL_ADDRESS, STATUS";
        private string from = "AM_MANUFACTURER AM";

        public IndieSearchManufacturer(string dbType, string queryString, SearchFilter filter, int maxRows)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR (LOWER(AM.MANUFACTURER_ID) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('%" + query2 + "%')";
            whereClause += " OR LOWER(AM.ADDRESS1) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.CONTACT) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.CONTACT_NO1) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.CONTACT_NO2) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.EMAIL_ADDRESS) LIKE ('" + query2 + "%'))";

            if (filter.ItemType != "B")
                whereClause += " AND (AM.TYPE ='" + filter.ItemType + "' )";

            if (filter.Status == "ALL")
                whereClause += " AND (AM.STATUS IN ('Y', 'N') )";
            else if (filter.Status != "ALL")
                whereClause += " AND (AM.STATUS ='" + filter.Status + "' )";

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
	public class IndieSearchManufacturerId : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "AM.MANUFACTURER_ID, AM.DESCRIPTION, AM.ADDRESS1, AM.CITY, AM.CONTACT,AM.CONTACT_NO1, AM.CONTACT_NO2, AM.EMAIL_ADDRESS, AM.STATUS";
		private string columnsSimple = "MANUFACTURER_ID, DESCRIPTION, ADDRESS1, CITY, CONTACT, CONTACT_NO1, CONTACT_NO2, EMAIL_ADDRESS, STATUS";
		private string from = "AM_MANUFACTURER AM";

		public IndieSearchManufacturerId(string dbType, string queryString, int maxRows)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			//foreach (string query in queryArray)
			//{
			var query2 = queryString.ToLower();
			whereClause += " OR LOWER(AM.MANUFACTURER_ID) = ('" + query2 + "')";
			



		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
			}
		}


	}
	public class IndieSearchInfoServiceDvice : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "AM.DEVICE_ID, AM.COMPUTER_BASE, AM.COMPUTER_TYPE, AM.MODEL, AM.OPERATING_SYSTEM,AM.FRIMWARE_VERSION, AM.PROCESSOR, AM.MEMORY, AM.HARD_DRIVE";
		private string columnsSimple = "DEVICE_ID, COMPUTER_BASE, COMPUTER_TYPE, MODEL, OPERATING_SYSTEM, FRIMWARE_VERSION, PROCESSOR, MEMORY, HARD_DRIVE";
		private string from = "AM_ASSET_INFOSERVICES_DEVICE AM";

		public IndieSearchInfoServiceDvice(string dbType, string queryString, string queryAssetNo, int maxRows)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			//foreach (string query in queryArray)
			//{
			var query2 = queryString.ToLower();
			whereClause += " OR (LOWER(AM.DEVICE_ID) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.COMPUTER_BASE) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.COMPUTER_TYPE) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.MODEL) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.OPERATING_SYSTEM) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.FRIMWARE_VERSION) LIKE ('" + query2 + "%')";
			whereClause += " OR LOWER(AM.PROCESSOR) LIKE ('" + query2 + "%'))";

			if (queryAssetNo != null && queryAssetNo != "")
			{
				whereClause += " AND (AM.ASSET_NO ='" + queryAssetNo + "' )";
			}

		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DEVICE_ID ASC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
			}
		}


	}

	public class IndieSearchManufacturerLookUpActive : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "AM.MANUFACTURER_ID, AM.DESCRIPTION";
		private string columnsSimple = "MANUFACTURER_ID, DESCRIPTION";
		private string from = "AM_MANUFACTURER AM";

		public IndieSearchManufacturerLookUpActive(string dbType, string queryString, int maxRows)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			//foreach (string query in queryArray)
			//{
			var query2 = queryString.ToLower();
			whereClause += " OR (LOWER(AM.MANUFACTURER_ID) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('%" + query2 + "%'))";

            whereClause += " AND AM.STATUS = 'Y' ";


		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
			}
		}


	}

	public class IndieSearchMSVLookUp : IndieSql
	{
		private int maxRows = 50;
		private string queryString = "";
		private string whereClause = "1=2 ";
		private string columns = "AM.MANUFACTURER_ID, AM.DESCRIPTION, AM.ADDRESS1, AM.ADDRESS2, AM.ADDRESS3, AM.CITY, AM.STATE, AM.ZIP, AM.SUPPILER_NO, AM.CONTACT, AM.CONTACT_NO1, AM.CONTACT_NO2, AM.EMAIL_ADDRESS";
		private string columnsSimple = "MANUFACTURER_ID, DESCRIPTION";
		private string from = "AM_MANUFACTURER AM";

		public IndieSearchMSVLookUp(string dbType, string queryString, string type, int maxRows)
		{
			this.DatabaseType = dbType;
			this.queryString = queryString;
			this.maxRows = maxRows;
			string[] queryArray = queryString.Split(new char[] { ' ' });

			//foreach (string query in queryArray)
			//{
			var query2 = queryString.ToLower();
			whereClause += " OR (LOWER(AM.MANUFACTURER_ID) LIKE ('" + query2 + "%')";
            whereClause += " OR LOWER(AM.DESCRIPTION) LIKE ('%" + query2 + "%'))";

            whereClause += " AND AM.STATUS = 'Y' ";
            whereClause += " AND (AM.TYPE = '" + type + "' OR AM.TYPE = 'B') ";


		}

		public override StmtOracle OracleSql
		{
			get
			{
				return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
			}
		}

		public override StmtSqlServer SqlServerSql
		{
			get
			{
				return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" };
			}
		}

		public override StmtMySql MySql
		{
			get
			{
				return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  DESCRIPTION ASC" + " LIMIT " + maxRows.ToString() };
			}
		}


	}

	public class IndieSearchothById : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.REF_ID, AM.REF_DESC,AM.REF_TYPE, AM.REF_1, AM.REF_2, AM.REF_3";
        private string columnsSimple = "REF_ID, REF_DESC";
        private string from = "WOL_LOOKUP_V AM";

        public IndieSearchothById(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.REF_ID) = ('" + query2 + "')";




        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REF_TYPE ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REF_TYPE ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndieSearchrefType : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.REF_ID, AM.REF_DESC,AM.REF_TYPE, AM.REF_1, AM.REF_2, AM.REF_3";
        private string columnsSimple = "REF_ID, REF_DESC";
        private string from = "WOL_LOOKUP_V AM";

        public IndieSearchrefType(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.REF_TYPE) = ('" + query2 + "')";




        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REF_TYPE ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REF_TYPE ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndieSearchWorkOrderParts : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.LABOUR_TYPE, AM.REF_WO, AM.PROBLEM, AM.REQ_DATE, AM.WO_STATUS,AM.REQ_NO, AM.ASSET_NO, AM.ASSET_DESC, AM.NO_ITEMS, AM.MATERIALS_REMARKS";
        private string columnsSimple = "LABOUR_TYPE, REF_WO, PROBLEM, REQ_DATE, WO_STATUS, REQ_NO, ASSET_NO, ASSET_DESC";
        private string from = "DISPATCH_WOPARTS_V AM";

        public IndieSearchWorkOrderParts(string dbType, string queryString,SearchFilter filter, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
          
                whereClause += " OR (LOWER(AM.ASSET_NO) LIKE ('" + query2 + "%') OR LOWER(AM.ASSET_DESC) LIKE ('" + query2 + "%') " +
                    " OR LOWER(AM.PROBLEM) LIKE ('" + query2 + "%') OR LOWER(AM.REF_WO) LIKE ('" + query2 + "%')" +
                    " OR LOWER(AM.PROBLEM) LIKE ('" + query2 + "%')  OR LOWER(AM.REF_WO) LIKE ('" + query2 + "%')" +
                    " OR LOWER(AM.REQ_NO) LIKE ('" + query2 + "%') OR LOWER(AM.WO_STATUS) LIKE ('" + query2 + "%') )";
            

            if (!string.IsNullOrWhiteSpace(filter.FromDate))
            {
                var whereDate = string.Format("AM.REQ_DATE >= '{0}'", filter.FromDate);
                if (!string.IsNullOrWhiteSpace(filter.ToDate))
                {
                    whereDate += string.Format(" and AM.REQ_DATE <= '{0}'", filter.ToDate);
                }
                whereClause += " AND (" + whereDate + ")";
            }

            if (!string.IsNullOrWhiteSpace(filter.Status))
            {
                if(filter.Status == "PENDING")
                {
                    whereClause += string.Format(" AND AM.WO_STATUS in ('NE','OP','HA','FC')");
                }
                else if(filter.Status == "CLOSED")
                {
                    whereClause += string.Format(" AND AM.WO_STATUS = 'CL'");
                }
               

            }

        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_DATE DESC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REQ_DATE DESC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }

    public class IndieSearchAssetWOSummaryById : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.ASSET_NO, AM.PM_COUNT_CURRENT, AM.OT_COUNT_CURRENT, AM.PM_COUNT_PREVIOUS, AM.OT_COUNT_PREVIOUS,AM.PM_DURATION_CURRENT, AM.OT_DURATION_CURRENT, AM.PM_DURATION_PREVIOUS, AM.OT_DURATION_PREVIOUS, AM.PM_COUNT_ALL, AM.OT_COUNT_ALL, AM.PM_DURATION_ALL,AM.OT_DURATION_ALL";
        private string columnsSimple = "ASSET_NO, PM_COUNT_CURRENT, OT_COUNT_CURRENT, PM_COUNT_PREVIOUS, OT_COUNT_PREVIOUS, PM_DURATION_CURRENT, OT_DURATION_CURRENT, PM_DURATION_PREVIOUS";
        private string from = "ASSET_WOSUMMARY_V AM";

        public IndieSearchAssetWOSummaryById(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.ASSET_NO) = ('" + query2 + "')";




        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ASSET_NO ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ASSET_NO ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchAssetWOCostSummaryById : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.ASSET_NO, AM.PM_COST_CURRENT, AM.OT_COST_CURRENT, AM.PM_COST_PREVIOUS, AM.OT_COST_PREVIOUS,AM.PM_COST_ALL, AM.OT_COST_ALL";
        private string columnsSimple = "ASSET_NO, PM_COST_CURRENT, OT_COST_CURRENT, PM_COST_PREVIOUS, OT_COST_PREVIOUS, PM_COST_ALL, OT_COST_ALL";
        private string from = "ASSET_WOPMOTCOST_V AM";

        public IndieSearchAssetWOCostSummaryById(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.ASSET_NO) = ('" + query2 + "')";




        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ASSET_NO ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ASSET_NO ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchAssetWOContractCostSummaryById : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.ASSET_NO, AM.C_COST_CURRENT, AM.C_COST_PREVIOUS, AM.C_COST_ALL";
        private string columnsSimple = "ASSET_NO, C_COST_CURRENT, C_COST_PREVIOUS, C_COST_ALL";
        private string from = "ASSET_WOCONTRACTCOST_V AM";

        public IndieSearchAssetWOContractCostSummaryById(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.ASSET_NO) = ('" + query2 + "')";




        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ASSET_NO ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ASSET_NO ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchAssetWOMaterialCostSummaryById : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=2 ";
        private string columns = "AM.ASSET_NO, AM.M_COST_CURRENT, AM.M_COST_PREVIOUS, AM.M_COST_ALL";
        private string columnsSimple = "ASSET_NO, M_COST_CURRENT, M_COST_PREVIOUS, M_COST_ALL";
        private string from = "ASSET_WOMATERIALCOST_V AM";

        public IndieSearchAssetWOMaterialCostSummaryById(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            var query2 = queryString.ToLower();
            whereClause += " OR LOWER(AM.ASSET_NO) = ('" + query2 + "')";




        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ASSET_NO ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ASSET_NO ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
    public class IndieSearchPRWOMaterial : IndieSql
    {
        private int maxRows = 50;
        private string queryString = "";
        private string whereClause = "1=1 ";
        private string columns = "AM.REF_WO, AM.WO_REQDATE, AM.WO_PROBLEM, AM.SPAREPARTS,AM.WO_ASSET_NO";
        private string columnsSimple = "REF_WO, WO_REQDATE, WO_PROBLEM, SPAREPARTS";
        private string from = "WORKORDERFORPR_V AM";

        public IndieSearchPRWOMaterial(string dbType, string queryString, int maxRows = 500)
        {
            this.DatabaseType = dbType;
            this.queryString = queryString;
            this.maxRows = maxRows;
            string[] queryArray = queryString.Split(new char[] { ' ' });

            //foreach (string query in queryArray)
            //{
            //var query2 = queryString.ToLower();
            //whereClause += " OR LOWER(AM.REF_WO) = ('" + query2 + "')";




        }

        public override StmtOracle OracleSql
        {
            get
            {
                return new StmtOracle() { SqlStatement = "SELECT " + columnsSimple + " FROM (SELECT rownum RN, " + columns + " FROM " + from + " WHERE " + whereClause + ") WHERE RN<=" + maxRows.ToString() + "ORDER BY STAFF_ID " };
            }
        }

        public override StmtSqlServer SqlServerSql
        {
            get
            {
                return new StmtSqlServer() { SqlStatement = "SELECT TOP " + maxRows.ToString() + " " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  REF_WO ASC" };
            }
        }

        public override StmtMySql MySql
        {
            get
            {
                return new StmtMySql() { SqlStatement = "SELECT " + columns + " FROM " + from + " WHERE " + whereClause + " ORDER BY  ASSET_NO ASC" + " LIMIT " + maxRows.ToString() };
            }
        }


    }
}
