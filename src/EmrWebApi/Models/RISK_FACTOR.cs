﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
	[PetaPoco.PrimaryKey("RISK_ID", autoIncrement = true)]
	public class RISK_FACTOR
	{
		public int RISK_ID { get; set; }
		public string ASSET_NO { get; set; }
		public int RISK_FACTORCODE { get; set; }
		public string RISK_CATEGORY { get; set; }
		public string RISK_GROUP { get; set; }
		public int RISK_SCORE { get; set; }
		public string CREATED_BY { get; set; }
		public DateTime? CREATED_DATE { get; set; }
	}
}