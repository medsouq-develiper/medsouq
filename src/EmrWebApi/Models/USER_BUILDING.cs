﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("BUILDING_CODE", autoIncrement = false)]
    public class USER_BUILDING
    {
        public string BUILDING_CODE { get; set; }
        public string USER_ID { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DATE { get; set; }
    }
}