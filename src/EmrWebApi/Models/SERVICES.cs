﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
     [PetaPoco.PrimaryKey("SERVICE_ID", autoIncrement = false)]
    public class SERVICES
    {
        public string SERVICE_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string DESCRIPTION_SHORT { get; set; }
        public string DESCRIPTION_OTH { get; set; }
        public string CREATED_BY { get; set; }

        [PetaPoco.Ignore]
        public DateTime? CREATED_DATE { get; set; }
    }
}