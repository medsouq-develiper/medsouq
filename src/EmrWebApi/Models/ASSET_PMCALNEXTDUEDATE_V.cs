﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class ASSET_PMCALNEXTDUEDATE_V
    {
        public string ASSET_NO { get; set; }
        public DateTime? NEXTDUECAL_DATE { get; set; }
        public DateTime? NEXT_PM_DUE { get; set; }
    }
}