﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("TICKET_ID", autoIncrement = true)]
    public class AM_HELPDESK
    {
        public int TICKET_ID { get; set; }
        public string CONTACT_PERSON { get; set; }
       public DateTime? ISSUE_DATE { get; set; }
        public string CONTACT_NO { get; set; }
        public string LOCATION { get; set; }

        public string ASSET_NO { get; set; }
        public string ISSUE_DETAILS { get; set; }

        public string STEPDETAILS { get; set; }
        public string ASSIGN_TO { get; set; }

       public DateTime? ASSIGN_DATE { get; set; }
        public string REF_WO { get; set; }

        public string PRIORITY { get; set; }
        public string CATEGORY { get; set; }
		public string WO_STATUS { get; set; }
		public string REQ_STAFF_ID { get; set; }
		public string COST_CENTER { get; set; }
		public string STATUS { get; set; }
        public string RECEIVED_BY { get; set; }

       public DateTime? RECEIVED_DATE { get; set; }
        public string ASSIGN_BY { get; set; }
       public DateTime? ASSIGN_BYDATE { get; set; }
		public DateTime? WO_CLOSEDDATE { get; set; }
		
		public string CREATED_BY { get; set; }
       public DateTime? CREATED_DATE { get; set; }
        public string ACCEPTED_BY { get; set; }
        public DateTime? ACCEPTED_DATE { get; set; }

        public string ACTION_DETAILS { get; set; }
    }
}