﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
	[PetaPoco.PrimaryKey("RISK_FACTORCODE", autoIncrement = true)]
	public class RISK_CATEGORYFACTOR
	{
		public int RISK_FACTORCODE { get; set; }
		public string RISK_DESCRIPTION { get; set; }
		public string RISK_CATEGORYCODE { get; set; }
		public string RISK_SCORE { get; set; }
		public string CREATED_BY { get; set; }
		public DateTime? CREATED_DATE { get; set; }
	}
}