﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class PURCHASEORDER_V
    {
        public string PURCHASE_NO { get; set; }
        public string REF_WO { get; set; }
        public string SUPPLIER_ID { get; set; }
        public string ASSIGN_TO { get; set; }
        public DateTime? REQ_DATE { get; set; }
    }
}