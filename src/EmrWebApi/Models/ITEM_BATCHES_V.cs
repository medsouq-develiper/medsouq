﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class ITEM_BATCHES_V
    {
        public string BATCH_ID { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string STORE { get; set; }
        public string EXPIRY_DATE_STR { get; set; }
        public string CREATED_DATE { get; set; }
       
      
        public string PRODUCT_NAME { get; set; }
        public int STOCK_ONHAND { get; set; }
        public string INVOICENO { get; set; }

        public string STORE_DESC { get; set; }
        public string BARCODE_ID { get; set; }
        public string SUPPLIER { get; set; }
        public string ITEMTYPE { get; set; }
        public string CLIENT_CODE { get; set; }
        public string UO_DESC { get; set; }
        public string CLIENT_NAME { get; set; }
        public string STATUS { get; set; }
        public DateTime? EXPIRY_DATE { get; set; }
        public int? SYSREPORTID { get; set; }

    }
}