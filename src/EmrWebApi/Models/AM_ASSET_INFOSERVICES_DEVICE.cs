﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
	[PetaPoco.PrimaryKey("DEVICE_ID", autoIncrement = false)]
	public class AM_ASSET_INFOSERVICES_DEVICE
	{
		public string DEVICE_ID { get; set; }
		public string ASSET_NO { get; set; }
		public string COMPUTER_BASE { get; set; }
		public string COMPUTER_TYPE { get; set; }
		
		public string MODEL { get; set; }
		public string SERIAL_NO { get; set; }
		public string OPERATING_SYSTEM { get; set; }
		public string FRIMWARE_VERSION { get; set; }

		public string PROCESSOR { get; set; }
		public string MEMORY { get; set; }
		public string HARD_DRIVE { get; set; }
		public string BIOS_VERSION { get; set; }
		public string ADDITIONAL_DRIVE { get; set; }
		public string WIRELESS_FREQ { get; set; }

		public string SOFTWARE_PACKAGE { get; set; }
		public string NET_DROP_LOCATION { get; set; }
		public string NET_IP_ADDRESS { get; set; }
		public string NET_SUB_NET { get; set; }
		public string NET_SUB_MASK { get; set; }
		public string NET_ROUTER_TYPE { get; set; }

		public string NET_MAC { get; set; }
		public string NET_AE_TITLE { get; set; }
		public string NET_MODEM_PHONE { get; set; }
		public string NET_VIRTUAL_LAN { get; set; }
		public string NET_ROUTER_LOC1 { get; set; }
		public string NET_ROUTER_LOC2 { get; set; }

		public string NET_ROUTER_LOC3 { get; set; }
		public string NET_ROUTER_LOC4 { get; set; }
		public string NET_ROUTER_LOC5 { get; set; }
		public string NET_SPEED { get; set; }
		public string STATUS { get; set; }
		public string CREATED_BY { get; set; }
		public DateTime? CREATED_DATE { get; set; }
	}
}