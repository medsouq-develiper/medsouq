﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class NOTIFY_SETUP_V
    {
        public string NOTIFY_ID { get; set; }
        public string STAFF_ID { get; set; }
        public string SMS_MESSAGE { get; set; }
        public string EMAIL_MESSAGE { get; set; }
        public string SMS_FLAG { get; set; }
        public string EMAIL_FLAG { get; set; }
        public string DASHBOARD_FLAG { get; set; }
        public string DASHBOARD_MESSAGE { get; set; }
    }
}