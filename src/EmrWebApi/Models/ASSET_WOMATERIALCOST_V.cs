﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class ASSET_WOMATERIALCOST_V
    {
        public string ASSET_NO { get; set; }
        public decimal M_COST_CURRENT { get; set; }
        public decimal M_COST_ALL { get; set; }
        public decimal M_COST_PREVIOUS { get; set; }
    }
}