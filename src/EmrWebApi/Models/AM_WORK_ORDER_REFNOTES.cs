﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("ID", autoIncrement = true)]
    public class AM_WORK_ORDER_REFNOTES
    {
        public string ASSET_NO { get; set; }
        public string REQ_NO { get; set; }
        public int ID { get; set; }
        public string WO_NOTES { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DATE { get; set; }
    }
}