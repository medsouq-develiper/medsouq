﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class DASHBOARD_CLIENT_V
    {
        public string CLIENT_CODE { get; set; }
        public string STOCK_ONHAND { get; set; }
        public string TICKET_TOTAL { get; set; }
        public string ASSET_TOTAL { get; set; }
        public string OUTOFSTOCK { get; set; }
        public string COSTUMER_REQ { get; set; }
        public string COSTUMER_REC { get; set; }
        public string STOCK_NEAREXPIRY { get; set; }
        public string RECALL_TOTAL { get; set; }
    }
}