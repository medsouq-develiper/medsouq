﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("AM_METERREADINGID", autoIncrement = true)]
    public class AM_METERREADING
    {
        public int AM_METERREADINGID { get; set; }
        public string ASSET_NO { get; set; }
        public int AM_INSPECTION_ID { get; set; }
        public int REQ_NO { get; set; }
       public DateTime? READING_DATE { get; set; }
        public int NEXT_PMDUE { get; set; }
        public decimal CURRENT_READING { get; set; }
        public string REMARKS { get; set; }
        public string CREATED_BY { get; set; }
       public DateTime? CREATED_DATE { get; set; }

    }
}