﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class PARTNO_BYPARTID_V
    {
        public string ID_PARTS { get; set; }
        public string PART_NUMBERS { get; set; }
    }
}