﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("CODEID", autoIncrement = false)]
    public class COSTCENTER_DETAILS
    {
        public string CODEID { get; set; }
        public string DESCRIPTION { get; set; }
        public string SHORTNAME{ get; set; }
        public string DESCRIPTIONOTH { get; set; }
        public string STATUS { get; set; }
        public string ADDRESS { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string EMAILADDRESS { get; set; }
        public string CONTACTNO { get; set; }
        public string CONTACTNO2 { get; set; }
        public string CONTACTNO3 { get; set; }
        public string CONTACTPERSON { get; set; }
        public string CONTACTPERSONEMAILADD { get; set; }
        public string CONTACTPERSONCONTACTNO { get; set; }
        public string LOGOPATH { get; set; }
        public string COSTCENTERTYPE { get; set; }
        
        public string CREATEDBY { get; set; }
        public DateTime? CREATEDDATE { get; set; }
        public string LASTUPDATEBY { get; set; }
        public DateTime? LASTUPDATEDATE { get; set; }
    }
}