﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("NOTE_ID", autoIncrement = true)]
    public class AM_ASSET_NOTES
    {
        public string ASSET_NO { get; set; }
        public int NOTE_ID { get; set; }
        public string NOTES { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DATE { get; set; }
    }
}