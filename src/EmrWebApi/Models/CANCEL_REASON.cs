﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class CANCEL_REASON
    {
        public string CANCEL_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string DESCRIPTION_SHORT { get; set; }
        public string CANCEL_TYPE { get; set; }
        public string DESCRIPTION_OTH { get; set; }
    }
}