﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("ID", autoIncrement = true)]
    public class AM_WORK_ORDER_ASSIGN_LOG
    {
        public int ID { get; set; }
        public string REQ_NO { get; set; }
        public string ASSIGN_TO { get; set; }
        public string ASSIGN_BY { get; set; }       
        public DateTime? ASSIGN_DATE { get; set; }
    }
}