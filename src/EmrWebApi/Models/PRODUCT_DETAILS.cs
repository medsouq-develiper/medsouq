﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("PRODUCT_ID", autoIncrement = false)]
    class PRODUCT_DETAILS
    {
        public string PRODUCT_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string SHORT_DESCRIPTION { get; set; }
        public string CATEGORY_ID { get; set; }
        public string GROUP_ID { get; set; }

        public string STOCK_FLAG { get; set; }
        public string DEPARTMENT_ID { get; set; }
        public string SECTION_ID { get; set; }
        public string CURRENT_PRICE { get; set; }
        public decimal DISCOUNT { get; set; }
        public string ORDER_FLAG { get; set; }

        public string STATUS { get; set; }
        public string STOCK_UNIT { get; set; }
        public string ICD_CODE { get; set; }
        public string CLASS_ID { get; set; }
        public string CLASS_SUB_ID { get; set; }
        public string ORDER_DETAILS_FLAG { get; set; }
        
        public string AVERAGE_COST { get; set; }
        public string SERVICE_ID { get; set; }
        public string SALE_UNIT { get; set; }
        public string STOCK_TAKECOUNT_FLAG { get; set; }
        public string BILL_FLAG { get; set; }
        public string ASSET_FLAG { get; set; }

        public DateTime? LAST_UPDATED { get; set; }
        public string PUSH_FLAG { get; set; }

    }
}
