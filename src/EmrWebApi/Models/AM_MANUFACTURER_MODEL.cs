﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("MODEL_ID", autoIncrement = true)]
    public class AM_MANUFACTURER_MODEL
    {
        public string VENDOR { get; set; }
        public int MODEL_ID { get; set; }
        public string TYPE { get; set; }
        public string MODEL { get; set; }
        public decimal LIST_PRICE { get; set; }
        public string FACILITY { get; set; }
        public string MODEL_NAME { get; set; }
        public decimal SALVAGE_VALUE { get; set; }

        public string TEST_EQU { get; set; }
        public decimal REPLACEMENT_COST { get; set; }
        public string CRITICAL { get; set; }

        public int COVERAGE_ID { get; set; }
        public int FREQUENCY { get; set; }
        public string FREQ_UNIT { get; set; }
        public string NOTE { get; set; }
        public DateTime? EXPIRE_DATETIME { get; set; }
        public string DESCRIPTION { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DATE { get; set; }
    }
}