﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("ASSET_INFOSERVICES_ID", autoIncrement = true)]
    public class AM_ASSET_INFOSERVICES
    {
        public int ASSET_INFOSERVICES_ID { get; set; }
        public string ASSET_NO { get; set; }
        public string PURPOSE { get; set; }
        public string SOFTWARE_VERSION { get; set; }
        public string REVISION_LEVEL { get; set; }
        public string AVAILABLE_MODULE { get; set; }
        public string CONNECTED_NETWORK { get; set; }
        public string SUPPORT_HIPAA { get; set; }
       public DateTime? SUPPORT_LASTUPDATED { get; set; }
        public string SUPPORT_DIAGCONNECTION { get; set; }
        public string SUPPORT_CHARGE { get; set; }
        public decimal? SUPPORT_COST { get; set; }
        public string SUPPORT_SERVSOFTWARE  { get; set; }
        public string SUPPORT_VERSION { get; set; }
        public string SUPPORT_CRITICALUPDATE { get; set; }
		public string SUPPORT_REMOTE { get; set; }
		public string BACKUP_METHOD { get; set; }
		public string BACKUP_SCHEDULE { get; set; }
		public string RECOVERY_POINT { get; set; }
		public string RECOVERY_TIME { get; set; }
		public string RECOVERY_PRIORITY { get; set; }
		public string RECOVERY_OFFSITE { get; set; }
		public string CREATED_BY { get; set; }
       public DateTime? CREATED_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
       public DateTime? LAST_UPDATED_DATE { get; set; }
    }
}
