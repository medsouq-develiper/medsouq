﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("AM_STOCKTRANSACTION_MASTERID", autoIncrement = true)]
    public class AM_STOCKTRANSACTION_MASTER
    {
        public int AM_STOCKTRANSACTION_MASTERID { get; set; }
        public string TRANS_ID { get; set; }
        public string TRANS_TYPE { get; set; }
        public int STORE_FROM { get; set; }
        public int STORE_TO { get; set; }

        public string TRANS_STATUS { get; set; }

        public string REQUEST_BY { get; set; }
       public DateTime? REQUEST_DATE { get; set; }

        public string DELIVER_BY { get; set; }
       public DateTime? DELIVER_DATE { get; set; }
        public string RECEIVE_BY { get; set; }
        public string TRANSDETFLAG { get; set; }
       public DateTime? RECEIVE_DATE { get; set; }

        public string REMARKS { get; set; }

        public string TRANSFER_REMARKS { get; set; }
        public string RECEIVE_REMARKS { get; set; }

        public string FOREIGN_CURRENCY { get; set; }
        public string SHIP_VIA { get; set; }

        public decimal? TOTAL_AMOUNT { get; set; }
        public int? TOTAL_NOITEMS { get; set; }
        public string SUPPLIER_ID { get; set; }
        public string SUPPLIER_ADDRESS1 { get; set; }
        public string SUPPLIER_CITY { get; set; }
        public string SUPPLIER_STATE { get; set; }
        public string SUPPLIER_ZIPCODE { get; set; }
        public string SUPPLIER_CONTACT_NO1 { get; set; }
        public string SUPPLIER_CONTACT_NO2 { get; set; }
        public string SUPPLIER_CONTACT { get; set; }
        public string SUPPLIER_EMAIL_ADDRESS { get; set; }
        public string OTHER_INFO { get; set; }
        public DateTime? SHIP_DATE { get; set; }
        public DateTime? EST_SHIP_DATE { get; set; }
        public decimal? HANDLING_FEE { get; set; }
        public string TRACKING_NO { get; set; }
        public decimal? EST_FREIGHT { get; set; }
        public decimal? FREIGHT { get; set; }
        public string FREIGHT_PAIDBY { get; set; }
        public string FREIGHT_CHARGETO { get; set; }
        public string TAXABLE { get; set; }
        public string TAX_CODE { get; set; }
        public decimal? TAX_AMOUNT { get; set; }
        public decimal? DISCOUNT { get; set; }
        public string FREIGHT_POLINE { get; set; }
        public string FREIGHT_SEPARATE_INV { get; set; }
        public decimal? INSURANCE_VALUE { get; set; }
        public string PO_BILLCODE_ADDRESS { get; set; }
        public string POINVOICENO { get; set; }
    }
}