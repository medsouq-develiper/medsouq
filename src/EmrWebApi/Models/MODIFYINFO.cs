﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class MODIFYINFO
    {
        public string statusBy { get; set; }
        public string StatusDate { get; set; }
        public string category { get; set; }
        public string refid { get; set; }
        public string filetype { get; set; }
        public string filename { get; set; }
        public string process { get; set; }

    }
}