﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
	public class SearchFilter
	{
		public string FromDate { get; set; }
		public string ToDate { get; set; }
		public string Status { get; set; }
		public int FromStore { get; set; }
		public int ToStore { get; set; }
		public string ProblemType { get; set; }
		public string Priority { get; set; }
		public string JobType { get; set; }
		public string AssetFilterCondition { get; set; }
		public string AssetFilterDevice { get; set; }
		public string AssetFilterCostCenter { get; set; }
		public string AssetFilterSuupiler { get; set; }
		public string AssetFilterManufacturer { get; set; }
		public string AssetFilterBuilding { get; set; }
        public string userConstCenter { get; set; }
        public string POStatus { get; set; }
        public string staffId { get; set; }
        public string interval { get; set; }
        public string frequency { get; set; }
        public string notifyStaffId { get; set; }
        public string notifyStatus { get; set; }
        public string assetNo { get; set; }
        public string woIdList { get; set; }
        public string AssetIdList { get; set; }
        public string assignTo { get; set; }
        public string assignType { get; set; }
        public string PR_NO { get; set; }
        public string PO_NO { get; set; }
        public string REF_WO { get; set; }
        public string overDue { get; set; }
        public bool unAssigned { get; set; }
        public string CloneId { get; set; }
        public string CostCenterStr { get; set; }
        public string ResponsibleCenterStr { get; set; }
        public string ManufacturerStr { get; set; }
        public string SupplierStr { get; set; }
        public string pmtype { get; set; }
        public string staffStatus { get; set; }
        public string userid { get; set; }
        public string curpassword { get; set; }
        public string newpassword { get; set; }
        public string groupCode { get; set; }
        public string woStatus { get; set; }
        public string assetDescription { get; set; }
        public string AssetFilterStatus { get; set; }
        public string HelpDeskRequestStatus { get; set; }
        public string companyAccess { get; set; }
        public string clientCode { get; set; }
        public string ItemName { get; set; }
        public string Category { get; set; }
        public string SerialBatch { get; set; }
        public string ItemModel { get; set; }
        public string TransType { get; set; }
        public string StoreId { get; set; }
        public string ItemType { get; set; }
        public string InvoiceNo { get; set; }
        public string Reporting { get; set; }
        public string RecallReturn { get; set; }
        public string SysReportId { get; set; }
        public string warrantyFrom { get; set; }
        public string warrantyTo { get; set; }
    }
}