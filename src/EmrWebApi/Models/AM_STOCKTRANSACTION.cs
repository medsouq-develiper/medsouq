﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("AM_STOCKTRANSACTIONID", autoIncrement = true)]
    public class AM_STOCKTRANSACTION
    {
        public int AM_STOCKTRANSACTIONID { get; set; }
        public string TRANS_ID { get; set; }
        public string TRANS_TYPE { get; set; }
        public string ID_PARTS { get; set; }
        public int STORE_FROM { get; set; }
        public int STORE_TO { get; set; }

        public string TRANS_STATUS { get; set; }

        public int QTY { get; set; }

        public int QTY_TRANSFER { get; set; }
        public string REQUEST_BY { get; set; }
       public DateTime? REQUEST_DATE { get; set; }

        public string DELIVER_BY { get; set; }
       public DateTime? DELIVER_DATE { get; set; }
        public string RECEIVE_BY { get; set; }
        public string TRANSDETFLAG { get; set; }
       public DateTime? RECEIVE_DATE { get; set; }

        public string REMARKS { get; set; }

        public string INVOICENO { get; set; }
        public string TRANSFER_REMARKS { get; set; }
        public string RECEIVE_REMARKS { get; set; }
        public decimal PRICE { get; set; }

        public bool? VAT_FLAG { get; set; }
        public decimal? VAT { get; set; }
    }
}