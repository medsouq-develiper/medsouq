﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmrWebApi.Models
{
    public class USER_APPLICATION_V
    {
        public string DESCRIPTION { get; set; }
        public string DESCRIPTION_OTH { get; set; }
        public string USER_ID { get; set; }
        public string APP_HOMEURL { get; set; }
        public string APP_ID { get; set; }
    }
}
