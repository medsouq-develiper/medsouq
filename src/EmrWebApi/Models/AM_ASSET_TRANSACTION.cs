﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("TRANSID", autoIncrement = true)]
    public class AM_ASSET_TRANSACTION
    {
        public int TRANSID { get; set; }
        public string ASSET_NO { get; set; }
        public DateTime? COSTCENTERINSTALLDATE { get; set; }
        public string COSTCENTERCODE { get; set; }
        public string COSTCENTERINVOICENO { get; set; }
        public DateTime? COSTCENTERPURCHASEDATE { get; set; }
        public decimal? COSTCENTERPURCHASECOST { get; set; }
        public string COSTCENTERASSETCOND { get; set; }
        public DateTime? RETURNEDDATE { get; set; }
        public string RETURNEDREASON { get; set; }
        public string RETURNEDREMARKS { get; set; }
        public string STATUS { get; set; }
        public DateTime CREATEDDATE { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime? RETURNCREATEDDATE { get; set; }
        public string RETURNCREATEDBY { get; set; }
        public string RETURNEDTYPE { get; set; }
        public string RETURNTOCOSTCENTERBY { get; set; }
        public DateTime? RETURNTOCOSTCENTERDATE { get; set; }
        public int? SYSREPORTID { get; set; }
    }
}