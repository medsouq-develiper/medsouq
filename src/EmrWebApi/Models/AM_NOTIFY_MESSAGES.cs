﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("MESSAGE_ID", autoIncrement = true)]
    public class AM_NOTIFY_MESSAGES
    {
        public int MESSAGE_ID { get; set; }
        
        public int NOTIFY_ID { get; set; }
        public string SMS_MESSAGE { get; set; }
        public string EMAIL_MESSAGE { get; set; }
        public string MOBILENO { get; set; }
        public string EMAIL_ADDRESS { get; set; }
        public string REF_CODE { get; set; }
        public string SMS_FLAG { get; set; }
        public string EMAIL_FLAG { get; set; }
        public string STAFF_ID { get; set; }
        public string NOTIFY_MESSAGE { get; set; }
        public string DASHBOARD_FLAG { get; set; }
        public string STATUS { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DATE { get; set; }
        public DateTime? DELIVERED_DATE { get; set; }
        public string UPDATED_BY { get; set; }
        public DateTime? UPDATED_DATE { get; set; }
    }
}