﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
	[PetaPoco.PrimaryKey("CLIENT_CODE", autoIncrement = false)]
	public class AM_CLIENT_INFO
	{
		
		public string CLIENT_CODE { get; set; }
		public string DESCRIPTION { get; set; }
		public string DESCRIPTION_OTH { get; set; }
		public string DESCRIPTION_SHORT { get; set; }
		public string ADDRESS { get; set; }
		public string BILL_ADDRESS { get; set; }
		public string CONTACT_NO1 { get; set; }
		public string CONTACT_NO2 { get; set; }
		public string CITY { get; set; }
		public string ZIPCODE { get; set; }
		public string STATE { get; set; }
		public string CLIENT_LOGO { get; set; }
		public string EMAIL_ADDRESS { get; set; }
        public string HEADING { get; set; }
        public string HEADING2 { get; set; }
        public string HEADING3 { get; set; }
        public string HEADING_OTH { get; set; }
        public string HEADING_OTH2 { get; set; }
        public string HEADING_OTH3 { get; set; }
        public string COMPANY_LOGO { get; set; }
        public string BILL_CONTACT_NO1 { get; set; }
		public string BILL_CONTACT_NO2 { get; set; }
		public string BILL_CITY { get; set; }
		public string BILL_ZIPCODE { get; set; }
		public string BILL_STATE { get; set; }
		public string BILL_EMAIL_ADDRESS { get; set; }
		public string WEBAPI { get; set; }

        public string CONTACT_PERSON { get; set; }
        public string CONTACTNO_PERSON { get; set; }
        public string CONTACT_EMAILADD { get; set; }

        public string CLIENT_PATH_LOGO { get; set; }
        public string CLIENT_PATH_PRINT_LOGO { get; set; }
        public string COMPANY_PATH_LOGO { get; set; }

        public string CREATED_BY { get; set; }
		public DateTime? CREATED_DATE { get; set; }
	}
}