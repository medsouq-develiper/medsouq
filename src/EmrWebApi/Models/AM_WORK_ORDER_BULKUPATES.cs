﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class AM_WORK_ORDER_BULKUPATES
    {
        public string REQ_NO { get; set; }
        public string WO_STATUS { get; set; }
        public string ASSIGN_BY { get; set; }
        public string CLOSED_BY { get; set; }
        public string ASSIGN_TO { get; set; }
        public DateTime? CLOSED_DATE { get; set; }
        public DateTime? ASSIGN_DATE { get; set; }
    }
}