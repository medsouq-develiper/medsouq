﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("CLIENT_ID", autoIncrement = false)]
    public class CLIENT_DETAILS
    {
        public string CLIENT_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string DESCRIPTION_OTH { get; set; }
        public string DESCRIPTION_SHORT { get; set; }
        public string CLIENT_ADDRESS { get; set; }
        public string PRIMARY_CONTACT { get; set; }
        public string SECONDARY_CONTACT { get; set; }
        public string BILLING_FLAG { get; set; }
        public string EMAIL_ADDRESS { get; set; }


    }
}