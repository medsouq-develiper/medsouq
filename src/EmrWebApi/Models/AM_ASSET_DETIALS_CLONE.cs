﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
   
    public class AM_ASSET_DETAILS_CLONE
    {
        public string ASSET_NO { get; set; }
        public string CLONE_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string CATEGORY { get; set; }
        public string MANUFACTURER { get; set; }
        public string SERIAL_NO { get; set; }
        public string MODEL_NO { get; set; }
        public string MODEL_NAME { get; set; }
        public string FACILITY { get; set; }
        public string BUILDING { get; set; }
        public string COST_CENTER { get; set; }
        public int RISK_INCLUSIONFACTOR { get; set; }
        public string LOCATION { get; set; }
        public DateTime? LOCATION_DATE { get; set; }
        public DateTime? INSERVICE_DATE { get; set; }
        public string STATUS { get; set; }
        public DateTime? STATUS_DATE { get; set; }


        public string SUPPLIER { get; set; }
        public string PURCHASE_NO { get; set; }
        public decimal PURCHASE_COST { get; set; }
        public DateTime? PURCHASE_DATE { get; set; }
        public decimal? REPLACEMENT_COST { get; set; }
        public DateTime? REPLACEMENT_DATE { get; set; }
        public decimal? SALVAGE_VALUE { get; set; }
        public DateTime? SALVAGE_DATE { get; set; }

        public DateTime? SUN_AM { get; set; }
        public DateTime? SUN_PM { get; set; }
        public DateTime? MON_AM { get; set; }
        public DateTime? MON_PM { get; set; }
        public DateTime? TUE_AM { get; set; }
        public DateTime? TUE_PM { get; set; }
        public DateTime? WED_AM { get; set; }
        public DateTime? WED_PM { get; set; }
        public DateTime? THUR_AM { get; set; }
        public DateTime? THUR_PM { get; set; }
        public DateTime? FRI_AM { get; set; }
        public DateTime? FRI_PM { get; set; }
        public DateTime? SAT_AM { get; set; }
        public DateTime? SAT_PM { get; set; }

        public string WO_REFERENCE_NOTES { get; set; }
        public string ERECORD_GENERAL_NOTES { get; set; }

        public string SERV_DEPT { get; set; }
        public string SPECIALTY { get; set; }
        public string INHOUSE_OUTSOURCE { get; set; }
        public string PRIMARY_EMPLOYEE { get; set; }
        public string SECONDARY_EMPLOYEE { get; set; }

        public string EST_ACQUISITIONCOST { get; set; }

        public string RESPONSIBLE_CENTER { get; set; }
        public string ASSET_CLASSIFICATION { get; set; }
        public int DEPRECIATED_LIFE { get; set; }
        public int USEFULL_LIFE { get; set; }
        public int YEARTODATEOTH { get; set; }
        public int PREVIOUSYROTH { get; set; }
        public int LIFETODATEOTH { get; set; }
        public int HAYEARTODATEOTH { get; set; }
        public string CONDITION { get; set; }
        public DateTime? CONDITION_DATE { get; set; }
        public DateTime? WARRANTY_EXPIRYDATE { get; set; }

        public int? TERM_PERIOD { get; set; }
        public string WARRANTY_INCLUDE { get; set; }
        public string WARRANTY_NOTES { get; set; }
        public string WARRANTY_FREQUNIT { get; set; }
        public string RISKGROUP { get; set; }
        public string RISKCLA { get; set; }
        public string RISKCLS { get; set; }
        public string RISKCMT { get; set; }
        public string RISKEQF { get; set; }
        public string RISKESE { get; set; }
        public string RISKEVS { get; set; }
        public string RISKEVT { get; set; }
        public string RISKFPF { get; set; }
        public string RISKLIS { get; set; }
        public string RISKMAR { get; set; }
        public string RISKPCA { get; set; }
        public string LOCATIONFR { get; set; }
        public string NOTE_RECORDED_BY { get; set; }
        public string WONOTE_RECORDED_BY { get; set; }
        public DateTime? WONOTE_RECORDED_DATE { get; set; }
        public string LAST_REQ_NO { get; set; }



        public DateTime? NOTE_RECORDED_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DATE { get; set; }


    }
}
