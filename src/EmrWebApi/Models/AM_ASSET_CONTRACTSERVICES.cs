﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("ASSET_CONTRACTSERVICE_ID", autoIncrement = true)]
    public class AM_ASSET_CONTRACTSERVICES
    {
        public int ASSET_CONTRACTSERVICE_ID { get; set; }
        public string CONTRACT_ID { get; set; }
        public string PROVIDER { get; set; }
        public string TYPE_COVERAGE { get; set; }
        public DateTime? TERM_EFFECTIVE { get; set; }
        public DateTime? TERM_TERMINATION { get; set; }
        public string REMARKS { get; set; }
        public string COVERAGE_LABOR { get; set; }
        public string COVERAGE_PARTSMATERIALS { get; set; }
        public string ASSET_NO { get; set; }
        public string PURCHASE_ORDER { get; set; }
        public DateTime? TERM_REVIEW { get; set; }
        public decimal COST_TOTAL { get; set; }
        public decimal COST_PERPIECE { get; set; }
        public decimal COST_LASTPERIOD { get; set; }
        public decimal LABOR_RATEPERHOUR { get; set; }
        public string LABOR_HOURS { get; set; }
        public string CREATED_BY { get; set; }
       public DateTime? CREATED_DATE { get; set; }
        public string LAST_UPDATED_BY { get; set; }
       public DateTime? LAST_UPDATED_DATE { get; set; }

    }
}
