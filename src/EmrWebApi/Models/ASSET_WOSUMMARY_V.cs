﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class ASSET_WOSUMMARY_V
    {
        public string ASSET_NO { get; set; }
        public int PM_COUNT_CURRENT { get; set; }
        public int OT_COUNT_CURRENT { get; set; }
        public int PM_COUNT_PREVIOUS { get; set; }
        public int OT_COUNT_PREVIOUS { get; set; }

        public int PM_DURATION_CURRENT { get; set; }
        public int OT_DURATION_CURRENT { get; set; }

        public int PM_DURATION_PREVIOUS { get; set; }
        public int OT_DURATION_PREVIOUS { get; set; }

        public int PM_COUNT_ALL { get; set; }
        public int OT_COUNT_ALL { get; set; }

        public int PM_DURATION_ALL { get; set; }
        public int OT_DURATION_ALL { get; set; }
    }
}