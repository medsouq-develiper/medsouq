﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("ID", autoIncrement = true)]
    public class AM_WORK_ORDERS_AUDIT
       
    {

        public string REQ_NO { get; set; }
        public string REQ_BY { get; set; }
        public string REQUESTER_CONTACT_NO { get; set; }
        public DateTime? REQ_DATE { get; set; }
        public string PROBLEM { get; set; }
        public string WO_STATUS { get; set; }
        public string REQ_REMARKS { get; set; }
        public string REQ_TYPE { get; set; }
        public string ASSET_NO { get; set; }
        public string JOB_TYPE { get; set; }
        public string SERV_DEPT { get; set; }
        public string ASSIGN_TYPE { get; set; }
        public string ASSIGN_TO { get; set; }
        public string ASSIGN_BY { get; set; }
        public DateTime? ASSIGN_DATE { get; set; }
        public string SPECIALTY { get; set; }
        public int AM_INSPECTION_ID { get; set; }
        public int TICKET_ID { get; set; }
        public string REQ_STAFF_ID { get; set; }
        public string JOB_URGENCY { get; set; }
        public string PARTS_CONFIRMED { get; set; }
        public DateTime? JOB_DUE_DATE { get; set; }
        public decimal JOB_DURATION { get; set; }
        public string MATERIALS_REMARKS { get; set; }
        public string WO_REFERENCE_NOTES { get; set; }
        public string WONOTE_RECORDED_BY { get; set; }
        public DateTime? WONOTE_RECORDED_DATE { get; set; }
        public string FAULT_FINDING { get; set; }
        public string ASSET_LOCATION { get; set; }
        public string ASSET_CONDITION { get; set; }
        public string ASSET_COSTCENTER { get; set; }
        public string ASSET_COSTCENTERCODE { get; set; }
        public string ASSIGN_OUTSOURCE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DATE { get; set; }
        public string LAST_MODIFY_BY { get; set; }
        public DateTime? LAST_MODIFY_DATE { get; set; }
        public string CLOSED_BY { get; set; }
        public DateTime? CLOSED_DATE { get; set; }
    }
}