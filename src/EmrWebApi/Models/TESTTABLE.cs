﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("ID", autoIncrement = true)]
    public class TESTTABLE
    {
        public int ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string OTHERDESCRIPTION { get; set; }
        public string SUPPLIERCODE { get; set; }
        public string STATUS { get; set; }
        public string JOBTYPE { get; set; }
        public string MANUFACTURERCODE { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime? CREATEDDATE { get; set; }
    }
}