﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("BUILDING_CODE", autoIncrement = false)]
    public class AM_CLIENT_BUILDING
    {
        public string BUILDING_CODE { get; set; }
        
        public string CLIENT_CODE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DATE { get; set; }
       
       
    }
}