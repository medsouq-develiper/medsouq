﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("AM_INSPECTIONTRANSID", autoIncrement = true)]
    public class AM_INSPECTIONTRANS
    {
        public int AM_INSPECTIONTRANSID { get; set; }
        public int AM_INSPECTION_ID { get; set; }
        public int REQ_NO { get; set; }
        public string ASSET_NO { get; set; }

       public DateTime? INSPECTION_DATE { get; set; }
       public DateTime? INSPECTION_TIME { get; set; }
        public string EMPLOYEE { get; set; }
        public string INSPECTION_RESULT { get; set; }
        public decimal TOTAL_HOURWS { get; set; }
        public decimal RATE { get; set; }
        public string COST_CENTER { get; set; }
        public string INSP_TYPE { get; set; }
        public string FAILURE { get; set; }
        public string INSPROCEDURE { get; set; }
        public string REMARKS { get; set; }

        public string CREATED_BY { get; set; }
       public DateTime? CREATED_DATE { get; set; }
    }
}