﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("ID", autoIncrement = true)]
    public class SYSTEMTRANSACTION_REPORT_SCANDOC
    {
        public int ID { get; set; }
        public int SYSREPORTID { get; set; }
        public string CATEGORY { get; set; }
        public string DOCPATH { get; set; }
    }
}