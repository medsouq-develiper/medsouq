﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("AM_INSPECTION_ID", autoIncrement = true)]
    public class AM_INSPECTION
    {
        public int AM_INSPECTION_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string PROCESS { get; set; }
        public string INSPECTION_TYPE { get; set; }

        public string ASSET_TYPECOVERED { get; set; }
        public string REMARKS { get; set; }

        public string CREATED_BY { get; set; }
       public DateTime? CREATED_DATE { get; set; }
    }
}