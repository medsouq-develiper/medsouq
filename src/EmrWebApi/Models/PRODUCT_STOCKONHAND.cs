﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmrWebApi.Models
{
   
   [PetaPoco.PrimaryKey("PRODUCT_CODE", autoIncrement = false)]
  
    public class PRODUCT_STOCKONHAND
    {
        public string PRODUCT_CODE { get; set; }
        public int MIN { get; set; }
        public int MAX { get; set; }
        public float QTY { get; set; }

        public float COMMITTED_QTY { get; set; }
        public int COMMITTED_WOQTY { get; set; }
        public int PR_QTY { get; set; }
        public string STORE { get; set; }
        public string UOM { get; set; }
		public string STATUS { get; set; }
        public string LOCATION { get; set; }
        public string LOCATION2 { get; set; }
        public string LOCATION3 { get; set; }
    }
}
