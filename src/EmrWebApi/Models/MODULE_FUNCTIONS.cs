﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("MODULE_FUNCTION_ID", autoIncrement = false)]
    public class MODULE_FUNCTIONS
    {
        
        public string MODULE_FUNCTION_ID { get; set; }
        public string MODULE_ID { get; set; }
        public string FUNCTION_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string FUNCTION_TYPE { get; set; }
        public string URL { get; set; }
        public string DESCRIPTION_OTH { get; set; }
        public string STATUS { get; set; }
        public string APP_ID { get; set; }
    }
}