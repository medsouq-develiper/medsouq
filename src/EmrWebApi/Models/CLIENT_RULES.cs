﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("ID", autoIncrement = false)]
    public class CLIENT_RULES
    {
        public string ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string STATUS { get; set; }
        public string RULE_VALUE { get; set; }
        public string INTERVAL { get; set; }
        public string LAST_MODIFIED_BY { get; set; }
        public DateTime? LAST_MODIFIED_DATE { get; set; }
    }
}