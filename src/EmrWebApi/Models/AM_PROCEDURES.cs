﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
	[PetaPoco.PrimaryKey("PROCEDURE_ID", autoIncrement = false)]
	public class AM_PROCEDURES
	{
		public string PROCEDURE_ID { get; set; }
		public string DESCRIPTION { get; set; }
		public string SERV_DEPT { get; set; }
		public string ASSIGN_TO { get; set; }

		public decimal AVERAGE_TIME { get; set; }
		public string PROCEDURE_TYPE { get; set; }
		public string REMARKS { get; set; }
		public string STATUS { get; set; }
		public string TEST_DVICE { get; set; }
		public string INC_TASKRESULT { get; set; }
		public string ROUND { get; set; }
		public string ALLOW_OVERRIDE { get; set; }

		public string CREATED_BY { get; set; }
		public DateTime? CREATED_DATE { get; set; }
	}
}