﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
	[PetaPoco.PrimaryKey("STAFFCODE", autoIncrement = false)]
	public class STAFF_STORE
	{
		public string STAFFCODE { get; set; }
		public int CODE { get; set; }
		public string CREATED_BY { get; set; }
		public DateTime? CREATED_DATE { get; set; }
	}
}