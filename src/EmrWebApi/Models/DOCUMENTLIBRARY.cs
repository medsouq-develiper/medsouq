﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("DOCID", autoIncrement = true)]
    public class DOCUMENTLIBRARY
    {
        public string DOCID { get; set; }
        public string DOCNO { get; set; }
      
        public string CREATEDBY { get; set; }
        public DateTime CREATEDDATE { get; set; }
        public string FILENAME { get; set; }
        public string FILETYPE { get; set; }
        public string STATUS { get; set; }
        public string DOCCATEGORY { get; set; }
        public string SEARCHKEY { get; set; }
        public string PATHFILE { get; set; }
        public string DEVICETYPECODE { get; set; }
        public string MANUFACTURERCODE { get; set; }
        public string MODELCODE { get; set; }
        public string FILESIZE { get; set; }

        public string DELETEDBY { get; set; }
        public DateTime? DELETEDDATE { get; set; }

    }
}