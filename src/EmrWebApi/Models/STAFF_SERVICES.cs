﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("STAFF_SERVICE_ID", autoIncrement = false)]
    public class STAFF_SERVICES
    {
        public string STAFF_SERVICE_ID { get; set; }
        public string STAFF_ID { get; set; }
        public string SERVICE_ID { get; set; }
        public string CREATED_BY { get; set; }
        [PetaPoco.Ignore]
        public DateTime? CREATED_DATE { get; set; }
    }
}