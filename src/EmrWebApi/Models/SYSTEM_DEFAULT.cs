﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class SYSTEM_DEFAULT
    {
        public string CODEID { get; set; }
        public string DESCRIPTION { get; set; }
        public string SYSDEFAULT { get; set; }
        public string SYSDEFAULTVAL { get; set; }
    }
}