﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("NO", autoIncrement = false)]
    public class ASSET_SERIALNO
    {
        public string NO { get; set; }
        public string SERIALNO { get; set; }
        public string PRODUCT_CODE { get; set; }
    }
}