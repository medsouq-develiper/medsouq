﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("SEQ_NO", autoIncrement = true)]
    public class GROUP_PRIVILEGES
    {
        public int SEQ_NO { get; set; }
        public string STAFF_GROUP_ID { get; set; }
        public string PRIVILEGES_ID { get; set; }
        public string CREATED_BY { get; set; }
       public DateTime? CREATED_DATE { get; set; }
    }
}