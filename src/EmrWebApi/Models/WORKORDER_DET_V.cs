﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class WORKORDER_DET_V
    {
        public string REQ_NO { get; set; }
        public string ASSET_NO { get; set; }
        public string BUILDING { get; set; }
    }
}