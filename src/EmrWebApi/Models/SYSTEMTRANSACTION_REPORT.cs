﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("ID", autoIncrement = true)]
    public class SYSTEMTRANSACTION_REPORT
    {
        public int ID { get; set; }
        public int SYSTRANSID { get; set; }
        public string SEARCHTYPE { get; set; }
        public string SEARCHKEY { get; set; }
        public string ACTION { get; set; }
        public string RECALLCATEGORY { get; set; }
        public string RECALLFIRMREFNO { get; set; }
        public string RECALLTYPE { get; set; }
        public int? RECALLTIMEFRAME { get; set; }
        public int? RESPONSETIMEFRAME { get; set; }
        public string HHLEVEL { get; set; }
        public string PROBABILITY { get; set; }
        public string DETECTABILITY { get; set; }
        public string DDSA { get; set; }
        public string QSA { get; set; }
        public string REASON { get; set; }
        public string REPORTS { get; set; }
        public string ACTIONREPORTS { get; set; }
        public string CLOSEREPORTS { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime CREATEDDATE { get; set; }

        public string REPORTSTATUS { get; set; }
        public string REPORTEDBY { get; set; }  
        public DateTime? REPORTEDDATE { get; set; }
        public string ACTIONBY { get; set; }
        public DateTime? ACTIONDATE { get; set; }
        public string CLOSEBY { get; set; }
        public DateTime? CLOSEDATE { get; set; }
        public bool ACKNOWLEDGEFLAG { get; set; }
        public bool ACKNOWLEDGEFLAG1 { get; set; }
        public bool ACKNOWLEDGEFLAG2 { get; set; }
        public bool ACKNOWLEDGEFLAG3 { get; set; }
    }
}