﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class DISPATCH_WOPARTS_V
    {
        public string LABOUR_TYPE { get; set; }
        public string AM_WORKORDER_LABOUR_ID { get; set; }
        public string PROBLEM { get; set; }
        public DateTime? REQ_DATE { get; set; }
        public string WO_STATUS { get; set; }
        public string REQ_NO { get; set; }
        public string ASSET_NO { get; set; }
        public string ASSET_DESC { get; set; }
        public int NO_ITEMS { get; set; }

        public string MATERIALS_REMARKS { get; set; }

    }
}