﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class ASSET_WOPMOTCOST_V
    {
        public string ASSET_NO { get; set; }
        public decimal PM_COST_CURRENT { get; set; }
        public decimal OT_COST_CURRENT { get; set; }
        public decimal PM_COST_PREVIOUS { get; set; }
        public decimal OT_COST_PREVIOUS { get; set; }
        public decimal PM_COST_ALL { get; set; }
        public decimal OT_COST_ALL { get; set; }
    }
}