﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("ID", autoIncrement = true)]
    public class AM_NOTIFYSETUPASSIGNED
    {
        public int ID { get; set; }
        public int NOTIFY_ID { get; set; }
        public string STAFF_ID { get; set; }
        public string STAFF_GROUP_ID { get; set; }
        
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DATE { get; set; }
    }
}