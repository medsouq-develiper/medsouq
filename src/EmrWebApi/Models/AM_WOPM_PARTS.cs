﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("WOPM_PARTS_ID", autoIncrement = true)]
    public class AM_WOPM_PARTS
    {
        public int WOPM_PARTS_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string ID_PARTS { get; set; }
        public int REFID { get; set; }
        public string REF_TYPE { get; set; }
        public decimal QTY { get; set; }
        public decimal PRICE_ITEM { get; set; }
        public decimal EXTENDED { get; set; }
        public string REMARKS { get; set; }
        public string BILLABLE { get; set; }
        public string CREATED_BY { get; set; }
       public DateTime? CREATED_DATE { get; set; }
    }
}