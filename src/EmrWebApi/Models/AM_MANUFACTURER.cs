﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("MANUFACTURER_ID", autoIncrement = false)]
    public class AM_MANUFACTURER
    {
        public string MANUFACTURER_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string ADDRESS1 { get; set; }
        public string ADDRESS2 { get; set; }
        public string ADDRESS3 { get; set; }
        public string TYPE { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string ZIP { get; set; }
        
        public string TERM { get; set; }
        public decimal MIN_ORDERAMOUNT { get; set; }
        public string REMARKS { get; set; }

        public string STATUS { get; set; }
        public string ADDITIONAL_INFO { get; set; }
        public string CONTACT { get; set; }
        public string CONTACT_NO1 { get; set; }
        public string CONTACT_NO2 { get; set; }
        public string CONTACT_NO3 { get; set; }
        public string PROVIDER_TYPE { get; set; }
        public string EMAIL_ADDRESS { get; set; }
        public string CREATED_BY { get; set; }
       public DateTime? CREATED_DATE { get; set; }
    }
}
