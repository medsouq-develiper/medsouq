﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("ID", autoIncrement = true)]
    public class AM_ASSET_WARRANTY
    {
        public int ID { get; set; }
        public string ASSET_NO { get; set; }
        public string FREQUENCY { get; set; }
        public int FREQUENCY_PERIOD { get; set; }
        public DateTime? WARRANTY_EXPIRY { get; set; }
        public DateTime? WARRANTY_EXTENDED { get; set; }
        public string WARRANTY_INCLUDED { get; set; }
        public string WARRANTY_NOTES { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DATE { get; set; }
        public string LAST_MODIFIED_BY { get; set; }
        public DateTime? LAST_MODIFIED_DATE { get; set; }
    }
}