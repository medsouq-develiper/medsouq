﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
	public class AM_PURCHASEORDERLIST
	{
		public string DESCRIPTION { get; set; }
		public string ID_PARTS { get; set; }
		public decimal PO_PRICE { get; set; }
		public int PURCHASE_QTY { get; set; }
	}
}