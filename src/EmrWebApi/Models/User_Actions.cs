﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class User_Actions
    {
        public string STAFF_ID { get; set; }
        public string USER_ID { get; set; }
        public DateTime? LOGON_DATE { get; set; }
        public String ScreenId { get; set; }
    }
}