﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
 
namespace EmrWebApi.Models
{
    public class MODULES
    {
        public string MODULE_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public int MODULE_GROUP { get; set; }
        public string DESCRIPTION_OTH { get; set; }
    }
}