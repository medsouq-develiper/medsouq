﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class PPM_Tracking
    {
        public string  Staff_Id { get;set; }
        public string PPM_Date { get; set; }
        public string PPM_Status { get; set; }
    }
}