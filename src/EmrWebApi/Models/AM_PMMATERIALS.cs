﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("AM_PMMATERIAL_ID", autoIncrement = true)]
    class AM_PMMATERIALS
    {
        public int AM_PMMATERIAL_ID { get; set; }
        public string REF_NO { get; set; }
        public string PRODUCT_ID { get; set; }
        public int QTY { get; set; }
        public string CATEGORY { get; set; }
        public string ASSET_NO { get; set; }
        public string CREATED_BY { get; set; }
       public DateTime? CREATED_DATE { get; set; }
    }
}
