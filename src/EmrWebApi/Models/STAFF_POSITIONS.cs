﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
     [PetaPoco.PrimaryKey("STAFF_POSITION_ID", autoIncrement = false)]
    public class STAFF_POSITIONS
    {
        public string STAFF_POSITION_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string DESCRIPTION_SHORT { get; set; }
        public string DESCRIPTION_OTH { get; set; }
    }
}