﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("SUPPLIER_ID", autoIncrement = true)]
    public class PRODUCT_SUPPLIERS
    {
        public string SUPPLIER_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string ADDRESS { get; set; }
        public string CONTACT_NO { get; set; }
        public string CONTACT_PERSON { get; set; }
        public string CONTACT_OTH { get; set; }
        public string STATUS { get; set; }
        public string CREATED_BY { get; set; }

       public DateTime? CREATED_DATE { get; set; }
    }
}
