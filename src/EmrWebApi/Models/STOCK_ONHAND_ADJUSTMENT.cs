﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations.Schema;
namespace EmrWebApi.Models
{
	[PetaPoco.PrimaryKey("ID", autoIncrement = true)]
	public class STOCK_ONHAND_ADJUSTMENT
	{
		public int ID { get; set; }
		public int STORE_ID { get; set; }
		public float QTY_ADJUST { get; set; }
		public float QTY_ACTUAL { get; set; }
		public string REASON { get; set; }
		public string ID_PARTS { get; set; }
		public string CREATED_BY { get; set; }
		public DateTime? CREATED_DATE { get; set; }
		public int MIN_ADJUST { get; set; }
		public int MAX_ADJUST { get; set; }
		public string STATUS_ACTUAL { get; set; }
		public int MIN_ACTUAL { get; set; }
		public int MAX_ACTUAL { get; set; }
		public string STATUS_ADJUST { get; set; }
	}
}