﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("TRANS_ID", autoIncrement = true)]
    public class PRODUCT_STOCKTRANSACTION
    {
        public string TRANS_ID { get; set; }
        public string TRANS_TYPE { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string FROM_STORE { get; set; }
        public string TO_STORE { get; set; }
        public int QTY { get; set; }
        public string TRANS_STATUS { get; set; }
        public string UOM { get; set; }
        public string COMMENT { get; set; }
        public int CREATED_BY { get; set; }
        public decimal PRICE { get; set; }
        public string BARCODE_ID { get; set; }
        
       public DateTime? CREATED_DATE { get; set; }
    }
}
