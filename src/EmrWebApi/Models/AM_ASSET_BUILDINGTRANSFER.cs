﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("TRANSFERID", autoIncrement = true)]
    public class AM_ASSET_BUILDINGTRANSFER
    {
        public int TRANSFERID { get; set; }
        public string FROM_BUILDING { get; set; }
        public string TO_BUILDING { get; set; }
        public DateTime? TRANSFER_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DATE { get; set; }
    }
}