﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
	[PetaPoco.PrimaryKey("LOG_ID", autoIncrement = true)]
	public class AM_WORK_ORDERS_STATUS_LOG
	{
		public int LOG_ID { get; set; }
		public string REQ_NO { get; set; }
		public string FACILITY { get; set; }
		public string WO_STATUS { get; set; }
		public string STAFF_ID { get; set; }
		public string PALM_NAME { get; set; }
		public int RECORD_ID { get; set; }
		public DateTime? RECORDED_DATE { get; set; }
		public string IP_ADDRESS { get; set; }
		public string HOSTNAME { get; set; }
	}
}