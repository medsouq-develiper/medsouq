﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("AM_ASSET_PMSCHEDULE_ID", autoIncrement = false)]
    public class AM_ASSET_PMSCHEDULES
    {
        public string AM_ASSET_PMSCHEDULE_ID { get; set; }
        public string ASSET_NO { get; set; }
        public string PM_PROCEDURE { get; set; }
        public string PM_PROCESS{ get; set; }
        public string PM_STATUS { get; set; }
        public string SERVICE_DEPARTMENT { get; set; }
        public string EMPLOYEE_TYPE { get; set; }
        public string EMPLOYEE_INFO { get; set; }
        public string FREQUENCY_PERIOD { get; set; }
        public string LAST_REQ_NO { get; set; }
        public string INTERVAL { get; set; }
        public DateTime? NEXT_PM_DUE { get; set; }
        public string RANDOM_PMSCHEDULE { get; set; }
        public string FLOATING_PMSCHEDULE { get; set; }
        public string STATUS { get; set; }
        public string REMARKS { get; set; }
        public string GRACE_PERIOD_TYPE { get; set; }
        public string GRACE_PERIOD { get; set; }
        public decimal TIME_PERFORM { get; set; }
        public string PM_PRIORITY    { get; set; }
       public DateTime? SESSION_START { get; set; }
       public DateTime? SESSION_END    { get; set; }
        public string CREATED_BY { get; set; }
        public string LAST_MODIFIED_BY { get; set; }
        public string CHECKER_UPDATE { get; set; }
        public DateTime? CREATED_DATE { get; set; }
       public DateTime? LAST_MODIFIED_DATE { get; set; }
        public DateTime? LAST_DUE_DATE { get; set; }
    }
}
