﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("HOSPITAL_ID",autoIncrement=false)]
    public class HOSPITAL_DETAIL
    {
        public string HOSPITAL_ID { get; set; }
        public string HOSPITAL_FULLNAME { get; set; }
        public string ADDRESS { get; set; }
        public string HOSPITAL_FULLNAME_OTH { get; set; }
    }
}