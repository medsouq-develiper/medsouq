﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
	[PetaPoco.PrimaryKey("STAFF_ID", autoIncrement = false)]
	public class STAFF
	{
		public string USER_ID { get; set; }
		public string STAFF_ID { get; set; }
		public string STAFF_POSITION_ID { get; set; }
		public string FIRST_NAME { get; set; }
		public string LAST_NAME { get; set; }
		public string FIRST_NAME_OTH { get; set; }
		public string LAST_NAME_OTH { get; set; }
		public string PASSWORD { get; set; }
		public DateTime? PASSWORD_EXPIRY { get; set; }
		public string STAFF_GROUP_ID { get; set; }
		public string CONTACT_PRIMARY { get; set; }
		public string CONTACT_SECONDARY { get; set; }
		public string EMAIL_ADDRESS { get; set; }
		public string STATUS { get; set; }
		public string CREATED_BY { get; set; }
		public string SHORT_NAME { get; set; }
		public string SHORT_NAME_OTH { get; set; }
		public string SYSTEM_ID { get; set; }
		public string DOCTOR_APPS { get; set; }

		public string CLIENT_CODE { get; set; }
		public string SYSTEM_LOOKUP { get; set; }
		public string API { get; set; }
		public string HOME_DEFAULT { get; set; }
		public string APP_ID { get; set; }
        public string LOCATION { get; set; }

        public string SRVC_DEPT { get; set; }
		public decimal CHRG_RATE { get; set; }
		public decimal PAY_RATE { get; set; }
		public string BEEPER_PH { get; set; }
		public string CONTACT_3 { get; set; }
		public decimal SCHED_HRS { get; set; }
		public string PAY_PERIOD { get; set; }
		public string PAY_ST_DAY { get; set; }
		public string PROMPT_PASSWORD { get; set; }
		public string REQUESTER { get; set; }
		public DateTime? HIRE_DATETIME { get; set; }
		public DateTime? TERM_DATETIME { get; set; }
        public DateTime? CREATED_DATE { get; set; }
       
    }
}