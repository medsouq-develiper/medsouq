﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("AM_WORKORDER_LABOUR_ID", autoIncrement = false)]
    public class AM_WORKORDER_LABOUR
    {
        public int AM_WORKORDER_LABOUR_ID { get; set; }
        public string ASSET_NO { get; set; }
        public int REQ_NO { get; set; }
        public string EMPLOYEE { get; set; }
        public string RESPONSE { get; set; }
        public string BILLABLE { get; set; }
        public decimal? HOURS { get; set; }
       public DateTime? LABOUR_DATE { get; set; }
        public string LABOUR_ACTION { get; set; }
        public decimal? LABOUR_COST { get; set; }
        public decimal? TOTAL_COST { get; set; }
        public decimal LABOUR_CHARGE { get; set; }
        public decimal? LABOUR_RATE { get; set; }
        public string LABOUR_TYPE { get; set; }
        public string CONTRACT_NO { get; set; }
        public string SUPPLIER_ID { get; set; }
        public string FLAT_FEE { get; set; }
        public string TRAVEL { get; set; }
        public string RATE_TYPE { get; set; }
        public string REF_ID { get; set; }
        public string MATERIALS_REMARKS { get; set; }
        
        public string CREATED_BY { get; set; }
       public DateTime? CREATED_DATE { get; set; }

    }
}
