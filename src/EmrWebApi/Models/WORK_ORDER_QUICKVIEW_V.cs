﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class WORK_ORDER_QUICKVIEW_V
    {
        public string QVWONO { get; set; }
        public string QVREQNO { get; set; }
        public string QVREQ_BY { get; set; }
        public string QVREQ_CONTACTNO { get; set; }
        public string QVREQ_DATE { get; set; }
        public string QVREQ_TYPE { get; set; }
        public string QVAM_INSPECTION_ID { get; set; }
        public string QVPROBLEM { get; set; }
        public string QVWO_STATUS { get; set; }
        public string QVCLOSED_DATE { get; set; }
        public string QVJOB_TYPE { get; set; }
        public string QVSERV_DEPT { get; set; }
        public string QVSPECIALTY { get; set; }
        public string QVASSIGN_TYPE { get; set; }
        public string QVASSIGN_TO { get; set; }
        public string QVJOB_URGENCY { get; set; }
        public string QVJOB_DURATION { get; set; }
        public string QVJOB_DUE_DATE { get; set; }
        public string QVASSET_NO { get; set; }
        public string QVDESCRIPTION { get; set; }
        public string QVCATEGORY { get; set; }
        public string QVSERIAL_NO { get; set; }
        public string QVMODEL_NO { get; set; }
        public string QVBUILDING { get; set; }
        public string QVWOCOST_CENTER { get; set; }
        public string QVWOLOCATION { get; set; }
        public string QVCONDITION { get; set; }
        public string QVSUPPLIER { get; set; }
        public string QVWONOTES { get; set; }
        

    }
}