﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class PRODUCT_CATEGORY
    {
        public string CATEGORY_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string DESCRIPTION_SHORT { get; set; }
    }
}