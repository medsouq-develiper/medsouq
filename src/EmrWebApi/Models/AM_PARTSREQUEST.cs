﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("AM_PARTREQUESTID", autoIncrement = true)]
    public class AM_PARTSREQUEST
    {
        public string AM_PARTREQUESTID { get; set; }
        public string DESCRIPTION { get; set; }
        public int QTY { get; set; }
        public int QTY_RETURN { get; set; }
        public int QTY_FORRETURN { get; set; }
        public int USED_QTY { get; set; }
        public decimal PRICE { get; set; }
        public decimal EXTENDED { get; set; }
        public string CONTRACT_NO { get; set; }
        public string BILLABLE { get; set; }
        public string REMARKS { get; set; }
        public string REF_WO { get; set; }
        public string STATUS { get; set; }
        public string WO_LABOURTYPE { get; set; }
        public string ID_PARTS { get; set; }
        public string REF_ASSETNO { get; set; }
        public string TRANSFER_FLAG { get; set; }
        public int PURCHASE_NO { get; set; }
        public int TRANSFER_QTY { get; set; }
        public int AM_WORKORDER_LABOUR_ID { get; set; }
        public string STATUS_BY { get; set; }
       public DateTime? STATUS_DATE { get; set; }
        public string REQ_BY { get; set; }
        public string REQ_CONTACTNO { get; set; }
        public string REQ_DEPT { get; set; }
       public DateTime? REQ_DATE { get; set; }
        public int PENDING_QTY { get; set; }
        public string PR_REQUEST_ID { get; set; }
        public string PR_REQUEST_QTY { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DATE { get; set; }
        public int STORE_ID { get; set; }
        public int FOR_COLLECTION_QTY { get; set; }
        public string DISPATCH_BY { get; set; }
        public DateTime? DISPATCH_DATE { get; set; }
        public string FOR_COLLECTION_CANCELBY { get; set; }
        public DateTime? FOR_COLLECTION_CANCELDATE { get; set; }
        public string LAST_MODIFIEDBY { get; set; }
        public DateTime? LAST_MODIFIEDDATE { get; set; }
    }
}
