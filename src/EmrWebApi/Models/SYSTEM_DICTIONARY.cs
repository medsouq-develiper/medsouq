﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("DICTIONARY_ID", autoIncrement = true)]
    public class SYSTEM_DICTIONARY
    {
        public int DICTIONARY_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string DESCRIPTION_OTH { get; set; }
        public string CATEGORY { get; set; }
        public string REMARKS { get; set; }
    }
}
