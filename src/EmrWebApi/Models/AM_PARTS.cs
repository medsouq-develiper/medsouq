﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("ID_PARTS", autoIncrement = false)]
    public class AM_PARTS
    {
        public string ID_PARTS { get; set; }
        public string DESCRIPTION { get; set; }
        public string ANCILLARY { get; set; }
        public string CATEGORY { get; set; }
        public string COMMITTABLE { get; set; }
        public string EXPENSE_RECEIVED { get; set; }
        public string ACCOUNT { get; set; }
        public string STATUS { get; set; }
        public string FACILITY { get; set; }
        public string REMARKS { get; set; }
        public string ALTERNATIVE_ITEM { get; set; }
        public decimal? LAST_POCOST { get; set; }
        public decimal? PART_COST { get; set; }
        public DateTime? LAST_PODATE { get; set; }
        public string LAST_POSUPPLIER { get; set; }
        public string PART_KIT { get; set; }
        public int PART_ID { get; set; }
        public string STOCK_ITEM_FLAG { get; set; }
        public string MODEL_ID { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DATE { get; set; }
        public string TYPE_ID { get; set; }
        public string UOM_ID { get; set; }
    }
}