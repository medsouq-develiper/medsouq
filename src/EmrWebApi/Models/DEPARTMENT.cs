﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("DEPARTMENT_ID",autoIncrement=false)]
    public class DEPARTMENT
    {
        public string DEPARTMENT_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string DESCRIPTION_SHORT { get; set; }
        public string DESCRIPTION_OTH { get; set; }
    }
}