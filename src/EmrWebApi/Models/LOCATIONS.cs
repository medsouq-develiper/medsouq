﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("LOCATION_ID", autoIncrement = false)]
    public class LOCATIONS
    {
        public string LOCATION_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string DESCRIPTION_OTH { get; set; }
        public string DESCRIPTION_SHORT { get; set; }
    }
}