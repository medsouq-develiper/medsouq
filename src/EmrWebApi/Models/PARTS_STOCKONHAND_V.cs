﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class PARTS_STOCKONHAND_V
    {
        public string PRODUCT_CODE { get; set; }
        public int QTY { get; set; }
        public int COMMITTED_QTY { get; set; }
        public int COMMITTED_WOQTY { get; set; }
        public int AVAILABLE_STOCK { get; set; }

        public int STORE { get; set; }
        public string PRODUCT_DESC { get; set; }
        public string STORE_DESC { get; set; }

    }
}