﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class GetCreditResult
    {
        //public string Credit { get; set; }
        //public string Description { get; set; }
        //public string Status { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Tagname { get; set; }
        public string RecepientNumber { get; set; }
        public string VariableList { get; set; }
        public string ReplacementList { get; set; }
        public string Message { get; set; }
        public string SendDateTime { get; set; }
        public bool EnableDR { get; set; }
    }
}