﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models

{
     [PetaPoco.PrimaryKey("LOV_LOOKUP_ID", autoIncrement = false)]
    public class LOV_LOOKUPS
    {
        public string LOV_LOOKUP_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string DESCRIPTION_SHORT { get; set; }
        public string DESCRIPTION_OTH { get; set; }
        public string CATEGORY { get; set; }
		public string STATUS { get; set; }
        public string ACTION { get; set; }
        public string WO_DURATION { get; set; }
        public DateTime? LAST_UPDATED { get; set; }
        public string LAST_UPDATEDBY { get; set; }
        public DateTime? CREATEDDATE { get; set; }
        public string CREATEDBY { get; set; }
        public string PUSH_FLAG { get; set; }

    }
}