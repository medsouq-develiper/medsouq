﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
	public class PM_SCHEDULE_V
	{
		public string ASSET_NO { get; set; }
		public string PM_PROCEDURE { get; set; }
		public string PM_DESCRIPTION { get; set; }
		public string SERVICE_DEPARTMENT { get; set; }
		public string SRVDEPT_DESC { get; set; }
		public DateTime NEXT_PM_DUE { get; set; }
		public string GRACE_PERIOD_TYPE { get; set; }
		public string PM_PRIORITY { get; set; }
		public DateTime SESSION_START { get; set; }
		public DateTime SESSION_END { get; set; }
		public string PM_SCHEDULE { get; set; }
	}
}