﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("USER_APPID", autoIncrement = true)]
    public class USER_APPLICATION
    {

        public string USER_ID { get; set; }
        public string APP_ID { get; set; }
        public string USER_APPID { get; set; }

        public string CREATED_BY { get; set; }

       public DateTime? CREATED_DATE { get; set; }

    }
}