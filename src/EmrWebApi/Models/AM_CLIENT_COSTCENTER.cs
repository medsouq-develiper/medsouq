﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("COSTCENTERCODE", autoIncrement = false)]

    public class AM_CLIENT_COSTCENTER
    {
        public string COSTCENTERCODE { get; set; }

        public string CLIENTCODE { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime CREATEDDATE { get; set; }
    }
}