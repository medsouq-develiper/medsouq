﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("PRIVILEGE_ID", autoIncrement = false)]
    public class STAFF_PRIVILEGES
    {
        public string PRIVILEGE_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string DESCRIPTION_OTH { get; set; }
    }
}