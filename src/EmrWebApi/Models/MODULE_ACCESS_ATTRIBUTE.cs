﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("MODULE_FUNCTION_ID", autoIncrement = false)]
   
    
    public class MODULE_ACCESS_ATTRIBUTE
    {
        public string MODULE_FUNCTION_ID { get; set; }
        public string ATTRIBUTE { get; set; }
        public string MODULE_ID { get; set; }
        public string ATTRIBUTE_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime? CREATE_DATE { get; set; }
        public string URL { get; set; }
        public string GROUP_ID { get; set; }
        public string DESCRIPTION_OTH { get; set; }
        [PetaPoco.Ignore]
        public string STAFF_GROUP_DESCRIPTION { get; set; }

    }
}