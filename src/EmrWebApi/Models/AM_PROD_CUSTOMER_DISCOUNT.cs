﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("id")]
    public class AM_PROD_CUSTOMER_DISCOUNT
    {
        public int id { get; set; }
        public string am_part_id { get; set; }
        public string discount_type { get; set; }
        public decimal? discount { get; set; }
        public DateTime? validity_date_from { get; set; }
        public DateTime? validity_date_to { get; set; }
        public string status { get; set; }
    }
}