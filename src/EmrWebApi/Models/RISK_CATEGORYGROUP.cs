﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
	[PetaPoco.PrimaryKey("RISK_CATEGORYCODE", autoIncrement = false)]
	public class RISK_CATEGORYGROUP
	{
		public string RISK_CATEGORYCODE { get; set; }
		public string RISK_CATEGORY { get; set; }
		public string RISK_GROUP { get; set; }
		public string CREATED_BY { get; set; }
		public DateTime? CREATED_DATE { get; set; }
	}
}