﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class CLIENT_V
    {
        public string Client_Code { get; set; }
        public string COSTCENTER { get; set; }
        public string ShortName { get; set; }
        public string Description { get; set; }
        public string LogoPath { get; set; }
        public string EmailAddress { get; set; }
        public string ContactNo { get; set; }
        public string ContactNo2 { get; set; }
    }
}