﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("SYSTRANSID", autoIncrement = true)]
    public class SYSTEM_TRANSACTIONDETAILS
    {
        public int SYSTRANSID { get; set; }
        public string REFTRANSID { get; set; }
        public string REFCODE { get; set; }
        public string REFDESCRIPTION { get; set; }
        public string MESSAGE { get; set; }
        public string MESSAGEOTH { get; set; }
        public string STATUS { get; set; }
        public DateTime? REFDATE { get; set; }
        public string REFBATCHSERIALNO { get; set; }
        public DateTime? REFEXPIRY { get; set; }
        public decimal? REFCOST { get; set; }
        public int? REFQTY { get; set; }
        public string REFASSETNO { get; set; }
        public string REFWONO { get; set; }
        public string REFPRODUCTCODE { get; set; }
        public string REFHELPDESKCODE { get; set; }
        public string REFCOSTCENTERCODE { get; set; }
        public string REASONTYPE { get; set; }
        public string REFMODEL { get; set; }
        public string REFTYPE { get; set; }
        public string REFUOM { get; set; }
        public string REFCOSTCENTERDESCRIPTION { get; set; }
        public string COSTCENTERMESSAGE { get; set; }
        public string REFINVOICENO { get; set; }       
        public DateTime CREATEDDATE { get; set; }
        public string CREATEDBY { get; set; }  
        public string REPORTSTATUS { get; set; }
        public string ACKNOWLEDGEBY { get; set; }
        public DateTime? ACKNOWLEDGEDATE { get; set; }
        public string RECALLBY { get; set; }
        public DateTime? RECALLDATE { get; set; }
        public int? SYSREPORTID { get; set; }
    }
}