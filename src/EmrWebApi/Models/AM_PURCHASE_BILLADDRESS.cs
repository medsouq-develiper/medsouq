﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
	[PetaPoco.PrimaryKey("PO_BILLCODE", autoIncrement = true)]
	public class AM_PURCHASE_BILLADDRESS
	{
		public int PO_BILLCODE { get; set; }
		public string BILL_ADDRESS { get; set; }
		public string BILL_CONTACT_NO1 { get; set; }
		public string BILL_CONTACT_NO2 { get; set; }
		public string BILL_CITY { get; set; }
		public string BILL_ZIPCODE { get; set; }
		public string DESCRIPTION { get; set; }
		public string BILL_STATE { get; set; }
		public string CREATED_BY { get; set; }
		public string STATUS { get; set; }
		public string SHIP_TO { get; set; }
		public string BILL_EMAIL_ADDRESS { get; set; }
		public DateTime? CREATED_DATE { get; set; }

	}
}