﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("PARTSP_ID", autoIncrement = true)]
    public class AM_PARTSUPPLIERDETAILS
    {
        public int PARTSP_ID { get; set; }
        public string SUPPLIER_ID { get; set; }
        public string CUSTOMER_NO { get; set; }
        public int PREFERENCE { get; set; }
        public string PART_NUMBER { get; set; }
        public string PART_DESCRIPTION { get; set; }
        public string EXCHANGEABLE { get; set; }
        public string RETURN_ADDRESS { get; set; }
        public string REMARKS { get; set; }
        public string RETURN_PARTDESC { get; set; }
        public int? RETURN_NODAYS { get; set; }
        public decimal? CREDIT { get; set; }
        public int? UNIT_QTY { get; set; }
        public int? QTY_PERUNIT { get; set; }
        public int? MIN_ORDERQTY { get; set; }
        public int? DELIVERY_LOADTIME { get; set; }
        public string STATUS { get; set; }
        public string ID_PARTS { get; set; }
        public string CREATED_BY { get; set; }
       public DateTime? CREATED_DATE { get; set; }
    }
}