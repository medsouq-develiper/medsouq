﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("ID", autoIncrement = true)]
    public class CLIENTOTHCONTACT
    {
        public string ID { get; set; }
        public string TYPECODE { get; set; }
        public string REFCODE { get; set; }
        public string CLIENTCONTACTTYPE { get; set; }
        public string CLIENTCONTACT { get; set; }
        
        public string CREATEDBY { get; set; }
        public DateTime CREATEDDATE { get; set; }
    }
}