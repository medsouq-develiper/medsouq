﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class ASSET_QUICKVIEW_V
    {
        public string AQV_ASSETDESCRIPTION { get; set; }
        public string AQV_ASSET_NO { get; set; }
        public string AQV_SERIAL_NO { get; set; }
        public string AQV_MODEL_NO { get; set; }
        public string AQV_MODEL_NAME { get; set; }
        public string AQV_BUILDING { get; set; }
        public string AQV_LOCATION { get; set; }
        public string AQV_CONDITION { get; set; }
        public string AQV_STATUS { get; set; }
        public string AQV_RESPOSIBLE_CENTER { get; set; }
        public string AQV_COST_CENTER { get; set; }
        public string AQV_CATEGORY { get; set; }
        public string AQV_CURRENTNOTES { get; set; }
        public string AQV_MANUFACTURER { get; set; }
        public string AQV_PM_TYPE { get; set; }
        public string AQV_PM_FREQUENCY { get; set; }
        public string AQV_PM_INTERVAL { get; set; }
        public string AQV_NEXT_DUE { get; set; }
        public string AQV_SUPPLIER { get; set; }
        public string AQV_INSTALLATION_DATE { get; set; }
        public int AQV_PMCOUNT { get; set; }


    }
}