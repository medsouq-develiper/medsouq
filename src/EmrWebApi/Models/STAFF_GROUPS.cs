﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("STAFF_GROUP_ID", autoIncrement = false)]
    public class STAFF_GROUPS
    {
        public string STAFF_GROUP_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string DESCRIPTION_OTH { get; set; }
        public string DESCRIPTION_SHORT { get; set; }
		public string DASHBOARDURL { get; set; }
		public string CREATED_BY { get; set; }
        public string RESTRICTEDFLAG { get; set; }
        public DateTime? CREATED_DATE { get; set; }
	}
}