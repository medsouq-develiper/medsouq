﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("ID", autoIncrement = true)]
    public class APPROVING_OFFICER
    {
        public int ID { get; set; }
        public string APPROVE_TYPE { get; set; }
        public string STAFF_ID { get; set; }
        public string STATUS { get; set; }
        public int STORE_ID { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DATE { get; set; }
    }
}