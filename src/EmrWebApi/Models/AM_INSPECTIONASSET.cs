﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("AM_INSPECTIONASSETID", autoIncrement = true)]
    public class AM_INSPECTIONASSET
    {
        public int AM_INSPECTIONASSETID { get; set; }
        public int REF_INSPECTION_ID { get; set; }
        public string ASSET_NO { get; set; }

        public string CREATED_BY { get; set; }
       public DateTime? CREATED_DATE { get; set; }
    }
}