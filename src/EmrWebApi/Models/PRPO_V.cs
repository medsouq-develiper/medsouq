﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
	public class PRPO_V
	{
		public string PURCHASE_NO { get; set; }
		public string ID_PARTS { get; set; }
		public string DESCRIPTION { get; set; }
		public decimal PURCHASE_UNITPRICE { get; set; }
		public decimal PURCHASE_QTY { get; set; }
		public decimal QTY { get; set; }
	}
}