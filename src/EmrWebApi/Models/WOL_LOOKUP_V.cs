﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class WOL_LOOKUP_V
    {
        public string REF_ID { get; set; }
        public string REF_DESC { get; set; }
        public string REF_1 { get; set; }
        public string REF_2 { get; set; }
        public string REF_3 { get; set; }
        public string REF_TYPE { get; set; }
    }
}