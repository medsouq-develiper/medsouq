﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class ASSET_WARRANTY_V
    {
        public int ID { get; set; }
        public string ASSET_NO { get; set; }
        public string FREQUENCY { get; set; }
        public int FREQUENCY_PERIOD { get; set; }
        public DateTime? WARRANTY_EXPIRY { get; set; }
        public DateTime? WARRANTY_EXTENDED { get; set; }
        public string WARRANTY_INCLUDED { get; set; }
        public string WARRANTY_NOTES { get; set; }
        public string WARRANTYDESC { get; set; }
    }
}