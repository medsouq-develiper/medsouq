﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class PRODUCT_BATCH_V
    {
        public string BATCH_ID { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string SUPPLIER { get; set; }
        public string STORE { get; set; }
        public string UOM { get; set; }
        public int QTY { get; set; }
        public int QTY_PO_REQUEST { get; set; }
        public int QTY_RECEIVED { get; set; }
        public int QTY_RETURN { get; set; }
        public DateTime? RETURN_DATE { get; set; }
        public string PURCHASE_NO { get; set; }
        public string RETURN_BY { get; set; }
        public string STOREID { get; set; }
        public string MODELCODE { get; set; }

        public string PURCHASE_ID { get; set; }
        public string ID_PARTS { get; set; }
        public string RETURN_REASON { get; set; }
        public decimal PRICE { get; set; }
        public decimal PRICE_DISCOUNT { get; set; }
        public string STATUS { get; set; }
        public string CREATED_BY { get; set; }
        public string TRANS_ID { get; set; }

        public decimal TOTAL_PRICE { get; set; }
        public string BARCODE_ID { get; set; }

        public DateTime? EXPIRY_DATE { get; set; }
        public DateTime? CREATED_DATE { get; set; }
        public DateTime? TRANS_DATE { get; set; }
        public int MIN { get; set; }
        public int MAX { get; set; }
    }
}