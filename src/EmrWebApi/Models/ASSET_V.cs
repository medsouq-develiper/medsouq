﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class ASSET_V
    {
        public int ROWID { get; set; }
        public string DESCRIPTION { get; set; }
        public string ASSET_NO { get; set; }
        public string CATEGORY { get; set; }
        public string MANUFACTURER { get; set; }
        public string SERIAL_NO { get; set; }
        public string MODEL_NO { get; set; }
        public string MODEL_NAME { get; set; }
        public string BUILDING { get; set; }
        public string COST_CENTER { get; set; }
        public string LOCATION { get; set; }
        public string CONDITION { get; set; }
        public string RESPONSIBLE_CENTER { get; set; }
        public string STATUS { get; set; }
        public string RESPCENTER_DESC { get; set; }
        public string COSTCENTER_DESC { get; set; }
        public string CLONE_ID { get; set; }
        public string SUPPLIERNAME { get; set; }
        public string MANUFACTURERNAME { get; set; }
        public string PURCHASE_NO { get; set; }
        public string GROUP_CODE { get; set; }
        public string PM_TYPE { get; set; }
        public string BUILDING_CODE { get; set; }
        public double PURCHASE_COST { get; set; }
        public string COSTCENTERTRANSID { get; set; }
        public string RETURNEDREASON { get; set; }
        public int? SYSREPORTID { get; set; }
    }
}