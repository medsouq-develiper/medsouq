﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class STAFF_LOGON_SESSIONS
    {
        public string SESSION_ID { get; set; }
        public string STAFF_ID { get; set; }
        public string USER_ID { get; set; }
		public string LOGIN_HOSTNAME { get; set; }
		public string LOGIN_IP { get; set; }
		public string LOGINEIIP { get; set; }
		public DateTime? LOGON_DATE { get; set; }
        public DateTime? LOGINEXPIRY { get; set; }
    }
}