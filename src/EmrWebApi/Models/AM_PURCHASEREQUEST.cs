﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("AM_PRREQITEMID", autoIncrement = true)]
    public class AM_PURCHASEREQUEST
    {
        public string AM_PRREQITEMID { get; set; }
        //public string DESCRIPTION { get; set; }
      
        //public string BILLABLE { get; set; }
        public string REMARKS { get; set; }
        public int? REF_WO { get; set; }
        public string STATUS { get; set; }
        public string REF_ASSETNO { get; set; }
        public string REQUEST_ID { get; set; }
        //public int PURCHASE_NO { get; set; }	
		public string REQ_BY { get; set; }
       
        public string REQ_CONTACTNO { get; set; }
        public string REQ_DEPT { get; set; }
       public DateTime? REQ_DATE { get; set; }

        public string CREATED_BY { get; set; }
       public DateTime? CREATED_DATE { get; set; }
        public int STORE_ID { get; set; }

        public string APPROVED_BY { get; set; }
        public DateTime? APPROVED_DATE { get; set; }
        public string CANCELLED_REASON { get; set; }
        public string CANCELLED_BY { get; set; }
        public DateTime? CANCELLED_DATE { get; set; }

        public string RETURNSTATUS_REASON { get; set; }
        public string RETURNSTATUS_BY { get; set; }
        public DateTime? RETURNSTATUS_DATE { get; set; }



        public int? PURCHASE_NO { get; set; }

        public decimal? QTY { get; set; }
        public string SUPPLIER_ID { get; set; }
        public DateTime? PURCHASE_DATE { get; set; }

        public string BILL_ADDRESS { get; set; }
        public string BILL_EMAIL_ADDRESS { get; set; }
        public string BILL_CONTACT_NO1 { get; set; }
        public string BILL_CONTACT_NO2 { get; set; }
        public string BILL_ZIPCODE { get; set; }
        public string BILL_CITY { get; set; }
        public string BILL_STATE { get; set; }
        public string BILL_SHIPTO { get; set; }
        public string FOREIGN_CURRENCY { get; set; }
        public string SHIP_VIA { get; set; }

        public decimal? TOTAL_AMOUNT { get; set; }
        public int? TOTAL_NOITEMS { get; set; }
        public string SUPPLIER_ADDRESS1 { get; set; }
        public string SUPPLIER_CITY { get; set; }
        public string SUPPLIER_STATE { get; set; }
        public string SUPPLIER_ZIPCODE { get; set; }
        public string SUPPLIER_CONTACT_NO1 { get; set; }
        public string SUPPLIER_CONTACT_NO2 { get; set; }
        public string SUPPLIER_CONTACT { get; set; }
        public string SUPPLIER_EMAIL_ADDRESS { get; set; }
        public string OTHER_INFO { get; set; }
        public DateTime? SHIP_DATE { get; set; }
        public DateTime? EST_SHIP_DATE { get; set; }
        public decimal? HANDLING_FEE { get; set; }
        public string TRACKING_NO { get; set; }
        public decimal? EST_FREIGHT { get; set; }
        public decimal? FREIGHT { get; set; }
        public string FREIGHT_PAIDBY { get; set; }
        public string FREIGHT_CHARGETO { get; set; }
        public string TAXABLE { get; set; }
        public string TAX_CODE { get; set; }
        public decimal? TAX_AMOUNT { get; set; }
        public string FREIGHT_POLINE { get; set; }
        public string FREIGHT_SEPARATE_INV { get; set; }
        public decimal? INSURANCE_VALUE { get; set; }
        public string PO_BILLCODE_ADDRESS { get; set; }
        public string POINVOICENO { get; set; }
    }
}