﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("ID", autoIncrement = true)]
    public class AM_PARTS_MODELDETAILS
    {
        public int ID { get; set; }
        public int PART_ID { get; set; }
        public int MODEL_ID { get; set; }
        public string ID_PARTS { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DATE { get; set; }
    }
}