﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("NOTIFY_ID", autoIncrement = true)]
    public class AM_NOTIFYSETUP
    {
        public int NOTIFY_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string SMS_MESSAGE { get; set; }
        public string SMS_MESSAGEOTH { get; set; }
        public string EMAIL_MESSAGE { get; set; }
        public string EMAIL_MESSAGEOTH { get; set; }
        public string SMS_FLAG { get; set; }
        public string EMAIL_FLAG { get; set; }
        public string STATUS { get; set; }
        public string DASHBOARD_FLAG { get; set; }
        public string DASHBOARD_MESSAGE { get; set; }
       
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DATE { get; set; }
    }
}