﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("ID", autoIncrement = true)]
    public class AM_SYSTEM_AUDIT
    {
        public int ID { get; set; }
        public string REF_ID { get; set; }
        public string FIELD_NAME { get; set; }
        public string ACTION_CODE { get; set; }
        public string OLDDATA { get; set; }
        public string NEWDATA { get; set; }
        public string RECORDED_BY { get; set; }
        public DateTime? RECORDED_DATE { get; set; }
        
    }
}