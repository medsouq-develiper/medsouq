﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
	[PetaPoco.PrimaryKey("STAFFCODE", autoIncrement = false)]
	public class STAFF_COSTCENTER_BUILDING
	{
		public string STAFFCODE { get; set; }
		public string CODE { get; set; }
		public string UCATEGORY { get; set; }
		public string CREATED_BY { get; set; }
		public DateTime? CREATED_DATE { get; set; }
	}
}