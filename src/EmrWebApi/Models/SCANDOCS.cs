﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("SCANDOC_ID", autoIncrement = true)]
    public class SCANDOCS
    {
        public string SCANDOC_ID { get; set; }
        public string SCANDOCS_DATA { get; set; }
        public string REF_ID { get; set; }
        public string EVENT_ID { get; set; }
        public string CREATED_BY { get; set; }
       public DateTime? CREATED_DATE { get; set; }
        public string FILE_NAME { get; set; }
        public string FILE_TYPE { get; set; }
        public string STATUS { get; set; }
        public string CATEGORY { get; set; }
        public string NOTES { get; set; }
        public string SCANDOCBKG_DATA { get; set; }
       public DateTime? LAST_UPDATED { get; set; }
        public string PUSH_FLAG { get; set; }
    }
}
