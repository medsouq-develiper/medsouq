﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class STAFF_LOCATIONS
    {
        public string STAFF_ID { get; set; }
        public string LOCATION_ID { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DATE { get; set; }
        public string STATUS { get; set; }
    }
}