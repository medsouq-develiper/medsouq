﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("STORE_ID", autoIncrement = true)]
    public class STORES
    {
        public string STORE_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string LOCATION { get; set; }
        public string CONTACT_NO { get; set; }
        public string STATUS { get; set; }
        public bool COSTCENTERFLAG { get; set; }
        public string CREATED_BY { get; set; }
        public string CLIENT_CODE { get; set; }
        public string ALLOWREQUEST_FLAG { get; set; }
        public string STORE_TYPE { get; set; }

        public DateTime? CREATED_DATE { get; set; }

    }
}
