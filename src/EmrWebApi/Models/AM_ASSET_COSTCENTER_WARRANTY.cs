﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("CCID", autoIncrement = true)]
    public class AM_ASSET_COSTCENTER_WARRANTY
    {
        public int CCID { get; set; }
        public int TRANSID { get; set; }
        public string ASSET_NO { get; set; }
        public string CCFREQUENCY { get; set; }
        public int CCFREQUENCY_PERIOD { get; set; }
        public DateTime? CCWARRANTY_EXPIRY { get; set; }
        public DateTime? CCWARRANTY_EXTENDED { get; set; }
        public string CCWARRANTY_INCLUDED { get; set; }
        public string CCWARRANTY_NOTES { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CREATED_DATE { get; set; }
        public string LAST_MODIFIED_BY { get; set; }
        public DateTime? LAST_MODIFIED_DATE { get; set; }
    }
}