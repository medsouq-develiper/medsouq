﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    [PetaPoco.PrimaryKey("AM_REQITEMID", autoIncrement = true)]
    public class AM_PURCHASEORDER_ITEMS
    {
        public string AM_REQITEMID { get; set; }
        public string ID_PARTS { get; set; }
        public int PURCHASE_NO { get; set; }
        public decimal QTY_RECEIVED { get; set; }
        public decimal PO_UNITPRICE { get; set; }
        public decimal PO_TOTALUNITPRICEITEM { get; set; }
        public decimal QTY_PO_REQUEST { get; set; }       
        public string PR_REQUEST_ID { get; set; }
        public decimal QTY_RETURN { get; set; }
        public string RETURN_REASON { get; set; }
        public DateTime? RETURN_DATE { get; set; }
        public DateTime? RETURN_AMEND_DATE { get; set; }
        public string RETURN_BY { get; set; }


    }
}