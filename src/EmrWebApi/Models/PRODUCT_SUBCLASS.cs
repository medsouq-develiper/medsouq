﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class PRODUCT_SUBCLASS
    {
        public string SUBCLASS_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public string DESCRIPTION_SHORT { get; set; }
        public string CLASS_ID { get; set; }
        public string GROUP_ID { get; set; }
        public string CATEGORY_ID { get; set; }
        public string STATUS { get; set; }
        public string CREATE_BY { get; set; }
        public DateTime? CREATE_DATE { get; set; }
    }
}