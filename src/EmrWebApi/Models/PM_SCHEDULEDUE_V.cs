﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmrWebApi.Models
{
    public class PM_SCHEDULEDUE_V
    {
        public string ASSET_NO { get; set; }
        public string COST_CENTER { get; set; }
        public string COSCENTERNAME { get; set; }

        public DateTime NEXT_PM_DUE { get; set; }
    }
}