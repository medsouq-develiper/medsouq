﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;

namespace EmrWebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            //Enable requests coming from EmrWebApp and other APP
            var cors = new EnableCorsAttribute(EmrWebApi.Properties.Settings.Default.corsURLs, "*", "*");
            config.EnableCors(cors);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            /*
            var json = config.Formatters.JsonFormatter;
            json.SerializerSettings.Converters.Add(new IsoDateTimeConverter());
            json.SerializerSettings.Converters.Add(new MyDateTimeConverter());
            */
        }

        public static JsonSerializerSettings JsonSerializationSettings = new JsonSerializerSettings() { DateFormatString = EmrWebApi.Properties.Settings.Default.clientPresentationDateFormat };
    }
    /*
    public class MyDateTimeConverter : DateTimeConverterBase
    {
        public override void WriteJson(JsonWriter writer,
                                           object value,
                                                  JsonSerializer serializer)
        {
            writer.WriteValue(((DateTime)value)
                               .ToString("yyyy-MM-ddTHH:mm:ss"));            
        }
        
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            serializer.DateFormatString = "dd/MM/yyyy";
            string value = reader.Value.ToString();
            return DateTime.Parse(value);
        }        
    }
     */
}
