﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;

//Project Related
using EmrWebApi.BusinessObjects;
using EmrWebApi.Models;

namespace EmrWebApi.Controllers
{
    public class InventoryController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage CreateNewProduct(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                PRODUCT_DETAILS productDetails = ((JObject)JsonConvert.DeserializeObject(postData["PRODUCT_DETAILS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<PRODUCT_DETAILS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var productReturned = boInventory.CreateNewProduct(productDetails);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PRODUCT_DETAILS", productReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadProduct(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string productID = postData["PRODUCT_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var clinic = boInventory.ReadProduct(productID);


                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PRODUCT_DETAILS", clinic } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateProductDetails(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                PRODUCT_DETAILS product = ((JObject)JsonConvert.DeserializeObject(postData["PRODUCT_DETAILS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<PRODUCT_DETAILS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                boInventory.UpdateProduct(product);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchProducts(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string queryStatus = postData["STATUS"].ToString();
                string queryCategoryID = postData["CATEGORY_ID"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var productList = boInventory.SearchProducts(queryString, queryStatus, queryCategoryID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PRODUCT_DETAILS", productList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        //Update item location store
        [HttpPost]
        public HttpResponseMessage UpdateItemLocation(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                string storeId = postData["STORE_ID"].ToString();
                string partId = postData["ID_PARTS"].ToString();
                string location = postData["LOCATION"].ToString();
                string location2 = postData["LOCATION2"].ToString();
                string location3 = postData["LOCATION3"].ToString();
                string location4 = postData["LOCATION4"].ToString();
                string updateBy = postData["UPDATEDBY"].ToString();
                string updatedDate = postData["UPDATEDDATE"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                boInventory.UpdateStockOnHandItemLocation(storeId, partId, location, location2, location3, location4, updateBy, updatedDate);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }


        //Stock on Hand Adjustment
        [HttpPost]
		public HttpResponseMessage CreateNewSOHAdjust(JObject postData)
		{
			HttpResponseMessage ret;
			Inventory boInventory = null;
			Security boSecurity = null;
			try
			{
				//gather data                
				STOCK_ONHAND_ADJUSTMENT infoDetails = ((JObject)JsonConvert.DeserializeObject(postData["STOCK_ONHAND_ADJUSTMENT"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STOCK_ONHAND_ADJUSTMENT>();
				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "UPDATE");

				boInventory = new Inventory(boSecurity.GetSharedDBInstance);
				var infoReturned = boInventory.CreateNewSOHAdjustment(infoDetails);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STOCK_ONHAND_ADJUSTMENT", infoReturned } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage SearchSOHAdjust(JObject postData)
		{
			HttpResponseMessage ret;
			Inventory boInventory = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				string idPart = postData["ID_PARTS"].ToString();
				string storeId = postData["STORE_ID"].ToString();
				var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boInventory = new Inventory(boSecurity.GetSharedDBInstance);
				var infoList = boInventory.SearchSOHAdjustment(queryString, idPart, storeId, filter);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STOCK_ONHAND_ADJUSTMENT", infoList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}

        //Store Batches
        [HttpPost]
        public HttpResponseMessage SearchStoreBatches(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var infoList = boInventory.SearchStoreBatches(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STORE_BATCHES", infoList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }



        [HttpPost]
        public HttpResponseMessage SearchProdServiceCat(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string queryService = postData["SERVICE"].ToString();
                string queryCategoryID = postData["CATEGORY_ID"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var productList = boInventory.SearchProdServiceCat(queryString, queryService, queryCategoryID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PRODUCT_DETAILS", productList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage CreateNewSupplier(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                PRODUCT_SUPPLIERS supplierDetails = ((JObject)JsonConvert.DeserializeObject(postData["PRODUCT_SUPPLIERS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<PRODUCT_SUPPLIERS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var supplierturned = boInventory.CreateNewSuppliers(supplierDetails);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PRODUCT_SUPPLIERS", supplierturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadSupplier(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string supplierID = postData["SUPPLIER_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var supplierReturned = boInventory.ReadSupplier(supplierID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PRODUCT_SUPPLIERS", supplierReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateSupplierDetails(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                PRODUCT_SUPPLIERS supplierDetail = ((JObject)JsonConvert.DeserializeObject(postData["PRODUCT_SUPPLIERS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<PRODUCT_SUPPLIERS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                boInventory.UpdateSupplier(supplierDetail);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }

        [HttpPost]
       
        public HttpResponseMessage CreateNewStore(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                STORES storeDetails = ((JObject)JsonConvert.DeserializeObject(postData["STORES"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STORES>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var storeReturned = boInventory.CreateNewStore(storeDetails, userSession.USER_ID);
                var storeBuilding = boInventory.SearchstoreBuilding(storeDetails.STORE_ID);
                var buildingLOV = boInventory.SearchStoreBuildingLOV();
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STORES", storeReturned }, { "STORE_BUILDING", storeBuilding }, { "BUILDING_LOV", buildingLOV } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadStore(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            Setup boSetup = null;
            try
            {
                string storeID = postData["STORE_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");
                boSetup = new Setup(boSecurity.GetSharedDBInstance);

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var storeReturned = boInventory.ReadStore(storeID);
                var storeBuilding = boInventory.SearchstoreBuilding(storeID);
                var buildingLOV = boInventory.SearchStoreBuildingLOV();
                var clientsView = boSetup.GetCLIENT_V();
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STORES", storeReturned }, { "STORE_BUILDING", storeBuilding }, { "BUILDING_LOV", buildingLOV }, { "CLIENTS_V", clientsView } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateStoreDetails(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                STORES storeDetail = ((JObject)JsonConvert.DeserializeObject(postData["STORES"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STORES>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                var buildingList = new List<STORE_BUILDING>();
                if (postData["STORE_BUILDING"] != null)

                {
                    buildingList = ((JArray)postData.SelectToken("STORE_BUILDING")).ToObject<List<STORE_BUILDING>>();
                }


                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                boInventory.UpdateStore(storeDetail, buildingList, userSession.USER_ID);
                var storeBuilding = boInventory.SearchstoreBuilding(storeDetail.STORE_ID);
                var buildingLOV = boInventory.SearchStoreBuildingLOV();
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." }, { "STORE_BUILDING", storeBuilding }, { "BUILDING_LOV", buildingLOV } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchStores(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var storeList = boInventory.SearchStore(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STORES", storeList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        [HttpPost]
        public HttpResponseMessage SearchStoresByUser(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            AIMS boAIMS = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var storeList = boInventory.SearchStoreByUser(queryString, userSession.USER_ID);
                var supplierList = boInventory.SearchSupplier(queryString);

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var modelList = boAIMS.SearchModel(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STORES", storeList }, { "SUPPLIERS", supplierList },{ "MODELS", modelList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        [HttpPost]
        public HttpResponseMessage SearchSuppliers(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var supplierList = boInventory.SearchSupplier(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PRODUCT_SUPPLIERS", supplierList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        //Stock Batch Receive
        [HttpPost]
        public HttpResponseMessage SearchStockBatchRecCatLov(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var storeList = boInventory.SearchStore(queryString);

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var supplierList = boInventory.SearchSupplier(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STORES", storeList }, { "PRODUCT_SUPPLIERS", supplierList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchStockBatchRecCatLovStore(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var storeList = boInventory.SearchStore(queryString);

                //boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                //var supplierList = boInventory.SearchSupplier(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STORES", storeList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchStore(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var storeList = boInventory.SearchStore(queryString);

       
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STORES", storeList }});
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage CreateNewStockBatchReceive(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            NotifySender boNotifySender = null;
            try
            {
                //gather data        

                var newStockBatchList = ((JArray)postData.SelectToken("PRODUCT_STOCKBATCH")).ToObject<List<PRODUCT_STOCKBATCH>>();

                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();
                var createDate = postData["CREATE_DATE"].ToString();
                var actionStr = postData["ACTION"].ToString();
                var transId = postData["TRANS_ID"].ToString();
                //DateTime createDate = DateTime.ParseExact(postData["CREATE_DATE"].ToString(), "dd/MM/yyyy", System.Threading.Thread.CurrentThread.CurrentUICulture);
                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var newStockBatchListReturned = boInventory.CreateNewStockBatch(newStockBatchList, userSession.STAFF_ID, createDate, actionStr, transId);
                if (actionStr == "POST")
                {
                    
                    //var purhcaseInfoReturned = boInventory.ReadPurchaseInfo(newStockBatchListReturned[0].PURCHASE_NO);
                   // if(purhcaseInfoReturned.ASSIGN_TO !=null && purhcaseInfoReturned.ASSIGN_TO != "")
                   // {
                    //    boNotifySender = new NotifySender(boSecurity.GetSharedDBInstance);
                   //     var notifyReturn = boNotifySender.UpdateNotify("2", purhcaseInfoReturned.ASSIGN_TO, purhcaseInfoReturned.REF_WO, "BSAIMS Purchase Order Received", "Y", purhcaseInfoReturned.REQ_DATE);
                   // }

                    //newStockBatchListReturned[0].PURCHASE_NO;
                    // var notifyReturn = boNotifySender.UpdateNotify("1", woturned.ASSIGN_TO, newStockBatchListReturned.w, "BSAIMS New Work Order", "Y");
                }

                var transIdcurr = newStockBatchListReturned[0].TRANS_ID;
                var itemsReturned = boInventory.SearchStockBatchReceiveItems(transIdcurr.ToString(), "");

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PRODUCT_STOCKBATCH", itemsReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage GetStockBatchByProductAndRequest(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            AIMS boAIMS = null;
            try
            {
                //gather data        
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();
                string requestId = postData["REQUEST_ID"].ToString();
                string productCode = postData["PRODUCT_CODE"].ToString();
                string barCodeId = postData["BARCODE_ID"].ToString();
                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var returnProductStockBatches = boInventory.GetStockBatchByProductAndRequest(requestId, productCode, barCodeId);

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var returnProductAssets = boAIMS.ReadAssetsByProductCode(productCode);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PRODUCT_STOCKBATCH", returnProductStockBatches }, { "AM_ASSET_DETAILS", returnProductAssets } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage CreateNewStockBatchV2(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            AIMS boAIMS = null;
            try
            {
                //gather data        
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();
                var amPurchaseRequest = ((JObject)(postData.GetValue("AM_PURCHASEREQUEST"))).ToObject<AM_PURCHASEREQUEST>();
                var productStockBatches = ((JArray)postData.SelectToken("PRODUCT_STOCKBATCH")).ToObject<List<PRODUCT_STOCKBATCH>>();

                var assetInfoList = new List<AM_ASSET_DETAILS>();
                if (postData["AM_ASSET_DETAILS"] != null)
                {
                    assetInfoList = ((JArray)postData.SelectToken("AM_ASSET_DETAILS")).ToObject<List<AM_ASSET_DETAILS>>();
                }

                var assetInfoSerial = new List<ASSET_SERIALNO>();
                if (postData["ASSET_SERIALNO"] != null)
                {
                    assetInfoSerial = ((JArray)postData.SelectToken("ASSET_SERIALNO")).ToObject<List<ASSET_SERIALNO>>();
                }



                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var AMPurchaseRequesItems = boAIMS.ReadPurchaseRequestItems(amPurchaseRequest.REQUEST_ID);

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var returnProductStockBatches = boInventory.CreateNewStockBatchV2(amPurchaseRequest, AMPurchaseRequesItems, productStockBatches, userSession);

                if (assetInfoList.Count() > 0)
                {
                    foreach (var item in assetInfoList)
                    {
                        item.CREATED_DATE = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Arab Standard Time");
                        item.CREATED_BY = userSession.STAFF_ID;
                        var batch = productStockBatches.Find(x => x.PRODUCT_CODE == item.PRODUCT_CODE);

                        var assetSerial = new List<ASSET_SERIALNO>();
                        assetSerial = assetInfoSerial.FindAll(x => x.PRODUCT_CODE == item.PRODUCT_CODE);

                        for (int i = 0; i < batch.QTY_RECEIVED; i++)
                        {
                            item.SERIAL_NO = assetSerial[i].SERIALNO;
                            boAIMS.CreateNewAsset(item);
                        }
                            
                    }
                }

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASEREQUEST", amPurchaseRequest } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage CreateProductBatch(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();
                var batch = ((JObject)JsonConvert.DeserializeObject(postData["BatchDetails"].ToString())).ToObject<PRODUCT_STOCKBATCH>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var retbatch = boInventory.CreateProductBatch(batch);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "Batch", retbatch } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage UpdateProductBatch(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();
                var batch = ((JObject)JsonConvert.DeserializeObject(postData["Batch"].ToString())).ToObject<PRODUCT_STOCKBATCH>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var retbatch = boInventory.UpdateProductBatch(batch);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "Batch", retbatch } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage RecalculateProductBatch(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();
                string queryStore = postData["STORE"].ToString();
                string queryProductCode = postData["PRODUCT_CODE"].ToString();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var PRODUCT_STOCKBATCHS = boInventory.GetStockBatchByProdStore(queryStore, queryProductCode);
                 boInventory.RecalculateProductBatch(PRODUCT_STOCKBATCHS);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PRODUCT_STOCKBATCHS", PRODUCT_STOCKBATCHS } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchStockItems(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string actionString = postData["ACTION"].ToString();
                string storeString = postData["STORE"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var supplierList = boInventory.SearchStockItems(queryString, storeString, actionString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PRODUCT_STOCKONHAND", supplierList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchStockItemsOverall(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string actionString = postData["ACTION"].ToString();
                string storeString = postData["STORE"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var supplierList = boInventory.SearchStockItemsOverall(queryString, storeString, actionString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PRODUCT_STOCKONHAND", supplierList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchStockBatchReceive(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();
                var searchFilter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var storeList = boInventory.SearchStockBatchReceive(queryString, searchFilter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PRODUCT_STOCKBATCH_LIST", storeList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchStockBatchReceiveItems(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string productCODE = postData["ID_PARTS"].ToString();
                string transID = postData["TRANS_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var itemsReturned = boInventory.SearchStockBatchReceiveItems(transID, productCODE);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PRODUCT_STOCKBATCH", itemsReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchStockOnHand(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string queryStatus = postData["STATUS"].ToString();
                string queryCategoryID = postData["CATEGORY"].ToString();
                string queryStoreId = postData["STORE"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var productList = boInventory.SearchStockOnHand(queryString, queryStatus, queryCategoryID, queryStoreId);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PRODUCT_STOCKONHAND", productList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        [HttpPost]
        public HttpResponseMessage SearchStockOnHandByUserStore(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string queryStatus = postData["STATUS"].ToString();
                string queryCategoryID = postData["CATEGORY"].ToString();
                string queryStoreId = postData["STORE"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var productList = boInventory.SearchStockOnHandByUserStore(queryString, queryStatus, queryCategoryID, queryStoreId, userSession.USER_ID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PRODUCT_STOCKONHAND", productList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        [HttpPost]
        public HttpResponseMessage SearchUserStoreStockOnHand(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string queryStatus = postData["STATUS"].ToString();
                string queryCategoryID = postData["CATEGORY"].ToString();
                string queryStoreId = postData["STORE"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var productList = boInventory.SearchUserStoreStockOnHand(queryString, queryStatus, queryCategoryID, queryStoreId, userSession.USER_ID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PRODUCT_STOCKONHAND", productList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchPartsStockOnhand(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var partsList = boInventory.SearchPartsOnhand(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PARTS_STOCKONHAND_V", partsList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        //Stock Transaction
        [HttpPost]
        public HttpResponseMessage CreateNewStocktransaction(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                //gather data        
                var newList = ((JArray)postData.SelectToken("AM_STOCKTRANSACTION")).ToObject<List<AM_STOCKTRANSACTION>>();
                var amStocktransactionMaster = ((JObject)(postData.GetValue("AM_STOCKTRANSACTION_MASTER"))).ToObject<AM_STOCKTRANSACTION_MASTER>();

                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();
                var createDate = postData["CREATE_DATE"].ToString();
                var actionStr = postData["ACTION"].ToString();
                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var newListReturned = boInventory.CreateNewStockTransaction(newList, amStocktransactionMaster, userSession.STAFF_ID, createDate, actionStr);
                var transId = newListReturned[0].TRANS_ID;
                var prList = boInventory.readStockReq(transId);
                var storeId = newListReturned[0].STORE_FROM;
                var productList = boInventory.SearchStockOnHand("", "", "", storeId.ToString());
                var storeReqId = newListReturned[0].STORE_TO;
                var productReqStoreList = boInventory.SearchStockOnHand("", "", "", storeReqId.ToString());

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_STOCKTRANSACTION", prList }, { "PRODUCT_STOCKONHAND", productList }, { "PRODUCT_STOCKONHAND_REQSTORE", productReqStoreList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage UpdateStorkreqStatus(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                var reqID = postData["TRANS_ID"].ToString();
                var prStatus = postData["STATUS"].ToString();
                var reqIDLine = postData["AM_STOCKTRANSACTIONID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                boInventory.UpdateStockReqStatus(reqID, prStatus, reqIDLine, userSession.USER_ID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }

        [HttpPost]
        public HttpResponseMessage UpdatePurchaseReturn(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                PRODUCT_BATCH_V poDetail = ((JObject)JsonConvert.DeserializeObject(postData["PRODUCT_BATCH_V"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<PRODUCT_BATCH_V>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                boInventory.UpdatePurchaseOrder(poDetail);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESSRETURN", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchStockTransDetFlag(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string queryAction = postData["RETACTION"].ToString();
                string queryTransStatus = postData["TRANS_STATUS"].ToString();
                string queryPage = postData["PAGE"].ToString();
                var searchFilter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var partList = boInventory.SearchStockTransaction(queryString, queryAction, queryTransStatus, queryPage, searchFilter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_STOCKTRANSACTION", partList }  });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage GetCustomerRequestByRequest(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["TRANS_ID"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var prList = boInventory.readStockReq(queryString);
                var stocktransactionMaster = boInventory.readStocktransactionMaster(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_STOCKTRANSACTIONS", prList }, { "AM_STOCKTRANSACTION", stocktransactionMaster } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        public HttpResponseMessage SearchStockReqDet(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["TRANS_ID"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var prList = boInventory.readStockReq(queryString);
                var storeId = prList[0].STORE_FROM;
                var productList = boInventory.SearchStockOnHand("", "", "", storeId.ToString());
                var storeReqId = prList[0].STORE_TO;
                var productReqStoreList = boInventory.SearchStockOnHand("", "", "", storeReqId.ToString());

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_STOCKTRANSACTION", prList }, { "PRODUCT_STOCKONHAND", productList }, { "PRODUCT_STOCKONHAND_REQSTORE", productReqStoreList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        //Dispatch Work Order Labour Parts
        public HttpResponseMessage SearchWOLPartReq(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            AIMS boAIMS = null;
            try
            {
                string queryString = postData["REF_WO"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var prList = boInventory.readWOLPartsReq(queryString);
                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var woReturned = boAIMS.ReadWorkOrder(queryString);


                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PARTSREQUEST", prList }, { "AM_WORK_ORDERS", woReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateWOLPartsRequest(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            AIMS boAIMS = null;
            NotifySender boNotifySender = null;
            try
            {
                //gather data        

                var newList = ((JArray)postData.SelectToken("AM_PARTSREQUEST")).ToObject<List<AM_PARTSREQUEST>>();

                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();
                var createDate = postData["CREATE_DATE"].ToString();
                var actionStr = postData["ACTION"].ToString();
                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var newListReturned = boInventory.UpdateWOLPartsRequest(newList, userSession.STAFF_ID, createDate, actionStr);
                var wolReq = newListReturned[0].REF_WO;
                var prList = boInventory.readWOLPartsReq(wolReq.ToString());
                if (newListReturned[0].REF_WO != null && newListReturned[0].REF_WO != "")
                {
                    boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                    var woReturned = boAIMS.ReadWorkOrder(newListReturned[0].REF_WO);

                    if(woReturned.ASSIGN_TO != null && woReturned.ASSIGN_TO != "")
                    {
                        boNotifySender = new NotifySender(boSecurity.GetSharedDBInstance);
                        var notifyReturn = boNotifySender.UpdateNotify("6", woReturned.ASSIGN_TO, newListReturned[0].REF_WO, "BSAIMS Spare Parts Ready for collection base on work order No." + newListReturned[0].REF_WO, "Y", newListReturned[0].CREATED_DATE);
                    }

                    
                }

                
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PARTSREQUEST", prList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage UpdateWOLPartsRequestForCollect(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            AIMS boAIMS = null;
            NotifySender boNotifySender = null;
            try
            {
                //gather data     
                AM_PARTSREQUEST partRequestInfo = ((JObject)JsonConvert.DeserializeObject(postData["AM_PARTSREQUEST"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_PARTSREQUEST>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();
               
                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var newListReturned = boInventory.UpdatePartRequestForCollection(partRequestInfo);
                var wolReq = partRequestInfo.REF_WO;
                var prList = boInventory.readWOLPartsReq(wolReq.ToString());
                if (partRequestInfo.REF_WO != null && partRequestInfo.REF_WO != "")
                {
                    boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                    var woReturned = boAIMS.ReadWorkOrder(partRequestInfo.REF_WO);

                    if (woReturned.ASSIGN_TO != null && woReturned.ASSIGN_TO != "")
                    {
                        boNotifySender = new NotifySender(boSecurity.GetSharedDBInstance);
                        var notifyReturn = boNotifySender.UpdateNotify("6", woReturned.ASSIGN_TO, partRequestInfo.REF_WO, "BSAIMS Spare Parts Ready for collection base on work order No." + partRequestInfo.REF_WO, "Y", partRequestInfo.CREATED_DATE);
                    }


                }


                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PARTSREQUEST", prList } , { "AM_PARTSREQUESTCONFICT", newListReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage UpdateWOLPartsRequestDispatch(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
          
            try
            {
                //gather data     
                AM_PARTSREQUEST partRequestInfo = ((JObject)JsonConvert.DeserializeObject(postData["AM_PARTSREQUEST"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_PARTSREQUEST>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();
              
                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var newListReturned = boInventory.UpdatePartRequestDispatch(partRequestInfo);
                var wolReq = partRequestInfo.REF_WO;
                var prList = boInventory.readWOLPartsReq(wolReq.ToString());
               
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PARTSREQUEST", prList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        public HttpResponseMessage SearchStockOnhandFromStore(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryStore = postData["STORE_FROM"].ToString();
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
             
                var productList = boInventory.SearchStockOnHand(queryString, "", "", queryStore);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PRODUCT_STOCKONHAND", productList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        public HttpResponseMessage SearchStockReceiveReqDet(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["TRANS_ID"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var prList = boInventory.readStockReq(queryString);
                var storeId = prList[0].STORE_FROM;
                var productList = boInventory.SearchStockOnHand("", "", "", storeId.ToString());
                var storeReqId = prList[0].STORE_TO;
                var productReqStoreList = boInventory.SearchStockOnHand("", "", "", storeReqId.ToString());
                var stocktransactionMaster = boInventory.readStocktransactionMaster(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_STOCKTRANSACTION", prList }, { "AM_STOCKTRANSACTION_MASTER", stocktransactionMaster }, { "PRODUCT_STOCKONHAND", productList }, { "PRODUCT_STOCKONHAND_REQSTORE", productReqStoreList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        //Work Order Labour Material
        [HttpPost]
        public HttpResponseMessage CreateNewWOLMaterial(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            NotifySender boNotifySender = null;
            try
            {
                //gather data        

                var newList = ((JArray)postData.SelectToken("AM_PARTSREQUEST")).ToObject<List<AM_PARTSREQUEST>>();

                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();
                var createDate = postData["CREATE_DATE"].ToString();
                var actionStr = postData["ACTION"].ToString();
                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var newListReturned = boInventory.CreateWOLMaterial(newList, userSession.STAFF_ID, createDate, actionStr);
                var WOLprList = boInventory.readWOIdMaterials(newListReturned[0].REF_WO);
                boNotifySender = new NotifySender(boSecurity.GetSharedDBInstance);
                var notifyReturn = boNotifySender.UpdateNotify("5", "SDFWERXDFSDR", newListReturned[0].REF_WO, "BSAIMS Spare Parts Requested base on work order No." + newListReturned[0].REF_WO, "N", userSession.LOGON_DATE);
             
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PARTSREQUEST_LIST", WOLprList }, { "AUTOREFRESHPARTS", "AUTOREFRESHPARTS" } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage CreateWorkDetailsMaterial(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                //gather data        

                var newList = ((JArray)postData.SelectToken("AM_PARTSREQUEST")).ToObject<List<AM_PARTSREQUEST>>();

                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();
                var createDate = postData["CREATE_DATE"].ToString();
                var actionStr = postData["ACTION"].ToString();
                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var newListReturned = boInventory.CreateWordDetailMaterial(newList, userSession.STAFF_ID, createDate, actionStr);
                var WOLprList = boInventory.readWOIdMaterials(newListReturned[0].REF_WO);


                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PARTSREQUEST_LIST", WOLprList }, { "AUTOREFRESHPARTS", "AUTOREFRESHPARTS" } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage deleteWorkOrderSpartParts(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                var reqId = postData["REQ_NO"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");
              
                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                boInventory.deleteWOSpartParts(reqId);
                var WOLprList = boInventory.readWOIdMaterials(reqId);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PARTSREQUEST_LIST", WOLprList }, { "AUTOREFRESHPARTS", "AUTOREFRESHPARTS" } ,{ "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage deleteWorkDetailsParts(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                var reqId = postData["REQ_NO"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                boInventory.deleteWorkDetailsSpartParts(reqId);
                var WOLprList = boInventory.readWOIdMaterials(reqId);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PARTSREQUEST_LIST", WOLprList }, { "AUTOREFRESHPARTS", "AUTOREFRESHPARTS" }, { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchWOPartsStockOnhand(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string wolIdString = postData["WORKORDER_LABOUR_ID"].ToString();
                
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var partsList = boInventory.SearchPartsOnhand(queryString);
                var WOLprList = boInventory.readWOLMaterials(wolIdString);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PARTS_STOCKONHAND_V", partsList } , { "AM_PARTSREQUEST", WOLprList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchWOIDPartsStockOnhand(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string reqIdString = postData["REQ_NO"].ToString();

                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var partsList = boInventory.SearchPartsOnhand(queryString);
                var WOLprList = boInventory.readWOIdMaterials(reqIdString);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PARTS_STOCKONHAND_V", partsList }, { "AM_PARTSREQUEST_LIST", WOLprList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchWOIDPartsRequested(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string reqIdString = postData["REQ_NO"].ToString();

                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
              
                var WOLprList = boInventory.readWOIdMaterials(reqIdString);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PARTSREQUEST_LIST", WOLprList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchPartsActiveLists(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string reqIdString = postData["REQ_NO"].ToString();

                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var partsList = boInventory.SearchPartsOnhandListActive(queryString);
              
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PARTS_STOCKONHAND_VI", partsList }});
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        [HttpPost]
        public HttpResponseMessage SearchPartsActiveByStoreBuilding(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string reqIdString = postData["REQ_NO"].ToString();

                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var partsList = boInventory.SearchPartsOnhandByStoreActive(queryString, reqIdString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PARTS_STOCKONHAND_VI", partsList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchWOPartsInc(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Setup boSetup = null;
            AIMS boAims = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string wolIdString = postData["REQNO"].ToString();
                string clientId = postData["CLIENTID"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);

                var WOLprList = boInventory.readWOIdPRMaterials(wolIdString);
                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var clientInformation = boSetup.ReadClientInfo(clientId);

                boAims = new AIMS(boSecurity.GetSharedDBInstance);
                var wolList = boAims.SearchWOLabourAct(queryString, wolIdString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PARTSREQUEST", WOLprList }, { "AM_CLIENT_INFO", clientInformation }, { "AM_WORKORDER_LABOUR", wolList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchWOPartsIncUse(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Setup boSetup = null;
            AIMS boAims = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string wolIdString = postData["REQNO"].ToString();
                string clientId = postData["CLIENTID"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);

                var WOLprList = boInventory.readWOIdPRMaterialsUse(wolIdString);
                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var clientInformation = boSetup.ReadClientInfo(clientId);

                boAims = new AIMS(boSecurity.GetSharedDBInstance);
                var wolList = boAims.SearchWOLabourAct("", wolIdString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PARTSREQUEST", WOLprList }, { "AM_CLIENT_INFO", clientInformation }, { "AM_WORKORDER_LABOUR", wolList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage GetStockBatchByProdStore(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();
                string queryStore = postData["STORE"].ToString();
                string queryProductCode = postData["PRODUCT_CODE"].ToString();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var PRODUCT_STOCKBATCHS = boInventory.GetStockBatchByProdStore(queryStore, queryProductCode);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PRODUCT_STOCKBATCHS", PRODUCT_STOCKBATCHS } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage TransferCustomerRequest(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();
                var amStocktransaction = ((JObject)JsonConvert.DeserializeObject(postData["AM_STOCKTRANSACTION"].ToString())).ToObject<AM_STOCKTRANSACTION_MASTER>();
                var productStockbatchs = postData["PRODUCT_STOCKBATCHS"] != null ? ((JArray)postData.SelectToken("PRODUCT_STOCKBATCHS")).ToObject<List<PRODUCT_STOCKBATCH>>() : new List<PRODUCT_STOCKBATCH>();
                var asset_Vs = postData["ASSET_VS"] != null ? ((JArray)postData.SelectToken("ASSET_VS")).ToObject<List<ASSET_V>>() : new List<ASSET_V>();
                var assetDetails = new List<AM_ASSET_DETAILS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                foreach (var asset_v in asset_Vs)
                {
                    var assetDetail = boAIMS.ReadAssetInfo(asset_v.ASSET_NO);
                    assetDetail.COST_CENTER = amStocktransaction.SUPPLIER_ID;
                    assetDetails.Add(assetDetail);

                    var amStockTransaction = boInventory.SearchStockTransactionByProd(amStocktransaction.TRANS_ID, assetDetail.PRODUCT_CODE);
                    var assetTransaction = new AM_ASSET_TRANSACTION()
                    {
                        ASSET_NO = assetDetail.ASSET_NO,
                        COSTCENTERINSTALLDATE = amStocktransaction.REQUEST_DATE,
                        COSTCENTERCODE = assetDetail.COST_CENTER,
                        COSTCENTERPURCHASEDATE = amStocktransaction.REQUEST_DATE,
                        COSTCENTERPURCHASECOST = amStockTransaction.PRICE,
                        COSTCENTERASSETCOND = "NEW",
                        COSTCENTERINVOICENO = amStocktransaction.POINVOICENO
                    };

                    var returnAssetDetails = boAIMS.UpdateAssetInfo(assetDetail, assetTransaction, userSession.STAFF_ID);
                }

                var returnAmStocktransaction = boInventory.TransferCustomerRequest(amStocktransaction, productStockbatchs, assetDetails, userSession);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_STOCKTRANSACTION", returnAmStocktransaction } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage ReturnRecallStockBatch(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();
                var productStockbatch = ((JObject)JsonConvert.DeserializeObject(postData["PRODUCT_STOCKBATCH"].ToString())).ToObject<PRODUCT_STOCKBATCH>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var returnProductStockbatch = boInventory.ReturnRecallStockBatch(productStockbatch, userSession);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PRODUCT_STOCKBATCH", returnProductStockbatch } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchItemBatchesV(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();
                var searchKey = postData["SEARCH"].ToString();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var returnItemBatchesV = boInventory.SearchItemBatchesV(searchKey);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "ITEM_BATCHES_V", returnItemBatchesV } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage GetItemBatchesVByReport(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();
                var sysReportId = postData["SYSREPORTID"].ToString();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                boSetup = new Setup(boSecurity.GetSharedDBInstance);

                var reportDetails = boSetup.GetSysTransReportDocsById(sysReportId);
                var reportDocs = boSetup.GetSysTransReportDocsByReport(sysReportId);
                var returnItemBatchesV = boInventory.GetItemBatchesVByReport(sysReportId, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "REPORT_DETAILS", reportDetails }, { "ITEM_BATCHES_V", returnItemBatchesV }, { "REPORT_SCANDOCS", reportDocs } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage CreatSysTransReport(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();
                var systemTransactionReport = ((JObject)JsonConvert.DeserializeObject(postData["SYSTEMTRANSACTION_REPORT"].ToString())).ToObject<SYSTEMTRANSACTION_REPORT>();
                var systemTransactionReportScandocs = postData["SYSTEMTRANSACTION_REPORT_SCANDOCS"] != null ? ((JArray)postData.SelectToken("SYSTEMTRANSACTION_REPORT_SCANDOCS")).ToObject<List<SYSTEMTRANSACTION_REPORT_SCANDOC>>() : new List<SYSTEMTRANSACTION_REPORT_SCANDOC>();
                var type = postData["TYPE"].ToString();
                var codeId = postData["CODEID"].ToString();

                var transItems1 = new List<ASSET_V>();
                var transItems2 = new List<ITEM_BATCHES_V>();
                if (type == "Asset")
                    transItems1 = postData["TRANS_ITEMS"] != null ? ((JArray)postData.SelectToken("TRANS_ITEMS")).ToObject<List<ASSET_V>>() : new List<ASSET_V>();
                else
                    transItems2 = postData["TRANS_ITEMS"] != null ? ((JArray)postData.SelectToken("TRANS_ITEMS")).ToObject<List<ITEM_BATCHES_V>>() : new List<ITEM_BATCHES_V>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var returnSystemTransactionReport = boInventory.CreatSysTransReport(systemTransactionReport, transItems1, transItems2, systemTransactionReportScandocs, userSession, codeId);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SYSTEMTRANSACTION_REPORT", returnSystemTransactionReport }, { "ASSET_V", transItems1 }, { "ITEM_BATCHES_V", transItems2 } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
    }
}