﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;
using EmrWebApi.BusinessObjects;
using EmrWebApi.Models;

namespace EmrWebApi.Controllers
{
    public class ClientRulesController : ApiController
    {

        [HttpPost]
        public HttpResponseMessage GetClientRules(JObject postData)
        {
            HttpResponseMessage ret;
            ClientRules boClientRules = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boClientRules = new ClientRules(boSecurity.GetSharedDBInstance);
                var clientRules = boClientRules.GetClientRules(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "ClientRules", clientRules } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage GetClientRuleById(JObject postData)
        {
            HttpResponseMessage ret;
            ClientRules boClientRules = null;
            Security boSecurity = null;
            try
            {
                string ruleId = postData["ruleId"].ToString();
                
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boClientRules = new ClientRules(boSecurity.GetSharedDBInstance);
                var CLIENTRULE = boClientRules.GetClientRuleById(ruleId);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "CLIENTRULE", CLIENTRULE } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
    }
}
