﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;

//Project Related
using EmrWebApi.BusinessObjects;
using EmrWebApi.Models;

namespace EmrWebApi.Controllers
{
    public class StaffController : ApiController
    {
        
        [HttpPost]

        
        public HttpResponseMessage CreateStaffUser(JObject postData)
        {
            HttpResponseMessage ret;
            Staff boStaffuser = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                STAFF staffuser = ((JObject)JsonConvert.DeserializeObject(postData["STAFF"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STAFF>();                
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boStaffuser = new Staff(boSecurity.GetSharedDBInstance);
                var staffuserReturned = boStaffuser.CreateStaffUser(staffuser);
                var clientBuilding = boStaffuser.SearchUserBuilding(staffuser.USER_ID);
                var clientLOV = boStaffuser.SearchUserBuildingLOV(staffuser.USER_ID);
                var userStore = boStaffuser.SearchUserStore(staffuser.USER_ID);
                var storeLOV = boStaffuser.SearchUserStoreLOV(staffuser.USER_ID);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF", staffuserReturned }, { "USER_BUILDING", clientBuilding }, { "USER_BUILDING_LOV", clientLOV }, { "USER_STORE", userStore }, { "USER_STORE_LOV", storeLOV } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage ReadStaffUser(JObject postData)
        {
            HttpResponseMessage ret;
            Staff boStaffUser = null;
            Security boSecurity = null;
            try
            {
                string staffuserID = postData["STAFF_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
                var staffuser = boStaffUser.ReadStaffUser(staffuserID);
                var clientBuilding = boStaffUser.SearchUserBuilding(staffuserID);
                var clientLOV = boStaffUser.SearchUserBuildingLOV(staffuserID);
                var userStore = boStaffUser.SearchUserStore(staffuser.USER_ID);
                var storeLOV = boStaffUser.SearchUserStoreLOV(staffuser.USER_ID);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF", staffuser }, { "USER_BUILDING", clientBuilding }, { "USER_BUILDING_LOV", clientLOV }, { "USER_STORE", userStore }, { "USER_STORE_LOV", storeLOV } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally {
                if (boStaffUser != null) boStaffUser.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage GetStaffUserLookUpTables(JObject postData)
        {
            HttpResponseMessage ret;
            Staff boStaffUser = null;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
                var staffGroupList = boStaffUser.GetStaffGroupList();
                var staffPositionList = boStaffUser.GetStaffPositionsList();
                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovList = boSetup.getLovList("'AIMS_SERVICEDEPARTMENT','AIMS_BUILDING'");

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "LOV_LOOKUPS", lovList },{ "STAFF_GROUPS", staffGroupList }, { "STAFF_POSITIONS", staffPositionList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage GetStaffPositionsList(JObject postData)
        {
            HttpResponseMessage ret;
            Staff boStaffUser = null;
            Security boSecurity = null;
            try
            {
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
                var staffpositionsList = boStaffUser.GetStaffPositionsList();

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF_POSITIONS", staffpositionsList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage UpdateStaffUserDetails(JObject postData)
        {
            HttpResponseMessage ret;
            Staff boStaffUser = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                STAFF staffuser = ((JObject)JsonConvert.DeserializeObject(postData["STAFF"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STAFF>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                var userBuildingList = new List<USER_BUILDING>();
                if (postData["USER_BUILDING"] != null)

                {
                    userBuildingList = ((JArray)postData.SelectToken("USER_BUILDING")).ToObject<List<USER_BUILDING>>();
                }

                var userStoreList = new List<USER_STORE>();
                if (postData["USER_STORE"] != null)

                {
                    userStoreList = ((JArray)postData.SelectToken("USER_STORE")).ToObject<List<USER_STORE>>();
                }

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
                boStaffUser.UpdateStaffUser(staffuser, userBuildingList, userStoreList, userSession.USER_ID);
                var clientBuilding = boStaffUser.SearchUserBuilding(staffuser.USER_ID);
                var clientLOV = boStaffUser.SearchUserBuildingLOV(staffuser.USER_ID);

                var userStore = boStaffUser.SearchUserStore(staffuser.USER_ID);
                var storeLOV = boStaffUser.SearchUserStoreLOV(staffuser.USER_ID);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." }, { "USER_BUILDING", clientBuilding }, { "USER_BUILDING_LOV", clientLOV }, { "USER_STORE", userStore }, { "USER_STORE_LOV", storeLOV } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }

        //notify setup
        [HttpPost]
        public HttpResponseMessage UpdateNotifySetupDetails(JObject postData)
        {
            HttpResponseMessage ret;
            Staff boStaffUser = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_NOTIFYSETUP notifyInfo = ((JObject)JsonConvert.DeserializeObject(postData["AM_NOTIFYSETUP"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_NOTIFYSETUP>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
                boStaffUser.UpdateNotifySetup(notifyInfo);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadNotifySetupDetails(JObject postData)
        {
            HttpResponseMessage ret;
            Staff boStaffUser = null;
            Security boSecurity = null;
            try
            {
                string notifyID = postData["NOTIFY_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
                var notifyInfo = boStaffUser.ReadNotifySetup(notifyID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_NOTIFYSETUP", notifyInfo } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boStaffUser != null) boStaffUser.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchNotifySetupDetails(JObject postData)
        {
            HttpResponseMessage ret;
            Staff boStaffUser = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
                var notifyList = boStaffUser.SearchNotifySetup(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_NOTIFYSETUP", notifyList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        //notify setup assigned
        [HttpPost]
        public HttpResponseMessage CreateNewNotifyAssigned(JObject postData)
        {
            HttpResponseMessage ret;
            Staff boStaff = null;
            Security boSecurity = null;
            try
            {
                //gather data        

                var newList = ((JArray)postData.SelectToken("AM_NOTIFYSETUPASSIGNED")).ToObject<List<AM_NOTIFYSETUPASSIGNED>>();

                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boStaff = new Staff(boSecurity.GetSharedDBInstance);
                var newListReturned = boStaff.CreateNewNotificationAssigned(newList);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_NOTIFYSETUPASSIGNED", newListReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchNotifyAssigned(JObject postData)
        {
            HttpResponseMessage ret;
            Staff boStaff = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string notifyID = postData["NOTIFY_ID"].ToString();

                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boStaff = new Staff(boSecurity.GetSharedDBInstance);
                var infoList = boStaff.SearchNotifyAssigned(queryString, notifyID);

                var lovList = boStaff.SearchStaffUserActive("");
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_NOTIFYSETUPASSIGNED", infoList }, { "STAFFLIST", lovList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage DeleteStaffNotify(JObject postData)
        {
            HttpResponseMessage ret;
            Staff boStaffUser = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                //STAFF staffuser = ((JObject)JsonConvert.DeserializeObject(postData["STAFF"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STAFF>();
                string notifyId = postData["NOTIFY_ID"].ToString();

                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
                boStaffUser.DeleteStaffNotify(notifyId);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchStaffLookUp(JObject postData)
        {
            HttpResponseMessage ret;
            Staff boStaffUser = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
                var staffuserList = boStaffUser.SearchStaffLookUp(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFFLIST", staffuserList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

		[HttpPost]
		public HttpResponseMessage CreateNewStaffCB(JObject postData)
		{
			HttpResponseMessage ret;
			Staff boStaff = null;
			Security boSecurity = null;
			try
			{
				//gather data        

				var newList = ((JArray)postData.SelectToken("STAFF_COSTCENTER_BUILDING")).ToObject<List<STAFF_COSTCENTER_BUILDING>>();

				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();
				
				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "UPDATE");

				boStaff = new Staff(boSecurity.GetSharedDBInstance);
				var newListReturned = boStaff.CreateNewStaffCB(newList);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF_COSTCENTER_BUILDING", newListReturned } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage CreateNewStaffStore(JObject postData)
		{
			HttpResponseMessage ret;
			Staff boStaff = null;
			Security boSecurity = null;
			try
			{
				//gather data        

				var newList = ((JArray)postData.SelectToken("STAFF_STORE")).ToObject<List<STAFF_STORE>>();

				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "UPDATE");

				boStaff = new Staff(boSecurity.GetSharedDBInstance);
				var newListReturned = boStaff.CreateNewStaffStore(newList);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF_STORE", newListReturned } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage DeleteStaffCB(JObject postData)
		{
			HttpResponseMessage ret;
			Staff boStaffUser = null;
			Security boSecurity = null;
			try
			{
				//gather data                
				//STAFF staffuser = ((JObject)JsonConvert.DeserializeObject(postData["STAFF"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STAFF>();
				string staffuserID = postData["STAFF_ID"].ToString();
				string uCategory = postData["UCATEGORY"].ToString();
		
				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "UPDATE");

				boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
				boStaffUser.DeleteStaffCB(staffuserID, uCategory);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}

			return ret;
		}
		[HttpPost]
		public HttpResponseMessage SearchStaffCB(JObject postData)
		{
			HttpResponseMessage ret;
			Staff boStaff = null;
			Setup boSetup = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				string queryAction = postData["UCATEGORY"].ToString();
				string staffID = postData["STAFF_ID"].ToString();
				
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boStaff = new Staff(boSecurity.GetSharedDBInstance);
				var infoList = boStaff.SearchStaffCB(queryString, queryAction,staffID);

				boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovList = new List<dynamic>();

                if (queryAction == "AIMS_COSTCENTER")
                {
                    lovList = boSetup.SearchClientV("");
                }
                else
                {
                    lovList = boSetup.getLovList("'" + queryAction + "'");
                }
				



				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF_COSTCENTER_BUILDING", infoList }, { "LOV_LOOKUPS", lovList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}

		[HttpPost]
		public HttpResponseMessage SearchStaffStore(JObject postData)
		{
			HttpResponseMessage ret;
			Staff boStaff = null;
			Inventory boInventory = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				
				string staffID = postData["STAFF_ID"].ToString();
				
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boStaff = new Staff(boSecurity.GetSharedDBInstance);
				var infoList = boStaff.SearchStaffStore(queryString,  staffID);

				boInventory = new Inventory(boSecurity.GetSharedDBInstance);

				var storeList = boInventory.SearchStore(queryString);
				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF_STORE", infoList }, { "STORES", storeList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}

        [HttpPost]
        public HttpResponseMessage GetStoresLookup(JObject postData)
        {
            HttpResponseMessage ret;
            Staff boStaff = null;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boStaff = new Staff(boSecurity.GetSharedDBInstance);
                var STAFF_STORES = boStaff.SearchUserStore(userSession.STAFF_ID);

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var STORES = boInventory.SearchStore("");

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF_STORES", STAFF_STORES }, { "STORES", STORES } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
    }
}
