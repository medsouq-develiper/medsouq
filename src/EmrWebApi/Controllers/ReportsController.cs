﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;

//Project Related
using EmrWebApi.BusinessObjects;
using EmrWebApi.Models;

namespace EmrWebApi.Controllers
{
    public class ReportsController : ApiController
	{

        //System Transaction Report
        [HttpPost]
        public HttpResponseMessage AcknowledgeSysReport(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();
                var systemTransactionReport = ((JObject)JsonConvert.DeserializeObject(postData["SYSTEMTRANSACTION_REPORT"].ToString())).ToObject<SYSTEMTRANSACTION_REPORT>();

                var transItems1 = new List<SYSTEM_TRANSACTIONDETAILS>();
                var transItems2 = new List<ITEM_BATCHES_V>();
                if (systemTransactionReport.SEARCHTYPE == "Asset")
                    transItems1 = postData["TRANS_ITEMS"] != null ? ((JArray)postData.SelectToken("TRANS_ITEMS")).ToObject<List<SYSTEM_TRANSACTIONDETAILS>>() : new List<SYSTEM_TRANSACTIONDETAILS>();
                else
                    transItems2 = postData["TRANS_ITEMS"] != null ? ((JArray)postData.SelectToken("TRANS_ITEMS")).ToObject<List<ITEM_BATCHES_V>>() : new List<ITEM_BATCHES_V>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var returnSystemTransactionReport = boReports.AcknowledgeSysReport(systemTransactionReport, transItems1, transItems2, userSession);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "REPORT_DETAILS", returnSystemTransactionReport }, { "ASSET_V", transItems1 }, { "ITEM_BATCHES_V", transItems2 } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage GetSysTransData(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Setup boSetup = null;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string SYSREPORTID = postData["SYSREPORTID"].ToString();
                string REFTYPE = postData["REFTYPE"].ToString();
                string REFCODE = postData["REFCODE"].ToString();
                string REFTRANSID = postData["REFTRANSID"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                boInventory = new Inventory(boSecurity.GetSharedDBInstance);

                var returnSystemTransactionReport = boSetup.GetSysTransReportDocsById(SYSREPORTID);
                var returnSystemTransactionDetails = boReports.GetSysTransData(REFTYPE, REFCODE, REFTRANSID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "REPORT_DETAILS", returnSystemTransactionReport }, { "TRANS_ITEMS", returnSystemTransactionDetails } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchTransReports(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var systemTransactionReports = boReports.SearchTransReports(queryString, filter);
          
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SYSTEMTRANSACTION_REPORTS", systemTransactionReports } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchSystemTransactionReporting(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var reptList = boReports.SearchSystemTransaction(queryString, filter);
          
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "TRANSACTION_DETAILS", reptList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        [HttpPost]
        public HttpResponseMessage SearchSystemTransaction(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var reptList = boReports.SearchSystemTransaction(queryString, filter);
                var reptsummaryList = boReports.SearchSystemTransactionRefCode(queryString, filter);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "TRANSACTION_DETAILS", reptList } , { "TRANSACTION_SUMMARY", reptsummaryList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        [HttpPost]
		public HttpResponseMessage SearchAssetByCostCenterSum(JObject postData)
		{
			HttpResponseMessage ret;
			Reports boReports = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();

                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boReports = new Reports(boSecurity.GetSharedDBInstance);
				var reportList = boReports.SearchByCostCenterSummary(queryString, filter);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSETSUMMARY", reportList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage SearchAssetByResponsibleCenterSum(JObject postData)
		{
			HttpResponseMessage ret;
			Reports boReports = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boReports = new Reports(boSecurity.GetSharedDBInstance);
				var reportList = boReports.SearchByResponsibleCenterSummary(queryString, filter);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSETSUMMARY", reportList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage SearchAssetByDeviceTypeSum(JObject postData)
		{
			HttpResponseMessage ret;
			Reports boReports = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boReports = new Reports(boSecurity.GetSharedDBInstance);
				var reportList = boReports.SearchByDeviceTypeSummary(queryString, filter);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSETSUMMARY", reportList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage SearchAssetByManufacturerSum(JObject postData)
		{
			HttpResponseMessage ret;
			Reports boReports = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boReports = new Reports(boSecurity.GetSharedDBInstance);
				var reportList = boReports.SearchByManufacturerSummary(queryString, filter);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSETSUMMARY", reportList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage SearchAssetBySupplierSum(JObject postData)
		{
			HttpResponseMessage ret;
			Reports boReports = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boReports = new Reports(boSecurity.GetSharedDBInstance);
				var reportList = boReports.SearchBySuplierSummary(queryString, filter);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSETSUMMARY", reportList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}

        [HttpPost]
        public HttpResponseMessage SearchNotification(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var notifyReportList = boReports.SearchStaffNotification(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "NOTIFICATION_LIST", notifyReportList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        
        [HttpPost]
        public HttpResponseMessage SearchPurchaseReqRept(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAims = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAims = new AIMS(boSecurity.GetSharedDBInstance);
                var reptList = boAims.SearchPurchaseRequestRept(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PURCHASEREQ_REPTLIST", reptList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage RequestedParts(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var reptList = boReports.WOPartsRequest(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "WOPARTSREQUEST_REPTLIST", reptList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage RequestedPartsZero(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var reptList = boReports.WOPartsRequestZero(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "WOPARTSREQUEST_REPTLIST", reptList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchWOPartsReq(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var reptList = boReports.SearchWOPartsRequest(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "WOPARTSREQUEST_REPTLIST", reptList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchWOPartsReqRorFR(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var reptList = boReports.SearchWOPartsRequestRorFR(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "WOPARTSREQUEST_REPTLIST", reptList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchSOHAdjustRept(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var reptList = boReports.SearchBySOHAdjustRept(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PURCHASEREQ_REPTLIST", reptList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchPMScheduleRept(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var reptList = boReports.SearchByPMSheduleRept(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PMSCHEDULE", reptList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        //WoReptSummary
        [HttpPost]
        public HttpResponseMessage searchPMType(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var reptList = boReports.SearchPMType(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PMType_List", reptList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage searchSupplier(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string queryAction = postData["RETACTION"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var rptList = boReports.SearchBySupplier(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "WO_JOBPROBLEMTYPE_V", rptList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage searchAsset(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string queryAction = postData["RETACTION"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var rptList = boReports.SearchByAsset(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "WO_JOBPROBLEMTYPE_V", rptList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage searchCostCenter(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string queryAction = postData["RETACTION"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var rptList = boReports.SearchByCostCenter(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "WO_JOBPROBLEMTYPE_V", rptList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage searchDeviceType(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string queryAction = postData["RETACTION"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var rptList = boReports.SearchByDeviceType(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "WO_JOBPROBLEMTYPE_V", rptList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchProblemTypeRpt(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string queryAction = postData["RETACTION"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var rptList = boReports.SearchByProblemType(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "WO_JOBPROBLEMTYPE_V", rptList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchJobTypeRpt(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string queryAction = postData["RETACTION"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var rptList = boReports.SearchByJobType(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "WO_JOBPROBLEMTYPE_V", rptList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchEngrCreatedRept(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var reptList = boReports.SearchByEngrStatusRept(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "ENGRCREATEDBY_STATUSLIST", reptList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchAssignToRept(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var reptList = boReports.SearchByAssignToStatusRept(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "ENGRASSIGNTO_STATUSLIST", reptList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchAssignTypeRept(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var reptList = boReports.IndieSearchAssignTypeStatusRept(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "ENGRASSIGNTYPE_STATUSLIST", reptList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchWOTimeRept(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var reptList = boReports.SearchWOTimeRept(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "WORD_ORDER_LIST", reptList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        [HttpPost]
        public HttpResponseMessage SearchWODetails(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            Setup boSetup = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string clientId = postData["CLIENTID"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var WOLprList = boReports.SearchWoPartsReqList(queryString, filter);
                var reptList = boReports.SearchWoList(queryString, filter);
                var wolList = boReports.readWOIdListLabours(queryString, filter);
                var warrantList = boReports.ReadWarrantyListByAssetNoV(queryString, filter);


                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var clientInformation = boSetup.ReadClientInfo(clientId);
                

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "WARRANTYLIST", warrantList }, { "WORD_ORDER_LIST", reptList }, { "AM_CLIENT_INFO", clientInformation },{ "AM_SPAREPARTSREQUEST", WOLprList }, { "AM_WORKORDER_LABOURLIST", wolList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchWorkOrderByResponsibleStaff(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();

                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var reportList = boReports.SearchWOByResponsibleStaff(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "WO_RESPONSIBLESTAFF", reportList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchWorkOrderAssign(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boaims = null;
            Staff boStaffUser = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boaims = new AIMS(boSecurity.GetSharedDBInstance);
                var reportList = boaims.SearchWorkOrderAssign(queryString, filter);
                boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
                var staffuserList = boStaffUser.SearchStaffUser("");

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "WO_STAFFLIST", reportList }, { "STAFFLIST", staffuserList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchAssetDP(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();

                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var reportList = boReports.SearchAssetDPRept(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSETSUMMARY", reportList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchByAssetPartsYR(JObject postData)
        {
            HttpResponseMessage ret;
            Reports boReports = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();               
            
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var WOLprList = boReports.SearchAssetPartsDPYR(queryString);
               

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PARTS_GROUPBYASSETYR_V", WOLprList }});
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchAssetsDP(JObject postData)
        {
            HttpResponseMessage ret;
            Reports Report = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                Report = new Reports(boSecurity.GetSharedDBInstance);
                var supplierList = Report.SearchAssetDPREPT(queryString, filter);
                var WOLprList = Report.SearchAssetPartsDPYR("");
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_DETAILS", supplierList }, { "PARTS_GROUPBYASSETYR_V", WOLprList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
    }
}