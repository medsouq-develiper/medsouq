﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;
using EmrWebApi.BusinessObjects;
using EmrWebApi.Models;

namespace EmrWebApi.Controllers
{
    public class TemplateController : ApiController
    {
        //Insert
        [HttpPost]
        public HttpResponseMessage InsertTestTable(JObject postData)
        {
            HttpResponseMessage ret;
            Template boTemplate = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                TESTTABLE infoData = ((JObject)JsonConvert.DeserializeObject(postData["TESTTABLE"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<TESTTABLE>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boTemplate = new Template(boSecurity.GetSharedDBInstance);
                var infoReturned = boTemplate.InsertTestTable(infoData);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "TESTTABLEINFO", infoReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        //Update
        [HttpPost]
        public HttpResponseMessage UpdateTestTable(JObject postData)
        {
            HttpResponseMessage ret;
            Template boTemplate = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                TESTTABLE infoData = ((JObject)JsonConvert.DeserializeObject(postData["TESTTABLE"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<TESTTABLE>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boTemplate = new Template(boSecurity.GetSharedDBInstance);
                boTemplate.UpdateTestTable(infoData);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }

        //Read by Id
        [HttpPost]
        public HttpResponseMessage ReadTestTAble(JObject postData)
        {
            HttpResponseMessage ret;
            Template boTemplate = null;
            Security boSecurity = null;
            try
            {
                string infoID = postData["ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boTemplate = new Template(boSecurity.GetSharedDBInstance);
                var infoReturned = boTemplate.ReadTestTable(infoID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "TESTTABLEINFO", infoReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        //Search
        [HttpPost]
        public HttpResponseMessage SearchTestTable(JObject postData)
        {
            HttpResponseMessage ret;
            Template boTemplate = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();

                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boTemplate = new Template(boSecurity.GetSharedDBInstance);
                var infoList = boTemplate.SearchTestTable(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "TESTTABLE_LIST", infoList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        //Get Dropdown List
        [HttpPost]
        public HttpResponseMessage SearchDrodownList(JObject postData)
        {
            HttpResponseMessage ret;

            Setup boSetup = null;
            AIMS boAIMS = null;
            Security boSecurity = null;

            try
            {

                string queryLOV = postData["LOV"].ToString();
                string querySearch = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovList = boSetup.getLovList(queryLOV);

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var supplierList = boAIMS.SearchSupplierLookup("");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var manufacturerList = boAIMS.SearchManufacturerLookUp("");


                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "JOBTYPELOV", lovList }, { "SUPPLIERLISTLOV", supplierList }, { "MANUFACTURERLISTLOV", manufacturerList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
    }
}
