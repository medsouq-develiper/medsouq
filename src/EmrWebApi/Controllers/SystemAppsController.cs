﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;

//Project Related
using EmrWebApi.BusinessObjects;
using EmrWebApi.Models;

namespace EmrWebApi.Controllers
{
    public class SystemAppsController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage CreateNewDictionary(JObject postData)
        {
            HttpResponseMessage ret;
            SystemApps boSytemApps = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                var newList = ((JArray)postData.SelectToken("SYSTEM_DICTIONARY")).ToObject<List<SYSTEM_DICTIONARY>>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession,"UPDATE");

                boSytemApps = new SystemApps(boSecurity.GetSharedDBInstance);
                var dictionaryturned = boSytemApps.CreateNew(newList);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SYSTEM_DICTIONARY", dictionaryturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage ReadDictionary(JObject postData)
        {
            HttpResponseMessage ret;
            SystemApps boSystemApps = null;
            Security boSecurity = null;
            try
            {
                string dictionaryID = postData["DICTIONARY_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession,"UPDATE");

                boSystemApps = new SystemApps(boSecurity.GetSharedDBInstance);
                var dictionaryReturned = boSystemApps.ReadExisting(dictionaryID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SYSTEM_DICTIONARY", dictionaryReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateDictionary(JObject postData)
        {
            HttpResponseMessage ret;
            SystemApps boSystemApps = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                SYSTEM_DICTIONARY dictionaryDetail = ((JObject)JsonConvert.DeserializeObject(postData["SYSTEM_DICTIONARY"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<SYSTEM_DICTIONARY>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSystemApps = new SystemApps(boSecurity.GetSharedDBInstance);
                boSystemApps.UpdateDictionary(dictionaryDetail);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchSystemDictionary(JObject postData)
        {
            HttpResponseMessage ret;
            SystemApps boSystemApps = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string queryStatus = postData["STATUS"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSystemApps = new SystemApps(boSecurity.GetSharedDBInstance);
                var dictionaryList = boSystemApps.SearchDictionary(queryString, queryStatus);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SYSTEM_DICTIONARY", dictionaryList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
    }
}