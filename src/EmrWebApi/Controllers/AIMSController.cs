﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.Web;

//Project Related
using EmrWebApi.BusinessObjects;
using EmrWebApi.Models;

namespace EmrWebApi.Controllers
{
    public class AIMSController : ApiController
    {
        //Asset
        [HttpPost]
        public HttpResponseMessage CreateNewAsset(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_ASSET_DETAILS assetDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_ASSET_DETAILS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_ASSET_DETAILS>();
                
                var warrantylist = ((JArray)postData.SelectToken("AM_ASSET_WARRANTY")).ToObject<List<AM_ASSET_WARRANTY>>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
				

				boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var assetturned = boAIMS.CreateNewAsset(assetDetails);
                var warrantyReturned = boAIMS.CreateUpdateAssetWarranty(warrantylist, assetturned.ASSET_NO, userSession.STAFF_ID);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_DETAILS", assetturned }, { "AM_ASSET_WARRANTY", warrantyReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadAssetInfo(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
			Setup boSetup = null;
            try
            {
                string assetID = postData["ASSET_NO"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var assetReturned = boAIMS.ReadAssetInfo(assetID);
                var manufactureReturned = new List<dynamic>();
                var warrantyReturnedList = boAIMS.ReadWarrantyListByAssetNo(assetID);
                var costCenterwarrantyReturnedList = boAIMS.ReadCostCenterWarrantyListAssetNo(assetID, assetReturned.COSTCENTERTRANSID.HasValue ? assetReturned.COSTCENTERTRANSID.Value : 0);
                 var costCenterTransaction = boAIMS.ReadCostCenterTransactionAssetNo(assetID, assetReturned.COSTCENTERTRANSID.HasValue ? assetReturned.COSTCENTERTRANSID.Value : 0);

                var supplierReturned = new List<dynamic>();
                if (assetReturned.SUPPLIER != null && assetReturned.SUPPLIER != "")
                {
                    supplierReturned = boAIMS.SearchManufacturerId(assetReturned.SUPPLIER);
                }
                var CurrentAgentReturned = new List<dynamic>();
                if (assetReturned.Current_Agent != null && assetReturned.Current_Agent != "")
                {
                    CurrentAgentReturned = boAIMS.SearchSupplierById(assetReturned.Current_Agent);
                }
                    
                if(assetReturned.MANUFACTURER != null && assetReturned.MANUFACTURER != "")
                {
                    manufactureReturned = boAIMS.SearchManufacturerId(assetReturned.MANUFACTURER);
                }
               
				boSetup = new Setup(boSecurity.GetSharedDBInstance);
				var LOVReturned = boSetup.getLovListLookUpId("'" + assetReturned.WARRANTY_INCLUDE + "','" +assetReturned.BUILDING+"','"+ assetReturned.CATEGORY+"','"+ assetReturned.COST_CENTER+"','"+ assetReturned.RESPONSIBLE_CENTER+"'");
                var costCenterReturned = boSetup.SearchClientV("");

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "CLIENT_COSTCENTER", costCenterReturned }, { "AM_ASSET_TRANSACTION", costCenterTransaction }, { "AM_ASSET_COSTCENTER_WARRANTY", costCenterwarrantyReturnedList }, {"AM_ASSET_DETAILS", assetReturned }, { "LOV_LOOKUPS", LOVReturned }, { "AM_SUPPLIER", supplierReturned }, { "CURRENTAGENT", CurrentAgentReturned }, { "AM_MANUFACTURER", manufactureReturned },{ "AM_ASSET_WARRANTY", warrantyReturnedList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage CreateCloneAssetInfo(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                

                string assetID = postData["ASSET_NO"].ToString();
                AM_ASSET_DETAILS assetDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_ASSET_DETAILS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_ASSET_DETAILS>();
                var warrantylist = ((JArray)postData.SelectToken("AM_ASSET_WARRANTY")).ToObject<List<AM_ASSET_WARRANTY>>();
                var clonelist = ((JArray)postData.SelectToken("AM_ASSET_DETAILS_CLONE")).ToObject<List<AM_ASSET_DETAILS_CLONE>>();

                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                
                var cloneListReturned = boAIMS.CreateAssetClone(assetDetails, clonelist, warrantylist, assetID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_DETAILS_CLONE", cloneListReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateCloneAssetInfo(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                
                var clonelist = ((JArray)postData.SelectToken("AM_ASSET_DETAILS_CLONE")).ToObject<List<AM_ASSET_DETAILS_CLONE>>();

                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);

                var cloneListReturned = boAIMS.UpdateAssetClone(clonelist);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_DETAILS_CLONE", cloneListReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateAssetInfo(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS= null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_ASSET_DETAILS assetDetail = ((JObject)JsonConvert.DeserializeObject(postData["AM_ASSET_DETAILS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_ASSET_DETAILS>();

                AM_ASSET_TRANSACTION assetTransaction = ((JObject)JsonConvert.DeserializeObject(postData["AM_ASSET_TRANSACTION"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_ASSET_TRANSACTION>();

                var warrantylist = ((JArray)postData.SelectToken("AM_ASSET_WARRANTY")).ToObject<List<AM_ASSET_WARRANTY>>();

                var ccwarrantylist = ((JArray)postData.SelectToken("AM_ASSET_COSTCENTER_WARRANTY")).ToObject<List<AM_ASSET_COSTCENTER_WARRANTY>>();

                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                assetDetail.LAST_MODIFIEDBY = userSession.STAFF_ID;
                var returnAssetDetails =  boAIMS.UpdateAssetInfo(assetDetail, assetTransaction, userSession.STAFF_ID);

                var warrantyReturned = boAIMS.CreateUpdateAssetWarranty(warrantylist, assetDetail.ASSET_NO, userSession.STAFF_ID);

                var ccwarrantyReturned = boAIMS.CreateUpdateAssetCostCenterWarranty(ccwarrantylist, assetDetail.ASSET_NO, userSession.STAFF_ID, returnAssetDetails.COSTCENTERTRANSID.HasValue ? returnAssetDetails.COSTCENTERTRANSID.Value : 0);

                


                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." }, { "AM_ASSET_WARRANTY", warrantyReturned }, { "AM_ASSET_COSTCENTER_WARRANTY", ccwarrantyReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage RedAssetInfoV(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string assetID = postData["ASSET_NO"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                

                var assetReturned = boAIMS.ReadAssetInfoV(assetID);
            
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_DETAILS", assetReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        [HttpPost]
        public HttpResponseMessage UpdateAssetTransactionReturn(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_ASSET_TRANSACTION trasactionDetail = ((JObject)JsonConvert.DeserializeObject(postData["AM_ASSET_TRANSACTION"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_ASSET_TRANSACTION>();
                AM_ASSET_DETAILS assetDetail = ((JObject)JsonConvert.DeserializeObject(postData["AM_ASSET_DETAILS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_ASSET_DETAILS>();
                string defaultCostCenterID = postData["COSTCENTER"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                boAIMS.UpdateAssetTransaction(trasactionDetail, assetDetail, defaultCostCenterID, userSession.USER_ID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }


        //Manufacturer Model
        [HttpPost]
        public HttpResponseMessage CreateNewModel(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_MANUFACTURER_MODEL infoDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_MANUFACTURER_MODEL"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_MANUFACTURER_MODEL>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var inforeturned = boAIMS.CreateNewModel(infoDetails);

                var manufactureReturned = boAIMS.SearchManufacturerId(infoDetails.VENDOR);
                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var LOVReturned = boSetup.getLovListLookUpId("'" + infoDetails.TYPE + "'");

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_MANUFACTURER_MODEL", inforeturned }, { "AM_MANUFACTURER", manufactureReturned }, { "LOOKUP_DATA", LOVReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadModel(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                string modelID = postData["MODEL_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var infoReturned = boAIMS.ReadModel(modelID);

                var manufactureReturned = boAIMS.SearchManufacturerId(infoReturned.VENDOR);
                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var LOVReturned = boSetup.getLovListLookUpId("'" + infoReturned.TYPE + "'");

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_MANUFACTURER_MODEL", infoReturned }, { "AM_MANUFACTURER", manufactureReturned }, { "LOOKUP_DATA", LOVReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateModel(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_MANUFACTURER_MODEL infoDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_MANUFACTURER_MODEL"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_MANUFACTURER_MODEL>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                boAIMS.UpdateModel(infoDetails);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchModel(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var modelList = boAIMS.SearchModel(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_MANUFACTURER_MODEL", modelList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        //Asset Notes
        [HttpPost]
        public HttpResponseMessage CreateNewAssetNotes(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_ASSET_NOTES infoDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_ASSET_NOTES"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_ASSET_NOTES>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var assetNotesturned = boAIMS.CreateNewAssetNotes(infoDetails);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_NOTES", assetNotesturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchAssetNotes(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var assetNotesList = boAIMS.SearchAssetNotes(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_NOTES", assetNotesList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        //Work Order  Notes
        [HttpPost]
        public HttpResponseMessage CreateNewWONotes(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_WORK_ORDER_REFNOTES infoDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_WORK_ORDER_REFNOTES"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_WORK_ORDER_REFNOTES>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var assetNotesturned = boAIMS.CreateNewWORefNotes(infoDetails);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_WORK_ORDER_REFNOTES", assetNotesturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchWONotes(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var woNotesList = boAIMS.SearchWORefNotes(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_WORK_ORDER_REFNOTES", woNotesList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchAssetWONotes(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var woNotesList = boAIMS.SearchAssetWORefNotes(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_WORK_ORDER_REFNOTES", woNotesList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchAssetsOnhand(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var supplierList = boAIMS.SearchAssetOnHand(queryString, filter, userSession.USER_ID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_ONHAND", supplierList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchAssetConstSummary(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                string assetID = postData["ASSET_NO"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
               
                var woSummaryList = boAIMS.SearchWOSummaryById(assetID);
                var wocostSummaryList = boAIMS.SearchWOCostSummaryById(assetID);
                var wocontractcostSummaryList = boAIMS.SearchWOContractCostSummaryById(assetID);
                var womaterialcostSummaryList = boAIMS.SearchWOMaterialCostSummaryById(assetID);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() {{ "ASSET_WOSUMMARY_V", woSummaryList } , { "ASSET_WOPMOTCOST_V", wocostSummaryList }, { "ASSET_WOCONTRACTCOST_V", wocontractcostSummaryList }, { "ASSET_WOMATERIALCOST_V", womaterialcostSummaryList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchAssets(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
				var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var supplierList = boAIMS.SearchAsset(queryString, filter, userSession.USER_ID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_DETAILS", supplierList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchAssetsCloneId(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            Setup boSetup = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string queryLOV = postData["LOV"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var assetList = boAIMS.SearchAssetCloneId(queryString, filter);
               
                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovList = boSetup.getLovList(queryLOV);
               
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() {  {"LOV_LOOKUPS", lovList }, { "AM_ASSET_DETAILSCLONE", assetList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
		public HttpResponseMessage SearchAssetsCostCenter(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var supplierList = boAIMS.SearchAssetCostCenter(queryString, filter);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_DETAILS", supplierList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
        [HttpPost]
        public HttpResponseMessage SearchAssetsActive(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var supplierList = boAIMS.SearchAssetActive(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_DETAILS", supplierList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchAssetsWOActive(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var assetList = boAIMS.SearchAssetWOActive(queryString, filter, userSession.USER_ID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_LOVLIST", assetList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchAssetsBIOActive(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var assetList = boAIMS.SearchAssetBIOActive(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_LOVLIST", assetList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        //Supplier
        [HttpPost]
        public HttpResponseMessage CreateNewSupplier(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_SUPPLIER supplierDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_SUPPLIER"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_SUPPLIER>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var assetturned = boAIMS.CreateNewSupplier(supplierDetails);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_SUPPLIER", assetturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadSupplierInfo(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string supplierID = postData["SUPPLIER_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var assetReturned = boAIMS.ReadSupplierInfo(supplierID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_SUPPLIER", assetReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateSupplierInfo(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_SUPPLIER supplierDetail = ((JObject)JsonConvert.DeserializeObject(postData["AM_SUPPLIER"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_SUPPLIER>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                boAIMS.UpdateSupplierInfo(supplierDetail);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchSuppier(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var supplierList = boAIMS.SearchSupplier(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_SUPPLIER", supplierList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
		[HttpPost]
		public HttpResponseMessage SearchSuppierLOV(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var supplierList = boAIMS.SearchSupplierLookup(queryString);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_SUPPLIERLIST", supplierList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage SearchSuppierLOVData(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var supplierList = boAIMS.SearchSupplierLookupData(queryString);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_SUPPLIERLIST", supplierList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		//PM Procedure
		[HttpPost]
		public HttpResponseMessage CreateNewPMProcedure(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				//gather data                
				AM_PROCEDURES infoDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_PROCEDURES"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_PROCEDURES>();
				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "UPDATE");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var procedureturned = boAIMS.CreateNewProcedure(infoDetails);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PROCEDURES", procedureturned } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage ReadPMProcedureInfo(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				string procedureID = postData["PROCEDURE_ID"].ToString();
				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var procedureReturned = boAIMS.ReadProcedureInfo(procedureID);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PROCEDURES", procedureReturned } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage UpdatePMProcedureInfo(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				//gather data                
				AM_PROCEDURES infoDetail = ((JObject)JsonConvert.DeserializeObject(postData["AM_PROCEDURES"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_PROCEDURES>();
				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "UPDATE");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				boAIMS.UpdateProcedureInfo(infoDetail);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}

			return ret;
		}
		[HttpPost]
		public HttpResponseMessage SearchPMProcedure(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var procedureList = boAIMS.SearchProcedure(queryString);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PROCEDURES", procedureList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}

		//ASSET Risk Factor Setup
		[HttpPost]
		public HttpResponseMessage CreateNewRFS(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				//gather data                
				RISK_CATEGORYFACTOR rfsDetails = ((JObject)JsonConvert.DeserializeObject(postData["RISK_CATEGORYFACTOR"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<RISK_CATEGORYFACTOR>();
				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "UPDATE");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var rfsreturned = boAIMS.CreateNewRiskFactorSetup(rfsDetails);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "RISK_CATEGORYFACTOR", rfsreturned } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage ReadRFSInfo(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				string rfsID = postData["RISK_FACTORCODE"].ToString();
				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var rfsReturned = boAIMS.ReadRiskFactorSetup(rfsID);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "RISK_CATEGORYFACTOR", rfsReturned } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage UpdateRFSInfo(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				//gather data                
				RISK_CATEGORYFACTOR rfsDetail = ((JObject)JsonConvert.DeserializeObject(postData["RISK_CATEGORYFACTOR"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<RISK_CATEGORYFACTOR>();
				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "UPDATE");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				boAIMS.UpdateRiskFactorSetup(rfsDetail);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}

			return ret;
		}
		[HttpPost]
		public HttpResponseMessage SearchRFS(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				string reftype = postData["RISK_CATEGORYCODE"].ToString();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var rfsList = boAIMS.SearchRiskFactorSetup(queryString, reftype);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "RISK_CATEGORYFACTOR", rfsList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage SearchRFGS(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var rfgsList = boAIMS.SearchRiskFactorGroupSetup(queryString);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "RISK_CATEGORYGROUP", rfgsList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}

		//WorkSheet/Inspection
		[HttpPost]
        public HttpResponseMessage CreateNewWS(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_INSPECTION infoDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_INSPECTION"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_INSPECTION>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var wsturned = boAIMS.CreateNewWorkSheet(infoDetails);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_INSPECTION", wsturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadWSInfo(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string infoID = postData["AM_INSPECTION_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var wsReturned = boAIMS.ReadWorkSheetInfo(infoID);
                var wsAssetReturned = boAIMS.SearchInspectionAsset(infoID);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_INSPECTION", wsReturned }, { "AM_INSPECTIONASSET", wsAssetReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateWSInfo(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_INSPECTION infoDetail = ((JObject)JsonConvert.DeserializeObject(postData["AM_INSPECTION"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_INSPECTION>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                boAIMS.UpdateWorkSheetInfo(infoDetail);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchWS(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var wsList = boAIMS.SearchWorkSheet(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_INSPECTION", wsList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        // Worksheet Inspection Asset
        [HttpPost]
        public HttpResponseMessage CreateNewWSAsset(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_INSPECTIONASSET infoDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_INSPECTIONASSET"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_INSPECTIONASSET>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var wsturned = boAIMS.CreateNewWSAsset(infoDetails);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_INSPECTIONASSET", wsturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage DeleteWSAsset(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                
                string queryString = postData["AM_INSPECTIONASSETID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var deleteReturned = boAIMS.DeleteWSAsset(queryString);


                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_INSPECTIONASSET", deleteReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        //Asset Meter Reading
        [HttpPost]
        public HttpResponseMessage CreateNewReading(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_METERREADING infoDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_METERREADING"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_METERREADING>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var wsturned = boAIMS.CreateNewMeterReading(infoDetails);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_METERREADING", wsturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchReading(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var wsList = boAIMS.SearchMeterReading(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_METERREADING", wsList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        //Company Purchase Request
        [HttpPost]
        public HttpResponseMessage CreateOrUpdateCompanyPR(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data        
                AM_PURCHASEREQUEST infoDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_PURCHASEREQUEST"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_PURCHASEREQUEST>();
                var newList = ((JArray)postData.SelectToken("AM_PARTSREQUEST")).ToObject<List<AM_PARTSREQUEST>>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();
                var mode = postData["MODE"].ToString();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var newListReturned = boAIMS.CreateOrUpdateCompanyPR(infoDetails, newList, mode);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASEREQUEST", newListReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        //Purchase Request
        [HttpPost]
        public HttpResponseMessage CreateNewPurchaseReq(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            NotifySender boNotifySender = null;
            try
            {
                //gather data        
                AM_PURCHASEREQUEST infoDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_PURCHASEREQUEST"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_PURCHASEREQUEST>();
                var newList = ((JArray)postData.SelectToken("AM_PARTSREQUEST")).ToObject<List<AM_PARTSREQUEST>>();

                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();
                var createDate = postData["CREATE_DATE"].ToString();
                var actionStr = postData["ACTION"].ToString();
                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var newListReturned = boAIMS.CreateNewPurchaseRequest(infoDetails,newList, userSession.STAFF_ID, createDate, actionStr);

                boNotifySender = new NotifySender(boSecurity.GetSharedDBInstance);
                var notifyReturn = boNotifySender.UpdateNotify("3", "XXXXVCCCCVV", newListReturned.REQUEST_ID, "BSAIMS New Purchase Request (No."+ newListReturned.REQUEST_ID +") for approval", "N", userSession.LOGON_DATE);
                
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASEREQUEST", newListReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage GetCompanyPRByRequest(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string prRequetNo = postData["REQUEST_ID"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var prInfo = boAIMS.ReadPurchaseRequest(prRequetNo);
                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var prItemsInfo = boAIMS.ReadPurchaseRequestItems(prRequetNo);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASEREQUEST", prInfo }, { "AM_PURCHASEREQUESITEMS", prItemsInfo } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadPRDetails(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string prRequetNo = postData["REQUEST_ID"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var prInfo = boAIMS.ReadPurchaseRequest(prRequetNo);
                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var prItemsInfo = boAIMS.ReadPurchaseRequestItems(prRequetNo);

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var WOLprList = boInventory.readWOIdPRMaterials(prInfo.REF_WO.ToString());

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASEREQUEST", prInfo }, { "AM_PURCHASEREQUESITEMS", prItemsInfo }, { "AM_PURCHASEREQUESITEMSFORPR", WOLprList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdatePurchaseRequest(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data        

                var newPOList = ((JArray)postData.SelectToken("AM_PARTSREQUEST")).ToObject<List<AM_PARTSREQUEST>>();
                AM_PURCHASEREQUEST POinfoDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_PURCHASEREQUEST"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_PURCHASEREQUEST>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var newpoReturned = boAIMS.UpdatePurchaseRequest(newPOList, POinfoDetails);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdatePRStatus(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                //var reqID = postData["REQUEST_ID"].ToString();
                AM_PURCHASEREQUEST infoDetail = ((JObject)JsonConvert.DeserializeObject(postData["AM_PURCHASEREQUEST"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_PURCHASEREQUEST>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");
                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                boAIMS.UpdatePurchaseReqStatus(infoDetail);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        public HttpResponseMessage SearchCompanyPR(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var partList = boAIMS.SearchCompanyPR(queryString, filter);


                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASEREQUEST", partList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        public HttpResponseMessage SearchPurchaseReqDetFlag(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string queryAction = postData["RETACTION"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var partList = boAIMS.SearchPurchaseRequest(queryString, queryAction, filter);


                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASEREQUEST", partList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        public HttpResponseMessage SearchPurchaseReqDet(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["REQUEST_ID"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var prList = boAIMS.readPurchaseReq(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASEREQUEST", prList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        public HttpResponseMessage SearchPurchasePRWO(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var prwoList = boAIMS.SearchPurchaseRequestWOParts(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASEREQUESTWOPARTS", prwoList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
		public HttpResponseMessage SearchPRApproved(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var prList = boAIMS.SearchPRApprovedSummary(queryString);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASEREQUEST_APPROVEDLIST", prList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage SearchPurchaseApprove(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string queryAction = postData["RETACTION"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var partList = boAIMS.SearchPurchaseRequestApprove(queryString, queryAction);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASEREQUEST", partList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
		[HttpPost]
		public HttpResponseMessage SearchPurchaseApproveByProdId(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				string prodId = postData["ID_PARTS"].ToString();
				string purchaseNo = postData["PURCHASE_NO"].ToString();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var partList = boAIMS.SearchPurchaseRequestApproveByProdId(queryString, prodId, purchaseNo);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASEREQUESTFORPOLIST", partList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}

        [HttpPost]
        public HttpResponseMessage SearchPurchaseRequestItemInfo(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string prodId = postData["ID_PARTS"].ToString();
                string prNo = postData["REQUEST_ID"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var partList = boAIMS.SearchPurchaseRequestItemInfo(queryString, prodId, prNo);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASEREQUESTFORPOLIST", partList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        //Purchase Order
        [HttpPost]
		public HttpResponseMessage CreateNewPurchaseOrder(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
            NotifySender boNotifySender = null;

            try
			{
				//gather data        

				var newPOList = ((JArray)postData.SelectToken("AM_PURCHASEORDER_ITEMS")).ToObject<List<AM_PURCHASEORDER_ITEMS>>();
				AM_PURCHASEORDER POinfoDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_PURCHASEORDER"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_PURCHASEORDER>();
				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();
				//var createDate = postData["CREATE_DATE"].ToString();
				//var actionStr = postData["ACTION"].ToString();
				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "UPDATE");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var newpoReturned = boAIMS.CreateNewPurchaseOrder(newPOList, POinfoDetails);
                var subTitle = "BSAIMS New Purchase Order(No." + newpoReturned.PURCHASE_NO.ToString() + ") for approval";
                var refId = newpoReturned.PURCHASE_NO.ToString();
                boNotifySender = new NotifySender(boSecurity.GetSharedDBInstance);
                var notifyReturn = boNotifySender.UpdateNotify("4", "XXXXVCCCCVV", refId, subTitle, "N", userSession.LOGON_DATE);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASEORDER", newpoReturned } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}

        [HttpPost]
        public HttpResponseMessage UpdatePurchaseOrderReq(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data        

                var newPOList = ((JArray)postData.SelectToken("AM_PURCHASEORDER_ITEMS")).ToObject<List<AM_PURCHASEORDER_ITEMS>>();
                AM_PURCHASEORDER POinfoDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_PURCHASEORDER"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_PURCHASEORDER>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();
                //var createDate = postData["CREATE_DATE"].ToString();
                //var actionStr = postData["ACTION"].ToString();
                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var newpoReturned = boAIMS.UpdatePurchaseOrder(newPOList, POinfoDetails);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
		public HttpResponseMessage UpdateStatusPurchaseOrder(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				//gather data        
				AM_PURCHASEORDER POinfoDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_PURCHASEORDER"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_PURCHASEORDER>();
				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();
				//var createDate = postData["CREATE_DATE"].ToString();
				//var actionStr = postData["ACTION"].ToString();
				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "UPDATE");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var newpoReturned = boAIMS.UpdateStatusPurchaseOrder(POinfoDetails);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASEORDER", newpoReturned } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage SearchPO(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				string queryAction = postData["RETACTION"].ToString();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var poList = boAIMS.SearchPurchaseOrder(queryString, queryAction);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASEORDERLIST", poList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage SearchPOItemsApprove(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				string queryAction = postData["RETACTION"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                //var partList = boAIMS.SearchPOItemsApprove(queryString, queryAction);
                var partList = boAIMS.SearchPurchaseRequestItemApprove(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASEREQUEST", partList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage ReadPODetails(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				string poRequetNo = postData["PURCHASE_NO"].ToString();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var poInfo = boAIMS.ReadPO(poRequetNo);
				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var poItemsInfo = boAIMS.readPOItems(poRequetNo);
				var supplierReturned = boAIMS.SearchSupplierById(poInfo.SUPPLIER_ID);
				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASEORDER", poInfo }, { "AM_PURCHASEORDERITEMS", poItemsInfo }, { "AM_SUPPLIER", supplierReturned } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage SearchPurchaseOrderItemBatch(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["PURCHASE_NO"].ToString();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var prList = boAIMS.readPurchaseOrderItems(queryString);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASEREQUEST", prList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}

		//Worksheet Inspection Transaction
		[HttpPost]
        public HttpResponseMessage CreateNewWSTrans(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_INSPECTIONTRANS infoDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_INSPECTIONTRANS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_INSPECTIONTRANS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var wsturned = boAIMS.CreateNewInspectionTrans(infoDetails);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_INSPECTIONTRANS", wsturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        
        [HttpPost]
        public HttpResponseMessage UpdateWSInfoTrans(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_INSPECTIONTRANS infoDetail = ((JObject)JsonConvert.DeserializeObject(postData["AM_INSPECTIONTRANS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_INSPECTIONTRANS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                boAIMS.UpdateInspectionTransInfo(infoDetail);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchWSTrans(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Staff boStaffUser = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var wsList = boAIMS.SearchInspectionTrans(queryString);
                boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
                var staffuserList = boStaffUser.SearchStaffLookUp("");
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_INSPECTIONTRANS", wsList }, { "STAFFLIST", staffuserList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        //Asset Parts
        [HttpPost]
        public HttpResponseMessage CreateNewAmPart(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_PARTS partDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_PARTS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_PARTS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var assetturned = boAIMS.CreateNewAssetPart(partDetails);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PARTS", assetturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage CreateOrUpdateCustomerDiscount(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_PROD_CUSTOMER_DISCOUNT customerDiscount = ((JObject)JsonConvert.DeserializeObject(postData["am_prod_customer_discount"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_PROD_CUSTOMER_DISCOUNT>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var assetturned = boAIMS.CreateOrUpdateCustomerDiscount(customerDiscount);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "am_prod_customer_discount", assetturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage ReadAmPart(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string partID = postData["ID_PARTS"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var partReturned = boAIMS.ReadAssetPart(partID);
                var partSupplierReturned = boAIMS.ReadAssetPartSupplier(partID);
                var partModelsReturned = boAIMS.SearchAssetPartsModels(partID);
                var customerDiscount = boAIMS.SearchCustomerDiscount(partID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PARTS", partReturned }, { "PARTNO_BYPARTID_V", partSupplierReturned }, { "AM_PARTS_MODELDETAILS", partModelsReturned }, { "AM_PROD_CUSTOMER_DISCOUNT", customerDiscount } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateAmPart(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data
                var newPartModelsList = new List<AM_PARTS_MODELDETAILS>();
                if (postData["AM_PARTS_MODELDETAILS"] != null)
               
                {
                    newPartModelsList = ((JArray)postData.SelectToken("AM_PARTS_MODELDETAILS")).ToObject<List<AM_PARTS_MODELDETAILS>>();
                }
                
                AM_PARTS partDetail = ((JObject)JsonConvert.DeserializeObject(postData["AM_PARTS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_PARTS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                boAIMS.UpdateAssetPart(partDetail, newPartModelsList);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchAmParts(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var partList = boAIMS.SearchAssetParts(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PARTS", partList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchAmPartsActive2(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var partList = boAIMS.SearchAmPartsActive2(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PARTS", partList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchAmPartsActive(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var partList = boAIMS.SearchAssetPartsActive(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PARTS", partList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchAmPartsWOPMPart(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string refId = postData["REFID"].ToString();
                string refType ="PMSCHEDULE";
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var partList = boAIMS.SearchAssetParts(queryString);

                var WOPMpartList = boAIMS.SearchWOPMPartsById(queryString, refType, refId);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PARTS", partList } , { "AM_WOPM_PARTS", WOPMpartList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        //PARTS SUPPLIER DETAILS
        [HttpPost]
        public HttpResponseMessage CreateNewPartSP(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_PARTSUPPLIERDETAILS partDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_PARTSUPPLIERDETAILS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_PARTSUPPLIERDETAILS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var partSPreturned = boAIMS.CreateNewPartSP(partDetails);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PARTSUPPLIERDETAILS", partSPreturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadPartSP(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string partSPID = postData["PARTSP_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var partSPReturned = boAIMS.ReadPartSP(partSPID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PARTSUPPLIERDETAILS", partSPReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdatePartSP(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_PARTSUPPLIERDETAILS partSPDetail = ((JObject)JsonConvert.DeserializeObject(postData["AM_PARTSUPPLIERDETAILS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_PARTSUPPLIERDETAILS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                boAIMS.UpdatePartSP(partSPDetail);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchPartSP(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string idPart = postData["ID_PARTS"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var partSPList = boAIMS.SearchPartSP(queryString, idPart);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PARTSUPPLIERDETAILS", partSPList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        //WOPM Parts
        [HttpPost]
        public HttpResponseMessage CreateNewWOPMPart(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data        

                var newWOPMPartsList = ((JArray)postData.SelectToken("AM_WOPM_PARTS")).ToObject<List<AM_WOPM_PARTS>>();

                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();
                var createDate = postData["CREATE_DATE"].ToString();
                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var wopmListReturned = boAIMS.CreateNewWOPMPart(newWOPMPartsList, userSession.STAFF_ID, createDate);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_WOPM_PARTS", wopmListReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        
        [HttpPost]
        public HttpResponseMessage ReadWOPMPart(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string partSPID = postData["WOPM_PARTS_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var WOPMpartReturned = boAIMS.ReadWOPMPart(partSPID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_WOPM_PARTS", WOPMpartReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateWOPMPart(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_WOPM_PARTS WOPMpartDetail = ((JObject)JsonConvert.DeserializeObject(postData["AM_WOPM_PARTS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_WOPM_PARTS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                boAIMS.UpdateWOPMPart(WOPMpartDetail);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchWOPMPart(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string refId= postData["REF_TYPE"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var WOPMpartList = boAIMS.SearchWOPMParts(queryString, refId);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_WOPM_PARTS", WOPMpartList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        //PM Schedule
        [HttpPost]
        public HttpResponseMessage CreateNewSchedule(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_ASSET_PMSCHEDULES pmscheduleDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_ASSET_PMSCHEDULES"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_ASSET_PMSCHEDULES>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var pmScheduleturned = boAIMS.CreateNewPMSchedule(pmscheduleDetails);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_PMSCHEDULES", pmScheduleturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadPMSchedule(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string pmscheduleID = postData["AM_ASSET_PMSCHEDULE_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var pmscheduleReturned = boAIMS.readPMSchedule(pmscheduleID);
                var scandocsCategoryImagesLists = boAIMS.SearchScandocsByAssetId(pmscheduleID);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_PMSCHEDULES", pmscheduleReturned }, { "SCANDOCS_CATEGORYIMAGELIST", scandocsCategoryImagesLists } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdatePMSchedule(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_ASSET_PMSCHEDULES pmscheduleDetail = ((JObject)JsonConvert.DeserializeObject(postData["AM_ASSET_PMSCHEDULES"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_ASSET_PMSCHEDULES>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                boAIMS.UpdatePMSchedule(pmscheduleDetail, userSession.STAFF_ID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchPMSchedule(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAims = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string assetNo = postData["ASSETNO"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAims = new AIMS(boSecurity.GetSharedDBInstance);
                var pmScheduleList = boAims.SearchPMSchedule(queryString, assetNo);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_PMSCHEDULES_LIST", pmScheduleList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
		[HttpPost]
		public HttpResponseMessage SearchPMLookUp(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAims = null;
			Setup boSetup = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boSetup = new Setup(boSecurity.GetSharedDBInstance);
				var lovList = boSetup.getLovList(queryString);

				boAims = new AIMS(boSecurity.GetSharedDBInstance);
				var pmProcedureList = boAims.SearchProcedureActiveLookup("");

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PROCEDURES", pmProcedureList }, { "LOV_LOOKUPS", lovList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}

		//Asset Information Services
		[HttpPost]
        public HttpResponseMessage CreateNewInfoServices(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_ASSET_INFOSERVICES infoDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_ASSET_INFOSERVICES"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_ASSET_INFOSERVICES>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var infoReturned = boAIMS.CreateNewInfoServices(infoDetails);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_INFOSERVICES", infoReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadInfoServices(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string infoServiceID = postData["ASSET_INFOSERVICES_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var infoReturned = boAIMS.readInfoServices(infoServiceID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_INFOSERVICES", infoReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateInfoServices(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_ASSET_INFOSERVICES infoDetail = ((JObject)JsonConvert.DeserializeObject(postData["AM_ASSET_INFOSERVICES"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_ASSET_INFOSERVICES>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                boAIMS.UpdateInfoServices(infoDetail);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }

		//Asset Information Services Dvice
		[HttpPost]
		public HttpResponseMessage CreateNewInfoSRVDvice(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				//gather data                
				AM_ASSET_INFOSERVICES_DEVICE assetDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_ASSET_INFOSERVICES_DEVICE"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_ASSET_INFOSERVICES_DEVICE>();
				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "UPDATE");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var assetInfoSRVReturned = boAIMS.CreateNewINFOSRVDvice(assetDetails);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_INFOSERVICES_DEVICE", assetInfoSRVReturned } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage ReadInfoSRVDvice(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				string infoServiceID = postData["DEVICE_ID"].ToString();
				string assetNo = postData["ASSET_NO"].ToString();
				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var infoReturned = boAIMS.readINFOSRVDvice(infoServiceID, assetNo);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_INFOSERVICES_DEVICE", infoReturned } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage UpdateInfoSRVDvice(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				//gather data                
				AM_ASSET_INFOSERVICES_DEVICE infoDetail = ((JObject)JsonConvert.DeserializeObject(postData["AM_ASSET_INFOSERVICES_DEVICE"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_ASSET_INFOSERVICES_DEVICE>();
				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "UPDATE");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				boAIMS.UpdateINFOSRVDvice(infoDetail);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}

			return ret;
		}
		[HttpPost]
		public HttpResponseMessage SearchInfoSRVDvice(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAims = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				string assetNo = postData["ASSETNO"].ToString();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boAims = new AIMS(boSecurity.GetSharedDBInstance);
				var infoSRVDviceList = boAims.SearchINFOSRVDvice(queryString, assetNo);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_INFOSERVICES_DEVICE", infoSRVDviceList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}

		//ASSET CONTRACT SERVICE
		[HttpPost]
        public HttpResponseMessage CreateNewContractService(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_ASSET_CONTRACTSERVICES contractserviceDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_ASSET_CONTRACTSERVICES"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_ASSET_CONTRACTSERVICES>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var contractserviceturned = boAIMS.CreateNewContractService(contractserviceDetails);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_CONTRACTSERVICES", contractserviceturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadAssetContractService(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string pmscheduleID = postData["ASSET_CONTRACTSERVICE_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var contractserviceReturned = boAIMS.readContractService(pmscheduleID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_CONTRACTSERVICES", contractserviceReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateAssetContractService(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_ASSET_CONTRACTSERVICES contractserviceDetail = ((JObject)JsonConvert.DeserializeObject(postData["AM_ASSET_CONTRACTSERVICES"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_ASSET_CONTRACTSERVICES>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                boAIMS.UpdateContractService(contractserviceDetail);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchAssetContractService(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAims = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string assetNo = postData["ASSETNO"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAims = new AIMS(boSecurity.GetSharedDBInstance);
                var contractserviceList = boAims.SearchAssetContractService(queryString, assetNo);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSET_CONTRACTSERVICES", contractserviceList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        //Manufacturer
        [HttpPost]
        public HttpResponseMessage CreateNewManufacturer(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_MANUFACTURER manufacturerDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_MANUFACTURER"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_MANUFACTURER>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                var CLIENTOTHCONTACTS = new List<CLIENTOTHCONTACT>();
                if (postData["CLIENTOTHCONTACT"] != null)
                {
                    CLIENTOTHCONTACTS = ((JArray)postData.SelectToken("CLIENTOTHCONTACT")).ToObject<List<CLIENTOTHCONTACT>>();
                }

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var assetturned = boAIMS.CreateNewManufacturer(manufacturerDetails, CLIENTOTHCONTACTS, userSession.USER_ID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_MANUFACTURER", assetturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadManufacturerInfo(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                string manufacturerID = postData["MANUFACTURER_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var assetReturned = boAIMS.ReadManufacturerInfo(manufacturerID);

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var CLIENTOTHCONTACTS = boSetup.ReadCostCenterClientOthContacts(manufacturerID, "MV");

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_MANUFACTURER", assetReturned }, { "CLIENTOTHCONTACTS", CLIENTOTHCONTACTS } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateManufacturerInfo(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_MANUFACTURER manufacturerDetail = ((JObject)JsonConvert.DeserializeObject(postData["AM_MANUFACTURER"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_MANUFACTURER>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                var CLIENTOTHCONTACTS = new List<CLIENTOTHCONTACT>();
                if (postData["CLIENTOTHCONTACT"] != null)
                {
                    CLIENTOTHCONTACTS = ((JArray)postData.SelectToken("CLIENTOTHCONTACT")).ToObject<List<CLIENTOTHCONTACT>>();
                }

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                boAIMS.UpdateManufacturerInfo(manufacturerDetail, CLIENTOTHCONTACTS, userSession.USER_ID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage DeleteManufacturerInfo(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                var manufacturerId = postData["manufacturerId"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                var CLIENTOTHCONTACTS = new List<CLIENTOTHCONTACT>();
                if (postData["CLIENTOTHCONTACT"] != null)
                {
                    CLIENTOTHCONTACTS = ((JArray)postData.SelectToken("CLIENTOTHCONTACT")).ToObject<List<CLIENTOTHCONTACT>>();
                }

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                boAIMS.DeleteManufacturerInfo(manufacturerId);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully deleted." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchManufacturer(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var manufacturerList = boAIMS.SearchManufacturer(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_MANUFACTURER", manufacturerList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
		[HttpPost]
		public HttpResponseMessage SearchManufacturerLookUp(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var manufacturerList = boAIMS.SearchManufacturerLookUp(queryString);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_MANUFACTURER", manufacturerList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage SearchMSVLookUp(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				string type = postData["TYPE"].ToString();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var manufacturerList = boAIMS.SearchMSVLookUp(queryString, type);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_MANUFACTURER", manufacturerList }, { "TYPE", type } });
            }
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}

		//Work Order
		[HttpPost]
        public HttpResponseMessage CreateNewWO(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            NotifySender boNotifySender = null;
            Security boSecurity = null;
            Setup boSetup = null;
            Staff boStaff = null;
            try
            {
                //gather data                
                AM_WORK_ORDERS woDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_WORK_ORDERS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_WORK_ORDERS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var woturned = boAIMS.CreateNewWordOrder(woDetails);
                var assetNo = "";
                if (woturned.ASSET_NO == null || woturned.ASSET_NO == "")
                {
                    assetNo = "WOWORKSHEET";
                }
                else
                {
                    assetNo = woturned.ASSET_NO;
                }
                boNotifySender = new NotifySender(boSecurity.GetSharedDBInstance);
                if (woturned.ASSIGN_TO != null && woturned.ASSIGN_TO != "")
                {
                    var notifyReturn = boNotifySender.UpdateNotify("1", woturned.ASSIGN_TO, woturned.REQ_NO, "BSAIMS New Work Order No." + woturned.REQ_NO, "Y", userSession.LOGON_DATE);
                }

                var warrantyReturnedList = boAIMS.ReadWarrantyListByAssetNoV(assetNo);

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var clientInformation = boSetup.ReadClientInfo("BS1");

                var assetReturned = boAIMS.ReadAssetInfoVWO(assetNo);
                var supplierList = boAIMS.SearchSupplierById(woturned.ASSIGN_OUTSOURCE);

                boStaff = new Staff(boSecurity.GetSharedDBInstance);
                var staffList = boStaff.SearchStaffById(woturned.ASSIGN_TO);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF_INFO", staffList }, { "AM_SUPPLIERLIST", supplierList }, { "AM_WORK_ORDERS", woturned }, { "AM_ASSET_DETAILS", assetReturned }, { "AM_ASSET_WARRANTY", warrantyReturnedList }, { "AM_CLIENT_INFO", clientInformation } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
		[HttpPost]
		public HttpResponseMessage ReadWO(JObject postData)
		{
			HttpResponseMessage ret;
			AIMS boAIMS = null;
            Inventory boInventory = null;
            Security boSecurity = null;
            Setup boSetup = null;
            Staff boStaff = null;
            try
			{
				string reqID = postData["REQ_NO"].ToString();
				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var woReturned = boAIMS.ReadWorkOrder(reqID);
				var assetNo = "";
				if (woReturned.ASSET_NO == null || woReturned.ASSET_NO == "") {
					assetNo = "WOWORKSHEET";
				} else {
					assetNo = woReturned.ASSET_NO;
				}
                var assetReturned = boAIMS.ReadAssetInfoVWO(assetNo);
                boInventory = new Inventory(boSecurity.GetSharedDBInstance);

                var WOLprList = boInventory.readWOIdMaterials(reqID);
                var scandocsCategoryImagesLists = boAIMS.SearchScandocsByCategoryId(reqID, "ASSETWO");
                var warrantyReturnedList = boAIMS.ReadWarrantyListByAssetNoV(assetNo);

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var clientInformation = boSetup.ReadClientInfo("BS1");
                var supplierList = boAIMS.SearchSupplierById(woReturned.ASSIGN_OUTSOURCE);

                boStaff = new Staff(boSecurity.GetSharedDBInstance);
                var staffList = boStaff.SearchStaffById(woReturned.ASSIGN_TO);
                
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF_INFO", staffList }, { "AM_SUPPLIERLIST", supplierList }, { "AM_PARTSREQUEST", WOLprList }, { "AM_WORK_ORDERS", woReturned }, { "AM_ASSET_DETAILS", assetReturned }, { "SCANDOCS_CATEGORYIMAGELIST", scandocsCategoryImagesLists }, { "AM_ASSET_WARRANTY", warrantyReturnedList }, { "AM_CLIENT_INFO", clientInformation } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage ReadWOQuickView(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Inventory boInventory = null;
            Security boSecurity = null;
            Setup boSetup = null;
            try
            {
                string reqID = postData["REQ_NO"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var woReturned = boAIMS.ReadWorkOrderQuickView(reqID);
                var wolList = boAIMS.SearchWOLabourAct("", reqID);
                var assetNo = "";
                if (woReturned.QVASSET_NO == null || woReturned.QVASSET_NO == "")
                {
                    assetNo = "WOWORKSHEET";
                }
                else
                {
                    assetNo = woReturned.QVASSET_NO;
                }
               
                boInventory = new Inventory(boSecurity.GetSharedDBInstance);

                var WOLprList = boInventory.readWOIdMaterials(reqID);               
                var warrantyReturnedList = boAIMS.ReadWarrantyListByAssetNoV(assetNo);

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var assetReturned = boAIMS.ReadAssetQuickView(assetNo);
                
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "WORK_ORDER_LABOUR", wolList }, { "ASSET_QUICKVIEW_V", assetReturned }, { "AM_PARTSREQUEST", WOLprList }, { "WORK_ORDER_QUICKVIEW_V", woReturned } , { "AM_ASSET_WARRANTY", warrantyReturnedList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage UpdateWO(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            NotifySender boNotifySender = null;
            Security boSecurity = null;
            Emessages boEmessages = null;
            try
            {
                //gather data   
                string modifyBy = postData["MODIFY_BY"].ToString();
                string modifyDate = postData["MODIFY_DATE"].ToString();
                AM_WORK_ORDERS woDetail = ((JObject)JsonConvert.DeserializeObject(postData["AM_WORK_ORDERS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_WORK_ORDERS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                boAIMS.UpdateWorkOrder(woDetail, modifyDate, modifyBy);

                boNotifySender = new NotifySender(boSecurity.GetSharedDBInstance);
                if(woDetail.ASSIGN_TO != null && woDetail.ASSIGN_TO != "")
                {
                    var notifyReturn = boNotifySender.UpdateNotify("1", woDetail.ASSIGN_TO, woDetail.REQ_NO, "BSAIMS New Work Order No."+ woDetail.REQ_NO, "Y", userSession.LOGON_DATE);
                    if(woDetail.WO_STATUS == "HA")
                    {
                        var notifyOnHoldReturn = boNotifySender.UpdateNotify("8", "TEXCSDFDSFSDT", woDetail.REQ_NO, "BSAIMS On-Hold Work Order No." + woDetail.REQ_NO, "N", userSession.LOGON_DATE);
                    }
                    if (woDetail.WO_STATUS == "CL")
                    {
                        if(woDetail.TICKET_ID > 0)
                        {
                            boEmessages = new Emessages(boSecurity.GetSharedDBInstance);
                            var helpdeskInfo = boEmessages.ReadHelpDesk(woDetail.TICKET_ID.ToString());

                            var notifyOnHoldReturn = boNotifySender.UpdateNotify("9", helpdeskInfo.REQ_STAFF_ID, helpdeskInfo.TICKET_ID.ToString(), "BSAIMS Ticket No." + helpdeskInfo.TICKET_ID.ToString()+ " has been completed", "Y", userSession.LOGON_DATE);
                        }
                        
                    }


                }
                

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchWO(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            Setup boSetup = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string assetNo = postData["ASSETNO"].ToString();
				string searchStatus = postData["STATUS"].ToString();
				string searchbyStaffId = postData["STAFF_ID"].ToString();
                string clientId = postData["CLIENT_ID"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var worderList = boAIMS.SearchWorkOrder(queryString, assetNo, filter, searchStatus, searchbyStaffId, userSession.USER_ID);
                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var clientInformation = boSetup.ReadClientInfo("BS1");
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_WORK_ORDERS", worderList }, { "AM_CLIENT_INFO", clientInformation } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchWOStatus(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string reqId = postData["REQ_NO"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var worderList = boAIMS.SearchWorkOrderStatus("", reqId);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_WORK_ORDERS_STATUS_LOG", worderList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage UpdateWorkOrderBulk(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                string updateAction = postData["UPDATE_ACTION"].ToString();
                var builklist = ((JArray)postData.SelectToken("AM_WORK_ORDER_BULKUPATES")).ToObject<List<AM_WORK_ORDER_BULKUPATES>>();

                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);

                var cloneListReturned = boAIMS.UpdateWOBuilk(builklist, updateAction, userSession.STAFF_ID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }

        [HttpPost]
        public HttpResponseMessage UpdateWorkDetail(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data   
                string modifyBy = postData["MODIFY_BY"].ToString();
                string modifyDate = postData["MODIFY_DATE"].ToString();
                AM_WORK_ORDERS woDetail = ((JObject)JsonConvert.DeserializeObject(postData["AM_WORK_ORDERS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_WORK_ORDERS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                boAIMS.UpdateWorkDetails(woDetail, modifyDate, modifyBy);


                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;

        }

        //Work Order Labour
        [HttpPost]
        public HttpResponseMessage CreateNewWOLabour(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_WORKORDER_LABOUR woDetails = ((JObject)JsonConvert.DeserializeObject(postData["AM_WORKORDER_LABOUR"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_WORKORDER_LABOUR>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var woturned = boAIMS.CreateNewWOLabour(woDetails);
                var searchAction = "";
                if (woturned.EMPLOYEE != null && woturned.EMPLOYEE != "")
                {
                    searchAction = woturned.EMPLOYEE;
                }
                else if (woturned.CONTRACT_NO != null && woturned.CONTRACT_NO != "")
                {
                    searchAction = woturned.CONTRACT_NO;
                }
                else if (woturned.SUPPLIER_ID != null && woturned.SUPPLIER_ID != "")
                {
                    searchAction = woturned.SUPPLIER_ID;
                }
                var woRefInfo = boAIMS.SearchOTHById(searchAction);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_WORKORDER_LABOUR", woturned },{ "AM_WORKORDER_LABOUR_OTH", woRefInfo } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadWOLabour(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string reqID = postData["REF_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var woLabourReturned = boAIMS.ReadWOLabour(reqID);
               
                var staffId = "";
                var supplierId = "";
                var contractId = "";
               

                if (woLabourReturned.EMPLOYEE !=null && woLabourReturned.EMPLOYEE != "")
                {
                    staffId = woLabourReturned.EMPLOYEE;
                }
                if (woLabourReturned.CONTRACT_NO != null && woLabourReturned.CONTRACT_NO != "")
                {
                    contractId = woLabourReturned.CONTRACT_NO;
                }
                if (woLabourReturned.SUPPLIER_ID != null && woLabourReturned.SUPPLIER_ID != "")
                {
                    supplierId = woLabourReturned.SUPPLIER_ID;
                }
                var woRefSupplierInfo = boAIMS.SearchOTHById(supplierId);
                var woRefStaffInfo = boAIMS.SearchOTHById(staffId);
                var woRefContractInfo = boAIMS.SearchOTHById(contractId);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_WORKORDER_LABOUR", woLabourReturned }, { "AM_WORKORDER_LABOUR_OTH", woRefContractInfo }, { "AM_WORKORDER_SUPPLIER", woRefSupplierInfo }, { "AM_WORKORDER_EMPLOYEE", woRefStaffInfo } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateWOLabour(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_WORKORDER_LABOUR woLabourDetail = ((JObject)JsonConvert.DeserializeObject(postData["AM_WORKORDER_LABOUR"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_WORKORDER_LABOUR>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                boAIMS.UpdateWOLabour(woLabourDetail);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateWOLabourDetails(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_WORKORDER_LABOUR woLabourDetail = ((JObject)JsonConvert.DeserializeObject(postData["AM_WORKORDER_LABOUR"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_WORKORDER_LABOUR>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                boAIMS.UpdateWOLabour(woLabourDetail);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESSES", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchWOLabour(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAims = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string reqNo = postData["REQNO"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAims = new AIMS(boSecurity.GetSharedDBInstance);
                var pmScheduleList = boAims.SearchWOLabour(queryString, reqNo);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_WORKORDER_LABOUR", pmScheduleList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchWOLabourLOV(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAims = null;
            Security boSecurity = null;
            Setup boSetup = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string reqNo = postData["REQNO"].ToString();
                string queryLOV = postData["LOV"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAims = new AIMS(boSecurity.GetSharedDBInstance);
                var pmScheduleList = boAims.SearchWOLabourLOV(queryString, reqNo);
                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovList = boSetup.getLovList(queryLOV);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_WORKORDER_LABOUR", pmScheduleList } , { "LOV_LOOKUPS", lovList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        public HttpResponseMessage SearchWOLRefType(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAims = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAims = new AIMS(boSecurity.GetSharedDBInstance);
                var wolRefTypeList = boAims.SearchRefType(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "WOL_LOOKUP_V", wolRefTypeList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        //Search List of Value
        [HttpPost]
        public HttpResponseMessage SearchAimsLOV(JObject postData)
        {
            HttpResponseMessage ret;

            Setup boSetup = null;
            AIMS boAIMS = null;
            Security boSecurity = null;

            try
            {

                string queryLOV = postData["LOV"].ToString();
                string querySearch = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovList = boSetup.getLovList(queryLOV);

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var manufacturerList = boAIMS.SearchManufacturerLookUp(querySearch);
                var supplierList = boAIMS.SearchSupplierLookup(querySearch);
				var rfsList = boAIMS.SearchRiskFactorSetup("", "");


				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "LOV_LOOKUPS", lovList }, { "AM_MANUFACTURER", manufacturerList } , { "AM_SUPPLIER", supplierList }, { "RISK_CATEGORYFACTOR", rfsList } });
			}
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
		 public HttpResponseMessage AimsLOVWOReport(JObject postData)
        {
            HttpResponseMessage ret;

            Setup boSetup = null;
            AIMS boAIMS = null;
            Security boSecurity = null;

            try
            {
                string queryLOV = postData["LOV"].ToString();
                string querySearch = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovList = boSetup.getLovList(queryLOV);

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var supplierList = boAIMS.SearchSupplierLookup(querySearch);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "LOV_LOOKUPS", lovList }, { "AM_SUPPLIER", supplierList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchAimsLOVInitialize(JObject postData)
        {
            HttpResponseMessage ret;

            Setup boSetup = null;
            AIMS boAIMS = null;
            Security boSecurity = null;

            try
            {

                string queryLOV = postData["LOV"].ToString();
                string querySearch = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovList = boSetup.getLovList(queryLOV);

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var rfsList = boAIMS.SearchRiskFactorSetup("", "");


                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "LOV_LOOKUPS", lovList }, { "RISK_CATEGORYFACTOR", rfsList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchAimsLOVQuery(JObject postData)
        {
            HttpResponseMessage ret;

            Setup boSetup = null;
            AIMS boAIMS = null;
            Security boSecurity = null;

            try
            {

                string queryLOV = postData["LOV"].ToString();
                string querySearch = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovList = boSetup.GetLovListQuery(querySearch,queryLOV);

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var manufacturerList = boAIMS.SearchManufacturerLookUp(querySearch);
                var supplierList = boAIMS.SearchSupplierLookup(querySearch);
                var rfsList = boAIMS.SearchRiskFactorSetup("", "");


                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "LOV_LOOKUPS", lovList }, { "AM_MANUFACTURER", manufacturerList }, { "AM_SUPPLIER", supplierList }, { "RISK_CATEGORYFACTOR", rfsList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchAimsLOVCostCenter(JObject postData)
        {
            HttpResponseMessage ret;

            Setup boSetup = null;
            AIMS boAIMS = null;
            Security boSecurity = null;

            try
            {

                string queryLOV = postData["LOV"].ToString();
                string querySearch = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovList = boSetup.GetLovListQuery(querySearch, queryLOV);
                

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "LOV_LOOKUPSCOSTCENTER", lovList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        //Asset Images
        [HttpPost]
        public HttpResponseMessage CreateAssetImages(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAims = null;
            Security boSecurity = null;
            try
            {
                //gather data        

                var assetImages = ((JArray)postData.SelectToken("SCANDOCS")).ToObject<List<SCANDOCS>>();

                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAims = new AIMS(boSecurity.GetSharedDBInstance);
                var assetImagesReturned = boAims.CreateAssetImages(assetImages);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SCANDOCS", assetImagesReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage CreateAssetPMImages(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAims = null;
            Security boSecurity = null;
            try
            {
                //gather data        
                
                var infoDet = ((JObject)JsonConvert.DeserializeObject(postData["MODIFYINFO"].ToString())).ToObject<MODIFYINFO>();
               
                SCANDOCS woDetails = ((JObject)JsonConvert.DeserializeObject(postData["SCANDOCS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<SCANDOCS>();

                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAims = new AIMS(boSecurity.GetSharedDBInstance);
                var assetImagesReturned = boAims.CreateAssetPMImages(woDetails, infoDet);
                boAims = new AIMS(boSecurity.GetSharedDBInstance);
                var scandocsCategoryImagesLists = boAims.SearchScandocsByCategoryId(infoDet.refid, "ASSETPM");

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SCANDOCS", assetImagesReturned } , { "SCANDOCS_CATEGORYIMAGELIST", scandocsCategoryImagesLists } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage DeleteImage(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string scandocID = postData["SCANDOC_ID"].ToString();
                string queryString = postData["REF_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();
                
                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var deleteReturned = boAIMS.DeleteAssetImage(scandocID);
                var scandocsCategoryImagesLists = boAIMS.SearchScandocsByAssetId(queryString);


                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SCANDOCS_CATEGORYIMAGELIST", scandocsCategoryImagesLists } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchAssetImageId(JObject postData)
        {
            HttpResponseMessage ret;


            Security boSecurity = null;
            AIMS boScandocs = null;
            try
            {
                string queryString = postData["REF_ID"].ToString();
                string queryScanDocId = postData["SCANDOC_ID"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");


                boScandocs = new AIMS(boSecurity.GetSharedDBInstance);
                var scandocsImages = boScandocs.SearchScandocsByAssetImageID(queryString, queryScanDocId);


                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SCANDOCS", scandocsImages } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchAssetId(JObject postData)
        {
            HttpResponseMessage ret;


            Security boSecurity = null;
            AIMS boScandocs = null;
            try
            {
                string queryString = postData["REF_ID"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");


                boScandocs = new AIMS(boSecurity.GetSharedDBInstance);
                var scandocsCategoryImagesLists = boScandocs.SearchScandocsByAssetId(queryString);


                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SCANDOCS_CATEGORYIMAGELIST", scandocsCategoryImagesLists } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage CreateAssetWODocs(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAims = null;
            Security boSecurity = null;
            try
            {
                //gather data        

                var infoDet = ((JObject)JsonConvert.DeserializeObject(postData["MODIFYINFO"].ToString())).ToObject<MODIFYINFO>();

                SCANDOCS woDetails = ((JObject)JsonConvert.DeserializeObject(postData["SCANDOCS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<SCANDOCS>();

                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boAims = new AIMS(boSecurity.GetSharedDBInstance);
                var assetImagesReturned = boAims.CreateAssetWODocs(woDetails, infoDet);
                boAims = new AIMS(boSecurity.GetSharedDBInstance);
                var scandocsCategoryImagesLists = boAims.SearchScandocsByCategoryId(infoDet.refid, "ASSETWO");

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SCANDOCS", assetImagesReturned }, { "SCANDOCS_CATEGORYIMAGELIST", scandocsCategoryImagesLists } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        //Work Order Labour Materials
        public HttpResponseMessage SearchWOParts(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var searchFilter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var wolPartsList = boAIMS.SearchWorkOrderParts(queryString, searchFilter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "DISPATCH_WOPARTS_V", wolPartsList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchAssetV(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string searchKey = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var returnAssetV = boAIMS.SearchAssetV(searchKey);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "ASSET_V", returnAssetV } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage GetAssetVByReport(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Setup boSetup = null;
            Security boSecurity = null;
            Reports boReports = null;
            try
            {
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var sysReportId = postData["SYSREPORTID"].ToString();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                boReports = new Reports(boSecurity.GetSharedDBInstance);

                //var returnAssetV = boAIMS.GetAssetVByReport(sysReportId);
                var returnAssetV = boReports.SearchSystemTransaction("", filter);
                var reportDetails = boSetup.GetSysTransReportDocsById(sysReportId);
                var reportDocs = boSetup.GetSysTransReportDocsByReport(sysReportId);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "REPORT_DETAILS", reportDetails }, { "ASSET_V", returnAssetV }, { "REPORT_SCANDOCS", reportDocs } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage CheckDuplicateManufacturer(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();
                var manufacturerId = postData["manufacturerId"].ToString();
                var oldManufacturerId = postData["oldManufacturerId"].ToString();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);

                var manufacturer = boAIMS.CheckDuplicateManufacturer(manufacturerId, oldManufacturerId);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "manufacturer", manufacturer }, { "SUCCESS", true } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
    }
}