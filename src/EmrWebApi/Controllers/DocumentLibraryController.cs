﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.Web;

//Project Related
using EmrWebApi.BusinessObjects;
using EmrWebApi.Models;

namespace EmrWebApi.Controllers
{
    public class DocumentLibraryController : ApiController
    {

        [HttpPost]
        public HttpResponseMessage UploadDocument(JObject postData)
        {
            HttpResponseMessage ret;
            DocumentLibrary boDocumentLibrary = null;
            Security boSecurity = null;
            try
            {
                //gather data        

                var document = ((JArray)postData.SelectToken("DOCUMENTLIBRARY")).ToObject<List<DOCUMENTLIBRARY>>();
                var docNo = postData["SEQNO"].ToString();
                var manufacturerCode = postData["MANUFACTURERCODE"].ToString();
                var deviceCode = postData["DEVICECODE"].ToString();
                var modelCode = postData["MODELCODE"].ToString();
                var pathFile = postData["FOLDERFILE"].ToString();
                var docCategory = postData["DOCCATEGORY"].ToString();
                var searchKey = postData["SEARCHKEY"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boDocumentLibrary = new DocumentLibrary(boSecurity.GetSharedDBInstance);
                var documentReturned = boDocumentLibrary.Upload(document, userSession.USER_ID, docNo, docCategory, pathFile, searchKey, deviceCode, manufacturerCode, modelCode);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "DOCUMENTLIBRARY", documentReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        [HttpPost]
        public HttpResponseMessage AssignDocumentNo(JObject postData)
        {
            HttpResponseMessage ret;
            DocumentLibrary boDocumentLibrary = null;
            Security boSecurity = null;
            try
            {
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boDocumentLibrary = new DocumentLibrary(boSecurity.GetSharedDBInstance);
                var docLastNo = boDocumentLibrary.GetLastDocumentNo();

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SEQ_DOCUMENT", docLastNo } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchDocumentLibraryInit(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            DocumentLibrary boDocumentLibrary = null;
            Security boSecurity = null;
            try
            {
                string queryLOV = postData["LOV"].ToString();
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovList = boSetup.getLovList(queryLOV);

                boDocumentLibrary = new DocumentLibrary(boSecurity.GetSharedDBInstance);
                var docList = boDocumentLibrary.SearchDocumentLibrary(queryString, filter);

                //var deviceTypeList = boDocumentLibrary.GetAllDeviceType("");



                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "DOCUMENTLIBRARY_LIST", docList }, { "LOV_LOOKUPS", lovList }  });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        [HttpPost]
        public HttpResponseMessage GetDocumentLOV(JObject postData)
        {
            HttpResponseMessage ret;
            DocumentLibrary boDocumentLibrary = null;
            Security boSecurity = null;
            try
            {
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boDocumentLibrary = new DocumentLibrary(boSecurity.GetSharedDBInstance);
             
                var deviceTypeList = boDocumentLibrary.GetAllDeviceType("");
                var manufacturerList = boDocumentLibrary.GetAllManufacturer("");
                var modelList = boDocumentLibrary.GetAllAssetModel("");

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "DEVICE_TYPE", deviceTypeList }, { "MANUFACTURER", manufacturerList }, { "ASSETMODEL", modelList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        [HttpPost]
        public HttpResponseMessage SearchDocumentLibrary(JObject postData)
        {
            HttpResponseMessage ret;
            DocumentLibrary boDocumentLibrary = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boDocumentLibrary = new DocumentLibrary(boSecurity.GetSharedDBInstance);
                var docList = boDocumentLibrary.SearchDocumentLibrary(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "DOCUMENTLIBRARY_LIST", docList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        [HttpPost]
        public HttpResponseMessage SearchDocumentLibraryByDocNo(JObject postData)
        {
            HttpResponseMessage ret;
            DocumentLibrary boDocumentLibrary = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["DOCNO"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boDocumentLibrary = new DocumentLibrary(boSecurity.GetSharedDBInstance);
                var docList = boDocumentLibrary.SearchDocumentLibraryDocNo(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "DOCUMENT_LIST", docList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage DeleteFile(JObject postData)
        {
            HttpResponseMessage ret;
            DocumentLibrary boDocumentLibrary = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                string docId = postData["DOCID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boDocumentLibrary = new DocumentLibrary(boSecurity.GetSharedDBInstance);
                boDocumentLibrary.DeleteFileFromLibrary(docId, userSession.USER_ID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESSDELETED", "The record was successfully Deleted." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }


        [HttpPost]
        public HttpResponseMessage SearchDocumentLibraryCriteria(JObject postData)
        {
            HttpResponseMessage ret;
            DocumentLibrary boDocumentLibrary = null;
            Security boSecurity = null;
            try
            {
                string deviceString = postData["DEVICETYPECODE"].ToString();
                string manufacturerString = postData["MANUFACTURERCODE"].ToString();
                string modelString = postData["MODELCODE"].ToString();
                string doccategoryString = postData["DOCCATEGORY"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boDocumentLibrary = new DocumentLibrary(boSecurity.GetSharedDBInstance);
                var docList = boDocumentLibrary.SearchDocumentLibraryCriteria(deviceString, manufacturerString,modelString, doccategoryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "DOCUMENTFILE_LIST", docList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

    }
}
