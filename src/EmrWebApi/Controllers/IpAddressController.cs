﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;

//Project Related
using EmrWebApi.BusinessObjects;
using EmrWebApi.Models;

namespace EmrWebApi.Controllers
{
	public class IpAddressController : ApiController
	{


		[HttpPost]
		public HttpResponseMessage getIpAddress()
		{
			HttpResponseMessage ret;


			try
			{
				String hostName = Dns.GetHostName();
				IPAddress[] ipaddress = Dns.GetHostAddresses(hostName);
				var clientIpAddress = "";
				var clientIEIpAddress = "";
				if (ipaddress.Count() >= 4)
				{
					clientIpAddress = ipaddress[2].ToString();
					clientIEIpAddress = ipaddress[4].ToString();

				}

				var clientInfoMachine = hostName + '@' + clientIpAddress + '@' + clientIEIpAddress;
				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "CLIENTIPHOST", clientInfoMachine }});
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				//if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}


	}
}
