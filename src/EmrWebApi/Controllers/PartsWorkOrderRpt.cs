﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;
using EmrWebApi.BusinessObjects;
using EmrWebApi.Models;

namespace EmrWebApi.Controllers
{
    public class PartsWorkOrderRptController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage PartsSearchAsset(JObject postData)
        {
            HttpResponseMessage ret;
            PartsWorkOrderRpt boTemplateReport = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();

                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boTemplateReport = new PartsWorkOrderRpt(boSecurity.GetSharedDBInstance);
                var reportList = boTemplateReport.ParetsSearchByAsset(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_ASSETLIST", reportList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage PartsSearchWorkOrder(JObject postData)
        {
            HttpResponseMessage ret;
            PartsWorkOrderRpt boTemplateReport = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();

                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boTemplateReport = new PartsWorkOrderRpt(boSecurity.GetSharedDBInstance);
                var reportList = boTemplateReport.PartsSearchByWorkOrder(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_WorkOrdLIST", reportList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
    }
}
