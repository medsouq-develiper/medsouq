﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;
using EmrWebApi.BusinessObjects;
using EmrWebApi.Models;

namespace EmrWebApi.Controllers
{
    public class StockOnhandRptController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage SearchStockOnHandRpt(JObject postData)
        {
            HttpResponseMessage ret;
            StockOnHandRpt boReport = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();

                //var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boReport = new StockOnHandRpt(boSecurity.GetSharedDBInstance);
                var reportList = boReport.ExampleStockOnHandRpt(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_StockOnHand", reportList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
    }
}
