﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;

//Project Related
using EmrWebApi.BusinessObjects;
using EmrWebApi.Models;

namespace EmrWebApi.Controllers
{
    public class SetupController : ApiController
    {

        public object boClinicsessions { get; private set; }

        //  System Transaction Report
        [HttpPost]
        public HttpResponseMessage CreateSysTransReport(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                SYSTEMTRANSACTION_REPORT reportDetails = ((JObject)JsonConvert.DeserializeObject(postData["SYSTEMTRANSACTION_REPORT"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<SYSTEMTRANSACTION_REPORT>();
                var systemTransactionReportScandocs = postData["SYSTEMTRANSACTION_REPORT_SCANDOCS"] != null ? ((JArray)postData.SelectToken("SYSTEMTRANSACTION_REPORT_SCANDOCS")).ToObject<List<SYSTEMTRANSACTION_REPORT_SCANDOC>>() : new List<SYSTEMTRANSACTION_REPORT_SCANDOC>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var returnReportDetails = boSetup.CreateSysTransReport(reportDetails, systemTransactionReportScandocs, userSession.USER_ID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "REPORT_DETAILS", returnReportDetails }, { "REPORT_SCANDOCS", systemTransactionReportScandocs } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadSysTransReport(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                string sysTransID = postData["SYSTRANSID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var reportDetails = boSetup.ReadSysTransReport(sysTransID);
                var reportDocs = boSetup.ReadSysTransReportDocs(sysTransID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "REPORT_DETAILS", reportDetails }, { "REPORT_SCANDOCS", reportDocs } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateSysTransReport(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                SYSTEMTRANSACTION_REPORT reportDetails = ((JObject)JsonConvert.DeserializeObject(postData["SYSTEMTRANSACTION_REPORT"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<SYSTEMTRANSACTION_REPORT>();
                var systemTransactionReportScandocs = postData["SYSTEMTRANSACTION_REPORT_SCANDOCS"] != null ? ((JArray)postData.SelectToken("SYSTEMTRANSACTION_REPORT_SCANDOCS")).ToObject<List<SYSTEMTRANSACTION_REPORT_SCANDOC>>() : new List<SYSTEMTRANSACTION_REPORT_SCANDOC>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var returnReportDetails = boSetup.UpdateSysTransReport(reportDetails, systemTransactionReportScandocs, userSession.USER_ID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "REPORT_DETAILS", returnReportDetails }, { "REPORT_SCANDOCS",  systemTransactionReportScandocs } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }

        [HttpPost]
        public HttpResponseMessage UpdateSysTransReportProcessing(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                SYSTEMTRANSACTION_REPORT reportDetails = ((JObject)JsonConvert.DeserializeObject(postData["SYSTEMTRANSACTION_REPORT"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<SYSTEMTRANSACTION_REPORT>();
                var systemTransactionReportScandocs = postData["SYSTEMTRANSACTION_REPORT_SCANDOCS"] != null ? ((JArray)postData.SelectToken("SYSTEMTRANSACTION_REPORT_SCANDOCS")).ToObject<List<SYSTEMTRANSACTION_REPORT_SCANDOC>>() : new List<SYSTEMTRANSACTION_REPORT_SCANDOC>();
                var systemTransactionDetails = postData["SYSTEM_TRANSACTIONDETAILS"] != null ? ((JArray)postData.SelectToken("SYSTEM_TRANSACTIONDETAILS")).ToObject<List<SYSTEM_TRANSACTIONDETAILS>>() : new List<SYSTEM_TRANSACTIONDETAILS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var returnReportDetails = boSetup.UpdateSysTransReportProcessing(reportDetails, systemTransactionReportScandocs, systemTransactionDetails, userSession);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "REPORT_DETAILS", returnReportDetails }, { "REPORT_SCANDOCS",  systemTransactionReportScandocs } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }


        [HttpPost]
        //LOV Lookups Setup
        public HttpResponseMessage CreateLovLookup(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                LOV_LOOKUPS lovlookup = ((JObject)JsonConvert.DeserializeObject(postData["LOV_LOOKUPS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<LOV_LOOKUPS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovlookupReturned = boSetup.CreateLovLookup(lovlookup);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "LOV_LOOKUPS", lovlookupReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadLovLookup(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                string codeId = postData["LOV_LOOKUP_ID"].ToString();
                string lovlookupCategory = postData["CATEGORY"].ToString();

                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovlookup = boSetup.ReadLovLookup(codeId, lovlookupCategory);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "LOV_LOOKUPS", lovlookup } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateLovLookup(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                LOV_LOOKUPS lovlookup = ((JObject)JsonConvert.DeserializeObject(postData["LOV_LOOKUPS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<LOV_LOOKUPS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                boSetup.UpdateLovLookup(lovlookup);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]


        //ModuleAccessAttribute
        public HttpResponseMessage CreateModuleAccessAttribute(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data        
 
                var newMFunctionAccessList = ((JArray)postData.SelectToken("MODULE_ACCESS_ATTRIBUTE")).ToObject<List<MODULE_ACCESS_ATTRIBUTE>>();

                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var moduleaccessattributeReturned = boSetup.CreateModuleAccessAttribute(newMFunctionAccessList);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "MODULE_ACCESS_ATTRIBUTE", moduleaccessattributeReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        public HttpResponseMessage UpdateModuleAccessAttribute(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data        
                var oldMFunctionAccessList = ((JArray)postData.SelectToken("MODULE_ACCESS_ATTRIBUTE")).ToObject<List<MODULE_ACCESS_ATTRIBUTE>>();
                var newMFunctionAccessList = ((JArray)postData.SelectToken("MODULE_ACCESS_ATTRIBUTE_APPEND")).ToObject<List<MODULE_ACCESS_ATTRIBUTE>>();
                
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var moduleaccessattributeReturned = boSetup.UpdateModuleAccessAttribute(oldMFunctionAccessList, newMFunctionAccessList);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "MODULE_ACCESS_ATTRIBUTE", moduleaccessattributeReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
		[HttpPost]
		public HttpResponseMessage DeleteModuleAccessAttribute(JObject postData)
		{
			HttpResponseMessage ret;
			Setup boSetup = null;
			Security boSecurity = null;
			try
			{
				//gather data                
				string userGroupId = postData["GROUP_ID"].ToString();
				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "UPDATE");

				boSetup = new Setup(boSecurity.GetSharedDBInstance);
				boSetup.DeleteModuleFunctionAttribute(userGroupId);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}

			return ret;
		}
		[HttpPost]
        //Staff Privileges Setup
        public HttpResponseMessage CreateStaffPrivileges(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                STAFF_PRIVILEGES staffprivileges = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_PRIVILEGES"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STAFF_PRIVILEGES>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var staffprivilegesReturned = boSetup.CreateStaffPrivileges(staffprivileges);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF_PRIVILEGES", staffprivilegesReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadStaffPrivileges(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                string privilegeID = postData["PRIVILEGE_ID"].ToString();

                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var staffprivileges = boSetup.ReadStaffPrivileges(privilegeID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF_PRIVILEGES", staffprivileges } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateStaffPrivilegesDetails(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                STAFF_PRIVILEGES staffprivileges = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_PRIVILEGES"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STAFF_PRIVILEGES>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                boSetup.UpdateStaffPrivileges(staffprivileges);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        
        
        //MOdule Function Setup
        public HttpResponseMessage CreateModuleFunction(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                MODULE_FUNCTIONS modulefunction = ((JObject)JsonConvert.DeserializeObject(postData["MODULE_FUNCTIONS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<MODULE_FUNCTIONS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var modulefunctionReturned = boSetup.CreateModuleFunction(modulefunction);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "MODULE_FUNCTIONS", modulefunctionReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadModuleFunction(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                string modulefunctionID = postData["MODULE_FUNCTION_ID"].ToString();
                
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var modulefunction = boSetup.ReadModuleFunction(modulefunctionID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "MODULE_FUNCTIONS", modulefunction } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateModuleFunctionDetails(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                MODULE_FUNCTIONS modulefunction = ((JObject)JsonConvert.DeserializeObject(postData["MODULE_FUNCTIONS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<MODULE_FUNCTIONS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                boSetup.UpdateModuleFunction(modulefunction);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage GetModulesLookUpTables(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var modulesList = boSetup.GetModuleList();

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "MODULES", modulesList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        
        //Locations Setup
        public HttpResponseMessage CreateLocation(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                LOCATIONS location = ((JObject)JsonConvert.DeserializeObject(postData["LOCATIONS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<LOCATIONS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var locationReturned = boSetup.CreateLocation(location);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "LOCATIONS", locationReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadLocation(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                string locationID = postData["LOCATION_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var location = boSetup.ReadLocation(locationID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "LOCATIONS", location } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage UpdateLocationDetails(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                LOCATIONS location = ((JObject)JsonConvert.DeserializeObject(postData["LOCATIONS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<LOCATIONS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                boSetup.UpdateLocation(location);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }



        //CostCenter Setup
        public HttpResponseMessage CreateCostCenter(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                COSTCENTER_DETAILS costcenter = ((JObject)JsonConvert.DeserializeObject(postData["COSTCENTER_DETAILS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<COSTCENTER_DETAILS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                var CLIENTOTHCONTACTS = new List<CLIENTOTHCONTACT>();
                if (postData["CLIENTOTHCONTACT"] != null)
                {
                    CLIENTOTHCONTACTS = ((JArray)postData.SelectToken("CLIENTOTHCONTACT")).ToObject<List<CLIENTOTHCONTACT>>();
                }

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var costcenterReturned = boSetup.CreateCostCenter(costcenter, CLIENTOTHCONTACTS, userSession.USER_ID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "COSTCENTER_DETAILS", costcenterReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadCostCenter(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                string codeID = postData["CODEID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var costcenter = boSetup.ReadCostCenter(codeID);
                var CLIENTOTHCONTACTS = boSetup.ReadCostCenterClientOthContacts(codeID, "CC");
                var store = boSetup.ReadCostCenterStore(codeID);
                var client = boSetup.ReadCostCenterClient(codeID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "COSTCENTER_DETAILS", costcenter  },{ "CLIENTOTHCONTACTS", CLIENTOTHCONTACTS },{ "STORES", store },{ "CLIENT", client } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateCostCenter(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Inventory boInventory = null;
            Security boSecurity = null;

            try
            {
                //gather data                
                COSTCENTER_DETAILS costcenter = ((JObject)JsonConvert.DeserializeObject(postData["COSTCENTER_DETAILS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<COSTCENTER_DETAILS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                var CLIENTOTHCONTACTS = new List<CLIENTOTHCONTACT>();
                if (postData["CLIENTOTHCONTACT"] != null)
                {
                    CLIENTOTHCONTACTS = ((JArray)postData.SelectToken("CLIENTOTHCONTACT")).ToObject<List<CLIENTOTHCONTACT>>();
                }

                var costcenterList = new List<AM_CLIENT_COSTCENTER>();
                if (postData["AM_CLIENT_COSTCENTER"] != null)
                {
                    costcenterList = ((JArray)postData.SelectToken("AM_CLIENT_COSTCENTER")).ToObject<List<AM_CLIENT_COSTCENTER>>();
                }

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                boSetup.UpdateCostCenter(costcenter, CLIENTOTHCONTACTS, userSession.USER_ID);

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                if (postData["StoreDetails"] != null)
                {
                    STORES store = ((JObject)JsonConvert.DeserializeObject(postData["StoreDetails"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STORES>();
                    if (store != null)
                    {
                        boInventory.CreateNewStore(store, userSession.USER_ID);
                    }
                }

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchCostCenter(JObject postData)
        {
            HttpResponseMessage ret;

            Setup boSetup = null;

            Security boSecurity = null;

            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var costcenterList = boSetup.SearchCostCenter(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "COSTCENTER_LIST", costcenterList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchCostCenterDetails(JObject postData)
        {
            HttpResponseMessage ret;

            Setup boSetup = null;

            Security boSecurity = null;

            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var costcenterList = boSetup.SearchCostCenterDetails(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "COSTCENTER_LIST", costcenterList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        //Purchase Billing Address
        [HttpPost]
		public HttpResponseMessage CreatePOBillAddress(JObject postData)
		{
			HttpResponseMessage ret;
			Setup boSetup = null;
			Security boSecurity = null;
			try
			{
				//gather data                
				AM_PURCHASE_BILLADDRESS infoData = ((JObject)JsonConvert.DeserializeObject(postData["AM_PURCHASE_BILLADDRESS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_PURCHASE_BILLADDRESS>();
				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "UPDATE");

				boSetup = new Setup(boSecurity.GetSharedDBInstance);
				var infoReturned = boSetup.CreatebillAddress(infoData);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASE_BILLADDRESS", infoReturned } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage ReadPOBillAddress(JObject postData)
		{
			HttpResponseMessage ret;
			Setup boSetup = null;
			Security boSecurity = null;
			try
			{
				string infoID = postData["PO_BILLCODE"].ToString();
				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boSetup = new Setup(boSecurity.GetSharedDBInstance);
				var infoReturned = boSetup.ReadbillAddress(infoID);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASE_BILLADDRESS", infoReturned } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		[HttpPost]
		public HttpResponseMessage UpdatePOBillAddress(JObject postData)
		{
			HttpResponseMessage ret;
			Setup boSetup = null;
			Security boSecurity = null;
			try
			{
				//gather data                
				AM_PURCHASE_BILLADDRESS infoData = ((JObject)JsonConvert.DeserializeObject(postData["AM_PURCHASE_BILLADDRESS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_PURCHASE_BILLADDRESS>();
				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "UPDATE");

				boSetup = new Setup(boSecurity.GetSharedDBInstance);
				boSetup.UpdatebillAddress(infoData);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}

			return ret;
		}

        //Client Info
        [HttpPost]
        public HttpResponseMessage CreateClient(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                AM_CLIENT_INFO clientinfo = ((JObject)JsonConvert.DeserializeObject(postData["AM_CLIENT_INFO"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_CLIENT_INFO>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                var CLIENTOTHCONTACTS = new List<CLIENTOTHCONTACT>();
                if (postData["CLIENTOTHCONTACT"] != null)
                {
                    CLIENTOTHCONTACTS = ((JArray)postData.SelectToken("CLIENTOTHCONTACT")).ToObject<List<CLIENTOTHCONTACT>>();
                }

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var clientinfoReturned = boSetup.CreateClientInfo(clientinfo, CLIENTOTHCONTACTS, userSession.USER_ID);
                var clientCostCenter = boSetup.SearchClientCostCenter(clientinfo.CLIENT_CODE);
                var clientLOV = boSetup.SearchClientCostCenterLOV();
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_CLIENT_INFO", clientinfoReturned }, { "AM_CLIENT_COSTCENTER", clientCostCenter }, { "CLIENT_LOV", clientLOV } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
		public HttpResponseMessage CreateUpdateClientInfo(JObject postData)
		{
			HttpResponseMessage ret;
			Setup boSetup = null;
			Security boSecurity = null;
            Inventory boInventory = null;
            try
			{
				//gather data                
				AM_CLIENT_INFO clientInfo = ((JObject)JsonConvert.DeserializeObject(postData["AM_CLIENT_INFO"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_CLIENT_INFO>();
				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                var CLIENTOTHCONTACTS = new List<CLIENTOTHCONTACT>();
                if (postData["CLIENTOTHCONTACT"] != null)
                {
                    CLIENTOTHCONTACTS = ((JArray)postData.SelectToken("CLIENTOTHCONTACT")).ToObject<List<CLIENTOTHCONTACT>>();
                }

                var costcenterList = new List<AM_CLIENT_COSTCENTER>();
                if (postData["AM_CLIENT_COSTCENTER"] != null)
                {
                    costcenterList = ((JArray)postData.SelectToken("AM_CLIENT_COSTCENTER")).ToObject<List<AM_CLIENT_COSTCENTER>>();
                }

                boSecurity = new Security();
                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                if (postData["StoreDetails"] != null)
                {
                    STORES store = ((JObject)JsonConvert.DeserializeObject(postData["StoreDetails"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STORES>();
                    if (store != null)
                    {
                        boInventory.CreateNewStore(store, userSession.USER_ID);
                    }
                }

				boSecurity.CheckSessionValidity(userSession, "UPDATE");
				boSetup = new Setup(boSecurity.GetSharedDBInstance);
				boSetup.UpdateClientInfo(clientInfo, costcenterList, CLIENTOTHCONTACTS, userSession.USER_ID);
                var clientCostCenter = boSetup.SearchClientCostCenter(clientInfo.CLIENT_CODE);
                var clientLOV = boSetup.SearchClientCostCenterLOV();
                var clientStore = boSetup.ReadCostCenterStore(clientInfo.CLIENT_CODE);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." }, { "AM_CLIENT_COSTCENTER", clientCostCenter }, { "CLIENT_LOV", clientLOV }, { "STORE", clientStore } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}

			return ret;
		}
		[HttpPost]
		public HttpResponseMessage ReadClientInfo(JObject postData)
		{
			HttpResponseMessage ret;
			Setup boSetup = null;
			Security boSecurity = null;
			try
			{
				string clientId = postData["CLIENTID"].ToString();
				STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boSetup = new Setup(boSecurity.GetSharedDBInstance);
				var clientInformation = boSetup.ReadClientInfo(clientId);
                var CLIENTOTHCONTACTS = boSetup.ReadCostCenterClientOthContacts(clientId, "C");
                var clientCostCenter = boSetup.SearchClientCostCenter(clientId); //am_client_costcenter tbl
                var clientLOV = boSetup.SearchClientCostCenterLOV(); //lov tbl
                var clientStore = boSetup.ReadCostCenterStore(clientId);
                var costCenterReturned = boSetup.SearchClientV(""); //costcenter_details tbl
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_CLIENT_INFO", clientInformation } , { "CLIENTOTHCONTACTS", CLIENTOTHCONTACTS }, { "AM_CLIENT_COSTCENTER", clientCostCenter }, { "CLIENT_LOV", clientLOV }, { "STORE", clientStore }, { "CLIENT_COSTCENTER", costCenterReturned } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}

        [HttpPost]
        public HttpResponseMessage SearchClient(JObject postData)
        {
            HttpResponseMessage ret;

            Setup boSetup = null;

            Security boSecurity = null;

            try
            {
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var clientList = boSetup.SearchClientInfo("");

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "CLIENT_LIST", clientList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        //Staff Positions Setup
        public HttpResponseMessage CreateStaffPosition(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                STAFF_POSITIONS staffposition = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_POSITIONS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STAFF_POSITIONS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var staffpositionReturned = boSetup.CreateStaffPosition(staffposition);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF_POSITIONS", staffpositionReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadStaffPosition(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                string staffpositionID = postData["STAFF_POSITION_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var staffposition = boSetup.ReadStaffPosition(staffpositionID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF_POSITIONS", staffposition } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage UpdateStaffPositionDetails(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                STAFF_POSITIONS staffposition = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_POSITIONS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STAFF_POSITIONS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                boSetup.UpdateStaffPosition(staffposition);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }

    

        //Staff Groups Setup
        public HttpResponseMessage CreateStaffGroup(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                STAFF_GROUPS staffgroup = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_GROUPS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STAFF_GROUPS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var staffgroupReturned = boSetup.CreateStaffGroup(staffgroup);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF_GROUPS", staffgroupReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        
        [HttpPost]
        public HttpResponseMessage ReadStaffGroup(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                string staffgroupID = postData["STAFF_GROUP_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var staffgroup = boSetup.ReadStaffGroup(staffgroupID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF_GROUPS", staffgroup } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage UpdateStaffGroupDetails(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                STAFF_GROUPS staffgroup = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_GROUPS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STAFF_GROUPS>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                boSetup.UpdateStaffGroup(staffgroup);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }

        //Services Setup
        public HttpResponseMessage CreateService(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boService = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                SERVICES service = ((JObject)JsonConvert.DeserializeObject(postData["SERVICES"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<SERVICES>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boService = new Setup(boSecurity.GetSharedDBInstance);
                var serviceReturned = boService.CreateService(service);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SERVICES", serviceReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadService(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boServices = null;
            Security boSecurity = null;
            try
            {
                string serviceID = postData["SERVICE_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boServices = new Setup(boSecurity.GetSharedDBInstance);
                var service = boServices.ReadService(serviceID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SERVICES", service } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage UpdateServiceDetails(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boServices = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                SERVICES service = ((JObject)JsonConvert.DeserializeObject(postData["SERVICES"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<SERVICES>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boServices = new Setup(boSecurity.GetSharedDBInstance);
                boServices.UpdateService(service);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }


        //Approving Officer
        [HttpPost]
        public HttpResponseMessage CreateApprovingOfficer(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boService = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                APPROVING_OFFICER infoDet = ((JObject)JsonConvert.DeserializeObject(postData["APPROVING_OFFICER"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<APPROVING_OFFICER>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boService = new Setup(boSecurity.GetSharedDBInstance);
                var infoDetReturned = boService.CreateApproveOfficer(infoDet);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "APPROVING_OFFICER", infoDetReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadApprovingOfficer(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boServices = null;
            Security boSecurity = null;
            try
            {
                string InfoID = postData["ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boServices = new Setup(boSecurity.GetSharedDBInstance);
                var infoDet = boServices.ReadApproveOfficer(InfoID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "APPROVING_OFFICER", infoDet } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateApprovingOfficer(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                APPROVING_OFFICER infoDet = ((JObject)JsonConvert.DeserializeObject(postData["APPROVING_OFFICER"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<APPROVING_OFFICER>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                boSetup.UpdateApproveOfficer(infoDet);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchApprovingOfficer(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Staff boStaff = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
   
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var approvingList = boSetup.SearchApproveOfficer(queryString);
                boStaff = new Staff(boSecurity.GetSharedDBInstance);
                var staffList = boStaff.SearchStaffUserActive(queryString);
                

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "APPROVING_OFFICER", approvingList }, { "STAFFLIST", staffList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }



        //Staff Service
        [HttpPost]
        public HttpResponseMessage GetStaffServicesLookUpTables(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var staffList = boSetup.GetStaffList();
                var servicesList = boSetup.GetServicesList();

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF", staffList }, { "SERVICES", servicesList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage CreateStaffService(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;

            try
            {
                //gather data                
                STAFF_SERVICES staffservice = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_SERVICES"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STAFF_SERVICES>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var clinicsessionReturned = boSetup.CreateStaffService(staffservice);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF_SERVICES", clinicsessionReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadStaffService(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                string staffserviceID = postData["STAFF_SERVICE_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var staffservice = boSetup.ReadStaffService(staffserviceID);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF_SERVICES", staffservice } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage UpdateStaffService(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;

            try
            {
                //gather data                
                STAFF_SERVICES staffservice = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_SERVICES"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STAFF_SERVICES>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                boSetup.UpdateStaffService(staffservice);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }

        //Staff Groups Setup
        public HttpResponseMessage insertSessionDR(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                STAFF_LOGON_SESSIONS staffgroup = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STAFF_LOGON_SESSIONS>();
                STAFF staffUser = ((JObject)JsonConvert.DeserializeObject(postData["STAFF"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STAFF>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                //boSecurity.CheckSessionValidity(userSession);

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var staffgroupReturned = boSetup.insertSessionDr(staffgroup, staffUser);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SESSIONOK", staffgroupReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

  
      
        [HttpPost]
        public HttpResponseMessage SearchLOV(JObject postData)
        {
            HttpResponseMessage ret;
            
            Setup boSetup = null;
           
            Security boSecurity = null;
           
            try
            {
               
                string queryLOV = postData["LOV"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");
         
                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovList = boSetup.getLovList(queryLOV);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() {{ "LOV_LOOKUPS", lovList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchInventoryLOV(JObject postData)
        {
            HttpResponseMessage ret;

            Setup boSetup = null;

            Security boSecurity = null;

            try
            {

                string queryLOV = postData["LOV"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovList = boSetup.getLovList(queryLOV);
                var servicesList = boSetup.GetServicesList();
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "LOV_LOOKUPS", lovList }, { "SERVICES", servicesList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        //GROUP PRIVILEGES
        [HttpPost]
        public HttpResponseMessage CreateNewGroupPrivileges(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data        

                var newList = ((JArray)postData.SelectToken("GROUP_PRIVILEGES")).ToObject<List<GROUP_PRIVILEGES>>();

                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();
                var groupId = postData["STAFF_GROUP_ID"].ToString();
                var createDate = postData["CREATE_DATE"].ToString();
                var actionStr = postData["ACTION"].ToString();
                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var newListReturned = boSetup.CreateNewGroupPrivileges(newList, userSession.STAFF_ID, createDate, actionStr);
                var groupPriviliges = boSetup.SearchGroupPrivileges(groupId);
                var priviligesList = boSetup.SearchPrivileges(groupId);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "GROUP_PRIVILEGES", newListReturned } , { "GROUP_PRIVILEGES_LIST", groupPriviliges } , { "PRIVILEGES_LIST", priviligesList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchPrivileges(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                //gather data        

    
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();
                var groupId = postData["STAFF_GROUP_ID"].ToString();
     
                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
      
                var groupPriviliges = boSetup.SearchGroupPrivileges(groupId);
                var priviligesList = boSetup.SearchPrivileges(groupId);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() {{ "GROUP_PRIVILEGES_LIST", groupPriviliges }, { "PRIVILEGES_LIST", priviligesList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        //Update notify
        [HttpPost]
        public HttpResponseMessage UpdateNotify(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Reports boReports = null;
            Security boSecurity = null;

            try
            {
                //gather data                
                string notifyId = postData["NOTIFY_ID"].ToString();
                string updateBy = postData["UPDATED_BY"].ToString();
                string updateDate = postData["UPDATED_DATE"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                boSetup.UpdateNotifyStatus(notifyId, updateBy, updateDate);
                boReports = new Reports(boSecurity.GetSharedDBInstance);
                var notifyReportList = boReports.SearchStaffNotification("", filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS NOTIFY UPDATE", "The record was successfully updated." }, { "NOTIFICATION_LIST", notifyReportList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }

        [HttpPost]
        public HttpResponseMessage GetAllCountry(JObject postData)
        {
            HttpResponseMessage ret;
            SystemObj boSystemObj = null;
            Security boSecurity = null;

            try
            {
                //gather data                
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSystemObj = new SystemObj(boSecurity.GetSharedDBInstance);
                var countries = boSystemObj.GetCountryList();

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "countries", countries } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }

        [HttpPost]
        public HttpResponseMessage GetSystemDefaultByCode(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;

            try
            {
                //gather data                
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();
                string code = postData["code"].ToString();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var systemDefault = boSetup.GetSystemDefaultByCode(code);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SYSTEM_DEFAULT", systemDefault } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
    }
}
