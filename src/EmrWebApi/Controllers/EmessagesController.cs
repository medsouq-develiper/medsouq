﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;


using EmrWebApi.BusinessObjects;
using EmrWebApi.Models;

namespace EmrWebApi.Controllers
{
    public class EmessagesController : ApiController
    {
        public HttpResponseMessage CreateHelpDesk(JObject postData)
        {
            HttpResponseMessage ret;
            Emessages boEmessages = null;
            Security boSecurity = null;
            AIMS boAIMS = null;
            Staff boStaffUser = null;
            NotifySender boNotifySender = null;
            try
            {
                //gather data                
                AM_HELPDESK helpdeskInfo = ((JObject)JsonConvert.DeserializeObject(postData["AM_HELPDESK"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_HELPDESK>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boEmessages = new Emessages(boSecurity.GetSharedDBInstance);
                var helpdeskInfoReturned = boEmessages.CreateNewHelpDesk(helpdeskInfo);

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var assetReturned = boAIMS.ReadAssetInfo(helpdeskInfoReturned.ASSET_NO);
                boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
                var staffuser = boStaffUser.ReadStaffUser(helpdeskInfoReturned.REQ_STAFF_ID);

                if (helpdeskInfoReturned.TICKET_ID.ToString() != null && helpdeskInfoReturned.TICKET_ID.ToString() != "")
                {
                    boNotifySender = new NotifySender(boSecurity.GetSharedDBInstance);
                    var notifyReturn = boNotifySender.UpdateNotify("10", "ALL", helpdeskInfoReturned.TICKET_ID.ToString(), "BSAIMS New Ticked No." + helpdeskInfoReturned.TICKET_ID.ToString() + " was created.", "Y", helpdeskInfoReturned.ISSUE_DATE);
                }

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFFINFO", staffuser }, { "AM_ASSET_DETAILS", assetReturned }, { "AM_HELPDESK", helpdeskInfoReturned } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage ReadHelpDesk(JObject postData)
        {
            HttpResponseMessage ret;
            Emessages boEmessages = null;
            Security boSecurity = null;
			AIMS boAIMS = null;
			Staff boStaffUser = null;
			try
            {
                string ticketID = postData["TICKET_ID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boEmessages = new Emessages(boSecurity.GetSharedDBInstance);
                var helpdeskInfo = boEmessages.ReadHelpDesk(ticketID);
				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var assetReturned = boAIMS.ReadAssetInfo(helpdeskInfo.ASSET_NO);
				boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
				var staffuser = boStaffUser.ReadStaffUser(helpdeskInfo.REQ_STAFF_ID);
				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFFINFO", staffuser },{ "AM_ASSET_DETAILS", assetReturned }, { "AM_HELPDESK", helpdeskInfo } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage UpdateHelpDesk(JObject postData)
        {
            HttpResponseMessage ret;
            Emessages boEmessages = null;
            Security boSecurity = null;
            NotifySender boNotifySender = null;
            try
            {
                //gather data                
                AM_HELPDESK helpdeskInfo = ((JObject)JsonConvert.DeserializeObject(postData["AM_HELPDESK"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<AM_HELPDESK>();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boEmessages = new Emessages(boSecurity.GetSharedDBInstance);
                boEmessages.UpdateHelpDesk(helpdeskInfo);

                
                if(helpdeskInfo.ASSIGN_TO != null && helpdeskInfo.ASSIGN_TO != "")
                {
                    boNotifySender = new NotifySender(boSecurity.GetSharedDBInstance);
                    var notifyReturn = boNotifySender.UpdateNotify("7", helpdeskInfo.ASSIGN_TO, helpdeskInfo.TICKET_ID.ToString(), "BSAIMS New Ticked No." + helpdeskInfo.TICKET_ID.ToString() + " was assigned.", "Y", helpdeskInfo.ASSIGN_BYDATE);
                }
                

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESS", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchHelpDesk(JObject postData)
        {
            HttpResponseMessage ret;
            Emessages boEmessages = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
				var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boEmessages = new Emessages(boSecurity.GetSharedDBInstance);
                var helpdeskList = boEmessages.SearchHelpDesk(queryString, filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_HELPDESK", helpdeskList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage UpdateHelpDeskAccept(JObject postData)
        {
            HttpResponseMessage ret;
            Emessages boEmessages = null;
        
            Security boSecurity = null;

            try
            {
                //gather data                
                string ticketId = postData["TICKET_ID"].ToString();
                string updateBy = postData["UPDATED_BY"].ToString();
                string updateDate = postData["UPDATED_DATE"].ToString();
      
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boEmessages = new Emessages(boSecurity.GetSharedDBInstance);
                boEmessages.UpdateHelpDeskAccepted(ticketId, updateBy, updateDate);
            

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SUCCESSNOTIFYUPDATE", "The record was successfully updated." } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }

            return ret;
        }
    }
}