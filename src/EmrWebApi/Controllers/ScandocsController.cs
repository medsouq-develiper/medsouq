﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;

//Project Related
using EmrWebApi.BusinessObjects;
using EmrWebApi.Models;

namespace EmrWebApi.Controllers
{
    public class ScandocsController : ApiController
    {
    
        public HttpResponseMessage SearchConsultImagesSketchTemp(JObject postData)
        {
            HttpResponseMessage ret;
           
            Scandocs boScandocs = null;
            Security boSecurity = null;

            try
            {
                string queryString = postData["PATIENT_ID"].ToString();
                string queryEventId = postData["EVENT_ID"].ToString();

                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");


                boScandocs = new Scandocs(boSecurity.GetSharedDBInstance);
                var imagesSketchList = boScandocs.SearchConsultImagesSketchTemp("");

                boScandocs = new Scandocs(boSecurity.GetSharedDBInstance);
                var scandocsCategoryList = boScandocs.SearchScandocsByPatientCategory(queryString);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "CONSULTATION_IMGSKETCHTEMP", imagesSketchList }, { "SCANDOCS_CATEGORY", scandocsCategoryList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
    }
}