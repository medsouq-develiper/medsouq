﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;

namespace EmrWebApi.Controllers
{
    public class JsonErrorClass
    {
        public string ErrorSource { get; set; }
        public string ErrorText { get; set; }
        public string ErrorExt { get; set; }
    }

    public class JsonErrorReturn
    {
        public static HttpResponseMessage GetHttpErrorResponse(HttpControllerContext controllerContext, Exception ex)
        {
            return controllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "ERROR", new JsonErrorClass() { ErrorSource = ex.Source, ErrorText = ex.Message } } });
        }
    }
}