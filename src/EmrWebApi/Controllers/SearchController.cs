﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;

//Project Related
using EmrWebApi.BusinessObjects;
using EmrWebApi.Models;


namespace EmrWebApi.Controllers
{
    public class SearchController : ApiController
    {
        public HttpResponseMessage InsertUserAction(JObject postData)
        {
            HttpResponseMessage ret;
            Search boSearch = null;
            Security boSecurity = null;
            try
            {
                //gather data                
                string screenId = postData["SCREENID"].ToString();
                string attributeId = postData["ATTRIBUTEID"].ToString();
                STAFF_LOGON_SESSIONS userSession = ((JObject)(postData.GetValue("STAFF_LOGON_SESSIONS"))).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "UPDATE");

                boSearch = new Search(boSecurity.GetSharedDBInstance);
                var screenReturned = boSearch.SaveActions(userSession.USER_ID, screenId);
                var tokenString = "SUBMENU";
                if (attributeId == "8657567")
                {
                    tokenString = boSearch.UpdateStaffToken(userSession.STAFF_ID);
                }
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "OKAY", "OKAY" }, { "TOKEN", tokenString } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        public HttpResponseMessage SearchStaffServices(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var staffservicesList = boSetup.SearchStaffServices(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF_SERVICES", staffservicesList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
     

        public HttpResponseMessage SearchStaffUser(JObject postData)
        {
            HttpResponseMessage ret;
            Staff boStaffUser = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
                var staffuserList = boStaffUser.SearchStaffUser(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF", staffuserList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        public HttpResponseMessage SearchStaffUserActive(JObject postData)
        {
            HttpResponseMessage ret;
            Staff boStaffUser = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
                var staffuserList = boStaffUser.SearchStaffUserActive(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF", staffuserList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        public HttpResponseMessage SearchServices(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var servicesList = boSetup.SearchServices(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "SERVICES", servicesList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        public HttpResponseMessage SearchPartsSOHN(JObject postData)
        {
            HttpResponseMessage ret;
            Inventory boInventory = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string storeId = postData["STORE"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boInventory = new Inventory(boSecurity.GetSharedDBInstance);
                var partsList = boInventory.SearchNStockOnHand(queryString, storeId);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PARTS_LIST", partsList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        public HttpResponseMessage SearchStaffGroups(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var staffgroupsList = boSetup.SearchStaffGroups(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF_GROUPS", staffgroupsList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        public HttpResponseMessage SearchStaffPositions(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var staffpositionsList = boSetup.SearchStaffPositions(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF_POSITIONS", staffpositionsList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        public HttpResponseMessage SearchLocations(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var locationsList = boSetup.SearchLocations(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "LOCATIONS", locationsList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

		public HttpResponseMessage SearchPOBillAddress(JObject postData)
		{
			HttpResponseMessage ret;
			Setup boSetup = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boSetup = new Setup(boSecurity.GetSharedDBInstance);
				var infoList = boSetup.SearchbillAddress(queryString);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASE_BILLADDRESS", infoList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		public HttpResponseMessage SearchPOBillAddressActData(JObject postData)
		{
			HttpResponseMessage ret;
			Setup boSetup = null;
			Security boSecurity = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");

				boSetup = new Setup(boSecurity.GetSharedDBInstance);
				var infoList = boSetup.SearchbillAddressActData(queryString);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_PURCHASE_BILLADDRESS", infoList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
		public HttpResponseMessage SearchStaffPrivileges(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var staffprivilegesList = boSetup.SearchStaffPrivileges(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFF_PRIVILEGES", staffprivilegesList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        public HttpResponseMessage SearchModuleFunctions(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var modulefunctionsList = boSetup.SearchModuleFunctions(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "MODULE_FUNCTIONS", modulefunctionsList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        public HttpResponseMessage SearchModuleAccessAttribute(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var moduleaccessattributeList = boSetup.SearchModuleAccessAttribute(queryString);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "MODULE_ACCESS_ATTRIBUTE", moduleaccessattributeList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        public HttpResponseMessage SearchMenuAccess(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            Reports boReport = null;
           
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string transqueryString = postData["SEARCHTRANS"].ToString();
                string querysysId = postData["SYSTEMID"].ToString();
                string queryString2 = postData["MENU_GROUP"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
               // boSecurity.CheckSessionValidity(userSession);

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var moduleaccessattributeList = boSetup.SearchMenuAccess(queryString, queryString2);
                var dashboardList = boSetup.SearchDashBoard(queryString);
               // var sud = boSetup.CreateWOPMDue();
                boReport = new Reports(boSecurity.GetSharedDBInstance);
                var notifyCountInfo = boReport.notifyCount(filter);

                var sysTransList = boReport.SearchSystemTransactionDashBoard(transqueryString, filter);


                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "DASHBOARD_V", dashboardList },{ "MODULE_ACCESS_ATTRIBUTE", moduleaccessattributeList }, { "NOTIFYCOUNT", notifyCountInfo }, { "SYSTEM_TRANSACTION", sysTransList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        public HttpResponseMessage UpdatePMSchedule(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();
                boSecurity = new Security();
                // boSecurity.CheckSessionValidity(userSession);
                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var recordsreturn = "";
                var sud = boSetup.CreateWOPMDue();
                //if(sud == -1)
                //{
                //    recordsreturn = "NORECORDS";
                //}
                //else
                //{
                //    recordsreturn = "MORE";
                //}
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PPMDONE", "PPM Creation Done" } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        public HttpResponseMessage SearchMenuAccessNoDashBoard(JObject postData)
		{
			HttpResponseMessage ret;
			Setup boSetup = null;
			Security boSecurity = null;

			try
			{
				string queryString = postData["SEARCH"].ToString();
				string querysysId = postData["SYSTEMID"].ToString();
				string queryString2 = postData["MENU_GROUP"].ToString();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				// boSecurity.CheckSessionValidity(userSession);

				boSetup = new Setup(boSecurity.GetSharedDBInstance);
				var moduleaccessattributeList = boSetup.SearchMenuAccess(queryString, queryString2);
				//var dashboardList = boSetup.SearchDashBoard(queryString);




				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "MODULE_ACCESS_ATTRIBUTE", moduleaccessattributeList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}

		public HttpResponseMessage SearchMenuAccessHDRDashBoard(JObject postData)
		{
			HttpResponseMessage ret;
			Setup boSetup = null;
			Security boSecurity = null;
			Emessages boEmessages = null;
			AIMS boAIMS = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				string querysysId = postData["SYSTEMID"].ToString();
				string queryString2 = postData["MENU_GROUP"].ToString();
				var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				// boSecurity.CheckSessionValidity(userSession);

				boSetup = new Setup(boSecurity.GetSharedDBInstance);
				var moduleaccessattributeList = boSetup.SearchMenuAccess(queryString, queryString2);
				boEmessages = new Emessages(boSecurity.GetSharedDBInstance);
				var helpdeskList = boEmessages.SearchHelpDeskCostCenter(queryString, filter);
                var RequesterPendingCount = boEmessages.HelpdeskRequesPendingByCostCenterCount(filter);
                var RequesterCompletedCount = boEmessages.HelpdeskRequesCompletedByCostCenterCount(filter);
                var RequesterCancelledCount = boEmessages.HelpdeskRequesCancelledByCostCenterCount(filter);

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var assetsCount = boAIMS.InServiceAssetCount(filter);
				//InServiceAssetCount


				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "ASSET_COUNT", assetsCount }, { "AM_HELPDESKLIST", helpdeskList }, { "MODULE_ACCESS_ATTRIBUTE", moduleaccessattributeList }, { "REQUESTERPENDING", RequesterPendingCount }, { "REQUESTERCOMPLETED", RequesterCompletedCount }, { "REQUESTERCANCELLED", RequesterCancelledCount } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}

        public HttpResponseMessage SearchMenuAccessCSTDashBoard(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            Emessages boEmessages = null;
            AIMS boAIMS = null;
            Reports boReport = null;
            try
            {
                string transqueryString = postData["SEARCHTRANS"].ToString();
                string queryString = postData["SEARCH"].ToString();
                string querysysId = postData["SYSTEMID"].ToString();
                string queryString2 = postData["MENU_GROUP"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                // boSecurity.CheckSessionValidity(userSession);

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var moduleaccessattributeList = boSetup.SearchMenuAccess(queryString, queryString2);
                boEmessages = new Emessages(boSecurity.GetSharedDBInstance);
                var helpdeskList = boEmessages.SearchHelpDeskCostCenter(queryString, filter);
                var RequesterPendingCount = boEmessages.HelpdeskRequesPendingByCostCenterCount(filter);
                var RequesterCompletedCount = boEmessages.HelpdeskRequesCompletedByCostCenterCount(filter);
                var RequesterCancelledCount = boEmessages.HelpdeskRequesCancelledByCostCenterCount(filter);

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var assetsCount = boAIMS.InServiceAssetCount(filter);
                //InServiceAssetCount
                boReport = new Reports(boSecurity.GetSharedDBInstance);
                var sysTransList = boReport.SearchSystemTransactionDashBoard(transqueryString, filter);
                var clientDashBoard = boReport.SearchDashBoardClient(filter);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "DASHBOARD_CLIENT", clientDashBoard }, { "ASSET_COUNT", assetsCount }, { "SYSTEM_TRANSACTION", sysTransList },  { "AM_HELPDESKLIST", helpdeskList }, { "MODULE_ACCESS_ATTRIBUTE", moduleaccessattributeList }, { "REQUESTERPENDING", RequesterPendingCount }, { "REQUESTERCOMPLETED", RequesterCompletedCount }, { "REQUESTERCANCELLED", RequesterCancelledCount } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        public HttpResponseMessage SearchMenuAccessENGRDashBoard(JObject postData)
		{
			HttpResponseMessage ret;
			Setup boSetup = null;
			Security boSecurity = null;
			Emessages boEmessages = null;
            Reports boReport = null;
            Search boSearch = null;
            AIMS boAIMS = null;
			try
			{
				string queryString = postData["SEARCH"].ToString();
				string querysysId = postData["SYSTEMID"].ToString();
				string queryString2 = postData["MENU_GROUP"].ToString();
				var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				// boSecurity.CheckSessionValidity(userSession);

				boSetup = new Setup(boSecurity.GetSharedDBInstance);
				var moduleaccessattributeList = boSetup.SearchMenuAccess(queryString, queryString2);
				boEmessages = new Emessages(boSecurity.GetSharedDBInstance);
				var helpdeskList = boEmessages.SearchHelpDeskENGR(queryString, filter);
				boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
				var assetsCount = boAIMS.InServiceAssetCount(filter);
				var worderList = boAIMS.SearchWorkOrder(queryString, null, filter, null, filter.staffId, userSession.USER_ID);
                boReport = new Reports(boSecurity.GetSharedDBInstance);
                var notifyCountInfo = boReport.notifyCount(filter);
                boSearch = new Search(boSecurity.GetSharedDBInstance);
                var woCount = boSearch.SearchEngrDashboardCount(filter.staffId);

                
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "WORK_ORDERLIST", worderList }, { "ASSET_COUNT", assetsCount }, { "AM_HELPDESKLIST", helpdeskList }, { "MODULE_ACCESS_ATTRIBUTE", moduleaccessattributeList }, { "NOTIFYCOUNT", notifyCountInfo }, { "WORKORDERCOUNT", woCount } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}

		public HttpResponseMessage SearchHelpDeskNew(JObject postData)
		{
			HttpResponseMessage ret;
			
			Security boSecurity = null;
			Emessages boEmessages = null;
			
			try
			{
				string queryString = postData["SEARCH"].ToString();
				string querysysId = postData["SYSTEMID"].ToString();
				string queryString2 = postData["MENU_GROUP"].ToString();
				var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				// boSecurity.CheckSessionValidity(userSession);

				
				boEmessages = new Emessages(boSecurity.GetSharedDBInstance);
				var helpdeskList = boEmessages.SearchHelpDeskENGR(queryString, filter);
				


				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_HELPDESKLIST", helpdeskList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}

		public HttpResponseMessage SearchLovLookups(JObject postData)
        {
            HttpResponseMessage ret;
            Setup boSetup = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string queryStringCategory = postData["CATEGORY"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovlookupsList = boSetup.SearchLovLookups(queryString, queryStringCategory);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "LOV_LOOKUPS", lovlookupsList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchModelManufacturer(JObject postData)
        {
            HttpResponseMessage ret;
            AIMS boAIMS = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();
                string manufacturerId = postData["MANUFACTURER_ID"].ToString();

                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");
                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var modelList = boAIMS.SearchModelManufacturer(queryString, manufacturerId);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_MANUFACTURER_MODEL", modelList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchListOfValueCategory(JObject postData)
        {
            HttpResponseMessage ret;
            
            Security boSecurity = null;
            Staff boStaffUser = null;
            Setup boSetup = null;
			
           
            try
            {               
                string queryLOV = postData["LOV"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");               
                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovList = boSetup.getLovList(queryLOV);
                boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
                var staffuserList = boStaffUser.SearchStaffLookUp("");
				
				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "LOV_LOOKUPS", lovList }, { "STAFFLIST", staffuserList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
		public HttpResponseMessage SearchListOfValueWO(JObject postData)
		{
			HttpResponseMessage ret;

			Security boSecurity = null;
			Staff boStaffUser = null;
			Setup boSetup = null;
			AIMS boAims = null;

			try
			{
				string queryLOV = postData["LOV"].ToString();
				var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

				boSecurity = new Security();
				boSecurity.CheckSessionValidity(userSession, "SEARCH");
				boSetup = new Setup(boSecurity.GetSharedDBInstance);
				var lovList = boSetup.getLovList(queryLOV);
				boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
				var staffuserList = boStaffUser.SearchStaffUser("");
                var staffuserWOList = boStaffUser.SearchStaffUserWO("");
                boAims = new AIMS(boSecurity.GetSharedDBInstance);
				var pmList = boAims.SearchProcedureActiveLookup("");
				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFFLISTWO", staffuserWOList }, { "LOV_LOOKUPS", lovList }, { "STAFFLIST", staffuserList }, { "AM_PROCEDURES", pmList } });
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}
			finally
			{
				if (boSecurity != null) boSecurity.Dispose();
			}
			return ret;
		}
        [HttpPost]
        public HttpResponseMessage SearchLOVWOReport(JObject postData)
        {
            HttpResponseMessage ret;

            Security boSecurity = null;
            AIMS boAIMS = null;
            Staff boStaffUser = null;
            Setup boSetup = null;

            try
            {
                string queryLOV = postData["LOV"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovList = boSetup.getLovList(queryLOV);

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var supplierList = boAIMS.SearchSupplierLookup("");

                boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
                var staffuserWOList = boStaffUser.SearchStaffUserWO("");

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFFLISTWO", staffuserWOList }, { "AM_SUPPLIER", supplierList }, { "LOV_LOOKUPS", lovList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        public HttpResponseMessage SearchLOVWorkOrder(JObject postData)
        {
            HttpResponseMessage ret;

            Security boSecurity = null;
            Staff boStaffUser = null;
            Setup boSetup = null;
            AIMS boAims = null;

            try
            {
                string queryLOV = postData["LOV"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");
                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovList = boSetup.getLovList(queryLOV);
                boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
                var staffuserList = boStaffUser.SearchStaffUser("");
                var staffuserWOList = boStaffUser.SearchStaffUserWO("");
                boAims = new AIMS(boSecurity.GetSharedDBInstance);
                var pmList = boAims.SearchProcedureActiveLookup("");
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() {{ "LOV_LOOKUPS", lovList }});
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        public HttpResponseMessage SearchAdvanceWOLov(JObject postData)
        {
            HttpResponseMessage ret;

            Security boSecurity = null;
            Staff boStaffUser = null;
            Setup boSetup = null;
            AIMS boAIMS = null;

            try
            {
               
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");
                boSetup = new Setup(boSecurity.GetSharedDBInstance);
              
                boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
               
                var staffuserWOList = boStaffUser.SearchStaffUserWO("");
                var lovList = boSetup.GetLovListQuery("", "'AIMS_COSTCENTER'");
                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var supplierList = boAIMS.SearchSupplierLookup("");
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "COST_CENTER_ADV", lovList }, { "STAFFLISTWO", staffuserWOList }, { "SUPPLIERLIST_ADV", supplierList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchListOfValueClone(JObject postData)
        {
            HttpResponseMessage ret;

            Security boSecurity = null;
            AIMS boAIMS = null;
            Setup boSetup = null;


            try
            {
                string queryLOV = postData["LOV"].ToString();
                string assetNo = postData["ASSET_NO"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");
                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovList = boSetup.getLovList(queryLOV);
                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var manufacturerList = boAIMS.SearchManufacturerLookUp("");
                var supplierList = boAIMS.SearchSupplierLookup("");
                var pmSchedule = boAIMS.readPMScheduleByAsset(assetNo);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "LOV_LOOKUPS", lovList }, { "AM_MANUFACTURER", manufacturerList }, { "AM_SUPPLIERLIST", supplierList }, { "CLONE", "CLONE" }, { "AM_ASSET_PMSCHEDULES", pmSchedule } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchListOfValue(JObject postData)
        {
            HttpResponseMessage ret;

            Security boSecurity = null;          
            Setup boSetup = null;
        

            try
            {
                string queryLOV = postData["LOV"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");
                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovList = boSetup.getLovList(queryLOV);
              
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "LOV_LOOKUPS", lovList }});
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage SearchHelpDeskLookUp(JObject postData)
        {
            HttpResponseMessage ret;
            Staff boStaffUser = null;
            Security boSecurity = null;
            try
            {
                string staffID = postData["STAFF_ID"].ToString();
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
                var staffuserList = boStaffUser.SearchStaffLookUp(queryString);
                var infoList = boStaffUser.SearchStaffCB("", "AIMS_COSTCENTER", staffID);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFFLIST", staffuserList }, { "STAFF_COSTCENTER_BUILDING", infoList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
        [HttpPost]
        public HttpResponseMessage SearchPMScheduleLookUp(JObject postData)
        {
            HttpResponseMessage ret;
            Staff boStaffUser = null;
            Security boSecurity = null;
            AIMS boAims = null;
            Setup boSetup = null;
            try
            {
                string staffID = postData["STAFF_ID"].ToString();
                string queryString = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovList = boSetup.getLovList(queryString);

                boAims = new AIMS(boSecurity.GetSharedDBInstance);
                var pmProcedureList = boAims.SearchProcedureActiveLookup("");

                boStaffUser = new Staff(boSecurity.GetSharedDBInstance);
                var staffuserList = boStaffUser.SearchStaffLookUp(queryString);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "STAFFLIST", staffuserList }, { "AM_PROCEDURES", pmProcedureList }, { "LOV_LOOKUPS", lovList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }


        public HttpResponseMessage SearchENGRWorkOrder(JObject postData)
        {
            HttpResponseMessage ret;
            Security boSecurity = null;
            AIMS boAIMS = null;
            try
            {
               
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                // boSecurity.CheckSessionValidity(userSession);

               
                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var worderList = boAIMS.SearchWorkOrder("", null, filter, null, filter.staffId,userSession.USER_ID);
               

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "WORK_ORDERLIST", worderList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

    }
}
