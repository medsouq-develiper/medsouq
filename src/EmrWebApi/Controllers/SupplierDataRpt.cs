﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;
using EmrWebApi.BusinessObjects;
using EmrWebApi.Models;

namespace EmrWebApi.Controllers
{
    public class SupplierDataRptController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage SearchNewLOVList(JObject postData)
        {
            HttpResponseMessage ret;

            Setup boSetup = null;
            AIMS boAIMS = null;
            Security boSecurity = null;

            try
            {

                string queryLOV = postData["LOV"].ToString();
                string queryLOVJ = postData["LOVJ"].ToString();

                //string querySearch = postData["SEARCH"].ToString();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");

                boSetup = new Setup(boSecurity.GetSharedDBInstance);
                var lovList = boSetup.getLovList(queryLOV);
                var LovListJ = boSetup.getLovList(queryLOVJ);

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var supplierList = boAIMS.SearchSupplierLookup("");

                boAIMS = new AIMS(boSecurity.GetSharedDBInstance);
                var MANUFACTURERList = boAIMS.SearchManufacturerLookUp("");

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "JobTypeLov", LovListJ }, { "MANUFACTURERLOV", MANUFACTURERList }, { "COSTCENTERLOV", lovList }, { "SUPPLIERLISTLOV", supplierList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }

        [HttpPost]
        public HttpResponseMessage ExampleSearchBySupplierRpt(JObject postData)
        {
            HttpResponseMessage ret;
            SupplierRpt boTemplateReport = null;
            Security boSecurity = null;
            try
            {
                string queryString = postData["SEARCH"].ToString();

                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
                var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

                boSecurity = new Security();
                boSecurity.CheckSessionValidity(userSession, "SEARCH");
                boTemplateReport = new SupplierRpt(boSecurity.GetSharedDBInstance);
                var reportList = boTemplateReport.ExampleSearchBySupplierRpt(queryString, filter);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "AM_SupplierLIST", reportList } });
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
        }
    }
}
