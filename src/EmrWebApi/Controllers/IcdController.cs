﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.Web.Script.Serialization;

//Project Related
using EmrWebApi.BusinessObjects;
using EmrWebApi.Models;

namespace EmrWebApi.Controllers
{
    public class IcdController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage SearchICD(string q)
        {
            HttpResponseMessage ret;       
            Icd boIcd = null;
            Security boSecurity = null;
            try
            {
                boSecurity = new Security();

                boIcd = new Icd(boSecurity.GetSharedDBInstance);
                var icdResult = boIcd.SearchIcd(q);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, icdResult);           

            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            finally
            {
                if (boSecurity != null) boSecurity.Dispose();
            }
            return ret;
    
        }

	}
}