﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.Threading.Tasks;
//Project Related
using EmrWebApi.BusinessObjects;
using EmrWebApi.Models;
using System.Web;




namespace EmrWebApi.Controllers
{
    public class SecurityController : ApiController
    {
      

        [HttpPost]
        public HttpResponseMessage AuthenticateUser(JObject postData)
        {
            HttpResponseMessage ret;
            try{
               


                var passkey = postData["MOREXDK"].ToString();
                STAFF staffDet = ((JObject)JsonConvert.DeserializeObject(postData["STAFF"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STAFF>();
                var session = (new LoginSecurity()).AuthenticateUser(staffDet, passkey);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, session);
              //  ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() {  { "Address", address }});

            }
            catch(Exception ex) {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }
            
            return ret;
        }

		[HttpPost]
		public HttpResponseMessage updateUserPassword(JObject postData)
		{
			HttpResponseMessage ret;
			try
			{
				var passkey = postData["MOREXDK"].ToString();
				STAFF staffDet = ((JObject)JsonConvert.DeserializeObject(postData["STAFF"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STAFF>();
				var session = (new LoginSecurity()).updateUser(staffDet, passkey);

				ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, session);
			}
			catch (Exception ex)
			{
				ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
			}

			return ret;
		}

        //public HttpResponseMessage UpdatePMSchedule(JObject postData)
        //{
        //    HttpResponseMessage ret;
        //    Setup boSetup = null;
        //    Security boSecurity = null;
        //    try
        //    {

        //        var userSession = ((JObject)JsonConvert.DeserializeObject(postData["STAFF_LOGON_SESSIONS"].ToString())).ToObject<STAFF_LOGON_SESSIONS>();

        //        boSecurity = new Security();
        //        // boSecurity.CheckSessionValidity(userSession);

        //        boSetup = new Setup(boSecurity.GetSharedDBInstance);
        //        var recordsreturn = "";
        //        var sud = boSetup.CreateWOPMDue();
        //        if (sud == -1)
        //        {
        //            recordsreturn = "NORECORDS";
        //        }
        //        else
        //        {
        //            recordsreturn = "MORE";
        //        }
        //        ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, new Dictionary<string, object>() { { "PM DONE", recordsreturn } });
        //    }
        //    catch (Exception ex)
        //    {
        //        ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
        //    }
        //    finally
        //    {
        //        if (boSecurity != null) boSecurity.Dispose();
        //    }
        //    return ret;
        //}

        [HttpPost]
        public HttpResponseMessage updateUserPasswordChange(JObject postData)
        {
            HttpResponseMessage ret;
            try
            {
                var passkey = postData["MOREXDK"].ToString();
                var filter = ((JObject)JsonConvert.DeserializeObject(postData["SEARCH_FILTER"].ToString())).ToObject<SearchFilter>();
               
                var session = (new LoginSecurity()).updateUserPasswordChange(filter, passkey);

                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, session);
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }

            return ret;
        }
        [HttpPost]
        public HttpResponseMessage insertSession(STAFF_LOGON_SESSIONS sessioncur, STAFF user)
        {
            HttpResponseMessage ret;
            try
            {
                var session = (new Security()).insertSessionStaffApi(sessioncur, user);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, session);
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }

            return ret;
        }

      

        [HttpPost]
        public HttpResponseMessage CreateMail(JObject postData)
        {
            HttpResponseMessage ret;
            try
            {
                STAFF staffDet = ((JObject)JsonConvert.DeserializeObject(postData["STAFF"].ToString(), WebApiConfig.JsonSerializationSettings)).ToObject<STAFF>();
                var session = (new Security()).emailSender(staffDet);
                ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, session);
            }
            catch (Exception ex)
            {
                ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
            }

            return ret;
        }

        //[HttpPost]
        //public HttpResponseMessage CreateSMS(JObject postData)
        //{
        //    HttpResponseMessage ret;
        //    try
        //    {
            
        //        var session = (new NotifySender()).smsSend("");
        //        ret = ControllerContext.Request.CreateResponse(HttpStatusCode.OK, session);
        //    }
        //    catch (Exception ex)
        //    {
        //        ret = JsonErrorReturn.GetHttpErrorResponse(ControllerContext, ex);
        //    }

        //    return ret;
      


    }
}
