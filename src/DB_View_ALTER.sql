/*Joanne -23012025 - Added ,ST.SERIAL_NO*/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER VIEW [dbo].[WORKORDERS_V]
AS
SELECT        (ROW_NUMBER() OVER (ORDER BY AM.REQ_NO DESC)) AS ROWID, AM.REQ_NO, AM.REQ_BY, AM.REQ_DATE, AM.PROBLEM, AM.WO_STATUS, AM.REQ_TYPE, AM.ASSET_NO, AM.JOB_TYPE, AM.ASSIGN_TYPE, 
AM.JOB_URGENCY, (CASE WHEN len(RTrim(LTrim(ISNULL(st.description, '')))) <= 0 THEN 'Undefined' ELSE st.description END) AS description, WS.AM_INSPECTION_ID, WS.INSPECTION_TYPE, AM.CREATED_BY, NULLIF (AM.ASSIGN_TO, 
'') AS ASSIGN_TO, ST.COST_CENTER, AM.CLOSED_DATE, AM.MATERIALS_REMARKS, AM.job_due_date, ST.BUILDING,
    (SELECT        Count(ap.id_parts)
      FROM            am_partsrequest ap
      WHERE        ap.ref_wo = am.req_no) AS SPAREPARTS, ST.PM_TYPE, ISNULL(AM.asset_costcenter, CC.DESCRIPTION) AS COSTCENTERNAME, (CASE WHEN len(RTrim(LTrim(ISNULL(LTrim(sp.description), '')))) 
<= 0 THEN 'N/A' ELSE LTrim(sp.description) END) AS SUPPIERNAME, IsNull(SF.first_name + ' ' + SF.last_name, '') AS STAFF_FULLNAME, ST.SERIAL_NO
FROM            AM_WORK_ORDERS AM LEFT JOIN
                         AM_ASSET_DETAILS ST ON ST.ASSET_NO = AM.ASSET_NO LEFT JOIN
                         AM_INSPECTION WS ON WS.AM_INSPECTION_ID = AM.AM_INSPECTION_ID LEFT OUTER JOIN
                         dbo.lov_lookups AS CC ON ST.cost_center = CC.LOV_LOOKUP_ID AND CC.CATEGORY = 'AIMS_COSTCENTER' LEFT OUTER JOIN
                         dbo.am_supplier AS SP ON ST.supplier = SP.supplier_id LEFT OUTER JOIN
                         dbo.staff AS SF ON AM.assign_to = SF.STAFF_ID
GO

IF NOT EXISTS (SELECT * FROM sys.fn_listextendedproperty(N'MS_DiagramPane1' , N'SCHEMA',N'dbo', N'VIEW',N'WORKORDERS_V', NULL,NULL))
	EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 20
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'WORKORDERS_V'
ELSE
BEGIN
	EXEC sys.sp_updateextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 20
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'WORKORDERS_V'
END
GO

IF NOT EXISTS (SELECT * FROM sys.fn_listextendedproperty(N'MS_DiagramPaneCount' , N'SCHEMA',N'dbo', N'VIEW',N'WORKORDERS_V', NULL,NULL))
	EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'WORKORDERS_V'
ELSE
BEGIN
	EXEC sys.sp_updateextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'WORKORDERS_V'
END
GO


//*****************LIVE_DEMO_UPDATE -- Joanne 05122004 (bstcomsa_mescs, bstcomsa_rabigh, bstcomsa_adham, bstcomsa_moh)*******************//
===================================
/*Joanne -04122024 - Added L.category = 'AIMS_COSTCENTER'*/

CREATE OR ALTER VIEW [dbo].[WorkOrderForReport_V]
AS
SELECT        A.asset_no, A.description, A.serial_no, A.model_no, W.req_no, W.req_date, JB.PROBLEM_TYPEDESC, CONVERT(varchar(16), W.req_date, 103) AS ReqDate, W.wo_status, W.req_type, W.job_type, S.description AS Supplier, 
                         M.description AS Manufacturer, St.USER_ID, W.closed_date, W.materials_remarks, W.job_due_date, CONVERT(varchar(16), W.job_due_date, 103) AS JobDueDate, L.DESCRIPTION AS CostCenter, A.model_name, A.pm_type, 
                         CONVERT(varchar(16), W.closed_date, 103) AS ClosedDate, Sp.description AS Current_Agent
FROM            dbo.am_asset_details AS A LEFT OUTER JOIN
                         dbo.am_work_orders AS W ON A.asset_no = W.asset_no LEFT OUTER JOIN
						 dbo.SupplierForRpt_V AS Sp ON A.Current_Agent COLLATE SQL_Latin1_General_CP1_CI_AS = Sp.supplier_id LEFT OUTER JOIN
                         dbo.lov_lookups AS L ON L.category = 'AIMS_COSTCENTER' and A.cost_center = L.LOV_LOOKUP_ID LEFT OUTER JOIN
                         dbo.WO_JOBPROBLEMTYPE_V AS JB ON W.req_no = JB.REQ_NO LEFT OUTER JOIN
                         dbo.am_supplier AS S ON A.supplier = S.supplier_id LEFT OUTER JOIN
                         dbo.am_manufacturer AS M ON A.manufacturer = M.manufacturer_id LEFT OUTER JOIN
                         dbo.staff AS St ON W.assign_to = St.STAFF_ID
WHERE        (W.req_no IS NOT NULL)
GO

================================
/*Joanne -19092024 - Added category_desc*/


/****** Object:  View [dbo].[ASSET_V]    Script Date: 19/09/2024 15:58:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE OR ALTER VIEW [dbo].[ASSET_V]
AS
SELECT        (ROW_NUMBER() OVER (ORDER BY AM.description ASC)) AS ROWID, (CASE WHEN len(RTrim(LTrim(ISNULL(AM.description, '')))) <= 0 THEN 'Undefined' ELSE AM.description END) AS description, AM.asset_no, AM.category, DT.description as category_desc,
AM.manufacturer, AM.serial_no, AM.model_no, AM.model_name, SB.DESCRIPTION AS building, AM.cost_center, AM.location, SC.DESCRIPTION AS condition, AM.responsible_center, AM.status, ST.DESCRIPTION AS RESPCENTER_DESC, 
(CASE WHEN len(RTrim(LTrim(ISNULL(CC.description, '')))) <= 0 THEN 'Undefined' ELSE LTrim(CC.description) END) AS COSTCENTER_DESC, AM.CLONE_ID, (CASE WHEN len(RTrim(LTrim(ISNULL(SP.description, '')))) 
<= 0 THEN 'Undefined' ELSE LTrim(SP.description) END) AS suppliername, LTrim(IsNull(MF.description, '')) AS manufacturername, AM.purchase_no, AM.group_code, AM.pm_type, AM.BUILDING AS BUILDING_CODE, AM.PURCHASE_COST, 
AM.costcentertransid, ATS.ReturnedReason, am.SysReportId, am.PRODUCT_CODE
FROM            dbo.am_asset_details AS AM LEFT OUTER JOIN
                         dbo.lov_lookups AS ST ON AM.responsible_center = ST.LOV_LOOKUP_ID AND ST.CATEGORY = 'AIMS_COSTCENTER' LEFT OUTER JOIN
                         dbo.lov_lookups AS SB ON AM.building = SB.LOV_LOOKUP_ID AND SB.CATEGORY = 'AIMS_BUILDING' LEFT OUTER JOIN
                         dbo.lov_lookups AS SC ON AM.condition = SC.LOV_LOOKUP_ID AND SC.CATEGORY = 'AIMS-CONDITION' LEFT OUTER JOIN
						 dbo.lov_lookups AS DT ON AM.category = DT.LOV_LOOKUP_ID AND DT.CATEGORY = 'AIMS_DTYPE' LEFT OUTER JOIN
                         dbo.CLIENT_COSTCENTER_V AS CC ON AM.cost_center = CC.CostCenter LEFT OUTER JOIN
                         dbo.am_supplier AS SP ON AM.supplier = SP.supplier_id LEFT OUTER JOIN
                         dbo.am_manufacturer AS MF ON AM.manufacturer = MF.manufacturer_id LEFT OUTER JOIN
                         dbo.am_asset_transaction AS ATS ON ATS.TransId = AM.costcentertransid
WHERE        (AM.asset_no IS NOT NULL)
GO

IF NOT EXISTS (SELECT * FROM sys.fn_listextendedproperty(N'MS_DiagramPane1' , N'SCHEMA',N'dbo', N'VIEW',N'ASSET_V', NULL,NULL))
	EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[29] 4[5] 2[32] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 28
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ASSET_V'
ELSE
BEGIN
	EXEC sys.sp_updateextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[29] 4[5] 2[32] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 28
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ASSET_V'
END
GO

IF NOT EXISTS (SELECT * FROM sys.fn_listextendedproperty(N'MS_DiagramPaneCount' , N'SCHEMA',N'dbo', N'VIEW',N'ASSET_V', NULL,NULL))
	EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ASSET_V'
ELSE
BEGIN
	EXEC sys.sp_updateextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ASSET_V'
END
GO

================================
/*Joanne -02062024 - Added Condemned Assets*/

CREATE OR ALTER VIEW [dbo].[dashboard_v]
AS
SELECT        count_big(DISTINCT product_stockonhand.PRODUCT_CODE) AS PR_REQ, N'STOCK_ONHAND' AS RPT_TYPE
FROM            dbo.product_stockonhand
WHERE        (product_stockonhand.QTY <= 0) AND (STATUS = 'Y')
UNION
SELECT        count_big(DISTINCT am_work_orders.req_no) AS PR_REQ, N'WO_ONHOLD' AS RPT_TYPE
FROM            dbo.am_work_orders
WHERE        (am_work_orders.wo_status = 'HA')
				and format([created_date],'yyyy-MM-dd') >= '2024-04-01'
UNION
SELECT        count_big(DISTINCT am_work_orders.req_no) AS PR_REQ, N'WO_PENDING_PM' AS RPT_TYPE
FROM            dbo.am_work_orders
WHERE        (am_work_orders.wo_status IN ('OP', 'PS', 'NE')) AND REQ_TYPE = 'PM'
				and format([created_date],'yyyy-MM-dd') >= '2024-04-01'
UNION
SELECT        count_big(DISTINCT am_work_orders.req_no) AS PR_REQ, N'WO_PENDING_OTH' AS RPT_TYPE
FROM            dbo.am_work_orders
WHERE        (am_work_orders.wo_status IN ('OP', 'PS', 'NE')) AND REQ_TYPE IN ('CM')
			and format([created_date],'yyyy-MM-dd') >= '2024-04-01'
UNION
SELECT        count_big(DISTINCT am_work_orders.req_no) AS PR_REQ, N'WO_OVERDUE_PM' AS RPT_TYPE
FROM            dbo.am_work_orders
WHERE        (am_work_orders.wo_status IN ('OP', 'PS', 'NE')) AND job_due_date < (CONVERT(varchar(11), GETDATE(), 111)) + ' 23:00:00.000' AND (CONVERT(varchar(11), am_work_orders.job_due_date, 105) <> '01-01-1900') AND 
				REQ_TYPE = 'PM' 
				and format([created_date],'yyyy-MM-dd') >= '2024-04-01'
UNION
SELECT        count_big(DISTINCT am_work_orders.req_no) AS PR_REQ, N'WO_OVERDUE_OTH' AS RPT_TYPE
FROM            dbo.am_work_orders
WHERE        (am_work_orders.wo_status IN ('OP', 'PS', 'NE')) AND job_due_date < (CONVERT(varchar(11), GETDATE(), 111)) + ' 23:00:00.000' AND (CONVERT(varchar(11), am_work_orders.job_due_date, 105) <> '01-01-1900') AND 
                         REQ_TYPE = 'CM'
						 and format([created_date],'yyyy-MM-dd') >= '2024-04-01'
UNION
SELECT        count_big(DISTINCT am_asset_details.asset_no) AS PR_REQ, N'ASSET' AS RPT_TYPE
FROM            dbo.am_asset_details
WHERE        (am_asset_details.status = 'I')
UNION
SELECT        count_big(DISTINCT am_asset_pmschedules.am_asset_pmschedule_id) AS PR_REQ, N'PM_OVERDUE30' AS RPT_TYPE
FROM            dbo.am_asset_pmschedules
WHERE        am_asset_pmschedules.next_pm_due <= DateAdd(Day, 30, (CONVERT(varchar(11), GETDATE(), 111)) + ' 23:00:00.000') AND (CONVERT(varchar(11), am_asset_pmschedules.next_pm_due, 105) <> '01-01-1900') AND 
                         asset_no IN
                             (SELECT        asset_no
                               FROM            am_asset_details
                               WHERE        status = 'I')
UNION
SELECT        count_big(am_purchaserequest.request_id) AS PR_REQ, N'STOCK_EXPIRY30' AS RPT_TYPE
FROM            dbo.am_purchaserequest
WHERE        (am_purchaserequest.status = 'APPROVED')
			and format([created_date],'yyyy-MM-dd') >= '2024-04-01'
UNION
SELECT        count_big(am_purchaserequest.request_id) AS PR_REQ, N'PR_REQUEST' AS RPT_TYPE
FROM            dbo.am_purchaserequest
WHERE        (am_purchaserequest.status = 'REQUEST')
			and format([created_date],'yyyy-MM-dd') >= '2024-04-01'
UNION
SELECT        count_big(SysTransId) AS PR_REQ, N'TOTAL_RECALL' AS RPT_TYPE
FROM            system_transactiondetails
WHERE        REASONTYPE = 'RECALL'
UNION
SELECT        count_big(DISTINCT am_asset_details.asset_no) AS PR_REQ, N'ASSETONHAND' AS RPT_TYPE
FROM            dbo.am_asset_details
WHERE        (am_asset_details.status IN ( '1'))
UNION
SELECT        count_big(DISTINCT am_asset_details.asset_no) AS PR_REQ, N'RETIRED' AS RPT_TYPE
FROM            dbo.am_asset_details
WHERE        (am_asset_details.status = 'R')
				
GO