
/***Joanne 28112024***/

INSERT [dbo].[lov_lookups] ([LOV_LOOKUP_ID], [DESCRIPTION], [DESCRIPTION_SHORT], [DESCRIPTION_OTH], [CATEGORY], [ACTION], [STATUS], [LAST_UPDATED], [LAST_UPDATEDBY], [PUSH_FLAG], [WO_DURATION], [CREATEDBY], [CREATEDDATE]) 
VALUES (N'AIMS_CLASSTYPE', N'Asset Class Type', N'', N'', N'LOV', N'', N'', NULL, NULL, NULL, NULL, NULL, NULL)
GO

//*****************LIVE_DEMO_UPDATE -- Joanne 28112004 (bstcomsa_mescs, bstcomsa_rabigh, bstcomsa_adham, bstcomsa_moh)*******************//

//*Joanne 28112024**//

SET IDENTITY_INSERT [dbo].[am_notifysetup] ON 

INSERT [dbo].[am_notifysetup] ([id], [notify_id], [description], [sms_message], [sms_messageoth], [email_message], [email_messageoth], [sms_flag], [email_flag], [status], [created_by], [created_date], [dashboard_flag], [dashboard_message]) VALUES (10, 10, N'Notify Engineer for Ticket Created', N'Please be inform that new ticket No.<refNo> has been created.', NULL, N'Dear <lastname>,

Please be informed that new ticket No.<refNo> has been created.
', NULL, N'N', N'N', N'Y', N'ADMIN', CAST(N'2024-11-21T12:30:00.000' AS DateTime), N'Y', N'Please be informed that new ticket No.<refNo> has been created.')

SET IDENTITY_INSERT [dbo].[am_notifysetup] OFF
