﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EmrWebApp.controllers
{
    public class LanguangeController : Controller
    {
        // GET: Languange
        public ActionResult Index()
        {
            sessionManager.locale = "ar";
            return View();
        }
        [HttpPost]
        public JsonResult changeLanguange(string localize)
        {
            var actionStatus = "";
            try
            {
                var languange = sessionManager.locale;
                if(languange == "en")
                {
                    sessionManager.locale = "ar";
                }
                else
                {
                    sessionManager.locale = "en";
                }
             
                
                actionStatus = sessionManager.locale;
                return Json(actionStatus, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                actionStatus = "File upload failed!!";
                return Json(actionStatus, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult getLanguange()
        {
            var actionStatus = "";
            try
            {
               
                actionStatus = sessionManager.locale;
                return Json(actionStatus, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                actionStatus = "File upload failed!!";
                return Json(actionStatus, JsonRequestBehavior.AllowGet);
            }
        }
    }
}