﻿

using System;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace EmrWebApp.controllers
{
    public class UploadController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult documentExplore()
        {
            return View();
        }

        [HttpGet]
        public ActionResult UploadFile()
        {
            return View();
        }
        [HttpPost] 
        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            try
            {
                if (file.ContentLength > 0)
                {
                    string _FileName = Path.GetFileName(file.FileName);
                    string _path = Path.Combine(Server.MapPath("~/UploadedFiles"), _FileName);
                    file.SaveAs(_path);
                }
                ViewBag.Message = "File Uploaded Successfully!!";
                return View();
            }
            catch
            {
                ViewBag.Message = "File upload failed!!";
                return View();
            }
        }

        [HttpPost]
        public JsonResult UploadDocument(HttpPostedFileBase file)
        {
            var actionStatus = "";
            try
            {

                if (file.ContentLength > 0)
                {
                    string _FileName = Path.GetFileName(file.FileName);
                    string _path = Path.Combine(Server.MapPath("~/UploadedFiles"), _FileName);
                    file.SaveAs(_path);
                }
                actionStatus = "File Uploaded Successfully!!";
                return Json(actionStatus, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                actionStatus = "File upload failed!!";
                return Json(actionStatus, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Employee()
        {
            return View();
        }
        public ActionResult SaveDocument(string REF_ID, string docFilename, string pathLocation)
        {
            try
            {
                if (Request.Files.Count > 0)
                {
                    var root = pathLocation;// "~/UploadedFiles";
                    bool folderpath = System.IO.Directory.Exists(HttpContext.Server.MapPath(root));
                    if (!folderpath)
                    {
                        System.IO.Directory.CreateDirectory(HttpContext.Server.MapPath(root));
                    }
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        var files = Request.Files[i];
                        string fileName = null;
                        if (REF_ID != null)
                            fileName = $"{REF_ID}_{files.FileName}";//docFilename + System.IO.Path.GetFileName(files.FileName);
                        else if (docFilename != null)
                            fileName = docFilename;//docFilename + System.IO.Path.GetFileName(files.FileName);

                        var path = System.IO.Path.Combine(HttpContext.Server.MapPath(root), fileName);
                        files.SaveAs(path);
                    }
                    return Json(new { success = true, message = "File uploaded successfully" }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = false, message = "Please select a file !" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult SaveDocumentUpload(string REF_ID, string docFilename, string pathLocation)
        {
            try
            {
                if (Request.Files.Count > 0)
                {
                    var root = pathLocation;// "~/UploadedFiles";
                    bool folderpath = System.IO.Directory.Exists(HttpContext.Server.MapPath(root));
                    if (!folderpath)
                    {
                        System.IO.Directory.CreateDirectory(HttpContext.Server.MapPath(root));
                    }
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        var files = Request.Files[i];
                        string fileName = null;
                        if (REF_ID != null)
                            fileName = $"{REF_ID}_{files.FileName}";//docFilename + System.IO.Path.GetFileName(files.FileName);
                        else if (docFilename != null)
                            fileName = docFilename;//docFilename + System.IO.Path.GetFileName(files.FileName);

                        var path = System.IO.Path.Combine(HttpContext.Server.MapPath(root), fileName);
                        files.SaveAs(path);
                    }
                    return Json(new { success = true, message = "File uploaded successfully" }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = false, message = "Please select a file !" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}