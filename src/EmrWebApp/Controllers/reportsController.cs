﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EmrWebApp.controllers
{
    public class reportsController : Controller
    {
        // GET: reports
        public ActionResult Index()
        {
            return View();
        }
		public ActionResult assetReptSummary()
		{
			return View();
		}
		public ActionResult woReptSummary()
		{
			return View();
		}
		public ActionResult purchaseRequestByItems()
		{
			return View();
		}
        public ActionResult AssetPMScheduleRept()
        {
            return View();
        }
        public ActionResult stockOnHandAdjustRept()
        {
            return View();
        }
        public ActionResult workOrderPartsReqRept()
        {
            return View();
        }
        public ActionResult requestedPartsWo()
        {
            return View();
        }
        public ActionResult RequestedPartsWoZ()
        {
            return View();
        }
        public ActionResult workOrderByEngrRept()
        {
            return View();
        }
        public ActionResult woResponsibleStaff()
        {
            return View();
        }
        public ActionResult assetDepreciationRept()
        {
            return View();
        }
        public ActionResult workOrderPartsReqReptRorFR()
        {
            return View();
        }
        public ActionResult TestReport()
        {
            return View();
        }
        public ActionResult WorkOrderDetailRpt()
        {
            return View();
        }
        public ActionResult AssetDetailReport()
        {
            return View();
        }
        public ActionResult StockOnHandReport()
        {
            return View();
        }
        public ActionResult WorkOrderLabour()
        {
            return View();
        }
        public ActionResult PartsWorkOrderRpt()
        {
            return View();
        }
        public ActionResult SupplierDetailRpt()
        {
            return View();
        }
        public ActionResult searchTransaction()
        {
            return View();
        }

        public ActionResult systemTransactionReporting()
        {
            return View();
        }

        public ActionResult CreateRecall()
        {
            return View();
        }

        public ActionResult RecallProcessing()
        {
            return View();
        }
    }
}