﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace EmrWebApp.Controllers
{
    public class setupController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult services()
        {
            return View();
        }
        public ActionResult staffGroups()
        {
            return View();
        }
        public ActionResult staffPositions()
        {
            return View();
        }
        public ActionResult locations()
        {
            return View();
        }
        public ActionResult moduleFunctions()
        {
            return View();
        }
        public ActionResult moduleAccessAttribute()
        {
            return View();
        }
        public ActionResult groupPrivileges()
        {
            return View();
        }
        public ActionResult lovLookups()
        {
            return View();
        }

		public ActionResult riskFactor()
		{
			return View();
		}
		public ActionResult pmProcedure()
		{
			return View();
		}
		public ActionResult purchaseBillAddress()
		{
			return View();
		}
        public ActionResult approvingOfficer()
        {
            return View();
        }
        public ActionResult notificationSetup()
        {
            return View();
        }
        public ActionResult viewTemplate()
        {
            return View();
        }
        public ActionResult viewTemplateReport()
        {
            return View();
        }

        public ActionResult costcenter()
        {
            return View();
        }
    }
}