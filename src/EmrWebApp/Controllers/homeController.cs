﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.Web.Mvc;

namespace EmrWebApp.Controllers
{
    public class homeController : Controller
    {
       
        public ActionResult index()
        {
            ViewBag.WebApiURL = EmrWebApp.Properties.Settings.Default.webApiURL;
            return View();
        }

        public ActionResult localize(string lang)
        {
            sessionManager.locale = lang;
            if(lang != "en") { 
              //  sessionManager.loaded = "";
            }
            return RedirectToAction("Index");  
               
        }

        public ActionResult about()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Overview()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult OverviewCont()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult menu() {
           
            ViewBag.Message = "Your menu page";
            return View();
        }
    }
}