﻿using System.Web;
using System.Web.Optimization;

namespace EmrWebApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/js_jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_knockout").Include(
                        "~/Scripts/knockout-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_home_index").Include(
                        "~/Scripts/home_index.js"));

            bundles.Add(new ScriptBundle("~/bundles/js_patient_index").Include(
                        "~/Scripts/patient_index.js"));
           
            bundles.Add(new ScriptBundle("~/bundles/js_menuapps_index").Include(
                       "~/Scripts/menuapps_index.js"));
      


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/js_modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css_main").Include(
                      "~/Content/site.css",
                      "~/Content/font-awesome.min.css"));

            bundles.Add(new StyleBundle("~/Content/css_home_index").Include(
                      "~/Content/home_index.css"));

            bundles.Add(new StyleBundle("~/Content/css_patient_index").Include(
                      "~/Content/patient_index.css"));

         
            bundles.Add(new StyleBundle("~/Content/menuapps_index").Include(
                      "~/Content/menuapps_index.css"));

         
        }
    }
}
