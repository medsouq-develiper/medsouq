﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            liCancelAll: '.liCancelAll',
            divInfo: '.divInfo',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            detailItem: '.detailItem'
        },
        fld: {
            assetNoInfo: 'ASSETNO',
            assetDescInfo: 'ASSETDESC',
            reqWONo: 'REQNO'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmInfo: '#frmInfo',
            clinicdoctorID: '#CLINIC_DOCTOR_ID',
            clinicID: '#CLINIC_ID',
            staffID: '#STAFF_ID',
            serviceID: '#SERVICE_ID',
            successMessage: '#successMessage',
			SUPPORT_COST: '#SUPPORT_COST',
			modifyButtonId: '#modifyButtonId'
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
			searchResult: 'searchResult',
			groupPrivileges: 'GROUP_PRIVILEGES',
            infoDetails: 'AM_SUPPLIER'
        },
        url: {
            homeIndex: '/Home/Index',
            aimsPMSchedule: 'aims/am_PMSchedule',
            aimsInformation: '/aims/assetInfo',
			modalLookUp: 'openModalLookup',
			aimsinformationDvice: '/aims/amInformationServiceDevice',
            aimsHomepage: '/Menuapps/aimsIndex'
        },
        api: {

            readSetup: '/AIMS/ReadInfoServices',
            searchAll: '/AIMS/SearchPMSchedule',
            createSetup: '/AIMS/CreateNewInfoServices',
            updateSetup: '/AIMS/UpdateInfoServices'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liCancelAll + ',' +thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;
        document.getElementById("SUPPORT_COST").disabled = true;
        document.getElementById("SUPPORT_LASTUPDATED").disabled = true;

    };

    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liCancelAll + ','+thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.liApiStatus
        ).hide();


        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("SUPPORT_COST").disabled = false;
        document.getElementById("SUPPORT_LASTUPDATED").disabled = false;

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatuxText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liCancelAll + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();
        $(thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

      $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
           thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("SUPPORT_COST").disabled = false;
        document.getElementById("SUPPORT_LASTUPDATED").disabled = false;
        
    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.liCancelAll + ','+thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liCancelAll + ','+thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

        $(
          thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
           thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
           thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);

        document.getElementById("SUPPORT_COST").disabled = true;
        document.getElementById("SUPPORT_LASTUPDATED").disabled = true;

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.getAge = function (birthDate) {

        var seconds = Math.abs(new Date() - birthDate) / 1000;

        var numyears = Math.floor(seconds / 31536000);
        var numdays = Math.floor((seconds % 31536000) / 86400);
        var nummonths = numdays / 30;

        return numyears + " y " + Math.round(nummonths) + " m ";
    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.formatJSONDateToString = function () {

        var dateToFormat = arguments[0];
        var strDate;

        if (dateToFormat === null) return "-";
        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');


            if (isDateTime) {
                val = thisApp.formatJSONDateTimeToString(val);
            }

            if (isDate) {
                val = thisApp.formatJSONDateToString(val);
            }

            var $el = $('#' + name), type = $el.attr('type'), isTime = $el.attr('data-istime');
            if (isTime) {
                if (val === null || val === "") {
                    val = "00:00";
                } else {
                    val = thisApp.formatJSONTimeToString(val);
                }

            }

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var amInformationServicesViewModel = function (systemText) {

    var self = this;

    var ah = new appHelper(systemText);

    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.toggleReadMode();

        ah.initializeLayout();

        assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        assetDescription = ah.getParameterValueByName(ah.config.fld.assetDescInfo);

        if (assetDescription.length > 70) {
            assetDescription = assetDescription.substring(0, 70) + '...';
        }
        if (assetNoID !== null && assetNoID !== "") {
            $("#infoID").text(assetNoID + "-" + assetDescription);
            self.readSetupID(assetNoID);
        }
        

    };

    self.returnHome = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };

    self.assetInfo = function () {
        assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        var reqWoNO = ah.getParameterValueByName(ah.config.fld.reqWONo);
        window.location.href = ah.config.url.aimsInformation + "?ASSETNO=" + assetNoID + "&REQNO=" + reqWoNO;
    }

	self.infoSrvDviceNetwork = function () {
		assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
		assetDescription = ah.getParameterValueByName(ah.config.fld.assetDescInfo);
		var reqWoNO = ah.getParameterValueByName(ah.config.fld.reqWONo);
		//window.location.href = ah.config.url.aimsinformationDvice + "?ASSETNO=" + assetNoID + "&REQNO=" + reqWoNO;

		window.location.href = encodeURI(ah.config.url.aimsinformationDvice + "?ASSETNO=" + assetNoID + "&ASSETDESC=" + assetDescription + "&REQNO=" + reqWoNO);
	}

    self.createNew = function () {

        ah.toggleAddMode();

    };

    self.modifySetup = function () {

        ah.toggleEditMode();

    };

    self.addMaterials = function () {
        alert(window.location.href);
        window.location.href = window.location.href + ah.config.url.modalLookUp;
    };

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.saveAllChanges = function () {

        $(ah.config.id.saveInfo).trigger('click');

    };

    self.cancelChanges = function () {
        
        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            var refID = $("#ASSET_NO").val();
            self.readSetupExistID(refID);
        }

    };

    self.showDetails = function (isNotUpdate) {

        if (isNotUpdate) {

            var infoDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.infoDetails));

            if (infoDetails.SUPPORT_LASTUPDATED == "" || infoDetails.SUPPORT_LASTUPDATED == null || infoDetails.SUPPORT_LASTUPDATED == "0001-01-01T00:00:00" || infoDetails.SUPPORT_LASTUPDATED == "1900-01-01T00:00:00") {
                infoDetails.SUPPORT_LASTUPDATED = "0000-00-00T00:00:00";
            }
            ah.ResetControls();
            $("#ASSET_INFOSERVICES_NO").val(infoDetails.ASSET_INFOSERVICES_ID);
            
            ah.LoadJSON(infoDetails);
        }

        ah.toggleDisplayMode();
		self.privilegesValidation('MODIFY');
    };

    self.searchResult = function (jsonData) {


        var sr = jsonData.AM_ASSET_PMSCHEDULES;
        // alert(JSON.stringify(sr));
        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {
                var dueDate = new Date(sr[o].NEXT_PM_DUE);
                var sessionStart = new Date(sr[o].SESSION_START);

                var sessionEnd = new Date(sr[o].SESSION_END);
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].AM_ASSET_PMSCHEDULE_ID, '', sr[o].PM_PROCEDURE, '');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.servdept, sr[o].SERVICE_DEPARTMENT);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.assignto, sr[o].EMPLOYEE_INFO);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.frequency, sr[o].FREQUENCY_PERIOD + ' ' + sr[o].INTERVAL);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.nextDue, dueDate.toDateString().substring(4, 15));
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.grace, sr[o].GRACE_PERIOD + ' ' + sr[o].GRACE_PERIOD_TYPE);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.sessionstart, sessionStart.toDateString().substring(4, 15));
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.sessionend, sessionEnd.toDateString().substring(4, 15));

                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.detailItem).bind('click', self.readSetupID);
        ah.displaySearchResult();

    };

    self.readSetupExistID = function (refId) {

        // var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { ASSET_INFOSERVICES_ID: refId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.readSetupID = function (asset_no) {

       // var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { ASSET_INFOSERVICES_ID: asset_no, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();
        assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), ASSETNO: assetNoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };

	self.privilegesValidation = function (actionVal) {

		var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
		if (actionVal === 'NEW') {
			gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "16" });
			if (gPrivileges.length <= 0) {
				$(ah.config.cls.liAddNew).hide();
			}
		} else if (actionVal === 'MODIFY') {
			gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "3" });
			if (gPrivileges.length <= 0) {
				$(ah.config.id.modifyButtonId).hide();
			}
		}
	}

    self.saveInfo = function () {
      
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var setupDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        setupDetails.CREATED_BY = staffLogonSessions.STAFF_ID;

        if (setupDetails.SUPPORT_LASTUPDATED == "" || setupDetails.SUPPORT_LASTUPDATED == null || setupDetails.SUPPORT_LASTUPDATED == "00/00/0000" || setupDetails.SUPPORT_LASTUPDATED == "00-00-0000") {
            setupDetails.SUPPORT_LASTUPDATED = "01/01/1900";
        }
        if (ah.CurrentMode == ah.config.mode.add) {
            assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
            setupDetails.ASSET_NO = assetNoID;
            var jDate = new Date();
            var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);

            setupDetails.CREATED_DATE = strDateStart + ' ' + hourmin;
            postData = { AM_ASSET_INFOSERVICES: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            ah.toggleSaveMode();
            //alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.createSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {

            ah.toggleSaveMode();
            setupDetails.ASSET_INFOSERVICES_ID = $("#ASSET_INFOSERVICES_NO").val();
            postData = { AM_ASSET_INFOSERVICES: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };

            $.post(self.getApi() + ah.config.api.updateSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }

    };

    self.webApiCallbackStatus = function (jsonData) {
        //alert(JSON.stringify(jsonData));
       
        if (jsonData.ERROR) {

            if (jsonData.ERROR.ErrorText === "Information Services not found.") {
                self.createNew();
            } else {
                alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            }
            
            // window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.AM_ASSET_INFOSERVICES || jsonData.STAFF || jsonData.SUCCESS) {

            if (ah.CurrentMode == ah.config.mode.save) {
                sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_ASSET_INFOSERVICES));
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();

                if (jsonData.SUCCESS) {
                    self.showDetails(false);
                }
                else {
                    self.showDetails(true);
                }
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_ASSET_INFOSERVICES));
                self.showDetails(true);
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
                self.searchResult(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {

            }

        }
        else {

            alert(systemText.errorTwo);
            // window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};