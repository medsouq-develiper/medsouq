﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            detailItem: '.detailItem',
            liassetTag: '.liassetTag',
            wsAsset: '.wsAsset',
            itemRequest: '.itemRequest'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7,
            deleted: 8
        },
        fld: {
            groupIdInfo: 'STAFF_GROUP_ID',
            groupDescInfo: 'DESCRIPTION'
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmInfo: '#frmInfo',           
            staffID: '#STAFF_ID',
            saveInfoLog: '#saveInfoLog',
            serviceID: '#SERVICE_ID',
            searchResult1: '#searchResult1',
            successMessage: '#successMessage',            
            txtGlobalSearchOth: '#txtGlobalSearchOth'
          
        },

        cssCls: {
            viewMode: 'view-mode'
        },
        tagId: {

            txtGlobalSearchOth: 'txtGlobalSearchOth',
            priorPage: 'priorPage',
            nextPage: 'nextPage'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfo: 'data-info',
            datainfoID: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'searchResult',
            infoDetails: 'AM_PURCHASEREQUEST',
            stockList: 'PRODUCT_STOCKONHAND',
            privilegesList: 'PRIVILEGES_LIST'
        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            staffGroup: '/setup/staffGroups'
    
        },
        api: {

  
            searchAll: '/Setup/SearchPrivileges',
            createPrivileges: '/Setup/CreateNewGroupPrivileges'
          

        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-info='{0}'><i class='fa fa-arrow-circle-o-left'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
            searchRowHeaderTemplateList: "<a href='#openModal' class='fs-medium-normal detailItem' data-infoAssetID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            searchLOVRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem fc-greendark' data-info ='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} <span>{3}</span></a>",
            searchExistRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem fc-slate-blue' data-info ='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} <span class='fc-green'> {3} </span></a>"
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };
    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.toolBoxRight + ',' +thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess + ',' + thisApp.config.cls.liassetTag + ',' + thisApp.config.cls.itemRequest + ',' + thisApp.config.cls.contentHome
        ).hide();

        $(thisApp.config.cls.liSave).show();

        thisApp.CurrentMode = thisApp.config.mode.search;

    };
    thisApp.toggleDisplayModereq = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        //$(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        //$(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(thisApp.config.cls.contentField + ',' + thisApp.config.cls.searchAsset).show();
        //$(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.liApiStatus).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        //$(
        //    thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        //).prop(thisApp.config.attr.readOnly, false);

        //$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };
    thisApp.toggleSearchItems = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        //$(thisApp.config.cls.statusText).text(systemText.searchModeStatuxText);
        //$(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liSave +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();

    };
    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.id.contentSubHeader).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.liassetTag + ',' + thisApp.config.cls.itemRequest
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("REQ_DATE").disabled = false;
        document.getElementById("REF_WO").disabled = false;
        document.getElementById("REF_ASSETNO").disabled = false;
        document.getElementById("REQ_DEPT").disabled = false;


    };
    thisApp.toggleEditModeModal = function () {


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
    };
    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.liassetTag
        ).hide();

    };
    thisApp.toggleSearchModalMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;


    };
    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.liApiStatus ).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.itemRequest + ',' + thisApp.config.id.contentSubHeader).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.liassetTag).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

        document.getElementById("REQ_DATE").disabled = false;
        document.getElementById("REF_WO").disabled = false;
        document.getElementById("REF_ASSETNO").disabled = false;
        document.getElementById("REQ_DEPT").disabled = false;

    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        //$(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

        $(
           thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
           thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);
        document.getElementById("REQ_DATE").disabled = true;
        document.getElementById("REF_WO").disabled = true;
        document.getElementById("REF_ASSETNO").disabled = true;
        document.getElementById("REQ_DEPT").disabled = true;

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };



    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };
    thisApp.formatJSONDateToStringOth = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[0] + '-' + strDate[1] + '-' + strDate[2].substring(0, 2);


        return dateToFormat;
    };
    thisApp.formatJSONDateToString = function () {

        var dateToFormat = arguments[0];
        var strDate;

        if (dateToFormat === null) return "-";
        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };
    thisApp.formatJSONDateTimeToString = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0] + ' ' + strTime[1].substring(0, 5);


        return dateToFormat;
    };
    thisApp.formatJSONTimeToString = function () {

        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        var timeToFormat = strTime[1].substring(0, 5);


        return timeToFormat;
    };
    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');


            if (isDateTime) {
                val = thisApp.formatJSONDateTimeToString(val);
            }

            if (isDate) {
                val = thisApp.formatJSONDateToString(val);
            }

            var $el = $('#' + name), type = $el.attr('type'), isTime = $el.attr('data-istime');
            if (isTime) {
                if (val === null || val === "") {
                    val = "00:00";
                } else {
                    val = thisApp.formatJSONTimeToString(val);
                }

            }

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var amGroupPriviligesViewModel = function (systemText) {

    var self = this;

    var ah = new appHelper(systemText);

    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        

        ah.initializeLayout();
        
        var groupID = ah.getParameterValueByName(ah.config.fld.groupIdInfo);
        var groupDesc = ah.getParameterValueByName(ah.config.fld.groupDescInfo);
        $("#searchGroupPrivileges").text(groupDesc + '- Group Privileges');
        var postData;
        ah.toggleSearchMode;
        postData = { STAFF_GROUP_ID: groupID, STAFF_LOGON_SESSIONS: staffLogonSessions };
    //    alert(JSON.stringify(postData));
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.reqList = ko.observableArray("");

    self.returnHome = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.staffGroupInfo = function () {
        var staffGroupId = ah.getParameterValueByName(ah.config.fld.groupIdInfo);
        
        window.location.href = ah.config.url.staffGroup + "?STAFFGROUP=" + staffGroupId;
    }

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };
  
    self.saveAllChanges = function () {

        $(ah.config.id.saveInfo).trigger('click');

    };
    self.addItemReq = function () {

        var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfo));
        //alert(JSON.stringify(infoID));

        var existPart = self.checkExist(infoID.ID_PARTS);
        if (existPart > 0) {
            alert('Unable to Proceed. The selected item was already exist.');
            return;
        }
        var reqDate = $("#REQ_DATE").val();

        var strTime = '';
        var apptDate = reqDate;
        reqDate = apptDate.substring(6, 10) + '-' + apptDate.substring(3, 5) + '-' + apptDate.substring(0, 2);

        strTime = String(apptDate).split(' ');
        strTime = strTime[1].substring(0, 5);
        reqDate = reqDate + ' ' + strTime;

        self.reqList.push({
            DESCRIPTION: infoID.DESCRIPTION,
            ID_PARTS: infoID.ID_PARTS,
            QTY: 0,
            REQ_DATE: reqDate,
            REQUEST_ID: null,
            REF_WO: 0,
            REF_ASSETNO: 0,
            STORE_ID: 0,
            STATUS: null,
            AMPRREQITEMID: 0,
            REQ_DEPT: null,
            REMARKS: null,
            CURRSTATUS: "REQUEST",
            REQ_DETFLAG: null

        });
        // $("#ASSET_NO").val(infoID.ASSET_NO);
        //self.saveWSAsset();
        //window.location.href = window.location.href + ah.config.url.modalLookUp;

    };
    self.removeItem = function (reqList) {
        self.reqList.remove(reqList);
        self.populatePrivileges();
    };


    self.checkExist = function (idPart) {

        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.reqList);
        var partLists = JSON.parse(jsonObj);

        var partExist = partLists.filter(function (item) { return item.ID_PARTS === idPart });

        return partExist.length;
    };
    self.cancelChanges = function () {
        var refId = $("#REQUEST_ID").val();
        if (refId === null || refId === "" || refId === "0") {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();

            self.readPurchaseRequestExist(refId);
        }
        $("#itemsReceiveBatch *").attr('disabled', 'disabled');

    };

    self.showDetails = function (isNotUpdate) {
        // alert('here');
        if (isNotUpdate) {

            var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.infoDetails));

            var prInfo = sr.filter(function (item) { return item.REQ_DETFLAG === 'Y' });
            //var jsetupDetails = JSON.parse(prInfo);

            ah.ResetControls();
            ah.toggleDisplayMode();
            var currStatus = "REQUEST";
            // ah.LoadJSON(prInfo);
            if (prInfo.length > 0) {
                var reqDate = ah.formatJSONDateTimeToString(prInfo[0].REQ_DATE);


                $("#REQ_DATE").val(reqDate);
                $("#REQ_DETFLAG").val(prInfo[0].REQ_DETFLAG);
                $("#STATUS").val(prInfo[0].STATUS);
                $("#STATUSID").val(prInfo[0].STATUS);
                $("#REF_ASSETNO").val(prInfo[0].REF_ASSETNO);
                $("#REF_WO").val(prInfo[0].REF_WO);
                $("#REQ_DEPT").val(prInfo[0].REQ_DEPT);
                $("#REMARKS").val(prInfo[0].REMARKS);
                $("#AM_PRREQITEMID").val(prInfo[0].AM_PRREQITEMID);
                $("#REQUEST_ID").val(prInfo[0].REQUEST_ID);
                $("#STORE_ID").val(prInfo[0].STORE_ID);
                var currStatus = prInfo[0].STATUS;
                if (currStatus === 'APPROVED' || currStatus === 'CANCELLED' || currStatus === 'RECEIVED') {
                    $(ah.config.cls.liModify + ',' + ah.config.id.contentSubHeader).hide();
                    currStatus = prInfo[0].STATUS
                }


            }
            $("#REQUEST_NO").val(prInfo[0].REQUEST_ID);

            //alert(JSON.stringify(prInfo));
            if (sr.length > 0) {
                self.reqList.splice(0, 5000);
                for (var o in sr) {
                    var rDate = ah.formatJSONDateToString(sr[o].REQ_DATE);
                    self.reqList.push({
                        DESCRIPTION: sr[o].DESCRIPTION,
                        ID_PARTS: sr[o].ID_PARTS,
                        QTY: sr[o].QTY,
                        //REQ_DATE: rDate,
                        STORE_ID: sr[o].STORE_ID,
                        REQUEST_ID: sr[o].REQUEST_ID,
                        REF_WO: sr[o].REF_WO,
                        REF_ASSETNO: sr[o].REF_ASSETNO,
                        STATUS: sr[o].STATUS,
                        AMPRREQITEMID: sr[o].AMPRREQITEMID,
                        REQ_DEPT: sr[o].REQ_DEPT,
                        REMARKS: sr[o].REMARKS,
                        REQ_DETFLAG: sr[o].REQ_DETFLAG,
                        CURRSTATUS: currStatus,

                    });
                }



            }
            // $(ah.config.id.btnRemove).hide();

        }

        $("#itemsReceiveBatch *").attr('disabled', 'disabled');

    };
 
    self.searchResult = function (jsonData) {

        
        var sr = jsonData.PRIVILEGES_LIST;
       
        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

       // $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';
            //alert(JSON.stringify(sr));
            for (var o in sr) {
                var groupPrivileges = jsonData.GROUP_PRIVILEGES_LIST.filter(function (item) { return item.PRIVILEGES_ID === sr[o].PRIVILEGES_ID });
               
                if (groupPrivileges.length === 0) {
                    htmlstr += '<li>';
                    htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].PRIVILEGES_ID, sr[o].DESCRIPTION, "");


                    htmlstr += '</li>';
                }

            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.detailItem).bind('click', self.pushPrivileges);
        ah.displaySearchResult();
        dataItemsInfo = jsonData.GROUP_PRIVILEGES_LIST;
        if (dataItemsInfo.length > 0) {
            for (var o in dataItemsInfo) {

                var groupPrivileges = jsonData.PRIVILEGES_LIST.filter(function (item) { return item.PRIVILEGES_ID === dataItemsInfo[o].PRIVILEGES_ID });

                self.reqList.push({
                    DESCRIPTION: groupPrivileges[0].DESCRIPTION,
                    PRIVILEGES_ID: dataItemsInfo[o].PRIVILEGES_ID,
                    STAFF_GROUP_ID: dataItemsInfo[o].STAFF_GROUP_ID,
                    CREATED_BY: dataItemsInfo[o].CREATED_BY,
                    CREATED_DATE: dataItemsInfo[o].CREATED_DATE
                });
            }
        }
        

    };
    self.populatePrivileges = function (jsonData) {


        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.privilegesList));
        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.reqList);


        var groupPrivilegesLists = JSON.parse(jsonObj);
        
        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

       //  $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';
           


            for (var o in sr) {
                var groupPrivileges = groupPrivilegesLists.filter(function (item) { return item.PRIVILEGES_ID === sr[o].PRIVILEGES_ID });
               
                if (groupPrivileges.length === 0){
                    htmlstr += '<li>';
                    htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].PRIVILEGES_ID, sr[o].DESCRIPTION, "");


                    htmlstr += '</li>';
                }
                

            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.detailItem).bind('click', self.pushPrivileges);
        ah.displaySearchResult();

    };
  
    self.pushPrivileges = function () {
        var dataItemsInfo = JSON.parse(this.getAttribute(ah.config.attr.datainfo));
        var groupID = ah.getParameterValueByName(ah.config.fld.groupIdInfo);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        var jDate = new Date();

        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        var reqDate = strDateStart + ' ' + hourmin;

        self.reqList.push({
            DESCRIPTION: dataItemsInfo.DESCRIPTION,
            PRIVILEGES_ID: dataItemsInfo.PRIVILEGES_ID,
            STAFF_GROUP_ID: groupID,
            CREATED_BY: staffLogonSessions.USER_ID,
            CREATED_DATE:reqDate
        });
        self.populatePrivileges();
    };

 
    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;
        self.reqList.splice(0, 5000);
        // self.clearTableData();
        ah.toggleSearchMode();

        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), RETACTION: "Y", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };

 
  
    self.saveInfo = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var itemDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.reqList);


        var groupPrivilegesLists = JSON.parse(jsonObj);
        //


        if (groupPrivilegesLists.length <= 0) {
            alert('Unable to proceed. No item to save.')
            return;
        }

        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        strDateStart = strDateStart + ' ' + hourmin;

        var actionStr = $("#POSTID").val();
        //if (ah.CurrentMode == ah.config.mode.add) {
        var groupID = ah.getParameterValueByName(ah.config.fld.groupIdInfo);
        postData = { STAFF_GROUP_ID: groupID, ACTION: actionStr, CREATE_DATE: strDateStart, GROUP_PRIVILEGES: groupPrivilegesLists, STAFF_LOGON_SESSIONS: staffLogonSessions };
       // alert(JSON.stringify(postData));
        // return;
        ah.toggleSaveMode();
        $.post(self.getApi() + ah.config.api.createPrivileges, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        //}

    };
   
    self.webApiCallbackStatus = function (jsonData) {


        // alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {
           
                alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
          
        }
        else if (jsonData.PRIVILEGES_LIST || jsonData.GROUP_PRIVILEGES_LIST || jsonData.GROUP_PRIVILEGES  || jsonData.STAFF || jsonData.SUCCESS) {
 
            if (ah.CurrentMode == ah.config.mode.save) {
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();

                if (jsonData.SUCCESS) {
                    $(ah.config.cls.liApiStatus).hide();
                   // self.showDetails(false);
                }
                else {
                    $(ah.config.cls.liApiStatus).hide();
                   // self.showDetails(true);

                }

            }
            
            else if (ah.CurrentMode == ah.config.mode.read) {
                // alert(JSON.stringify(jsonData));
              
                self.showDetails(true);
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
                sessionStorage.setItem(ah.config.skey.privilegesList, JSON.stringify(jsonData.PRIVILEGES_LIST));
                    self.searchResult(jsonData);
  
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {
        
            }

        }
        else {

            alert(systemText.errorTwo);
            //window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};