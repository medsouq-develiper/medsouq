﻿var appHelper = function (systemText) {

    var thisApp = this;
      
    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            detailItem: '.detailItem',
            liassetTag: '.liassetTag',
            wsAsset: '.wsAsset'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeNumber: 'input[type=number]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7,
            deleted: 8
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmInfo: '#frmInfo',
            frmInfoInspection: '#frmInfoInspection',
            clinicdoctorID: '#CLINIC_DOCTOR_ID',
            clinicID: '#CLINIC_ID',
            frmwsAsset: '#frmwsAsset',
            frmInfoReading: '#frmInfoReading',
            staffID: '#STAFF_ID',
            saveInfoLog: '#saveInfoLog',
            serviceID: '#SERVICE_ID',
            searchResult1: '#searchResult1',
            successMessage: '#successMessage',
            saveInfoReading: '#saveInfoReading',
            txtGlobalSearchOth: '#txtGlobalSearchOth',
            pageNum: '#pageNum',
            pageCount: '#pageCount',
            loadNumber: '#loadNumber',
            totNumber: '#totNumber',
            noResult: '#noresult',
            emp: '#EMPLOYEE'
        },

        cssCls: {
            viewMode: 'view-mode'
        },
        tagId: {
            
            txtGlobalSearchOth: 'txtGlobalSearchOth',
            priorPage: 'priorPage',
            nextPage: 'nextPage'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID',
            datainfoAssetID: 'data-infoAssetID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'searchResult',
            infoDetails: 'AM_INSPECTION',
            assetList: 'AM_ASSET_DETAILS',
            wsassetList: 'AM_INSPECTIONASSET'
        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalLookUp: 'openModalLookup',
            modalClose: '#',
            modalAddItems: '#openModal',
            modalReading: 'openModalReading'
        },
        api: {

            readSetup: '/AIMS/ReadWSInfo',
            searchAll: '/AIMS/SearchWS',
            createSetup: '/AIMS/CreateNewWS',
            updateSetup: '/AIMS/UpdateWSInfo',
            createInsp: '/AIMS/CreateNewWSTrans',
            searchInsp: '/AIMS/SearchWSTrans',
            searchAssets: '/AIMS/SearchAssets',
            updateInsp: '/AIMS/UpdateWSInfoTrans',
            createWSAsset: '/AIMs/CreateNewWSAsset',
            deleteWSAsset: '/AIMS/DeleteWSAsset',
            createReading: '/AIMS/CreateNewReading',
            searchReading: '/AIMS/SearchReading'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
            searchRowHeaderTemplateList: "<a href='#openModal' class='fs-medium-normal detailItem' data-infoAssetID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",

        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {
        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess + ',' + thisApp.config.cls.liassetTag
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };
    thisApp.toggleDisplayModeAsset = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;


        $(thisApp.config.cls.contentField + ',' + thisApp.config.cls.searchAsset).show();
    };
    thisApp.toggleSearchItems = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        //$(thisApp.config.cls.statusText).text(systemText.searchModeStatuxText);
        //$(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liSave +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();
   
    };
    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.liassetTag
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

        $("label[for='AM_INSPECTION_NO']").hide();
        $("#AM_INSPECTION_NO").hide();

    };
    thisApp.toggleEditModeModal = function () {


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
    };
    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.liassetTag
        ).hide();

    };
    thisApp.toggleSearchModalMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;
      
    };
    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.liassetTag).hide();
        $(
            thisApp.config.tag.inputTypeText + '.' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        $("#AM_INSPECTION_NO").prop("disabled", "disabled");

    };

    thisApp.toggleSaveMode = function () {

        $("label[for='AM_INSPECTION_NO']").show();
        $("#AM_INSPECTION_NO").show();
        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
        $("label[for='AM_INSPECTION_NO']").show();
        $("#AM_INSPECTION_NO").show();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

        $(
           thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
           thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {
        $(form).find("input:disabled").removeAttr("disabled");
        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

 

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.formatJSONDateToString = function () {

        var dateToFormat = arguments[0];
        var strDate;

        if (dateToFormat === null) return "-";
        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };
    thisApp.formatJSONDateTimeToString = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0] + ' ' + strTime[1].substring(0, 5);


        return dateToFormat;
    };
    thisApp.formatJSONTimeToString = function () {

        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        var timeToFormat = strTime[1].substring(0, 5);


        return timeToFormat;
    };
    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');

           

            if (isDateTime) {
                val = thisApp.formatJSONDateTimeToString(val);
            }

            if (isDate) {
                val = thisApp.formatJSONDateToString(val);
            }

            var $el = $('#' + name), type = $el.attr('type'), isTime = $el.attr('data-istime');
            if (isTime) {
                if (val === null || val === "") {
                    val = "00:00";
                } else {
                    val = thisApp.formatJSONTimeToString(val);
                }

            }

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    }
                    else if (name == "AM_INSPECTION_ID") {
                        if (!$("#AM_INSPECTION_ID").val()) {
                            $el.val(val);
                        }
                    }
                    else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var amWorksheetInspectionViewModel = function (systemText) {

    var self = this;

    var ah = new appHelper(systemText);
	self.searchAssetFilter = {
		status: ko.observable('I')
	};
    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.toggleReadMode();

        ah.initializeLayout();

    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.wsAssetList = ko.observableArray("");
    self.readingList = ko.observableArray("");
    self.returnHome = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.createNew = function () {
        self.wsAssetList.splice(0, 5000);
        sessionStorage.removeItem("AM_INSPECTIONASSET");
        ah.toggleAddMode();

    };
    self.pageLoader = function () {
        var senderID = $(arguments[1].currentTarget).attr('id');

        var searchStr = $(ah.config.id.txtGlobalSearchOth).val().toString();

        var pageNum = parseInt($(ah.config.id.pageNum).val());
        var pageCount = parseInt($(ah.config.id.pageCount).val());
        if (isNaN(pageCount)) {
            pageCount = 1;
        }
        if (isNaN(pageNum)) {
            pageNum = 0;
        }
        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.assetList));
        var lastPage = Math.ceil(sr.length / 10);

        
        switch (senderID) {

            case ah.config.tagId.nextPage: {
                if (pageCount < lastPage) {
                    pageNum = pageNum + 10;
                    pageCount++;
                    self.searchItemsResult(searchStr, pageNum);
                }
                break;
            }
            case ah.config.tagId.priorPage: {

                if (pageNum === 0) {
                } else {
                    pageNum = pageNum - 10;
                    if (pageNum < 0) {
                        pageNum = 0;
                    }
                    pageCount = pageCount - 1;
                }

                self.searchItemsResult(searchStr, pageNum);
                break;
            }

            case ah.config.tagId.txtGlobalSearchOth: {
                pageCount = 1;
                pageNum = 0;

                self.searchItemsResult(searchStr, 0);
                break;
            }
        }

        $(ah.config.id.loadNumber).text(pageCount.toString());
        $(ah.config.id.pageCount).val(pageCount.toString());
        $(ah.config.id.pageNum).val(pageNum);

    };
    self.modifySetup = function () {

        ah.toggleEditMode();

    };
    self.cancelChangesModal = function () {
        $("#readingTag").text("Last Reading");
        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();

        } else {
            var refId = $("#AM_INSPECTION_ID").val();
            ah.toggleDisplayMode();
            self.readSetupExistID(refId);

        }

        window.location.href = ah.config.url.modalClose;

    };
    self.loadInspectionLog = function () {
        ah.toggleEditModeModal();
        $("#winTitle").text("Last Inspection Log");
        $("#newRecord").show();
       // window.location.href = window.location.href + ah.config.url.modalLookUp;
    };
    self.addInspectionLog = function () {
        ah.toggleEditModeModal();
        document.getElementById("frmInfoInspection").reset();
        $("#winTitle").text("New Record");
        $("#newRecord").hide();
        $("#AM_INSPECTIONTRANSID").val(0);
        var jDate = new Date();
        var strDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        var reqDate = strDate + ' ' + hourmin;


        $("#INSPECTION_DATE").val(reqDate);
        //window.location.href = window.location.href + ah.config.url.modalLookUp;
    };
    self.addNewInspectionLog = function () {
        ah.toggleEditModeModal();
        document.getElementById("frmInfoInspection").reset();
        $("#winTitle").text("New Record");
        $("#newRecord").hide();
        $("#AM_INSPECTIONTRANSID").val(0);
        var jDate = new Date();
        var strDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        var reqDate = strDate + ' ' + hourmin;


        $("#INSPECTION_DATE").val(reqDate);
       
    };
    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };
    self.assetSearchNow = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));


        //ah.toggleSearchMode();
        ah.toggleSearchItems();
		postData = { SEARCH: "", SEARCH_FILTER: self.searchAssetFilter, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAssets, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = ah.config.url.modalAddItems;

    };
    self.saveAllChanges = function () {

        $(ah.config.id.saveInfo).trigger('click');

    };
    self.addWSAsset = function () {

        var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfoAssetID));
        var wsId = $("#AM_INSPECTION_ID").val();

        var existPart = self.checkExist(infoID.ASSET_NO);
        if (existPart > 0) {
            alert('Unable to Proceed. The selected asset was already exist in pieces covered.');
            return;
        }

        if (infoID.DESCRIPTION.length > 60)
            infoID.DESCRIPTION = infoID.DESCRIPTION.substring(0, 59) + "...";

        self.wsAssetList.push({
            DESCRIPTION: infoID.DESCRIPTION,
            ASSET_NO: infoID.ASSET_NO,
            REF_INSPECTION_ID: wsId
        });
        $("#ASSET_NO").val(infoID.ASSET_NO);
        self.saveWSAsset();
        window.location.href = window.location.href + ah.config.url.modalLookUp;

    };
    self.removeItem = function (wsAssetList) {
        self.wsAssetList.remove(wsAssetList);
        var infoDetails = JSON.stringify(wsAssetList);
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
       
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        infoDetails = JSON.parse(infoDetails);
      
		var refId = infoDetails.AM_INSPECTIONASSETID;
	
		if (typeof refId === 'undefined') {
			alert('Unable to proceed, Screen will automatically refresh.');
			var refId = $("#AM_INSPECTION_ID").val();
			self.readSetupExistID(refId);
			return;
		}
        postData = { AM_INSPECTIONASSETID: refId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        ah.CurrentMode = ah.config.mode.deleted;
        $.post(self.getApi() + ah.config.api.deleteWSAsset, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.addReading = function () {
        ah.toggleEditModeModal();
        
        var jDate = new Date();
        var strDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        var reqDate = strDate + ' ' + hourmin;


        $("#READING_DATE").val(reqDate);
        $("#AM_METERREADINGID").val(0);
        $("#NEXT_PMDUE").val(0);
        $("#CURRENT_READING").val(0);
        $("#readingTag").text('New Reading');
        $("#saveReading").show();
        $("#newReading").hide();
        
    };
    self.mReading = function (wsAssetList) {
        

        var infoDetails = JSON.stringify(wsAssetList);
        infoDetails = JSON.parse(infoDetails);
        var assetDescription = infoDetails.DESCRIPTION;
        if (assetDescription.length > 40) {
            assetDescription = assetDescription.substring(0, 40) + '...';
        }
        //alert(infoDetails.ASSET_NO);
        $("#ASSETID").val(infoDetails.ASSET_NO);
        $("#assetInfo").text("Asset: " + infoDetails.ASSET_NO + "-" + assetDescription);
        self.searchReading(infoDetails.ASSET_NO);
    };
    self.checkExist = function (idPart) {

        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.wsAssetList);
        var partLists = JSON.parse(jsonObj);

        var partExist = partLists.filter(function (item) { return item.ASSET_NO === idPart });

        return partExist.length;
    };
    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            var refId = $("#AM_INSPECTION_ID").val();
            self.readSetupExistID(refId);
        }

    };

    self.showDetails = function (isNotUpdate) {

        if (isNotUpdate) {

            var infoDetails = sessionStorage.getItem(ah.config.skey.infoDetails);

            var jsetupDetails = JSON.parse(infoDetails);

            ah.ResetControls();
            ah.LoadJSON(jsetupDetails);

            $("#AM_INSPECTION_NO").val(jsetupDetails.AM_INSPECTION_ID);
            ah.toggleDisplayMode();
            $(ah.config.cls.wsAsset).show();
            var inspType = $("#INSPECTION_TYPE").val();
           
            if (inspType === "Plain") {

                $(ah.config.cls.liassetTag).hide();
                $(ah.config.cls.wsAsset).hide();
            } else {
                $(ah.config.cls.liassetTag).show();
                $(ah.config.cls.wsAsset).show();
                // alert(self.wsAssetList().length);

                var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.wsassetList));

                self.wsAssetList.splice(0, 5000);
                //alert(JSON.stringify(sr));
                if (sr.length > 0) {

                    for (var o in sr) {
                        
                        if (sr[o].DESCRIPTION.length > 60)
                            sr[o].DESCRIPTION = sr[o].DESCRIPTION.substring(0, 59) + "...";

                        self.wsAssetList.push({
                            DESCRIPTION: sr[o].DESCRIPTION,
                            ASSET_NO: sr[o].ASSET_NO,
                            AM_INSPECTIONASSETID: sr[o].AM_INSPECTIONASSETID,
                            REF_INSPECTION_ID: sr[o].REF_INSPECTION_ID
                        });
                    }


                } else {

                }

            }

        } else {
            var inspType = $("#INSPECTION_TYPE").val();
            
            if (inspType === "Plain") {

                $(ah.config.cls.liassetTag).hide();
                $(ah.config.cls.wsAsset).hide();
            } else {
                $(ah.config.cls.liassetTag).show();
                $(ah.config.cls.wsAsset).show();
            }
        }
        ah.toggleDisplayMode();
        
        
    };
    self.searchItemsResult = function (LovCategory, pageLoad) {

        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.assetList));
       // alert(JSON.stringify(sr));
        if (LovCategory !== null && LovCategory !== "") {
            var arr = sr;
            var keywords = LovCategory.toUpperCase();
            var filtered = [];

            for (var i = 0; i < sr.length; i++) {
                for (var j = 0; j < keywords.length; j++) {
                    if ((sr[i].DESCRIPTION.toUpperCase()).indexOf(keywords) > -1 || (sr[i].ASSET_NO.toUpperCase()).indexOf(keywords) > -1) {
                        filtered.push(sr[i]);
                        break;
                    }
                }
            }
            sr = filtered;
        }

        $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).html('');

        if (sr.length > 0) {
            $(ah.config.id.noResult).hide();
            $("#page1").show();
            $("#page2").show();
            $("#nextPage").show();
            $("#priorPage").show();
            $("#pageSeparator").show();
            var totalrecords = sr.length;
            var pagenum = Math.ceil(totalrecords / 10);
            //if (pagenum < 2 && pagenum > 1) {
            //    pagenum = 2;
            //}
            $(ah.config.id.totNumber).text(pagenum.toString());

            if (pagenum <= 1) {
                $("#page1").hide();
                $("#page2").hide();
                $("#nextPage").hide();
                $("#priorPage").hide();
                $("#pageSeparator").hide();


            }
            var htmlstr = '';
            var o = pageLoad;

            for (var i = 0; i < 10; i++) {
                var assetDescription = sr[o].DESCRIPTION;
                if (assetDescription.length > 70) {
                    assetDescription = assetDescription.substring(0, 70) + '...';
                }
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplateList, JSON.stringify(sr[o]), sr[o].ASSET_NO, assetDescription, '');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.buildingName, sr[o].BUILDING) + '&nbsp;&nbsp;';
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.locationName, sr[o].LOCATION + '&nbsp;&nbsp;');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.conditionName, sr[o].CONDITION + '&nbsp;&nbsp;');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.statusName, sr[o].STATUS);
                htmlstr += '</li>';
                o++
                if (totalrecords === o) {
                    break;
                }
            }
            //alert(JSON.stringify(sr));
            //alert(htmlstr);
            $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).append(htmlstr);
        }
        else {
            $("#page1").hide();
            $("#page2").hide();
            $("#nextPage").hide();
            $("#priorPage").hide();
            $("#pageSeparator").hide();
            $(ah.config.id.noResult).show();
        }

        $(ah.config.cls.detailItem).bind('click', self.addWSAsset);
        ah.toggleDisplayModeAsset();
        //ah.toggleAddItems();
    };
    self.searchResult = function (jsonData) {


        var sr = jsonData.AM_INSPECTION;

        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {

                if (sr[o].PROCESS != null && sr[o].PROCESS.length > 100)
                    sr[o].PROCESS = sr[o].PROCESS.substring(0, 99) + "...";

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].AM_INSPECTION_ID, sr[o].AM_INSPECTION_ID, '(' + sr[o].INSPECTION_TYPE + ')', sr[o].DESCRIPTION);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.inspectionprocess, sr[o].PROCESS);

                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.detailItem).bind('click', self.readSetupID);
        ah.displaySearchResult();

    };
    self.searchInspectionLogResult = function (jsonData) {


        var sr = jsonData.AM_INSPECTIONTRANS;
        
       
        if (sr.length > 0) {

           // alert(JSON.stringify(sr[0]));
            var jsetupDetails = sr[0];
            
            ah.LoadJSON(jsetupDetails);
            self.loadInspectionLog();
        } else {
            self.addNewInspectionLog();
        }
        document.getElementById("EMPLOYEE").options.length = 0;
        $.each(jsonData.STAFFLIST, function (item) {

            $(ah.config.id.emp)
                .append($("<option></option>")
                .attr("value", jsonData.STAFFLIST[item].STAFF_ID)
                .text(jsonData.STAFFLIST[item].FIRST_NAME + ' ' + jsonData.STAFFLIST[item].LAST_NAME));
        });

        if (!window.location.href.includes("openModalLookup"))
            window.location.href = window.location.href + ah.config.url.modalLookUp;
    };
    self.searchReadingResult = function (jsonData) {


        var sr = jsonData.AM_METERREADING;

        self.readingList.splice(0, 5000);
        if (sr.length > 0) {
            // alert(JSON.stringify(sr[0]));
            var jsetupDetails = sr[0];

            ah.LoadJSON(jsetupDetails);
            //self.loadInspectionLog();
            for (var o in sr) {
                var readingDate = ah.formatJSONDateTimeToString(sr[o].READING_DATE);
                self.readingList.push({
                    DATEINFO: readingDate,
                    READINGINFO: sr[o].CURRENT_READING
                });
            }
            $("#saveReading").hide();
            $("#newReading").show();
        } else {
            self.addReading();
        }

        if (!window.location.href.includes('openModalReading'))
            window.location.href = window.location.href + ah.config.url.modalReading;

    };
    self.readSetupID = function () {

        var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { AM_INSPECTION_ID: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.readSetupExistID = function (refId) {

        
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();
        
        postData = { AM_INSPECTION_ID: refId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.searchInspectionLog = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchModalMode();
		var wsInspectionId = $("#AM_INSPECTION_ID").val();
		
        postData = { SEARCH: wsInspectionId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchInsp, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.searchReading = function (refId) {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchModalMode();
        postData = { SEARCH: refId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchReading, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();

        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.saveLog = function () {
        $(ah.config.id.saveInfoLog).trigger('click');
    };

    self.saveReading = function () {
        $(ah.config.id.saveInfoReading).trigger('click');
        $("#readingTag").text('Last Reading');

    };
    self.saveWSAsset = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var infoDetails = ah.ConvertFormToJSON($(ah.config.id.frmwsAsset)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        infoDetails.CREATED_BY = staffLogonSessions.STAFF_ID;
   

       
        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);
        var inspId = $("#AM_INSPECTION_ID").val();
        infoDetails.AM_INSPECTIONASSETID = 0;
        infoDetails.REF_INSPECTION_ID = inspId;
        infoDetails.CREATED_DATE = strDateStart + ' ' + hourmin;
        postData = { AM_INSPECTIONASSET: infoDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
        ah.toggleSaveMode();
        //alert(JSON.stringify(postData));
        $.post(self.getApi() + ah.config.api.createWSAsset, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
 
    };
    self.saveInspectionLog = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var inspectionDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfoInspection)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        inspectionDetails.CREATED_BY = staffLogonSessions.STAFF_ID;
        var strTime = '';
        var apptDate = inspectionDetails.INSPECTION_DATE;
        var appointmentDate = apptDate.substring(6, 10) + '-' + apptDate.substring(3, 5) + '-' + apptDate.substring(0, 2);
        var apptDateStr = appointmentDate;
        strTime = String(apptDate).split(' ');
        strTime = strTime[1].substring(0, 5);
        appointmentDate = appointmentDate + ' ' + strTime;
        inspectionDetails.INSPECTION_DATE = appointmentDate;
 
        var transId = inspectionDetails.AM_INSPECTIONTRANSID;
        if (transId === "0" || transId === "" || transId === null) {
            var jDate = new Date();
            var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);
            var inspId = $("#AM_INSPECTION_ID").val();
            inspectionDetails.AM_INSPECTIONTRANSID = 0;
            inspectionDetails.AM_INSPECTION_ID = inspId;
            inspectionDetails.CREATED_DATE = strDateStart + ' ' + hourmin;
            postData = { AM_INSPECTIONTRANS: inspectionDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            ah.toggleSaveMode();
            //alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.createInsp, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {

            ah.toggleSaveMode();
            postData = { AM_INSPECTIONTRANS: inspectionDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
          
            $.post(self.getApi() + ah.config.api.updateInsp, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        window.location.href = ah.config.url.modalClose;
    };

    self.saveReadingLog = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var readingDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfoReading)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        readingDetails.CREATED_BY = staffLogonSessions.STAFF_ID;
        var strTime = '';
        var apptDate = readingDetails.READING_DATE;
        var appointmentDate = apptDate.substring(6, 10) + '-' + apptDate.substring(3, 5) + '-' + apptDate.substring(0, 2);
        var apptDateStr = appointmentDate;
        strTime = String(apptDate).split(' ');
        strTime = strTime[1].substring(0, 5);
        appointmentDate = appointmentDate + ' ' + strTime;
        readingDetails.READING_DATE = appointmentDate;

        var transId = readingDetails.AM_METERREADINGID;
        if (transId === "0" || transId === "" || transId === null) {
            readingDetails.ASSET_NO = $("#ASSETID").val();
            var jDate = new Date();
            var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);
            var inspId = $("#AM_INSPECTION_ID").val();
            readingDetails.AM_METERREADINGID = 0;
            readingDetails.AM_INSPECTION_ID = inspId;
            readingDetails.CREATED_DATE = strDateStart + ' ' + hourmin;
            postData = { AM_METERREADING: readingDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            ah.toggleSaveMode();
            //alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.createReading, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {

            ah.toggleSaveMode();
            postData = { AM_INSPECTIONTRANS: inspectionDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };

            $.post(self.getApi() + ah.config.api.updateInsp, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        window.location.href = ah.config.url.modalClose;
    };
    self.saveInfo = function () {
        //self.wsAssetList.splice(0, 5000);
        //sessionStorage.removeItem("AM_INSPECTIONASSET");
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var setupDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        setupDetails.CREATED_BY = staffLogonSessions.STAFF_ID;

        

        if (ah.CurrentMode == ah.config.mode.add) {
            var jDate = new Date();
            var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);
            setupDetails.AM_INSPECTION_ID = 0;
            setupDetails.CREATED_DATE = strDateStart + ' ' + hourmin;
            postData = { AM_INSPECTION: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            ah.toggleSaveMode();
            //alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.createSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {

            ah.toggleSaveMode();
            postData = { AM_INSPECTION: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            //alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.updateSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }

    };

    self.webApiCallbackStatus = function (jsonData) {
        //alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            //window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.STAFFLIST || jsonData.AM_METERREADING || jsonData.AM_INSPECTIONASSET || jsonData.AM_INSPECTION || jsonData.AM_ASSET_DETAILS || jsonData.AM_INSPECTIONTRANS || jsonData.STAFF || jsonData.SUCCESS) {

            if (ah.CurrentMode == ah.config.mode.save) {
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();
                if (jsonData.AM_INSPECTIONASSET) {
                    ah.toggleDisplayMode();
                }else if (jsonData.AM_INSPECTIONTRANS) {
                    ah.toggleDisplayMode();
                } else if (jsonData.AM_METERREADING) {
                    ah.toggleDisplayMode();
                } else {
                    sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_INSPECTION));
                    sessionStorage.setItem(ah.config.skey.wsassetList, JSON.stringify(jsonData.AM_INSPECTIONASSET));

                    if (jsonData.SUCCESS) {
                        self.showDetails(false);
                    }
                    else {
                        self.showDetails(true);
                    }
                }
                
            }
            else if (ah.CurrentMode == ah.config.mode.deleted) {
                
                $(ah.config.id.successMessage).html(systemText.deletedSuccessMessage);
                ah.showSavingStatusSuccess();
                ah.toggleDisplayMode();
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_INSPECTION));
                sessionStorage.setItem(ah.config.skey.wsassetList, JSON.stringify(jsonData.AM_INSPECTIONASSET));
                self.showDetails(true);
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
                if (jsonData.AM_INSPECTIONTRANS) {
                    
                    self.searchInspectionLogResult(jsonData);
                }else if (jsonData.AM_ASSET_DETAILS) {
                    sessionStorage.setItem(ah.config.skey.assetList, JSON.stringify(jsonData.AM_ASSET_DETAILS));
                    $(ah.config.id.loadNumber).text("1");
                    $(ah.config.id.pageCount).val("1");
                    $(ah.config.id.pageNum).val(0);
                    self.searchItemsResult("", 0);
                } else if (jsonData.AM_METERREADING) {
                    self.searchReadingResult(jsonData);
                } else
                {
                    self.searchResult(jsonData);
                }
                
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {

            }

        }
        else {

            alert(systemText.errorTwo);
            //window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};