﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            detailItem: '.detailItem',
            liAddItems: '.liAddItems'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            inputTypeNumber: 'input[type=number]',	
            inputTypeCheckbox: 'input[type=checkbox]',	
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7,
            searchDropdown: 8
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmInfo: '#frmInfo',
            clinicdoctorID: '#CLINIC_DOCTOR_ID',
            clinicID: '#CLINIC_ID',
            staffID: '#STAFF_ID',
            serviceID: '#SERVICE_ID',
            successMessage: '#successMessage',
            woLabourStaff: '#EMPLOYEE',
            loadNumber: '#loadNumber',
            pageCount: '#pageCount',
            pageNum: '#pageNum',
            txtGlobalSearchOth: '#txtGlobalSearchOth',
            searchResult1: '#searchResult1',
            searchResultPartsOnhand: '#searchResultPartsOnhand',
            saveMaterialInfo: '#saveMaterialInfo',
            modifyButton: '#modifyButton',
            addButtonModal: '#addButtonModal',
            wolResponse: '#RESPONSE',
            lookUptxtGlobalSearchOth: '#lookUptxtGlobalSearchOth',
            searchResultLookUp: '#searchResultLookUp'
        },
        fld: {
            assetNoInfo: 'ASSETNO',
            assetDescInfo: 'ASSETDESC',
            assetwoReqNo: 'REQNO',
            assetwoRequestor: 'REQTOR',
            assetwoReqContact: 'REQCONTACT',
            assetwoReqDate: 'REQDATE',
            assetwoReqType: 'REQTYPE',
            assetwoProblem: 'REQDESC',
            assetwoStatus: 'REQSTATUS',
            assetwoAssignTo: 'ASSIGNTO',
            reqWOAction: 'REQWOAC',
            assetAssignDesc: "ASSIGNDESC",
            keyAccess: "jhsadfjhxsdf",
            fromDashBoard: 'asdfxhsadfsdfx'
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID',
            datainfoPartID: 'data-infoPartID'
        },
        tagId: {
            
            txtGlobalSearchOth: 'txtGlobalSearchOth',
            priorPage: 'priorPage',
            nextPage: 'nextPage',
            searchStaffId: 'searchStaffId',
            searchSupplierId: 'searchSupplierId',
            lookUptxtGlobalSearchOth: 'lookUptxtGlobalSearchOth'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'searchResult',
            infoDetails: 'AM_SUPPLIER',
            refList: 'WOL_LOOKUP_V',
            partsOnhandList: 'PARTS_STOCKONHAND_V',
            woMaterials: 'AM_PARTSREQUEST',
            wolLookup: 'AM_WORKORDER_LABOUR_OTH'
        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            aimsWorkOrder: '/aims/amWorkOrder',
            aimsWorkOrderForClose: '/aims/amWorkOrderForClose',
            openModal: '#openModal',
            modalAddItems: '#openModalInHouse',
            modalClose: '#',
            openModalLookup: '#openModalLookup'
        },
        api: {

            readSetup: '/AIMS/ReadWOLabour',
            searchAll: '/AIMS/SearchWOLabourLOV',
            createSetup: '/AIMS/CreateNewWOLabour',
            updateSetup: '/AIMS/UpdateWOLabour',
            searchwolRefTypeList: '/AIMS/SearchWOLRefType',
            partssearchAll: '/Inventory/SearchWOPartsStockOnhand',
            createMaterials: '/Inventory/CreateNewWOLMaterial',
            searchLOV: '/Search/SearchListOfValue',
            searchSupplier: '/AIMS/SearchSuppierLOV',
            searchStaffList: '/Staff/SearchStaffLookUp'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} {2} {3}</a>",
            searchRowTemplate: '<span class="search-row-label fc-light-grey-2">{0}: <span class="search-row-data">{1}</span></span> &nbsp;&nbsp;',
            searchRowHeaderTemplateList: "<a href='#openModalInHouse' class='fs-medium-normal detailItem' data-infoPartID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liAddItems + ',' +thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;
        document.getElementById("LABOUR_DATE").disabled = true;
        document.getElementById("HOURS").disabled = true;
        document.getElementById("LABOUR_RATE").disabled = true;
        
        $("#searchStaffId").hide();
        $("#searchContract").hide();
        $("#searchSupplierId").hide();
    };

    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
 
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    thisApp.toggleDisplayModalMode = function () {

       
        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.searchAsset).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.liApiStatus).hide();

      
    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' +  thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
             thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("LABOUR_DATE").disabled = false;
        document.getElementById("HOURS").disabled = false;
        document.getElementById("LABOUR_RATE").disabled = false;
        

    };
    thisApp.toggleSearchItems = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        //$(thisApp.config.cls.statusText).text(systemText.searchModeStatuxText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();

    };
    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.liSave
        ).hide();

    };
    thisApp.toggleSearchPartsOnhand = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        //$(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        //$(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(thisApp.config.cls.liAddNew + ','+thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.searchAsset).show();
        $(thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.liApiStatus).hide();

        //$(
        //    thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
        //    thisApp.config.tag.textArea
        //).removeClass(thisApp.config.cssCls.viewMode);

        //$(
        //    thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        //).prop(thisApp.config.attr.disabled, false);

        //$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };
    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave).hide();
        document.getElementById("HOURS").disabled = true;
        document.getElementById("LABOUR_RATE").disabled = true;
       

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("LABOUR_DATE").disabled = false;
        document.getElementById("HOURS").disabled = false;
        document.getElementById("LABOUR_RATE").disabled = false;
        

    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
        document.getElementById("LABOUR_DATE").disabled = true;
        document.getElementById("HOURS").disabled = true;
        document.getElementById("LABOUR_RATE").disabled = true;
        
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

        $(
           thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
           thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);
        document.getElementById("LABOUR_DATE").disabled = true;
        document.getElementById("HOURS").disabled = true;
        document.getElementById("LABOUR_RATE").disabled = true;
        $("#searchStaffId").hide();
        $("#searchContract").hide();
        $("#searchSupplierId").hide();

		
		

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.getAge = function (birthDate) {

        var seconds = Math.abs(new Date() - birthDate) / 1000;

        var numyears = Math.floor(seconds / 31536000);
        var numdays = Math.floor((seconds % 31536000) / 86400);
        var nummonths = numdays / 30;

        return numyears + " y " + Math.round(nummonths) + " m ";
    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.formatJSONDateTimeToString = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
       
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0] + ' ' + strTime[1].substring(0, 5);


        return dateToFormat;
    };
    thisApp.formatJSONTimeToString = function () {

        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        var timeToFormat = strTime[1].substring(0, 5);


        return timeToFormat;
    };
    thisApp.formatJSONDateToString = function () {

        var dateToFormat = arguments[0];
        var strDate;

        if (dateToFormat === null) return "-";
        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };


    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');


            if (isDateTime) {
                val = thisApp.formatJSONDateTimeToString(val);
            }

            if (isDate) {
                val = thisApp.formatJSONDateToString(val);
            }

            var $el = $('#' + name), type = $el.attr('type'), isTime = $el.attr('data-istime');
            if (isTime) {
                if (val === null || val === "") {
                    val = "00:00";
                } else {
                    val = thisApp.formatJSONTimeToString(val);
                }

            }

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var amWorkOrderLabourViewModel = function (systemText) {

    var self = this;
    
    var ah = new appHelper(systemText);
    
    self.partsWLList = ko.observableArray("");
	self.optSupplier = ko.observable(false);
    self.optEmployee = ko.observable(false);
    self.optContract = ko.observable(false);
    var employeeInf = ko.observableArray([]);
    var supplierInf = ko.observableArray([]);
    var contractInf = ko.observableArray([]);
    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.toggleReadMode();
       
        ah.initializeLayout();
        assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);

        assetDescription = ah.getParameterValueByName(ah.config.fld.assetDescInfo);
        if (assetDescription.length > 70) {
            assetDescription = assetDescription.substring(0, 70) + '...';
        }
        if (assetNoID !== null && assetNoID !== "") {
            $("#infoID").text(assetNoID + "-" + assetDescription);


        }
        
        self.searchNow();
        //postData = { LOV: "'AIMS_WOLRESP'", STAFF_LOGON_SESSIONS: staffLogonSessions };
        //$.post(self.getApi() + ah.config.api.searchLOV, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.createNew = function () {

        ah.toggleAddMode();
        self.populateWO();
		self.optSupplier(false);
        self.optEmployee(false);
        self.optContract(false);

        var jDate = new Date();
        var strDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        var reqDate = strDate + ' ' + hourmin;

        $("#LABOUR_DATE").val(reqDate);

    };
    self.populateWO = function () {
        var assetwoReqNo = ah.getParameterValueByName(ah.config.fld.assetwoReqNo);
        $("#REQ_ID").val(assetwoReqNo);
        $("#REQ_BY").val(ah.getParameterValueByName(ah.config.fld.assetwoRequestor));
        $("#REQUESTER_CONTACT_NO").val(ah.getParameterValueByName(ah.config.fld.assetwoReqContact));
        $("#REQ_DATE").val(ah.getParameterValueByName(ah.config.fld.assetwoReqDate));
        $("#REQ_TYPE").val(ah.getParameterValueByName(ah.config.fld.assetwoReqType));
        $("#PROBLEM").val(ah.getParameterValueByName(ah.config.fld.assetwoProblem));
        $("#WO_STATUS").val(ah.getParameterValueByName(ah.config.fld.assetwoStatus));
    };
    self.modifySetup = function () {
        ah.toggleEditMode();
        var woLabourType = $("#LABOUR_TYPE").val();

        if (woLabourType === "In House") {
            $("#EMPLOYEE_NAME").attr('required', '');
         
            $("#CONTRACT_NAME").removeAttr('required');
            $("#SUPPLIER_ID").removeAttr('required');
            document.getElementById("EMPLOYEE_NAME").disabled = false;
            document.getElementById('EMPLOYEE_NAME').readOnly = true;
            document.getElementById("CONTRACT_NAME").disabled = true;
            document.getElementById("SUPPLIER_ID").disabled = true;
            document.getElementById('FLAT_FEE').disabled = true;
           
            document.getElementById('TRAVEL').disabled = true;
            
            $("#LABOUR_CHARGE").attr('required', '');
            document.getElementById('LABOUR_CHARGE').disabled = false;

        } else if (woLabourType === "Contract Service") {
            $("#CONTRACT_NAME").attr('required', '');
            $("#EMPLOYEE_NAME").removeAttr('required');
            $("#SUPPLIER_NAME").removeAttr('required');
            document.getElementById("EMPLOYEE_NAME").disabled = true;
            document.getElementById("CONTRACT_NAME").disabled = false;
            document.getElementById("SUPPLIER_NAME").disabled = true;
            document.getElementById('FLAT_FEE').disabled = false;
            
            document.getElementById('TRAVEL').disabled = false;
            var flatFee = $("#FLAT_FEE").val();
            if (flatFee === "YES") {
                document.getElementById('LABOUR_CHARGE').disabled = false;
                $("#LABOUR_CHARGE").attr('required', '');
                document.getElementById('HOURS').disabled = true;
               
                document.getElementById('LABOUR_RATE').disabled = true;
                $("#HOURS").removeAttr('required');
                
                $("#LABOUR_RATE").removeAttr('required');
            } else {
                $("#LABOUR_CHARGE").removeAttr('required');
                $("#HOURS").attr('required', '');
               
                $("#LABOUR_RATE").attr('required', '');
                document.getElementById('LABOUR_CHARGE').disabled = true;
                document.getElementById('HOURS').disabled = false;
                
                document.getElementById('LABOUR_RATE').disabled = false;
            }
        } else if (woLabourType === "Non-Contract Service") {
            $("#SUPPLIER_NAME").attr('required', '');
            $("#CONTRACT_NAME").removeAttr('required');
            $("#EMPLOYEE_NAME").removeAttr('required');
            document.getElementById("EMPLOYEE_NAME").disabled = true;
            document.getElementById("CONTRACT_NAME").disabled = true;
            document.getElementById("SUPPLIER_NAME").disabled = false;
            document.getElementById('FLAT_FEE').disabled = false;
           
            document.getElementById('TRAVEL').disabled = false;
            var flatFee = $("#FLAT_FEE").val();
            if (flatFee === "YES") {
                document.getElementById('LABOUR_CHARGE').disabled = false;
                $("#LABOUR_CHARGE").attr('required', '');
                document.getElementById('HOURS').disabled = true;
                
                document.getElementById('LABOUR_RATE').disabled = true;
                $("#HOURS").removeAttr('required');
               
                $("#LABOUR_RATE").removeAttr('required');
            } else {
                $("#LABOUR_CHARGE").removeAttr('required');
                $("#HOURS").attr('required', '');
               
                $("#LABOUR_RATE").attr('required', '');
                document.getElementById('LABOUR_CHARGE').disabled = true;
                document.getElementById('HOURS').disabled = false;
                
                document.getElementById('LABOUR_RATE').disabled = false;
            }
        } else {
            document.getElementById("EMPLOYEE_NAME").disabled = true;
            document.getElementById("CONTRACT_NAME").disabled = true;
            document.getElementById("SUPPLIER_NAME").disabled = true;
        }

    };
    self.pageLoader = function () {
        var senderID = $(arguments[1].currentTarget).attr('id');

        var searchStr = $(ah.config.id.txtGlobalSearchOth).val().toString();

        var pageNum = parseInt($(ah.config.id.pageNum).val());
        var pageCount = parseInt($(ah.config.id.pageCount).val());
        if (isNaN(pageCount)) {
            pageCount = 1;
        }
        if (isNaN(pageNum)) {
            pageNum = 0;
        }

        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.refList));
        var lastPage = Math.ceil(sr.length / 10);

        switch (senderID) {

            case ah.config.tagId.nextPage: {
                if (pageCount < lastPage) {
                    pageNum = pageNum + 10;
                    pageCount++;
                    self.searchLOVResult(searchStr, pageNum);
                }
                break;
            }
            case ah.config.tagId.priorPage: {

                if (pageNum === 0) {
                } else {
                    pageNum = pageNum - 10;
                    if (pageNum < 0) {
                        pageNum = 0;
                    }
                    pageCount = pageCount - 1;
                }

                self.searchLOVResult(searchStr, pageNum);
                break;
            }

            case "txtGlobalSearchOth": {
                pageCount = 1;
                pageNum = 0;

                self.searchLOVResult(searchStr, 0);
                break;
            }
        }

        $(ah.config.id.loadNumber).text(pageCount.toString());
        $(ah.config.id.pageCount).val(pageCount.toString());
        $(ah.config.id.pageNum).val(pageNum);

    };
    self.pageLoader1 = function () {
        var senderID = $(arguments[1].currentTarget).attr('id');

        var searchStr = $("#txtGlobalSearchOth1").val().toString();

        var pageNum = parseInt($("#pageNum1").val());
        var pageCount = parseInt($("#pageCount1").val());

        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.partsOnhandList));
        var lastPage = Math.ceil(sr.length / 10);
        if (isNaN(pageCount)) {
            pageCount = 1;
        }
        if (isNaN(pageNum)) {
            pageNum = 0;
        }

        switch (senderID) {

            case ah.config.tagId.nextPage+"1": {
                if (pageCount < lastPage) {
                    pageNum = pageNum + 10;
                    pageCount++;
                    self.searchPartsOnhandResult(searchStr, pageNum);
                }
                break;
            }
            case ah.config.tagId.priorPage+"1": {

                if (pageNum === 0) {
                } else {
                    pageNum = pageNum - 10;
                    if (pageNum < 0) {
                        pageNum = 0;
                    }
                    pageCount = pageCount - 1;
                }

                self.searchPartsOnhandResult(searchStr, pageNum);
                break;
            }

            case ah.config.tagId.txtGlobalSearchOth+"1": {
                pageCount = 1;
                pageNum = 0;

                self.searchPartsOnhandResult(searchStr, 0);
                break;
            }
        }

		$("#loadNumber1").text(pageCount.toString());
        $("#pageCount1").val(pageCount.toString());
        $("#pageNum1").val(pageNum);

    };
    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };
    self.addWOPart = function () {
        var pop = ah.getParameterValueByName(ah.config.fld.keyAccess);
        if (pop === "xchvisjfkkewiorusjcxkvjsdfervcvsdfwer") {
            return;
        }
        var worequestId = $("#REQ_ID").val();
        var wo_LabourType = $("#LABOUR_TYPE").val();
        var wo_LabourId = $("#AM_WORKORDER_LABOUR_ID").val();
        var requestId = $("#REQUEST_ID").val();
        var assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);

        if (self.optEmployee()) {
            var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfoPartID));
            // var pmScheduleId = $("#AM_ASSET_PMSCHEDULE_ID").val();

            var existPart = self.checkExist(infoID.PRODUCT_CODE);
            if (existPart > 0) {
                alert('Unable to Proceed. The selected part was already exist in Work Order Labour Materials.');
                return;
            }

            if (infoID.PRODUCT_DESC.length > 60)
                infoID.PRODUCT_DESC = infoID.PRODUCT_DESC.substring(0, 59) + "...";

            if (infoID.STORE_DESC.length > 40)
                infoID.STORE_DESC = infoID.STORE_DESC.substring(0, 39) + "...";

            self.partsWLList.push({
                PRODUCT_DESC: infoID.PRODUCT_DESC,
                DESCRIPTION: infoID.DESCRIPTION,
                ID_PARTS: infoID.PRODUCT_CODE,
                STORE_DESC: infoID.STORE_DESC,
                STORE_ID: infoID.STORE_ID,
                BILLABLE: false,
                PRICE: infoID.PART_COST,
                REQUEST_ID: requestId,
                STATUSREQ: 'Request',
                TRANSFER_QTY: 0,
                EXTENDED: 0,
                STORE_ID: infoID.STORE,
                REF_WO: worequestId,
                WO_LABOURTYPE: wo_LabourType,
                STATUS: 'Request',
                REF_ASSETNO: assetNoID,
                AM_WORKORDER_LABOUR_ID: wo_LabourId,
                QTY: 0
            });
        }
        else {
            self.partsWLList.push({
                PRODUCT_DESC: "",
                DESCRIPTION: "",
                ID_PARTS: "",
                STORE_DESC: "",
                STORE_ID: 0,
                BILLABLE: false,
                PRICE: 0,
                REQUEST_ID: requestId,
                STATUSREQ: 'REQUEST',
                TRANSFER_QTY: 0,
                EXTENDED: 0,
                REF_WO: worequestId,
                WO_LABOURTYPE: wo_LabourType,
                STATUS: 'REQUEST',
                REF_ASSETNO: assetNoID,
                AM_WORKORDER_LABOUR_ID: wo_LabourId,
                QTY: 0
            });
        }
        
 
    };
    self.flagFeeOnChange = function () {
        $("#LABOUR_RATE").val(null);
        $("#TOTAL_COST").val(null);
        $("#HOURS").val(null);
        $("#LABOUR_CHARGE").val(null);
        var flatFee = $("#FLAT_FEE").val();
        if (flatFee === "YES") {
            document.getElementById('LABOUR_CHARGE').disabled = false;
            $("#LABOUR_CHARGE").attr('required', '');
            document.getElementById('HOURS').disabled = true;
            
            document.getElementById('LABOUR_RATE').disabled = true;
            $("#HOURS").removeAttr('required');
           
            $("#LABOUR_RATE").removeAttr('required');
        } else {
            $("#LABOUR_CHARGE").removeAttr('required');
            $("#HOURS").attr('required', '');
            
            $("#LABOUR_RATE").attr('required', '');
            document.getElementById('LABOUR_CHARGE').disabled = true;
            document.getElementById('HOURS').disabled = false;
          
            document.getElementById('LABOUR_RATE').disabled = false;
        }
    };
    self.checkExist = function (idPart) {

        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.partsWLList);
        var partLists = JSON.parse(jsonObj);

        var partExist = partLists.filter(function (item) { return item.ID_PARTS === idPart });

        return partExist.length;
    };
    self.returnWO = function () {
        var reqWoNO = ah.getParameterValueByName(ah.config.fld.assetwoReqNo);
        var assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        var reqWOActionxd = ah.getParameterValueByName(ah.config.fld.reqWOAction);

        
        if (reqWOActionxd === "" || reqWOActionxd === null || reqWOActionxd === " ") {
           
        } else {
           
            assetNoID = "";
        }

        var dashBoard = ah.getParameterValueByName(ah.config.fld.fromDashBoard);
        var dsfsdf = "";
        if (dashBoard == "dsdxsesesdfsadfsadfqwerqwerqwersadfsadfasdfasdfsadfsadfsadfasdf") {
            dsfsdf = "dsdxsesesdfsadfsadfqwerqwerqwersadfsadfasdfasdfsadfsadfsadfasdf";
        }
        var pop = ah.getParameterValueByName(ah.config.fld.keyAccess);
        if (pop === "xchvisjfkkewiorusjcxkvjsdfervcvsdfwer") {
            window.location.href = ah.config.url.aimsWorkOrderForClose + "?REQNO=" + reqWoNO + "&ASSETNO=" + '' + "&REQWOAC=" + reqWOActionxd + "&asdfxhsadfsdfx=" + dsfsdf;
        } else {
            window.location.href = ah.config.url.aimsWorkOrder + "?REQNO=" + reqWoNO + "&ASSETNO=" + '' + "&REQWOAC=" + reqWOActionxd + "&asdfxhsadfsdfx=" + dsfsdf;
        }
       
    };
    self.searchrefTypeList = function () {
        //var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        //var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        //var postData;
        
        //ah.CurrentMode = ah.config.mode.searchDropdown;
        
        var labourType = $("#LABOUR_TYPE").val();
        var searchType = '';
        if (labourType === 'In House') {
            searchType = "STAFF";
        } else if (labourType === 'Contract Service') {
            searchType = "CONTRACT";
        } else if (labourType === 'Non-Contract Service') {
            searchType = "SUPPLIER";
        }
        window.location.href = ah.config.url.openModal;
        //postData = { SEARCH: searchType, STAFF_LOGON_SESSIONS: staffLogonSessions };
        //$.post(self.getApi() + ah.config.api.searchwolRefTypeList, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    }
    self.showOptions = function () {
        var woLabourType = $("#LABOUR_TYPE").val();

        if (woLabourType === "In House") {
            self.optSupplier(false);
            self.optEmployee(true);
            self.optContract(false);
        } else if (woLabourType === "Contract Service") {
            self.optSupplier(false);
            self.optEmployee(false);
            self.optContract(true);
        } else if (woLabourType === "Non-Contract Service") {
            self.optSupplier(true);
            self.optEmployee(false);
            self.optContract(false);
        }
    } 
    self.woLabourAction = function () {
		self.showOptions();		   
       // $("#EMPLOYEE").val(null);
       // $("#CONTRACT_NO").val(null);
       // $("#SUPPLIER_ID").val(null);
        var woLabourType = $("#LABOUR_TYPE").val();
        //$("#LABOUR_RATE").val(null);
        //$("#TOTAL_COST").val(null);
        //$("#HOURS").val(null);
        //$("#LABOUR_CHARGE").val(null);
        if (woLabourType === "In House") {

            var empData = $("#EMPLOYEE").val();
            if (empData == null || empData == "") {
                var assignToPerson = ah.getParameterValueByName(ah.config.fld.assetwoAssignTo);
              
                $("#EMPLOYEE").val(assignToPerson);
                var assignToPersonDesc = ah.getParameterValueByName(ah.config.fld.assetAssignDesc);
                $("#EMPLOYEE_NAME").val(assignToPersonDesc);
            }

            $("#EMPLOYEE_NAME").attr('required', '');
           
            $("#CONTRACT_NAME").removeAttr('required');
            $("#SUPPLIER_NAME").removeAttr('required');
            document.getElementById("EMPLOYEE_NAME").disabled = false;
            document.getElementById('EMPLOYEE_NAME').readOnly = true;
            document.getElementById("CONTRACT_NAME").disabled = true;
            document.getElementById("SUPPLIER_NAME").disabled = true;
           // $("#CONTRACT_NAME").val("");
           // $("#SUPPLIER_NAME").val("");
           
            document.getElementById('FLAT_FEE').disabled = true;
            
            document.getElementById('TRAVEL').disabled = true;
           
            document.getElementById('LABOUR_CHARGE').disabled = false;
            //$("#LABOUR_CHARGE").attr('required', '');

            $("#searchStaffId").show();
            $("#searchContract").hide();
            $("#searchSupplierId").hide();
        } else if (woLabourType === "Contract Service") {
            $("#CONTRACT_NAME").attr('required', '');
            $("#EMPLOYEE_NAME").removeAttr('required');
            $("#SUPPLIER_NAME").removeAttr('required');
            document.getElementById("EMPLOYEE_NAME").disabled = true;
            document.getElementById('CONTRACT_NAME').readOnly = true;
            document.getElementById("CONTRACT_NAME").disabled = false;
            document.getElementById("SUPPLIER_NAME").disabled = true;
           // $("#EMPLOYEE_NAME").val("");
           // $("#SUPPLIER_NAME").val("");
            
            document.getElementById('FLAT_FEE').disabled = false;
            
            document.getElementById('TRAVEL').disabled = false;
            var flatFee = $("#FLAT_FEE").val();
            if (flatFee === "YES") {
                document.getElementById('LABOUR_CHARGE').disabled = false;
                $("#LABOUR_CHARGE").attr('required', '');
                document.getElementById('HOURS').disabled = true;
                
                document.getElementById('LABOUR_RATE').disabled = true;
                $("#HOURS").removeAttr('required');
                
                $("#LABOUR_RATE").removeAttr('required');
            } else {
                $("#LABOUR_CHARGE").removeAttr('required');
                $("#HOURS").attr('required', '');
               
                $("#LABOUR_RATE").attr('required', '');
                document.getElementById('LABOUR_CHARGE').disabled = true;
                document.getElementById('HOURS').disabled = false;
                
                document.getElementById('LABOUR_RATE').disabled = false;
                $("#searchStaffId").hide();
                $("#searchContract").show();
                $("#searchSupplierId").hide();
            }
        } else if (woLabourType === "Non-Contract Service") {
            $("#SUPPLIER_NAME").attr('required', '');
            $("#CONTRACT_NAME").removeAttr('required');
            $("#EMPLOYEE_NAME").removeAttr('required');
            document.getElementById("EMPLOYEE_NAME").disabled = true;
            document.getElementById('SUPPLIER_NAME').readOnly = true;
            document.getElementById("CONTRACT_NAME").disabled = true;
            document.getElementById("SUPPLIER_NAME").disabled = false;
           
           
            document.getElementById('FLAT_FEE').disabled = false;
            
            document.getElementById('TRAVEL').disabled = false;
            var flatFee = $("#FLAT_FEE").val();
            if (flatFee === "YES") {
                document.getElementById('LABOUR_CHARGE').disabled = false;
                $("#LABOUR_CHARGE").attr('required', '');
                document.getElementById('HOURS').disabled = true;
               
                document.getElementById('LABOUR_RATE').disabled = true;
                $("#HOURS").removeAttr('required');
             
                $("#LABOUR_RATE").removeAttr('required');
            } else {
                $("#LABOUR_CHARGE").removeAttr('required');
                $("#HOURS").attr('required', '');
                
                $("#LABOUR_RATE").attr('required', '');
                document.getElementById('LABOUR_CHARGE').disabled = true;
                document.getElementById('HOURS').disabled = false;
                
                document.getElementById('LABOUR_RATE').disabled = false;
            }
            $("#searchStaffId").hide();
            $("#searchContract").hide();
            $("#searchSupplierId").show();
        } else {
            document.getElementById("EMPLOYEE_NAME").disabled = true;
            document.getElementById("CONTRACT_NAME").disabled = true;
            document.getElementById("SUPPLIER_NAME").disabled = true;
            $("#searchStaffId").hide();
            $("#searchContract").hide();
            $("#searchSupplierId").hide()
            self.optSupplier(false);
            self.optEmployee(false);
            self.optContract(false);
        }
    }
    self.saveAllChanges = function () {

        $(ah.config.id.saveInfo).trigger('click');

    };
    self.infoLovLoader = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        var senderID = $(arguments[1].currentTarget).attr('id');
        $("#searchingLOVList").hide();
        $("#lookUpnoresult").hide();
        $("#searchAllLOVList").hide();
        $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');
        var searchStr = $(ah.config.id.lookUptxtGlobalSearchOth).val();
    
        switch (senderID) {
            case ah.config.tagId.searchSupplierId: {
                $(ah.config.id.lookUptxtGlobalSearchOth).val("");
                $("#LOVBYACION").val("SUPPIER");
                $("#searchAllLOVList").show();
                $("#lookUpTitle").text("Please search/select from active supplier list");
                break;
            }
            case ah.config.tagId.searchStaffId: {
                $(ah.config.id.lookUptxtGlobalSearchOth).val("");
                $("#lookUpTitle").text("Please select from active staff list");
                $("#LOVBYACION").val("STAFF");
                postData = { SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
                $.post(self.getApi() + ah.config.api.searchStaffList, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
                break;
            }
            case ah.config.tagId.lookUptxtGlobalSearchOth: {
                var lovByAction = $("#LOVBYACION").val();
                if (lovByAction == "SUPPIER") {
                    if (supplierListLOV.length > 0) {
                        self.searchResultSupplierListLov(searchStr);
                    } else {
                        self.supplierSearch();
                    }

                } else if (lovByAction == "STAFF") {
                    self.searchResultStaffListLov(searchStr);
                }
                break;
            }


        }
        window.location.href = ah.config.url.openModalLookup;
    };
    self.searchPartsOnhandResult = function (LovCategory, pageLoad) {

        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.partsOnhandList));
        
        if (LovCategory !== null && LovCategory !== "" && LovCategory !== "LOAD") {
            var arr = sr;
            var keywords = LovCategory.toUpperCase();
            var filtered = [];

            for (var i = 0; i < sr.length; i++) {
                for (var j = 0; j < keywords.length; j++) {
                    if ((sr[i].PRODUCT_CODE.toUpperCase()).indexOf(keywords) > -1 || (sr[i].PRODUCT_DESC.toUpperCase()).indexOf(keywords) > -1 || (sr[i].STORE_DESC.toUpperCase()).indexOf(keywords) > -1) {
                        filtered.push(sr[i]);
                        break;
                    }
                }
            }
            sr = filtered;
        }
        
        $(ah.config.id.searchResultPartsOnhand + ' ' + ah.config.tag.ul).html('');

    

        if (sr.length > 0) {
            $("#noresult1").hide();
            $("#pageSeparator1").show();
            $("#page11").show();
            $("#page21").show();
            $("#nextPage1").show();
            $("#priorPage1").show();
            var totalrecords = sr.length;
            var pagenum = Math.ceil(totalrecords / 10);
            $("#totNumber1").text(pagenum.toString());

            if (pagenum < 2 && pagenum > 1) {
                pagenum = 2;
            }
            $("#totNumber1").text(pagenum.toString());

            if (pagenum <= 1) {
                $("#page11").hide();
                $("#page21").hide();
                $("#pageSeparator1").hide();
                $("#nextPage1").hide();
                $("#priorPage1").hide();

            }
            var htmlstr = '';
            var o = pageLoad;
            //alert(JSON.stringify(sr));
            for (var i = 0; i < 10; i++) {
                var partDescription = sr[o].PRODUCT_DESC;
                if (partDescription.length > 50) {
                    partDescription = partDescription.substring(0, 50) + '...';
                }
                // alert(partDescription);
                if (sr[o].PART_COST === null || sr[o].PART_COST === "") {
                    sr[o].PART_COST = 0;
                }
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplateList, JSON.stringify(sr[o]), sr[o].PRODUCT_CODE, partDescription, '');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.storeDesc, sr[o].STORE_DESC);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.availableStock, sr[o].AVAILABLE_STOCK);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.partCost, sr[o].PART_COST);

                htmlstr += '</li>';
                o++
                if (totalrecords === o) {
                    break;
                }
            }

            $(ah.config.id.searchResultPartsOnhand + ' ' + ah.config.tag.ul).append(htmlstr);
        }
        else {
            $("#page11").hide();
            $("#page21").hide();
            $("#pageSeparator1").hide();
            $("#nextPage1").hide();
            $("#priorPage1").hide();
            $("#noresult1").show();
        }

            

        $(ah.config.cls.detailItem).bind('click', self.addWOPart);
        if (LovCategory === "LOAD") {
            var wopmPartsr = JSON.parse(sessionStorage.getItem(ah.config.skey.woMaterials));
            
            if (wopmPartsr.length > 0) {
                self.partsWLList.splice(0, 5000);
                $("#REQUEST_ID").val(wopmPartsr[0].REQUEST_ID);
                for (var o in wopmPartsr) {
                    var billable = true;
                    if (wopmPartsr[o].BILLABLE === "false") {
                        billable = false;
                    }

                    if (wopmPartsr[o].PRODUCT_DESC != null && wopmPartsr[o].PRODUCT_DESC.length > 60)
                        wopmPartsr[o].PRODUCT_DESC = wopmPartsr[o].PRODUCT_DESC.substring(0, 59) + "..."
                    if (wopmPartsr[o].WO_LABOURTYPE.toString() == $("#LABOUR_TYPE").val()) {
                        self.partsWLList.push({
                            PRODUCT_DESC: wopmPartsr[o].PRODUCT_DESC,
                            DESCRIPTION: wopmPartsr[o].DESCRIPTION,
                            ID_PARTS: wopmPartsr[o].ID_PARTS,
                            STORE_DESC: wopmPartsr[o].STORE_DESC,
                            STORE_ID: wopmPartsr[o].STORE_ID,
                            BILLABLE: billable,
                            PRICE: wopmPartsr[o].PRICE,
                            STATUSREQ: wopmPartsr[o].STATUS,
                            TRANSFER_QTY: wopmPartsr[o].TRANSFER_QTY,
                            TRANSFER_FLAG: wopmPartsr[o].TRANSFER_FLAG,
                            EXTENDED: wopmPartsr[o].EXTENDED,
                            REQUEST_ID: wopmPartsr[o].REQUEST_ID,
                            REF_WO: wopmPartsr[o].REF_WO,
                            WO_LABOURTYPE: wopmPartsr[o].WO_LABOURTYPE,
                            STATUS: wopmPartsr[o].STATUS,
                            QTY_RETURN: wopmPartsr[o].QTY_RETURN,
                            QTY_FORRETURN: wopmPartsr[o].QTY_FORRETURN,
                            REF_ASSETNO: wopmPartsr[o].REF_ASSETNO,
                            AM_WORKORDER_LABOUR_ID: wopmPartsr[o].AM_WORKORDER_LABOUR_ID,
                            QTY: wopmPartsr[o].QTY
                        });
                    }
                   
                }
            }
        }
        ah.toggleSearchPartsOnhand();
        var reqWoStatus = ah.getParameterValueByName(ah.config.fld.assetwoStatus);
        if (reqWoStatus === 'CL') {
            $(ah.config.cls.liAddNew + ',' + ah.config.id.modifyButton + ',' + ah.config.id.addButtonModal).hide();
        }
        var pop = ah.getParameterValueByName(ah.config.fld.keyAccess);
        if (pop === "xchvisjfkkewiorusjcxkvjsdfervcvsdfwer") {
            $(ah.config.cls.liAddNew + ',' + ah.config.id.modifyButton + ',' + ah.config.id.addButtonModal).hide();
            $(
                ah.config.tag.inputTypeText + ',' + ah.config.tag.inputTypeNumber + ',' + ah.config.tag.inputTypeEmail + ',' + ah.config.tag.select + ',' +
                ah.config.tag.textArea
            ).addClass(ah.config.cssCls.viewMode);

            $(
                ah.config.tag.inputTypeText + ',' + ah.config.tag.inputTypeNumber + ',' + ah.config.tag.inputTypeCheckbox + ',' + ah.config.tag.inputTypeEmail + ',' + ah.config.tag.textArea
            ).prop(ah.config.attr.disabled, true);

            $(ah.config.tag.select).prop(ah.config.attr.disabled, true);
        }
    };
    self.searchResultStaffListLov = function (LovCategory) {
        var sr = staffListLOV;
        if (LovCategory !== null && LovCategory !== "") {
            var arr = sr;
            var keywords = LovCategory.toUpperCase();
            var filtered = [];

            for (var i = 0; i < sr.length; i++) {
                for (var j = 0; j < keywords.length; j++) {
                    if ((sr[i].FIRST_NAME.toUpperCase()).indexOf(keywords) > -1 || (sr[i].LAST_NAME.toUpperCase()).indexOf(keywords) > -1) {
                        filtered.push(sr[i]);
                        break;
                    }
                }
            }
            sr = filtered;
        }
        //alert(JSON.stringify(sr));
        $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');

        if (sr.length > 0) {
            var htmlstr = "";
            for (var o in sr) {

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, JSON.stringify(sr[o]), "", sr[o].FIRST_NAME, sr[o].LAST_NAME);

                htmlstr += '</li>';

            }
            // alert(htmlstr);
            $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).append(htmlstr);
            $(ah.config.cls.detailItem).bind('click', self.populateAssignTo);
        }

    };
    self.populateAssignTo = function () {

        var reqID = JSON.parse(this.getAttribute(ah.config.attr.datainfoId).toString());
        $("#EMPLOYEE_NAME").val(reqID.FIRST_NAME + ' ' + reqID.LAST_NAME);
        $("#EMPLOYEE").val(reqID.STAFF_ID);


    }
    self.searchResultSupplierListLov = function (LovCategory) {
        var sr = supplierListLOV;

        if (LovCategory !== null && LovCategory !== "") {
            var arr = sr;
            var keywords = LovCategory.toUpperCase();
            var filtered = [];

            for (var i = 0; i < sr.length; i++) {
                for (var j = 0; j < keywords.length; j++) {
                    if ((sr[i].DESCRIPTION.toUpperCase()).indexOf(keywords) > -1 || (sr[i].SUPPLIER_ID.toUpperCase()).indexOf(keywords) > -1) {
                        filtered.push(sr[i]);
                        break;
                    }
                }
            }
            sr = filtered;
        }

        //alert(JSON.stringify(sr));
        $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');

        if (sr.length > 0) {
            var htmlstr = "";
            for (var o in sr) {

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, JSON.stringify(sr[o]), "", sr[o].DESCRIPTION, "");

                htmlstr += '</li>';

            }
            // alert(htmlstr);
            $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).append(htmlstr);
            $(ah.config.cls.detailItem).bind('click', self.populateAssignOutsource);
        }

    };
    self.populateAssignOutsource = function () {

        var reqID = JSON.parse(this.getAttribute(ah.config.attr.datainfoId).toString());
        $("#SUPPLIER_NAME").val(reqID.DESCRIPTION);
        $("#SUPPLIER_ID").val(reqID.SUPPLIER_ID);


    }
    self.cancelChanges = function () {
        $("#searchStaffId").hide();
        $("#searchContract").hide();
        $("#searchSupplierId").hide();
        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
            self.searchNow();
        } else {
            ah.toggleDisplayMode();
            self.showDetails(true);
        }

    };
    self.partsSearchNow = function () {
        
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        ah.toggleSearchItems();
        var wo_LabourId = $("#AM_WORKORDER_LABOUR_ID").val();
        postData = { SEARCH: "", WORKORDER_LABOUR_ID:wo_LabourId,  STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.partssearchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = encodeURI(ah.config.url.modalAddItems);
        $("#loadNumber1").text(1);
    };
    self.showDetails = function (isNotUpdate) {

        if (isNotUpdate) {

            var infoDetails = sessionStorage.getItem(ah.config.skey.infoDetails);
           
            var jsetupDetails = JSON.parse(infoDetails);


            ah.ResetControls();

            var reqDate = ah.formatJSONDateTimeToString(jsetupDetails.LABOUR_DATE);
            jsetupDetails.LABOUR_DATE = reqDate;
            ah.LoadJSON(jsetupDetails);
           
           
            if (jsetupDetails.LABOUR_DATE == "" || jsetupDetails.LABOUR_DATE == "null" || jsetupDetails.LABOUR_DATE == "0001-01-01T00:00:00" || jsetupDetails.LABOUR_DATE == "1900-01-01T00:00:00") {

                $("#LABOUR_DATE").val("");
            }


           


          

           // var woinfoDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.wolLookup));
            
                if (employeeInf.length > 0) {
                    $("#EMPLOYEE").val(employeeInf[0].REF_ID);
                    $("#EMPLOYEE_NAME").val(employeeInf[0].REF_DESC);
                }
               
                if (supplierInf.length > 0) {
                    $("#SUPPLIER_ID").val(supplierInf[0].REF_ID);
                    $("#SUPPLIER_NAME").val(supplierInf[0].REF_DESC);
                }
                if (contractInf.length > 0) {
                    $("#CONTRACT_NO").val(contractInf[0].REF_ID);
                    $("#CONTRACT_NAME").val(contractInf[0].REF_DESC);
                }
            
        }
        
        self.populateWO();
        ah.toggleDisplayMode();
        self.showOptions();	
        var reqWoStatus = ah.getParameterValueByName(ah.config.fld.assetwoStatus);
        if (reqWoStatus === 'CL') {
            $(ah.config.cls.liAddNew + ',' + ah.config.id.modifyButton + ',' + ah.config.id.addButtonModal).hide();
        }
        var pop = ah.getParameterValueByName(ah.config.fld.keyAccess);
        if (pop === "xchvisjfkkewiorusjcxkvjsdfervcvsdfwer") {
            $(ah.config.cls.liAddNew + ',' + ah.config.id.modifyButton + ',' + ah.config.id.addButtonModal).hide();
        }
        if (jsetupDetails.LABOUR_TYPE === 'In House') {
            $(ah.config.cls.liAddItems).hide();

        } else {
            $(ah.config.cls.liAddItems).show();
        }
    };
    self.materialSaveAllChanges = function () {

        $(ah.config.id.saveMaterialInfo).trigger('click');

    };
    self.returnHome = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.searchResult = function (jsonData) {


        var sr = jsonData.AM_WORKORDER_LABOUR;
     //   alert(JSON.stringify(jsonData.AM_WORKORDER_LABOUR));
        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';
            var labourType = '';
            for (var o in sr) {
                if (sr[o].RESPONSE === null) {
                    sr[o].RESPONSE = "";
                }
                
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].AM_WORKORDER_LABOUR_ID, '', sr[o].LABOUR_TYPE, '');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.responsible, sr[o].REF_DESC) + '&nbsp;&nbsp;';
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.actionInfo, sr[o].LABOUR_ACTION) + '&nbsp;&nbsp;';
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.response, sr[o].RESPONSE_NAME) +'&nbsp;&nbsp;';
                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.detailItem).bind('click', self.readSetupID);
        ah.displaySearchResult();
        var reqWoStatus = ah.getParameterValueByName(ah.config.fld.assetwoStatus);
        if (reqWoStatus === 'CL') {
            $("#STATUSACTION").val("LOCKED");
            $(ah.config.cls.liAddNew).hide();
        }
        var pop = ah.getParameterValueByName(ah.config.fld.keyAccess);
        if (pop === "xchvisjfkkewiorusjcxkvjsdfervcvsdfwer") {
            $(ah.config.cls.liAddNew).hide();
        }
    };
    self.searchLOVResult = function (LovCategory, pageLoad) {

        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.refList));

        if (LovCategory !== null && LovCategory !== "") {
            var arr = sr;
            var keywords = LovCategory.toUpperCase();
            var filtered = [];

            for (var i = 0; i < sr.length; i++) {
                for (var j = 0; j < keywords.length; j++) {
                    if ((sr[i].REF_DESC.toUpperCase()).indexOf(keywords) > -1 || (sr[i].REF_3.toUpperCase()).indexOf(keywords) > -1) {
                        filtered.push(sr[i]);
                        break;
                    }
                }
            }
            sr = filtered;
        }

        $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).html('');
       
        if (sr.length > 0) {

 
            var labourType = $("#LABOUR_TYPE").val();
            if (labourType === 'Contract Service') {
                var assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
                sr = sr.filter(function (item) { return item.REF_3 === assetNoID });
            
            }

            $("#noresult").hide();
            $("#pageSeparator").show();
            $("#page1").show();
            $("#page2").show();
            $("#nextPage").show();
            $("#priorPage").show();
            var totalrecords = sr.length;
            var pagenum = Math.ceil(totalrecords / 10);
            $("#totNumber1").text(pagenum.toString());
            if (pagenum < 2 && pagenum > 1) {
                pagenum = 2;
            }
            $(ah.config.id.totNumber).text(pagenum.toString());

            if (pagenum <= 1) {
                $("#noresult").hide();
                $("#pageSeparator").hide();
                $("#page1").hide();
                $("#page2").hide();
                $("#nextPage").hide();
                $("#priorPage").hide();
            }
            var htmlstr = '';
            var o = pageLoad;

            for (var i = 0; i < 10; i++) {
               
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].REF_ID, sr[o].REF_DESC,"" );
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.contactNo, sr[o].REF_1) + '&nbsp;&nbsp;';
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.emailAdd, sr[o].REF_2 + '&nbsp;&nbsp;');
                htmlstr += '</li>';
                o++
                if (totalrecords === o) {
                    break;
                }
            }
            //alert(JSON.stringify(sr));
            //alert(htmlstr);
            $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).append(htmlstr);
        }
        else {
            $("#noresult").show();
            $("#pageSeparator").hide();
            $("#page1").hide();
            $("#page2").hide();
            $("#nextPage").hide();
            $("#priorPage").hide();
        }

        $(ah.config.cls.detailItem).bind('click', self.populaterefInfo);
        ah.toggleDisplayModalMode();
        var reqWoStatus = ah.getParameterValueByName(ah.config.fld.assetwoStatus);
        if (reqWoStatus === 'CL') {
            $(ah.config.cls.liAddNew + ',' + ah.config.id.modifyButton + ',' + ah.config.id.addButtonModal).hide();
        }
    };
    self.readSetupID = function () {

        var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { REF_ID: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.supplierSearch = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        postData = { SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchSupplier, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.populaterefInfo = function () {
        var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
       
        infoID = JSON.parse(infoID);
        
        var refName = infoID.REF_DESC;
        var labourType = $("#LABOUR_TYPE").val();
        if (labourType === 'In House') {
            $("#EMPLOYEE_NAME").val(refName);
            $("#EMPLOYEE").val(infoID.REF_ID);
        } else if (labourType === 'Contract Service') {
            $("#CONTRACT_NAME").val(refName);
            $("#CONTRACT_NO").val(infoID.REF_ID);
        } else if (labourType === 'Non-Contract Service') {
            $("#SUPPLIER_NAME").val(refName);
            $("#SUPPLIER_ID").val(infoID.REF_ID);
        }

        
    };
    self.removeItem = function (partsWLList) {
        self.partsWLList.remove(partsWLList);
    };
    self.calculateTotalCost = function () {
        var labourType = $("#LABOUR_TYPE").val();
        var flagFee = $("#FLAT_FEE").val();
        var labourCharge = $("#LABOUR_CHARGE").val();
        var labourRate = $("#LABOUR_RATE").val();
        var labourHour = $("#HOURS").val();

        if (labourType === 'In House') {
            $("#TOTAL_COST").val(labourCharge);
        } else {
            if (flagFee === "YES") {
                $("#TOTAL_COST").val(labourCharge);
            } else {
                var totalCost = labourHour * labourRate;
                $("#TOTAL_COST").val(totalCost);
            }
        }

    };
    self.isDateFutre = function (date1, date2) {
        if (date1 < date2) {
            return true;
        }
        return false;

    }
    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();

        var assetwoReqNo = ah.getParameterValueByName(ah.config.fld.assetwoReqNo);
        //alert(assetwoReqNo);
        postData = { LOV: "'AIMS_WOLRESP'", SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), REQNO: assetwoReqNo, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.saveWOLMInfo = function () {

      

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var itemDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfoParts)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.partsWLList);


        var wolParts = JSON.parse(jsonObj);
        

        for (var i = 0; i < wolParts.length; i++) {
            if (wolParts.STATUS === 'Request') {
                if (wolParts[i].QTY < 1 && self.optEmployee()) {
                    return alert("Qty must be greater than 0.");
                }
                else if (wolParts[i].DESCRIPTION == "" && !self.optEmployee()) {
                    return alert("Description must have a value.");
                }
            }

               
            }


        if (wolParts.length <= 0) {
            return;
        }
        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        strDateStart = strDateStart + ' ' + hourmin;

        postData = { CREATE_DATE: strDateStart, AM_PARTSREQUEST: wolParts, ACTION:"", STAFF_LOGON_SESSIONS: staffLogonSessions };
       // alert(JSON.stringify(wolParts));
       

       // ah.toggleSaveMode();
        $.post(self.getApi() + ah.config.api.createMaterials, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.saveInfo = function () {

        
        self.calculateTotalCost();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var setupDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        setupDetails.CREATED_BY = staffLogonSessions.STAFF_ID;

        if (setupDetails.AM_WORKORDER_LABOUR_ID === null || setupDetails.AM_WORKORDER_LABOUR_ID === "") {
            setupDetails.AM_WORKORDER_LABOUR_ID = 0;
        }
        if (setupDetails.LABOUR_DATE == "" || setupDetails.LABOUR_DATE == "null" || setupDetails.LABOUR_DATE == "00/00/0000" || setupDetails.LABOUR_DATE == "00-00-0000") {
            setupDetails.LABOUR_DATE = "01/01/1900";
        }
        var labourType = $("#LABOUR_TYPE").val();
       
        if (labourType === 'In House') {
            if ($("#EMPLOYEE_NAME").val() === null || $("#EMPLOYEE_NAME").val() === "") {
                alert('Unable to proceed. Please provide data for Employee to proceed.');
                return;
            }
        } else if (labourType === 'Contract Service') {
            if ($("#CONTRACT_NAME").val() === null || $("#CONTRACT_NAME").val() === "") {
                alert('Unable to proceed. Please provide data for Contract to proceed.');
                return;
            }
        } else if (labourType === 'Non-Contract Service') {
            if ($("#SUPPLIER_NAME").val() === null || $("#SUPPLIER_NAME").val() === "") {
                alert('Unable to proceed. Please provide data for Supplier to proceed.');
                return;
            }
        }

        var wo_labourId = $("#AM_WORKORDER_LABOUR_ID").val();
        if (setupDetails.LABOUR_CHARGE === null || setupDetails.LABOUR_CHARGE === "") {
            setupDetails.LABOUR_CHARGE = 0;
        }



        var reqDate = $("#REQ_DATE").val();
        
        var checkreqDate = self.isDateFutre(moment(setupDetails.LABOUR_DATE, 'DD/MM/YYYY hh:mm:ss'), moment(reqDate, 'DD/MM/YYYY hh:mm:ss'));
        if (checkreqDate) {
            alert("Unable ro proceed. Labour date cannot be less than Work Order requested Date");
            return;
        }



        var closeDateTime = setupDetails.LABOUR_DATE;
        var closeDate = setupDetails.LABOUR_DATE;
        if (closeDate !== "" && closeDate != null) {
            closeDate = closeDate.substring(6, 10) + '-' + closeDate.substring(3, 5) + '-' + closeDate.substring(0, 2);
            strTime = String(closeDateTime).split(' ');
            strTime = strTime[1].substring(0, 5);
            setupDetails.LABOUR_DATE = closeDate + ' ' + strTime;
        }
        




        if (wo_labourId === null || wo_labourId === "") {
            var jDate = new Date();
            var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);

           

            var assetwoReqNo = ah.getParameterValueByName(ah.config.fld.assetwoReqNo);
           
            setupDetails.REQ_NO = assetwoReqNo;
            setupDetails.ASSET_NO = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
            setupDetails.CREATED_DATE = strDateStart + ' ' + hourmin;
            postData = { AM_WORKORDER_LABOUR: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            ah.toggleSaveMode();
            //alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.createSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {
           // alert(JSON.stringify(setupDetails));
            ah.toggleSaveMode();
         
            postData = { AM_WORKORDER_LABOUR: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
           // alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.updateSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        $("#searchStaffId").hide();
        $("#searchContract").hide();
        $("#searchSupplierId").hide();
    };

    self.webApiCallbackStatus = function (jsonData) {
        //alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
           // window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.STAFFLIST|| jsonData.AM_SUPPLIERLIST ||jsonData.LOV_LOOKUPS || jsonData.AM_WORKORDER_LABOUR_OTH || jsonData.AM_PARTSREQUEST || jsonData.PARTS_STOCKONHAND_V || jsonData.WOL_LOOKUP_V || jsonData.AM_WORKORDER_LABOUR || jsonData.STAFF || jsonData.SUCCESS) {
            //alert(ah.CurrentMode);
            if (ah.CurrentMode == ah.config.mode.save) {
                sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_WORKORDER_LABOUR));
                sessionStorage.setItem(ah.config.skey.wolLookup, JSON.stringify(jsonData.AM_WORKORDER_LABOUR_OTH));
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();
                if (jsonData.AM_PARTSREQUEST) {
                } else {
                    if (jsonData.SUCCESS) {
                        self.showDetails(false);
                    }
                    else {
                        self.showDetails(true);
                    }
                }
                
            }
            
            else if (ah.CurrentMode == ah.config.mode.add) {
                 if (jsonData.STAFFLIST) {
                    staffListLOV = jsonData.STAFFLIST;
                    self.searchResultStaffListLov("");
                } else if (jsonData.AM_SUPPLIERLIST) {
                    supplierListLOV = jsonData.AM_SUPPLIERLIST;
                    var searchStr = $(ah.config.id.lookUptxtGlobalSearchOth).val();
                    self.searchResultSupplierListLov(searchStr);
                }
            }
            else if (ah.CurrentMode == ah.config.mode.edit) {
                if (jsonData.STAFFLIST) {
                    staffListLOV = jsonData.STAFFLIST;
                    self.searchResultStaffListLov("");
                } else if (jsonData.AM_SUPPLIERLIST) {
                    supplierListLOV = jsonData.AM_SUPPLIERLIST;
                    var searchStr = $(ah.config.id.lookUptxtGlobalSearchOth).val();
                    self.searchResultSupplierListLov(searchStr);
                }
            }   
            else if (ah.CurrentMode == ah.config.mode.read) {
               // alert(JSON.stringify(jsonData.AM_WORKORDER_SUPPLIER));
                sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_WORKORDER_LABOUR));
                //sessionStorage.setItem(ah.config.skey.wolLookup, JSON.stringify(jsonData.AM_WORKORDER_LABOUR_OTH));
               
                employeeInf = jsonData.AM_WORKORDER_EMPLOYEE;
                supplierInf = jsonData.AM_WORKORDER_SUPPLIER;
                contractInf = jsonData.AM_WORKORDER_LABOUR_OTH;
              

                self.showDetails(true);
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
                if (jsonData.PARTS_STOCKONHAND_V) {
                    sessionStorage.setItem(ah.config.skey.partsOnhandList, JSON.stringify(jsonData.PARTS_STOCKONHAND_V));
                    sessionStorage.setItem(ah.config.skey.woMaterials, JSON.stringify(jsonData.AM_PARTSREQUEST));
                    $(ah.config.id.loadNumber).text("1");
                    $(ah.config.id.pageCount).val("1");
                    $(ah.config.id.pageNum).val(0);
                    self.searchPartsOnhandResult("LOAD", 0);
                } else {
                    var aimsResponse = jsonData.LOV_LOOKUPS;

                    $.each(aimsResponse, function (item) {

                        $(ah.config.id.wolResponse)
                            .append($("<option></option>")
                                .attr("value", aimsResponse[item].LOV_LOOKUP_ID)
                                .text(aimsResponse[item].DESCRIPTION));


                    });
                    self.searchResult(jsonData);
                }
                    
            }
            else if (ah.CurrentMode == ah.config.mode.searchDropdown) {
                
                if (jsonData.WOL_LOOKUP_V) {
                    sessionStorage.setItem(ah.config.skey.refList, JSON.stringify(jsonData.WOL_LOOKUP_V));
                    $(ah.config.id.loadNumber).text("1");
                    $(ah.config.id.pageCount).val("1");
                    $(ah.config.id.pageNum).val(0);
                    self.searchLOVResult("", 0);
                } else {
                    

                    self.searchResult(jsonData);
                }
            }
            else if (jsonData.AM_PARTSREQUEST.length > 0 && ah.CurrentMode == ah.config.mode.display) {
                sessionStorage.setItem(ah.config.skey.woMaterials, JSON.stringify(jsonData.AM_PARTSREQUEST));
                window.location.href = window.location.href.replace("#openModalInHouse", "#");
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {
                
               


            }

        }
        else {

           alert(systemText.errorTwo);
            //window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};