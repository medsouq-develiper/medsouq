﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: { 
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liNew: '.liNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divPatientAssessmentID: '.divPatientAssessmentID',
            statusText: '.status-text',
            patientAssessmentDetailItem: '.patientAssessmentDetailItem',
            consultbookDetailItem: '.consultBookDetailItem',
            imageItem: '.imageItem',
            transactionList: '.transaction-list',
            alertsList: '.alerts-list',
            fieldList: '.field-list',
            itemnameText: '.itemnameText',
            lovlistText: '.LovListText',
            patDob: '.PATDOB',
            fieldVisitlist: '.field-visitlist',
            tabHeader: '.tabHeader',
            liDelete: '.liDelete',
            imageInfo: '.imageInfo'

        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul',
            table: 'table'
        },
        mode: {
            idle: 0,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',

            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            txtDateFrom: '#txtDateFrom',
            eventID: '#EVENT_ID',
            successMessage: '#successMessage',

            searchItems2: '#searchItems2',

            itemlistId: '#itemlistId',
            itemName: '#itemName',
            imageToLoad: '#imageToLoad',
            patientID: '#PATIENT_ID',
			frmCategory: '#frmCategory',
			addButtonId: '#addButtonId',
			modifyButtonId: '#modifyButtonId'



        },
        tagId: {

            oeInstruction: 'INSTRUCTION'

        },
        cssCls: {
            viewMode: 'view-mode',
            searchRowDivider: 'search-row-divider',
            alignLeft: 'align-left',
            alignRight: 'align-right'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            dataPatientAssessmentID: 'data-patientassessmentID',
            dataPatientClinicBooking: 'data-patientClinicBooking',
            dataitemInfo: 'data-itemInfo',
            datalistitemInfo: 'data-listitemInfo',
            senderID: 'id'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
			groupPrivileges: 'GROUP_PRIVILEGES',
            searchResult: 'searchResult',
            scanDocs: 'SCANDOCS',       
            categoryDetails: 'SCANDOCS_CATEGORY'

        },
        fld: {
            fldzdexdsesxxlz: 'zdexdsesxxlz',
            fldAssetID: 'ASSETNO',
            assetDescInfo: 'ASSETDESC',
            fldEventID: 'eventID',
            fldsearchPatient: 'searchPatient',
            reqWONo: 'REQNO'
        },
        url: {
            homeIndex: '/Home/Index',
            orderEntry: '/orders/OrderEntry?',
            aimsInformation: '/aims/assetInfo',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalUpload: '#openModalUpload',
            closeModal: '#'
        },
        api: {

            searchInfo: '/AIMS/SearchAssetId',
            searchInfoCagetoryImages: '/Patient/SearchScandocsByPatientCategoryID',
            createpatientImages: '/AIMS/CreateAssetImages',
            searchInfoImages: '/AIMS/SearchAssetImageId',
            deleteImage: '/AIMS/DeleteImage',
            getClientRules: '/ClientRules/GetClientRules'

        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal patientAssessmentDetailItem' data-patientClinicBooking='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            searchBookRowHeaderTemplate: "<a href='#' class='search-row-data consultBookDetailItem fc-green' data-itemInfo='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            searchListRowHeaderTemplate: "<a href='#' class='search-row-data imageItem fc-slate-blue' data-listitemInfo='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            searchOrdersRowHeaderTemplate: "<a href='#' class='search-row-data consultBookDetailItem fc-green' data-itemInfo='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            searchRowTemplate: '<span class="search-row-label fc-light-grey-2">{0}: <span class="search-row-data">{1}</span></span>',
            searchRowTemplateStrong: '<span class="search-row-label fc-green">{0}: <span class="search-row-data">{1}</span></span>',
            vitalsRowHeaderTemplate: '<p><i class="fa fa-stethoscope"></i>&nbsp;&nbsp;<span >{0}</span></p>',
            vitalsRowTemplate1: '<span class="summary-row-label">{0}: <span class="summary-row-data">{1}</span></span>',
            vitalsRowTemplate2: '<span class="summary-row-label">{0}: <span class="summary-row-data">{1}</span><span class="summary-row-data"></span><span class="summary-row-data">{2}</span></span>',
            alertsRowHeaderTemplate: '<p><i class="fa fa-info-circle"></i>&nbsp;&nbsp;<span>{0}</span></p>',
            searchRowTableTemplate: '<span class="search-row-data">{0}</span>',
        }
    };

    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();


        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
             + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
             thisApp.config.cls.notifySuccess + ',' + thisApp.config.id.frmCategory+','+thisApp.config.cls.liDelete).hide();

        $(thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";
        $('#iFrame').css('height', (window.innerHeight - 275) + 'px');

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;


        $(thisApp.config.id.itemlistId + ',' + thisApp.config.id.searchItems1).show();



    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liDelete).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.fieldList).show();
        $(thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.liDelete).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };
    thisApp.toggleSaveImage = function () {
        $(thisApp.config.cls.liSave).show();
    };
    thisApp.togglenewDoc = function () {
        $(thisApp.config.cls.liSave).hide();
    };
    thisApp.toggleConsultRedMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;


        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.fieldList + ',' + thisApp.config.id.rptDetail + ',' + thisApp.config.cls.fieldVisitlist).show();
        $(thisApp.config.id.searchOrders).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liDelete + ',' + thisApp.config.cls.imageInfo).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.id.frmCategory
        ).hide();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        //$(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        //$(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divPatientAssessmentID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.fieldList
        ).hide();

        $(
           thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
           thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);
        self.initialize();

    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.formatJSONDateTimeToString = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0] + ' ' + strTime[1].substring(0, 5);


        return dateToFormat;
    };

    thisApp.formatJSONDateToString = function () {

        var dateToFormat = arguments[0];
        var strDate;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 10) return "-";
        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');

            if (isDateTime) {
                val = thisApp.formatJSONDateTimeToString(val);
            }

            if (isDate) {
                val = thisApp.formatJSONDateToString(val);
            }

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var imageLoaderViewModel = function (systemText) {

    var self = this;
    var uploadedFiles = [];
    var srDataArray = ko.observableArray([]);
    var x = 0;
    var ah = new appHelper(systemText);
    var modal = document.getElementById('openModalAlert');
    var clientRules = {};
    var imageExtensions = new RegExp("(.*?)\.(BMP|bmp|gif|jpg|png|pdf|txt)$");
    var tifExtensions = new RegExp("(.*?)\.(TIF|TIFF|tif|tiff)$");
    var officeExtensions = new RegExp("(.*?)\.(doc|docx|dot|csv|xls|xlsx|xlw|pps|ppsx|ppt|pptx)$");
    var unsupportedExtension = new RegExp("(.*?)\.(dwg|DWG)$");

    self.initialize = function () {
        
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));


        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());
        window.sessionStorage.removeItem("SCANDOCS");
        ah.toggleReadMode();
        ah.initializeLayout();
        assetNoID = ah.getParameterValueByName(ah.config.fld.fldAssetID);
        assetDescription = ah.getParameterValueByName(ah.config.fld.assetDescInfo);
        if (assetDescription.length > 70) {
            assetDescription = assetDescription.substring(0, 70) + '...';
        }
        if (assetNoID !== null && assetNoID !== "") {
            $("#infoID").text(assetNoID + "-" + assetDescription);
     
        }
        self.readAssetEventID();


        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        postData = { SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };

        $.post(self.getApi() + ah.config.api.getClientRules, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };

    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
   
    self.assetInfo = function () {
        assetNoID = ah.getParameterValueByName(ah.config.fld.fldAssetID);
        var reqWoNO = ah.getParameterValueByName(ah.config.fld.reqWONo);
        window.location.href = ah.config.url.aimsInformation + "?ASSETNO=" + assetNoID + "&REQNO=" + reqWoNO;
    };

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };


    self.signOFF = function () {
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        $(ah.config.id.signoffBYDUM).val(staffLogonSessions.STAFF_ID);

        self.savePatietConsultInfo()

    };

    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            self.showPatientConsultationInfo();
        }

    };
    self.uploadNewImages = function () {
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";
        window.sessionStorage.removeItem("SCANDOCS");
        ah.togglenewDoc();
        $("#SAVEACTION").val('IMAGE');
        document.getElementById("pdfInfo").value = "";
        $("#uploadTitle").text('Upload Image File');
        $("#pdfInfo").hide();
        $("#imageInfo").show();
        $(ah.config.id.frmCategory).show();
        $(ah.config.cls.liDelete).hide();
        $(ah.config.cls.imageInfo + ' ' + ah.config.tag.ul).html('');

    };
    self.newUpload = function () {
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";
        window.sessionStorage.removeItem("SCANDOCS");
        document.getElementById("uploadedInfo").value = "";
        $("#SAVEACTION").val('PDF');
        ah.togglenewDoc();
        $("#uploadTitle").text('Upload File');
        $("#uploadedInfo").show();
        $(ah.config.id.frmCategory).show();
        $(ah.config.cls.liDelete).hide();
        $(ah.config.cls.imageInfo + ' ' + ah.config.tag.ul).html('');
        window.location.href = ah.config.url.modalUpload;
    };
    $('#imageInfo').change(function (e) {
        var files = e.target.files;
        alert(files.name);
        self.loadImages(files);

    });
    $('#pdfInfo').change(function (e) {
        var files = e.target.files;
        uploadedFiles.push(files[0]);
        var myFile = document.getElementById('pdfInfo').value;
        var myFileData = document.getElementById('pdfInfo');
        var fName = null;
        //alert(myFile.length);
        if (myFile.length > 0) {
            var fName = myFileData.files[0].name;
        } else {
            return;
        }
        var assetID = ah.getParameterValueByName(ah.config.fld.fldAssetID);
        fName = assetID + '_' + fName;
        if (srDataArray.length > 0) {
            srDataArray = srDataArray.filter(function (item) { return item.FILE_NAME === fName });
            if (srDataArray.length > 0) {
                alert('The Document Name is already Exist, please rename or select other file to upload.');
                document.getElementById("pdfInfo").value = "";
                return;
            }
        }
        
        ah.toggleSaveImage();
        self.eloaddata(fName,"pdf","", 0, files);
    });

    $('#uploadedInfo').change(function (e) {
        var files = e.target.files;
        if (files.length != 0) {
            if (imageExtensions.test(files[0].name) || tifExtensions.test(files[0].name) || officeExtensions.test(files[0].name) || unsupportedExtension.test(files[0].name)) {
                var myFile = document.getElementById('uploadedInfo').value;
                var myFileData = document.getElementById('uploadedInfo');
                var fName = null;
                var fileType = null;
                //alert(myFile.length);
                if (myFile.length > 0) {
                    var fName = myFileData.files[0].name;
                    fileType = fName.substring(fName.lastIndexOf('.') + 1, fName.length);
                    $("#SAVEACTION").val(fileType.toUpperCase());
                } else {
                    return;
                }
                var assetID = ah.getParameterValueByName(ah.config.fld.fldAssetID);
                fName = assetID + '_' + fName;
                if (srDataArray.length > 0) {
                    var tempSrDataArray = srDataArray.filter(function (item) { return item.FILE_NAME === fName });
                    if (tempSrDataArray.length > 0) {
                        alert('The Document Name is already Exist, please rename or select other file to upload.');
                        document.getElementById("uploadedInfo").value = "";
                        return;
                    }
                }

                ah.toggleSaveImage();
                self.eloaddata(fName, fileType, "", 0, files);
            } else {
                alert('File not supported.');
                $(this).val('');
            }
        }
    });

    self.loadImages = function (files) {

        var formDetails = ah.ConvertFormToJSON($(ah.config.id.frmCategory)[0]);
        if (formDetails.CATEGORY === null || formDetails.CATEGORY === "") {
            formDetails.CATEGORY = 'Others';
            
            
        }

        'use strict';
        x = 0;
        var resize = new window.resize();
        resize.init();
        //event.preventDefault();           

        // var files = event.target.files;       
        var array = 0;
        var arrayFileName = new Array;
        ah.toggleSaveImage();
        for (var i in files) {

            if (typeof files[i] !== 'object') return false;

            var file_name = files[i].name;
            arrayFileName.push(file_name);
            var base64 = self.resizeImage(files[i], arrayFileName);

        }
        

    };
    self.returnHome = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.resizeImage = function (file, arrayFileName) {


        var resize = new window.resize();
        resize.init();


        var reader = new FileReader();
        reader.onload = function (readerEvent) {

            self.resizeCanvas(readerEvent.target.result, 1200, 'file', arrayFileName, file);

        }
        reader.readAsDataURL(file);


    };
    self.resizeCanvas = function (dataURL, maxSize, outputType, arrayFileName, file) {

        var _this = this;
        var x = 0;
        var image = new Image();
        image.onload = function (imageEvent) {

            // Resize image
            var canvas = document.createElement('canvas'),
                width = image.width,
                height = image.height;
            if (width > height) {
                if (width > maxSize) {
                    height *= maxSize / width;
                    width = maxSize;
                }
            } else {
                if (height > maxSize) {
                    width *= maxSize / height;
                    height = maxSize;
                }
            }
            canvas.width = width;
            canvas.height = height;
            canvas.getContext('2d').drawImage(image, 0, 0, width, height);

            var newdataURL = canvas.toDataURL('image/jpeg', 0.8)
            self.resizeOutcome(newdataURL, 600, 'dataURL', arrayFileName, file);

            // _this.output(canvas, outputType, callback);

        }

        image.src = dataURL;
    };

    self.resizeOutcome = function (dataURL, maxSize, outputType, arrayFileName, file) {

        var _this = this;

        var image = new Image();
        image.onload = function (imageEvent) {

            // Resize image
            var canvas = document.createElement('canvas'),
                width = image.width,
                height = image.height;
            if (width > height) {
                if (width > maxSize) {
                    height *= maxSize / width;
                    width = maxSize;
                }
            } else {
                if (height > maxSize) {
                    width *= maxSize / height;
                    height = maxSize;
                }
            }
            canvas.width = width;
            canvas.height = height;
            canvas.getContext('2d').drawImage(image, 0, 0, width, height);
            var newdataURL = canvas.toDataURL('image/jpeg', 0.8)

            self.eloaddata("","",newdataURL, arrayFileName, file);
            // _this.output(canvas, outputType, callback);

        }

        image.src = dataURL;
    };

    self.eloaddata = function (filename, filetype, data, filenameArray, file) {
        var file_name = file.name;
        
        var file_type = 'JPG';
        //var saveAction = $("#SAVEACTION").val();
        //if (saveAction === 'PDF') {
        //    file_name = filename;
        //    file_type = filetype;
        //} else {
        //    var assetID = ah.getParameterValueByName(ah.config.fld.fldAssetID);
        //    file_name = assetID + '_' + file_name;
        //}
        file_name = filename;
        file_type = filetype;
 
        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());

            return hours + ":" + minutes;
        }

        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);
        var strDate = hourmin + ' ' + strDateStart;


        var appendData = JSON.parse(sessionStorage.getItem(ah.config.skey.scanDocs));
        var fldRefID = ah.getParameterValueByName(ah.config.fld.fldAssetID).trim();
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var formDetails = ah.ConvertFormToJSON($(ah.config.id.frmCategory)[0]);

        var htmlstr = "";

        var appendDatafilter = appendData;
        if (appendDatafilter != null) {
            appendDatafilter = appendDatafilter.filter(function (item) { return item.FILE_NAME === file_name });
            if (appendDatafilter.length > 0) {
                alert('The Document Name is already Exist, please rename or select other file to upload.');
                document.getElementById("uploadedInfo").value = "";
                return;
            }
        }
        uploadedFiles.push($('#uploadedInfo')[0].files[0]);

        htmlstr += '<li>';
        htmlstr += '<p class="fs-medium-normal fc-slate-blue"><i class=""></i>' + file_name + '</p>';

        htmlstr += '<img src="' + data + '" id="newImage" style="vertical-align:top" >';

        htmlstr += '</li>';

        $(ah.config.id.imageToLoad + ' ' + ah.config.tag.ul).append(htmlstr);
        x++;

        if (appendData === null) {
            var newAttribute = JSON.stringify({ "REF_ID": fldRefID, "EVENT_ID": 0, "SCANDOCS_DATA": data, "FILE_NAME": file_name, "FILE_TYPE": file_type, "CATEGORY": 'ASSETDOC', "CREATED_BY": staffLogonSessions.STAFF_ID, "CREATED_DATE": strDate });
            var appendData = JSON.parse('[' + newAttribute + ']');

        } else {
            appendData.push({ "REF_ID": fldRefID, "EVENT_ID": 0, "SCANDOCS_DATA": data, "FILE_NAME": file_name, "FILE_TYPE": file_type, "CATEGORY": 'ASSETDOC', "CREATED_BY": staffLogonSessions.STAFF_ID, "CREATED_DATE": strDate });
        }

        sessionStorage.setItem(ah.config.skey.scanDocs, JSON.stringify(appendData));

    };
    self.uploadPDF = function () {
        
        var fd = new FormData(),
            myFile = document.getElementById("pdffile").files[0];

        fd.append('file', myFile);
       
        $.ajax({
            url: 'http://localhost:51732/view',
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                console.log(data);
            }
        });


    };
    self.savePdf = function () {
        //$(ah.config.id.saveInfo).trigger('click');
    }
    self.showImagesList = function (jsonData) {


    
        var htmlStr;

        //  ah.ResetControls();
        var rd = jsonData.SCANDOCS_CATEGORYIMAGELIST;

        var currCategoryId = "";
        if (rd.length > 0) {
            currCategoryId = rd[0].CATEGORY;
        }
        $(ah.config.cls.transactionList + ' ' + ah.config.tag.ul).html('');
        var htmlstr = "";

        for (var o in rd) {
       
                    htmlstr += '<li>';
                    htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                    htmlstr += ah.formatString(ah.config.html.searchListRowHeaderTemplate, JSON.stringify(rd[o]), '  ' + ah.formatJSONDateToString(rd[o].CREATED_DATE), '(' + rd[o].FILE_NAME + ')', "");
                    htmlstr += '</span>';

                    htmlstr += '</li>';

        }

        $(ah.config.cls.transactionList + ' ' + ah.config.tag.ul).append(htmlstr);
      
        $(ah.config.cls.imageItem).bind('click', self.readAssetImageId);
        ah.displaySearchResult();
        ah.toggleDisplayMode();
		self.privilegesValidation('NEW');


    };

    self.saveAllChanges = function () {
        var assetNoInfo = ah.getParameterValueByName(ah.config.fld.fldAssetID);
        var frmData = new FormData();
        var filebase = $("#documentInfo").get(0);
        var files = filebase.files;
        frmData.append("docFilename", assetNoInfo);
        frmData.append(files[0].name, files[0]);
        $.ajax({
            url: '/Upload/SaveDocument',
            type: "POST",
            contentType: false,
            processData: false,
            data: frmData,
            success: function (data) {
                alert('Success Upload');
            },
            error: function (err) {
               alert(error);
            }
        });
  

    //End of the document ready function...
      
    };    
   
    self.saveDoc = function (e) {
        var files = e.target.files;
        var formData = new FormData($("#frmDocument")[0]);
        var input = document.getElementById('documentInfo');
        console.log(input.files);
        $.ajax({
            url: '/Upload/UploadDocument',
            type: 'Post',
            beforeSend: function () { },
            success: function (result) {

            },
            xhr: function () {  // Custom XMLHttpRequest
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) { // Check if upload property exists
                    // Progress code if you want
                }
                return myXhr;
            },
            error: function () { },
            data: files,
            cache: false,
            contentType: false,
            processData: false
        });
    }
    self.showAssetImagesInfo = function (jsonData) {


        var rd = jsonData.SCANDOCS;

        var htmlStr;
       
        //ah.ResetControls();

        $(ah.config.cls.imageInfo + ' ' + ah.config.tag.ul).html('');
        var htmlstr = "";

        if (rd.length) {

            for (var o in rd) {


                htmlstr += '<li>';
                htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                htmlstr += '<p class="fs-medium-normal fc-slate-blue"><i class=""></i>' + rd[o].FILE_NAME + '</p>';
                htmlstr += '<img src="' + rd[o].SCANDOCS_DATA + '" id="newImage" style="vertical-align:top"  >';
                htmlstr += '</span>';
                htmlstr += '<br/><p class="fs-medium-normal fc-slate-blue fs-small-normal">Click Image to enlarge&nbsp;&nbsp;</p>'
                htmlstr += '</li>';

            }
        }

        $(ah.config.cls.imageInfo + ' ' + ah.config.tag.ul).append(htmlstr);
		self.privilegesValidation('NEW');
		self.privilegesValidation('MODIFY');


    };

    self.readAssetImageId = function () {
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";
        var imageDetails = JSON.parse(this.getAttribute(ah.config.attr.datalistitemInfo));


        $('#iFrame').show();
        var path = `/UploadedFiles/${imageDetails.FILE_NAME}`;
        if (imageExtensions.test(imageDetails.FILE_NAME)) {
            $('#iFrame').attr('src', path);
            $('#iFrameNewTab').attr('href', path).html('Full View');
            $('#iFrameName').html(imageDetails.FILE_NAME);
        }
        else if (tifExtensions.test(imageDetails.FILE_NAME)) {
            var xhr = new XMLHttpRequest();
            xhr.responseType = 'arraybuffer';
            xhr.open('GET', path);
            xhr.onload = function (e) {
                var buffer = xhr.response;
                var tiff = new Tiff({ buffer: buffer });
                var data = tiff.toDataURL();
                $('#iFrame').attr('src', data);
                $('#iFrameNewTab').attr('href', path).html('Click here to download.');
                $('#iFrameName').html(imageDetails.FILE_NAME);
            };
            xhr.send();
        }
        else if (officeExtensions.test(imageDetails.FILE_NAME)) {
            
            var clientRule = clientRules;
            if (clientRule != null) {
                if (clientRule.STATUS == "A") {
                    $('#iFrame').attr('src', `${clientRule.RULE_VALUE}${imageDetails.FILE_NAME}`);
                    $('#iFrameNewTab').attr('href', `${clientRule.RULE_VALUE}${imageDetails.FILE_NAME}`);
                    $('#iFrameName').html(imageDetails.FILE_NAME);
                } else {
                    $('#iFrameNewTab').attr('href', path).html(`File not supported, click here to download.`);
                   
                    $('#iFrameName').html(imageDetails.FILE_NAME);
                }
            } else {
                $('#iFrameNewTab').attr('href', path).html(`File not supported, click here to download.`);
                
                $('#iFrameName').html(imageDetails.FILE_NAME);
            }
            
        }
        else if (unsupportedExtension.test(imageDetails.FILE_NAME)) {
            $('#iFrameNewTab').attr('href', path).html(`File not supported, click here to download.`);
            $('#iFrameName').html(imageDetails.FILE_NAME);
            $('#iFrame').hide();
        }


        //clientRules.RULE_VALUE
        //clientRules.STATUS
    };

    self.imageDisplay = function () {
        var appendData = JSON.parse(sessionStorage.getItem(ah.config.skey.scanDocs));


        var mywin = window.open("/UploadedFiles/" + imageDetails.FILE_NAME, "width=960,height=764");

        var htmlstr = "";
        htmlstr += '<ul>';
        htmlstr += '<li>';
        htmlstr += '<span>';
        // htmlstr += '<p class="fs-medium-normal fc-slate-blue"><i class=""></i>' + rd[o].FILE_NAME + '</p>';
        htmlstr += '<img src="' + appendData[0].SCANDOCS_DATA + '" id="newImage" style="vertical-align:top height: 660px; width: 564px;" >';
        htmlstr += '</span>';

        htmlstr += '</li>';
        htmlstr += '</ul>';

        var contents = htmlstr;//$('#RECEIPTPRINT').val();
        var mywin = window.open("", "ckeditor_preview", "location=0,status=0,scrollbars=0,width=960,height=764");

        $(mywin.document.body).html(contents);

    };
    self.searchCategoryImages = function () {
        var cagetoryInfo = JSON.parse(this.getAttribute(ah.config.attr.dataitemInfo));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        ah.CurrentMode = ah.config.mode.read;
        //    ah.toggleReadMode();

        postData = { PATIENT_ID: cagetoryInfo.PATIENT_ID, CATEGORY: cagetoryInfo.CATEGORY, STAFF_LOGON_SESSIONS: staffLogonSessions };

        $.post(self.getApi() + ah.config.api.searchInfoCagetoryImages, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.alertMessage = function () {
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "block";
       
    };
    self.deleteImage = function () {
        modal.style.display = "none"
        $(ah.config.cls.imageInfo + ' ' + ah.config.tag.ul).html('');
                $(ah.config.id.successMessage).html(systemText.deleteSuccessMessage);
             $(ah.config.cls.notifySuccess).show();
             $(ah.config.cls.divProgress).hide();

             setTimeout(function () {
                 $(ah.config.cls.notifySuccess).fadeOut(1000, function () { });
                 self.initialize();
             }, 3000);
            var imageDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.scanDocs));
            var fldRefID = ah.getParameterValueByName(ah.config.fld.fldAssetID).trim();
            var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var postData;

            ah.toggleReadMode();
            ah.CurrentMode = ah.config.mode.read;
            $(ah.config.cls.imageInfo).hide();
            postData = {REF_ID: fldRefID, SCANDOC_ID: imageDetails[0].SCANDOC_ID, STAFF_LOGON_SESSIONS: staffLogonSessions };

            $.post(self.getApi() + ah.config.api.deleteImage, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        
    };
    self.cancelChangesModal = function () {
        window.location.href = ah.config.url.closeModal;
        uploadedFiles = [];
    }
    self.readAssetEventID = function () {
        
        var fldRefID = ah.getParameterValueByName(ah.config.fld.fldAssetID).trim();
        var fldEventID = "";
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { REF_ID: fldRefID, SCANDOC_ID: fldEventID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchInfo, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.UploadImage = function (REF_ID) {
        window.location.href = ah.config.url.closeModal;
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        var frmData = new FormData();

        frmData.append("REF_ID", REF_ID);
        frmData.append("pathLocation", "~/UploadedFiles");
        uploadedFiles.forEach((file, i) => {
            frmData.append(`file${i}`, file);
        });
        $.ajax({
            url: '/Upload/SaveDocument',
            type: "POST",
            contentType: false,
            processData: false,
            data: frmData,
            success: function (data) {
                ah.toggleSaveMode()

                var jDate = new Date();
                var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
                function time_format(d) {
                    hours = format_two_digits(d.getHours());
                    minutes = format_two_digits(d.getMinutes());
                    return hours + ":" + minutes;
                }
                function format_two_digits(n) {
                    return n < 10 ? '0' + n : n;
                }
                var hourmin = time_format(jDate);

                var scanDocsSave = JSON.parse(sessionStorage.getItem(ah.config.skey.scanDocs));

                scanDocsSave.LAST_UPDATED = strDateStart + ' ' + hourmin;
                scanDocsSave.PUSH_FLAG = "Y";
                postData = { SCANDOCS: scanDocsSave, STAFF_LOGON_SESSIONS: staffLogonSessions };
                
                $.when($.post(self.getApi() + ah.config.api.createpatientImages, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus)).done(function () {
                    uploadedFiles = [];
                });
            },
            error: function (err) {
                alert(error);
            }
        });
    };
    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleSearchMode();
        var date = Date.parse($(ah.config.id.txtDateFrom).val());
        var jDate = new Date(date);
        //alert(jDate);
        var strDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();
        //alert(strDate);
        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), DATE_FROM: strDate, STAFF_LOGON_SESSIONS: staffLogonSessions };

        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.searchItems = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleSearchMode();

        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), CATEGORY: "", ACTION: "ORDERLIST", STAFF_LOGON_SESSIONS: staffLogonSessions };

        $.post(self.getApi() + ah.config.api.searchItems, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
	self.privilegesValidation = function (actionVal) {

		var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
		if (actionVal === 'NEW') {
			gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "16" });
			if (gPrivileges.length <= 0) {
				$(ah.config.id.addButtonId).hide();
			}
		} else if (actionVal === 'MODIFY') {
			gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "3" });
			if (gPrivileges.length <= 0) {
				$(ah.config.id.modifyButtonId + ',' + ah.config.id.addButtonIcon).hide();
			}
		}
	}
    self.saveAssetImages = function () {
        var saveAction = $("#SAVEACTION").val();

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);

        var scanDocsSave = JSON.parse(sessionStorage.getItem(ah.config.skey.scanDocs));
       // alert(JSON.stringify(scanDocsSave));

        if (scanDocsSave === null) {
            alert('Unable to proceed. There is no data to be save.')
            return;
        }
       
        self.UploadImage(scanDocsSave[0].REF_ID);

    };

    self.webApiCallbackStatus = function (jsonData) {
        //console.log(jsonData);
         //alert(JSON.stringify(jsonData));

        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));

           // window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.SCANDOCS || jsonData.SCANDOCS_CATEGORY || jsonData.SCANDOCS_CATEGORYIMAGELIST || jsonData.SUCCESS || jsonData.ClientRules) {


            if (ah.CurrentMode == ah.config.mode.save) {
                if (jsonData.SUCCESS) {

                } else {

                }
              
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);

                // ah.showSavingStatusSuccess();
                $(ah.config.cls.notifySuccess).show();
                $(ah.config.cls.divProgress).hide();
                $(ah.config.cls.imageInfo + ' ' + ah.config.tag.ul).html('');
                setTimeout(function () {
                    $(ah.config.cls.notifySuccess).fadeOut(1000, function () { });
                    self.initialize();
                }, 3000);
                
                

            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                if (jsonData.SCANDOCS_CATEGORYIMAGELIST) {
                    srDataArray = jsonData.SCANDOCS_CATEGORYIMAGELIST;
                    self.showImagesList(jsonData);
                } else if (jsonData.SCANDOCS) {


                    sessionStorage.setItem(ah.config.skey.scanDocs, JSON.stringify(jsonData.SCANDOCS));
                    self.showAssetImagesInfo(jsonData);
                }
               
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
                alert('here');
                self.searchResult(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {


            }

            if (jsonData.ClientRules) {
                clientRules = jsonData.ClientRules[0];
            }

        }
        else {

            alert(systemText.errorTwo);
            //window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};