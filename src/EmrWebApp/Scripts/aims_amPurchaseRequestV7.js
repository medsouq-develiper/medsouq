﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            liAddItems: '.liAddItems',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            detailItem: '.detailItem',
            liassetTag: '.liassetTag',
            wsAsset: '.wsAsset',
            itemRequest: '.itemRequest',
            liRequest: '.liRequest'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7,
            deleted: 8
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmInfo: '#frmInfo',
            storeID: '#STORE_ID',
            frmInfoInspection: '#frmInfoInspection',
            clinicdoctorID: '#CLINIC_DOCTOR_ID',
            clinicID: '#CLINIC_ID',
            frmwsAsset: '#frmwsAsset',
            frmInfoReading: '#frmInfoReading',
            staffID: '#STAFF_ID',
            saveInfoLog: '#saveInfoLog',
            serviceID: '#SERVICE_ID',
            searchResult1: '#searchResult1',
            successMessage: '#successMessage',
            saveInfoReading: '#saveInfoReading',
            txtGlobalSearchOth: '#txtGlobalSearchOth',
            pageNum: '#pageNum',
            pageCount: '#pageCount',
            loadNumber: '#loadNumber',
            btnRemove: '#btnRemove',
            itemsReceiveBatch: '#itemsReceiveBatch',
            btnApprove: '#btnApprove',
            searchRequestParts: '#searchRequestParts',
            searchWorkOrder: '#searchWorkOrder'
        },

        cssCls: {
            viewMode: 'view-mode'
        },
        tagId: {

            txtGlobalSearchOth: 'txtGlobalSearchOth',
            priorPage: 'priorPage',
            nextPage: 'nextPage'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfo: 'data-info',
            datainfoID: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'searchResult',
            infoDetails: 'AM_PURCHASEREQUEST',
            stockList: 'PRODUCT_STOCKONHAND',
            wsassetList: 'AM_INSPECTIONASSET', 
            groupPrivileges: 'GROUP_PRIVILEGES'
        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalLookUp: 'openModalLookup',
            modalClose: '#',
            modalAddItems: '#openModal',
            modalReading: 'openModalReading',
            modalFilter: '#openModalFilter',
            modalWOParts: '#openModalRequestParts',
            openModalReason: '#openModalReason'
        },
        fld: {
            prNo: 'PRNO',
            prRequest: 'xikhkjxhexdher'
        },
        api: {

            readPurchaseReq: '/AIMS/ReadPRDetails',
            searchAll: '/AIMS/SearchPurchaseReqDetFlag',
            createRequest: '/AIMS/CreateNewPurchaseReq',
            updateStatus: '/AIMS/UpdatePRStatus',
            updatePurchaseReq: '/AIMS/UpdatePurchaseRequest',
            readWOrder: '/AIMS/ReadWO',
            readAssetInfo: '/AIMS/ReadAssetInfo',
            seachLookUp: '/Inventory/SearchStores',
            searchStockOnhand: '/Inventory/SearchStockItemsOverall',
            searchWOparts: '/AIMS/SearchPurchasePRWO',
            searchwoMaterial: '/Inventory/SearchWOPartsInc',
           
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
            searchRowHeaderTemplateList: "<a href='#openModal' class='fs-medium-normal detailItem' data-infoAssetID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            searchLOVRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem fc-greendark' data-info ='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} <span>{3}</span></a>",
            searchExistRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem fc-slate-blue' data-info ='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} <span class='fc-green'> {3} </span></a>"
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };
    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    thisApp.initializeLayout = function () {

        thisApp.ResetControls();
        
        $(
            thisApp.config.cls.liRequest + ',' + thisApp.config.cls.liAddItems + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess + ',' + thisApp.config.cls.liassetTag + ',' + thisApp.config.cls.itemRequest
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };
    thisApp.toggleDisplayModereq = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        //$(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        //$(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(thisApp.config.cls.contentField + ',' + thisApp.config.cls.searchAsset ).show();
        //$(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.liApiStatus).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        //$(
        //    thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        //).prop(thisApp.config.attr.readOnly, false);

        //$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };
    thisApp.toggleSearchItems = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liSave +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.liSave
        ).hide();

    };
    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.id.contentSubHeader).show();

        $(
            thisApp.config.cls.liRequest + ',' +  thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.liassetTag + ',' + thisApp.config.cls.itemRequest
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("REQ_DATE").disabled = false;
        document.getElementById("REF_WO").disabled = false;
        document.getElementById("REF_ASSETNO").disabled = false;
        document.getElementById("REQ_DEPT").disabled = false;
      

    };
    thisApp.toggleEditModeModal = function () {


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
    };
    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.liassetTag
        ).hide();

    };
    thisApp.toggleSearchModalMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;


    };
    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.itemRequest+','+ thisApp.config.id.contentSubHeader).show();
        $(thisApp.config.cls.liRequest + ',' +  thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.liassetTag).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        
        document.getElementById("REQ_DATE").disabled = false;
        document.getElementById("REF_WO").disabled = false;
        document.getElementById("REF_ASSETNO").disabled = false;
        document.getElementById("REQ_DEPT").disabled = false;

    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {
        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
    };
    
    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liRequest + ',' +  thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

        $(
           thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
           thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);
        document.getElementById("REQ_DATE").disabled = true;
        document.getElementById("REF_WO").disabled = true;
        document.getElementById("REF_ASSETNO").disabled = true;
        document.getElementById("REQ_DEPT").disabled = true;

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };



    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };
    thisApp.formatJSONDateToStringOth = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[0] + '-' + strDate[1] + '-' + strDate[2].substring(0, 2);


        return dateToFormat;
    };
    thisApp.formatJSONDateToString = function () {

        var dateToFormat = arguments[0];
        var strDate;

        if (dateToFormat === null) return "-";
        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };
    thisApp.formatJSONDateTimeToString = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0] + ' ' + strTime[1].substring(0, 5);


        return dateToFormat;
    };
    thisApp.formatJSONTimeToString = function () {

        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        var timeToFormat = strTime[1].substring(0, 5);


        return timeToFormat;
    };
    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');


            if (isDateTime) {
                val = thisApp.formatJSONDateTimeToString(val);
            }

            if (isDate) {
                val = thisApp.formatJSONDateToString(val);
            }

            var $el = $('#' + name), type = $el.attr('type'), isTime = $el.attr('data-istime');
            if (isTime) {
                if (val === null || val === "") {
                    val = "00:00";
                } else {
                    val = thisApp.formatJSONTimeToString(val);
                }

            }

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var amPurchaseRequestViewModel = function (systemText) {

    var self = this;
    self.searchKeyword = ko.observable('');
    self.searchFilter = {
        fromDate: ko.observable(''),
        toDate: ko.observable(''),
        status: ko.observable(''),
    };
    self.modifyInfo = {
        statusBy: ko.observable(''),
        StatusDate: ko.observable(''),
    };
    self.clearFilter = function () {
        self.searchFilter.toDate('');
        self.searchFilter.fromDate('');
        self.searchFilter.status('');
        $('#requestFromDate').val('');
        $('#requestStartDate').val('');
        window.location.href = ah.config.url.modalClose;
    }
    self.searchFilterNow = function () {
        window.location.href = ah.config.url.modalClose;
        self.searchNow();
    }
    self.searchKeyUp = function (d, e) {
        if (e.keyCode == 13) {
            self.searchNow();
        }
    }
    var ah = new appHelper(systemText);

    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.toggleReadMode();

        ah.initializeLayout();
        if (self.validatePrivileges('36') === 0) {
            $(ah.config.cls.liAddNew).hide();
        }
        var postData;
        postData = { SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.seachLookUp, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.reqList = ko.observableArray("");
 
    self.returnHome = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.dateClick = function () {
        $("#dateClickAction").val("ACTIVE");
    };
    self.showModalFilter = function () {
        var element = document.getElementById("filterStatus");
        element.classList.remove("view-mode");
        document.getElementById("filterStatus").disabled = false;
        window.location.href = ah.config.url.modalFilter;
    };
    self.createNew = function () {
        self.reqList.splice(0, 5000);
        //sessionStorage.removeItem("AM_INSPECTIONASSET");
        ah.toggleAddMode();

        var jDate = new Date();
        var strDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        var reqDate = strDate + ' ' + hourmin;

        
        $("#REQ_DATE").val(reqDate);
        $("#STATUSID").val("REQUEST");
        $("#STATUS").val("REQUEST");
        document.getElementById("REQ_DATE").readOnly = true;
        document.getElementById("REF_ASSETNO").readOnly = true;
        document.getElementById("REF_WO").readOnly = true;
        document.getElementById("STATUSID").disabled = true;
       
        $(ah.config.id.searchWorkOrder).show();

    };
    self.searchWONow = function () {
        var refId = $("#REF_WO").val();
        if (refId === null || refId === "0" || refId === " ") {
            alert('Unable to proceed. Please provide a valid Work Order to Proceed.')
            return;
        }
        self.readWOId(refId);
    };
    self.pageLoader = function () {
        var senderID = $(arguments[1].currentTarget).attr('id');

        var searchStr = $(ah.config.id.txtGlobalSearchOth).val().toString();

        var pageNum = parseInt($(ah.config.id.pageNum).val());
        var pageCount = parseInt($(ah.config.id.pageCount).val());


        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.stockList));
        var lastPage = Math.ceil(sr.length / 10);

        if (isNaN(pageCount)) {
            pageCount = 1;
        }
        if (isNaN(pageNum)) {
            pageNum = 0;
        }

        switch (senderID) {

            case ah.config.tagId.nextPage: {

                if (pageCount < lastPage) {
                    pageNum = pageNum + 10;
                    pageCount++;
                    self.searchItemsResult(searchStr, pageNum);
                }
                break;
            }
            case ah.config.tagId.priorPage: {

                if (pageNum === 0) {
                } else {
                    pageNum = pageNum - 10;
                    if (pageNum < 0) {
                        pageNum = 0;
                    }
                    pageCount = pageCount - 1;
                }

                self.searchItemsResult(searchStr, pageNum);
                break;
            }

            case ah.config.tagId.txtGlobalSearchOth: {
                pageCount = 1;
                pageNum = 0;

                self.searchItemsResult(searchStr, 0);
                break;
            }
        }

        $(ah.config.id.loadNumber).text(pageCount.toString());
        $(ah.config.id.pageCount).val(pageCount.toString());
        $(ah.config.id.pageNum).val(pageNum);

    };
    self.modifySetup = function () {

        ah.toggleEditMode();
        $("#itemsReceiveBatch *").removeAttr('disabled', 'disabled');
        $(".button-ext").each(function (i) { $(this).show() });
        document.getElementById("REQ_DATE").readOnly = true;
        document.getElementById("REF_ASSETNO").readOnly = true;
        document.getElementById("REF_WO").readOnly = true;
        if (self.validatePrivileges('24') > 0) {
            $(ah.config.cls.liAddItems).show();
        }
        //$("#itemsReceiveBatch").closest('tr').find('input').removeAttr('readonly');
    };
    self.cancelChangesModal = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            var refId = $("#AM_INSPECTION_ID").val();
            self.readSetupExistID(refId);

        }

        window.location.href = ah.config.url.modalClose;

    };
  
    self.readWOId = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

   
        var refID = $("#REF_WO").val();
        postData = { REQ_NO: refID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readWOrder, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };
    self.stockSearchNow = function () {
        //var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        //var storeId="";

        //////ah.toggleSearchMode();
        //ah.toggleSearchItems();
        //postData = { SEARCH: "", ACTION: "", STORE: storeId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        //$.post(self.getApi() + ah.config.api.searchStockOnhand, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = ah.config.url.modalAddItems;

    };
    self.saveAllChanges = function () {

        $(ah.config.id.saveInfo).trigger('click');

    };
    self.addItemReq = function () {
   
        var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfo));
        //alert(JSON.stringify(infoID));
            
        
        var existPart = self.checkExist(infoID.ID_PARTS);
        if (existPart > 0) {
            alert('Unable to Proceed. The selected item was already exist.');
            return;
        }
        var reqDate = $("#REQ_DATE").val();
        var strTime = '';
        var apptDate = reqDate;
        reqDate = apptDate.substring(6, 10) + '-' + apptDate.substring(3, 5) + '-' + apptDate.substring(0, 2);

        strTime = String(apptDate).split(' ');
        strTime = strTime[1].substring(0, 5);
        reqDate = reqDate + ' ' + strTime;
        var refId = $("#REQUEST_ID").val();
        self.reqList.push({
            DESCRIPTION: infoID.DESCRIPTION,
            ID_PARTS: infoID.ID_PARTS,
            REQUEST_QTY: 0,
            REQ_DATE: reqDate,
            REQUEST_ID: refId,
            REF_WO: 0,
            REF_ASSETNO: 0,
            STORE_ID: 0,
            STATUS: null,
            AMPRREQITEMID: 0,
            REQ_DEPT: null,
            REMARKS: null,
            CURRSTATUS:"REQUEST",
            REQ_DETFLAG: null
        });
       // $("#ASSET_NO").val(infoID.ASSET_NO);
        //self.saveWSAsset();
        //window.location.href = window.location.href + ah.config.url.modalLookUp;

    };
    self.AddItemFromWo = function () {

        var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfo));
        var existPart = self.checkExist(infoID.ID_PARTS);
        if (existPart > 0) {
            alert('Unable to Proceed. The selected item was already exist.');
            return;
        }
            var reqDate = $("#REQ_DATE").val();
            var strTime = '';
            var apptDate = reqDate;
            reqDate = apptDate.substring(6, 10) + '-' + apptDate.substring(3, 5) + '-' + apptDate.substring(0, 2);

            strTime = String(apptDate).split(' ');
            strTime = strTime[1].substring(0, 5);
            reqDate = reqDate + ' ' + strTime;
        
                var refId = $("#REQUEST_ID").val();
                self.reqList.push({
                    DESCRIPTION: infoID.PRODUCT_DESC,
                    ID_PARTS: infoID.ID_PARTS,
                    PR_REQUEST_QTY: infoID.QTY,
                    PART_NUMBERS: infoID.PART_NUMBERS,
                    REQ_DATE: reqDate,
                    REQUEST_ID: refId,
                    REF_WO: 0,
                    REF_ASSETNO: 0,
                    STORE_ID: 0,
                    STATUS: null,
                    AMPRREQITEMID: 0,
                    REQ_DEPT: null,
                    REMARKS: null,
                    CURRSTATUS: "REQUEST",
                    REQ_DETFLAG: null

                });
  
    };
    self.removeItem = function (reqList) {
        self.reqList.remove(reqList);
       
    };
    self.validatePrivileges = function (privilegeId) {
        var groupPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));

        var groupPrivileges = groupPrivileges.filter(function (item) { return item.PRIVILEGES_ID === privilegeId });
        return groupPrivileges.length;
    };
    self.addWOItems = function (jsonData) {
    
        var sr = jsonData.AM_PARTSREQUEST;
        if (sr.length > 0) {
            var reqDate = $("#REQ_DATE").val();
            var strTime = '';
            var apptDate = reqDate;
            reqDate = apptDate.substring(6, 10) + '-' + apptDate.substring(3, 5) + '-' + apptDate.substring(0, 2);

            strTime = String(apptDate).split(' ');
            strTime = strTime[1].substring(0, 5);
            reqDate = reqDate + ' ' + strTime;
            self.reqList.splice(0, 5000);
            
            for (var o in sr) {
                if (sr[o].TRANSFER_QTY == null) {
                    sr[o].TRANSFER_QTY = 0;
                }
                //if (sr[o].QTY > sr[o].TRANSFER_QTY ) {
                    sr[o].QTY = sr[o].QTY - sr[o].TRANSFER_QTY;
                //}
                
                    var refId = $("#REQUEST_ID").val();
                    if (sr[o].QTY > 0) {
                        self.reqList.push({
                            DESCRIPTION: sr[o].PRODUCT_DESC,
                            ID_PARTS: sr[o].ID_PARTS,
                            PART_NUMBERS: sr[o].PART_NUMBERS,
                            PR_REQUEST_QTY: sr[o].QTY,
                            REQ_DATE: reqDate,
                            REQUEST_ID: refId,
                            REF_WO: 0,
                            REF_ASSETNO: 0,
                            STORE_ID: 0,
                            STATUS: null,
                            AMPRREQITEMID: 0,
                            REQ_DEPT: null,
                            REMARKS: null,
                            CURRSTATUS: "REQUEST",
                            REQ_DETFLAG: null

                        });
                    }
                
            }

           

        }
        

    };
    self.checkExistPR = function (idPart) {

        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.reqList);
        var partLists = JSON.parse(jsonObj);

        var partExist = partLists.filter(function (item) { return item.ID_PARTS === idPart });

        return partExist.length;
    };
    self.checkExist = function (idPart) {

        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.reqList);
        var partLists = JSON.parse(jsonObj);

        var partExist = partLists.filter(function (item) { return item.ID_PARTS === idPart });

        return partExist.length;
    };
    self.cancelChanges = function () {
        var refId = $("#REQUEST_ID").val();
        if (refId === null || refId === "" || refId=== "0") {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            
            self.readPurchaseRequestExist(refId);
        }
        $("#itemsReceiveBatch *").attr('disabled', 'disabled');

    };
    self.openReasonReturnStatus = function () {
        var element = document.getElementById("STATUSREASON");
        element.classList.remove("view-mode");
        element.disabled = false;
        document.getElementById("STATUSREASON").readOnly = false;
        $("#RESMODIFY").val("REOPEN");
        window.location.href = ah.config.url.openModalReason;
        //self.updateStatus("RETURNTOSTATUS");
    };

    self.openReasonCancelStatus = function () {
        var element = document.getElementById("STATUSREASON");
        element.classList.remove("view-mode");
        element.disabled = false;
        document.getElementById("STATUSREASON").readOnly = false;
        $("#RESMODIFY").val("CANCEL");
        window.location.href = ah.config.url.openModalReason;
        //self.updateStatus("RETURNTOSTATUS");
    };
    self.showDetails = function (isNotUpdate, jsonData) {
       // alert('here');
        if (isNotUpdate) {

            var sr = jsonData.AM_PURCHASEREQUEST;//JSON.parse(sessionStorage.getItem(ah.config.skey.infoDetails));
           // alert(JSON.stringify(sr));
            
            ah.ResetControls();
            ah.toggleDisplayMode();
            var currStatus = "REQUEST";
           
            var reqDate = ah.formatJSONDateTimeToString(sr.REQ_DATE);
                
               
                $("#REQ_DATE").val(reqDate);
                $("#REQ_DETFLAG").val(sr.REQ_DETFLAG);
                $("#STATUS").val(sr.STATUS);
                $("#STATUSID").val(sr.STATUS);
                $("#REF_ASSETNO").val(sr.REF_ASSETNO);
                $("#REF_WO").val(sr.REF_WO);
                $("#REQ_DEPT").val(sr.REQ_DEPT);
                $("#REMARKS").val(sr.REMARKS);
                $("#AM_PRREQITEMID").val(sr.AM_PRREQITEMID);
                $("#REQUEST_ID").val(sr.REQUEST_ID);
                $("#STORE_ID").val(sr.STORE_ID);
                var currStatus = sr.STATUS;
                if (currStatus === 'APPROVED' || currStatus === 'CANCELLED' || currStatus  === 'RECEIVED') {
                    $(ah.config.cls.liModify + ','+ ah.config.id.contentSubHeader).hide();
                   // currStatus = sr.STATUS
                }
                //alert(currStatus);

           // }
                if (self.validatePrivileges('34') > 0 && sr.STATUS =="CANCELLED") {
                    $(ah.config.cls.liRequest).show();
                }



                $("#REQUEST_NO").val(sr.REQUEST_ID);
                //alert(JSON.stringify(jsonData.AM_PURCHASEREQUESITEMS));
                var prList = jsonData.AM_PURCHASEREQUESITEMS;
            if (prList.length > 0) {
                if (self.validatePrivileges('2') === 0) {
                    $(ah.config.id.btnApprove).hide();

                };

                self.reqList.splice(0, 5000);
                for (var o in prList) {
                   
                    //var rDate = ah.formatJSONDateToString(sr[o].REQ_DATE);
                        self.reqList.push({
                            DESCRIPTION: prList[o].PRODUCT_DESC,
                            ID_PARTS: prList[o].ID_PARTS,
                            PR_REQUEST_QTY: prList[o].PR_REQUEST_QTY,
                            PART_NUMBERS: prList[o].PART_NUMBERS,
                            //REQUEST_ID: sr[o].REQUEST_ID,
                          
                            //STATUS: sr[o].STATUS,
                            //AMPRREQITEMID: sr[o].AMPRREQITEMID,
                            CURRSTATUS: currStatus

                        });
                    }


               
                }

            var forprList = jsonData.AM_PURCHASEREQUESITEMSFORPR;
            //alert(JSON.stringify(forprList));
            $(ah.config.cls.liAddItems).hide();
            $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).html('');
            if (forprList.length > 0) {
                var htmlstr = '';
                
                for (var o in forprList) {
                    htmlstr += '<li>';
                    htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                   
                    htmlstr += ah.formatString(ah.config.html.searchExistRowHeaderTemplate, JSON.stringify(forprList[o]), forprList[o].ID_PARTS, forprList[o].PRODUCT_DESC, "     &nbsp;&nbsp;&nbsp; Reference Work Order No:" + forprList[o].REF_WO + "    &nbsp;&nbsp;&nbsp;  Requested QTY:" + forprList[o].QTY);
                 
                    htmlstr += '</span>';
                    htmlstr += '</li>';
                   
                }
                $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).append(htmlstr);
                $(ah.config.cls.detailItem).bind('click', self.AddItemFromWo);
            }
            if (self.validatePrivileges('36') === 0) {
                $(ah.config.cls.liAddNew).hide();
            }
            if (self.validatePrivileges('37') === 0) {
                $(ah.config.id.liModify).hide();
            }
            // alert(JSON.stringify(sr));
            //alert(htmlstr);
           
           // $(ah.config.id.btnRemove).hide();

        }
        $(".button-ext").each(function (i) { $(this).hide() });
        $("#itemsReceiveBatch *").attr('disabled', 'disabled');
        $(ah.config.cls.itemRequest).show();
        $(ah.config.id.searchWorkOrder).hide();

    };
    self.searchItemsResult = function (LovCategory, pageLoad) {

        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.stockList));
        if (LovCategory !== null && LovCategory !== "") {
            var arr = sr;
            var keywords = LovCategory.toUpperCase();
            var filtered = [];

            for (var i = 0; i < sr.length; i++) {
				for (var j = 0; j < keywords.length; j++) {
					if (sr[i].DESCRIPTION === null) sr[i].DESCRIPTION = "";
                    if ((sr[i].DESCRIPTION.toUpperCase()).indexOf(keywords) > -1 || (sr[i].ID_PARTS.toUpperCase()).indexOf(keywords) > -1) {
                        filtered.push(sr[i]);
                        break;
                    }
                }
            }
            sr = filtered;
        }

        $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).html('');

        if (sr.length > 0) {
            $("#noresult").hide();
            $("#pageSeparator").show();
            $("#page1").show();
            $("#page2").show();
            $("#nextPage").show();
            $("#priorPage").show();
            var totalrecords = sr.length;
			var pagenum = Math.ceil(totalrecords / 15);
			
			if (pagenum < 2 && pagenum > 1) {
				
                pagenum = 2;
			}
			
			$("#totNumber").text(pagenum.toString());
           

            if (pagenum <= 1) {
                $("#page1").hide();
                $("#page2").hide();
                $("#pageSeparator").hide();
                $("#nextPage").hide();
                $("#priorPage").hide();

            }
            var htmlstr = '';
            var o = pageLoad;

            for (var i = 0; i < 15; i++) {
                htmlstr += '<li>';
                htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                if (sr[o].QTY !== null) {
                    htmlstr += ah.formatString(ah.config.html.searchExistRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].ID_PARTS, sr[o].DESCRIPTION, " Over all Stock on Hand:" + sr[o].QTY);
                } else {
                    htmlstr += ah.formatString(ah.config.html.searchLOVRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].ID_PARTS, sr[o].DESCRIPTION, "");
                }
                htmlstr += '</span>';
                htmlstr += '</li>';
                o++
                if (totalrecords === o) {
                    break;
                }
            }
           // alert(JSON.stringify(sr));
            //alert(htmlstr);
            $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).append(htmlstr);
        }
        else {
            $("#page1").hide();
            $("#page2").hide();
            $("#pageSeparator").hide();
            $("#nextPage").hide();
            $("#priorPage").hide();
            $("#noresult").show();
        }
        $(ah.config.cls.detailItem).bind('click', self.addItemReq);
        ah.toggleDisplayModereq();
        //ah.toggleAddItems();
    };
    self.searchResult = function (jsonData) {


        var sr = jsonData.AM_PURCHASEREQUEST;
       // alert(JSON.stringify(sr));
        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {
                var transDate = new Date(sr[o].REQ_DATE);
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].REQUEST_ID, sr[o].REQUEST_ID, 'Request Date: ' + sr[o].REQ_DATE, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total Items: ' + sr[o].ITEMREQ + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Work Order No.:&nbsp;&nbsp;' + sr[o].REF_WO + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Status:&nbsp;&nbsp;' + sr[o].STATUS + "&nbsp;&nbsp;&nbsp; Work Order Type:&nbsp;&nbsp;" + sr[o].PROBLEM_TYPE );
                //htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.inspectionprocess, sr[o].PROCESS);

                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.detailItem).bind('click', self.readPurchaseRequest);
        ah.displaySearchResult();

    };
    self.searchResultWOSpareParts = function (jsonData) {
        //alert(JSON.stringify(jsonData));
        var sr = jsonData.AM_PURCHASEREQUESTWOPARTS;
       
        $(ah.config.id.searchRequestParts + ' ' + ah.config.tag.ul).html('');



        if (sr.length > 0) {
            var htmlstr = '';
            $('#noresultHDR').hide();
            for (var o in sr) {

               
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, JSON.stringify(sr[o]), "Wor Order No.&nbsp; " + sr[o].REF_WO, "Request Date&nbsp; " + sr[o].WO_REQDATE, "");
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.problem, sr[o].WO_PROBLEM + "&nbsp;&nbsp; ");
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.partsRequest, sr[o].SPAREPARTS + "&nbsp;&nbsp; ");
                htmlstr += '</li>';

            }


        }
        $(ah.config.id.searchRequestParts + ' ' + ah.config.tag.ul).append(htmlstr);
        $(ah.config.cls.detailItem).bind('click', self.searchwoSpareParts);
       
    };
    self.searchwoSpareParts = function () {
    
        var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfoID).toString());
      
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;
       
        var reqID = infoID.REF_WO;
        $("#REF_WO").val(reqID);
        $("#REF_ASSETNO").val(infoID.WO_ASSET_NO);
        
        postData = { CLIENTID: staffDetails.CLIENT_CODE, SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), REQNO: reqID, STAFF_LOGON_SESSIONS: staffLogonSessions, SEARCH_FILTER: self.searchFilter, STATUS: "" };
        $.post(self.getApi() + ah.config.api.searchwoMaterial, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);


    };
    self.readPurchaseRequestExist = function (refId) {

 
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { REQUEST_ID: refId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readPurchaseReq, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.readPurchaseRequest = function () {

        var infoID = this.getAttribute(ah.config.attr.datainfoID).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { REQUEST_ID: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readPurchaseReq, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.readAssetID = function () {

        var infoID = $("#REF_ASSETNO").val();

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

     

        postData = { ASSET_NO: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readAssetInfo, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.readSetupExistID = function (refId) {


        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { AM_INSPECTION_ID: refId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.statusApprove = function () {
        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.reqList);
        var receviceLists = JSON.parse(jsonObj);
        //alert(JSON.stringify(receviceLists));
        if (receviceLists.length <= 0) {
            alert('Unable to proceed. No item to approve.')
            return;
        }
        var lThanzero = receviceLists.filter(function (item) { return item.PR_REQUEST_QTY.toString() === "-0" || item.PR_REQUEST_QTY <= -1 });

        if (lThanzero.length > 0) {

            alert('Unable to proceed. Item name: ' + lThanzero[0].DESCRIPTION + ', have negative request qty.')
            return;
        }

        self.updateStatus("APPROVED");
    }
    self.statusCancel = function () {
        var reasonStatus = $("#STATUSREASON").val();
        
        if (reasonStatus == null || reasonStatus == "") {
          
            alert('Please provide valid reason to proceed.');
            return;
        }

        window.location.href = ah.config.url.modalClose;
        self.updateStatus("CANCELLED");
    }
    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;
        self.reqList.splice(0, 5000);
       // self.clearTableData();
        ah.toggleSearchMode();

		var fromDate = $('#requestFromDate').val().split('/');
		
        if (fromDate.length == 3) {
            self.searchFilter.fromDate(fromDate[2] + "/" + fromDate[1] + "/" + fromDate[0]);
        }
        var toDate = $('#requestStartDate').val().split('/');
        if (toDate.length == 3) {
            self.searchFilter.toDate(toDate[2] + "/" + toDate[1] + "/" + toDate[0] + "  23:00:00.000");
        }

		postData = { SEARCH: $("#txtGlobalSearch").val(), RETACTION: "Y", STAFF_LOGON_SESSIONS: staffLogonSessions, SEARCH_FILTER: self.searchFilter };
		//alert(JSON.stringify(postData));
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.SearchNowWorkOrder = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        //ah.toggleSearchItems();
        postData = { SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchWOparts, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = ah.config.url.modalWOParts;
    };
    self.validatePrivileges = function (privilegeId) {
        var groupPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));

        var groupPrivileges = groupPrivileges.filter(function (item) { return item.PRIVILEGES_ID === privilegeId });
        return groupPrivileges.length;
    };
    self.clearTableData = function () {
        var new_tbody = document.createElement('tbody');
        populate_with_new_rows(new_tbody);
        old_tbody.parentNode.replaceChild(new_tbody, old_tbody)
    };
    self.updateStatus = function (StatusAction) {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var statusModify = $("#RESMODIFY").val();
        var reasonStatus = $("#STATUSREASON").val();
        $("#STATUSID").val(StatusAction);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var prRequestDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var postData;
        
        $(ah.config.cls.liModify + ',' + ah.config.id.contentSubHeader).hide();

        var jDate = new Date();
        var statusDate = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        statusDate = statusDate + ' ' + hourmin;
       
        self.modifyInfo.StatusDate = statusDate;
        prRequestDetails.STATUS = StatusAction;
        if (StatusAction === "APPROVED") {
            prRequestDetails.APPROVED_BY = staffLogonSessions.STAFF_ID;
            prRequestDetails.APPROVED_DATE = statusDate;
        } else if (StatusAction === "CANCELLED" && statusModify == "CANCEL"){
            prRequestDetails.CANCELLED_BY = staffLogonSessions.STAFF_ID;
            prRequestDetails.CANCELLED_DATE = statusDate;
            prRequestDetails.CANCELLED_REASON = reasonStatus;
        } else if (StatusAction === "CANCELLED" && statusModify == "REOPEN") {
            prRequestDetails.RETURNSTATUS_REASON = reasonStatus;
            prRequestDetails.RETURNSTATUS_BY = staffLogonSessions.STAFF_ID;
            prRequestDetails.RETURNSTATUS_DATE = statusDate;
            prRequestDetails.STATUS = "REQUEST";
        }


        postData = { AM_PURCHASEREQUEST:prRequestDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
       
        ah.toggleSaveMode();
        $.post(self.getApi() + ah.config.api.updateStatus, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

       
    }
    self.saveInfo = function () {
        
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var prRequestDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        
       

        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.reqList);

        

        var prRequestLists = JSON.parse(jsonObj);
        //
       
      
        if (prRequestLists.length <= 0){
            alert('Unable to proceed. No item to request. Please click on Add item button and select parts you wish to purchase.')
            return;
        }
        for (var i = 0; i < prRequestLists.length; i++) {
            if (prRequestLists[i].QTY < 1)
                return alert("Please make sure that the " + prRequestLists[i].DESCRIPTION +" qty value is greater than 0.");
        }

        var lThanzero = prRequestLists.filter(function (item) { return item.PR_REQUEST_QTY.toString() === "-0" || item.PR_REQUEST_QTY <= -1 });

        if (lThanzero.length > 0) {

            alert('Unable to proceed. Item name: ' + lThanzero[0].DESCRIPTION + ', have negative request qty. Please make sure that there is no negative qty')
            return;
        }
        //var refId = $("#AM_PRREQITEMID").val();

        //if (refId === null || refId === "") {
            var reqWO = $("#REF_WO").val();
            if (reqWO === null || reqWO ==="") {
                receviceLists[0].REF_WO = 0;
            } else {
                prRequestLists[0].REF_WO = $("#REF_WO").val(); 
            }
            var reqDate = $("#REQ_DATE").val();

            var strTime = '';
            var apptDate = reqDate;
            reqDate = apptDate.substring(6, 10) + '-' + apptDate.substring(3, 5) + '-' + apptDate.substring(0, 2);
           
            strTime = String(apptDate).split(' ');
            strTime = strTime[1].substring(0, 5);
            reqDate = reqDate + ' ' + strTime;
            var storeID = $("#STORE_ID").val();
            if (storeID === null || storeID === "") {
                storeID = 0;
            }

            prRequestDetails.REQ_DATE = reqDate;

            prRequestLists[0].REQ_DATE = reqDate;
            prRequestLists[0].REQ_DETFLAG = 'Y';
            prRequestLists[0].STATUS = $("#STATUS").val();
            prRequestLists[0].REF_ASSETNO = $("#REF_ASSETNO").val();
            prRequestLists[0].REQ_DEPT = $("#REQ_DEPT").val();
            prRequestLists[0].REMARKS = $("#REMARKS").val();
            prRequestLists[0].STORE_ID = storeID;
        //}
           
        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        strDateStart = strDateStart + ' ' + hourmin;

        actionStr = $("#POSTID").val();
       
        if (ah.CurrentMode == ah.config.mode.add) {
            prRequestDetails.CREATED_BY = staffLogonSessions.STAFF_ID;
            prRequestDetails.CREATED_DATE = strDateStart;
            postData = { AM_PURCHASEREQUEST: prRequestDetails, ACTION: actionStr, CREATE_DATE: strDateStart, AM_PARTSREQUEST: prRequestLists, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.createRequest, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }else{
            postData = { AM_PURCHASEREQUEST: prRequestDetails, AM_PARTSREQUEST: prRequestLists, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.updatePurchaseReq, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        ah.toggleSaveMode();
     

          // return;
           
           
        //}

    };
    self.readPurchaseRequestRefId = function (refId) {

        
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { REQUEST_ID: refId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readPurchaseReq, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.webApiCallbackStatus = function (jsonData) {


       // alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {
            if (jsonData.ERROR.ErrorText === "Work Order not found.") {
                $("#REF_WO").val(null);
                alert(jsonData.ERROR.ErrorText);
            } else if (jsonData.ERROR.ErrorText === "Asset not found.") {
                $("#REF_ASSETNO").val(null);
                alert(jsonData.ERROR.ErrorText);
            }else {
                alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            }
            
            //window.location.href = ah.config.url.homeIndex;
            //alert(jsonData.ERROR.ErrorText);
        }
        else if (jsonData.AM_PARTSREQUEST || jsonData.AM_PURCHASEREQUESTWOPARTS || jsonData.STORES || jsonData.AM_ASSET_DETAILS || jsonData.AM_WORK_ORDERS || jsonData.PRODUCT_STOCKONHAND || jsonData.AM_PURCHASEREQUEST || jsonData.STAFF || jsonData.SUCCESS) {
            //alert(ah.CurrentMode);
            if (ah.CurrentMode == ah.config.mode.save) {
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();
               
                    sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_PURCHASEREQUEST));

                    if (jsonData.SUCCESS) {
                        $(ah.config.cls.liApiStatus).hide();
                        self.showDetails(false);
                    }
                    else {
                        $(ah.config.cls.liApiStatus).hide();
                        self.showDetails(true);
                        
                    }
           
            }
            else if (ah.CurrentMode == ah.config.mode.deleted) {

                $(ah.config.id.successMessage).html(systemText.deletedSuccessMessage);
                ah.showSavingStatusSuccess();
                ah.toggleDisplayMode();
            }
            else if (ah.CurrentMode == ah.config.mode.add || ah.CurrentMode == ah.config.mode.edit) {
                if (jsonData.AM_PARTSREQUEST) {
                    self.addWOItems(jsonData);
                } else {
                    self.searchResultWOSpareParts(jsonData);
                }
                
            
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
               // alert(JSON.stringify(jsonData));
                sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_PURCHASEREQUEST));
                self.showDetails(true,jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
                if (jsonData.PRODUCT_STOCKONHAND) {
                    sessionStorage.setItem(ah.config.skey.stockList, JSON.stringify(jsonData.PRODUCT_STOCKONHAND));
                    $(ah.config.id.loadNumber).text("1");
                    $(ah.config.id.pageCount).val("1");
                    $(ah.config.id.pageNum).val(0);
                    self.searchItemsResult("", 0);
                    $(ah.config.cls.liApiStatus).hide();
                    $(ah.config.cls.liSave).show();
                } else {
                    self.searchResult(jsonData);
                }
              

            }
            else if (ah.CurrentMode == ah.config.mode.idle) {
                var store = jsonData.STORES;
                $.each(store, function (item) {
                    $(ah.config.id.storeID)
                        .append($("<option></option>")
                        .attr("value", store[item].STORE_ID)
                        .text(store[item].DESCRIPTION));
                });

                var reqNO = ah.getParameterValueByName(ah.config.fld.prNo);
                if (reqNO !== null && reqNO !== "") {
                    self.readPurchaseRequestRefId(reqNO);
                } 
                var prReq = ah.getParameterValueByName(ah.config.fld.prRequest);
                if (prReq !== null && prReq == "REQ") {
                    $("#filterStatus").val("REQUEST");
                    self.searchFilter.status = "REQUEST";
                    self.searchNow();
                } 
                if (prReq !== null && prReq == "APPROVED") {
                    $("#filterStatus").val("APPROVED");
                    self.searchFilter.status = "APPROVED";
                    self.searchNow();
                } 
                

            }

        }
        else {

            alert(systemText.errorTwo);
            //window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};