﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            detailItem: '.detailItem',
            searhButton: '.searhButton',
            lookupDetailItem: '.lookupDetailItem',
            info1: '.info1',
            info2: '.info2',
            info3: '.info3',
            info4: '.info4',
            info5: '.info5',
            info6: '.info6',
            infoRiskLS: '.infoRiskLS',
            infoRiskUMR: '.infoRiskUMR',
            infoRiskME: '.infoRiskME',
            iconDet: '.iconDet',
            searchIcon: '.searchIcon',
            riskScoreInput: '.riskScoreInput',
            pagingDiv: '.pagingDiv',
          
            liClone: '.liClone',
            lisort: '.lisort',
            liPrint: '.liPrint'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul',
            tbody: 'tbody',
            inputTypeNumber: 'input[type=number]'
        },
        tagId: {
            btnCSummary: 'btnCSummary',
            btnLCycle: 'btnLCycle',
            btnHOperation: 'btnHOperation',
            btnANotes: 'btnANotes',
            btnSComponent: 'btnSComponent',
            btnAPersonnel: 'btnAPersonnel',
            btnCagetory: 'btnCagetory',
            btnManufacturer: 'btnManufacturer',
            btnManufacturerModel: 'btnManufacturerModel',
            btnSupplier: 'btnSupplier',
            btnBuilding: 'btnBuilding',
            btnCostCenter: 'btnCostCenter',
            btnResponsibleCenter: 'btnResponsibleCenter',
            btnLocation: 'btnLocation',
            btnDepreciation: 'btnDepreciation',
            lookUpnextPage: 'lookUpnextPage',
            lookUppriorPage: 'lookUppriorPage',
            lookUptxtGlobalSearchOth: 'lookUptxtGlobalSearchOth',
            PaginGnextPage: 'PaginGnextPage',
            PaginGpriorPage: 'PaginGpriorPage',
            searchAllLOVList: 'searchAllLOVList'

        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,

            save: 4,
            read: 5,
            display: 6,
            edit: 7,
            advanceSearch: 8
        },
        id: {
            assetPrintSearchResultList: '#assetPrintSearchResultList',
            loadingBackground: '#loadingBackground',
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmInfo: '#frmInfo',
            itemInfo: "#itemInfo",
            assetCategory: '#CATEGORY',
            staffID: '#STAFF_ID',
            serviceID: '#SERVICE_ID',
            searchResultLookUp: '#searchResultLookUp',
            successMessage: '#successMessage',
            serviceDept: '#SERV_DEPT',
            assetSpecialty: '#SPECIALTY',
            assetClass: '#ASSET_CLASSIFICATION',
            lovAction: '#LOVACTION',
            assetManufacturer: '#MANUFACTURER',
            assetSupplier: '#SUPPLIER',
            assetBuilding: '#BUILDING',
            assetFacility: '#FACILITY',
            assetCostCenter: '#COST_CENTER',
            assetResponsibleCenter: '#RESPONSIBLE_CENTER',
            assetCondition: '#CONDITION',
            assetLocation: '#LOCATION',
            returnWO: '#RETURNWO',
            historyWO: '#historyWO',
            empPrimary: '#PRIMARY_EMPLOYEE',
            empSecondary: '#SECONDARY_EMPLOYEE',
            riskGroup: '#RISKGROUP',
            riskCLA: '#RISKCLA',
            riskCLS: '#RISKCLS',
            riskCMT: '#RISKCMT',
            riskEQF: '#RISKEQF',
            riskESE: '#RISKESE',
            riskEVS: '#RISKEVS',
            riskEVT: '#RISKEVT',
            riskFPF: '#RISKFPF',
            riskLIS: '#RISKLIS',
            riskMAR: '#RISKMAR',
            riskPCA: '#RISKPCA',
            inforiskGroup: '#INFORISKGROUP',
            inforiskCLA: '#INFORISKCLA',
            inforiskCLS: '#INFORISKCLS',
            inforiskCMT: '#INFORISKCMT',
            inforiskEQF: '#INFORISKEQF',
            inforiskESE: '#INFORISKESE',
            inforiskEVS: '#INFORISKEVS',
            inforiskEVT: '#INFORISKEVT',
            inforiskFPF: '#INFORISKFPF',
            inforiskLIS: '#INFORISKLIS',
            inforiskMAR: '#INFORISKMAR',
            inforiskPCA: '#INFORISKPCA',
            btnManufacturerModel: '#btnManufacturerModel',
            inforiskCLAScore: '#INFORISKCLASCORE',
            inforiskCLSScore: '#INFORISKCLSSCORE',
            inforiskCMTScore: '#INFORISKCMTSCORE',
            inforiskEQFScore: '#INFORISKEQFSCORE',
            inforiskESEScore: '#INFORISKESESCORE',
            inforiskEVSScore: '#INFORISKEVSSCORE',
            inforiskEVTScore: '#INFORISKEVTSCORE',
            inforiskFPFScore: '#INFORISKFPFSCORE',
            inforiskLISScore: '#INFORISKLISSCORE',
            inforiskMARScore: '#INFORISKMARSCORE',
            inforiskPCAScore: '#INFORISKPCASCORE',
            riskFactorScore: '#riskFactorScore',
            lookUptxtGlobalSearchOth: '#lookUptxtGlobalSearchOth',
            lookUppageNum: '#lookUppageNum',
            lookUppageCount: '#lookUppageCount',
            warranty_cover: '#WARRANTY_INCLUDE',
            filterCondition: '#filterCondition',
            PaginGpageNum: '#PaginGpageNum',
            PaginGpageCount: '#PaginGpageCount',
            assetModify: '#ASSETMODIFY',
            addButtonIcon: '#addButtonIcon',
            noteHistory: '#noteHistory',
            noteEntry: '#noteEntry',
            woReferenceNotes: '#WO_REFERENCE_NOTES',
            warrantyList: '#warrantyList',
            addWarranty: '#addWarranty',
            UpdateButtonIcon: '#UpdateButtonIcon',
            addConeButtonIcon: '#addConeButtonIcon',
            sortPage: '#SORTPAGE',
            costcenterStrList: '#costcenterStrList',
            manufacturerStrList: '#manufacturerStrList',
            supplierStrList: '#supplierStrList',
            printSummary: '#printSummary',
            searchDepreciationCal: '#searchDepreciationCal',
            searchResultSummaryCount: '#searchResultSummaryCount'
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID',
            datalookUp: 'data-lookUp'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'searchResult',
            assetDetails: 'AM_ASSET_DETAILS',
            assetManufacturerList: 'AM_MANUFACTURER',
            lovList: 'LOV_LOOKUPS',
            assetSupplierList: 'AM_SUPPLIER',
            assetNO: 'ASSET_NO',
            groupPrivileges: 'GROUP_PRIVILEGES',
            woAssetSummary: 'ASSET_WOSUMMARY_V',
            assetManufacturerModelList: 'AM_MANUFACTURER_MODEL'
        },
        url: {
            homeIndex: '/Home/Index',
            modalClose: '#',
            aimsHomepage: '/Menuapps/aimsIndex',
            aimsDetails: '/aims/assetInfo#',
            modalFilter: '#openModalFilter',
            aimsInformation: '/aims/assetInfo',
            openModalInfo: '#openModalInfo'
      
        },
        fld: {
            assetNoInfo: 'ASSETNO',
            assetDescInfo: 'ASSETDESC',
            reqWONo: 'REQNO',
            fromDashBoard: 'asdfxhsadfsdfx'
        },
        api: {

            readSetup: '/AIMS/ReadAssetInfo',
            searchAll: '/Reports/SearchAssetsDP',
            searchCostSumm: '/AIMS/SearchAssetConstSummary',
            updateSetup: '/AIMS/UpdateAssetInfo',
            searchLov: '/AIMS/SearchAimsLOV',
            searchLovInitialize: '/AIMS/SearchAimsLOVInitialize',
            searchManufacturer: '/AIMS/SearchManufacturerLookUp',
            searchSupplier: '/AIMS/SearchSuppier',
            getListLOV: '/Search/SearchListOfValueCategory',
            searchStaffList: '/Staff/SearchStaffLookUp',
            searchModel: '/Search/SearchModelManufacturer',
            searchLOVQuery: '/AIMS/SearchAimsLOVQuery',

            readClientInfo: '/Setup/ReadClientInfo',
            getAssetPartsDP: '/Reports/SearchByAssetPartsYR'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplate: '<span class="search-row-label fc-light-grey-2">{0}: <span class="search-row-data">{1}</span></span>',
            searchExistRowHeaderTemplate: "<a href='#' class='fs-medium-normal lookupDetailItem fc-slate-blue' data-lookUp='{0}' >&nbsp;&nbsp;&nbsp;&nbsp;  <i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} </span></a>",
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;
    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };
    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liPrint + ',' +  thisApp.config.cls.pagingDiv + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess + ',' + thisApp.config.cls.iconDet + ',' + thisApp.config.id.returnWO + ',' + thisApp.config.id.addWarranty
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome + ',' + thisApp.config.cls.lisort).show();
     

        thisApp.CurrentMode = thisApp.config.mode.idle;
     

    };
    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    thisApp.toggleAddExistMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;


        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.searhButton).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.statusText +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.iconDet + ',' + thisApp.config.cls.divProgress
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        //$(
        //    thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        // ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("LOCATION_DATE").disabled = false;
        document.getElementById("INSERVICE_DATE").disabled = false;
        document.getElementById("CONDITION_DATE").disabled = false;
        //document.getElementById("WARRANTY_EXPIRYDATE").disabled = false;
        //document.getElementById("TERM_PERIOD").disabled = false;
        document.getElementById("PURCHASE_COST").disabled = false;

    };
    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.liClone + ',' + thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.liSave
        ).hide();
       

    };
    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.lisort).show();
        $(thisApp.config.cls.liApiStatus).hide();
        $(thisApp.config.cls.lisort).removeClass(thisApp.config.cssCls.viewMode);
        $(thisApp.config.id.sortPage).prop(thisApp.config.attr.disabled, false);

    };
    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.searhButton + ',' + thisApp.config.id.addWarranty).show();
        $(thisApp.config.cls.liClone +thisApp.config.cls.pagingDiv + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("LOCATION_DATE").disabled = false;
        document.getElementById("INSERVICE_DATE").disabled = false;
        document.getElementById("CONDITION_DATE").disabled = false;
        //document.getElementById("WARRANTY_EXPIRYDATE").disabled = false;
        //document.getElementById("TERM_PERIOD").disabled = false;
        document.getElementById("PURCHASE_COST").disabled = false;
        document.getElementById("CATEGORYDESC").disabled = true;
        document.getElementById("MANUFACTURERDESC").disabled = true;
        document.getElementById("SUPPLIERDESC").disabled = true;
        document.getElementById("BUILDINGDESC").disabled = true;
        document.getElementById("RESPONSIBLE_CENTERDESC").disabled = true;
        document.getElementById("COST_CENTERDESC").disabled = true;


    };
    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };
    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.searhButton + ',' + thisApp.config.id.addWarranty
        ).hide();
        document.getElementById("LOCATION_DATE").disabled = true;
        document.getElementById("INSERVICE_DATE").disabled = true;
        document.getElementById("CONDITION_DATE").disabled = true;
        //document.getElementById("WARRANTY_EXPIRYDATE").disabled = true;
        //document.getElementById("TERM_PERIOD").disabled = true;
        document.getElementById("PURCHASE_COST").disabled = true;
    };
    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.iconDet + ',' + thisApp.config.id.addWarranty
        ).show();

        $(
            thisApp.config.cls.pagingDiv + ',' + thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.searhButton + ',' + thisApp.config.id.addWarranty + ',' + thisApp.config.cls.lisort
        ).hide();

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        //$(thisApp.config.tag.inputTypeText).prop(thisApp.config.attr.readOnly, true);

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);
        document.getElementById("LOCATION_DATE").disabled = true;
        document.getElementById("INSERVICE_DATE").disabled = true;
        document.getElementById("CONDITION_DATE").disabled = true;
        //document.getElementById("WARRANTY_EXPIRYDATE").disabled = true;
        //document.getElementById("TERM_PERIOD").disabled = true;
        document.getElementById("PURCHASE_COST").disabled = true;
      

    };
    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };
    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };
    thisApp.getAge = function (birthDate) {

        var seconds = Math.abs(new Date() - birthDate) / 1000;

        var numyears = Math.floor(seconds / 31536000);
        var numdays = Math.floor((seconds % 31536000) / 86400);
        var nummonths = numdays / 30;

        return numyears + " y " + Math.round(nummonths) + " m ";
    };
    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };
    thisApp.formatJSONDateToString = function () {

        var dateToFormat = arguments[0];
        var strDate;

        if (dateToFormat === null) return "";
        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };
    thisApp.formatJSONTimeToString = function () {

        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        var timeToFormat = strTime[1].substring(0, 5);


        return timeToFormat;
    };
    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');


            if (isDateTime) {
                val = thisApp.formatJSONDateTimeToString(val);
            }

            if (isDate) {
                val = thisApp.formatJSONDateToString(val);
            }

            var $el = $('#' + name), type = $el.attr('type'), isTime = $el.attr('data-istime');
            if (isTime) {
                if (val === null || val === "") {
                    val = "00:00";
                } else {
                    val = thisApp.formatJSONTimeToString(val);
                }

            }

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var assetDepreciationViewModel = function (systemText) {

    var self = this;

    var ah = new appHelper(systemText);
    var sort = "SORTASSETDESCAC";
    var srDataArray = ko.observableArray([]);
    var warrantyList = ko.observableArray([]);
    var buildingList = ko.observableArray([]);
    var costCenterList = ko.observableArray([]);
    var lovCostCenterArray = ko.observableArray([]);
    var lovManufacturerArray = ko.observableArray([]);
    var lovSupplierArray = ko.observableArray([]);
    var partsDPUsedList = ko.observableArray([]);
    var Scostcenter = null;
    var Scloneid = null;
    var Sstatus = null;
    var Scondition = null;
    var SsearchTxt = null;
    var resultHtml = null;
    var Spono = null;
    self.warrantyLOV = ko.observableArray([]);
    self.warrantyData = ko.observableArray([]);
    self.cloneAssetData = ko.observableArray('');
    self.showRow = ko.observable(true);

    self.showLoadingBackground = function () {
        var loadingBackground = $(ah.config.id.loadingBackground);
        loadingBackground.addClass('initializing').removeClass('done');
        var offset = loadingBackground.offset();
        var height = loadingBackground.height();
        var centerY = (offset.top + height / 2) - 120;
        $('.sk-circle').css({
            'margin-top': centerY + 'px',
            'display': 'block'
        });
    };
    self.initialize = function () {


        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());



        ah.initializeLayout();

        self.showLoadingBackground();

        $.when(self.getLov()).done(function () {
            $(ah.config.id.loadingBackground).removeClass('initializing').addClass('done');
            $('.dark-bg, ' + ah.config.id.loadingBackground).css({
                'top': '90px',
                'z-index': '0'
            });
        });
    };
   

    self.dateClick = function () {
        $("#dateClickAction").val("ACTIVE");
    };
    self.getLov = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        ah.CurrentMode = ah.config.mode.idle;
        postData = { LOV: "'AM_WARRANTY','AIMS_FACILITY','AIMS_SERVICEDEPARTMENT','AIMS_SPECIALTY','AIMS_CLASS','AIMS-CONDITION','AIMS_LOCATION'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchLovInitialize, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    }
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
  
    self.assetCategory = {
        count: ko.observable()
    };
    self.searchFilter = {
        AssetFilterCondition: ko.observable(''),
        toDate: ko.observable(''),
        status: ko.observable(''),
        staffId: ko.observable(''),
        CloneId: ko.observable(''),
        CostCenterStr: ko.observable(''),
        ManufacturerStr: ko.observable(''),
        SupplierStr: ko.observable(''),
        PO_NO: ko.observable('')
    };
    self.clientDetails = {
        CLIENTNAME: ko.observable(),
        CLIENT_LOGO: ko.observable(),
        COMPANY_LOGO: ko.observable(),
        HEADING: ko.observable(),
        HEADING2: ko.observable(),
        HEADING3: ko.observable(),
        HEADING_OTH: ko.observable(),
        HEADING_OTH2: ko.observable(),
        HEADING_OTH3: ko.observable(),
        CLIENTADDRESS: ko.observable(),
        CLIENTCONTACT: ko.observable()
    };
    self.searchFilterNow = function () {
        window.location.href = ah.config.url.modalClose;
        self.searchNow();
    };
    self.clearFilter = function () {
        self.searchFilter.AssetFilterCondition('');
        self.searchFilter.toDate('');
        self.searchFilter.status('');
        self.searchFilter.CloneId('');
        self.searchFilter.CostCenterStr('');
        self.searchFilter.ManufacturerStr('');
        self.searchFilter.SupplierStr('');
        self.searchFilter.PO_NO('');

    };
    self.showModalFilter = function () {
        ah.CurrentMode = ah.config.mode.advanceSearch;
        //$("#cloneid").removeClass(ah.config.cssCls.viewMode);
        var element = document.getElementById("cloneid");
        element.classList.remove("view-mode");
        element.disabled = false;
        var element2 = document.getElementById("costcenterStr");
        element2.classList.remove("view-mode");
        element2.disabled = false;

        var element3 = document.getElementById("filterStatus");
        element3.classList.remove("view-mode");
        element3.disabled = false;

        var element4 = document.getElementById("filterCondition");
        element4.classList.remove("view-mode");
        element4.disabled = false;

        var element4 = document.getElementById("purchaseNo");
        element4.classList.remove("view-mode");
        element4.disabled = false;

        var element2 = document.getElementById("manufacturerStr");
        element2.classList.remove("view-mode");
        element2.disabled = false;

        var element2 = document.getElementById("supplierStr");
        element2.classList.remove("view-mode");
        element2.disabled = false;

        window.location.href = ah.config.url.modalFilter;
    };
    self.calculateDepreciation = function (assetNo) {
        $(ah.config.id.searchDepreciationCal + ' ' + ah.config.tag.tbody).html('');

        var htmlstr = '';

        assetInfo = srDataArray;
        assetInfo = assetInfo.filter(function (item) { return item.ASSET_NO == assetNo });
        $("#itemDescription").text(assetInfo[0].ASSET_NO + '-' + assetInfo[0].DESCRIPTION);
        $("#DEPRECIATIONPERCENT").text(assetInfo[0].DEPRECIATION_PERCENT);
        var installationDate = ah.formatJSONDateToString(assetInfo[0].Inservice_date);
        if (installationDate == "01/01/1900") {
            installationDate = '';
        }
        
        var srparts = partsDPUsedList;
        var x = 1;
        var jDate = new Date();
        var curYear = parseFloat(jDate.getFullYear());
        var calDate = installationDate.substring(6, 10);
        var newYear = "";

        newYear = parseInt(calDate);
        var pCost = assetInfo[0].PURCHASE_COST;
        var dpPercent = parseFloat(assetInfo[0].DEPRECIATION_PERCENT);
        dpPercent = dpPercent / 100;

        var newdpCost = parseFloat(pCost);

        var initialCycle = 10;
        do {
            x++
            var partsCost = 0;

            htmlstr += "<tr>";
            htmlstr += '<td class="a">' + newYear + '</td>';
            htmlstr += '<td class="a">' + newdpCost.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 }) + '</td>';


            if (srparts.length > 0) {
                var srpartsFilter = srparts;

                srpartsFilter = srpartsFilter.filter(function (item) { return item.REQ_YEAR == newYear && item.ASSET_NO == assetNo });
                if (srpartsFilter.length > 0) {
                    partsCost = srpartsFilter[0].COSTBYPARTS;
                } else {
                    partsCost = 0;
                }

            }


            htmlstr += '<td class="a">' + partsCost.toLocaleString('en') + '</td>';
            //htmlstr += '<td class="a">' + subCotract.toLocaleString('en') + '</td>';

            newYear = newYear + 1;
            newdpCost = (newdpCost - (newdpCost * dpPercent)) - parseFloat(partsCost);
            newdpCost = parseFloat(newdpCost);




            htmlstr += '<td class="a">' + newdpCost.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 }) + '</td>';
            htmlstr += "</tr>";
            // newdpCost = parseFloat(Math.round(newdpCost * 100) / 100).toFixed(2);



            if ((newYear - 1) == curYear) {
                $("#DPCURVALUE").text(newdpCost.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 }));
                break;
            }
            if (x == initialCycle) {
                if (curYear >= newYear) {
                    initialCycle++;
                }
            }
        }
        while (x < initialCycle);
        $(ah.config.id.searchDepreciationCal + ' ' + ah.config.tag.tbody).append(htmlstr);
        window.location.href = ah.config.url.openModalInfo;
       
    }
    self.getLastSearch = function () {
        SsearchTxt = $("#txtGlobalSearch").val();
        Scondition = $("#filterCondition").val();
        Sstatus = $("#filterStatus").val();
        Scloneid = $("#cloneid").val();
        Scostcenter = $("#costcenterStr").val();
        Spono = $("#purchaseNo").val();

    };
    self.populateLastsearch = function () {
        $(ah.config.id.txtGlobalSearch).val(SsearchTxt);

        $("#filterCondition").val(Scondition);
        $("#filterStatus").val(Sstatus);
        $("#cloneid").val(Scloneid);
        $("#costcenterStr").val(Scostcenter);
        $("#purchaseNo").val(Spono);


    };
    self.isValidDate = function (dateString) {
        // First check for the pattern
        var regex_date = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
        var regex_date2 = /^(\d{1,2})-(\d{1,2})-(\d{4})$/;
        if (!regex_date.test(dateString)) {
            if (!regex_date2.test(dateString)) {
                return 'Invalid';
            }
        }
        // Parse the date parts to integers
        var n = dateString.search("-");
        var parts = "";
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var newDateFormat = "";
        if (n <= 0) {
            parts = dateString.split("/");
            var day = parseInt(parts[0], 10);
            var month = parseInt(parts[1], 10);
            var year = parseInt(parts[2], 10);
            newDateFormat = (year + '/' + format_two_digits(month) + '/' + format_two_digits(day)).trim();
        } else {
            parts = dateString.split("-");
            var day = parseInt(parts[0], 10);
            var month = parseInt(parts[1], 10);
            var year = parseInt(parts[2], 10);
            newDateFormat = (year + '/' + format_two_digits(month) + '/' + format_two_digits(day)).trim();

        }

        // Check the ranges of month and year
        if (year < 1000 || year > 3000 || month == 0 || month > 12) {
            return 'Invalid';
        }

        var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        // Adjust for leap years
        if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
            monthLength[1] = 29;
        }

        // Check the range of the day
        if (day > 0 && day <= monthLength[month - 1]) {
            return newDateFormat;
        } else {
            return 'Invalid';
        }
    };
    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };
  
    self.cancelChangesModal = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
         
            window.location.href = ah.config.url.modalClose;

        }

        //var infoDetails = sessionStorage.getItem(ah.config.skey.assetDetails);
        window.location.href = ah.config.url.modalClose;
        self.privilegesValidation('NEW');
        self.privilegesValidation('MODIFY');
    };
    
    self.sorting = function () {
        if (srDataArray.length > 0) {
            sortby = $("#SORTPAGE").val();
            self.searchResult(0, sortby);
        }

    };
   
    self.pagingLoader = function () {


        var senderID = $(arguments[1].currentTarget).attr('id');



        var pageNum = parseInt($(ah.config.id.PaginGpageNum).val());
        var pageCount = parseInt($(ah.config.id.PaginGpageCount).val());

        //alert(JSON.stringify(srDataArray));
        //sr = JSON.parse(sessionStorage.getItem(ah.config.skey.assetDetails));
        var sr = srDataArray;


        var lastPage = Math.ceil(sr.length / 500);

        if (isNaN(pageCount)) {
            pageCount = 1;
        }
        if (isNaN(pageNum)) {
            pageNum = 0;
        }
        
        switch (senderID) {

            case ah.config.tagId.PaginGnextPage: {

                if (pageCount < lastPage) {
                    pageNum = pageNum + 500;
                    pageCount++;

                    var loadcount = $("#LASTLOADPAGE").val();
                    var currloadcount = loadcount;
                    loadcount = parseInt(loadcount) + 500;
                    // alert(loadcount);
                    if (sr.length < parseInt(loadcount)) {
                        var needtoLoad = parseInt(loadcount) - sr.length;
                        loadcount = currloadcount;// + needtoLoad;
                        // alert(loadcount);
                    }
                    $("#LASTLOADPAGE").val(loadcount);
                    
                    self.searchResult(loadcount, sort);
                }
                break;
            }
            case ah.config.tagId.PaginGpriorPage: {
                if (pageNum === 1) {
                    return;
                }
                if (pageNum === 0) {
                } else {
                    pageNum = pageNum - 500;
                    if (pageNum < 0) {
                        pageNum = 0;
                    }
                    pageCount = pageCount - 1;
                }
                var loadcount = $("#LASTLOADPAGE").val();
        
                if (loadcount > 1000) {
                    loadcount = parseInt(loadcount) - 500;
                } else {
                    loadcount = parseInt(loadcount) - 1000;
                }

                if (loadcount < 0) {
                    loadcount = 0
                }
                // alert(loadcount);
                $("#LASTLOADPAGE").val(loadcount);
                self.searchResult(loadcount, sort);
                break;
            }

        }

        $("#PaginGloadNumber").text(pageCount.toString());
        $("#PaginGpageCount").val(pageCount.toString());
        $("#PaginGpageNum").val(pageNum);

    };
    self.searchResult = function (pageNum, sortby) {
        ah.displaySearchResult();

        //JSON.parse(sessionStorage.getItem(ah.config.skey.assetDetails));
        //srData = sr;
        //ah.config.skey.assetDetails, JSON.stringify(jsonData.AM_ASSET_DETAILS);
        $(ah.config.id.searchResultList + ' ' + ah.config.tag.tbody).html('');
        var sr = srDataArray;
        resultHtml = null;

        if (sortby == 'SORTASSETDESCAC' && pageNum == 0) {
            sr.sort(function (a, b) {
                var textA = a.DESCRIPTION.toUpperCase();
                var textB = b.DESCRIPTION.toUpperCase();
                textA = textA.substring(0, 3);
                textB = textB.substring(0, 3);
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });
        } else if (sortby == 'SORTASSETDESCDC' && pageNum == 0) {
            sr.sort(function (a, b) {
                var textA = a.DESCRIPTION.toUpperCase();
                var textB = b.DESCRIPTION.toUpperCase();
                textA = textA.substring(0, 3);
                textB = textB.substring(0, 3);
                return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
            });
        } else if (sortby == 'SORTASSETNOAC' && pageNum == 0) {
            sr.sort(function (a, b) {

                if (a["ASSET_NO"] < b["ASSET_NO"])
                    return -1;
                if (a["ASSET_NO"] > b["ASSET_NO"])
                    return 1;
                return 0;
            });
        } else if (sortby == 'SORTASSETNODC' && pageNum == 0) {
            sr.sort(function (a, b) {

                if (a["ASSET_NO"] > b["ASSET_NO"])
                    return -1;
                if (a["ASSET_NO"] < b["ASSET_NO"])
                    return 1;
                return 0;
            });
        } else if (sortby == 'SORTCOSTCENTERAC' && pageNum == 0) {
            sr.sort(function (a, b) {
                var textA = a.COSTCENTER_DESC.toUpperCase();
                var textB = b.COSTCENTER_DESC.toUpperCase();
                textA = textA.substring(0, 3);
                textB = textB.substring(0, 3);
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });
        } else if (sortby == 'SORTCOSTCENTERDC' && pageNum == 0) {
            sr.sort(function (a, b) {
                var textA = a.COSTCENTER_DESC.toUpperCase();
                var textB = b.COSTCENTER_DESC.toUpperCase();
                textA = textA.substring(0, 3);
                textB = textB.substring(0, 3);
                return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
            });
        } else if (sortby == 'SORTSUPPLIERAC' && pageNum == 0) {

            sr.sort(function (a, b) {
                var textA = a.SUPPLIERNAME.toUpperCase();
                var textB = b.SUPPLIERNAME.toUpperCase();
                textA = textA.substring(0, 3);
                textB = textB.substring(0, 3);
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });
        } else if (sortby == 'SORTSUPPLIERDC' && pageNum == 0) {

            sr.sort(function (a, b) {
                var textA = a.SUPPLIERNAME.toUpperCase();
                var textB = b.SUPPLIERNAME.toUpperCase();
                textA = textA.substring(0, 3);
                textB = textB.substring(0, 3);
                return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
            });
        } else if (sortby == 'SORTINSDATEAC' && pageNum == 0) {
            sr.sort(function (a, b) {

                if (a["Inservice_date"] < b["Inservice_date"])
                    return -1;
                if (a["Inservice_date"] > b["Inservice_date"])
                    return 1;
                return 0;
            });
        } else if (sortby == 'SORTINSDATEDC' && pageNum == 0) {
            sr.sort(function (a, b) {

                if (a["Inservice_date"] > b["Inservice_date"])
                    return -1;
                if (a["Inservice_date"] < b["Inservice_date"])
                    return 1;
                return 0;
            });
        }


        var srLen = sr.length;
        self.assetCategory.count("No. of Records: " + srLen.toString());
        $(ah.config.id.searchResultSummaryCount).text("Number of Records: " + sr.length);
        //$(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), srLen));
        $(ah.config.cls.liPrint).hide();
        if (sr.length > 0) {
            $(ah.config.cls.liPrint).show();
            var pageNumto = pageNum + 500;
            var pagenum = Math.ceil(sr.length / 500);

            if (pageNum == 0) {
                $("#PaginGloadNumber").text(1);
                $("#PaginGpageCount").val(1);
                $("#PaginGpageNum").val(1);
                $("#LASTLOADPAGE").val(0);
            }

            //alert(pageNumto);
            //alert(pagenum);
            //sr = sr.filter(function (item) { return item.ROWNUMID >= pageNum && item.ROWNUMID <= pageNumto });

            var index = 200000;
            sr = sr.slice(pageNum, index);

            if (pagenum >= 2) {

                $(ah.config.cls.pagingDiv).show();
            } else {
                $(ah.config.cls.pagingDiv).hide();
            }

            $("#PaginGtotNumber").text(pagenum.toString());

            var htmlstr = '';
            var countsr = 0
            for (var o in sr) {
                countsr++;
                if (sr[o].DESCRIPTION !== null && sr[o].DESCRIPTION !== "") {
                    if (sr[o].DESCRIPTION.length > 100)
                        sr[o].DESCRIPTION = sr[o].DESCRIPTION.substring(0, 99) + "...";
                } else {
                    sr[o].DESCRIPTION = "";
                }
               // alert(JSON.stringify(sr));
                var installationDate = ah.formatJSONDateToString(sr[o].Inservice_date);
                if (installationDate == '01/01/1900') {
                    installationDate = '';
                }
              
                htmlstr += "<tr>";
                htmlstr += '<td class="d">' + sr[o].ASSET_NO + '</td>';
                htmlstr += '<td class="h">' + sr[o].DESCRIPTION + '</td>';
                htmlstr += '<td class="d">' + sr[o].SUPPLIERNAME + '</td>';
                htmlstr += '<td class="d">' + sr[o].SERIAL_NO + '</td>';
                htmlstr += '<td class="d">' + installationDate + '</td>';
                htmlstr += '<td class="a">' + sr[o].PURCHASE_COST.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 }) + '</td>';
               

                //var installationDate = ah.formatJSONDateToString(sr[o].Inservice_DATE);
                var srparts = partsDPUsedList;
                var x = 1;
                var jDate = new Date();
                var curYear = parseFloat(jDate.getFullYear());
                var calDate = installationDate.substring(6, 10);
                var newYear = "";

                newYear = parseInt(calDate);
                var pCost = sr[o].PURCHASE_COST;
                var dpPercent = parseFloat(sr[o].DEPRECIATION_PERCENT);
                dpPercent = dpPercent / 100;

                var newdpCost = parseFloat(pCost);

                var initialCycle = 10;
                var partsCost = 0;
                if (pCost > 0) {
                    do {
                        x++
                        
                        if (srparts.length > 0) {
                            var srpartsFilter = srparts;

                            srpartsFilter = srpartsFilter.filter(function (item) { return item.REQ_YEAR == newYear && item.ASSET_NO == sr[o].ASSET_NO});
                            if (srpartsFilter.length > 0) {
                                partsCost = srpartsFilter[0].COSTBYPARTS;
                            } else {
                                partsCost = 0;
                            }

                        } else {
                            partsCost = 0
                        }

                        newYear = newYear + 1;
                        newdpCost = (newdpCost - (newdpCost * dpPercent)) - parseFloat(partsCost);
                        newdpCost = parseFloat(newdpCost);

                        if ((newYear - 1) == curYear) {
                            break;
                        }
                        if (x == initialCycle) {
                            if (curYear >= newYear) {
                                initialCycle++;
                            }
                        }
                    }
                    while (x < initialCycle); 
                }
                       
                htmlstr += '<td class="a">' + newdpCost.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 }) + '</td>';
                htmlstr += '<td class="a no-print"><i data-bind="click:getDepreciationDet" class="fa fa-calculator button-ext detailItem " title="Click to display Details" data-infoID=' + sr[o].ASSET_NO +'></i></td>';
                htmlstr += "</tr>";


                if (countsr === 500) {
                   
                    break;
                }

            }
            resultHtml = htmlstr;
            $(ah.config.id.searchResultList + ' ' + ah.config.tag.tbody).append(htmlstr);
            $(ah.config.cls.detailItem).bind('click', self.getDepreciationDet);
        }

      
        
      
    };
    self.getDepreciationDet = function () {
        var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
        self.calculateDepreciation(infoID);
    }

    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.printSummary = function () {
        //window.location.href = ah.config.url.openModalPrintPage;
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;


        var staffInfo = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        postData = { CLIENTID: staffInfo.CLIENT_CODE, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readClientInfo, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.print = function (jsonData) {
        //alert(JSON.stringify(jsonData.AM_CLIENT_INFO));
        var clientInfo = jsonData.AM_CLIENT_INFO;

        self.clientDetails.CLIENTNAME(clientInfo.DESCRIPTION);
        self.clientDetails.CLIENT_LOGO(clientInfo.CLIENT_LOGO);
        self.clientDetails.COMPANY_LOGO(clientInfo.COMPANY_LOGO);
        self.clientDetails.HEADING(clientInfo.HEADING);
        self.clientDetails.HEADING2(clientInfo.HEADING2);
        self.clientDetails.HEADING3(clientInfo.HEADING3);
        self.clientDetails.HEADING_OTH(clientInfo.HEADING_OTH);
        self.clientDetails.HEADING_OTH2(clientInfo.HEADING_OTH2);
        self.clientDetails.HEADING_OTH3(clientInfo.HEADING_OTH3);
        self.clientDetails.CLIENTADDRESS(clientInfo.ADDRESS + ', ' + clientInfo.CITY + ', ' + clientInfo.ZIPCODE + ', ' + clientInfo.STATE);
        self.clientDetails.CLIENTCONTACT('Tel.No: ' + clientInfo.CONTACT_NO1 + ' Fax No:' + clientInfo.CONTACT_NO2);

        document.getElementById("printSummary").classList.add('printSummary');
        $(ah.config.id.assetPrintSearchResultList).html('');
        $(ah.config.id.assetPrintSearchResultList).append($(ah.config.id.searchResultList).html());
        setTimeout(function () {
            window.location.href = ah.config.url.modalClose;
            window.print();

        }, 4000);
    };
    self.getNow = function () {
        var now = moment();

        return now;
    };
    self.isDateFuture = function (testDate, tolerance) {
        var now = self.getNow();
        return testDate.diff(now, 'days') >= tolerance;
    };
    self.isDateFutreCompare = function (date1, date2) {
        if (date1 < date2) {
            return true;
        }
        return false;

    }
    self.searchNow = function () {
        self.getLastSearch();
        assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        if (assetNoID !== null && assetNoID !== "") {
            urlText = 'ljllk';
            history.pushState('', 'New Page Title', ah.config.url.aimsDetails);
        }


        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
        var postData;

        gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "15" });
        if (gPrivileges.length <= 0) {
            self.searchFilter.staffId = staffLogonSessions.STAFF_ID;
        }

        ah.toggleSearchMode();
        var searchStr = $(ah.config.id.txtGlobalSearch).val().trim();
        postData = { SEARCH_FILTER: self.searchFilter, SEARCH: searchStr.toString(), STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.searchAdvanceLOV = function () {
        if (lovCostCenterArray.length == 0 || lovManufacturerArray.length == 0 || lovSupplierArray.length == 0) {
            var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
            var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
            var postData;
            postData = { LOV: "'AIMS_COSTCENTER'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
    };
   
    self.privilegesValidation = function (actionVal) {

        var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
        if (actionVal === 'NEW') {
            gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "16" });
            if (gPrivileges.length <= 0) {
                $(ah.config.cls.liAddNew).hide();
            }
        } else if (actionVal === 'MODIFY') {
            gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "3" });
            if (gPrivileges.length <= 0) {
                $(ah.config.id.assetModify + ',' + ah.config.id.addButtonIcon).hide();
            }
        } else if (actionVal === 'NEWCLONE') {
            gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "29" });

            if (gPrivileges.length <= 0) {

                $("#liClone").hide();
                $("#liClone2").hide();
            }
       
        }
    }
    self.webApiCallbackStatus = function (jsonData) {
        //alert(JSON.stringify(jsonData));
        //alert(JSON.stringify(jsonData.AM_MANUFACTURER));
        if (jsonData.ERROR) {
            var errorText = jsonData.ERROR.ErrorText;
            var res = errorText.substring(0, 12);
            
            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
           
        }
        else if (jsonData.PARTS_GROUPBYASSETYR_V || jsonData.AM_CLIENT_INFO || jsonData.AM_ASSET_DETAILSCLONE     || jsonData.AM_MANUFACTURER_MODEL || jsonData.STAFFLIST ||  jsonData.AM_SUPPLIER || jsonData.AM_MANUFACTURER || jsonData.LOV_LOOKUPS || jsonData.AM_ASSET_DETAILS || jsonData.STAFF || jsonData.SUCCESS) {


            $("#ASSET_NO").next("span").remove();

            if (ah.CurrentMode == ah.config.mode.save) {
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();

                if (jsonData.SUCCESS) {
                
                    window.location.href = ah.config.url.modalClose;
               
                }
                else {
                 
                
                }
            }
            else if (ah.CurrentMode == ah.config.mode.searchResult) {
                self.print(jsonData);
            }

            else if (ah.CurrentMode == ah.config.mode.read) {
              
            } else if (ah.CurrentMode == ah.config.mode.advanceSearch) {
                lovCostCenterArray = jsonData.LOV_LOOKUPS;
                $.each(lovCostCenterArray, function (item) {

                    $(ah.config.id.costcenterStrList)
                        .append($("<option>")
                            .attr("value", lovCostCenterArray[item].DESCRIPTION.trim())
                            .attr("id", lovCostCenterArray[item].LOV_LOOKUP_ID));
                });

                lovManufacturerArray = jsonData.AM_MANUFACTURER;
                $.each(lovManufacturerArray, function (item) {

                    $(ah.config.id.manufacturerStrList)
                        .append($("<option>")
                            .attr("value", lovManufacturerArray[item].DESCRIPTION.trim())
                            .attr("id", lovManufacturerArray[item].MANUFACTURER_ID));
                });

                lovSupplierArray = jsonData.AM_SUPPLIER;
                $.each(lovSupplierArray, function (item) {

                    $(ah.config.id.supplierStrList)
                        .append($("<option>")
                            .attr("value", lovSupplierArray[item].DESCRIPTION.trim())
                            .attr("id", lovSupplierArray[item].SUPPLIER_ID));
                });

            }
            else if (ah.CurrentMode == ah.config.mode.display || ah.CurrentMode == ah.config.mode.edit) {

                if (jsonData.STAFFLIST) {
                    var empPrimarycurr = $(ah.config.id.empPrimary).val();
                    var empSecondarycurr = $(ah.config.id.empSecondary).val();
                    document.getElementById("PRIMARY_EMPLOYEE").options.length = 0;

                    $.each(jsonData.STAFFLIST, function (item) {

                        $(ah.config.id.empPrimary)
                            .append($("<option></option>")
                                .attr("value", jsonData.STAFFLIST[item].STAFF_ID)
                                .text(jsonData.STAFFLIST[item].FIRST_NAME + ' ' + jsonData.STAFFLIST[item].LAST_NAME));
                    });
                    document.getElementById("SECONDARY_EMPLOYEE").options.length = 0;
                    $.each(jsonData.STAFFLIST, function (item) {

                        $(ah.config.id.empSecondary)
                            .append($("<option></option>")
                                .attr("value", jsonData.STAFFLIST[item].STAFF_ID)
                                .text(jsonData.STAFFLIST[item].FIRST_NAME + ' ' + jsonData.STAFFLIST[item].LAST_NAME));
                    });

                    $(ah.config.id.empPrimary).val(empPrimarycurr);
                    $(ah.config.id.empSecondary).val(empSecondarycurr);
                
                } else if (jsonData.PARTS_GROUPBYASSETYR_V) {
                    partsDPUsedList = jsonData.PARTS_GROUPBYASSETYR_V;
                    self.calculateDepreciation();
                
             
                
               
                }

            }
            else if (ah.CurrentMode == ah.config.mode.search || ah.CurrentMode == ah.config.mode.add) {
                partsDPUsedList = jsonData.PARTS_GROUPBYASSETYR_V;
                    $("#PaginGloadNumber").text("1");
                    $("#PaginGpageCount").val("1");
                    $("#PaginGpageNum").val(0);
                    $("#LASTLOADPAGE").val(0);
                    srDataArray = jsonData.AM_ASSET_DETAILS;
                    var sortby = $("#SORTPAGE").val();
                    self.searchResult(0, sortby);
      

            }

            else if (ah.CurrentMode == ah.config.mode.idle) {
                
            }

        }
        else {

            alert(systemText.errorTwo);
            //window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};