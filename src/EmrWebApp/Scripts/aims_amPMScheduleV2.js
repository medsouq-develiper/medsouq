﻿var appHelper = function (systemText) {
     
    var thisApp = this;
    
    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divInfo: '.divInfo',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            detailItem: '.detailItem'
        },
        fld: {
            assetNoInfo: 'ASSETNO',
            assetDescInfo: 'ASSETDESC',
            reqWONo: 'REQNO',
            installDate: 'installDate'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmInfo: '#frmInfo',
            frmInfoParts: '#frmInfoParts',
			serviceDept: '#SERVICE_DEPARTMENT',
            staffID: '#STAFF_ID',
            serviceID: '#SERVICE_ID',
            successMessage: '#successMessage',
            winTitle: '#winTitle',
            searchResult1: '#searchResult1',
            partsaveInfoParts: "#partsaveInfoParts",
            txtGlobalSearchOth: "#txtGlobalSearchOth",
            pageNum: '#pageNum',
			pageCount: '#pageCount',
			pmProcedureId: '#PM_PROCEDURE',
			modifyId: '#modifyId',
			newId: '#newId',
            addButtonId: '#addButtonId',
            searchResultPMDoc: '#searchResultPMDoc'

        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID',
            datainfoPartID: 'data-infoPartID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'searchResult',
            infoDetails: 'AM_SUPPLIER',
			partList: "AM_PARTS",
			groupPrivileges: 'GROUP_PRIVILEGES',
            pmParts: "AM_WOPM_PARTS"
        },
        url: {
            homeIndex: '/Home/Index',
            aimsPMSchedule: 'aims/am_PMSchedule',
            aimsInformation: '/aims/assetInfo',
            modalLookUp: 'openModalLookup',
            modalClose: '#',
            modalAddItems: '#openModal',
            aimsHomepage: '/Menuapps/aimsIndex'
        },
        tagId: {
            
            txtGlobalSearchOth: 'txtGlobalSearchOth',
            priorPage: 'priorPage',
            nextPage: 'nextPage'
        },
        api: {

            readSetup: '/AIMS/ReadPMSchedule',
            searchAll: '/AIMS/SearchPMSchedule',
            createSetup: '/AIMS/CreateNewSchedule',
            createMaterials: '/AIMS/CreateNewWOPMPart',
            updateSetup: '/AIMS/UpdatePMSchedule',
			partssearchAll: '/AIMS/SearchAmPartsWOPMPart',
            searchLov: '/AIMS/SearchPMLookUp',
            createImagePM: '/AIMS/CreateAssetPMImages'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            searchRowHeaderTemplateList: "<a href='#openModal' class='fs-medium-normal detailItem' data-infoPartID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            
            searchRowHeaderDOCTemplateList: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $( thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;
        document.getElementById("TIME_PERFORM").disabled = true;
        document.getElementById("SESSION_START").disabled = true;
        document.getElementById("SESSION_END").disabled = true;
        document.getElementById("NEXT_PM_DUE").disabled = true;
        document.getElementById("GRACE_PERIOD").disabled = true;

    };

    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("TIME_PERFORM").disabled = false;
        document.getElementById("SESSION_START").disabled = false;
        document.getElementById("SESSION_END").disabled = false;
        document.getElementById("NEXT_PM_DUE").disabled = false;
        document.getElementById("GRACE_PERIOD").disabled = false;

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.liSave
        ).hide();

    };
    thisApp.toggleSearchItems = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        //$(thisApp.config.cls.statusText).text(systemText.searchModeStatuxText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();

    };
    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("TIME_PERFORM").disabled = false;
        document.getElementById("SESSION_START").disabled = false;
        document.getElementById("SESSION_END").disabled = false;
        document.getElementById("NEXT_PM_DUE").disabled = false;
        document.getElementById("GRACE_PERIOD").disabled = false;

    };

    thisApp.toggleEditModeModal = function () {    


      
        $(         
            thisApp.config.tag.textArea).removeClass(thisApp.config.cssCls.viewMode);

        //$(thisApp.config.tag.textArea).prop(thisApp.config.attr.readOnly, false);
        $(thisApp.config.tag.textArea).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };
    thisApp.toggleDisplayModeAsset = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        //$(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        //$(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.searchAsset).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.liApiStatus).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };
    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' +  thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

        $(
           thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
           thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);
        document.getElementById("TIME_PERFORM").disabled = true;
        document.getElementById("SESSION_START").disabled = true;
        document.getElementById("SESSION_END").disabled = true;
        document.getElementById("NEXT_PM_DUE").disabled = true;
        document.getElementById("GRACE_PERIOD").disabled = true;
    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {
        $(form).find(":disabled").removeAttr("disabled");
        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.getAge = function (birthDate) {

        var seconds = Math.abs(new Date() - birthDate) / 1000;

        var numyears = Math.floor(seconds / 31536000);
        var numdays = Math.floor((seconds % 31536000) / 86400);
        var nummonths = numdays / 30;

        return numyears + " y " + Math.round(nummonths) + " m ";
    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.formatJSONDateToString = function () {

        var dateToFormat = arguments[0];
        var strDate;

        if (dateToFormat === null) return "-";
        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };
    thisApp.formatJSONTimeToString = function () {

        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        var timeToFormat = strTime[1].substring(0, 5);


        return timeToFormat;
    };
    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');


            if (isDateTime) {
                val = thisApp.formatJSONDateTimeToString(val);
            }

            if (isDate) {
                val = thisApp.formatJSONDateToString(val);
            }

            //if (isTime) {
            //    val = thisApp.formatJSONTimeToString(val);
            //    alert(val);
            //}

            var $el = $('#' + name), type = $el.attr('type'), isTime = $el.attr('data-istime');
            if (isTime) {
                if (val === null || val === "") {
                    val = "00:00";
                } else {
                    val = thisApp.formatJSONTimeToString(val);
                }

            }

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var ampmscheduleViewModel = function (systemText) {

    var self = this;

    var ah = new appHelper(systemText);
    self.processInfo = {
        filetype: ko.observable(''),
        statusBy: ko.observable(''),
        StatusDate: ko.observable(''),
        category: ko.observable(''),
        refid: ko.observable(''),
        filename: ko.observable(''),
        process: ko.observable('')
    };
    self.pmpartsList = ko.observableArray("");
    var srDataArray = ko.observableArray([]);
    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.toggleReadMode();

        ah.initializeLayout();
        postData = { SEARCH: "'AIMS_SERVICEDEPARTMENT'", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);


    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.assetInfo = function () {
        assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        var reqWoNO = ah.getParameterValueByName(ah.config.fld.reqWONo);
        window.location.href = ah.config.url.aimsInformation + "?ASSETNO=" + assetNoID + "&REQNO=" + reqWoNO;
    }
    self.addPMPart = function () {

        var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfoPartID));
        var pmScheduleId = $("#AM_ASSET_PMSCHEDULE_ID").val();

        var existPart = self.checkExist(infoID.ID_PARTS);
        if (existPart > 0) {
            alert('Unable to Proceed. The selected part was already exist in PM Materials.');
            return;
        }

        if (infoID.DESCRIPTION.length > 60)
            infoID.DESCRIPTION = infoID.DESCRIPTION.substring(0, 59) + "...";

        self.pmpartsList.push({
            DESCRIPTION: infoID.DESCRIPTION,
            ID_PARTS: infoID.ID_PARTS,
            QTY: 0,
            REFID: pmScheduleId,
            REF_TYPE: "PMSCHEDULE"
        });
        

        window.location.href = window.location.href + ah.config.url.modalLookUp;

    };
    self.checkExist = function (idPart) {

        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.pmpartsList);
        var partLists = JSON.parse(jsonObj);

        var partExist = partLists.filter(function (item) { return item.ID_PARTS === idPart });

        return partExist.length;
    };
    self.removeItem = function (pmpartsList) {
        // alert(ko.utils.stringifyJson(self.itemsReceive));
        self.pmpartsList.remove(pmpartsList);
    };
    self.createNew = function () {

        ah.toggleAddMode();

    };
    self.calculateDueDate = function () {
       
        var insDate = ah.getParameterValueByName(ah.config.fld.installDate);
        var dueDate = $("#NEXT_PM_DUE").val();
        var interval = $("#INTERVAL").val();
        var period = $("#FREQUENCY_PERIOD").val();
        var nodays = 0;
        insDate = ah.formatJSONDateToString(insDate);
        if (dueDate == "" && insDate != null && insDate != "" && insDate != "01/01/1900" && interval != "" && interval != null && period != "" && period != null) {

            if (interval == "D") {
                nodays = period;
            } else if (interval == "W") {
                nodays = period * 7;
            } else if (interval == "M") {
                nodays = period * 30;
            } else if (interval == "Y") {
                nodays = period * 365;
            }

           
           
            var dattmp = insDate.split('/').reverse().join('/');

            var nwdate = new Date(dattmp);
          
            nwdate.setDate(nwdate.getDate() + nodays);
            var nextDueDate = [nwdate.getDate(), nwdate.getMonth() + 1, nwdate.getFullYear()].join('/');
            $("#NEXT_PM_DUE").val(nextDueDate);
        }

    };
    self.modifySetup = function () {

        ah.toggleEditMode();

    };
    self.privilegesValidation = function (actionVal) {

        var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
        if (actionVal === 'NEW') {
            gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "16" });
            if (gPrivileges.length <= 0) {
                $(ah.config.id.newId).hide();
            }
        } else if (actionVal === 'MODIFY') {
            gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "3" });
            if (gPrivileges.length <= 0) {
                $(ah.config.id.modifyId + ',' + ah.config.id.addButtonId).hide();
            }
        }
    }
    self.pageLoader = function () {
        var senderID = $(arguments[1].currentTarget).attr('id');

        var searchStr = $(ah.config.id.txtGlobalSearchOth).val().toString();

        var pageNum = parseInt($(ah.config.id.pageNum).val());
        var pageCount = parseInt($(ah.config.id.pageCount).val());

        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.partList));
        var lastPage = Math.ceil(sr.length / 15);

        if (isNaN(pageCount)) {
            pageCount = 1;
        }
        if (isNaN(pageNum)) {
            pageNum = 0;
        }

        switch (senderID) {

            case ah.config.tagId.nextPage: {
                if (pageCount < lastPage) {
                    pageNum = pageNum + 15;
                    pageCount++;
                    self.searchItemsResult(searchStr, pageNum);
                }
                break;
            }
            case ah.config.tagId.priorPage: {

                if (pageNum === 0) {
                } else {
                    pageNum = pageNum - 15;
                    if (pageNum < 0) {
                        pageNum = 0;
                    }
                    pageCount = pageCount - 1;
                }

                self.searchItemsResult(searchStr, pageNum);
                break;
            }

            case ah.config.tagId.txtGlobalSearchOth: {
                pageCount = 1;
                pageNum = 0;

                self.searchItemsResult(searchStr, 0);
                break;
            }
        }

        $(ah.config.id.loadNumber).text(pageCount.toString());
        $(ah.config.id.pageCount).val(pageCount.toString());
        $(ah.config.id.pageNum).val(pageNum);

    };
    self.partsSearchNow = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        ah.toggleSearchItems();
        var pmScheduleId = $("#AM_ASSET_PMSCHEDULE_ID").val();
        postData = { SEARCH: "", REFID: pmScheduleId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.partssearchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = ah.config.url.modalAddItems;

    };

    self.searchItemsResult = function (LovCategory, pageLoad) {

        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.partList));

        if (LovCategory !== null && LovCategory !== "" && LovCategory !== "LOAD") {
            var arr = sr;
            var keywords = LovCategory.toUpperCase();
            var filtered = [];

            for (var i = 0; i < sr.length; i++) {
                for (var j = 0; j < keywords.length; j++) {
                    if (sr[i].DESCRIPTION === null) {
                        sr[i].DESCRIPTION = " ";
                    }
                    if ((sr[i].DESCRIPTION.toUpperCase()).indexOf(keywords) > -1 || (sr[i].ID_PARTS.toUpperCase()).indexOf(keywords) > -1) {
                        filtered.push(sr[i]);
                        break;
                    }
                }
            }
            sr = filtered;
        }

        $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).html('');

        if (sr.length > 0) {
            $("#noresult").hide();
            $("#pageSeparator").show();
            $("#page1").show();
            $("#page2").show();
            $("#nextPage").show();
            $("#priorPage").show();
            var totalrecords = sr.length;
            var pagenum = Math.ceil(totalrecords / 15);
            $("#totNumber").text(pagenum.toString());
            var pagenum = totalrecords / 15;
            if (pagenum < 2 && pagenum > 1) {
                pagenum = 2;
            }
            $(ah.config.id.totNumber).text(pagenum.toString());

            if (pagenum <= 1) {
                $("#noresult").hide();
                $("#pageSeparator").hide();
                $("#page1").hide();
                $("#page2").hide();
                $("#nextPage").hide();
                $("#priorPage").hide();

            }
            var htmlstr = '';
            var o = pageLoad;

            for (var o in sr) {
                var partDescription = sr[o].DESCRIPTION;
                //if (partDescription !== null || partDescription !== ""){
                //	if (partDescription.length > 70) {
                //		partDescription = partDescription.substring(0, 70) + '...';
                //	}
                //}
                // alert(partDescription);
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplateList, JSON.stringify(sr[o]), sr[o].ID_PARTS, partDescription, '');
                htmlstr += '</li>';
                o++
                if (totalrecords === o) {
                    break;
                }
            }

            $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).append(htmlstr);
        }
        else {
            $("#noresult").show();
            $("#pageSeparator").hide();
            $("#page1").hide();
            $("#page2").hide();
            $("#nextPage").hide();
            $("#priorPage").hide();
        }

        $(ah.config.cls.detailItem).bind('click', self.addPMPart);
        self.pmpartsList.splice(0, 5000);
        if (LovCategory === "LOAD") {
            var pmPartsr = JSON.parse(sessionStorage.getItem(ah.config.skey.pmParts));
            if (pmPartsr.length > 0) {

                for (var o in pmPartsr) {
                    if (pmPartsr[o].DESCRIPTION.length > 60)
                        pmPartsr[o].DESCRIPTION = pmPartsr[o].DESCRIPTION.substring(0, 59) + "..."

                    self.pmpartsList.push({
                        DESCRIPTION: pmPartsr[o].DESCRIPTION,
                        ID_PARTS: pmPartsr[o].ID_PARTS,
                        QTY: pmPartsr[o].QTY,
                        REFID: pmPartsr[o].REFID,
                        REF_TYPE: "PMSCHEDULE"
                    });
                }
            }
        }
        ah.toggleDisplayModeAsset();

    };

    self.addMaterials = function () {

        window.location.href = window.location.href + ah.config.url.modalLookUp;
    };
    
    self.cancelChangesModal = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            //var infoDetails = sessionStorage.getItem(ah.config.skey.infoDetails);

            //var jsetupDetails = JSON.parse(infoDetails);
            //var refID = jsetupDetails.AM_ASSET_PMSCHEDULE_ID;
            var refId = $("#AM_ASSET_PMSCHEDULE_ID").val();
            self.readSetupExistID(refId);

        }

        window.location.href = ah.config.url.modalClose;
        self.privilegesValidation('NEW');
        self.privilegesValidation('MODIFY');
    };
    self.applyProcess = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var setupDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        ah.toggleSaveMode();
        var myFile = document.getElementById('pdffile').value;
        var myFileData = document.getElementById('pdffile');


        var fName = null;
        //alert(myFile.length);
        if (myFile.length > 0) {
            var fName = myFileData.files[0].name;
        }


        var refId = $("#AM_ASSET_PMSCHEDULE_ID").val();
        self.processInfo.statusBy = staffLogonSessions.STAFF_ID;
        self.processInfo.filename = fName;
        self.processInfo.filetype = 'pdf';
        self.processInfo.category = 'ASSETPM';
        self.processInfo.refid = refId;
        var processDet = $("#PM_PROCESS").val();
        self.processInfo.process = processDet;
        postData = { MODIFYINFO: self.processInfo, SCANDOCS: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
        // alert(JSON.stringify(postData));
        $.post(self.getApi() + ah.config.api.createImagePM, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    }

    self.readSetupExistID = function (refId) {


        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { AM_ASSET_PMSCHEDULE_ID: refId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.addProcess = function () {
        ah.toggleEditModeModal();
     


        $(ah.config.id.searchResultPMDoc + ' ' + ah.config.tag.ul).html('');
        var sr = srDataArray;
        if (sr.length > 0) {

            var htmlstr = '';

            for (var o in sr) {
         
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderDOCTemplateList, sr[o].FILE_NAME, sr[o].FILE_NAME, '', '');
    
                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultPMDoc + ' ' + ah.config.tag.ul).append(htmlstr);
            $(ah.config.cls.detailItem).bind('click', self.readDoc);
        }

        window.location.href = window.location.href + ah.config.url.modalLookUp;
    };

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };
    self.readDoc = function () {
   
        var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
 

        var mywin = window.open("/UploadedFiles/" + infoID, "width=960,height=764");


    };
    self.saveAllChanges = function () {

        $(ah.config.id.saveInfo).trigger('click');

    };
    self.partsSaveAllChanges = function () {

        $(ah.config.id.partsaveInfoParts).trigger('click');

    };
    self.saveAllChangesProcess = function () {
        window.location.href = ah.config.url.modalClose;
        $(ah.config.id.saveInfo).trigger('click');

    };
    self.UploadImage = function (jsonData) {
        var scanInfo = jsonData.SCANDOCS;
       
        var fileNameInfo = scanInfo.FILE_NAME;
        var frmData = new FormData();
        var filebase = $("#pdffile").get(0);
        var files = filebase.files;
        frmData.append("docFilename", fileNameInfo);
        frmData.append(files[0].name, files[0]);
        $.ajax({
            url: '/Upload/SaveDocument',
            type: "POST",
            contentType: false,
            processData: false,
            data: frmData,
            success: function (data) {
                alert('Success Upload');
            },
            error: function (err) {
                alert(error);
            }
        });
    };
    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            self.initialize();
        } else {
            ah.toggleDisplayMode();
            //var refID = $("#AM_ASSET_PMSCHEDULE_ID").val();
            var infoDetails = sessionStorage.getItem(ah.config.skey.infoDetails);

            var jsetupDetails = JSON.parse(infoDetails);
            var refID = jsetupDetails.AM_ASSET_PMSCHEDULE_ID;
            self.readSetupExistID(refID);
        }
        self.privilegesValidation('NEW');
        self.privilegesValidation('MODIFY');
    };

    self.showDetails = function (isNotUpdate) {

        if (isNotUpdate) {

            var infoDetails = sessionStorage.getItem(ah.config.skey.infoDetails);

            var jsetupDetails = JSON.parse(infoDetails);

            ah.ResetControls();
            ah.LoadJSON(jsetupDetails);
            if (jsetupDetails.NEXT_PM_DUE == "" || jsetupDetails.NEXT_PM_DUE == "null" || jsetupDetails.NEXT_PM_DUE == "0001-01-01T00:00:00" || jsetupDetails.NEXT_PM_DUE == "1900-01-01T00:00:00") {

                $("#NEXT_PM_DUE").val("");
            }

            if (jsetupDetails.SESSION_START == "" || jsetupDetails.SESSION_START == "null" || jsetupDetails.SESSION_START == "0001-01-01T00:00:00" || jsetupDetails.SESSION_START == "1900-01-01T00:00:00") {

                $("#SESSION_START").val("");
            }
            if (jsetupDetails.SESSION_START == "" || jsetupDetails.SESSION_END == "null" || jsetupDetails.SESSION_END == "0001-01-01T00:00:00" || jsetupDetails.SESSION_END == "1900-01-01T00:00:00") {

                $("#SESSION_END").val("");
            }
        }

        ah.toggleDisplayMode();
        $(ah.config.cls.liAddNew).hide();
        self.privilegesValidation('NEW');
        self.privilegesValidation('MODIFY');
    };

    self.searchResult = function (jsonData) {
        ah.displaySearchResult();
        $(ah.config.cls.liAddNew).hide();
        var sr = jsonData.AM_ASSET_PMSCHEDULES_LIST;
        // alert(JSON.stringify(sr));
        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {
                var dueDate = new Date(sr[o].NEXT_PM_DUE);
                var sessionStart = new Date(sr[o].SESSION_START);

                var sessionEnd = new Date(sr[o].SESSION_END);
                if (sr[o].STATUS == "A") {
                    sr[o].STATUS = "Active";

                } else if (sr[o].STATUS == "I") {
                    sr[o].STATUS = "Inactive";
                }

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].AM_ASSET_PMSCHEDULE_ID, '', sr[o].PM_DESCRIPTION, '');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.servdept, sr[o].SRVDEPT_DESC);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.frequency, sr[o].FREQUENCY_PERIOD + ' ' + sr[o].INTERVAL);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.nextDue, dueDate.toDateString().substring(4, 15));
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.grace, sr[o].GRACE_PERIOD + ' ' + sr[o].GRACE_PERIOD_TYPE);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.status, sr[o].STATUS);
                

                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        } else {
            $(ah.config.cls.liAddNew).show();
        }

        $(ah.config.cls.detailItem).bind('click', self.readSetupID);
        
        self.privilegesValidation('NEW');
        self.privilegesValidation('MODIFY');
    };

    self.readSetupID = function () {

        var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { AM_ASSET_PMSCHEDULE_ID: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();
        assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), ASSETNO: assetNoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };

    self.saveInfo = function () {
        document.getElementById("SESSION_START").disabled = false;
        document.getElementById("SESSION_END").disabled = false;
        document.getElementById("NEXT_PM_DUE").disabled = false;
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var setupDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        setupDetails.CREATED_BY = staffLogonSessions.STAFF_ID;


        var EstimateTime = setupDetails.TIME_PERFORM;
        if (EstimateTime === 0 || EstimateTime > 999.99 || EstimateTime < 0 || EstimateTime === 0.00) {
            alert('Estimate Time to Perform accepts 0.01 to 999.99');
            return;
        }
        if (EstimateTime === null || EstimateTime === "") {
            setupDetails.TIME_PERFORM = 0.00
        }

        if (setupDetails.NEXT_PM_DUE == "" || setupDetails.NEXT_PM_DUE == null || setupDetails.NEXT_PM_DUE == "00/00/0000" || setupDetails.NEXT_PM_DUE == "00-00-0000") {
            setupDetails.NEXT_PM_DUE = "01/01/1900";
        }
        if (setupDetails.SESSION_START == "" || setupDetails.SESSION_START == null || setupDetails.SESSION_START == "00/00/0000" || setupDetails.SESSION_START == "00-00-0000") {
            setupDetails.SESSION_START = "01/01/1900";
        }
        if (setupDetails.SESSION_END == "" || setupDetails.SESSION_END == null || setupDetails.SESSION_END == "00/00/0000" || setupDetails.SESSION_END == "00-00-0000") {
            setupDetails.SESSION_END = "01/01/1900";
        }

        if (ah.CurrentMode == ah.config.mode.add) {

            var jDate = new Date();
            var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);

            setupDetails.CREATED_DATE = strDateStart + ' ' + hourmin;

            setupDetails.ASSET_NO = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
            postData = { AM_ASSET_PMSCHEDULES: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            ah.toggleSaveMode();

            // alert(JSON.stringify(postData));
            //return;
            $.post(self.getApi() + ah.config.api.createSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {

            var jDate = new Date();
            var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);

            setupDetails.LAST_MODIFIED_BY = staffLogonSessions.STAFF_ID;
                setupDetails.LAST_MODIFIED_DATE = strDateStart + ' ' + hourmin;

            ah.toggleSaveMode();
            postData = { AM_ASSET_PMSCHEDULES: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            // alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.updateSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }

    };
    self.savePartsInfo = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var itemDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfoParts)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.pmpartsList);


        var pmParts = JSON.parse(jsonObj);

        if (pmParts.length <= 0) {
            return;
        }
        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        strDateStart = strDateStart + ' ' + hourmin;

        postData = { CREATE_DATE: strDateStart, AM_WOPM_PARTS: pmParts, STAFF_LOGON_SESSIONS: staffLogonSessions };
        //    alert(JSON.stringify(pmParts));
        ah.toggleSaveMode();
        $.post(self.getApi() + ah.config.api.createMaterials, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.webApiCallbackStatus = function (jsonData) {
        //alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            // window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.SCANDOCS_CATEGORYIMAGELIST || jsonData.SCANDOCS || jsonData.AM_ASSET_PMSCHEDULES_LIST || jsonData.AM_PROCEDURES || jsonData.LOV_LOOKUPS || jsonData.AM_ASSET_PMSCHEDULES ||jsonData.AM_WOPM_PARTS|| jsonData.AM_PARTS || jsonData.STAFF || jsonData.SUCCESS) {

            if (ah.CurrentMode == ah.config.mode.save) {
                sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_ASSET_PMSCHEDULES));
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();

                if (jsonData.SUCCESS) {
                    self.showDetails(false);
                } else if (jsonData.AM_WOPM_PARTS) {
                    ah.toggleDisplayMode();
                    window.location.href = ah.config.url.modalClose;
                } else if (jsonData.SCANDOCS) {
                    ah.toggleDisplayMode();
                    self.UploadImage(jsonData);
                    srDataArray = jsonData.SCANDOCS_CATEGORYIMAGELIST;
                }else {
                    self.showDetails(true);
                }
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_ASSET_PMSCHEDULES));
                self.showDetails(true);
                srDataArray = jsonData.SCANDOCS_CATEGORYIMAGELIST;
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
                if (jsonData.AM_PARTS) {
                    sessionStorage.setItem(ah.config.skey.partList, JSON.stringify(jsonData.AM_PARTS));
                    sessionStorage.setItem(ah.config.skey.pmParts, JSON.stringify(jsonData.AM_WOPM_PARTS));
                    $(ah.config.id.loadNumber).text("1");
                    $(ah.config.id.pageCount).val("1");
                    $(ah.config.id.pageNum).val(0);
                    self.searchItemsResult("LOAD", 0);
                } else {
                    self.searchResult(jsonData);
                }
                
            }
			else if (ah.CurrentMode == ah.config.mode.idle) {
				
				var serviceDeptLOV = jsonData.LOV_LOOKUPS;
				
				
				$.each(serviceDeptLOV, function (item) {

					$(ah.config.id.serviceDept)
						.append($("<option></option>")
							.attr("value", serviceDeptLOV[item].LOV_LOOKUP_ID)
							.text(serviceDeptLOV[item].DESCRIPTION));
				});

				var procedureLOV = jsonData.AM_PROCEDURES;

				$.each(procedureLOV, function (item) {

					$(ah.config.id.pmProcedureId)
						.append($("<option></option>")
							.attr("value", procedureLOV[item].PROCEDURE_ID)
							.text(procedureLOV[item].DESCRIPTION));
				});

				assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
				assetDescription = ah.getParameterValueByName(ah.config.fld.assetDescInfo);

				if (assetDescription.length > 70) {
					assetDescription = assetDescription.substring(0, 70) + '...';
				}
				if (assetNoID !== null && assetNoID !== "") {
					$("#infoID").text(assetNoID + "-" + assetDescription);
					self.searchNow();
				}
            }

        }
        else {

            alert(systemText.errorTwo);
           // window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};