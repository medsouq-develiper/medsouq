﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            detailItem: '.detailItem',
            searhButton: '.searhButton'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul'
        },
        tagId: {
            txtGlobalSearchOth: 'txtGlobalSearchOth',
            priorPage: 'priorPage',
            nextPage: 'nextPage',
            searchSupplierId: 'searchSupplierId',
            lookUptxtGlobalSearchOth: 'lookUptxtGlobalSearchOth'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7,
            searchItems:8
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmInfo: '#frmInfo',
            clinicdoctorID: '#CLINIC_DOCTOR_ID',
            clinicID: '#CLINIC_ID',
            staffID: '#STAFF_ID',
            serviceID: '#SERVICE_ID',
            successMessage: '#successMessage',
            searchResult1: '#searchResult1',
            txtGlobalSearchOth: '#txtGlobalSearchOth',
            pageNum: '#pageNum',
            pageCount: '#pageCount',
            loadNumber: '#loadNumber',
            supplierDESC: '#SUPPLIERDESC',
            noResult: '#noresult',
            totNumber: '#totNumber',
            searchResultLookUp: '#searchResultLookUp',
            lookUptxtGlobalSearchOth: '#lookUptxtGlobalSearchOth'
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'searchResult',
            infoDetails: 'AM_PARTSUPPLIERDETAILS',
            supplierList: 'AM_SUPPLIER'
        },
        url: {
            aimsHomepage: '/Menuapps/aimsIndex',
            homeIndex: '/Home/Index',
            modalClose: '#',
            aimsPartsSetup: '/aims/amPartsSetup',
            modalAddItems: '#openModal',
            openModalLookup: '#openModalLookup'
        },
        fld: {
            fldpartId: 'PARTID'
        },

        api: {

            readSetup: '/AIMS/ReadPartSP',
            searchAll: '/AIMS/SearchPartSP',
            createSetup: '/AIMS/CreateNewPartSP',
            updateSetup: '/AIMS/UpdatePartSP',
            searchSupplier: '/AIMS/SearchSuppierLOV'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);
        
        $(thisApp.config.id.supplierDESC).hide();
    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess + ',' + thisApp.config.cls.searhButton
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;
        document.getElementById("RETURN_NODAYS").disabled = true;
        document.getElementById("CREDIT").disabled = true;
        document.getElementById("UNIT_QTY").disabled = true;
        document.getElementById("QTY_PERUNIT").disabled = true;
        document.getElementById("MIN_ORDERQTY").disabled = true;
        document.getElementById("DELIVERY_LOADTIME").disabled = true;
        document.getElementById("PREFERENCE").disabled = true;
        document.getElementById("SUPPLIER_DESC").disabled = true;
        
        
    };
    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.searhButton).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ','+thisApp.config.id.supplierDESC
        ).hide();

        
        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("SUPPLIER_DESC").disabled = true;
        document.getElementById("RETURN_NODAYS").disabled = false;
        document.getElementById("CREDIT").disabled = false;
        document.getElementById("UNIT_QTY").disabled = false;
        document.getElementById("QTY_PERUNIT").disabled = false;
        document.getElementById("MIN_ORDERQTY").disabled = false;
        document.getElementById("DELIVERY_LOADTIME").disabled = false;
        document.getElementById("PREFERENCE").disabled = false;

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.searhButton).hide();

        document.getElementById("RETURN_NODAYS").disabled = true;
        document.getElementById("CREDIT").disabled = true;
        document.getElementById("UNIT_QTY").disabled = true;
        document.getElementById("QTY_PERUNIT").disabled = true;
        document.getElementById("MIN_ORDERQTY").disabled = true;
        document.getElementById("DELIVERY_LOADTIME").disabled = true;
        document.getElementById("PREFERENCE").disabled = true;
        document.getElementById("SUPPLIER_DESC").disabled = true;

    };
    thisApp.toggleSearchItems = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        //$(thisApp.config.cls.statusText).text(systemText.searchModeStatuxText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();

    };
    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField ).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.id.supplierDESC + ',' + thisApp.config.cls.searhButton).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("SUPPLIER_DESC").disabled = true;
        document.getElementById("RETURN_NODAYS").disabled = false;
        document.getElementById("CREDIT").disabled = false;
        document.getElementById("UNIT_QTY").disabled = false;
        document.getElementById("QTY_PERUNIT").disabled = false;
        document.getElementById("MIN_ORDERQTY").disabled = false;
        document.getElementById("DELIVERY_LOADTIME").disabled = false;
        document.getElementById("PREFERENCE").disabled = false;

    };
    thisApp.toggleDisplayModeSupplier = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchItems;

        //$(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        //$(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.searchAsset).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.liApiStatus).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };
    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.searhButton + ',' + thisApp.config.id.supplierDESC
        ).hide();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.searhButton + ',' + thisApp.config.id.supplierDESC
        ).hide();

        $(
           thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
           thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);

        document.getElementById("RETURN_NODAYS").disabled = true;
        document.getElementById("CREDIT").disabled = true;
        document.getElementById("UNIT_QTY").disabled = true;
        document.getElementById("QTY_PERUNIT").disabled = true;
        document.getElementById("MIN_ORDERQTY").disabled = true;
        document.getElementById("DELIVERY_LOADTIME").disabled = true;
        document.getElementById("PREFERENCE").disabled = true;
        document.getElementById("SUPPLIER_DESC").disabled = true;

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.getAge = function (birthDate) {

        var seconds = Math.abs(new Date() - birthDate) / 1000;

        var numyears = Math.floor(seconds / 31536000);
        var numdays = Math.floor((seconds % 31536000) / 86400);
        var nummonths = numdays / 30;

        return numyears + " y " + Math.round(nummonths) + " m ";
    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name),
                type = $el.attr('type');

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var amPartSupplierDetailsViewModel = function (systemText) {

    var self = this;
    editable: ko.observable(false);

    var supplierListLOV = ko.observableArray([]);
  
    var ah = new appHelper(systemText);
    self.closeModal = function () {
        document.getElementById("SUPPLIER_DESC").disabled = true;
        window.location.href = ah.config.url.modalClose;
    }
    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());
        
        ah.toggleReadMode();

        ah.initializeLayout();
        self.searchNow();
    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.returnHome = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.returnPartSetup = function () {
        var partId = ah.getParameterValueByName(ah.config.fld.fldpartId);        
        window.location.href = encodeURI(ah.config.url.aimsPartsSetup + "?PARTID=" + partId);
    };
    self.createNew = function () {

        ah.toggleAddMode();

    };
    self.supplierSearchNow = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));


        //ah.toggleSearchMode();
        ah.toggleSearchItems();
        postData = { SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchSupplier, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = ah.config.url.modalAddItems;
    };
    self.modifySetup = function () {

        ah.toggleEditMode();

    };
    self.pageLoader = function () {
        var senderID = $(arguments[1].currentTarget).attr('id');

        var searchStr = $(ah.config.id.txtGlobalSearchOth).val().toString();

        var pageNum = parseInt($(ah.config.id.pageNum).val());
        var pageCount = parseInt($(ah.config.id.pageCount).val());
        
        if (isNaN(pageCount)) {
            pageCount = 1;
        }
        if (isNaN(pageNum)) {
            pageNum = 0;
        }

        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.supplierList));
        var lastPage = Math.ceil(sr.length / 10);

        switch (senderID) {

            case ah.config.tagId.nextPage: {
                if (pageCount < lastPage) {
                    pageNum = pageNum + 10;
                    pageCount++;
                    self.searchItemsResult(searchStr, pageNum);
                }
                break;
            }
            case ah.config.tagId.priorPage: {

                if (pageNum === 0) {
                } else {
                    pageNum = pageNum - 10;
                    if (pageNum < 0) {
                        pageNum = 0;
                    }
                    pageCount = pageCount - 1;
                }

                self.searchItemsResult(searchStr, pageNum);
                break;
            }

            case ah.config.tagId.txtGlobalSearchOth: {
                pageCount = 1;
                pageNum = 0;

                self.searchItemsResult(searchStr, 0);
                break;
            }
        }

        $(ah.config.id.loadNumber).text(pageCount.toString());
        $(ah.config.id.pageCount).val(pageCount.toString());
        $(ah.config.id.pageNum).val(pageNum);

    };
    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };
    self.infoLovLoader = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        var senderID = $(arguments[1].currentTarget).attr('id');
        $("#searchingLOVList").hide();
        $("#lookUpnoresult").hide();
      
        $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');
        var searchStr = $(ah.config.id.lookUptxtGlobalSearchOth).val();
        switch (senderID) {
            case ah.config.tagId.searchSupplierId: {
                $(ah.config.id.lookUptxtGlobalSearchOth).val("");
               
                $("#lookUpTitle").text("Please search/select from active supplier list");
                break;
            }
            
            case ah.config.tagId.lookUptxtGlobalSearchOth: {
               
                    if (supplierListLOV.length > 0) {
                        self.searchItemsResult(searchStr,0);
                    } else {
                        self.supplierSearch();
                    }

                break;
            }


        }
        window.location.href = ah.config.url.openModalLookup;
    };
    self.onFocusDesc = function () {
      
        document.getElementById("SUPPLIER_DESC").disabled = true;
    };
    self.saveAllChanges = function () {
        document.getElementById("SUPPLIER_DESC").disabled = false;
        $(ah.config.id.saveInfo).trigger('click');

    };

    self.cancelChanges = function () {
        
        if (ah.CurrentMode == ah.config.mode.add || ah.CurrentMode == ah.config.mode.searchItems) {
            ah.initializeLayout();
            self.searchNow();
        } else {
            ah.toggleDisplayMode();
            var refId = $("#PARTSP_ID").val();
          
            self.readSetupIDRef(refId);
        }

    };

    self.showDetails = function (isNotUpdate) {
        var supplierdesc = $("#SUPPLIERDESC").text();

        if (isNotUpdate) {

            var infoDetails = sessionStorage.getItem(ah.config.skey.infoDetails);

            var jsetupDetails = JSON.parse(infoDetails);
            
            ah.ResetControls();
            ah.LoadJSON(jsetupDetails);
        }
        //var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.));
        ah.toggleDisplayMode();
        if (supplierdesc !== null && supplierdesc !== "") {
            $("#SUPPLIER_DESC").val(supplierdesc);
        }
        
        

    };

    self.searchResult = function (jsonData) {


        var sr = jsonData.AM_PARTSUPPLIERDETAILS;

        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {

                if (sr[o].DESCRIPTION.length > 100)
                    sr[o].DESCRIPTION = sr[o].DESCRIPTION.substring(0, 99) + "...";

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].PARTSP_ID, sr[o].DESCRIPTION, '');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.category, sr[o].PREFERENCE);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.ancillary, sr[o].EXCHANGEABLE);

                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.detailItem).bind('click', self.readSetupID);
        ah.displaySearchResult();

    };
    self.populateSupplierInfo = function () {

        var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfoId).toString());
        sessionStorage.setItem(ah.config.skey.assetItems, JSON.stringify(infoID));
        //alert(JSON.stringify(infoID));
        $("#SUPPLIER_ID").val(infoID.SUPPLIER_ID);
        $("#SUPPLIER_DESC").val(infoID.DESCRIPTION);
        $("#SUPPLIER_DESC").prop("disabled",true);
        
    };
    self.searchItemsResult = function (LovCategory, pageLoad) {

        var sr = supplierListLOV;
       
        if (LovCategory !== null && LovCategory !== "") {
            var arr = sr;
            var keywords = LovCategory.toUpperCase();
            var filtered = [];

            for (var i = 0; i < sr.length; i++) {
                for (var j = 0; j < keywords.length; j++) {
                    if ((sr[i].DESCRIPTION.toUpperCase()).indexOf(keywords) > -1 || (sr[i].SUPPLIER_ID.toUpperCase()).indexOf(keywords) > -1) {
                        filtered.push(sr[i]);
                        break;
                    }
                }
            }
            sr = filtered;
        }
            $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');
       
            if (sr.length > 0) {
                $(ah.config.id.noResult).hide();
                $("#page1").show();
                $("#page2").show();
                $("#pageSeparator").show();
                $("#nextPage").show();
                $("#priorPage").show();
                var totalrecords = sr.length;
                var pagenum = Math.ceil(totalrecords / 10);
                if (pagenum < 2 && pagenum > 1) {
                    pagenum = 2;
                }
                $(ah.config.id.totNumber).text(pagenum.toString());

                if (pagenum <= 1) {
                    $("#page1").hide();
                    $("#page2").hide();
                    $("#pageSeparator").hide();
                    $("#nextPage").hide();
                    $("#priorPage").hide();

                }
                var htmlstr = '';
              
                for (var o in sr) {
                    var assetDescription = sr[o].DESCRIPTION;
                    if (assetDescription.length > 70) {
                        assetDescription = assetDescription.substring(0, 70) + '...';
                    }
                    htmlstr += '<li>';
                    htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].SUPPLIER_ID, assetDescription, '');

                    htmlstr += '</li>';
                  
                }

                $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).append(htmlstr);
            }
            else {
                $(ah.config.id.noResult).show();
            }

         $(ah.config.cls.detailItem).bind('click', self.populateSupplierInfo);
       // ah.toggleDisplayModeSupplier();
        //ah.toggleAddItems();
    };
    self.readSetupIDRef = function (refId) {


        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { PARTSP_ID: refId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.readSetupID = function () {

        
        var infoIDetails = JSON.parse(this.getAttribute(ah.config.attr.datainfoId));

        var infoID = infoIDetails.PARTSP_ID;// this.getAttribute(ah.config.attr.datainfoId).toString();
        var supplierDesc = infoIDetails.DESCRIPTION;
        //alert(supplierDesc);
        $("#SUPPLIERDESC").text(supplierDesc);
        //alert(JSON.stringify(infoIDetails));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();
        
        postData = { PARTSP_ID: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.supplierSearch = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;
      
        postData = { SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchSupplier, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();
        var partId = ah.getParameterValueByName(ah.config.fld.fldpartId);
        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), ID_PARTS: partId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };

    self.saveInfo = function () {
        
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var setupDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        setupDetails.CREATED_BY = staffLogonSessions.STAFF_ID;
        
        $("#SUPPLIERDESC").text(setupDetails.SUPPLIER_DESC);

        if (ah.CurrentMode == ah.config.mode.add || ah.CurrentMode == ah.config.mode.searchItems) {
            var partId = ah.getParameterValueByName(ah.config.fld.fldpartId);
            setupDetails.ID_PARTS = partId;
            setupDetails.PARTSP_ID = 0;
            var jDate = new Date();
            var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);
            
            setupDetails.CREATED_DATE = strDateStart + ' ' + hourmin;
            postData = { AM_PARTSUPPLIERDETAILS: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            ah.toggleSaveMode();
            //alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.createSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {

            ah.toggleSaveMode();
            postData = { AM_PARTSUPPLIERDETAILS: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            //alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.updateSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }

    };

    self.webApiCallbackStatus = function (jsonData) {
        //alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
           // window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.AM_SUPPLIERLIST || jsonData.AM_PARTSUPPLIERDETAILS || jsonData.AM_SUPPLIER || jsonData.STAFF || jsonData.SUCCESS) {

            if (ah.CurrentMode == ah.config.mode.save) {
                sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_PARTSUPPLIERDETAILS));
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();

                if (jsonData.SUCCESS) {
                    self.showDetails(false);
                }
                else {
                    self.showDetails(true);
                }
            }
            else if (ah.CurrentMode == ah.config.mode.add || ah.CurrentMode == ah.config.mode.edit) {
                if (jsonData.AM_SUPPLIERLIST) {
                    var searchStr = $(ah.config.id.lookUptxtGlobalSearchOth).val();
                    supplierListLOV = jsonData.AM_SUPPLIERLIST;
                    $(ah.config.id.loadNumber).text("1");
                    $(ah.config.id.pageCount).val("1");
                    $(ah.config.id.pageNum).val(0);
                    self.searchItemsResult(searchStr, 0);

                }
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_PARTSUPPLIERDETAILS));
                self.showDetails(true);
            }


            else if (ah.CurrentMode == ah.config.mode.search) {
                if (jsonData.AM_SUPPLIERLIST) {
                    supplierListLOV = jsonData.AM_SUPPLIERLIST;
                    $(ah.config.id.loadNumber).text("1");
                    $(ah.config.id.pageCount).val("1");
                    $(ah.config.id.pageNum).val(0);
                    self.searchItemsResult("", 0);

                } else {
                    self.searchResult(jsonData);
                }
                
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {

            }

        }
        else {

            alert(systemText.errorTwo);
           // window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};