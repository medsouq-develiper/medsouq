﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divPatientAssessmentID: '.divPatientAssessmentID',
            statusText: '.status-text',
            imageDetailItem: '.imageDetailItem',
            consultbookDetailItem: '.consultBookDetailItem',
            imagestemplateList: '.imagestemplate-list',
            alertsList: '.alerts-list',
            fieldList: '.field-list',
            itemnameText: '.itemnameText',
            lovlistText: '.LovListText',
            patDob: '.PATDOB',
            clspatientID: '.PATIENT_ID',
            fieldVisitlist: '.field-visitlist',
            imageItem: '.imageItem',
            demo: '.demo',
            newSketch: '.newSketch',
            displayImages: '.displayImages'

        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul',
            table: 'table'
        },
        mode: {
            idle: 0,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            productID: '#PRODUCT_ID',
            categoryID: '#CATEGORY_ID',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            txtDateFrom: '#txtDateFrom',
            colors_sketch: '#colors_sketch',
            receiptDetail: '#receiptDetail',
            eventID: '#EVENT_ID',
            successMessage: '#successMessage',
            btnSearchNow: '#btnSearchNow',
            searchItems1: '#searchItems1',
            searchItems2: '#searchItems2',
            rptDetail: '#rptDetail',
            itemlistId: '#itemlistId',
            itemName: '#itemName',
            searchOrders: '#searchOrders',
            orderList: '#orderList',
            patientID: '#PATIENT_ID',
            orderform: '#orderform',
            btnPrint: '#btnPrint',
            patientDob: '#BIRTH_DATE',
            summaryVisitList: '#summaryVisitList'


        },
        tagId: {
            imageTemplate1: 'imageTemplate1',
            imageTemplate2: 'imageTemplate2',
            imageTemplate3: 'imageTemplate3',
            imageTemplate4: 'imageTemplate4',
            imageTemplate5: 'imageTemplate5',
            oeInstruction: 'INSTRUCTION'

        },
        cssCls: {
            viewMode: 'view-mode',
            searchRowDivider: 'search-row-divider',
            alignLeft: 'align-left',
            alignRight: 'align-right'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            dataPatientAssessmentID: 'data-patientassessmentID',
            dataimageTemplate: 'data-imageTemplate',
            datalistitemInfo: 'data-listitemInfo',
            dataitemInfo: 'data-itemInfo',
            senderID: 'id'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            patientTriageInfo: 'PATIENT_TRIAGE_INFO',
            categoryImages: 'SCANDOCS_CATEGORY',
            searchResult: 'searchResult',
            imagesTemplate: 'CONSULTATION_IMGSKETCHTEMP',
            imagesTemplatetoUpload: 'IMGSKETCHTEMP_TOUPLOAD',
            patientconsultationDetails: 'PATIENT_CLINIC_CONSULTATIONBOOKINGS',
            clinicDoctors: 'CLINIC_DOCTORS',
            patientBookingInfo: 'PATIENT_BOOKING_INFO'

        },
        fld: {
            fldzdexdsesxxlz: 'zdexdsesxxlz',
            fldPatientID: 'patientID',
            fldEventID: 'eventID',
            fldsearchPatient: 'searchPatient'
        },
        url: {
            homeIndex: '/Home/Index',
            orderEntry: '/orders/OrderEntry?'
        },
        api: {
            getLookupList: '/Outpatient/GetNurseTriageLookUpTables',
            readPatientAssessment: '/Search/SearchPatientClinicEventsInfo',
            searchAll: '/Search/SearchClinicBookingsByDate',
            searchInfoImages: '/Patient/SearchPatientImageId',
            searchInfoCagetoryImages: '/Patient/SearchScandocsByPatientCategoryID',
            searchInfo: '/Patient/SearchPatientProfile',
            searchImagesSketch: '/Scandocs/SearchConsultImagesSketchTemp',
            createpatientImages: '/Patient/CreatePatientImages'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal patientAssessmentDetailItem' data-patientClinicBooking='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            searchBookRowHeaderTemplate: "<a href='#' class='search-row-data consultBookDetailItem fc-green' data-itemInfo='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            searchOrdersRowHeaderTemplate: "<a href='#' class='search-row-data consultBookDetailItem fc-green' data-itemInfo='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            searchRowTemplate: '<span class="search-row-label fc-light-grey-2">{0}: <span class="search-row-data">{1}</span></span>',
            searchRowTemplateStrong: '<span class="search-row-label fc-green">{0}: <span class="search-row-data">{1}</span></span>',
            vitalsRowHeaderTemplate: '<p><i class="fa fa-stethoscope"></i>&nbsp;&nbsp;<span >{0}</span></p>',
            vitalsRowTemplate1: '<span class="summary-row-label">{0}: <span class="summary-row-data">{1}</span></span>',
            vitalsRowTemplate2: '<span class="summary-row-label">{0}: <span class="summary-row-data">{1}</span><span class="summary-row-data"></span><span class="summary-row-data">{2}</span></span>',
            alertsRowHeaderTemplate: '<p><i class="fa fa-info-circle"></i>&nbsp;&nbsp;<span>{0}</span></p>',
            searchRowTableTemplate: '<span class="search-row-data">{0}</span>',
            searchListRowHeaderTemplate: "<a href='#' class='search-row-data imageItem fc-slate-blue' data-listitemInfo='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
        }
    };

    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    thisApp.CurrentMode = thisApp.config.mode.idle;

   

    thisApp.initializeLayout = function () {

     

        $(thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        //$(thisApp.config.cls.statusText).text(systemText.searchModeStatuxText);
        $(thisApp.config.id.itemlistId + ',' + thisApp.config.id.searchItems1).show();

        //$(thisApp.config.cls.liApiStatus).show();

        //$(
        //    thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liModify + ',' +
        //    thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        //).hide();

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.fieldList).show();
        $(thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };



    thisApp.toggleConsultRedMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;


        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.fieldList + ',' + thisApp.config.id.rptDetail + ',' + thisApp.config.cls.fieldVisitlist).show();
        $(thisApp.config.id.searchOrders).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };
    

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).show();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        //$(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liSaveAll).show();
        $(thisApp.config.cls.displayImages).show();
        $(thisApp.config.cls.newSketch).hide();
        //$(
        //    thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.fieldList + ',' +
        //    thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
        //    thisApp.config.cls.contentField + ',' + thisApp.config.id.receiptDetail
        //).hide();
    };
    thisApp.toggleNewMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        //$(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        //$(thisApp.config.cls.liApiStatus).show();
        $(thisApp.config.cls.displayImages).hide();
        $(thisApp.config.cls.newSketch).show();
        //$(
        //    thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.fieldList + ',' +
        //    thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
        //    thisApp.config.cls.contentField + ',' + thisApp.config.id.receiptDetail
        //).hide();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divPatientAssessmentID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.fieldList
        ).hide();

        $(
           thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
           thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.formatJSONDateTimeToString = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0] + ' ' + strTime[1].substring(0, 5);


        return dateToFormat;
    };

    thisApp.formatJSONDateToString = function () {

        var dateToFormat = arguments[0];
        var strDate;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 10) return "-";
        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');

            if (isDateTime) {
                val = thisApp.formatJSONDateTimeToString(val);
            }

            if (isDate) {
                val = thisApp.formatJSONDateToString(val);
            }

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var imageSketchViewModel = function (systemText) {
  
    var self = this;

    var ah = new appHelper(systemText);

    self.initialize = function () {
       
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));


        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());
        window.sessionStorage.removeItem("IMGSKETCHTEMP_TOUPLOAD");
        
        //ah.toggleReadMode();
        ah.initializeLayout();

        self.readPatientEventID();
        //self.showPatientInfo();
        
    };
    $('#imageInfo').change(function (e) {
        var files = e.target.files;
        self.loadImages(files);

    });

    self.loadImages = function (files) {

        var formDetails = ah.ConvertFormToJSON($(ah.config.id.frmCategory)[0]);
        if (formDetails.CATEGORY === null || formDetails.CATEGORY === "") {
            alert('Unable to proceed. Please select Image Category to continue.');
            $(ah.config.id.frmCategory)[0].reset();
            return;
        }

        'use strict';

       
        x = 0;
        var resize = new window.resize();
        resize.init();

        //event.preventDefault();

       
        //var files = event.target.files;
        var array = 0;
        var arrayFileName = new Array;

        for (var i in files) {

            if (typeof files[i] !== 'object') return false;

            var file_name = files[i].name;
            arrayFileName.push(file_name);
            var base64 = self.resizeImage(files[i], arrayFileName);
           
        }


    };

    self.resizeImage = function (file, arrayFileName) {


        var resize = new window.resize();
        resize.init();


        var reader = new FileReader();
        reader.onload = function (readerEvent) {

            self.resizeCanvas(readerEvent.target.result, 1200, 'file', arrayFileName, file);

        }
        reader.readAsDataURL(file);


    };
    self.resizeCanvas = function (dataURL, maxSize, outputType, arrayFileName, file) {

        var _this = this;
        var x = 0;
        var image = new Image();
        image.onload = function (imageEvent) {

            // Resize image
            var canvas = document.createElement('canvas'),
                width = image.width,
                height = image.height;
            if (width > height) {
                if (width > maxSize) {
                    height *= maxSize / width;
                    width = maxSize;
                }
            } else {
                if (height > maxSize) {
                    width *= maxSize / height;
                    height = maxSize;
                }
            }
            canvas.width = width;
            canvas.height = height;
            canvas.getContext('2d').drawImage(image, 0, 0, width, height);

            var newdataURL = canvas.toDataURL('image/jpeg', 0.8)
            self.resizeOutcome(newdataURL, 600, 'dataURL', arrayFileName, file);

            // _this.output(canvas, outputType, callback);

        }

        image.src = dataURL;
    };

    self.resizeOutcome = function (dataURL, maxSize, outputType, arrayFileName, file) {

        var _this = this;

        var image = new Image();
        image.onload = function (imageEvent) {

            // Resize image
            var canvas = document.createElement('canvas'),
                width = image.width,
                height = image.height;
            if (width > height) {
                if (width > maxSize) {
                    height *= maxSize / width;
                    width = maxSize;
                }
            } else {
                if (height > maxSize) {
                    width *= maxSize / height;
                    height = maxSize;
                }
            }
            canvas.width = width;
            canvas.height = height;
            canvas.getContext('2d').drawImage(image, 0, 0, width, height);
            var newdataURL = canvas.toDataURL('image/jpeg', 0.8)

            self.eloaddata(newdataURL, arrayFileName, file);
            // _this.output(canvas, outputType, callback);

        }

        image.src = dataURL;
    };

    self.eloaddata = function (data, filenameArray, file) {
        var file_name = file.name;
        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());

            return hours + ":" + minutes;
        }

        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);
        var strDate = hourmin + ' ' + strDateStart;

        
        var appendData = JSON.parse(sessionStorage.getItem(ah.config.skey.imagesTemplate));

        var appendDatatoUpload = JSON.parse(sessionStorage.getItem(ah.config.skey.imagesTemplatetoUpload));

        var bookingInfo = JSON.parse(sessionStorage.getItem(ah.config.skey.patientBookingInfo));

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var htmlstr = "";        
        var appendDatafilter = appendDatatoUpload;
        if (appendDatafilter != null) {            
            if (appendDatafilter.length > 0) {
                alert(file_name);
                appendDatafilter = appendDatafilter.filter(function (item) { return item.FILE_NAME === file_name });
                if (appendDatafilter.length > 0) {
                    return;
                }
            }
        }
        $('#colors_sketch').sketch({ defaultColor: "#ff0" });
        $("#colors_sketch").attr("style", 'background: url(' + data + ');background-repeat: no-repeat; background-size: 600px 500px;');
      
        x++;

        if (appendDatatoUpload === null) {
            var newAttribute = JSON.stringify({ "SCANDOCBKG_DATA": data, "NOTES": '', "PATIENT_ID": bookingInfo.PATIENT_ID, "EVENT_ID": bookingInfo.BOOKING_ID, "SCANDOCS_DATA": '', "FILE_NAME": file_name, "FILE_TYPE": 'JPG', "CATEGORY": 'Consultation', "CREATED_BY": staffLogonSessions.STAFF_ID, "CREATED_DATE": strDate });
            var appendDatatoUpload = JSON.parse('[' + newAttribute + ']');

        } else {
            // appendData = appendData.filter(function (item) { return item.FILE_NAME != ""});
            appendDatatoUpload.push({ "SCANDOCBKG_DATA": data, "NOTES": '', "PATIENT_ID": bookingInfo.PATIENT_ID, "EVENT_ID": bookingInfo.BOOKING_ID, "SCANDOCS_DATA": '', "FILE_NAME": file_name, "FILE_TYPE": 'JPG', "CATEGORY": 'Consultation', "CREATED_BY": staffLogonSessions.STAFF_ID, "CREATED_DATE": strDate });
        }
        
        
        sessionStorage.setItem(ah.config.skey.imagesTemplatetoUpload, JSON.stringify(appendDatatoUpload));

    };
    self.readImage = function () {

        var senderID = $(arguments[1].currentTarget).attr('id');
        var headerText = '';
        var subHeaderText = '';
        alert(senderID);
        switch (senderID) {
            case ah.config.tagId.imageTemplate1: {
              //  var imgs = $("#image2").attr("src");
                $("#colors_sketch").attr("style", "background: url(../images/image1.png);background-repeat: no-repeat; background-size: 600px 500px;");

                break;
            }

            case ah.config.tagId.imageTemplate2: {          
                $("#colors_sketch").attr("style", "background: url(../images/image2.png);background-repeat: no-repeat; background-size: 600px 500px;");                
                break;
            }
           
            case ah.config.tagId.imageTemplate3: {
                $("#colors_sketch").attr("style", "background: url(../images/image3.png);background-repeat: no-repeat; background-size: 600px 500px;");
                break;
            }
            case ah.config.tagId.imageTemplate4: {
                $("#colors_sketch").attr("style", "background: url(../images/image4.png);background-repeat: no-repeat; background-size: 600px 500px;");
                break;
            }
            case ah.config.tagId.imageTemplate5: {
                $("#colors_sketch").attr("style", "background: url(../images/image5.png);background-repeat: no-repeat; background-size: 600px 500px;");
                break;
            }

        }

   



    };   
    self.signOut = function () {
        
        window.location.href = ah.config.url.homeIndex;

    };

    self.saveAllChanges = function () {
        var imageDatatoUpload = JSON.parse(sessionStorage.getItem(ah.config.skey.imagesTemplatetoUpload)); 
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        if (imageDatatoUpload === null) {
            alert('Unable to proceed. There is no data to be save.')
            return;
        }

        var base64 = $('#colors_sketch')[0].toDataURL();
        imageDatatoUpload[0].SCANDOCS_DATA = base64;
        ah.toggleSaveMode()

        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        
        imageDatatoUpload.LAST_UPDATED = strDateStart + ' ' + hourmin;
        imageDatatoUpload.PUSH_FLAG = "Y";
        postData = { PATIENT_SCANDOCS: imageDatatoUpload, STAFF_LOGON_SESSIONS: staffLogonSessions };

        $.post(self.getApi() + ah.config.api.createpatientImages, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        

    };

    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            self.showPatientConsultationInfo();
        }

    };

    self.newSkecthImage = function () {
        ah.toggleNewMode();
    };
    self.showPatientInfo = function (jsonData) {

        
        var bookingInfo = JSON.parse(sessionStorage.getItem(ah.config.skey.patientBookingInfo));
       // alert(JSON.stringify(bookingInfo));

        ah.LoadJSON(bookingInfo);

        var rd = jsonData.SCANDOCS_CATEGORY;
        $(ah.config.cls.imagestemplateList + ' ' + ah.config.tag.ul).html('');
        var htmlstr = "";
        if (rd.length) {

            for (var o in rd) {


                htmlstr += '<li>';
                htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                htmlstr += ah.formatString(ah.config.html.searchBookRowHeaderTemplate, JSON.stringify(rd[o]), rd[o].CATEGORY, 'No.of Records: ' + rd[o].TOTALRECORDS, "");
                htmlstr += '</span>';

                htmlstr += '</li>';


            }
        }

        $(ah.config.cls.imagestemplateList + ' ' + ah.config.tag.ul).append(htmlstr);
        $(ah.config.cls.consultbookDetailItem).bind('click', self.searchCategoryImages);
    
    };

    self.displayImage = function () {
       // alert('here');
        var imageInfo = this.getAttribute(ah.config.attr.dataimageTemplate);
       // alert(JSON.stringify(imageInfo));
       //// $(ah.config.cls.demo + ' ' + ah.config.tag.ul).append("");
       // var image = new Image();
       // image.src = imageInfo;
       

        $("#colors_sketch").attr("style", 'background: url(' + imageInfo + ');background-repeat: no-repeat; background-size: 600px 500px;');
      
    };

    self.readPatientEventID = function () {
        
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var bookingInfo = JSON.parse(sessionStorage.getItem(ah.config.skey.patientBookingInfo));
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        var fldPatientID = bookingInfo.PATIENT_ID;
        var fldEventID = bookingInfo.BOOKING_ID;
        ah.toggleReadMode();
        postData = { PATIENT_ID: fldPatientID, EVENT_ID: fldEventID, STAFF_LOGON_SESSIONS: staffLogonSessions };
       
        $.post(self.getApi() + ah.config.api.searchImagesSketch, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.searchCategoryImages = function () {
        var cagetoryInfo = JSON.parse(this.getAttribute(ah.config.attr.dataitemInfo));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        ah.CurrentMode = ah.config.mode.read;
        

        postData = { PATIENT_ID: cagetoryInfo.PATIENT_ID, CATEGORY: cagetoryInfo.CATEGORY, STAFF_LOGON_SESSIONS: staffLogonSessions };

        $.post(self.getApi() + ah.config.api.searchInfoCagetoryImages, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.showCagetoryImages = function (jsonData) {


        var cagetoryDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.categoryImages));

        var htmlStr;

        //  ah.ResetControls();
        var rd = jsonData.SCANDOCS_CATEGORYIMAGELIST;

        var currCategoryId = "";
        if (rd.length > 0) {
            currCategoryId = rd[0].CATEGORY;
        }
        $(ah.config.cls.imagestemplateList + ' ' + ah.config.tag.ul).html('');
        var htmlstr = "";

        if (cagetoryDetails.length) {

            for (var o in cagetoryDetails) {


                htmlstr += '<li>';
                htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                htmlstr += ah.formatString(ah.config.html.searchBookRowHeaderTemplate, JSON.stringify(cagetoryDetails[o]), cagetoryDetails[o].CATEGORY, 'No.of Records: ' + cagetoryDetails[o].TOTALRECORDS, "");
                htmlstr += '</span>';

                htmlstr += '</li>';
                if (currCategoryId === cagetoryDetails[o].CATEGORY) {
                    for (var x in rd) {
                        htmlstr += '<li>';
                        htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                        htmlstr += ah.formatString(ah.config.html.searchListRowHeaderTemplate, JSON.stringify(rd[x]), '  ' + ah.formatJSONDateToString(rd[x].CREATED_DATE), '(' + rd[x].FILE_NAME + ')', "");
                        htmlstr += '</span>';

                        htmlstr += '</li>';

                    }
                }


            }
        }

        $(ah.config.cls.imagestemplateList + ' ' + ah.config.tag.ul).append(htmlstr);
        $(ah.config.cls.consultbookDetailItem).bind('click', self.searchCategoryImages);
        $(ah.config.cls.imageItem).bind('click', self.readPatientImage);

    };
    self.readPatientImage = function () {
      
        var imageDetails = JSON.parse(this.getAttribute(ah.config.attr.datalistitemInfo));

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        //$(ah.config.id.frmCategory).hide();
        
       // alert(JSON.stringify(imageDetails));

        ah.CurrentMode = ah.config.mode.read;

        postData = { PATIENT_ID: imageDetails.PATIENT_ID, SCANDOC_ID: imageDetails.SCANDOC_ID, STAFF_LOGON_SESSIONS: staffLogonSessions };

        $.post(self.getApi() + ah.config.api.searchInfoImages, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };

    self.showPatientImagesInfo = function (jsonData) {
        var rd = jsonData.PATIENT_SCANDOCS;
        if (rd.length) {
            //SCANDOCBKG_DATA
            for (var o in rd) {
                $("#imgCapture").attr("style", 'background: url(' + rd[o].SCANDOCBKG_DATA + ');background-repeat: no-repeat; background-size: 600px 500px;');
                $("#imgCapture").attr("src", rd[o].SCANDOCS_DATA);
                $("#imgCapture").show();
            }
        }

    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleSearchMode();
        var date = Date.parse($(ah.config.id.txtDateFrom).val());
        var jDate = new Date(date);
        //alert(jDate);
        var strDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();
        //alert(strDate);
        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), DATE_FROM: strDate, STAFF_LOGON_SESSIONS: staffLogonSessions };

        $.post(self.getApi()+ ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.searchItems = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleSearchMode();

        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), CATEGORY: "", ACTION: "ORDERLIST", STAFF_LOGON_SESSIONS: staffLogonSessions };

        $.post(self.getApi() + ah.config.api.searchItems, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };



    self.webApiCallbackStatus = function (jsonData) {

        //alert(JSON.stringify(jsonData));

        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));

            window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.PATIENT_SCANDOCS || jsonData.SCANDOCS_CATEGORYIMAGELIST || jsonData.CONSULTATION_IMGSKETCHTEMP || jsonData.SCANDOCS_CATEGORY || jsonData.SCANDOCS_CATEGORYIMAGELIST || jsonData.PATIENT_DETAILS || jsonData.SUCCESS) {
           

            if (ah.CurrentMode == ah.config.mode.save) {
                if (jsonData.SUCCESS) {

                } else {
                    //sessionStorage.setItem(ah.config.skey.patientconsultationDetails, JSON.stringify(jsonData.PATIENT_CONSULTATIONS));
                }
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);

                ah.showSavingStatusSuccess();
                self.initialize();



            }
            else if (ah.CurrentMode == ah.config.mode.read) {

                if (jsonData.SCANDOCS_CATEGORY) {
                    //sessionStorage.setItem(ah.config.skey.patientconsultationDetails, JSON.stringify(jsonData.PATIENT_CLINIC_CONSULTATIONBOOKINGS));
                    sessionStorage.setItem(ah.config.skey.categoryImages, JSON.stringify(jsonData.SCANDOCS_CATEGORY));
                    self.showPatientInfo(jsonData);
                }
                else if (jsonData.PATIENT_SCANDOCS) {
                    sessionStorage.setItem(ah.config.skey.patientScanDocs, JSON.stringify(jsonData.PATIENT_SCANDOCS));
                    self.showPatientImagesInfo(jsonData);
                }

                else if (jsonData.SCANDOCS_CATEGORYIMAGELIST) {
                    // alert(JSON.stringify(jsonData));
                    self.showCagetoryImages(jsonData);

                }

            }
            else if (ah.CurrentMode == ah.config.mode.search) {

                if (jsonData.PRODUCT_DETAILS) {

                    self.searchResultItems(jsonData);
                } else {
                    self.searchResult(jsonData);
                }

            }
            else if (ah.CurrentMode == ah.config.mode.idle) {


            }

        }
        else {

            alert(systemText.errorTwo);
            window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};