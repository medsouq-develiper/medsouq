﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divServiceID: '.divServiceID',
            statusText: '.status-text',
            serviceDetailItem: '.serviceDetailItem'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveService: '#saveService',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmService: '#frmService',
            serviceID: '#SERVICE_ID',
            successMessage: '#successMessage'
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            dataServiceID: 'data-serviceID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            serviceDetails: 'SERVICES',
            searchResult: 'searchResult'
        },
        url: {
            homeIndex: '/Home/Index'
        },
        api: {
            readService: '/Setup/ReadService',
            searchAll: '/Search/SearchServices',
            createService: '/Setup/CreateService',
            updateService: '/Setup/UpdateServiceDetails'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal serviceDetailItem' data-serviceID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}</a>",
            searchRowTemplate: '<span class="search-row-label fc-light-grey-2">{0}: <span class="search-row-data">{1}</span></span>'
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divServiceID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divServiceID
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatuxText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divServiceID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

        $(
           thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
           thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

  

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name),
                type = $el.attr('type');

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var servicesViewModel = function (systemText) {

    var self = this;

    var ah = new appHelper(systemText);

    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));

        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.toggleReadMode();

  
        ah.initializeLayout();

    };

    self.createNewService = function () {

        ah.toggleAddMode();

    };

    self.modifyServiceDetails = function () {

        ah.toggleEditMode();

    };

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.saveAllChanges = function () {

        $(ah.config.id.saveService).trigger('click');

    };

    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            self.showServiceDetails();
        }

    };

    self.showServiceDetails = function (isNotUpdate) {

        if (isNotUpdate) {

            var serviceDetails = sessionStorage.getItem(ah.config.skey.serviceDetails);

            var jService = JSON.parse(serviceDetails);

            ah.ResetControls();
            ah.LoadJSON(jService);
        }

        ah.toggleDisplayMode();

    };

    self.searchResult = function (jsonData) {

        sessionStorage.setItem(ah.config.skey.searchResult, JSON.stringify(jsonData.SERVICES));
        var sr = jsonData.SERVICES;

        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].SERVICE_ID, sr[o].DESCRIPTION_SHORT, '','');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.descriptionname, sr[o].DESCRIPTION);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.descriptionothname, sr[o].DESCRIPTION_OTH);

                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.serviceDetailItem).bind('click', self.readServiceByServiceID);
        ah.displaySearchResult();

    };

    self.readServiceByServiceID = function () {

        var serviceID = this.getAttribute(ah.config.attr.dataServiceID).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();
    
        postData = { SERVICE_ID: serviceID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi()+ ah.config.api.readService, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleSearchMode();

        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.saveServiceDetails = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var serviceDetails = ah.ConvertFormToJSON($(ah.config.id.frmService)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        serviceDetails.CREATED_BY = staffLogonSessions.STAFF_ID;
        postData = { SERVICES: serviceDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };

        if (ah.CurrentMode == ah.config.mode.add) {

            ah.toggleSaveMode();
            $.post(self.getApi() + ah.config.api.createService, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {

            ah.toggleSaveMode();
            $.post(self.getApi() + ah.config.api.updateService, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }

    };

    self.webApiCallbackStatus = function (jsonData) {
       
        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.SERVICE_ID || jsonData.SERVICES || jsonData.SUCCESS || jsonData.SERVICES) {

            if (ah.CurrentMode == ah.config.mode.save) {

                sessionStorage.setItem(ah.config.skey.serviceDetails, JSON.stringify(jsonData.SERVICES));
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();

                if (jsonData.SUCCESS) {
                    self.showServiceDetails(false);
                }
                else {
                    self.showServiceDetails(true);
                }
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                sessionStorage.setItem(ah.config.skey.serviceDetails, JSON.stringify(jsonData.SERVICES));
                self.showServiceDetails(true);
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
                self.searchResult(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {

               
            }

        }
        else {

            alert(systemText.errorTwo);
            window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};