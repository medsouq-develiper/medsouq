﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            detailItem: '.detailItem'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeNumber: 'input[type=number]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7,
            advanceSearch: 8,
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmInfo: '#frmInfo',
            staffID: '#STAFF_ID',
            successMessage: '#successMessage',
            displayType: '#DISPLAYTYPE',
            searchTransTableTV: '#searchTransTableTV',
            costcenterStrList: '#costcenterStrList',
            storeCURR: '#STORENO',
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'searchResult',
            infoDetails: 'AM_SUPPLIER'
        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalFilter: '#openModalFilter',
            modalClose: '#',
            openReturnRecallModal: '#openReturnRecallModal'
        },
        api: {

            searchLookUps: '/Inventory/SearchStoresByUser',
            searchAll: '/Inventory/SearchStoreBatches',
            createSetup: '/AIMS/CreateNewPMProcedure',
            updateSetup: '/AIMS/UpdatePMProcedureInfo',
            searchLov: '/AIMS/SearchAimsLOV',
            searchSetupLov: '/Setup/SearchInventoryLOV',
            returnRecallStockBatch: '/Inventory/ReturnRecallStockBatch',
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
            searchRowTemplateRed: "<span class='search-row-label'>{0}: <span class='fc-red'>{1}</span></span>",
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID
        ).hide();


        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("PROCEDURE_CODE").disabled = true;

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("PROCEDURE_CODE").disabled = true;

    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleAddExistMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;


        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.searhButton).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.statusText +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.iconDet + ',' + thisApp.config.cls.divProgress
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);


    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {
        $(form).find("input:disabled").removeAttr("disabled");
        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.getAge = function (birthDate) {

        var seconds = Math.abs(new Date() - birthDate) / 1000;

        var numyears = Math.floor(seconds / 31536000);
        var numdays = Math.floor((seconds % 31536000) / 86400);
        var nummonths = numdays / 30;

        return numyears + " y " + Math.round(nummonths) + " m ";
    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name),
                type = $el.attr('type');

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var storeBatchesViewModel = function (systemText) {

    var self = this;

    var ah = new appHelper(systemText);
    var searchDisplayBy = null;
    self.srDataArray = ko.observableArray([]);
    self.srDataArraySum = ko.observableArray([]);
    var lovCostCenterArray = ko.observableArray([]);
    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.toggleReadMode();
       
        ah.initializeLayout();
        var postData;
        postData = { SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchLookUps, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

        postData = { LOV: "'STOCKBATCH_STATUSTYPE','RPT_ACTION'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchSetupLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.searchFilter = {
        fromDate: ko.observable(''),
        toDate: ko.observable(''),
        StoreId: ko.observable(''),
        ItemType: ko.observable(''),
        SerialBatch: ko.observable(''),
        Status: ko.observable(''),
        InvoiceNo: ko.observable(''),
    }
    self.selectedBatch = ko.observable({
        STATUS: ko.observable('')
    });
    self.statusType = ko.observableArray([]);
    self.systemDefault = ko.observable({});
    self.storeBatches = [];
    self.typesOfAction = ko.observableArray([]);

    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.createNew = function () {

        ah.toggleAddMode();

    };

    self.modifySetup = function () {

        ah.toggleEditMode();

    };

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.saveAllChanges = function () {

        $(ah.config.id.saveInfo).trigger('click');

    };

    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();

        } else {
            ah.toggleDisplayMode();
            self.showDetails(true);
        }

    };
    self.clearFilter = function () {
        self.searchFilter.toDate('');
        self.searchFilter.fromDate('');
        $('#requestFromDate').val('');
        $('#requestStartDate').val('');

        self.searchFilter.ItemType('');
        self.searchFilter.SerialBatch('');
        self.searchFilter.Status('');
        self.searchFilter.InvoiceNo('');

    }
    self.showModalFilter = function () {
        ah.CurrentMode = ah.config.mode.advanceSearch;
       
        window.location.href = ah.config.url.modalFilter;
    };
    self.showDetails = function (isNotUpdate) {

        if (isNotUpdate) {

            var infoDetails = sessionStorage.getItem(ah.config.skey.infoDetails);

            var jsetupDetails = JSON.parse(infoDetails);

            ah.ResetControls();
            ah.LoadJSON(jsetupDetails);
            $("#PROCEDURE_CODE").val(jsetupDetails.PROCEDURE_ID);
        }

        ah.toggleDisplayMode();

    };

    self.getObservableValue = function (data) {
        if (Array.isArray(data)) {
            var returnData = [];
            for (var i = 0; i < data.length; i++) {
                var _object = data[i];
                returnData.push({});
                Object.keys(_object).forEach(function (key) {
                    if (ko.isObservable(_object[key]))
                        returnData[i][key] = _object[key]();
                    else
                        returnData[i][key] = _object[key];
                });
            }
        }
        else if (data instanceof Object) {
            var returnData = {};
            var _object = data;
            Object.keys(_object).forEach(function (key) {
                if (ko.isObservable(_object[key]))
                    returnData[key] = _object[key]();
                else
                    returnData[key] = _object[key];
            });
        }
        else
            returnData = data;

        return returnData;
    };
    self.searchResult = function (jsonData) {
        ah.displaySearchResult();
       
        var table;
        var srden = jsonData.STORE_BATCHES;
        self.storeBatches = srden;

        if ($.fn.dataTable.isDataTable(ah.config.id.searchTransTableTV)) {
            $(ah.config.id.searchTransTableTV).DataTable().clear().destroy();
        }
        table = $(ah.config.id.searchTransTableTV).DataTable({
            data: srden,
            createdRow: function (row, data, dataIndex) {
                if (data.STATUS == 'RETURNED' || data.STATUS == 'RETURN' || data.STATUS == 'RECALL')
                    $(row).addClass('fc-red');

                $(row).find('.btnReturnRecall').on('click', function (event) {
                    event.preventDefault();
                    var pCode = $(this).closest("tr").find("td:eq(1)").text();
                    var batch = $(this).closest("tr").find("td:eq(4)").text();
                    var item = self.storeBatches.slice(0).find(function (item) { return item.PRODUCT_CODE == pCode && item.BARCODE_ID == batch });
                    item = self.getObservableValue(item);
                    item.viewOnly = false;
                    if (item.STATUS == 'RETURNED' || item.STATUS == 'RETURN' || item.STATUS == 'RECALL' || item.RETURN_TYPE) {
                        item.viewOnly = true;
                        item.STATUS = ko.observable(item.STATUS);
                        self.selectedBatch(item);
                    }
                    else {
                        item.STATUS = ko.observable('RETURN');
                        item.RETURN_TYPE = ko.observable('');
                        item.RETURN_REASON = ko.observable('');
                        item.SUPPLIER = ko.observable(item.SUPPLIER || '')
                        item.supplierIsNull = true;
                        if (item.SUPPLIER())
                            item.supplierIsNull = false;

                        self.selectedBatch(item);
                    }

                    self.systemDefault(JSON.parse(sessionStorage.getItem('0ed52a42-9019-47d1-b1a0-3fed45f32b8f'))[0]);

                    window.location.href = ah.config.url.openReturnRecallModal;
                });

                //$(row).find('.btnReturned').on('click', function (event) {
                //    event.preventDefault();
                //    var pCode = $(this).closest("tr").find("td:eq(1)").text();
                //    var batch = $(this).closest("tr").find("td:eq(4)").text();
                //    var item = self.storeBatches.slice(0).find(function (item) { return item.PRODUCT_CODE == pCode && item.BARCODE_ID == batch });

                //    var confirmFlag = confirm('Are you sure you want to return.');
                //    if (confirmFlag) {
                //        ah.toggleSaveMode();
                //        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
                //        var PRODUCT_STOCKBATCH = item;
                //        PRODUCT_STOCKBATCH.STATUS = 'RETURNED';
                //        PRODUCT_STOCKBATCH.CREATED_DATE = PRODUCT_STOCKBATCH.CREATED_DATE ? moment(PRODUCT_STOCKBATCH.CREATED_DATE, 'DD/MM/YYYY').format() : '';
                //        PRODUCT_STOCKBATCH.EXPIRY_DATE = PRODUCT_STOCKBATCH.EXPIRY_DATE ? moment(PRODUCT_STOCKBATCH.EXPIRY_DATE, 'DD/MM/YYYY').format() : '';

                //        var postData = { PRODUCT_STOCKBATCH: PRODUCT_STOCKBATCH, STAFF_LOGON_SESSIONS: staffLogonSessions };
                //        $.post(self.getApi() + ah.config.api.returnRecallStockBatch, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
                //    }
                //});
            },
            columns: [
                { data: 'CREATED_DATE' },
                { data: 'PRODUCT_CODE' },
                { data: 'PRODUCT_NAME' },
                { data: 'ITEMTYPE' },
                { data: 'BARCODE_ID' },
                { data: 'STOCK_ONHAND' },
                { data: 'EXPIRY_DATE' },
                { data: 'INVOICENO' },
                { data: 'STORE_DESC' },
                { data: 'STATUS' },
                {
                    data: null,
                    "orderable": false,
                    render: function (data, type, row) {
                        if (data.STATUS == 'RETURN' || data.STATUS == 'RECALL' || data.STATUS == 'RETURN' || data.RETURN_TYPE)
                            return [
                                "<a href='#' class='button-ext btnReturnRecall' title='Return Or Recall Information'><i class='fa fa-share-square-o'></i></a>&nbsp;"
                            ].join('');
                        else
                            return null;
                    }
                }
            ],
            "searching": false,
            //"order":true,
            "iDisplayLength": 100,
            "aLengthMenu": [100, 200, 500, 6500, srden.length]
        });
    };
    self.returnRecallModalSubmit = function () {
        ah.toggleSaveMode();
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var PRODUCT_STOCKBATCH = self.selectedBatch();
        PRODUCT_STOCKBATCH.CREATED_DATE = PRODUCT_STOCKBATCH.CREATED_DATE ? moment(PRODUCT_STOCKBATCH.CREATED_DATE, 'DD/MM/YYYY').format() : '';
        PRODUCT_STOCKBATCH.EXPIRY_DATE = PRODUCT_STOCKBATCH.EXPIRY_DATE ? moment(PRODUCT_STOCKBATCH.EXPIRY_DATE, 'DD/MM/YYYY').format() : '';

        var postData = { PRODUCT_STOCKBATCH: PRODUCT_STOCKBATCH, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.returnRecallStockBatch, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
   
    self.dateClick = function () {
        $("#dateClickAction").val("ACTIVE");
    };

    self.searchFilterNow = function () {
        window.location.href = ah.config.url.modalClose;
        self.searchNow();
    }
    self.searchAdvanceLOV = function () {
        if (lovCostCenterArray.length == 0 || lovManufacturerArray.length == 0 || lovSupplierArray.length == 0) {
            var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
            var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
            var postData;
            postData = { LOV: "'AIMS_COSTCENTER'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
    };

    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();

        searchDisplayBy = $(ah.config.id.displayType).val();

        var fromDate = $('#requestFromDate').val().split('/');

        if (fromDate.length == 3) {
            self.searchFilter.fromDate(fromDate[2] + "/" + fromDate[1] + "/" + fromDate[0]);
        }
        var toDate = $('#requestStartDate').val().split('/');
        if (toDate.length == 3) {
            self.searchFilter.toDate(toDate[2] + "/" + toDate[1] + "/" + toDate[0] + "  23:00:00.000");
        }
        self.searchFilter.StoreId($("#STORENO").val());

        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), SEARCH_FILTER: self.searchFilter, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };

    self.saveInfo = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var setupDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        setupDetails.CREATED_BY = staffLogonSessions.STAFF_ID;

        if (setupDetails.AVERAGE_TIME === null || setupDetails.AVERAGE_TIME === "") {
            setupDetails.AVERAGE_TIME = 0;
        }


        if (ah.CurrentMode == ah.config.mode.add) {
            var jDate = new Date();
            var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);

            setupDetails.CREATED_DATE = strDateStart + ' ' + hourmin;
            postData = { AM_PROCEDURES: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            ah.toggleSaveMode();
            alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.createSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {

            ah.toggleSaveMode();
            postData = { AM_PROCEDURES: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            //alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.updateSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }

    };

    self.webApiCallbackStatus = function (jsonData) {
        //  alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {
            if (jsonData.ERROR.ErrorText.includes("Duplicate")) {
                ah.toggleAddExistMode();

            }
            else {
                alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
                //window.location.href = ah.config.url.homeIndex;
            }

        }
        else if (jsonData.STORES || jsonData.LOV_LOOKUPS || jsonData.STORE_BATCHES || jsonData.TRANSACTION_SUMMARY || jsonData.SUCCESS || jsonData.PRODUCT_STOCKBATCH) {

            if (ah.CurrentMode == ah.config.mode.save) {
                if (jsonData.PRODUCT_STOCKBATCH) {
                    $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                    ah.showSavingStatusSuccess();

                    window.location.href = ah.config.url.modalClose;

                    self.searchNow();
                }
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_PROCEDURES));
                self.showDetails(true);
            } else if (ah.CurrentMode == ah.config.mode.advanceSearch) {
                lovCostCenterArray = jsonData.LOV_LOOKUPS;
                $.each(lovCostCenterArray, function (item) {

                    $(ah.config.id.costcenterStrList)
                        .append($("<option>")
                            .attr("value", lovCostCenterArray[item].DESCRIPTION.trim())
                            .attr("id", lovCostCenterArray[item].LOV_LOOKUP_ID));
                });

            }

            else if (ah.CurrentMode == ah.config.mode.search) {
                self.searchResult(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {

                if (jsonData.STORES) {
                    var store = jsonData.STORES;
                    $.each(store, function (item) {
                        $(ah.config.id.storeCURR)
                            .append($("<option></option>")
                                .attr("value", store[item].STORE_ID)
                                .text(store[item].DESCRIPTION));
                    });
                }
                if (jsonData.LOV_LOOKUPS) {
                    var statusType = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'STOCKBATCH_STATUSTYPE' });
                    statusType.unshift({
                        LOV_LOOKUP_ID: null,
                        DESCRIPTION: '- select -'
                    });
                    self.statusType(statusType);

                    var typesOfAction = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'RPT_ACTION' });
                    typesOfAction.unshift({
                        LOV_LOOKUP_ID: null,
                        DESCRIPTION: '- select -'
                    });
                    self.typesOfAction(typesOfAction);
                }
            }

        }
        else {

            alert(systemText.errorTwo);
            //window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};

//