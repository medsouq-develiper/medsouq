﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divStaffUserID: '.divStaffUserID',
            statusText: '.status-text',
            staffuserDetailItem: '.staffuserDetailItem'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email],input[type=password]',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveStaffUser: '#saveStaffUser',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmStaffUser: '#frmStaffUser',
            staffuserID: '#STAFF_ID',
            staffgroupsID: '#STAFF_GROUP_ID',
            staffpositionID: '#STAFF_POSITION_ID',
            successMessage: '#successMessage'
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            dataStaffUserID: 'data-StaffUserID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            staffuserDetails: 'STAFF_USER',
            searchResult: 'searchResult'
        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex'
        },
        api: {
            getLookupList: '/Staff/GetStaffUserLookUpTables',
            readStaffUser: '/Staff/ReadStaffUser',
            searchAll: '/Search/SearchStaffUser',
            createStaffUser: '/Staff/CreateStaffUser',
            updateStaffUser: '/Staff/UpdateStaffUserDetails'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal staffuserDetailItem' data-staffuserID='{0}'><i class='fa fa-arrow-circle-o-right' id='menu'></i> </i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplate: '<span class="search-row-label fc-light-grey-2">{0}: <span class="search-row-data">{1}</span></span>'
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divStaffUserID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divStaffUserID
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divStaffUserID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

        $(
           thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
           thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.getAge = function (birthDate) {

        var seconds = Math.abs(new Date() - birthDate) / 1000;

        var numyears = Math.floor(seconds / 31536000);
        var numdays = Math.floor((seconds % 31536000) / 86400);
        var nummonths = numdays / 30;

        return numyears + " y " + Math.round(nummonths) + " m ";
    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.formatJSONDateTimeToString = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0] + ' ' + strTime[1].substring(0, 5);


        return dateToFormat;
    };

    thisApp.formatJSONDateToString = function () {

        var dateToFormat = arguments[0];
        var strDate;

        if (dateToFormat === null) return "-";
        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');


            if (isDateTime) {
                val = thisApp.formatJSONDateTimeToString(val);
            }

            if (isDate) {
                val = thisApp.formatJSONDateToString(val);
            }

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var staffuserViewModel = function (systemText) {

    var self = this;

    var ah = new appHelper(systemText);

    window.onload = function () {
        var options =
        {
            imageBox: '.imageBox',
            thumbBox: '.thumbBox',
            spinner: '.spinner',
            imgSrc: '/images/nophotok.png'
        }
        var cropper = new cropbox(options);
        document.querySelector('#file').addEventListener('change', function () {
            var reader = new FileReader();
            reader.onload = function (e) {
                options.imgSrc = e.target.result;
                cropper = new cropbox(options);
            }
            reader.readAsDataURL(this.files[0]);
            this.files = [];
        })
        document.querySelector('#btnCrop').addEventListener('click', function () {
            var img = cropper.getDataURL();
            document.querySelector('.cropped').innerHTML = '<a href="#openModal"><img id="PHOTO_IMAGE" src="' + img + '" width = 100px height = 100px></a>';
        })
        document.querySelector('#btnZoomIn').addEventListener('click', function () {
            cropper.zoomIn();
        })
        document.querySelector('#btnZoomOut').addEventListener('click', function () {
            cropper.zoomOut();
        })
    };

    self.initialize = function () {

        var webApiURL = localStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(localStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(localStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.toggleReadMode();

        postData = { STAFF_LOGON_SESSIONS: staffLogonSessions };
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        $.post(api + ah.config.api.getLookupList, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

        ah.initializeLayout();

    };
    self.getApi = function () {
        var staffDetails = JSON.parse(localStorage.getItem(ah.config.skey.staff));
        var webApiURL = localStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.returnHome = function () {

        window.location.href = ah.config.url.aimsHomepage;
    };
    self.createNewStaffUser = function () {

        ah.toggleAddMode();

    };

    self.modifyStaffUserDetails = function () {

        ah.toggleEditMode();

    };
    self.cropImage = function () {
        

    };

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.saveAllChanges = function () {

        $(ah.config.id.saveStaffUser).trigger('click');

    };

    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            self.showStaffUserDetails(true);
        }

    };

    self.showStaffUserDetails = function (isNotUpdate) {

        if (isNotUpdate) {

            var staffuserDetails = localStorage.getItem(ah.config.skey.staffuserDetails);
            var jStaffUser = JSON.parse(staffuserDetails);

            var strDate;

            strDate = String(jStaffUser.PASSWORD_EXPIRY).split('-', 3);
            jStaffUser.PASSWORD_EXPIRY = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0]

            ah.ResetControls();
            ah.LoadJSON(jStaffUser);
        }

        ah.toggleDisplayMode();

    };

    self.searchResult = function (jsonData) {

        localStorage.setItem(ah.config.skey.searchResult, JSON.stringify(jsonData.STAFF));
        var sr = jsonData.STAFF;

        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {
                var passwordExpiry = new Date(sr[o].PASSWORD_EXPIRY);

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].STAFF_ID, sr[o].STAFF_ID, sr[o].FIRST_NAME.toLowerCase(), sr[o].LAST_NAME.toLowerCase());
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.userid, sr[o].USER_ID);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.emailaddress, sr[o].EMAIL_ADDRESS);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.passwordexpiry, passwordExpiry.toDateString().substring(4, 15));
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.staffposition, sr[o].POSITION_DESCRIPTION);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.staffgroup, sr[o].GROUP_DESCRIPTION);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.status, (sr[o].STATUS));
                htmlstr += '</li>';

            }
            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.staffuserDetailItem).bind('click', self.readStaffUserByStaffID);
        ah.displaySearchResult();

    };

    self.readStaffUserByStaffID = function () {

        var staffuserID = this.getAttribute(ah.config.attr.dataStaffUserID).toString();
        var webApiURL = localStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(localStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { STAFF_ID: staffuserID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readStaffUser, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.searchNow = function () {

        var webApiURL = localStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(localStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleSearchMode();

        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.saveStaffUserDetails = function () {

        var webApiURL = localStorage.getItem(ah.config.skey.webApiUrl);
        var staffuserDetails = ah.ConvertFormToJSON($(ah.config.id.frmStaffUser)[0]);
        var staffLogonSessions = JSON.parse(localStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        //var someimage = document.getElementById('PHOTO_IMAGE');
        //var myimg = someimage.getElementsByTagName('img')[0];
        //var mysrc = myimg.src;
        //console.log(mysrc);
        //alert(mysrc);
        //return
        staffuserDetails.CREATED_BY = staffLogonSessions.STAFF_ID;
        postData = { STAFF: staffuserDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };

        if (ah.CurrentMode == ah.config.mode.add) {

            ah.toggleSaveMode();
            $.post(self.getApi() + ah.config.api.createStaffUser, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {

            ah.toggleSaveMode();

            $.post(self.getApi() + ah.config.api.updateStaffUser, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }

    };

    self.webApiCallbackStatus = function (jsonData) {

        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.STAFF_ID || jsonData.STAFF || jsonData.STAFF_POSITIONS || jsonData.STAFF_GROUPS || jsonData.SUCCESS || jsonData.STAFF) {

            if (ah.CurrentMode == ah.config.mode.save) {

                localStorage.setItem(ah.config.skey.staffuserDetails, JSON.stringify(jsonData.STAFF));
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();

                if (jsonData.SUCCESS) {
                    self.showStaffUserDetails(false);
                }
                else {
                    self.showStaffUserDetails(true);
                }
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                localStorage.setItem(ah.config.skey.staffuserDetails, JSON.stringify(jsonData.STAFF));
                self.showStaffUserDetails(true);
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
                self.searchResult(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {

                $.each(jsonData.STAFF_GROUPS, function (item) {
                    $('#STAFF_GROUP_ID')
                        .append($("<option></option>")
                        .attr("value", jsonData.STAFF_GROUPS[item].STAFF_GROUP_ID)
                        .text(jsonData.STAFF_GROUPS[item].DESCRIPTION));
                });

                $.each(jsonData.STAFF_POSITIONS, function (item) {
                    $('#STAFF_POSITION_ID')
                        .append($("<option></option>")
                        .attr("value", jsonData.STAFF_POSITIONS[item].STAFF_POSITION_ID)
                        .text(jsonData.STAFF_POSITIONS[item].DESCRIPTION));
                });
            }

        }
        else {

            alert(systemText.errorTwo);
            window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};