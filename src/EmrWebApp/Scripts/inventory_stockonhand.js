﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            detailItem: '.detailItem',
            productDetailItem: '.productDetailItem',
            modalBox: '.modalBox',
            modalwidth5: '.modalwidth5',
            modalwidth7: '.modalwidth7',
			contentHeader: '#contentHeader',
			pagingDiv: '.pagingDiv',
            updSuccessModNotif: '.updSuccess',
        },
        fld: {
            menuZero: 'xikhkjxhexdher',
            
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul',
            table: 'table'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            txtGlobalSearchOth: '#txtGlobalSearchOth',
            frmInfo: '#frmInfo',
            frmitemInfo: '#itemInfo',
            storeCURR: '#STORENO',
            supplierCURR: '#SUPPLIER',
            staffID: '#STAFF_ID',
            totNumber: '#totNumber',
            loadNumber: '#loadNumber',
            searchResult1: '#searchResult1',
            pageNum: '#pageNum',
            pageCount: '#pageCount',
            populateData: '#populateData',
            successMessage: '#successMessage',
            initialTable: '#initialTable',
            itemDescription: '#itemDescription',
            itemUOM: '#itemUOM',
            infoStore: '#INFOSTORE',
            infoSupplier: '#INFOSUPPLIER',
            infoProductId: '#INFOPRODUCT_ID',
            infoProductDesc: '#INFOPRODUCT_DESC',
            infoUom: '#INFOUOM',
            addButton: '#addButton',
			updateButton: '#updateButton',
			PaginGpageNum: '#PaginGpageNum',
            PaginGpageCount: '#PaginGpageCount',
            searchResultParts: '#searchResultParts',
            itemsReceiveBatch: '#itemsReceiveBatch',
            itemsReceiveBatchCont: '#itemsReceiveBatchCont',
            searchResultBatchList: '#searchResultBatchList',
            dvRecal: '#dvRecal',
            ddSUPPLIER: '#SUPPLIER',
            ddMODELCODE: '#MODELCODE'
        },
        tagId: {
            page1: 'page1',
            page2: 'page2',
            priorPage: 'priorPage',
            nextPage: 'nextPage',
			txtGlobalSearchOth: 'txtGlobalSearchOth',
			PaginGnextPage: 'PaginGnextPage',
			PaginGpriorPage: 'PaginGpriorPage'

        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID',
            dataItems: 'data-items'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'searchResult',
            storeDetails: 'STORES',
            productItems: 'PRODUCT_STOCKONHAND',
			productStockBatch: 'PRODUCT_STOCKBATCH',
			groupPrivileges: 'GROUP_PRIVILEGES',
            productStockBatchPush: 'PRODUCT_STOCKBATCHPUSH'
        },
        model:{

        },
        url: {
            homeIndex: '/Home/Index',
            modalAddItems: '/inventory/stockBatchReceive#openModal',
            modalInfo: '/inventory/stockOnHand#openModalInfo',
            modalInfoItem: '/inventory/stockOnHand#openModalInfoItemLocation',
            modalBatchInfoItems: '/inventory/stockOnHand#openModalBachInfoItemsLocation',
            modalAddBachInfoItems: '/inventory/stockOnHand#openModalAddBachInfoItemsLocation',
            
            aimsHomepage: '/Menuapps/aimsIndex',
            modalClose: '#',
            modalSearhParts: '/inventory/stockOnHand#openModalPartList'
        },
        api: {

            readBatchList: '/Inventory/SearchStockBatchReceiveItems',
            searchAll: '/Inventory/SearchUserStoreStockOnHand',
            searchLookUps: '/Inventory/SearchStoresByUser',
            searchStockItems: '/Inventory/SearchStockItems',
            createStockBatchReceive: '/Inventory/CreateNewStockBatchReceive',
            updateSetup: '/Inventory/CreateNewSOHAdjust',
            updateItemLocation: '/Inventory/UpdateItemLocation',
            seachParts: '/Search/SearchPartsSOHN',
            searchProductBatch: '/Search/SearchPartsSOHN',
            GetStockBatchByProdStore: '/Inventory/GetStockBatchByProdStore',
            RecalculateProductBatch: '/Inventory/RecalculateProductBatch',
            updateProductBatch: '/Inventory/UpdateProductBatch',
            createProductBatch: '/Inventory/CreateProductBatch'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchLOVRowHeaderTemplate: "<a href='#openModalInfo' class='fs-medium-normal productDetailItem fc-greendark' data-items='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} <span>{3}</span></a>",
            searchExistRowHeaderTemplate: "<a href='#openModalInfo' class='fs-medium-normal productDetailItem fc-slate-blue' data-items='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} <span class='fc-green'> {3} </span></a>",
            searchDeleteRowHeaderTemplate: "<a href='#'onclick='deleteRow(this)' ><i class='fa fa-trash'></a>",
            searchRowTemplate: '<span class="search-row-label">{0} <span class="search-row-data">{1}</span></span>',
            searchRowHeaderTemplateList: "<a href='#openModalInfo' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>"
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };
    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
			thisApp.config.cls.pagingDiv + ',' +thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $( thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

       // thisApp.ResetControls();
        //$(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.contentField).show();

        $(
			thisApp.config.cls.pagingDiv + ',' +thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.id.contentHeader
        ).hide();
        $(thisApp.config.cls.liApiStatus).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };
    thisApp.toggleAddItems = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;


        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };
    thisApp.toggledisplayItemInfo = function () {

        //$(thisApp.config.cls.modalwidth5).hide();
        //$(thisApp.config.cls.modalwidth7).show();

    };
    thisApp.toggleSearchItems = function () {

        $(thisApp.config.cls.modalBox).hide();

        $(
            thisApp.config.cls.modalwidth3).show();

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatuxText);
        $(thisApp.config.cls.liApiStatus).show();

     

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentField
        ).hide();

   

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        //$(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
              thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
           thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

    
    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

   
    thisApp.formatJSONDateToString = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };
    thisApp.formatJSONDateToStringOth = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[0] + '-' + strDate[1] + '-' + strDate[2].substring(0, 2);


        return dateToFormat;
    };
    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name),
                type = $el.attr('type');

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var stockonhandViewModel = function (systemText) {
    var self = this;
    var ah = new appHelper(systemText);
    var srDataArray = ko.observableArray([]);
    var srProdArray = ko.observableArray([]);
    self.unsaveBatch = {
        PRODUCT_CODE: ko.observable(""),
        DESCRIPTION: ko.observable(""),
        STORE: ko.observable(""),
        STORE_DESC: ko.observable(""),
        UOM: ko.observable(""),
        EXPIRY_DATE: ko.observable(""),
        QTY: ko.observable(""),
        QTY_RECEIVED: ko.observable(""),
        STOCK_ONHAND: ko.observable(""),
        INVOICENO: ko.observable(""),
        BARCODE_ID: ko.observable(""),
        STATUS: ko.observable(""),
        MODELCODE: ko.observable(""),
        SUPPLIER: ko.observable(""),
        PARTTYPE: ko.observable(false),
    };
    self.modSOH = {
        BatchTotalItemCount: ko.observable(0),
    };
    var Suppliers = [];
    var Models = [];
    var currCount = 0;
    var sort = null;
    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.toggleReadMode();

        ah.initializeLayout();
        if (self.validatePrivileges('14') > 0) {
            $(ah.config.cls.liAddNew).show();

        };
        var postData;
        postData = { SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchLookUps, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        
    };

	
    //ko.applyBindings(new PagedGridModel(initialData));
    self.returnHome = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.itemsOnHand = ko.observableArray([]);
    self.modSOHBatchItems = ko.observableArray([]);
    self.isValidDate = function (dateString, action) {
        // First check for the pattern
        var regex_date = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
        var regex_date2 = /^(\d{1,2})-(\d{1,2})-(\d{4})$/;
        if (!regex_date.test(dateString)) {
            if (!regex_date2.test(dateString)) {
                return 'Invalid';
            }
        }
        // Parse the date parts to integers
        var n = dateString.search("-");
        var parts = "";
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var newDateFormat = "";
        if (n <= 0) {
            parts = dateString.split("/");
            var day = parseInt(parts[0], 10);
            var month = parseInt(parts[1], 10);
            var year = parseInt(parts[2], 10);
            newDateFormat = dateString;
        } else {
            parts = dateString.split("-");
            var day = parseInt(parts[0], 10);
            var month = parseInt(parts[1], 10);
            var year = parseInt(parts[2], 10);
            newDateFormat = (format_two_digits(day) + '/' + format_two_digits(month) + '/' + year).trim();
            $("#INFOEXPIRY_DATE").val(newDateFormat);

        }

        // Check the ranges of month and year
        if (year < 1000 || year > 3000 || month == 0 || month > 12) {
            return 'Invalid';
        }

        var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        // Adjust for leap years
        if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
            monthLength[1] = 29;
        }

        // Check the range of the day
        if (day > 0 && day <= monthLength[month - 1]) {
            return newDateFormat;
        } else {
            return 'Invalid';
        }
    };    
    
	self.applyAdjustment = function () {
		//
		var reason = $("#REASON").val();

		if (reason === null || reason < 5) {
			alert('Please enter a valid reason to proceed.');
			return;
		}
        var actionTrans = $("#ACTIONTRANS").val();
        if (actionTrans === "MODIFY") {
            var idx = $("#CURRINDEX").val();
            idx = parseInt(idx) + 1;
         
            document.getElementById("itemsReceiveBatch").rows[idx].cells[4].innerHTML = $("#INFOQTY").val();
            document.getElementById("itemsReceiveBatch").rows[idx].cells[2].innerHTML = $("#INFOMIN").val();
            document.getElementById("itemsReceiveBatch").rows[idx].cells[3].innerHTML = $("#INFOMAX").val();
        } else if (actionTrans === "NEW") {
            var allowUpdate = 'Y';
            if (currCount > 0) {
                currCount++;
            }
            
            self.itemsOnHand.push({
                DESCRIPTION: $("#PARTDESC").val(),
                PRODUCT_CODE: $("#ID_PARTS").val(),
                MIN: $("#INFOMIN").val(),
                MAX: $("#INFOMAX").val(),
                QTY: $("#INFOQTY").val(),
                COMMITTED_QTY: 0,
                PR_QTY: 0,
                STORE_DESC: $("#storeDescription").text(),
                STORE: $("#STORE_ID").val(),
                STATUS: $("#INFOSTATUS").val(),
                ALLOWUPDATE: allowUpdate,
                LOCATION: null,
                LOCATION2: null,
                LOCATION3: null,
                LOCATION4: null,
                PART_NUMBERS:'',
                cIndex: currCount
            });
        }
		self.saveInfo();
		window.location.href = ah.config.url.modalClose;
    };

    self.applyNewLocation = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var itemDetails = ah.ConvertFormToJSON($(ah.config.id.frmitemInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
       
        var idx = $("#CURRINDEX").val();
            
            //idx = parseInt(idx) + 1;
    
            self.itemsOnHand()[idx].LOCATION = $("#INFLOCATION").val();
            self.itemsOnHand()[idx].LOCATION2 = $("#INFLOCATION2").val();
            self.itemsOnHand()[idx].LOCATION3 = $("#INFLOCATION3").val();
            self.itemsOnHand()[idx].LOCATION4 = $("#INFLOCATION4").val();
         
            var storeId = self.itemsOnHand()[idx].STORE;
            var partId = self.itemsOnHand()[idx].PRODUCT_CODE;
            var location = $("#INFLOCATION").val();
            var location2 = $("#INFLOCATION2").val();
            var location3 = $("#INFLOCATION3").val();
            var location4 = $("#INFLOCATION4").val();
           
            
           

            var jDate = new Date();
            var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }

            var hourmin = time_format(jDate);

            strDateStart = strDateStart + ' ' + hourmin;
           
            postData = { STORE_ID: storeId, ID_PARTS: partId, LOCATION: location, LOCATION2: location2, LOCATION3: location3, LOCATION4: location4, UPDATEDBY: staffLogonSessions.STAFF_ID, UPDATEDDATE: strDateStart , STAFF_LOGON_SESSIONS: staffLogonSessions };
            //alert(JSON.stringify(postData));
            //return;
            ah.toggleSaveMode();
            $.post(self.getApi() + ah.config.api.updateItemLocation, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

       
        window.location.href = ah.config.url.modalClose;
    };

    self.select = function (itemsOnHand) {
        self.selected(itemsOnHand);
    };
    self.selected = ko.observable(self.itemsOnHand()[0]);


    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.createNew = function () {

        ah.toggleAddMode();

    };
   
    self.pageLoader = function () {
        var senderID = $(arguments[1].currentTarget).attr('id');
        var searchStr = $(ah.config.id.txtGlobalSearchOth).val().toString();
        var pageNum = parseInt($(ah.config.id.pageNum).val());
        var pageCount = parseInt($(ah.config.id.pageCount).val());
        if (isNaN(pageCount)) {
            pageCount = 1;
        }
        if (isNaN(pageNum)) {
            pageNum = 0;
        }
        switch (senderID) {

            case ah.config.tagId.nextPage: {
                pageNum = pageNum + 18;
                pageCount++;
                self.searchItemsResult(searchStr, pageNum);
                break;
            }
            case ah.config.tagId.priorPage: {

                if (pageNum === 0) {
                } else {
                    pageNum = pageNum - 18;
                    if (pageNum < 0) {
                        pageNum = 0;
                    }
                    pageCount = pageCount - 1;
                }

                self.searchItemsResult(searchStr, pageNum);
                break;
            }

            case ah.config.tagId.txtGlobalSearchOth: {
                pageCount = 1;
                pageNum = 0;
                self.searchItemsResult(searchStr, 0);
                break;
            }
        }

        $(ah.config.id.loadNumber).text(pageCount.toString());
        $(ah.config.id.pageCount).val(pageCount.toString());
        $(ah.config.id.pageNum).val(pageNum);

    };    

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };
    self.searchParts = function () {
        
        $("#ACTIONTRANS").val('NEW');
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData = '';
        var storeId = $("#STORENO").val();
        if (storeId === null || storeId === "") {
            alert('Please select Store to proceed.');
            return;
        }
        ah.toggleSearchMode();
        postData = { STORE: storeId ,SEARCH: "", SEARCH_FILTER: self.searchFilter, STAFF_LOGON_SESSIONS: staffLogonSessions };
       // ah.toggleSearchMode();
        $.post(self.getApi() + ah.config.api.seachParts, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = ah.config.url.modalSearhParts;

    };
    self.saveAllChanges = function () {

        $(ah.config.id.saveInfo).trigger('click');

    };
    self.postAllChanges = function () {
        $("#POSTID").val("POST");
        $(ah.config.id.saveInfo).trigger('click');

    };
    self.pageLoader = function () {
        var senderID = $(arguments[1].currentTarget).attr('id');

        var searchStr = $(ah.config.id.txtGlobalSearchOth).val().toString();

        switch (senderID) {


            case ah.config.tagId.txtGlobalSearchOth: {
                pageCount = 1;
                pageNum = 0;

                self.searchResultItemsParts(searchStr);
                break;
            }
        }



    };
	self.adjustItems = function (itemsOnHand) {
		//alert(idx);

        $("#ACTIONTRANS").val('MODIFY');
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

		self.selected(itemsOnHand);
		if (itemsOnHand.QTY === null || itemsOnHand.QTY === "") {
			itemsOnHand.QTY = '0';
		}
		if (itemsOnHand.MIN === null || itemsOnHand.MIN === "") {
			itemsOnHand.MIN = '0';
		}
		if (itemsOnHand.MAX === null || itemsOnHand.MAX === "") {
			itemsOnHand.MAX = '0';
		}
		
		$("#productDescription").text("(" + itemsOnHand.PRODUCT_CODE +") - "+itemsOnHand.DESCRIPTION);
		$("#storeDescription").text(itemsOnHand.STORE_DESC);
		$("#INFOQTY").val(itemsOnHand.QTY);
		$("#INFOMIN").val(itemsOnHand.MIN);
		$("#INFOMAX").val(itemsOnHand.MAX);
		$("#INFOSTATUS").val(itemsOnHand.STATUS);
		$("#STORE_ID").val(itemsOnHand.STORE);
		$("#ID_PARTS").val(itemsOnHand.PRODUCT_CODE);
		$("#QTY_ACTUAL").val(itemsOnHand.QTY);
		$("#MIN_ACTUAL").val(itemsOnHand.MIN);
		$("#MAX_ACTUAL").val(itemsOnHand.MAX);
		$("#STATUS_ACTUAL").val(itemsOnHand.STATUS);
		$("#CURRINDEX").val(itemsOnHand.cIndex);
      
        $("#REASON").val("");
        window.location.href = ah.config.url.modalInfo;
    };

    self.locationItems = function (itemsOnHand) {
        //alert(idx);

        $("#ACTIONTRANS").val('MODIFY');
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        self.selected(itemsOnHand);
       
        $("#productDescriptionItem").text("(" + itemsOnHand.PRODUCT_CODE + ") - " + itemsOnHand.DESCRIPTION);
        $("#storeDescriptionItem").text(itemsOnHand.STORE_DESC);
       
        $("#INFLOCATION").val(itemsOnHand.LOCATION);
        $("#INFLOCATION2").val(itemsOnHand.LOCATION2);
        $("#INFLOCATION3").val(itemsOnHand.LOCATION3);
        $("#INFLOCATION4").val(itemsOnHand.LOCATION4);
        $("#CURRINDEX").val(itemsOnHand.cIndex);
       
        window.location.href = ah.config.url.modalInfoItem;
    };

    self.bacthItems = function (itemsOnHand) {
        $(ah.config.cls.updSuccessModNotif).hide();
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData = { STORE: itemsOnHand.STORE, PRODUCT_CODE: itemsOnHand.PRODUCT_CODE, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.GetStockBatchByProdStore, postData).done(
            function (data) {
                self.modSOHBatchItems.removeAll();
                self.modSOH.BatchTotalItemCount(0);
                if (data.PRODUCT_STOCKBATCHS.length > 0) {
                    data.PRODUCT_STOCKBATCHS.forEach(function (batch) {
                        batch.STOCK_ONHAND = ko.observable(batch.STOCK_ONHAND);
                        if (batch.EXPIRY_DATE) { 
                            batch.ExpiryDisplay = moment(batch.EXPIRY_DATE, "YYYY-MM-DD").format("DD/MM/YYYY");
                            batch.ItemExpired = ko.observable(moment(batch.EXPIRY_DATE).isSameOrBefore(moment()));
                            if (batch.ItemExpired()) batch.Class = "fc-red";
                            else self.modSOH.BatchTotalItemCount(self.modSOH.BatchTotalItemCount() + batch.STOCK_ONHAND());
                        } else self.modSOH.BatchTotalItemCount(self.modSOH.BatchTotalItemCount() + batch.STOCK_ONHAND());
                        batch.Updated = ko.observable(false);
                    });
                    self.modSOHBatchItems(data.PRODUCT_STOCKBATCHS);
                }
                window.location.href = ah.config.url.modalBatchInfoItems;
                if (self.modSOH.BatchTotalItemCount() > 1) $(ah.config.id.dvRecal).show();
                else $(ah.config.id.dvRecal).hide();
            }
        ).fail(self.webApiCallbackStatus);
    };

    self.openBatchModal = function (itemsOnHand) {
        self.unsaveBatch.PRODUCT_CODE(itemsOnHand.PRODUCT_CODE);
        self.unsaveBatch.DESCRIPTION(itemsOnHand.DESCRIPTION);
        self.unsaveBatch.STORE(itemsOnHand.STORE);
        self.unsaveBatch.STORE_DESC(itemsOnHand.STORE_DESC);
        self.unsaveBatch.STATUS("RECEIVED");
        self.unsaveBatch.PARTTYPE(itemsOnHand.TYPE_DESC == "Parts");

        var supp = Suppliers;
        $.each(supp, function (item) {
            $(ah.config.id.ddSUPPLIER)
                .append($("<option></option>")
                    .attr("value", supp[item].DESCRIPTION)
                    .text(supp[item].DESCRIPTION));
        });
        var mod = Models;
        $.each(mod, function (item) {
            $(ah.config.id.ddMODELCODE)
                .append($("<option></option>")
                    .attr("value", mod[item].MODEL_ID)
                    .text(mod[item].DESCRIPTION));
        });

        window.location.href = ah.config.url.modalAddBachInfoItems;
    };

    self.addBatch = function () {
        if (!self.unsaveBatch.QTY()) {
            alert('Please enter a valid Quantity to proceed.');
            return;
        }
        if (!self.unsaveBatch.INVOICENO()) {
            alert('Please enter a valid Invoice No. to proceed.');
            return;
        }
        if (!self.unsaveBatch.BARCODE_ID()) {
            alert('Please enter a valid Serial/Batch to proceed.');
            return;
        }
        if (!self.unsaveBatch.EXPIRY_DATE()) {
            alert('Please enter a valid Expiry Date to proceed.');
            return;
        }
        if (self.unsaveBatch.PARTTYPE()) {
            if (!self.unsaveBatch.MODELCODE()) {
                alert('Please enter a valid Model to proceed.');
                return;
            }
        }
        if (!self.unsaveBatch.SUPPLIER()) {
            alert('Please enter a valid Supplier to proceed.');
            return;
        }
        self.unsaveBatch.QTY_RECEIVED(self.unsaveBatch.QTY());
        self.unsaveBatch.STOCK_ONHAND(self.unsaveBatch.QTY());
        console.log(self.unsaveBatch);


        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var BatchData = {
            BARCODE_ID: self.unsaveBatch.BARCODE_ID(),
            EXPIRY_DATE: self.unsaveBatch.EXPIRY_DATE(),
            INVOICENO: self.unsaveBatch.INVOICENO(),
            PRODUCT_CODE: self.unsaveBatch.PRODUCT_CODE(),
            QTY: self.unsaveBatch.QTY(),
            QTY_RECEIVED: self.unsaveBatch.QTY(),
            STOCK_ONHAND: self.unsaveBatch.QTY(),
            STORE: self.unsaveBatch.STORE(),
            MODELCODE: self.unsaveBatch.MODELCODE(),
            SUPPLIER: self.unsaveBatch.SUPPLIER(),
            STATUS: self.unsaveBatch.STATUS(),
            CREATED_BY: staffLogonSessions.STAFF_ID
        }
        $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);

        postData = { BatchDetails: BatchData, STAFF_LOGON_SESSIONS: staffLogonSessions};
        $.post(self.getApi() + ah.config.api.createProductBatch, postData).done(
            function (data) {
                self.batchModalNotif(ah.config.cls.notifySuccess);
                self.resetUnsaveBatch();
                self.searchNow();
            }
        ).fail(self.webApiCallbackStatus);
        window.location.href = ah.config.url.modalClose;
    };

    self.batchDataUpdated = function (batch) {
        batch.Updated(true);
    };

    self.updateBatch = function (batch) {
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData = { Batch: batch, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.updateProductBatch, postData).done(
            function (data) {
                batch.Updated(false);
                self.modSOH.BatchTotalItemCount(0);
                self.modSOHBatchItems().forEach(function (batch) {
                    if (!batch.ItemExpired()) self.modSOH.BatchTotalItemCount(self.modSOH.BatchTotalItemCount() + parseInt(batch.STOCK_ONHAND()));
                });
                self.batchModalNotif(ah.config.cls.updSuccessModNotif);
            }
        ).fail(self.webApiCallbackStatus);
    };

    self.batchModalNotif = function (cls) {
        $(cls).show();
        setTimeout(function () {
            $(cls).fadeOut(1000, function () { });
        }, 3000);
    };

    self.resetUnsaveBatch = function () {
        self.unsaveBatch.PRODUCT_CODE("");
        self.unsaveBatch.DESCRIPTION("");
        self.unsaveBatch.STORE("");
        self.unsaveBatch.STORE_DESC("");
        self.unsaveBatch.EXPIRY_DATE("");
        self.unsaveBatch.QTY("");
        self.unsaveBatch.QTY_RECEIVED("");
        self.unsaveBatch.STOCK_ONHAND("");
        self.unsaveBatch.INVOICENO("");
        self.unsaveBatch.BARCODE_ID("");
        self.unsaveBatch.STATUS("");
        self.unsaveBatch.SUPPLIER("");
        self.unsaveBatch.PARTTYPE("");
    };

    self.closeCreateBatchModal = function () {
        window.location.href = ah.config.url.modalClose;
        self.resetUnsaveBatch();
    };

    self.RecalculateBatchItems = function () {
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var item = self.modSOHBatchItems()[0];
        var postData = { STORE: item.STORE, PRODUCT_CODE: item.PRODUCT_CODE, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.RecalculateProductBatch, postData).done(
            function (data) {
                self.modSOHBatchItems.removeAll();
                self.modSOH.BatchTotalItemCount(0);
                if (data.PRODUCT_STOCKBATCHS.length > 0) {
                    data.PRODUCT_STOCKBATCHS.forEach(function (batch) {
                        batch.STOCK_ONHAND = ko.observable(batch.STOCK_ONHAND);
                        batch.ExpiryDisplay = moment(batch.EXPIRY_DATE, "YYYY-MM-DD").format("DD/MM/YYYY");
                        batch.ItemExpired = ko.observable(moment(batch.EXPIRY_DATE).isSameOrBefore(moment()));
                        if (batch.ItemExpired()) batch.Class = "fc-red";
                        else self.modSOH.BatchTotalItemCount(self.modSOH.BatchTotalItemCount() + batch.STOCK_ONHAND());
                        batch.Updated = ko.observable(false);
                    });
                    self.modSOHBatchItems(data.PRODUCT_STOCKBATCHS);
                }
            }
        ).fail(self.webApiCallbackStatus);
    };

    self.closeBatchModal = function () {
        window.location.href = ah.config.url.modalClose;
        self.searchNow();
    };

    self.validatePrivileges = function (privilegeId) {
        var groupPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));

        var groupPrivileges = groupPrivileges.filter(function (item) { return item.PRIVILEGES_ID === privilegeId });
        return groupPrivileges.length;
    };
    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            self.showDebtorDetails();
        }

    };

    self.showDetails = function (isNotUpdate) {

        if (isNotUpdate) {

            var storeDetails = sessionStorage.getItem(ah.config.skey.storeDetails);

            var jsetupDetails = JSON.parse(storeDetails);

            ah.ResetControls();
            ah.LoadJSON(jsetupDetails);
            $("#STORE_NO").val(jsetupDetails.STORE_ID);
        }

        ah.toggleDisplayMode();

    };
    self.sorting = function () {
        if (srProdArray.length > 0) {
            sortby = $("#SORTPAGE").val();
            self.searchResultItems(0, sortby);
        }

    };
    self.searchResult = function (jsonData) {


        var sr = srProdArray;

		//JSON.parse(jsonData.PRODUCT_STOCKBATCH);
        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {
                var transDate = new Date(sr[o].TRANS_DATE);
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].TRANS_ID, 'Trans.ID ' + sr[o].TRANS_ID, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;         Trans. Date: ' + transDate.toDateString().substring(4, 15), ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      Total Items ' + sr[o].NOITEMS);

                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.detailItem).bind('click', self.readtransID);
        ah.displaySearchResult();

    };
   
    self.searchResultItems = function (pageNum, sortby) {
     
        ah.toggleAddMode();   
       
        ah.toggledisplayItemInfo();
        var sr = srProdArray; 
        //self.itemsOnHand.removeAll();
        //if (pageNum == 0) {
        //    $("#PaginGloadNumber").text("1");
        //    $("#PaginGpageCount").val("1");
        //    $("#PaginGpageNum").val(0);
        //}
        //if (sortby == 'SORTCODEAC' && pageNum == 0) {
        //    sr.sort(function (a, b) {
        //        if (a["PRODUCT_CODE"] < b["PRODUCT_CODE"])
        //            return -1;
        //        if (a["PRODUCT_CODE"] > b["PRODUCT_CODE"])
        //            return 1;
        //        return 0;
        //    });
        //} else if (sortby == 'SORTCODECDC' && pageNum == 0) {
        //    sr.sort(function (a, b) {
        //        if (a["PRODUCT_CODE"] > b["PRODUCT_CODE"])
        //            return -1;
        //        if (a["PRODUCT_CODE"] < b["PRODUCT_CODE"])
        //            return 1;
        //        return 0;
        //    });
        //} else if (sortby == 'SORTDESCAC' && pageNum == 0) {
        //    sr.sort(function (a, b) {
        //        var textA = a.DESCRIPTION.toUpperCase();
        //        var textB = b.DESCRIPTION.toUpperCase();
        //        textA = textA.substring(0, 3);
        //        textB = textB.substring(0, 3);
        //        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        //    });
        //} else if (sortby == 'SORTDESCDC' && pageNum == 0) {
        //    sr.sort(function (a, b) {
        //        var textA = a.DESCRIPTION.toUpperCase();
        //        var textB = b.DESCRIPTION.toUpperCase();
        //        textA = textA.substring(0, 3);
        //        textB = textB.substring(0, 3);
        //        return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
        //    });
        //} else if (sortby == 'SORTONHANDAC' && pageNum == 0) {
        //    sr.sort(function (a, b) {

        //        if (a["QTY"] < b["QTY"])
        //            return -1;
        //        if (a["QTY"] > b["QTY"])
        //            return 1;
        //        return 0;
        //    });
        //} else if (sortby == 'SORTONHANDDC' && pageNum == 0) {
        //    sr.sort(function (a, b) {

        //        if (a["QTY"] > b["QTY"])
        //            return -1;
        //        if (a["QTY"] < b["QTY"])
        //            return 1;
        //        return 0;
        //    });
        //} else if (sortby == 'SORTCOMAC' && pageNum == 0) {
        //    sr.sort(function (a, b) {

        //        if (a["QTY"] < b["COMMITTED_QTY"])
        //            return -1;
        //        if (a["QTY"] > b["COMMITTED_QTY"])
        //            return 1;
        //        return 0;
        //    });
        //} else if (sortby == 'SORTCOMDC' && pageNum == 0) {
        //    sr.sort(function (a, b) {

        //        if (a["QTY"] > b["COMMITTED_QTY"])
        //            return -1;
        //        if (a["QTY"] < b["COMMITTED_QTY"])
        //            return 1;
        //        return 0;
        //    });
        //} else if (sortby == 'SORTCOMAC' && pageNum == 0) {
        //    sr.sort(function (a, b) {

        //        if (a["QTY"] < b["PR_QTY"])
        //            return -1;
        //        if (a["QTY"] > b["PR_QTY"])
        //            return 1;
        //        return 0;
        //    });
        //} else if (sortby == 'SORTCOMDC' && pageNum == 0) {
        //    sr.sort(function (a, b) {

        //        if (a["QTY"] > b["PR_QTY"])
        //            return -1;
        //        if (a["QTY"] < b["PR_QTY"])
        //            return 1;
        //        return 0;
        //    });
        //}
		
		var criteria = $("#sohCriteria").val();
		if (sr.length > 0) {
			if (criteria === 'SOHZERO') {
				sr = sr.filter(function (item) { return item.QTY <= 0 });
			} else if (criteria === 'SOHON') {
				sr = sr.filter(function (item) { return item.QTY > 0 });
			} else if (criteria === 'SOHCOMMITTED') {
				sr = sr.filter(function (item) { return item.COMMITTED_QTY > 0 });
			} else if (criteria === 'SOHPR') {
				sr = sr.filter(function (item) { return item.PR_QTY > 0 });
			}

			if (criteria === 'SOHINAT') {
				sr = sr.filter(function (item) { return item.STATUS === 'N' });
			} else {
				sr = sr.filter(function (item) { return item.STATUS === 'Y' });
			}
            self.itemsOnHand = sr;
            $(ah.config.id.itemsReceiveBatchCont).show();
            $(ah.config.id.searchResultBatchList).hide();

            if ($.fn.dataTable.isDataTable(ah.config.id.itemsReceiveBatch)) {
                $(ah.config.id.itemsReceiveBatch).DataTable().clear().destroy();
            }
            $(ah.config.id.itemsReceiveBatch).DataTable({
                data: sr,
                columns: [
                    { data: 'PRODUCT_CODE' },
                    { data: 'PART_NUMBERS' },
                    { data: 'DESCRIPTION' },
                    { data: 'MIN' },
                    { data: 'MAX' },
                    { data: 'QTY' },
                    { data: 'COMMITTED_QTY' },
                    { data: 'PR_QTY' },
                    { data: 'TYPE_DESC' },
                    { data: 'UOM_DESC' }, 
                    { data: 'STORE_DESC' },
                    {
                        data: null,
                        "orderable": false,
                        render: function (data, type, row) {
                            return "<a href='#' class=\"button-ext btnADJSOH\"><i class=\"fa fa-share-square-o\" title=\"Click to Adjust stock on hand\"></i></a>&nbsp;" +
                                "<a href='#' class=\"button-ext btnASSToBin\"><i class=\"fa fa-sitemap\" title=\"Click to Assign Stock to Bin\"></i></a>&nbsp;" +
                                "<a href='#' class=\"button-ext btnADJBatch\" style=\"margin-right: 4px;\"><i class=\"fa fa-dropbox\" title=\"Click to Adjust stock batch\"></i></a>" +
                                "<a href='#' class=\"button-ext btnADDBatch\"><i class=\"fa fa-file-text-o\" title=\"Click to Add batch\"></i></a>";
                        }
                    }
                ],
                "searching": false,
                "iDisplayLength": 15,
                "aLengthMenu": [15, 200, 6500, sr.length]
            });

            $('.btnADJSOH').on("click", function (e) {
                e.preventDefault();
                var pCode = $(this).closest("tr").find("td:eq(0)").text();
                var storeDesc = $(this).closest("tr").find("td:eq(10)").text();
                self.adjustItems(self.itemsOnHand.find(function (item) { return item.PRODUCT_CODE == pCode && item.STORE_DESC == storeDesc }));
            });

            $('.btnASSToBin').on("click", function (e) {
                e.preventDefault();
                var pCode = $(this).closest("tr").find("td:eq(0)").text();
                var storeDesc = $(this).closest("tr").find("td:eq(10)").text();
                self.locationItems(self.itemsOnHand.find(function (item) { return item.PRODUCT_CODE == pCode && item.STORE_DESC == storeDesc }));
            });

            $('.btnADJBatch').on("click", function (e) {
                e.preventDefault();
                var pCode = $(this).closest("tr").find("td:eq(0)").text();
                var storeDesc = $(this).closest("tr").find("td:eq(10)").text();
                self.bacthItems(self.itemsOnHand.find(function (item) { return item.PRODUCT_CODE == pCode && item.STORE_DESC == storeDesc }));
            });

            $('.btnADDBatch').on("click", function (e) {
                e.preventDefault();
                var pCode = $(this).closest("tr").find("td:eq(0)").text();
                var storeDesc = $(this).closest("tr").find("td:eq(10)").text();
                self.openBatchModal(self.itemsOnHand.find(function (item) { return item.PRODUCT_CODE == pCode && item.STORE_DESC == storeDesc }));
            });
   //         var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
			//gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "13" });
   //         $(ah.config.cls.contentHeader).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));
			//$(ah.config.cls.contentHeader).show();
			//var pageNumto = pageNum + 200;
			//var pagenum = Math.ceil(sr.length / 200);
			////sr = sr.filter(function (item) { return item.ROWNUMID >= pageNum && item.ROWNUMID <= pageNumto });
   //         var index = 200000;
   //         sr = sr.slice(pageNum, index);

			//if (pagenum >= 2) {

			//	$(ah.config.cls.pagingDiv).show();
			//} else {
			//	$(ah.config.cls.pagingDiv).hide();
			//}

   //         $("#PaginGtotNumber").text(pagenum.toString());
   //         var countsr = 0
			//for (var o in sr) {   
   //             countsr++;
   //             var onHand = 0;
   //             onhand = sr[o].QTY - sr[o].COMMITTED_QTY;
   //             if (onhand < 0) {
   //                 onhand = 0;
   //             }
   //             var allowUpdate = 'Y';
			//	if (gPrivileges.length <= 0) {
			//		//$(".adjustId").each(function (o) { $(this.remove()) });
   //                 allowUpdate = 'N';
   //             }
   //             if (o > 0) {
   //                 currCount++;
   //             }
   //             self.itemsOnHand.push({
   //                 DESCRIPTION: sr[o].DESCRIPTION,
   //                 PRODUCT_CODE: sr[o].PRODUCT_CODE,  
   //                 MIN: sr[o].MIN,
   //                 MAX: sr[o].MAX,                   
   //                 QTY: onhand,
   //                 COMMITTED_QTY: sr[o].COMMITTED_QTY,
   //                 PR_QTY: sr[o].PR_QTY,
			//		STORE_DESC: sr[o].STORE_DESC,
			//		STORE: sr[o].STORE,
   //                 STATUS: sr[o].STATUS,
   //                 ALLOWUPDATE: allowUpdate,
   //                 LOCATION: sr[o].LOCATION,
   //                 LOCATION2: sr[o].LOCATION2,
   //                 LOCATION3: sr[o].LOCATION3,
   //                 LOCATION4: sr[o].LOCATION4,
   //                 PART_NUMBERS: sr[o].PART_NUMBERS,
   //                 TYPE_DESC: sr[o].TYPE_DESC,
   //                 UOM_DESC: sr[o].UOM_DESC,
			//		cIndex: o
   //             });

   //             if (countsr === 200) {
                   
   //                 break;
   //             }
				
   //         }
           
			
        }
        if (self.validatePrivileges('14') > 0) {
            $(ah.config.cls.liAddNew).show();

        }
    };
    self.searchResultItemsParts = function (LovCategory) {

        ah.toggleAddMode();
        //ah.toggledisplayItemInfo();
        var sr = srDataArray;
        //alert(JSON.stringify(sr));
        if (LovCategory !== null && LovCategory !== "") {
            var arr = sr;
            var keywords = LovCategory.toUpperCase();
            var filtered = [];

            for (var i = 0; i < sr.length; i++) {
                for (var j = 0; j < keywords.length; j++) {
                    if ((sr[i].DESCRIPTION.toUpperCase()).indexOf(keywords) > -1 || (sr[i].ID_PARTS.toUpperCase()).indexOf(keywords) > -1) {
                        filtered.push(sr[i]);
                        break;
                    }
                }
            }
            sr = filtered;
        }

        $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).html('');

        if (sr.length > 0) {

            var htmlstr = '';

            for (var o in sr) {
                var assetDescription = sr[o].DESCRIPTION;
                if (assetDescription === null || assetDescription === "" || assetDescription === "null") {

                    sr[o].DESCRIPTION = "Undefined"
                    assetDescription = "Undefined"
                }
                if (assetDescription.length > 70) {
                    assetDescription = assetDescription.substring(0, 70) + '...';
                }
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplateList, JSON.stringify(sr[o]), sr[o].ID_PARTS, assetDescription, '');
                htmlstr += '</li>';

            }

            $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).append(htmlstr);
        }
        $(ah.config.cls.detailItem).bind('click', self.populatepartInfo);
        if (self.validatePrivileges('14') > 0) {
            $(ah.config.cls.liAddNew).show();

        }
       
    };
    self.populatepartInfo = function () {
        ah.toggledisplayItemInfo();
        
        var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfoId).toString());
        
        var storeId = $("#STORENO").val();
        var x = document.getElementById("STORENO").selectedIndex;
        var y = document.getElementById("STORENO").options;
        $("#productDescription").text("(" + infoID.ID_PARTS + ") - " + infoID.DESCRIPTION);
        $("#storeDescription").text(y[x].text);
       
        $("#PARTDESC").val(infoID.DESCRIPTION);
        $("#INFOMIN").val(0);
        $("#INFOQTY").val(0);
        $("#INFOMIN").val(0);
        $("#INFOMAX").val(0);
        $("#INFOSTATUS").val('Y');
        $("#STORE_ID").val(storeId);
        $("#ID_PARTS").val(infoID.ID_PARTS );
        $("#QTY_ACTUAL").val(0);
        $("#MIN_ACTUAL").val(0);
        $("#MAX_ACTUAL").val(0);
        $("#STATUS_ACTUAL").val('Y');
        $("#REASON").val("");
        
  
    };
    self.openModal = function () {
      
        window.location.href = ah.config.url.modalInfo;
        alert(window.location.href);
    };
	self.pagingLoader = function () {


		var senderID = $(arguments[1].currentTarget).attr('id');

	

		var pageNum = parseInt($(ah.config.id.PaginGpageNum).val());
		var pageCount = parseInt($(ah.config.id.PaginGpageCount).val());

	
        sr = srProdArray;

	
        sort = $("#SORTPAGE").val();
		var lastPage = Math.ceil(sr.length / 200);

		if (isNaN(pageCount)) {
			pageCount = 1;
		}
		if (isNaN(pageNum)) {
			pageNum = 0;
		}
		
		switch (senderID) {

			case ah.config.tagId.PaginGnextPage: {

				if (pageCount < lastPage) {
					pageNum = pageNum + 200;
					pageCount++;

                    self.searchResultItems(pageNum, sort);
				}
				break;
			}
			case ah.config.tagId.PaginGpriorPage: {

				if (pageNum === 0) {
				} else {
					pageNum = pageNum - 200;
					if (pageNum < 0) {
						pageNum = 0;
					}
					pageCount = pageCount - 1;
				}

                self.searchResultItems(pageNum, sort);
				break;
			}
	
		}

		$("#PaginGloadNumber").text(pageCount.toString());
		$("#PaginGpageCount").val(pageCount.toString());
		$("#PaginGpageNum").val(pageNum);

	};
    self.searchBatchResult = function (LovCategory, pageLoad, jsonData) {

        var sr = jsonData.PRODUCT_STOCKBATCH;

        //alert(JSON.stringify(sr));

        $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).html('');
        if (sr.length > 0) {
            $("#page1").show();
            $("#page2").show();
            $("#nextPage").show();
            $("#priorPage").show();
            var totalrecords = sr.length;
            var pagenum = Math.round(totalrecords / 18);
            $(ah.config.id.totNumber).text(pagenum.toString());
            if (pagenum <= 1) {
                $("#page1").hide();
                $("#page2").hide();
                $("#nextPage").hide();
                $("#priorPage").hide();

            }
            var htmlstr = '';
            var o = pageLoad;
            $("#PRODDESC").text('   Item Name :'+ sr[o].PRODUCT_DESC);

          
            for (var i = 0; i < 18; i++) {
                var expiryDate = ah.formatJSONDateToString(sr[o].EXPIRY_DATE);
                htmlstr += '<li>';
                htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
               
                htmlstr += ah.formatString(ah.config.html.searchExistRowHeaderTemplate, JSON.stringify(sr[o]), 'Expiry Date:' + expiryDate, "Total QTY:" + sr[o].QTY, "");
               


                htmlstr += '</span>';
                htmlstr += '</li>';
                o++
                if (totalrecords === o) {
                    break;
                }
            }

            //alert(htmlstr);
            $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        //$(ah.config.cls.productDetailItem).bind('click', self.addProduct);
        ah.toggleDisplayMode();
        //ah.toggleAddItems();
    };
  
    self.readtransID = function () {

        var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { TRANS_ID: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readTransIDList, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();
        var storeId = $("#STORENO").val();
        postData = { CATEGORY: "", STATUS: "", SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), STORE: storeId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

     
    };

    self.saveInfo = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var itemDetails = ah.ConvertFormToJSON($(ah.config.id.frmitemInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
		itemDetails.QTY_ADJUST = $("#INFOQTY").val();
		itemDetails.MIN_ADJUST = $("#INFOMIN").val();
		itemDetails.MAX_ADJUST = $("#INFOMAX").val();
		itemDetails.STATUS_ADJUST = $("#INFOSTATUS").val();
		
      
        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
		}

        var hourmin = time_format(jDate);
		
		strDateStart = strDateStart + ' ' + hourmin;
		itemDetails.CREATED_BY = staffLogonSessions.STAFF_ID;
		itemDetails.CREATED_DATE = strDateStart;
  		postData = {STOCK_ONHAND_ADJUSTMENT: itemDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
			//alert(JSON.stringify(postData));
			//return;
        ah.toggleSaveMode();
		$.post(self.getApi() + ah.config.api.updateSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
  

    };

    self.webApiCallbackStatus = function (jsonData) {
      //  alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            //window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.PARTS_LIST || jsonData.STOCK_ONHAND_ADJUSTMENT || jsonData.PRODUCT_STOCKBATCH || jsonData.PRODUCT_SUPPLIERS || jsonData.PRODUCT_STOCKONHAND || jsonData.STORES || jsonData.STAFF || jsonData.SUCCESS) {
            
            if (ah.CurrentMode == ah.config.mode.save) {
                sessionStorage.setItem(ah.config.skey.storeDetails, JSON.stringify(jsonData.STORES));
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();
				ah.toggleDisplayMode();
                if (self.validatePrivileges('14') > 0) {
                    $(ah.config.cls.liAddNew).show();

                }
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                
                $(ah.config.id.loadNumber).text("1");
                $(ah.config.id.pageCount).val("1");
                $(ah.config.id.pageNum).val(0);
                self.searchBatchResult("", 0, jsonData);
                //self.searchResultItems(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
              
                if (jsonData.PRODUCT_STOCKONHAND) {
                    srProdArray = jsonData.PRODUCT_STOCKONHAND;
                    $("#LASTLOADPAGE").val(0);
					//sessionStorage.setItem(ah.config.skey.productStockBatch, JSON.stringify(jsonData.PRODUCT_STOCKONHAND));
					$("#PaginGloadNumber").text("1");
					$("#PaginGpageCount").val("1");
                    $("#PaginGpageNum").val(0);
                    var sortby = $("#SORTPAGE").val();
                    self.searchResultItems(0, sortby);
                   
                } if (jsonData.PARTS_LIST) {
                    srDataArray = jsonData.PARTS_LIST;
                    self.searchResultItemsParts("");

                } else {
                   
                    
                }
               
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {
                Suppliers = jsonData.SUPPLIERS;
                Models = jsonData.MODELS;
                var store = jsonData.STORES;
                $.each(store, function (item) {
                    $(ah.config.id.storeCURR)
                        .append($("<option></option>")
                        .attr("value", store[item].STORE_ID)
                        .text(store[item].DESCRIPTION));
                });
                var stockZero = ah.getParameterValueByName(ah.config.fld.menuZero);
                if (stockZero == "zero") {
                    $("#sohCriteria").val("SOHZERO");
                    self.searchNow(); 
                }
            }
        }
        else {

            alert(systemText.errorTwo);
            //window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};