﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            divProgress: '.divProgressScreen',
            toolBoxRight: '.toolbox-right',
            detailItem: '.detailItem',
            liPrint: '.liPrint'
        },
        tag: {

            textArea: 'textarea',
            select: 'select',
            tbody: 'tbody',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            search: 2,
            searchResult: 3,
            print: 8
        },
        tagId: {

        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            searchResultTable: '#searchResultTable',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            printSummary: '#printSummary',
            printSearchResultList: '#printSearchResultList',
            loadingBackground: '#loadingBackground'
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            datainfoId: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',


        },
        url: {
            modalFilter: '#openModalFilter',
            modalClose: '#'
        },
        api: {
            searchAll: '/TemplateReport/ExampleSearchAsset',//identify the api controller you want to get you data,
            readClientInfo: '/Setup/ReadClientInfo'
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };
    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liPrint + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };
    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;
        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();
        $(thisApp.config.cls.contentSearch).show();

    };
    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };
    thisApp.displaySearchResult = function () {
        thisApp.CurrentMode = thisApp.config.mode.searchResult;
        $(thisApp.config.cls.contentSearch).show();
    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var viewTemplateViewModel = function (systemText) {
    var self = this;
    var ah = new appHelper(systemText);
    self.srDataArray = ko.observableArray([]);

    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.initializeLayout();
        var today = moment(new Date()).format("DD/MM/YYYY");

        $('#requestFromDate').val(today);
        $('#requestStartDate').val(today);

        self.showLoadingBackground();

        $.when(self.getPageData()).done(function () {
            $(ah.config.id.loadingBackground).removeClass('initializing').addClass('done');
            $('.dark-bg, ' + ah.config.id.loadingBackground).css({
                'top': '90px',
                'z-index': '0'
            });
        });
    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.clientDetails = {
        CLIENTNAME: ko.observable(),
        CLIENT_LOGO: ko.observable(),
        COMPANY_LOGO: ko.observable(),
        HEADING: ko.observable(),
        HEADING2: ko.observable(),
        HEADING3: ko.observable(),
        HEADING_OTH: ko.observable(),
        HEADING_OTH2: ko.observable(),
        HEADING_OTH3: ko.observable(),
        CLIENTADDRESS: ko.observable(),
        CLIENTCONTACT: ko.observable()
    };
    self.searchFilter = {
        fromDate: ko.observable(''),
        toDate: ko.observable(''),


    };
    
    self.getPageData = function () {

    };
    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };
    
    self.showLoadingBackground = function () {
        var loadingBackground = $(ah.config.id.loadingBackground);
        loadingBackground.addClass('initializing').removeClass('done');
        var offset = loadingBackground.offset();
        var height = loadingBackground.height();
        var centerY = (offset.top + height / 2) - 120;
        $('.sk-circle').css({
            'margin-top': centerY + 'px',
            'display': 'block'
        });
    };
    self.searchFilterNow = function () {
        window.location.href = ah.config.url.modalClose;
        self.searchNow();
    }
    self.showModalFilter = function () {
        window.location.href = ah.config.url.modalFilter;
    };
    self.clearFilter = function () {
        $('#requestFromDate').val('');
        $('#requestStartDate').val('');
        self.searchFilter.fromDate = '';
        self.searchFilter.toDate = '';
    };
    self.printSummary = function () {
        ah.CurrentMode = ah.config.mode.print;
        document.getElementById("printSummary").classList.add('printSummary');

        if (self.clientDetails.CLIENTNAME() != null) {
            self.print();

        } else {
            var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var postData;

            var staffInfo = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
            postData = { CLIENTID: staffInfo.CLIENT_CODE, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.readClientInfo, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
    };
    self.print = function () {
        setTimeout(function () {
            window.print();

        }, 2000);
    };
    self.dateClick = function () {
        $("#dateClickAction").val("ACTIVE");
    };
    self.searchResult = function () {
        var table;

        if ($.fn.dataTable.isDataTable(ah.config.id.searchResultTable)) {
            $(ah.config.id.searchResultTable).DataTable().clear().destroy();
        }

        table = $(ah.config.id.searchResultTable).DataTable({
            data: self.srDataArray,
            columns: [
                { data: 'ASSET_NO' },
                { data: 'DESCRIPTION' }
            ],
            "searching": false,
            "iDisplayLength": 200,
            "aLengthMenu": [15, 200, 500, self.srDataArray.length],
        });
    };
    self.searchNow = function () {
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;
        ah.toggleSearchMode();

        var fromDate = $('#requestFromDate').val();

        fromDate = fromDate.split('/');
        if (fromDate.length == 3) {
            self.searchFilter.fromDate(fromDate[2] + "/" + fromDate[1] + "/" + fromDate[0]);
        }
        var toDate = $('#requestStartDate').val().split('/');
        if (toDate.length == 3) {
            self.searchFilter.toDate(toDate[2] + "/" + toDate[1] + "/" + toDate[0] + "  23:00:00.000");
        }


        self.showLoadingBackground();
        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), SEARCH_FILTER: self.searchFilter,  STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.when($.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus)).done(function () {
            $(ah.config.id.loadingBackground).removeClass('initializing').addClass('done');
        });
    };
    self.webApiCallbackStatus = function (jsonData) {

        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));


        }
        else if (jsonData.AM_CLIENT_INFO || jsonData.AM_ASSETLIST) {
            if (ah.CurrentMode == ah.config.mode.search) {
                self.srDataArray = jsonData.AM_ASSETLIST; //assign you data from api
                self.searchResult();
                
            }
            else if (ah.CurrentMode == ah.config.mode.print) {

                var clientInfo = jsonData.AM_CLIENT_INFO;

                self.clientDetails.CLIENTNAME(clientInfo.DESCRIPTION);
                self.clientDetails.CLIENT_LOGO(clientInfo.CLIENT_LOGO);
                self.clientDetails.COMPANY_LOGO(clientInfo.COMPANY_LOGO);
                self.clientDetails.HEADING(clientInfo.HEADING);
                self.clientDetails.HEADING2(clientInfo.HEADING2);
                self.clientDetails.HEADING3(clientInfo.HEADING3);
                self.clientDetails.HEADING_OTH(clientInfo.HEADING_OTH);
                self.clientDetails.HEADING_OTH2(clientInfo.HEADING_OTH2);
                self.clientDetails.HEADING_OTH3(clientInfo.HEADING_OTH3);
                self.clientDetails.CLIENTADDRESS(clientInfo.ADDRESS + ', ' + clientInfo.CITY + ', ' + clientInfo.ZIPCODE + ', ' + clientInfo.STATE);
                self.clientDetails.CLIENTCONTACT('Tel.No: ' + clientInfo.CONTACT_NO1 + ' Fax No:' + clientInfo.CONTACT_NO2);

                self.print();
            }

        }
        else {

            alert(systemText.errorTwo);

        }

    };

    self.initialize();
};