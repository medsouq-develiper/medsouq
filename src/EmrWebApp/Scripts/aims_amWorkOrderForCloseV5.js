﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            detailItem: '.detailItem',
            info1: '.info1',
            info2: '.info2',
            info3: '.info3',
            info4: '.info4',
            info5: '.info5',
            info6: '.info6',
            info7: '.info7',
            liassetInfo: '.liassetInfo',
            searchAsset: '.searchAsset',
            otherButton: '.otherButton',
            pagingDiv: '.pagingDiv',
            detailParts: '.detailParts',
            printDetails: '.printDetails',
            liPrint: '.liPrint',
            liSearch: '.liSearch'

        },
        fld: {
            assetNoInfo: 'ASSETNO',
            assetDescInfo: 'ASSETDESC',
            reqNo: 'REQNO',
            reqWOAction: 'REQWOAC',
            newWorkOrder: 'asdfxhsadf',
            fromDashBoard: 'asdfxhsadfsdfx'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            tbody: 'tbody',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul'
        },
        tagId: {
            btnContract: 'btnContract',
            btnLCycle: 'btnLCycle',
            btnHOperation: 'btnHOperation',
            btnANotes: 'btnANotes',
            btnSComponent: 'btnSComponent',
            btnAPersonnel: 'btnAPersonnel',
            btnCagetory: 'btnCagetory',
            btnManufacturer: 'btnManufacturer',
            btnSupplier: 'btnSupplier',
            btnBuilding: 'btnBuilding',
            btnCostCenter: 'btnCostCenter',
            btnLogHistory: 'btnLogHistory',
            btnpdf: 'btnpdf',
            btnResponsibleCenter: 'btnResponsibleCenter',
            btnLocation: 'btnLocation',
            txtGlobalSearchOth: 'txtGlobalSearchOth',
            priorPage: 'priorPage',
            nextPage: 'nextPage',
            PaginGnextPage: 'PaginGnextPage',
            PaginGpriorPage: 'PaginGpriorPage'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            searchResult1: '#searchResult1',
            searchResult2: '#searchResult2',
            frmInfo: '#frmInfo',
            clinicdoctorID: '#CLINIC_DOCTOR_ID',
            clinicID: '#CLINIC_ID',
            statusLogList: '#statusLogList',
            staffID: '#STAFF_ID',
            serviceID: '#SERVICE_ID',
            assetInfobtn: '#assetInfobtn',
            successMessage: '#successMessage',
            servDept: '#SERV_DEPT',
            woPriority: '#JOB_URGENCY',
            woProblemType: '#REQ_TYPE',
            woSpecialty: '#SPECIALTY',
            txtGlobalSearchOth: '#txtGlobalSearchOth',
            totNumber: '#totNumber',
            loadNumber: '#loadNumber',
            pageCount: '#pageCount',
            pageNum: '#pageNum',
            staffAssignTo: '#ASSIGN_TO',
            searchStatus: '#SEARCH_STATUS',
            PaginGpageNum: '#PaginGpageNum',
            PaginGpageCount: '#PaginGpageCount',
            jobType: '#JOB_TYPE',
            searchHelpDeskResult: '#searchHelpDeskResult',
            modifyButton: '#modifyButton',
            noteHistory: '#noteHistory',
            noteEntry: '#noteEntry',
            woReferenceNotes: '#WO_REFERENCE_NOTESREF',
            addButtonIcon: '#addButtonIcon',
            assetNOTES: '#ERECORD_GENERAL_NOTES',
            woStatus: '#WO_STATUS',
            newNotes: '#newNotes',
            supportingDocs: '#supportingDocs',
            searchTicket: '#searchTicket',
            searchAsset: '#searchAsset',
            searchResultListParts: '#searchResultListParts',
            closeDate: '#CLOSED_DATE',
            lblClosedDate: '#lblClosedDate',
            printSummary: '#printSummary',
            printBulk: '#printBulk',

            filterAssignTo: '#filterAssignTo',
            filterJobType: '#filterJobType'
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID',
            closedDate: 'CLOSED_DATE'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'searchResult',
            woDetails: 'AM_WORK_ORDERS',
            assetItems: 'AM_ASSET_DETAILS',
            assetList: 'AM_ASSET_DETAILS',
            groupPrivileges: 'GROUP_PRIVILEGES',
        },
        url: {
            homeIndex: '/Home/Index',
            modalAddItems: '#openModal',
            aimsInformation: '/aims/assetInfo',
            modalAddItemsInfo: '#openModalInfo',
            modalClose: '#',
            aimWorkOrderLabour: '/aims/amWorkOrderLabour',
            aimsHomepage: '/Menuapps/aimsIndex',
            aimsWOSearch: '/aims/amWorkOrderForClose',
            modalFilter: '#openModalFilter',
            modalhdr: '#openModalHelpDesk',
            openModalInHouse: '#openModalInHouse',
            openModalPrintPage: '#openModalPrintPage'
        },
        lfor: {
            closedDate: 'CLOSED_DATE'
        },
        api: {


            readSetup: '/AIMS/ReadWO',
            searchAll: '/AIMS/SearchWO',
            searchAssets: '/AIMS/SearchAssets',
            createSetup: '/AIMS/CreateNewWO',
            updateSetup: '/AIMS/UpdateWO',
            getListLOV: '/Search/SearchListOfValueWO',
            readAssetNo: '/AIMS/ReadAssetInfo',
            searchContract: '/AIMS/SearchAssetContractService',
            searchStaffList: '/Staff/SearchStaffLookUp',
            readWSInspection: '/AIMS/ReadWSInfo',
            searchHelpDeskNew: '/Search/SearchHelpDeskNew',
            searchAssetNotes: '/AIMS/SearchAssetNotes',
            searchWONotes: '/AIMS/SearchWONotes',
            searchwoMaterial: '/Inventory/SearchWOPartsInc',
            createDocs: '/AIMS/CreateAssetWODocs',
            searchWOStatus: '/AIMS/SearchWOStatus',
            readAssetInfoV: '/AIMS/RedAssetInfoV',
            serachWoList: '/Reports/SearchWODetails'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right no-print'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplateP: '<span class="search-row-label fc-light-grey-2">{0}: <span class="search-row-data">{1}</span></span>',
            searchRowTemplate: '<span class="search-row-label fc-light-grey-2">{0}: <span class="search-row-data">{1}</span></span>',
            searchRowTemplateC: '<span class="search-row-label fc-light-grey-2">{0}: <span class=" fc-green">{1}</span></span>',
            searchRowTemplateSpareParts: "<span class='search-row - label spareParts  detailParts' data-infoID='{0}'> <span>{1}</span></span>",
            searchRowHeaderDOCTemplateList: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            searchLOVRowHeaderTemplate: "<a href='#openModalInfo' class='fs-medium-normal productDetailItem fc-greendark' data-items='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} <span>{3}</span></a>",
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;
    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.pagingDiv + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess
            + ',' + thisApp.config.cls.liassetInfo + ',' + thisApp.config.cls.searchAsset + ',' + thisApp.config.cls.otherButton + ',' + thisApp.config.cls.liPrint).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;
        document.getElementById("JOB_DUE_DATE").disabled = true;
        document.getElementById("JOB_DURATION").disabled = true;
        document.getElementById("ASSET_NO").disabled = true;


    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.searchAsset).show();

        $(
            thisApp.config.cls.pagingDiv + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.otherButton + ',' + thisApp.config.id.closeDate + ',' + thisApp.config.id.lblClosedDate + ',' + thisApp.config.cls.liPrint
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("JOB_DUE_DATE").disabled = false;
        document.getElementById("JOB_DURATION").disabled = false;
        $("#assetInfoDet").hide();
        document.getElementById("ASSET_NO").disabled = true;
        document.getElementById("REQ_ID").disabled = true;
        $("#REQ_ID").hide();
        $("#REQ_ID").next("br").remove();
        $("label[for='REQ_ID']").hide();
    };

    thisApp.toggleAddItems = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;


        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("JOB_DUE_DATE").disabled = false;
        document.getElementById("JOB_DURATION").disabled = false;
        document.getElementById("ASSET_NO").disabled = true;

    };
    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();
        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);
        document.getElementById("ASSET_NO").disabled = true;
    };
    thisApp.toggleSearchItems = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        //$(thisApp.config.cls.statusText).text(systemText.searchModeStatuxText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();
        document.getElementById("ASSET_NO").disabled = true;
    };
    thisApp.formatJSONDateTimeToString = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "";
        if (dateToFormat.length < 16) return "";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0] + ' ' + strTime[1].substring(0, 5);


        return dateToFormat;
    };
    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
    };

    thisApp.togglehideOthInfo = function () {
        $(thisApp.config.cls.info7 + ',' + thisApp.config.cls.info6 + ',' + thisApp.config.cls.info1 + ',' + thisApp.config.cls.info2 + ',' + thisApp.config.cls.info3 + ',' + thisApp.config.cls.info4 + ',' + thisApp.config.cls.info5).hide();
    };
    thisApp.toggleShowInfo1 = function () {
        $(thisApp.config.cls.info1).show();
    };
    thisApp.toggleShowInfo2 = function () {
        $(thisApp.config.cls.info2).show();
    };
    thisApp.toggleShowInfo3 = function () {
        $(thisApp.config.cls.info3).show();
    };
    thisApp.toggleShowInfo4 = function () {
        $(thisApp.config.cls.info4).show();
    };
    thisApp.toggleShowInfo5 = function () {
        $(thisApp.config.cls.info5).show();
    };
    thisApp.toggleShowInfo6 = function () {
        $(thisApp.config.cls.info6).show();
    };
    thisApp.toggleShowInfo7 = function () {
        $(thisApp.config.cls.info7).show();
    };
    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);
        $(thisApp.config.cls.liApiStatus).hide();

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.otherButton + ',' + thisApp.config.cls.liModify).show();
        $(thisApp.config.cls.pagingDiv + ',' +  thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);
        document.getElementById("JOB_DUE_DATE").disabled = true;
        document.getElementById("JOB_DURATION").readOnly = true;
        document.getElementById("ASSET_NO").readOnly = true;
        document.getElementById("REQ_ID").readOnly = true;
        //document.getElementById("CLOSED_DATE").readOnly = false;
        document.getElementById("WO_STATUS").disabled = false;

    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();
        document.getElementById("ASSET_NO").disabled = true;

    };
   

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.otherButton).show();

        $(
            thisApp.config.cls.pagingDiv + ',' + thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.searchAsset + ',' + thisApp.config.cls.liPrint
        ).hide();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.otherButton + ',' + thisApp.config.id.closeDate + ',' + thisApp.config.id.lblClosedDate
        ).show();

        $(
            thisApp.config.cls.pagingDiv + ',' + thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.searchAsset + ',' + thisApp.config.cls.liPrint
        ).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);
        document.getElementById("JOB_DUE_DATE").disabled = true;
        document.getElementById("JOB_DURATION").disabled = true;
        document.getElementById("ASSET_NO").disabled = true;

    };

    thisApp.toggleDisplayModeAsset = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        //$(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        //$(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.searchAsset).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.liApiStatus).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("ASSET_NO").disabled = true;

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {
        $(form).find(":disabled").removeAttr("disabled");
        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };


    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };
    thisApp.formatJSONDateToString = function () {

        var dateToFormat = arguments[0];
        var strDate;

        if (dateToFormat === null) return "";
        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };

    thisApp.formatJSONTimeToString = function () {

        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "";
        if (dateToFormat === "") return "";
        if (dateToFormat.length < 16) return "";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        var timeToFormat = strTime[1].substring(0, 5);


        return timeToFormat;
    };
    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');


            if (isDateTime) {
                val = thisApp.formatJSONDateTimeToString(val);
            }

            if (isDate) {
                val = thisApp.formatJSONDateToString(val);
            }

            var $el = $('#' + name), type = $el.attr('type'), isTime = $el.attr('data-istime');
            if (isTime) {
                if (val === null || val === "") {
                    val = "00:00";
                } else {
                    val = thisApp.formatJSONTimeToString(val);
                }

            }

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var woInfoViewModel = function (systemText) {

    var self = this;
    self.partsWLList = ko.observableArray("");
    self.partsWOList = ko.observableArray("");
    var resultHtml = "";
    var srDataArray = ko.observableArray([]);
    var clientInfo = ko.observableArray([]);
    var assetIdList = "";
    var woIdList = "";
    var sparePartsCount = 0;
    self.processInfo = {
        filetype: ko.observable(''),
        statusBy: ko.observable(''),
        StatusDate: ko.observable(''),
        category: ko.observable(''),
        refid: ko.observable(''),
        filename: ko.observable(''),
        process: ko.observable('')
    };
    self.searchKeyword = ko.observable('');
    self.searchAssetFilter = {
        status: ko.observable('I'),
        staffId: ko.observable(''),
        assetNo: ko.observable('')
    };
    self.searchFilter = {
        fromDate: ko.observable(''),
        toDate: ko.observable(''),
        status: ko.observable('FC'),
        problemType: ko.observable(''),
        priority: ko.observable(''),
        jobType: ko.observable(''),
        userConstCenter: ko.observable(''),
        woIdList: ko.observable(''),
        assignTo: ko.observable(''),
        assignType: ko.observable('')

    }

    self.clearFilter = function () {
        self.searchFilter.toDate('');
        self.searchFilter.fromDate('');
        self.searchFilter.status = ('FC');
        self.searchFilter.problemType = "";
        self.searchFilter.assignType = "";
        self.searchFilter.jobType = "";
        self.searchFilter.assignTo = "";
        self.searchFilter.priority = "";
        $('#requestFromDate').val('');
        $('#requestStartDate').val('');
        $("#filterStatus").val('FC');
        $("#filterProblemType").val(null);
        $("#filterJobType").val(null);
        $("#filterAssignType").val(null);
        $("#filterAssignTo").val(null);
        $("#filterPriority").val(null);


        //window.location.href = ah.config.url.modalClose;
    }
    self.searchFilterNow = function () {
        window.location.href = ah.config.url.modalClose;
        self.searchNow();
    }
    self.dateClick = function () {
        $("#dateClickAction").val("ACTIVE");
    };
    self.searchKeyUp = function (d, e) {
        if (e.keyCode == 13) {
            self.searchNow();
        }
    }

    self.woDetails = {
        WO_STATUS: ko.observable(),
        SPECIALTY: ko.observable(),
        SERV_DEPT: ko.observable(),
        REQ_TYPE: ko.observable(),
        REQ_REMARKS: ko.observable(),
        REQ_NO: ko.observable(0),
        REQ_DATE: ko.observable(),
        REQ_BY: ko.observable(),
        REQUESTER_CONTACT_NO: ko.observable(),
        PROBLEM: ko.observable(),
        JOB_URGENCY: ko.observable(),
        JOB_TYPE: ko.observable(),
        JOB_DURATION: ko.observable(),
        JOB_DUE_DATE: ko.observable(),
        ASSIGN_TYPE: ko.observable(),
        ASSIGN_TO: ko.observable(),
        ASSET_NO: ko.observable(),
        COST_CENTER: ko.observable(),
        DESCRIPTION: ko.observable(),
        LOCATION: ko.observable(),
        DEVICE_TYPE: ko.observable(),
        STATUS: ko.observable(),
        RESPONSIBLE_CENTER: ko.observable(),
        MODEL_NAME: ko.observable(),
        MODEL_NO: ko.observable(),
        SERIAL_NO: ko.observable(),
        WARRANTY_NOTES: ko.observable(),
        WARRANTY_INCLUDE: ko.observable(),
        WARRANTY_EXPIRYDATE: ko.observable(),
        TERM_PERIOD: ko.observable(),
        RISK_INCLUSIONFACTOR: ko.observable(),
        REQTICKETNO: ko.observable(),
        CLIENTNAME: ko.observable(),
        CLIENTADDRESS: ko.observable(),
        CLIENTCONTACT: ko.observable(),
        WOL_EMPLOYEE: ko.observable(),
        WOL_HOURS: ko.observable(),
        WOL_BILLABLE: ko.observable(),
        WOL_LABOUR_DATE: ko.observable(),
        WOL_ACTION: ko.observable()
    };

    var ah = new appHelper(systemText);

    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.toggleReadMode();
        var assetNoIDd = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        var dsdfx = ah.getParameterValueByName(ah.config.fld.reqWOAction);
        if (assetNoIDd !== null && assetNoIDd !== "") {

        } else {
            if (dsdfx === null || dsdfx === "") {
               
            }

        }
        $("#assetInfoDet").hide();

        ah.initializeLayout();
        postData = { LOV: "'AM_JOBTYPE','AIMS_SERVICEDEPARTMENT','AIMS_SPECIALTY','AIMS_PROBLEMTYPE','AIMS_PRIORITY'", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.getListLOV, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);


    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.createNew = function () {

        ah.toggleAddMode();


        var jDate = new Date();
        var strDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        var reqDate = strDate + ' ' + hourmin;

        $(ah.config.id.searchTicket).show();
        $(ah.config.id.searchAsset).show();
        $("#REQ_DATE").val(reqDate);
        $("#JOB_DURATION").val("0");
        $("#WO_STATUS").val("NE");
        var assetNoIDd = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        if (assetNoIDd !== null && assetNoIDd !== "") {
            self.readAssetNo(assetNoIDd);
        }
    };
    self.showModalFilter = function () {
        window.location.href = ah.config.url.modalFilter;
    };
    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.printpageCount = function () {

    };
    self.searchResultHelpDesk = function (jsonData) {
        var sr = jsonData.AM_HELPDESKLIST;

        $(ah.config.id.searchHelpDeskResult + ' ' + ah.config.tag.ul).html('');



        if (sr.length > 0) {
            var htmlstr = '';
            $('#noresultHDR').hide();
            for (var o in sr) {

                var reqDate = ah.formatJSONDateTimeToString(sr[o].ISSUE_DATE);
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, JSON.stringify(sr[o]), "Ticket No.&nbsp; " + sr[o].TICKET_ID, "Issue Date&nbsp; " + reqDate, "");
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.problem, sr[o].ISSUE_DETAILS + "&nbsp;&nbsp; ");
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.priority, sr[o].PRIORITY + "&nbsp;&nbsp; ");


                htmlstr += '</li>';

            }


        }
        $(ah.config.id.searchHelpDeskResult + ' ' + ah.config.tag.ul).append(htmlstr);
        $(ah.config.cls.detailItem).bind('click', self.populateWOHelpDesk);
        ah.toggleDisplayModeAsset();
    };
    self.getAssetNotes = function () {
        $("#notesHeader").text("Asset Record General Notes History");
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));

        var postData;

        var assetNo = $('#ASSET_NO').val();
        postData = { SEARCH: assetNo, STAFF_LOGON_SESSIONS: staffLogonSessions };

        $.post(self.getApi() + ah.config.api.searchAssetNotes, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.getWoNotes = function () {
        $("#notesHeader").text("Work Order Reference Note History");
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));

        var postData;

        var assetNo = $('#REQ_NO').val();
        postData = { SEARCH: assetNo, STAFF_LOGON_SESSIONS: staffLogonSessions };

        $.post(self.getApi() + ah.config.api.searchWONotes, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.newNote = function () {
        $("#WO_REFERENCE_NOTESREF").val(null);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        $("#WONOTE_RECORDED_BY").val(staffLogonSessions.STAFF_ID);
        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        strDateStart = strDateStart + ' ' + hourmin;
        $(ah.config.id.woReferenceNotes).prop(ah.config.attr.disabled, false);
        var woNotesInfo = staffLogonSessions.STAFF_ID + '@' + strDateStart;

        $("#woNotesEntered").text("Enter By: " + woNotesInfo);

        $("#WONOTE_RECORDED_DATE").val(strDateStart);
    };
    self.returnToAssetNotes = function () {
        $(ah.config.id.noteHistory).hide();
        $(ah.config.id.noteEntry).show();
    }
    self.populateWOHelpDesk = function () {
        var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfoId).toString());

        $(ah.config.id.searchAsset).hide();
        $("#TICKET_ID").val(infoID.TICKET_ID);
        $("#PROBLEM").val(infoID.ISSUE_DETAILS);
        $("#REQUESTER_CONTACT_NO").val(infoID.CONTACT_NO);
        $("#REQ_BY").val(infoID.FIRST_NAME + ' ' + infoID.LAST_NAME);
        $("#REQUESTER_CONTACT_NO").val(infoID.CONTACT_NO);

        $("#ASSET_NO").val(infoID.ASSET_NO);
        $("#DESCRIPTION").val(infoID.ASSET_DESC);
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        self.searchAssetFilter.assetNo = infoID.ASSET_NO;

        postData = { SEARCH: "", ASSET_NO: infoID.ASSET_NO, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readAssetInfoV, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);



    };
    self.pageLoader = function () {
        var senderID = $(arguments[1].currentTarget).attr('id');

        var searchStr = $(ah.config.id.txtGlobalSearchOth).val().toString();

        var pageNum = parseInt($(ah.config.id.pageNum).val());
        var pageCount = parseInt($(ah.config.id.pageCount).val());

        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.assetList));
        var lastPage = Math.ceil(sr.length / 10)

        if (isNaN(pageCount)) {
            pageCount = 1;
        }
        if (isNaN(pageNum)) {
            pageNum = 0;
        }

        switch (senderID) {

            case ah.config.tagId.nextPage: {
                if (pageCount < lastPage) {
                    pageNum = pageNum + 10;
                    pageCount++;
                    self.searchItemsResult(searchStr, pageNum);
                }

                break;
            }
            case ah.config.tagId.priorPage: {

                if (pageNum === 0) {
                } else {
                    pageNum = pageNum - 10;
                    if (pageNum < 0) {
                        pageNum = 0;
                    }
                    pageCount = pageCount - 1;
                }

                self.searchItemsResult(searchStr, pageNum);
                break;
            }

            case ah.config.tagId.txtGlobalSearchOth: {
                pageCount = 1;
                pageNum = 0;

                self.searchItemsResult(searchStr, 0);
                break;
            }
        }

        $(ah.config.id.loadNumber).text(pageCount.toString());
        $(ah.config.id.pageCount).val(pageCount.toString());
        $(ah.config.id.pageNum).val(pageNum);

    };
    self.othInfoLoader = function () {
        var infoDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.assetDetails));

        ah.togglehideOthInfo();
        $(ah.config.id.noteHistory + ',' + ah.config.id.addButtonIcon).hide();
        var senderID = $(arguments[1].currentTarget).attr('id');
        var assetDescription = $("#DESCRIPTION").val();
        if (assetDescription.length > 60) {
            assetDescription = assetDescription.substring(0, 60) + '...';
        }
        $("#itemDescription").text(assetDescription);
        $("#costHeader").hide();
        $(ah.config.id.noteEntry).show();
        $(ah.config.id.noteHistory).hide();
        switch (senderID) {

            case ah.config.tagId.btnContract: {
                ah.toggleShowInfo1();
                $("#winTitleInfo").text("CONTRACT LIST");
                $("#costHeader").show();
                self.searchContractNow();
                window.location.href = ah.config.url.modalAddItemsInfo;
                break;
            }
            case ah.config.tagId.btnLogHistory: {
                ah.toggleShowInfo7();
                $("#winTitleInfo").text("Status Log");
                $("#costHeader").show();
                self.searchStatusLogNow();
                window.location.href = ah.config.url.modalAddItemsInfo;
                break;
            }
            case ah.config.tagId.btnpdf: {
                ah.toggleShowInfo6();
                $("#winTitleInfo").text("Supporting Documents");
                $("#OTHSAVE").val("DOCS");
                $("#costHeader").show();
                $(ah.config.id.addButtonIcon).show();
                $(ah.config.id.supportingDocs + ' ' + ah.config.tag.ul).html('');
                var sr = srDataArray;
                if (sr.length > 0) {

                    var htmlstr = '';

                    for (var o in sr) {

                        htmlstr += '<li>';
                        htmlstr += ah.formatString(ah.config.html.searchRowHeaderDOCTemplateList, sr[o].FILE_NAME, sr[o].FILE_NAME, '', '');

                        htmlstr += '</li>';

                    }

                    $(ah.config.id.supportingDocs + ' ' + ah.config.tag.ul).append(htmlstr);
                    $(ah.config.cls.detailItem).bind('click', self.readDoc);
                }

                window.location.href = ah.config.url.modalAddItemsInfo;
                break;
            }
            case ah.config.tagId.btnLCycle: {
                ah.toggleShowInfo2();
                $("#winTitleInfo").text("WARRANTY");
                window.location.href = ah.config.url.modalAddItemsInfo;
                break;
            }
            case ah.config.tagId.btnHOperation: {
                ah.toggleShowInfo3();
                $("#winTitleInfo").text("HOURS OF OPERATION");
                window.location.href = ah.config.url.modalAddItemsInfo;
                break;
            }
            case ah.config.tagId.btnANotes: {
                ah.toggleShowInfo4();
                $("#winTitleInfo").text("NOTES");
                $("#OTHSAVE").val("NOTES");
                $(ah.config.id.addButtonIcon).show();

                var woStatus = $("#WO_STATUS").val();
                if (self.validatePrivileges('20') === 0 && woStatus === 'CL') {

                    $(ah.config.id.addButtonIcon + ',' + ah.config.id.newNotes).hide();
                }

                window.location.href = ah.config.url.modalAddItemsInfo;
                break;
            }



            case ah.config.tagId.btnAPersonnel: {
                ah.toggleShowInfo5();
                $("#winTitleInfo").text("ASSIGNED SERVICE PERSONNEL");
                window.location.href = ah.config.url.modalAddItemsInfo;
                break;
            }
        }



    };
    self.readDoc = function () {

        var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();


        var mywin = window.open("/UploadedFiles/" + infoID, "width=960,height=764");


    };
    self.assetSearchNow = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        //self.searchFilter.status();
        self.searchAssetFilter.assetNo = '';
        //ah.toggleSearchMode();
        ah.toggleSearchItems();

        if (self.validatePrivileges('15') === 0) {
            self.searchAssetFilter.staffId = staffDetails.STAFF_ID;
        }

        postData = { SEARCH: "", SEARCH_FILTER: self.searchAssetFilter, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAssets, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = ah.config.url.modalAddItems;

    };
    self.SearchNowHelpDesk = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        //self.searchFilter.status();

        //ah.toggleSearchMode();
        ah.toggleSearchItems();

        if (self.validatePrivileges('21') === 0) {
            self.searchAssetFilter.staffId = staffDetails.STAFF_ID;
        }
        postData = { MENU_GROUP: "", SYSTEMID: "", SEARCH: "", SEARCH_FILTER: self.searchAssetFilter, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchHelpDeskNew, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = ah.config.url.modalhdr;

    };
    self.workOrderLabour = function () {
        var assetpDetails = sessionStorage.getItem(ah.config.skey.assetItems);
        var jassetpDetails = JSON.parse(assetpDetails);
        // alert(JSON.stringify(jassetpDetails));
        var woDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.woDetails));
        //  alert(JSON.stringify(woDetails));
        var assetNoIDd = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        var xsersdfsdf = "";
        if (assetNoIDd === null || assetNoIDd === "") {
            xsersdfsdf = "sdfsdfxdfwefsd";
        }
        var assetNo = "";
        var assetNoDesc = ""
        if (jassetpDetails !== null) {
            assetNo = jassetpDetails.ASSET_NO;
            assetNoDesc = jassetpDetails.DESCRIPTION;
        }
        var reqWoNO = ah.getParameterValueByName(ah.config.fld.reqNo);
        var x = document.getElementById("ASSIGN_TO").selectedIndex;
        var y = document.getElementById("ASSIGN_TO").options;
        var yDesc = y[x].text;
        if (yDesc === "- select -") {
            yDesc = "";
        }

        var dashBoard = ah.getParameterValueByName(ah.config.fld.fromDashBoard);
        var sdfsdf = "";
       
        if (dashBoard == "dsdxsesesdfsadfsadfqwerqwerqwersadfsadfasdfasdfsadfsadfsadfasdf") {
            sdfsdf = "dsdxsesesdfsadfsadfqwerqwerqwersadfsadfasdfasdfsadfsadfsadfasdf";
        }

        window.location.href = encodeURI(ah.config.url.aimWorkOrderLabour + "?ASSETNO=" + assetNo + "&ASSETDESC=" + assetNoDesc + "&REQNO=" + woDetails.REQ_NO + "&REQTOR=" + woDetails.REQ_BY + "&REQCONTACT=" + woDetails.REQUESTER_CONTACT_NO + "&REQDATE=" + woDetails.REQ_DATE + "&REQTYPE=" + woDetails.REQ_TYPE + "&REQDESC=" + woDetails.PROBLEM + "&REQSTATUS=" + woDetails.WO_STATUS + "&REQWOAC=" + xsersdfsdf + "&ASSIGNTO=" + woDetails.ASSIGN_TO + "&ASSIGNDESC=" + yDesc + "&jhsadfjhxsdf=xchvisjfkkewiorusjcxkvjsdfervcvsdfwer&uycsder?sdfsdferucd&asdfxhsadfsdfx=" + sdfsdf);
    };
    self.modifySetup = function () {

        ah.toggleEditMode();
        if (self.validatePrivileges('23') === 0) {
            $(ah.config.id.closeDate).prop(ah.config.attr.readOnly, true);
        } else {
            $(ah.config.id.closeDate).prop(ah.config.attr.readOnly, false);
        }
        var woStatus = $("#WO_STATUS").val();
        if (self.validatePrivileges('20') === 0 && woStatus === 'CL') {

            $(ah.config.id.woStatus).prop(ah.config.attr.disabled, true);
        }
        var ticketNo = $("#TICKET_ID").val();
        if (ticketNo > 0) {
            $(ah.config.id.searchTicket).hide();
            $(ah.config.id.searchAsset).hide();
        } else {
            $(ah.config.id.searchTicket).show();
            $(ah.config.id.searchAsset).show();
        }


    };
    self.applyWONotes = function () {
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var othAction = $("#OTHSAVE").val();

        if (othAction === "NOTES") {
            $("#WONOTE_RECORDED_BY").val(staffLogonSessions.STAFF_ID);
            var jDate = new Date();
            var strDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);
            var modifyBy = staffLogonSessions.STAFF_ID;
            var modifyDate = strDate + ' ' + hourmin;
            //$("#WONOTE_RECORDED_DATE").val(modifyDate);
            var refNotes = $("#WO_REFERENCE_NOTESREF").val();
            if (refNotes === null || refNotes === "") {
                alert('Please enter a valid notes to proceed.');
                return;
            }
            $("#WO_REFERENCE_NOTES").val(refNotes);

            $("#SAVEACTION").val('SAVEINFO');
            $(ah.config.id.saveInfo).trigger('click');
        } else {

            var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
            var setupDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var postData;
            ah.toggleSaveMode();
            var myFile = document.getElementById('pdffile').value;
            var myFileData = document.getElementById('pdffile');

            var fName = null;
            //alert(myFile.length);
            if (myFile.length > 0) {
                var fName = myFileData.files[0].name;
            }


            var refId = $("#REQ_NO").val();
            self.processInfo.statusBy = staffLogonSessions.STAFF_ID;
            self.processInfo.filename = fName;
            self.processInfo.filetype = 'pdf';
            self.processInfo.category = 'ASSETWO';
            self.processInfo.refid = refId;
            var jDate = new Date();
            var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);

            self.processInfo.StatusDate = strDateStart + ' ' + hourmin;
            postData = { MODIFYINFO: self.processInfo, SCANDOCS: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            // alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.createDocs, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

        }



    };
    self.printWorkOrder = function () {
        // self.loadDataPrint("");
    
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;
        document.getElementById("printSummary").classList.remove('printSummary');
        document.getElementById("printDetails").classList.add('printSummary');
        document.getElementById("printBulk").classList.remove('printSummary');
        var reqId = document.getElementById("REQ_NO").value;
        postData = { CLIENTID: staffDetails.CLIENT_CODE, SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), REQNO: reqId, STAFF_LOGON_SESSIONS: staffLogonSessions, SEARCH_FILTER: self.searchFilter, STATUS: "" };
        $.post(self.getApi() + ah.config.api.searchwoMaterial, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        //window.print();
    };
    self.printBulk = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;
        self.searchFilter.woIdList(woIdList);
        postData = { CLIENTID: staffDetails.CLIENT_CODE, SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions, SEARCH_FILTER: self.searchFilter };
        $.post(self.getApi() + ah.config.api.serachWoList, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = ah.config.url.openModalPrintPage;
    };

    self.getspareParts = function () {
        var reqID = this.getAttribute(ah.config.attr.datainfoId).toString();

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        postData = { CLIENTID: staffDetails.CLIENT_CODE, SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), REQNO: reqID, STAFF_LOGON_SESSIONS: staffLogonSessions, SEARCH_FILTER: self.searchFilter, STATUS: "" };
        $.post(self.getApi() + ah.config.api.searchwoMaterial, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.saveAllChanges = function () {
        $("#SAVEACTION").val('SAVEINFO');
        $(ah.config.id.saveInfo).trigger('click');

    };

    self.cancelChanges = function () {
        $("#REQ_ID").show();
        $("label[for='REQ_ID']").show();
        var assetNoIDx = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        $("#ASSET_NO+a").next("span").remove();
        var reqId = $("#REQ_NO").val();
        if (reqId === null || reqId === "") {
            if (assetNoIDx !== null && assetNoIDx !== "") {
                $("#infoID").text(assetNoID + "-" + assetDescription);
                $("#assetInfobtn").show();
                $("#assetInfoDet").hide();
                self.searchNow();
            } else {
                ah.initializeLayout();
            }

        } else {
            ah.toggleDisplayMode();
            self.readWOId(reqId);
        }

    };
    self.cancelChangesModal = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            //ah.toggleDisplayMode();
            //self.showDetails();
        }
        window.location.href = ah.config.url.modalClose;

    };

    self.loadDataPrint = function (data) {
        var infoAsset = JSON.parse(sessionStorage.getItem(ah.config.skey.assetItems));
        if (infoAsset !== null) {
            if (infoAsset.ASSET_NO !== null && infoAsset.ASSET_NO !== "") {

                self.woDetails.ASSET_NO(infoAsset.ASSET_NO);
                self.woDetails.RISK_INCLUSIONFACTOR(infoAsset.RISK_INCLUSIONFACTOR);
                self.woDetails.COST_CENTER(infoAsset.COST_CENTER);
                self.woDetails.DESCRIPTION(infoAsset.DESCRIPTION);
                self.woDetails.LOCATION(infoAsset.LOCATION);
                self.woDetails.DEVICE_TYPE(infoAsset.DTYPE_DESC);
                self.woDetails.RESPONSIBLE_CENTER(infoAsset.RESPONSIBLE_CENTER);
                self.woDetails.STATUS(infoAsset.STATUS);
                self.woDetails.MODEL_NAME(infoAsset.MODEL_NAME);
                self.woDetails.MODEL_NO(infoAsset.MODEL_NO);
                self.woDetails.SERIAL_NO(infoAsset.SERIAL_NO);
                self.woDetails.WARRANTY_NOTES(infoAsset.WARRANTY_NOTES);
                self.woDetails.WARRANTY_INCLUDE(infoAsset.WARRANTY_DESC);
                self.woDetails.WARRANTY_EXPIRYDATE(ah.formatJSONDateTimeToString(infoAsset.WARRANTY_EXPIRYDATE));
                self.woDetails.TERM_PERIOD(infoAsset.TERM_PERIOD + ' ' + infoAsset.WARRANTY_FREQUNIT);
            }
        }


        var x = document.getElementById("WO_STATUS").selectedIndex;
        var y = document.getElementById("WO_STATUS").options;
        var yDesc = y[x].text;
        self.woDetails.WO_STATUS(yDesc);

        var ax = document.getElementById("ASSIGN_TO").selectedIndex;
        var ay = document.getElementById("ASSIGN_TO").options;
        var ayDesc = ay[ax].text;
        if (ayDesc === "- select -") {
            ayDesc = "";
        }
        self.woDetails.ASSIGN_TO(ayDesc);

        var ax = document.getElementById("ASSIGN_TYPE").selectedIndex;
        var ay = document.getElementById("ASSIGN_TYPE").options;
        if (ax === -1) {

        } else {
            var ayDesc = ay[ax].text;
            self.woDetails.ASSIGN_TYPE(ayDesc);
        }


        ayDesc = document.getElementById("JOB_DUE_DATE").value;
        self.woDetails.JOB_DUE_DATE(ayDesc);

        //self.woDetails.JOB_DURATION(data.JOB_DURATION);
        var ax = document.getElementById("JOB_TYPE").selectedIndex;
        var ay = document.getElementById("JOB_TYPE").options;
        if (ax === -1) {

        } else {
            var ayDesc = ay[ax].text;

            if (ayDesc === "- select -") {
                ayDesc = "";
            }
        }
        var ayDesc = ay[ax].text;

        if (ayDesc === "- select -") {
            ayDesc = "";
        }
        self.woDetails.JOB_TYPE(ayDesc);

        ax = document.getElementById("JOB_URGENCY").selectedIndex;
        ay = document.getElementById("JOB_URGENCY").options;
        if (ax === -1) {

        } else {
            ayDesc = ay[ax].text;
            if (ayDesc === "- select -") {
                ayDesc = "";
            }
        }
        self.woDetails.JOB_URGENCY(ayDesc);

        ayDesc = document.getElementById("PROBLEM").value;
        self.woDetails.PROBLEM(ayDesc);

        ayDesc = document.getElementById("REQUESTER_CONTACT_NO").value;
        ayDesc = ayDesc + '/' + document.getElementById("REQ_BY").value;
        //self.woDetails.REQUESTER_CONTACT_NO(data.REQUESTER_CONTACT_NO);
        self.woDetails.REQ_BY(ayDesc);

        ayDesc = document.getElementById("REQ_DATE").value;
        self.woDetails.REQ_DATE(ayDesc);
        ayDesc = document.getElementById("REQ_NO").value;
        self.woDetails.REQ_NO(ayDesc);

        ayDesc = document.getElementById("TICKET_ID").value;
        self.woDetails.REQTICKETNO(ayDesc);
        //self.woDetails.REQ_REMARKS(data.REQ_REMARKS);

        ax = document.getElementById("REQ_TYPE").selectedIndex;
        ay = document.getElementById("REQ_TYPE").options;
        ayDesc = ay[ax].text;
        self.woDetails.REQ_TYPE(ayDesc);

        ax = document.getElementById("SERV_DEPT").selectedIndex;
        ay = document.getElementById("SERV_DEPT").options;
        ayDesc = ay[ax].text;
        if (ayDesc === "- select -") {
            ayDesc = "";
        }

        self.woDetails.SERV_DEPT(ayDesc);
        //self.woDetails.SPECIALTY(data.SPECIALTY);


    };
    self.getAssetEmp = function () {

        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var assignType = $("#ASSIGN_TYPE").val();

        if (assignType === "PERSON") {
            $("#ASSIGN_TO").val(staffDetails.STAFF_ID);
        } else {
            $("#ASSIGN_TO").val(null);
        }
    };
    self.showDetails = function (isNotUpdate, jsonData) {
        //ah.toggleDisplayMode();
        ah.toggleEditMode();
        sparePartsCount = 0;
        var dashBoard = ah.getParameterValueByName(ah.config.fld.fromDashBoard);

       
       


        if (isNotUpdate) {


            var infoDetails = sessionStorage.getItem(ah.config.skey.woDetails);

            var jsetupDetails = JSON.parse(infoDetails);

            var strTime = '';
            var reqDate = ah.formatJSONDateTimeToString(jsetupDetails.REQ_DATE);
            var closeDate = ah.formatJSONDateTimeToString(jsetupDetails.CLOSED_DATE);
            jsetupDetails.REQ_DATE = reqDate;
            jsetupDetails.CLOSED_DATE = closeDate;



            ah.ResetControls();
            $("#CURRSTATUS").val(jsetupDetails.WO_STATUS);
            var infoAsset = JSON.parse(sessionStorage.getItem(ah.config.skey.assetItems));
            //alert(JSON.stringify(infoAsset));
            if (infoAsset !== null) {
                self.populateAssetInfo(jsonData);
                //ah.LoadJSON(infoAsset);
                //$("#ASSETSTATUS").val(infoAsset.STATUS);
            }

            if (self.validatePrivileges('23') === 0) {
                $(ah.config.id.closeDate).prop(ah.config.attr.readOnly, true);
            } else {
                $(ah.config.id.closeDate).prop(ah.config.attr.readOnly, false);
            }
            var woStatus = jsetupDetails.WO_STATUS;
            //if (self.validatePrivileges('20') === 0 && woStatus === 'CL') {

                
          //  }
            $(ah.config.cls.liSave).hide();
            if (woStatus !=='CL'){
                $("#CLOSED_DATE").hide();
                $("#lblClosedDate").hide();
                $(ah.config.id.woStatus).prop(ah.config.attr.disabled, false);
                
            } else {
                $(ah.config.id.woStatus).prop(ah.config.attr.disabled, true);
                $("#CLOSED_DATE").show();
                $("#lblClosedDate").show();
            }


            if (jsetupDetails.AM_INSPECTION_ID === 0) {
                jsetupDetails.AM_INSPECTION_ID = null;
            };

            $("#WO_REFERENCE_NOTESREF").val(jsetupDetails.WO_REFERENCE_NOTES);
            if (jsetupDetails.WONOTE_RECORDED_DATE !== "" && jsetupDetails.WONOTE_RECORDED_DATE !== "null" && jsetupDetails.WONOTE_RECORDED_DATE !== "0001-01-01T00:00:00" && jsetupDetails.WONOTE_RECORDED_DATE !== "1900-01-01T00:00:00") {
                var woNotesInfo = ah.formatJSONDateTimeToString(jsetupDetails.WONOTE_RECORDED_DATE);
                woNotesInfo = jsetupDetails.WONOTE_RECORDED_BY + '@' + woNotesInfo;

                $("#woNotesEntered").text("Last Entered By: " + woNotesInfo);
            }


            ah.LoadJSON(jsetupDetails);
            if (jsetupDetails.TICKET_ID > 0) {
                $(ah.config.id.searchTicket).hide();
                $(ah.config.id.searchAsset).hide();
            }
            $("#REQ_ID").val(jsetupDetails.REQ_NO);
            if (jsetupDetails.JOB_DUE_DATE == "" || jsetupDetails.JOB_DUE_DATE == "null" || jsetupDetails.JOB_DUE_DATE == "0001-01-01T00:00:00" || jsetupDetails.JOB_DUE_DATE == "1900-01-01T00:00:00") {
                //var jobDueDate = ah.formatJSONDateTimeToString("1900-01-01T00:00:00");
                $("#JOB_DUE_DATE").val("");
            }
            

            //alert(JSON.stringify(jsonData.AM_PARTSREQUEST));
            //populate parts
            $(ah.config.id.searchResultListParts + ' ' + ah.config.tag.tbody).html('');
            var srparts = jsonData.AM_PARTSREQUEST;
            var htmlstr = ""
            if (srparts.length > 0) {
                sparePartsCount = 1;
                $(ah.config.id.searchResultListParts).show();
                for (var o in srparts) {
                    if (srparts[o].PRODUCT_DESC === null || srparts[o].PRODUCT_DESC === "") {
                        srparts[o].PRODUCT_DESC = srparts[o].DESCRIPTION;
                        srparts[o].ID_PARTS = 'Supplier'
                    }

                    htmlstr += "<tr>";
                    htmlstr += '<td class="a">' + srparts[o].ID_PARTS + '</td>';
                    htmlstr += '<td class="h">' + srparts[o].PRODUCT_DESC + '</td>';
                    htmlstr += '<td class="a">' + srparts[o].QTY + '</td>';
                    htmlstr += "</tr>";
                }

                $(ah.config.id.searchResultListParts + ' ' + ah.config.tag.tbody).append(htmlstr);
            } else {
                $(ah.config.id.searchResultListParts).hide();
            }

        } else {

        }

        if (dashBoard == "dsdxsesesdfsadfsadfqwerqwerqwersadfsadfasdfasdfsadfsadfsadfasdf") {
            $(ah.config.cls.liSearch).hide();
        }

    };
    self.getDateNow = function () {
        var dateNow = moment();
        return dateNow;
    }
    self.isDateFutre = function (date1,date2) {        
        if (date1 < date2) {
            return true;
        }
        return false;

    }
    self.showAssetDetails = function (jsonData) {
        //alert(isNotUpdate);

        var infoAsset = JSON.parse(sessionStorage.getItem(ah.config.skey.assetItems));
        var lov = jsonData.LOV_LOOKUPS;
       
        ah.LoadJSON(infoAsset);
        $("#ASSETSTATUS").val(infoAsset.STATUS);
        if (lov !== null) {
            lov = sr.filter(function (item) { return item.CATEGORY === "AIMS_DTYPE" });

            if (sr.length > 0) {
                $("#CATEGORY").val(sr[0].DESCRIPTION);
            }
        }


    };
    self.pagingLoader = function () {


        var senderID = $(arguments[1].currentTarget).attr('id');



        var pageNum = parseInt($(ah.config.id.PaginGpageNum).val());
        var pageCount = parseInt($(ah.config.id.PaginGpageCount).val());


        sr = JSON.parse(sessionStorage.getItem(ah.config.skey.woDetails));



        var lastPage = Math.ceil(sr.length / 500);

        if (isNaN(pageCount)) {
            pageCount = 1;
        }
        if (isNaN(pageNum)) {
            pageNum = 0;
        }

        switch (senderID) {

            case ah.config.tagId.PaginGnextPage: {

                if (pageCount < lastPage) {
                    pageNum = pageNum + 500;
                    pageCount++;

                    self.searchResult(pageNum);
                }
                break;
            }
            case ah.config.tagId.PaginGpriorPage: {

                if (pageNum === 0) {
                } else {
                    pageNum = pageNum - 500;
                    if (pageNum < 0) {
                        pageNum = 0;
                    }
                    pageCount = pageCount - 1;
                }

                self.searchResult(pageNum);
                break;
            }

        }

        $("#PaginGloadNumber").text(pageCount.toString());
        $("#PaginGpageCount").val(pageCount.toString());
        $("#PaginGpageNum").val(pageNum);

    };
    self.searchResult = function (pageNum) {


        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.woDetails));

        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {

            self.woDetails.CLIENTNAME(clientInfo.DESCRIPTION);
            self.woDetails.CLIENTADDRESS(clientInfo.ADDRESS + ', ' + clientInfo.CITY + ', ' + clientInfo.ZIPCODE + ', ' + clientInfo.STATE);
            self.woDetails.CLIENTCONTACT('Tel.No: ' + clientInfo.CONTACT_NO1 + ' Fax No:' + clientInfo.CONTACT_NO2);
            $(ah.config.cls.liPrint).show();
            woIdList = "";
            var htmlstr = '';
            var pageNumto = pageNum + 500;
            var pagenum = Math.ceil(sr.length / 500);

            sr = sr.filter(function (item) { return item.ROWNUMID >= pageNum && item.ROWNUMID <= pageNumto });



            if (pagenum >= 2) {

                $(ah.config.cls.pagingDiv).show();
            } else {
                $(ah.config.cls.pagingDiv).hide();
            }

            $("#PaginGtotNumber").text(pagenum.toString());
            var countsr = 0
            for (var o in sr) {
                countsr++;
                var assetDesc = "";
                if (sr[o].DESCRIPTION === null || sr[o].DESCRIPTION === "") {
                    assetDesc = "WorkSheet Inspection"
                } else {
                    assetDesc = sr[o].DESCRIPTION
                }

                if (sr[o].WO_STATUS === 'CL') {
                    sr[o].WO_STATUS = 'Closed';
                } else if (sr[o].WO_STATUS === 'OP') {
                    sr[o].WO_STATUS = 'Open';
                }
                else if (sr[o].WO_STATUS === 'HA') {
                    sr[o].WO_STATUS = 'On Hold';
                } else if (sr[o].WO_STATUS === 'FA') {
                    sr[o].WO_STATUS = 'HELD';
                } else if (sr[o].WO_STATUS === 'NE') {
                    sr[o].WO_STATUS = 'New';
                } else if (sr[o].WO_STATUS === 'PS') {
                    sr[o].WO_STATUS = 'Posted';
                } else if (sr[o].WO_STATUS === 'FC') {
                    sr[o].WO_STATUS = 'For Close';
                }
                if (sr[o].REQ_BY === null) {
                    sr[o].REQ_BY = '';
                }
                if (countsr === sr.length) {
                    if (sr[o].ASSET_NO !== null && sr[o].ASSET_NO !== "") {
                        assetIdList += "'" + sr[o].ASSET_NO + "'";
                    }

                    woIdList += "'" + sr[o].REQ_NO + "'";
                } else {
                    if (sr[o].ASSET_NO !== null && sr[o].ASSET_NO !== "") {
                        assetIdList += "'" + sr[o].ASSET_NO + "',";
                    }
                    woIdList += "'" + sr[o].REQ_NO + "',";
                }

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].REQ_NO, sr[o].ASSET_NO, assetDesc, "");
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.woOrder, sr[o].REQ_NO);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.problem, sr[o].PROBLEM);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.requestor, sr[o].REQ_BY);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.reqDate, ah.formatJSONDateTimeToString(sr[o].REQ_DATE));
                if (sr[o].WO_STATUS === "Closed") {
                    htmlstr += ah.formatString(ah.config.html.searchRowTemplateC, systemText.searchModeLabels.statusName, sr[o].WO_STATUS);
                } else {
                    htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.statusName, sr[o].WO_STATUS);
                }
                if (sr[o].AM_INSPECTION_ID !== null && sr[o].AM_INSPECTION_ID > 0) {
                    htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.workSheet, sr[o].AM_INSPECTION_ID.toString() + '-' + sr[o].INSPECTION_TYPE);
                }
                if (sr[o].SPAREPARTS > 0) {
                    htmlstr += ah.formatString(ah.config.html.searchRowTemplateSpareParts, sr[o].REQ_NO, 'Spare Parts: ' + sr[o].SPAREPARTS);
                }

                htmlstr += '</li>';

            }
            resultHtml = htmlstr;
            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        } else {
            $(ah.config.cls.liPrint).hide();
        }

        $(ah.config.cls.detailItem).bind('click', self.readSetupID);
        $(ah.config.cls.detailParts).bind('click', self.getspareParts);
        ah.displaySearchResult();
        if (self.validatePrivileges('6') === 0) {
            $(ah.config.cls.liAddNew).hide();
        };

    };
    self.searchResultNotes = function (jsonData) {
        $(ah.config.id.noteEntry).hide();
        $(ah.config.id.noteHistory).show();
        var sr = jsonData.AM_ASSET_NOTES;
        $(ah.config.id.noteHistory + ' ' + ah.config.tag.ul).html('');


        if (sr.length > 0) {
            var htmlstr = "";
            for (var o in sr) {
                if (sr[o].FIRST_NAME === null) {
                    sr[o].FIRST_NAME = "";
                }
                if (sr[o].LAST_NAME === null) {
                    sr[o].LAST_NAME = "";
                }
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].ASSET_NO, sr[o].CREATED_DATE, sr[o].FIRST_NAME, sr[o].LAST_NAME);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.notes, sr[o].NOTES);

                htmlstr += '</li>';

            }

            $(ah.config.id.noteHistory + ' ' + ah.config.tag.ul).append(htmlstr);
        }
    }
    self.searchResultWONotes = function (jsonData) {
        $(ah.config.id.noteEntry).hide();
        $(ah.config.id.noteHistory).show();
        var sr = jsonData.AM_WORK_ORDER_REFNOTES;
        $(ah.config.id.noteHistory + ' ' + ah.config.tag.ul).html('');


        if (sr.length > 0) {
            var htmlstr = "";
            for (var o in sr) {
                if (sr[o].FIRST_NAME === null) {
                    sr[o].FIRST_NAME = "";
                }
                if (sr[o].LAST_NAME === null) {
                    sr[o].LAST_NAME = "";
                }
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].FIRST_NAME, sr[o].CREATED_DATE, sr[o].FIRST_NAME, sr[o].LAST_NAME);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.notes, sr[o].WO_NOTES);

                htmlstr += '</li>';

            }

            $(ah.config.id.noteHistory + ' ' + ah.config.tag.ul).append(htmlstr);
        }
    }
    self.searchResultWOStatus = function (jsonData) {

        var sr = jsonData.AM_WORK_ORDERS_STATUS_LOG;

        $(ah.config.id.statusLogList + ' ' + ah.config.tag.ul).html('');


        if (sr.length > 0) {
            var htmlstr = "";
            for (var o in sr) {
                if (sr[o].FIRST_NAME === null) {
                    sr[o].FIRST_NAME = "";
                }
                if (sr[o].LAST_NAME === null) {
                    sr[o].LAST_NAME = "";
                }
                if (sr[o].WO_STATUS === 'NE') {
                    sr[o].WO_STATUS = 'New';
                } else if (sr[o].WO_STATUS === 'CL') {
                    sr[o].WO_STATUS = 'Closed';
                }
                else if (sr[o].WO_STATUS === 'OP') {
                    sr[o].WO_STATUS = 'Open';
                }
                else if (sr[o].WO_STATUS === 'PS') {
                    sr[o].WO_STATUS = 'Posted';
                }
                else if (sr[o].WO_STATUS === 'FC') {
                    sr[o].WO_STATUS = 'For Close';
                }
                else if (sr[o].WO_STATUS === 'HA') {
                    sr[o].WO_STATUS = 'On-Hold';
                }

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].RECORDED_DATE, sr[o].WO_STATUS, sr[o].FIRST_NAME, sr[o].LAST_NAME + '@' + sr[o].RECORDED_DATE);

                htmlstr += '</li>';

            }

            $(ah.config.id.statusLogList + ' ' + ah.config.tag.ul).append(htmlstr);
        }
    }
    self.printSummary = function () {
        window.location.href = ah.config.url.openModalPrintPage;


        document.getElementById("printSummary").classList.add('printSummary');
        document.getElementById("printDetails").classList.remove('printSummary');
        document.getElementById("printBulk").classList.remove('printSummary');
        $(ah.config.id.printSummary + ' ' + ah.config.tag.ul).html('');
        $(ah.config.id.printSummary + ' ' + ah.config.tag.ul).append(resultHtml);
        setTimeout(function () {
            window.location.href = ah.config.url.modalClose;
            window.print();

        }, 4000);
        //setTimeout(function () { window.print(); }, 3000);
        //window.print();
        // window.location.href = ah.config.url.modalClose;
    };
    self.searchContractResult = function (jsonData) {


        var sr = jsonData.AM_ASSET_CONTRACTSERVICES;

        $(ah.config.id.searchResult2 + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].CONTRACT_ID, "Contract ID&nbsp; " + sr[o].CONTRACT_ID, "Coverage&nbsp; " + sr[o].TYPE_COVERAGE, "");
                //htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.typeCoverage, sr[o].TYPE_COVERAGE);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.provider, sr[o].PROVIDER + "&nbsp;&nbsp; ");
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.labour, sr[o].COVERAGE_LABOR + "&nbsp;&nbsp; ");
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.parts, sr[o].COVERAGE_PARTSMATERIALS);


                htmlstr += '</li>';

            }

            $(ah.config.id.searchResult2 + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        //$(ah.config.cls.detailItem).bind('click', self.readSetupID);
        ////ah.toggleDisplayMode();
        ////ah.toggleAddItems();

    };
    self.searchItemsResult = function (LovCategory, pageLoad) {

        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.assetList));

        if (LovCategory !== null && LovCategory !== "") {
            var arr = sr;
            var keywords = LovCategory.toUpperCase();
            var filtered = [];

            for (var i = 0; i < sr.length; i++) {
                for (var j = 0; j < keywords.length; j++) {
                    if ((sr[i].DESCRIPTION.toUpperCase()).indexOf(keywords) > -1 || (sr[i].ASSET_NO.toUpperCase()).indexOf(keywords) > -1) {
                        filtered.push(sr[i]);
                        break;
                    }
                }
            }
            sr = filtered;
        }

        $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).html('');

        if (sr.length > 0) {
            $("#noresult").hide();
            $("#pageSeparator").show();
            $("#page1").show();
            $("#page2").show();
            $("#nextPage").show();
            $("#priorPage").show();
            var totalrecords = sr.length;
            var pagenum = Math.ceil(totalrecords / 10);
            if (pagenum < 2 && pagenum > 1) {
                pagenum = 2;
            }
            $(ah.config.id.totNumber).text(pagenum.toString());

            if (pagenum <= 1) {
                $("#page1").hide();
                $("#page2").hide();
                $("#pageSeparator").hide();
                $("#nextPage").hide();
                $("#priorPage").hide();

            }
            var htmlstr = '';
            var o = pageLoad;

            for (var i = 0; i < 10; i++) {
                var assetDescription = sr[o].DESCRIPTION;
                if (assetDescription === null || assetDescription === "" || assetDescription === "null") {

                    sr[o].DESCRIPTION = "Undefined"
                    assetDescription = "Undefined"
                }
                if (assetDescription.length > 70) {
                    assetDescription = assetDescription.substring(0, 70) + '...';
                }
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].ASSET_NO, assetDescription, '');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.buildingName, sr[o].BUILDING) + '&nbsp;&nbsp;';
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.locationName, sr[o].LOCATION + '&nbsp;&nbsp;');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.conditionName, sr[o].CONDITION + '&nbsp;&nbsp;');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.statusName, sr[o].STATUS);
                htmlstr += '</li>';
                o++
                if (totalrecords === o) {
                    break;
                }
            }
            //alert(JSON.stringify(sr));
            //alert(htmlstr);
            $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).append(htmlstr);
        }
        else {
            $("#page1").hide();
            $("#page2").hide();
            $("#pageSeparator").hide();
            $("#nextPage").hide();
            $("#priorPage").hide();
            $("#noresult").show();
        }
        $(ah.config.cls.detailItem).bind('click', self.populateAssetInfoById);
        ah.toggleDisplayModeAsset();
        //ah.toggleAddItems();
    };
    self.searchPartsOnhandResult = function (jsonData) {
        var wopmPartsr = jsonData.AM_PARTSREQUEST;

        if (wopmPartsr.length > 0) {
            self.partsWOList.splice(0, 5000);

            for (var o in wopmPartsr) {
                var billable = 'Yes';
                if (wopmPartsr[o].BILLABLE === "false") {
                    billable = 'No';
                }

                if (wopmPartsr[o].PRODUCT_DESC != null && wopmPartsr[o].PRODUCT_DESC.length > 60)
                    wopmPartsr[o].PRODUCT_DESC = wopmPartsr[o].PRODUCT_DESC.substring(0, 59) + "..."
                if (wopmPartsr[o].PRODUCT_DESC === null || wopmPartsr[o].PRODUCT_DESC === "") {
                    wopmPartsr[o].PRODUCT_DESC = wopmPartsr[o].DESCRIPTION;
                    wopmPartsr[o].ID_PARTS = 'Supplier';
                }
                self.partsWOList.push({
                    PRODUCT_DESC: wopmPartsr[o].PRODUCT_DESC,
                    DESCRIPTION: wopmPartsr[o].DESCRIPTION,
                    ID_PARTS: wopmPartsr[o].ID_PARTS,
                    STORE_DESC: wopmPartsr[o].STORE_DESC,
                    STORE_ID: wopmPartsr[o].STORE_ID,
                    BILLABLE: billable,
                    PRICE: wopmPartsr[o].PRICE,
                    STATUSREQ: wopmPartsr[o].STATUS,
                    TRANSFER_QTY: wopmPartsr[o].TRANSFER_QTY,
                    TRANSFER_FLAG: wopmPartsr[o].TRANSFER_FLAG,
                    EXTENDED: wopmPartsr[o].EXTENDED,
                    REQUEST_ID: wopmPartsr[o].REQUEST_ID,
                    REF_WO: wopmPartsr[o].REF_WO,
                    WO_LABOURTYPE: wopmPartsr[o].WO_LABOURTYPE,
                    STATUS: wopmPartsr[o].STATUS,
                    QTY_RETURN: wopmPartsr[o].QTY_RETURN,
                    QTY_FORRETURN: wopmPartsr[o].QTY_FORRETURN,
                    REF_ASSETNO: wopmPartsr[o].REF_ASSETNO,
                    AM_WORKORDER_LABOUR_ID: wopmPartsr[o].AM_WORKORDER_LABOUR_ID,
                    QTY: wopmPartsr[o].QTY
                });


            }
        }
        window.location.href = ah.config.url.openModalInHouse;
    };
    self.searchWoMaterialResult = function (jsonData) {

        var wopmPartsr = jsonData.AM_PARTSREQUEST;

        var clientInformation = jsonData.AM_CLIENT_INFO;

        var htmlstr = '';
        htmlstr += "<tr>";
        self.woDetails.CLIENTNAME(clientInformation.DESCRIPTION);
        self.woDetails.CLIENTADDRESS(clientInformation.ADDRESS + ', ' + clientInformation.CITY + ', ' + clientInformation.ZIPCODE + ', ' + clientInformation.STATE);
        self.woDetails.CLIENTCONTACT('Tel.No: ' + clientInformation.CONTACT_NO1 + ' Fax No:' + clientInformation.CONTACT_NO2);


        var wolInformation = jsonData.AM_WORKORDER_LABOUR;

        if (wolInformation.length > 0) {
            $("#woLabourInfo").show();
            //alert(JSON.stringify(wolInformation));
            if (wolInformation[0].EMPLOYEE === null || wolInformation[0].EMPLOYEE === "") {
                //alert(wolInformation[0].SUPPLIER);
                self.woDetails.WOL_EMPLOYEE(wolInformation[0].SUPPLIER);
            } else {
                self.woDetails.WOL_EMPLOYEE(wolInformation[0].EMPLOYEE);
            }
            self.woDetails.WOL_HOURS(wolInformation[0].HOURS);
            if (wolInformation[0].BILLABLE === 'Y') {
                wolInformation[0].BILLABLE = 'Yes';
            } else {
                wolInformation[0].BILLABLE = 'No';
            }
            self.woDetails.WOL_BILLABLE(wolInformation[0].BILLABLE);

            self.woDetails.WOL_ACTION(wolInformation[0].LABOUR_ACTION);
            if (wolInformation[0].LABOUR_DATE == "" || wolInformation[0].LABOUR_DATE == "null" || wolInformation[0].LABOUR_DATE == "0001-01-01T00:00:00" || wolInformation[0].LABOUR_DATE == "1900-01-01T00:00:00") {

            } else {
                self.woDetails.WOL_LABOUR_DATE(ah.formatJSONDateTimeToString(wolInformation[0].LABOUR_DATE));

            }

        } else {
            $("#woLabourInfo").hide();
        }
        if (wopmPartsr.length > 0) {
            $("#woMaterialInfo").show();
            self.partsWLList.splice(0, 5000);
            $("#REQUEST_ID").val(wopmPartsr[0].REQUEST_ID);
            for (var o in wopmPartsr) {

                // if (wopmPartsr[o].DESCRIPTION !== null && wopmPartsr[o].DESCRIPTION !== "") {
                if (wopmPartsr[o].PRODUCT_DESC === null || wopmPartsr[o].PRODUCT_DESC === "") {
                    wopmPartsr[o].PRODUCT_DESC = wopmPartsr[o].DESCRIPTION;
                    wopmPartsr[o].ID_PARTS = 'Supplier'
                }
                self.partsWLList.push({
                    PRODUCT_DESC: wopmPartsr[o].PRODUCT_DESC,
                    DESCRIPTION: wopmPartsr[o].DESCRIPTION,
                    ID_PARTS: wopmPartsr[o].ID_PARTS,
                    QTY: wopmPartsr[o].QTY

                });
                // }



            }
        }
        else {
            $("#woMaterialInfo").hide();

        }
        self.loadDataPrint("");
        htmlstr += "</tr>";

        var bulkPrint = $("#BULKPRINT").val();
        if (bulkPrint !== "BULKPRINT") {
            setTimeout(function () { window.print(); }, 1000);
        } else {

            //var MyDiv1 = document.getElementById('printDetails');
            //var htmlstr = '';
            //htmlstr += "<tr>";
            //htmlstr += "<td>";
            //htmlstr += MyDiv1.innerHTML;
            //htmlstr += "</td id='tdcontiner'>";
            //htmlstr += "</tr>";
            //alert(htmlstr);
            //$(ah.config.id.printBulk + ' ' + ah.config.tag.tbody).append(htmlstr);
            //var lastReq = $("#LASTREQ").val();
            //alert(lastReq);
            //if (lastReq === "YES") {
            //    document.getElementById("printSummary").classList.remove('printSummary');
            //    document.getElementById("printDetails").classList.remove('printSummary');
            //    document.getElementById("printBulk").classList.add('printSummary');
            //    setTimeout( async function () {  window.print(); }, 1000);
            //}
        }
        //

    };
    self.printWO = function () {
        //document.getElementById("printDetails").classList.add('printDetails');


        window.print();

    }
    self.populateBulkPrint = function (jsonData) {

        var sr = jsonData.WORD_ORDER_LIST;
        $(ah.config.id.printBulk + ' ' + ah.config.tag.tbody).html('');
        if (sr.length > 0) {

            var clientInformation = jsonData.AM_CLIENT_INFO;

            self.woDetails.CLIENTNAME(clientInformation.DESCRIPTION);
            self.woDetails.CLIENTADDRESS(clientInformation.ADDRESS + ', ' + clientInformation.CITY + ', ' + clientInformation.ZIPCODE + ', ' + clientInformation.STATE);
            self.woDetails.CLIENTCONTACT('Tel.No: ' + clientInformation.CONTACT_NO1 + ' Fax No:' + clientInformation.CONTACT_NO2);

            var srcount = sr.length;
            var countloop = 0;
            var htmlstr = '';
            for (var o in sr) {
                countloop++;
                if (sr[o].ASSET_NO !== null) {
                    if (sr[o].ASSET_NO !== null && sr[o].ASSET_NO !== "") {

                        self.woDetails.ASSET_NO(sr[o].ASSET_NO);

                        self.woDetails.COST_CENTER(sr[o].COSTCENTER);
                        self.woDetails.DESCRIPTION(sr[o].ASSETDESCRIPTION);
                        self.woDetails.LOCATION(sr[o].LOCATION);
                        self.woDetails.DEVICE_TYPE(sr[o].DEVICETYPE);
                        self.woDetails.RESPONSIBLE_CENTER(sr[o].RESPCENTER);
                        self.woDetails.STATUS(sr[o].STATUS);
                        self.woDetails.MODEL_NO(sr[o].MODEL_NO);
                        self.woDetails.SERIAL_NO(sr[o].SERIAL_NO);
                        self.woDetails.WARRANTY_NOTES(sr[o].WARRANTY_NOTES);
                        self.woDetails.WARRANTY_INCLUDE(sr[o].WARRANTYINCLUDED);
                        self.woDetails.WARRANTY_EXPIRYDATE(ah.formatJSONDateToString(sr[o].WARRANTY_EXPIRYDATE));
                        self.woDetails.TERM_PERIOD(sr[o].TERM_PERIOD + ' ' + sr[o].WARRANTY_FREQUNIT);
                    }
                }
                if (sr[o].WO_STATUS === 'CL') {
                    var closedDate = ah.formatJSONDateToString(sr[o].CLOSED_DATE);
                    sr[o].WO_STATUS = 'Closed  ' + closedDate;
                } else if (sr[o].WO_STATUS = 'OP') {
                    sr[o].WO_STATUS = 'Open';
                }
                else if (sr[o].WO_STATUS = 'FA') {
                    sr[o].WO_STATUS = 'HE';
                } else if (sr[o].WO_STATUS = 'FA') {
                    sr[o].WO_STATUS = 'HELD';
                } else if (sr[o].WO_STATUS = 'NE') {
                    sr[o].WO_STATUS = 'NEW';
                } else if (sr[o].WO_STATUS = 'PS') {
                    sr[o].WO_STATUS = 'Posted';
                }

                self.woDetails.WO_STATUS(sr[o].WO_STATUS);
                self.woDetails.ASSIGN_TO(sr[o].STAFFNAME);

                self.woDetails.JOB_DUE_DATE(ah.formatJSONDateToString(sr[o].JOB_DUE_DATE));
                self.woDetails.JOB_TYPE(sr[o].JOBTYPEDESC);
                //self.woDetails.JOB_URGENCY(sr[o].STAFFNAME);
                self.woDetails.PROBLEM(sr[o].PROBLEM);
                self.woDetails.REQ_BY(sr[o].REQ_BY);
                self.woDetails.REQ_DATE(ah.formatJSONDateTimeToString(sr[o].REQ_DATE));
                self.woDetails.REQ_NO(sr[o].REQ_NO);
                self.woDetails.REQTICKETNO(sr[o].TICKET_ID);
                self.woDetails.REQ_TYPE(sr[o].PROBLEMTYPE);
                self.woDetails.SERV_DEPT(sr[o].SERVDEPARTMENT);

                var wolInformation = jsonData.AM_WORKORDER_LABOURLIST;

                var curwolInformation = wolInformation.filter(function (item) { return item.REQ_NO == sr[o].REQ_NO });


                if (curwolInformation.length > 0) {
                    $("#woLabourInfo").show();

                    if (curwolInformation[0].EMPLOYEE === null || curwolInformation[0].EMPLOYEE === "") {

                        self.woDetails.WOL_EMPLOYEE(curwolInformation[0].SUPPLIER);
                    } else {
                        self.woDetails.WOL_EMPLOYEE(curwolInformation[0].EMPLOYEE);
                    }
                    self.woDetails.WOL_HOURS(curwolInformation[0].HOURS);
                    if (curwolInformation[0].BILLABLE === 'Y') {
                        curwolInformation[0].BILLABLE = 'Yes';
                    } else {
                        curwolInformation[0].BILLABLE = 'No';
                    }
                    self.woDetails.WOL_BILLABLE(curwolInformation[0].BILLABLE);

                    self.woDetails.WOL_ACTION(curwolInformation[0].LABOUR_ACTION);
                    if (curwolInformation[0].LABOUR_DATE == "" || curwolInformation[0].LABOUR_DATE == "null" || curwolInformation[0].LABOUR_DATE == "0001-01-01T00:00:00" || curwolInformation[0].LABOUR_DATE == "1900-01-01T00:00:00") {

                    } else {
                        self.woDetails.WOL_LABOUR_DATE(ah.formatJSONDateTimeToString(curwolInformation[0].LABOUR_DATE));

                    }

                } else {
                    $("#woLabourInfo").hide();
                }
                var wopmPartsr = jsonData.AM_SPAREPARTSREQUEST;
                var curparts = wopmPartsr.filter(function (item) { return item.REF_WO == sr[o].REQ_NO });

                if (curparts.length > 0) {
                    $("#woMaterialInfo").show();
                    self.partsWLList.splice(0, 5000);

                    for (var o in curparts) {

                        if (curparts[o].PRODUCT_DESC === null || curparts[o].PRODUCT_DESC === "") {
                            curparts[o].PRODUCT_DESC = curparts[o].DESCRIPTION;
                            curparts[o].ID_PARTS = 'Supplier'
                        }
                        self.partsWLList.push({
                            PRODUCT_DESC: curparts[o].PRODUCT_DESC,
                            DESCRIPTION: curparts[o].DESCRIPTION,
                            ID_PARTS: curparts[o].ID_PARTS,
                            QTY: curparts[o].QTY

                        });



                    }
                }
                else {
                    $("#woMaterialInfo").hide();

                }


                var MyDiv1 = document.getElementById('printDetails');

                htmlstr += '<tr style="width: 850px; border-color:transparent;" class="page-break">';
                htmlstr += '<td style="width: 850px; border-color:transparent;">';
                htmlstr += MyDiv1.innerHTML;
                htmlstr += "</td>";
                htmlstr += "</tr>";


                if (countloop === srcount) {
                    //alert(htmlstr);
                    //alert('here');
                    $(ah.config.id.printBulk + ' ' + ah.config.tag.tbody).append(htmlstr);
                    document.getElementById("printSummary").classList.remove('printSummary');
                    document.getElementById("printDetails").classList.remove('printSummary');
                    document.getElementById("printBulk").classList.add('printSummary');
                    window.location.href = ah.config.url.modalClose;
                    setTimeout(function () { window.print(); }, 1000);
                }


            }



        }
    };
    self.populateAssetInfoById = function () {
        var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfoId).toString());

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        self.searchAssetFilter.assetNo = infoID.ASSET_NO;

        postData = { SEARCH: "", ASSET_NO: infoID.ASSET_NO, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readAssetInfoV, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.populateAssetInfo = function (jsonData) {

        var infoID = jsonData.AM_ASSET_DETAILS;//JSON.parse(this.getAttribute(ah.config.attr.datainfoId).toString());

        //  sessionStorage.setItem(ah.config.skey.assetItems, JSON.stringify(infoID));
        //alert(JSON.stringify(infoID));
        $("#ASSET_NO").val(infoID.ASSET_NO);
        $("#DESCRIPTION").val(infoID.DESCRIPTION);
        $("#CATEGORY").val(infoID.DTYPE_DESC);
        $("#SERIAL_NO").val(infoID.SERIAL_NO);
        $("#MODEL_NO").val(infoID.MODEL_NO);
        $("#BUILDING").val(infoID.BUILDING);
        $("#COST_CENTER").val(infoID.COSTCENTER_DESC);
        $("#LOCATION").val(infoID.LOCATION);
        $("#CONDITION").val(infoID.CONDITION);
        $("#ASSETSTATUS").val(infoID.STATUS);
        $("#ASSET_NO+a").next("span").remove();
        $("#ERECORD_GENERAL_NOTES").val(infoID.ERECORD_GENERAL_NOTES);
        $("#WARRANTY_EXPIRYDATE").val(ah.formatJSONDateTimeToString(infoID.WARRANTY_EXPIRYDATE));
        $("#TERM_PERIOD").val(infoID.TERM_PERIOD);
        $("#WARRANTY_INCLUDE").val(infoID.WARRANTY_DESC);
        $("#WARRANTY_NOTES").val(infoID.WARRANTY_NOTES);

        $("#SUN_AM").val(ah.formatJSONTimeToString(infoID.SUN_AM));
        $("#MON_AM").val(ah.formatJSONTimeToString(infoID.MON_AM));
        $("#TUE_AM").val(ah.formatJSONTimeToString(infoID.TUE_AM));
        $("#WED_AM").val(ah.formatJSONTimeToString(infoID.WED_AM));
        $("#THUR_AM").val(ah.formatJSONTimeToString(infoID.THUR_AM));
        $("#FRI_AM").val(ah.formatJSONTimeToString(infoID.FRI_AM));
        $("#SAT_AM").val(ah.formatJSONTimeToString(infoID.SAT_AM));
        $("#SUN_PM").val(ah.formatJSONTimeToString(infoID.SUN_PM));
        $("#MON_PM").val(ah.formatJSONTimeToString(infoID.MON_PM));
        $("#TUE_PM").val(ah.formatJSONTimeToString(infoID.TUE_PM));
        $("#WED_PM").val(ah.formatJSONTimeToString(infoID.WED_PM));
        $("#THUR_PM").val(ah.formatJSONTimeToString(infoID.THUR_PM));
        $("#FRI_PM").val(ah.formatJSONTimeToString(infoID.FRI_PM));
        $("#SAT_PM").val(ah.formatJSONTimeToString(infoID.SAT_PM));

    };
    self.statusUpdate = function () {
        var currStatus = $(ah.config.id.woStatus).val();
     
        if (currStatus === 'CL') {
            var partsConfirm = $("#PARTS_CONFIRMED").val();
            if (partsConfirm !== "Y" && sparePartsCount > 0) {
                alert('Unable to proceed. Please make sure that the spare parts requested QTY is Confirmed.');
                var currStatus = $("#CURRSTATUS").val();
                $("#WO_STATUS").val(currStatus);
                return;
            }
           
            $("#CLOSED_DATE").show();
            $("#lblClosedDate").show();
            $(ah.config.cls.liSave).show();
            var jDate = new Date();
            var strDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);

            var closedDate = strDate + ' ' + hourmin;

            $(ah.config.id.closeDate).val(closedDate);
        } else {
            $(ah.config.id.closeDate).val(null);
            $("#CLOSED_DATE").hide();
            $("#lblClosedDate").hide();
            $(ah.config.cls.liSave).hide();
        }


    };
    self.readSetupID = function () {

        var reqID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();
        $("#REQ_ID").show();
        $("label[for='REQ_ID']").show();
        postData = { REQ_NO: reqID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.readAssetNo = function (assetNo) {


        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;


        postData = { ASSET_NO: assetNo, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readAssetNo, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.readWOId = function (refID) {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { REQ_NO: refID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.assetInfo = function () {
        assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);

        window.location.href = encodeURI(ah.config.url.aimsInformation + "?ASSETNO=" + assetNoID);
    };
    self.assetInfoDet = function () {
        assetNoID = $("#ASSET_NO").val();
        var reqNo = $("#REQ_NO").val();
        var dashBoard = ah.getParameterValueByName(ah.config.fld.fromDashBoard);
        var sdfsdf = "";

        if (dashBoard == "dsdxsesesdfsadfsadfqwerqwerqwersadfsadfasdfasdfsadfsadfsadfasdf") {
            sdfsdf = "dsdxsesesdfsadfsadfqwerqwerqwersadfsadfasdfasdfsadfsadfsadfasdf";
        }
        window.location.href = encodeURI(ah.config.url.aimsInformation + "?ASSETNO=" + assetNoID + "&REQNO=" + reqNo + "&asdfxhsadfsdfx=" + sdfsdf);
    };
    self.readWSId = function () {


        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        var WSId = $("#AM_INSPECTION_ID").val();
        var isValid = !isNaN(parseFloat(WSId)) && isFinite(WSId);
        if (isValid === false) {
            $("#AM_INSPECTION_ID").val(null);
            alert('Please enter a valid Work Sheet Inspection code to proceed.');
            return;
        }
        postData = { AM_INSPECTION_ID: WSId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readWSInspection, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.searchNow = function () {
        //$("#assetInfoDet").hide();
        var reqNO = assetNoID = ah.getParameterValueByName(ah.config.fld.reqNo);
        var assetNoIDd = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        var dsdfx = ah.getParameterValueByName(ah.config.fld.reqWOAction);
        if (assetNoIDd !== null && assetNoIDd !== "") {

        } else {
            urlText = 'ljllk';
            history.pushState('', 'New Page Title', ah.config.url.aimsWOSearch);
        }

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();
        assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);


        var fromDate = $('#requestFromDate').val().split('/');
        if (fromDate.length == 3) {
            self.searchFilter.fromDate(fromDate[2] + "/" + fromDate[1] + "/" + fromDate[0]);
        }
        var toDate = $('#requestStartDate').val().split('/');
        if (toDate.length == 3) {
            self.searchFilter.toDate(toDate[2] + "/" + toDate[1] + "/" + toDate[0] + "  23:00:00.000");
        }

        var staffId = "";
        //if (self.validatePrivileges('24') === 0) {            
        //        self.searchFilter.status = "FC";
        //    }
       

        postData = { CLIENT_ID: staffDetails.CLIENT_CODE, SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), ASSETNO: assetNoID, STAFF_ID: staffId, STAFF_LOGON_SESSIONS: staffLogonSessions, SEARCH_FILTER: self.searchFilter, STATUS: "" };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.searchContractNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;


        var assetNo = $("#ASSET_NO").val();
        if (assetNo === null || assetNo === "") {
            alert('Unable to proceed.Asset must be provided.')
            return;
        }
        ah.CurrentMode = ah.config.mode.search;
        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), ASSETNO: assetNo, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchContract, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.searchStatusLogNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        var reqId = $("#REQ_NO").val();
        ah.CurrentMode = ah.config.mode.search;
        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), REQ_NO: reqId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchWOStatus, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.UploadImage = function (jsonData) {
        var scanInfo = jsonData.SCANDOCS;

        var fileNameInfo = scanInfo.FILE_NAME;
        var frmData = new FormData();
        var filebase = $("#pdffile").get(0);
        var files = filebase.files;
        frmData.append("docFilename", fileNameInfo);
        frmData.append(files[0].name, files[0]);
        $.ajax({
            url: '/Upload/SaveDocument',
            type: "POST",
            contentType: false,
            processData: false,
            data: frmData,
            success: function (data) {
                alert('Success Upload');
            },
            error: function (err) {
                alert(error);
            }
        });

        var sr = srDataArray;

        $(ah.config.id.supportingDocs + ' ' + ah.config.tag.ul).html('');
        if (sr.length > 0) {

            var htmlstr = '';

            for (var o in sr) {

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderDOCTemplateList, sr[o].FILE_NAME, sr[o].FILE_NAME, '', '');

                htmlstr += '</li>';

            }

            $(ah.config.id.supportingDocs + ' ' + ah.config.tag.ul).append(htmlstr);
            $(ah.config.cls.detailItem).bind('click', self.readDoc);
        }
    };
    self.validatePrivileges = function (privilegeId) {
        var groupPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));

        var groupPrivileges = groupPrivileges.filter(function (item) { return item.PRIVILEGES_ID === privilegeId });
        return groupPrivileges.length;
    };
    self.saveInfo = function () {
        $(ah.config.id.woStatus).prop(ah.config.attr.disabled, false);
        $("#REQ_ID").show();
        $("label[for='REQ_ID']").show();
        var wsId = $("#AM_INSPECTION_ID").val();
        if (wsId > 0) {

        } else {
            if ($("#ASSET_NO").val() == "") {
                $("#ASSET_NO+a").next("span").remove();
                $("#ASSET_NO+a").after("<span style='color:red;padding-left:5px'>* This field is required</span>");
                return;
            }
        }


        var saveAction = $("#SAVEACTION").val();

        if (saveAction != 'SAVEINFO') {
            return;
        }
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var setupDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;


        if (setupDetails.WO_STATUS == "CL" && (setupDetails.ASSIGN_TO == null || setupDetails.ASSIGN_TO == "")) {

            alert('Unable to proceed. Please select Assign To...');
            return;
        }
        if (setupDetails.RISK_INCLUSIONFACTOR === null || setupDetails.RISK_INCLUSIONFACTOR === "") {
            setupDetails.RISK_INCLUSIONFACTOR = 0;
        }

        if (setupDetails.JOB_DUE_DATE == "" || setupDetails.JOB_DUE_DATE == "null" || setupDetails.JOB_DUE_DATE == "00/00/0000" || setupDetails.JOB_DUE_DATE == "00-00-0000") {
            setupDetails.JOB_DUE_DATE = "01/01/1900";
        }

        if (setupDetails.AM_INSPECTION_ID === "" || setupDetails.AM_INSPECTION_ID === null) {
            setupDetails.AM_INSPECTION_ID = 0;
        };

        var strTime = '';
        var apptDate = setupDetails.REQ_DATE;


        var appointmentDate = apptDate.substring(6, 10) + '-' + apptDate.substring(3, 5) + '-' + apptDate.substring(0, 2);
        var apptDateStr = appointmentDate;
        strTime = String(apptDate).split(' ');
        strTime = strTime[1].substring(0, 5);
        appointmentDate = appointmentDate + ' ' + strTime;
        setupDetails.REQ_DATE = appointmentDate;
        if (setupDetails.TICKET_ID === null || setupDetails.TICKET_ID === "") {
            setupDetails.TICKET_ID = 0;
        }
        if (setupDetails.PURCHASE_COST === null || setupDetails.PURCHASE_COST === "") {
            setupDetails.PURCHASE_COST = 0;
        }
        //(setupDetails.REQ_NO);
        sparePartsCount = 0;
        if ((ah.CurrentMode == ah.config.mode.add || ah.CurrentMode == ah.config.mode.display) && (setupDetails.REQ_NO === null || setupDetails.REQ_NO === "")) {
            setupDetails.CREATED_BY = staffLogonSessions.STAFF_ID;
            var jDate = new Date();
            var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);

            setupDetails.CREATED_DATE = strDateStart + ' ' + hourmin;
            postData = { AM_WORK_ORDERS: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            ah.toggleSaveMode();
            //alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.createSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {

            


            var jDate = new Date();
            var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);




            var modifyBy = staffLogonSessions.STAFF_ID;
            var modifyDate = strDateStart + ' ' + hourmin;
            if (setupDetails.CLOSED_DATE !== null && setupDetails.CLOSED_DATE !== "") {
                var dateNow = self.getDateNow();
                var checkDate = self.isDateFutre(dateNow,moment(setupDetails.CLOSED_DATE, 'DD/MM/YYYY hh:mm:ss'));       
                if (checkDate) { 
                    alert('Unable ro proceed. Close date cannot be future date.')
                    return;
                }

                //check the reqest Date
                var reqDate = $("#REQ_DATE").val();
                var checkreqDate = self.isDateFutre(moment(setupDetails.CLOSED_DATE, 'DD/MM/YYYY hh:mm:ss'),moment(reqDate, 'DD/MM/YYYY hh:mm:ss'));    
                if (checkreqDate) {
                    alert('Unable ro proceed. Close date cannot be less than Requested Date')
                    return;
                }
                var closeTime = setupDetails.CLOSED_DATE;
                var closeDate = setupDetails.CLOSED_DATE;
               
                closeDate = closeDate.substring(6, 10) + '-' + closeDate.substring(3, 5) + '-' + closeDate.substring(0, 2);
                
                strTime = String(closeTime).split(' ');
                strTime = strTime[1].substring(0, 5);
                setupDetails.CLOSED_DATE = closeDate + ' ' + strTime;
            }


            ah.toggleSaveMode();

            postData = { MODIFY_BY: modifyBy, MODIFY_DATE: modifyDate, AM_WORK_ORDERS: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            //   alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.updateSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }

    };

    self.webApiCallbackStatus = function (jsonData) {
        //alert(JSON.stringify(jsonData));
        //alert(ah.CurrentMode);
        if (jsonData.ERROR) {
            if (jsonData.ERROR.ErrorText === "Work Sheet not found.") {

                $("#AM_INSPECTION_ID").val(null);
                alert(jsonData.ERROR.ErrorText);
            } else {
                alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            }

            // window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.AM_WORKORDER_LABOURLIST || jsonData.AM_SPAREPARTSREQUEST || jsonData.WORD_ORDER_LIST || jsonData.AM_WORK_ORDERS_STATUS_LOG || jsonData.SCANDOCS_CATEGORYIMAGELIST || jsonData.SCANDOCS || jsonData.AM_WORKORDER_LABOUR || jsonData.AM_CLIENT_INFO || jsonData.AM_PARTSREQUEST || jsonData.AM_WORK_ORDER_REFNOTES || jsonData.AM_ASSET_NOTES || jsonData.AM_PROCEDURES || jsonData.AM_HELPDESKLIST || jsonData.AM_INSPECTION || jsonData.STAFFLIST || jsonData.AM_ASSET_CONTRACTSERVICES || jsonData.LOV_LOOKUPS || jsonData.AM_WORK_ORDERS || jsonData.AM_ASSET_DETAILS || jsonData.STAFF || jsonData.SUCCESS) {
           
            if (ah.CurrentMode == ah.config.mode.save) {
                // alert(JSON.stringify(jsonData));
                if (jsonData.AM_WORK_ORDERS) {
                    $("#ASSET_NO+a").next("span").remove();
                    sessionStorage.setItem(ah.config.skey.woDetails, JSON.stringify(jsonData.AM_WORK_ORDERS));
                }
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();

                if (jsonData.SUCCESS) {
                    self.showDetails(false);
                } else if (jsonData.SCANDOCS) {
                    ah.toggleDisplayMode();
                    srDataArray = jsonData.SCANDOCS_CATEGORYIMAGELIST;
                    self.UploadImage(jsonData);

                }
                else {
                    self.showDetails(true);
                }
            }
            else if (ah.CurrentMode == ah.config.mode.display || ah.CurrentMode == ah.config.mode.edit) {
                if (jsonData.AM_ASSET_NOTES) {
                    self.searchResultNotes(jsonData);
                } else if (jsonData.AM_WORK_ORDER_REFNOTES) {
                    self.searchResultWONotes(jsonData);

                } else if (jsonData.AM_ASSET_DETAILS) {
                    self.populateAssetInfo(jsonData);

                } else if (jsonData.AM_PARTSREQUEST) {
                    //alert(jsonData.AM_CLIENT_INFO.CLIENT_LOGO);
                    document.getElementById("CLIENT_LOGO").src = jsonData.AM_CLIENT_INFO.CLIENT_LOGO;
                    self.searchWoMaterialResult(jsonData);

                }
            }
            else if (ah.CurrentMode == ah.config.mode.searchResult) {
                if (jsonData.AM_PARTSREQUEST) {
                    self.searchPartsOnhandResult(jsonData);
                } else if (jsonData.WORD_ORDER_LIST) {
                    document.getElementById("CLIENT_LOGO").src = jsonData.AM_CLIENT_INFO.CLIENT_LOGO;
                    self.populateBulkPrint(jsonData);


                }

            }


            else if (ah.CurrentMode == ah.config.mode.add) {
                if (jsonData.AM_INSPECTION) {
                    // $("#AM_INSPECTION_DESC").val(jsonData.AM_INSPECTION.AM_INSPECTION_ID + '-' + jsonData.AM_INSPECTION.INSPECTION_TYPE);
                    $("#AM_INSPECTION_ID").val(jsonData.AM_INSPECTION.AM_INSPECTION_ID);
                } else {
                    sessionStorage.setItem(ah.config.skey.assetItems, JSON.stringify(jsonData.AM_ASSET_DETAILS));
                    self.showAssetDetails(jsonData);
                }

            }
            else if (ah.CurrentMode == ah.config.mode.edit) {
                if (jsonData.AM_INSPECTION) {
                    // $("#AM_INSPECTION_DESC").val(jsonData.AM_INSPECTION.AM_INSPECTION_ID + '-' + jsonData.AM_INSPECTION.INSPECTION_TYPE);
                    $("#AM_INSPECTION_ID").val(jsonData.AM_INSPECTION.AM_INSPECTION_ID);
                }

            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                sessionStorage.setItem(ah.config.skey.woDetails, JSON.stringify(jsonData.AM_WORK_ORDERS));
                sessionStorage.setItem(ah.config.skey.assetItems, JSON.stringify(jsonData.AM_ASSET_DETAILS));
                srDataArray = jsonData.SCANDOCS_CATEGORYIMAGELIST;
                self.showDetails(true, jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.search) {

                if (jsonData.AM_ASSET_DETAILS) {
                    sessionStorage.setItem(ah.config.skey.assetList, JSON.stringify(jsonData.AM_ASSET_DETAILS));
                    $("#PaginGloadNumber").text("1");
                    $("#PaginGpageCount").val("1");
                    $("#PaginGpageNum").val(0);

                    self.searchItemsResult("", 0);
                } else if (jsonData.AM_ASSET_CONTRACTSERVICES) {

                    self.searchContractResult(jsonData);
                } else if (jsonData.AM_WORK_ORDERS_STATUS_LOG) {
                    //alert(JSON.stringify(jsonData));
                    self.searchResultWOStatus(jsonData);
                } else if (jsonData.AM_HELPDESKLIST) {
                    self.searchResultHelpDesk(jsonData);
                } else {
                    clientInfo = jsonData.AM_CLIENT_INFO;
                    document.getElementById("CLIENT_LOGO_SUM").src = jsonData.AM_CLIENT_INFO.CLIENT_LOGO;
                    sessionStorage.setItem(ah.config.skey.woDetails, JSON.stringify(jsonData.AM_WORK_ORDERS));
                    self.searchResult(0);
                }

            }
            else if (ah.CurrentMode == ah.config.mode.idle) {
                // alert(JSON.stringify(jsonData.LOV_LOOKUPS));
                var serviceDept = jsonData.LOV_LOOKUPS;
                serviceDept = serviceDept.filter(function (item) { return item.CATEGORY === 'AIMS_SERVICEDEPARTMENT' });
                // alert(JSON.stringify(serviceDept));
                $.each(serviceDept, function (item) {

                    $(ah.config.id.servDept)
                        .append($("<option></option>")
                            .attr("value", serviceDept[item].LOV_LOOKUP_ID)
                            .text(serviceDept[item].DESCRIPTION));
                });

                $.each(jsonData.STAFFLIST, function (item) {

                    $(ah.config.id.staffAssignTo)
                        .append($("<option></option>")
                            .attr("value", jsonData.STAFFLIST[item].STAFF_ID)
                            .text(jsonData.STAFFLIST[item].FIRST_NAME + ' ' + jsonData.STAFFLIST[item].LAST_NAME));
                });

                $.each(jsonData.STAFFLIST, function (item) {

                    $(ah.config.id.filterAssignTo)
                        .append($("<option></option>")
                            .attr("value", jsonData.STAFFLIST[item].STAFF_ID)
                            .text(jsonData.STAFFLIST[item].FIRST_NAME + ' ' + jsonData.STAFFLIST[item].LAST_NAME));
                });


                var aimsJobType = jsonData.LOV_LOOKUPS;
                aimsJobType = aimsJobType.filter(function (item) { return item.CATEGORY === 'AM_JOBTYPE' });
                $.each(aimsJobType, function (item) {
                    $(ah.config.id.jobType)
                        .append($("<option></option>")
                            .attr("value", aimsJobType[item].LOV_LOOKUP_ID)
                            .text(aimsJobType[item].DESCRIPTION));
                });
                $.each(aimsJobType, function (item) {
                    $(ah.config.id.filterJobType)
                        .append($("<option></option>")
                            .attr("value", aimsJobType[item].LOV_LOOKUP_ID)
                            .text(aimsJobType[item].DESCRIPTION));
                });




                var aimsPriority = jsonData.LOV_LOOKUPS;
                aimsPriority = aimsPriority.filter(function (item) { return item.CATEGORY === 'AIMS_PRIORITY' });

                $.each(aimsPriority, function (item) {

                    $(ah.config.id.woPriority)
                        .append($("<option></option>")
                            .attr("value", aimsPriority[item].LOV_LOOKUP_ID)
                            .text(aimsPriority[item].DESCRIPTION));

                    $("#filterPriority")
                        .append($("<option></option>")
                            .attr("value", aimsPriority[item].LOV_LOOKUP_ID)
                            .text(aimsPriority[item].DESCRIPTION));
                });

                var aimsProblemType = jsonData.LOV_LOOKUPS;
                //alert(JSON.stringify(aimsProblemType));
                aimsProblemType = aimsProblemType.filter(function (item) { return item.CATEGORY === 'AIMS_PROBLEMTYPE' });

                $.each(aimsProblemType, function (item) {

                    $(ah.config.id.woProblemType)
                        .append($("<option></option>")
                            .attr("value", aimsProblemType[item].LOV_LOOKUP_ID)
                            .text(aimsProblemType[item].DESCRIPTION));
                    $("#filterProblemType")
                        .append($("<option></option>")
                            .attr("value", aimsProblemType[item].LOV_LOOKUP_ID)
                            .text(aimsProblemType[item].DESCRIPTION));
                });
                var aimsSpecialty = jsonData.LOV_LOOKUPS;
                aimsSpecialty = aimsSpecialty.filter(function (item) { return item.CATEGORY === 'AIMS_SPECIALTY' });

                $.each(aimsSpecialty, function (item) {

                    $(ah.config.id.woSpecialty)
                        .append($("<option></option>")
                            .attr("value", aimsSpecialty[item].LOV_LOOKUP_ID)
                            .text(aimsSpecialty[item].DESCRIPTION));
                });

                assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
                assetDescription = ah.getParameterValueByName(ah.config.fld.assetDescInfo);
                if (assetNoID !== null && assetNoID !== "") {
                    $("#infoID").text(assetNoID + "-" + assetDescription);
                    $("#assetInfobtn").show();
                    //$("#assetInfoDet").hide();
                    self.searchNow();
                }

                var reqWoNO = ah.getParameterValueByName(ah.config.fld.reqNo);
                var dashBoard = ah.getParameterValueByName(ah.config.fld.fromDashBoard);
              
                if (reqWoNO !== null && reqWoNO !== "") {
                    if (dashBoard == "dsdxsesesdfsadfsadfqwerqwerqwersadfsadfasdfasdfsadfsadfsadfasdf") {
                        $(ah.config.cls.liSearch).hide();
                    }
                   
                    self.readWOId(reqWoNO);

                }
                var newWO = ah.getParameterValueByName(ah.config.fld.newWorkOrder);

                if (newWO === 'NEW') {

                    self.createNew();
                }

                if (self.validatePrivileges('6') === 0) {
                    $(ah.config.cls.liAddNew).hide();
                };
                $("#filterStatus").val('FC');
                self.searchFilter.status = 'FC';
            }

        }
        else {

            alert(systemText.errorTwo);
            //window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};