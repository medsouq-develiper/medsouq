﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divStaffUserID: '.divStaffUserID',
            statusText: '.status-text',
			staffuserDetailItem: '.staffuserDetailItem',
            detailItem: '.detailItem',
            staffuserStoreItem: '.staffuserStoreItem'
			
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email],input[type=password]',
            ul: 'ul'
		},
		tagId: {
			btnCostCenter: 'btnCostCenter',
			btnBuilding: 'btnBuilding',
			btnStore: 'btnStore'
		},
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
			edit: 7,
			updateModal:8
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveStaffUser: '#saveStaffUser',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmStaffUser: '#frmStaffUser',
            staffuserID: '#STAFF_ID',
            staffgroupsID: '#STAFF_GROUP_ID',
            staffpositionID: '#STAFF_POSITION_ID',
			successMessage: '#successMessage',
            searchResultLov: '#searchResult1',
			lovHeader: '#lovHeader',
			uCategoryId: '#UCATEGORYID',
            assignedHeader: '#assignedHeader',
            staffLocationId: '#LOCATION',
            staffSrv: '#SRVC_DEPT',
            searchResultLookUp: '#searchResultLookUp',
            searchResulStore: '#searchResulStore'
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
			dataStaffUserID: 'data-StaffUserID',
            datainfoID: 'data-infoID',
            datauserStoreID: 'data-userStoreID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            staffuserDetails: 'STAFF_USER',
			searchResult: 'searchResult',
			storeList: 'STORES',
			staffStore: 'STAFF_STORE'
        },
        url: {
            homeIndex: '/Home/Index',
			aimsHomepage: '/Menuapps/aimsIndex',
			othSetup: '#openOthSetup',
            modalClose: '#',
            modalLookUp: '#openModalLookup',
            modalStore: '#openModalStore'
        },
        api: {
            getLookupList: '/Staff/GetStaffUserLookUpTables',
            readStaffUser: '/Staff/ReadStaffUser',
            searchAll: '/Search/SearchStaffUser',
            createStaffUser: '/Staff/CreateStaffUser',
			updateStaffUser: '/Staff/UpdateStaffUserDetails',
			searchStaffStore: '/Staff/SearchStaffStore',
			createStaffStore: '/Staff/CreateNewStaffStore',
			searchStaffCB: '/Staff/SearchStaffCB',
			createStaffCB: '/Staff/CreateNewStaffCB',
			deleteStaffCB: '/Staff/DeleteStaffCB'

        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal staffuserDetailItem' data-staffuserID='{0}'><i class='fa fa-arrow-circle-o-right' id='menu'></i> </i>&nbsp;&nbsp;{1} - {2} {3}</a>",
			searchRowTemplate: '<span class="search-row-label fc-light-grey-2">{0}: <span class="search-row-data">{1}</span></span>',
            searchRowHeaderTemplateList: "<a href='#openOthSetup' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-left'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            searchRowHeaderTemplateStore: "<a href='#' class='fs-medium-normal staffuserStoreItem' data-userStoreID='{0}'><i class='fa fa-arrow-circle-o-right' id='menu'></i> </i>&nbsp;&nbsp;{1} - {2} {3}</a>",
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divStaffUserID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

		thisApp.CurrentMode = thisApp.config.mode.idle;
		var modal = document.getElementById('openModalAlert');
		if (modal !== null)
			modal.style.display = "none";

    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divStaffUserID
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

	};
	thisApp.toggleReturnAddMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.add;

		
		$(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
		$(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

		$(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

		$(
			thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
			thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divStaffUserID
		).hide();


		$(
			thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
			thisApp.config.tag.textArea
		).removeClass(thisApp.config.cssCls.viewMode);

		$(
			thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
		).prop(thisApp.config.attr.disabled, false);

		$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

	
		$(thisApp.config.cls.divProgress).hide();
		$(thisApp.config.cls.liApiStatus).hide();

	};

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

		$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
		document.getElementById("STAFF_ID").readOnly = true;
		document.getElementById("USER_ID").readOnly = true;

    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divStaffUserID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

        $(
           thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
           thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.getAge = function (birthDate) {

        var seconds = Math.abs(new Date() - birthDate) / 1000;

        var numyears = Math.floor(seconds / 31536000);
        var numdays = Math.floor((seconds % 31536000) / 86400);
        var nummonths = numdays / 30;

        return numyears + " y " + Math.round(nummonths) + " m ";
    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.formatJSONDateTimeToString = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0] + ' ' + strTime[1].substring(0, 5);


        return dateToFormat;
    };

    thisApp.formatJSONDateToString = function () {

        var dateToFormat = arguments[0];
        var strDate;

        if (dateToFormat === null) return "-";
        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');


            if (isDateTime) {
                val = thisApp.formatJSONDateTimeToString(val);
            }

            if (isDate) {
                val = thisApp.formatJSONDateToString(val);
            }

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var staffuserViewModel = function (systemText) {

    var self = this;
    self.othList = ko.observableArray("");
    var CostCenterList = ko.observableArray([]);
    var ah = new appHelper(systemText);
    var modal = document.getElementById('openModalAlert');
    self.modelsList = ko.observableArray("");
    var userBuildingLOV = ko.observableArray([]);
    var userBuildingList = ko.observableArray([]);

    self.userStore = ko.observableArray("");
    var userStoreLOV = ko.observableArray([]);
    var userStoreList = ko.observableArray([]);

    window.onload = function () {
        var options =
        {
            imageBox: '.imageBox',
            thumbBox: '.thumbBox',
            spinner: '.spinner',
            imgSrc: '/images/nophotok.png'
        }
        var cropper = new cropbox(options);
        document.querySelector('#file').addEventListener('change', function () {
            var reader = new FileReader();
            reader.onload = function (e) {
                options.imgSrc = e.target.result;
                cropper = new cropbox(options);
            }
            reader.readAsDataURL(this.files[0]);
            this.files = [];
        })
        document.querySelector('#btnCrop').addEventListener('click', function () {
            var img = cropper.getDataURL();
            document.querySelector('.cropped').innerHTML = '<a href="#openModal"><img id="PHOTO_IMAGE" src="' + img + '" width = 100px height = 100px></a>';
        })
        document.querySelector('#btnZoomIn').addEventListener('click', function () {
            cropper.zoomIn();
        })
        document.querySelector('#btnZoomOut').addEventListener('click', function () {
            cropper.zoomOut();
        })
    };
	self.infoList = ko.observableArray("");
    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.toggleReadMode();

        postData = { STAFF_LOGON_SESSIONS: staffLogonSessions };
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        $.post(api + ah.config.api.getLookupList, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

        ah.initializeLayout();

    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.returnHome = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.createNewStaffUser = function () {

        ah.toggleAddMode();

    };
	self.alertMessage = function (refMessage) {
		$("#alertmessage").text(refMessage);
		var modal = document.getElementById('openModalAlert');
		modal.style.display = "block";

	};
	self.closeAlertModal = function () {
		modal.style.display = "none"
	}
    self.modifyStaffUserDetails = function () {

        ah.toggleEditMode();

    };
    self.cropImage = function () {
        

    };
	self.othSetup = function () {
		var senderID = $(arguments[1].currentTarget).attr('id');

		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var postData;
		self.infoList = ko.observableArray("");
		var staffID = $("#STAFF_ID").val();
		switch (senderID) {

			case ah.config.tagId.btnCostCenter: {
				$(ah.config.id.uCategoryId).val("AIMS_COSTCENTER");
				$(ah.config.id.lovHeader).text("Select from list of Cost Center");
				$(ah.config.id.assignedHeader).text("User Assigned Cost Center");
				postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), STAFF_ID: staffID, UCATEGORY:"AIMS_COSTCENTER", STAFF_LOGON_SESSIONS: staffLogonSessions };
				$.post(self.getApi() + ah.config.api.searchStaffCB, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
				window.location.href = ah.config.url.othSetup;
				break;
			}
			case ah.config.tagId.btnBuilding: {
				$(ah.config.id.uCategoryId).val("AIMS_BUILDING");
				$(ah.config.id.lovHeader).text("Select from list of Building");
				$(ah.config.id.assignedHeader).text("User Assigned Building");
				postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), STAFF_ID: staffID, UCATEGORY: "AIMS_BUILDING", STAFF_LOGON_SESSIONS: staffLogonSessions };
				$.post(self.getApi() + ah.config.api.searchStaffCB, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
				window.location.href = ah.config.url.othSetup;
				break;
			}
			case ah.config.tagId.btnStore: {
				postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), STAFF_ID: staffID, STAFF_LOGON_SESSIONS: staffLogonSessions };
				$.post(self.getApi() + ah.config.api.searchStaffStore, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

				break;
			}
		}
	};
	self.removeItem = function (othList) {
		self.othList.remove(othList);
	};
	self.searchResultCostCenter = function (jsonData) {
        var sr = jsonData.LOV_LOOKUPS;
        CostCenterList = jsonData.LOV_LOOKUPS;
		var stcb = jsonData.STAFF_COSTCENTER_BUILDING
		
		$(ah.config.id.searchResultLov + ' ' + ah.config.tag.ul).html('');
		if (sr.length > 0) {
			var htmlstr = '';
			for (var o in sr) {
				
				htmlstr += '<li>';
				htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplateList, JSON.stringify(sr[o]), '', sr[o].DESCRIPTION, '');
				

				htmlstr += '</li>';
			
			}
			
			$(ah.config.id.searchResultLov + ' ' + ah.config.tag.ul).append(htmlstr);
		}
		self.othList.splice(0, 5000);
		if (stcb.length > 0) {
			
			for (var o in stcb) {
				self.othList.push({
					DESCRIPTION: stcb[o].DESCRIPTION,
					LOV_LOOKUP_ID: stcb[o].CODE,
					UCATEGORY: stcb[o].UCATEGORY,
					STAFFCODE: stcb[o].STAFFCODE,
					CREATED_BY: stcb[o].CREATED_BY,
					CREATED_DATE: "",
					CODE: stcb[o].CODE
				});
			

			}
		}

		$(ah.config.cls.detailItem).bind('click', self.populateData);
		
	};
	self.populateData = function () {
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfoID));
		var existLovId = self.checkExist(infoID.LOV_LOOKUP_ID);
		if (existLovId > 0) {
			alert('Unable to Proceed. It is already assigned to user.');
			return;
		}
		var uCategoryID = $(ah.config.id.uCategoryId).val();
		var staffId = $("#STAFF_ID").val();

		var jDate = new Date();
		var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
		function time_format(d) {
			hours = format_two_digits(d.getHours());
			minutes = format_two_digits(d.getMinutes());
			return hours + ":" + minutes;
		}
		function format_two_digits(n) {
			return n < 10 ? '0' + n : n;
		}
		var hourmin = time_format(jDate);
		var createDate = strDateStart + ' ' + hourmin;

		self.othList.push({
			DESCRIPTION: infoID.DESCRIPTION,			
			LOV_LOOKUP_ID: infoID.LOV_LOOKUP_ID,
			UCATEGORY: uCategoryID,
			STAFFCODE: staffId,
			CREATED_BY: staffLogonSessions.STAFF_ID,
			CREATED_DATE: createDate,
			CODE: infoID.LOV_LOOKUP_ID
		});
	};

    self.assignModel = function () {

        var infoID = JSON.parse(this.getAttribute(ah.config.attr.dataStaffUserID).toString());

        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.modelsList);
        var partLists = JSON.parse(jsonObj);

        var buildingExist = partLists.filter(function (item) { return item.BUILDING_CODE === infoID.LOV_LOOKUP_ID });

        if (buildingExist.length > 0) {
            alert('Unable to Proceed. The selected building was already added.');
            return;
        };

        self.modelsList.push({
            BUILDING_CODE: infoID.LOV_LOOKUP_ID,
            DESCRIPTION: infoID.DESCRIPTION

        });

        return;
    };
    self.addModels = function () {
        window.location.href = ah.config.url.modalLookUp;
    };
    self.removeItem = function (srmodels) {
        self.modelsList.remove(srmodels);
    }


    self.assignStore = function () {

        var infoID = JSON.parse(this.getAttribute(ah.config.attr.datauserStoreID).toString());

        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.userStore);
        var partLists = JSON.parse(jsonObj);

        var storeExist = partLists.filter(function (item) { return item.STORE_ID === infoID.STORE_ID });

        if (storeExist.length > 0) {
            alert('Unable to Proceed. The selected store was already added.');
            return;
        };

        self.userStore.push({
            STORE_ID: infoID.STORE_ID,
            DESCRIPTION: infoID.DESCRIPTION

        });

        return;
    };
    self.addModelStore = function () {
        window.location.href = ah.config.url.modalStore;
    };
    self.removeUserStoreItem = function (srmodels) {
        self.userStore.remove(srmodels);
    }


	self.checkExist = function (lovId) {

		var jsonObj = [];
		jsonObj = ko.utils.stringifyJson(self.othList);
		var infoLists = JSON.parse(jsonObj);

		var infoLists = infoLists.filter(function (item) { return item.LOV_LOOKUP_ID === lovId });

		return infoLists.length;
	};
	self.searchStaffStoreResult = function (LovCategory, pageLoad) {

		var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.partsOnhandList));

		if (LovCategory !== null && LovCategory !== "" && LovCategory !== "LOAD") {
			var arr = sr;
			var keywords = LovCategory.toUpperCase();
			var filtered = [];

			for (var i = 0; i < sr.length; i++) {
				for (var j = 0; j < keywords.length; j++) {
					if ((sr[i].PRODUCT_CODE.toUpperCase()).indexOf(keywords) > -1 || (sr[i].PRODUCT_DESC.toUpperCase()).indexOf(keywords) > -1 || (sr[i].STORE_DESC.toUpperCase()).indexOf(keywords) > -1) {
						filtered.push(sr[i]);
						break;
					}
				}
			}
			sr = filtered;
		}

		$(ah.config.id.searchResultPartsOnhand + ' ' + ah.config.tag.ul).html('');



	

	};
    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.saveAllChanges = function () {

        $(ah.config.id.saveStaffUser).trigger('click');

    };
    self.removeAllCostCenter = function () {
        self.othList.splice(0, 5000);
    };
    self.addAllCostCenter = function () {
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        
        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.othList);
        var infoLists = JSON.parse(jsonObj);

        var uCategoryID = $(ah.config.id.uCategoryId).val();
        var staffId = $("#STAFF_ID").val();

        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);
        var createDate = strDateStart + ' ' + hourmin;
        
        sr = CostCenterList;

        if (sr.length > 0) {

            for (var o in sr) {
                var existLovId = self.checkExist(sr[o].LOV_LOOKUP_ID);
              
                if (existLovId == 0) {
                    self.othList.push({
                        DESCRIPTION: sr[o].DESCRIPTION,
                        LOV_LOOKUP_ID: sr[o].LOV_LOOKUP_ID,
                        UCATEGORY: uCategoryID,
                        STAFFCODE: staffId,
                        CREATED_BY: staffLogonSessions.STAFF_ID,
                        CREATED_DATE: createDate,
                        CODE: sr[o].LOV_LOOKUP_ID
                    });
                }
                


            }
        }

    };
    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            self.showStaffUserDetails(true);
        }

    };

    self.showStaffUserDetails = function (isNotUpdate) {

        if (isNotUpdate) {

            var staffuserDetails = sessionStorage.getItem(ah.config.skey.staffuserDetails);
            var jStaffUser = JSON.parse(staffuserDetails);

            var strDate;
			if (jStaffUser.PASSWORD_EXPIRY !== null) {
				strDate = String(jStaffUser.PASSWORD_EXPIRY).split('-', 3);
				jStaffUser.PASSWORD_EXPIRY = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0]
			}
			if (jStaffUser.HIRE_DATETIME !== null) {
				strDate = String(jStaffUser.HIRE_DATETIME).split('-', 3);
				jStaffUser.HIRE_DATETIME = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0]
			}
			if (jStaffUser.TERM_DATETIME !== null) {
				strDate = String(jStaffUser.TERM_DATETIME).split('-', 3);
				jStaffUser.TERM_DATETIME = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0]
			}
            ah.ResetControls();
            ah.LoadJSON(jStaffUser);

            var srmodels = userBuildingList;
            var htmlstr = '';
            $(ah.config.id.modelsResultList + ' ' + ah.config.tag.tbody).html('');
            self.modelsList.splice(0, 5000);
            if (srmodels.length > 0) {
                for (var o in srmodels) {
                    self.modelsList.push({
                        BUILDING_CODE: srmodels[o].BUILDING_CODE,
                        DESCRIPTION: srmodels[o].DESCRIPTION
                    });
                }

            }

            var sr = userBuildingLOV;
            $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');
            if (sr.length > 0) {
                var htmlstr = "";
                for (var o in sr) {

                    htmlstr += '<li>';
                    htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                    htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].LOV_LOOKUP_ID, sr[o].DESCRIPTION, "");
                    htmlstr += '</span>';
                    htmlstr += '</li>';

                }

                $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).append(htmlstr);
                $(ah.config.cls.staffuserDetailItem).bind('click', self.assignModel);

            }


            //user store
            var srmodels = userStoreList;
            var htmlstr = '';
            $(ah.config.id.modelsResultList + ' ' + ah.config.tag.tbody).html('');
            self.userStore.splice(0, 5000);
            if (srmodels.length > 0) {
                for (var o in srmodels) {
                    self.userStore.push({
                        STORE_ID: srmodels[o].STORE_ID,
                        DESCRIPTION: srmodels[o].DESCRIPTION
                    });
                }

            }

            var srT = userStoreLOV;
            $(ah.config.id.searchResulStore + ' ' + ah.config.tag.ul).html('');
            if (srT.length > 0) {
                var htmlstr = "";
                for (var o in srT) {

                    htmlstr += '<li>';
                    htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                    htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplateStore, JSON.stringify(srT[o]), srT[o].STORE_ID, srT[o].DESCRIPTION, "");
                    htmlstr += '</span>';
                    htmlstr += '</li>';

                }

                $(ah.config.id.searchResulStore + ' ' + ah.config.tag.ul).append(htmlstr);
                $(ah.config.cls.staffuserStoreItem).bind('click', self.assignStore);

            }




        }

        ah.toggleDisplayMode();

    };

    self.searchResult = function (jsonData) {

        sessionStorage.setItem(ah.config.skey.searchResult, JSON.stringify(jsonData.STAFF));
        var sr = jsonData.STAFF;

        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {
                var passwordExpiry = new Date(sr[o].PASSWORD_EXPIRY);

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].STAFF_ID, sr[o].STAFF_ID, sr[o].FIRST_NAME, sr[o].LAST_NAME);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.userid, sr[o].USER_ID);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.emailaddress, sr[o].EMAIL_ADDRESS);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.passwordexpiry, passwordExpiry.toDateString().substring(4, 15));
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.staffposition, sr[o].POSITION_DESCRIPTION);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.staffgroup, sr[o].GROUP_DESCRIPTION);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.status, (sr[o].STATUS));
                htmlstr += '</li>';

            }
            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.staffuserDetailItem).bind('click', self.readStaffUserByStaffID);
        ah.displaySearchResult();

    };

    self.readStaffUserByStaffID = function () {

        var staffuserID = this.getAttribute(ah.config.attr.dataStaffUserID).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { STAFF_ID: staffuserID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readStaffUser, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleSearchMode();

        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
	self.saveOthInfo = function () {
		window.location.href = ah.config.url.modalClose;
		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffuserDetails = ah.ConvertFormToJSON($(ah.config.id.frmStaffUser)[0]);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var postData;
		var jsonObj = [];
		jsonObj = ko.utils.stringifyJson(self.othList);
		var othInfoDet = JSON.parse(jsonObj);
		//alert(JSON.stringify(othInfoDet));
		var uCategory = $(ah.config.id.uCategoryId).val();
		
		if (othInfoDet.length > 0) {
			ah.CurrentMode = ah.config.mode.updateModal;
			postData = { STAFF_COSTCENTER_BUILDING: othInfoDet, STAFF_LOGON_SESSIONS: staffLogonSessions };
			$.post(self.getApi() + ah.config.api.createStaffCB, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
		} else {
			var staffId = $("#STAFF_ID").val();
			ah.CurrentMode = ah.config.mode.updateModal;
			postData = { UCATEGORY: uCategory, STAFF_ID: staffId, STAFF_LOGON_SESSIONS: staffLogonSessions };
			$.post(self.getApi() + ah.config.api.deleteStaffCB, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
		}
			
		
		//createStaffCB
	};
    self.saveStaffUserDetails = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffuserDetails = ah.ConvertFormToJSON($(ah.config.id.frmStaffUser)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        //var someimage = document.getElementById('PHOTO_IMAGE');
        //var myimg = someimage.getElementsByTagName('img')[0];
        //var mysrc = myimg.src;
        //console.log(mysrc);
        //alert(mysrc);
        //return

		if (staffuserDetails.CHRG_RATE === null || staffuserDetails.CHRG_RATE === "") {
			staffuserDetails.CHRG_RATE = 0;
		}
		if (staffuserDetails.PAY_RATE === null || staffuserDetails.PAY_RATE === "") {
			staffuserDetails.PAY_RATE = 0;
		}
		if (staffuserDetails.SCHED_HRS === null || staffuserDetails.SCHED_HRS === "") {
			staffuserDetails.SCHED_HRS = 0;
		}
        staffuserDetails.CREATED_BY = staffLogonSessions.STAFF_ID;
       

        if (ah.CurrentMode == ah.config.mode.add) {
			var jDate = new Date();
			var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
			function time_format(d) {
				hours = format_two_digits(d.getHours());
				minutes = format_two_digits(d.getMinutes());
				return hours + ":" + minutes;
			}
			function format_two_digits(n) {
				return n < 10 ? '0' + n : n;
			}
			var hourmin = time_format(jDate);
			staffuserDetails.CREATED_DATE = strDateStart + ' ' + hourmin;
			ah.toggleSaveMode();
			postData = { STAFF: staffuserDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.createStaffUser, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {

            var jsonObj = ko.observableArray([]);

            jsonObj = ko.utils.stringifyJson(self.modelsList);

            var clientBuildingList = JSON.parse(jsonObj);



            var jsonObjS = ko.observableArray([]);

            jsonObjS = ko.utils.stringifyJson(self.userStore);

            var userStoreList = JSON.parse(jsonObjS);


            ah.toggleSaveMode();
            postData = { STAFF: staffuserDetails, USER_BUILDING: clientBuildingList, USER_STORE: userStoreList, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.updateStaffUser, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }

    };

    self.webApiCallbackStatus = function (jsonData) {
		//alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {

			var res = jsonData.ERROR.ErrorText.substring(0, 11);
			if (res === "USERIDEXIST") {
				ah.toggleReturnAddMode();
				self.alertMessage("The Staff Id/ User Id has been use by another staff. Please Enter unique Staff Id / User Id to proceed.");
			} else {
				alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
			}
			
           // window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.LOV_LOOKUPS || jsonData.STAFF_STORE || jsonData.STAFF_COSTCENTER_BUILDING || jsonData.STAFF_ID || jsonData.STAFF || jsonData.STAFF_POSITIONS || jsonData.STAFF_GROUPS || jsonData.SUCCESS || jsonData.STAFF || jsonData.USER_BUILDING || jsonData.USER_BUILDING_LOV || jsonData.USER_STORE || jsonData.USER_STORE_LOV) {
			//alert(ah.CurrentMode);
            if (ah.CurrentMode == ah.config.mode.save) {

                sessionStorage.setItem(ah.config.skey.staffuserDetails, JSON.stringify(jsonData.STAFF));
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();

                if (jsonData.SUCCESS) {
                    self.showStaffUserDetails(false);
                }
                else {
                    self.showStaffUserDetails(true);
                }
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                sessionStorage.setItem(ah.config.skey.staffuserDetails, JSON.stringify(jsonData.STAFF));
                userBuildingLOV = jsonData.USER_BUILDING_LOV;
                userBuildingList = jsonData.USER_BUILDING;

                userStoreLOV = jsonData.USER_STORE_LOV;
                userStoreList = jsonData.USER_STORE;

                self.showStaffUserDetails(true);
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
                self.searchResult(jsonData);
			}
			else if (ah.CurrentMode == ah.config.mode.display) {
				if (jsonData.LOV_LOOKUPS) {
					self.searchResultCostCenter(jsonData);
				} else {
					self.searchResult(jsonData);
				}
				
			}
            else if (ah.CurrentMode == ah.config.mode.idle) {
             
            
                $.each(jsonData.STAFF_GROUPS, function (item) {
                    $('#STAFF_GROUP_ID')
                        .append($("<option></option>")
                        .attr("value", jsonData.STAFF_GROUPS[item].STAFF_GROUP_ID)
                        .text(jsonData.STAFF_GROUPS[item].DESCRIPTION));
                });

                $.each(jsonData.STAFF_POSITIONS, function (item) {
                    $('#STAFF_POSITION_ID')
                        .append($("<option></option>")
                        .attr("value", jsonData.STAFF_POSITIONS[item].STAFF_POSITION_ID)
                        .text(jsonData.STAFF_POSITIONS[item].DESCRIPTION));
                });

                var staffLocation = jsonData.LOV_LOOKUPS;
                staffLocation = staffLocation.filter(function (item) { return item.CATEGORY === 'AIMS_BUILDING' });
                $.each(staffLocation, function (item) {

                    $(ah.config.id.staffLocationId)
                        .append($("<option></option>")
                            .attr("value", staffLocation[item].LOV_LOOKUP_ID)
                            .text(staffLocation[item].DESCRIPTION));
                });
                var staffService = jsonData.LOV_LOOKUPS;
                staffService = staffService.filter(function (item) { return item.CATEGORY === 'AIMS_SERVICEDEPARTMENT' });
                $.each(staffService, function (item) {

                    $(ah.config.id.staffSrv)
                        .append($("<option></option>")
                            .attr("value", staffService[item].LOV_LOOKUP_ID)
                            .text(staffService[item].DESCRIPTION));
                });

			}
			else if (ah.CurrentMode == ah.config.mode.updateModal) {
				$(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
				ah.showSavingStatusSuccess();
				ah.CurrentMode = ah.config.mode.display;
			}

        }
        else {

            alert(systemText.errorTwo);
          //  window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};