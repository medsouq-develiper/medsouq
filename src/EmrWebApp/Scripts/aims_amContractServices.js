﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divInfo: '.divInfo',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            detailItem: '.detailItem'
        },
        fld: {
            assetNoInfo: 'ASSETNO',
            assetDescInfo: 'ASSETDESC',
            reqWONo: 'REQNO'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmInfo: '#frmInfo',
            clinicdoctorID: '#CLINIC_DOCTOR_ID',
            clinicID: '#CLINIC_ID',
            staffID: '#STAFF_ID',
            serviceID: '#SERVICE_ID',
			successMessage: '#successMessage',
			addButtonId: '#addButtonId',
			modifyButtonId: '#modifyButtonId'
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
			searchResult: 'searchResult',
			groupPrivileges: 'GROUP_PRIVILEGES',
            infoDetails: 'AM_SUPPLIER'
        },
        url: {
            homeIndex: '/Home/Index',
            aimsPMSchedule: 'aims/am_PMSchedule',
            aimsInformation: '/aims/assetInfo',
            aimsHomepage: '/Menuapps/aimsIndex',
        },
        api: {

            readSetup: '/AIMS/ReadAssetContractService',
            searchAll: '/AIMS/SearchAssetContractService',
            createSetup: '/AIMS/CreateNewContractService',
            updateSetup: '/AIMS/UpdateAssetContractService'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;
        document.getElementById("CONTRACT_ID").disabled = true;
        document.getElementById("TERM_EFFECTIVE").disabled = true;
        document.getElementById("TERM_TERMINATION").disabled = true;
        document.getElementById("TERM_REVIEW").disabled = true;

        document.getElementById("LABOR_RATEPERHOUR").disabled = true;
        document.getElementById("LABOR_HOURS").disabled = true;
        document.getElementById("COST_TOTAL").disabled = true;
        document.getElementById("COST_PERPIECE").disabled = true;
        document.getElementById("COST_LASTPERIOD").disabled = true;
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";

    };

    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("CONTRACT_ID").disabled = false;
        document.getElementById("TERM_EFFECTIVE").disabled = false;
        document.getElementById("TERM_TERMINATION").disabled = false;
        document.getElementById("TERM_REVIEW").disabled = false;

        document.getElementById("LABOR_RATEPERHOUR").disabled = false;
        document.getElementById("LABOR_HOURS").disabled = false;
        document.getElementById("COST_TOTAL").disabled = false;
        document.getElementById("COST_PERPIECE").disabled = false;
        document.getElementById("COST_LASTPERIOD").disabled = false;
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";

    };

    thisApp.toggleAddExistMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

       
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.statusText + ',' + thisApp.config.cls.divProgress
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("CONTRACT_ID").disabled = false;
        document.getElementById("TERM_EFFECTIVE").disabled = false;
        document.getElementById("TERM_TERMINATION").disabled = false;
        document.getElementById("TERM_REVIEW").disabled = false;

        document.getElementById("LABOR_RATEPERHOUR").disabled = false;
        document.getElementById("LABOR_HOURS").disabled = false;
        document.getElementById("COST_TOTAL").disabled = false;
        document.getElementById("COST_PERPIECE").disabled = false;
        document.getElementById("COST_LASTPERIOD").disabled = false;
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.liSave+','+thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();
        document.getElementById("CONTRACT_ID").disabled = true;
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("CONTRACT_ID").disabled = true;
        document.getElementById("TERM_EFFECTIVE").disabled = false;
        document.getElementById("TERM_TERMINATION").disabled = false;
        document.getElementById("TERM_REVIEW").disabled = false;

        document.getElementById("LABOR_RATEPERHOUR").disabled = false;
        document.getElementById("LABOR_HOURS").disabled = false;
        document.getElementById("COST_TOTAL").disabled = false;
        document.getElementById("COST_PERPIECE").disabled = false;
        document.getElementById("COST_LASTPERIOD").disabled = false;
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";

    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();
        
    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

        $(
           thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
           thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);
        document.getElementById("TERM_EFFECTIVE").disabled = true;
        document.getElementById("TERM_TERMINATION").disabled = true;
        document.getElementById("TERM_REVIEW").disabled = true;

        document.getElementById("LABOR_RATEPERHOUR").disabled = true;
        document.getElementById("LABOR_HOURS").disabled = true;
        document.getElementById("COST_TOTAL").disabled = true;
        document.getElementById("COST_PERPIECE").disabled = true;
        document.getElementById("COST_LASTPERIOD").disabled = true;
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {
        $(form).find("input:disabled").removeAttr("disabled");
        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

   
    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.formatJSONDateToString = function () {

        var dateToFormat = arguments[0];
        var strDate;

        if (dateToFormat === null) return "-";
        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');


            if (isDateTime) {
                val = thisApp.formatJSONDateTimeToString(val);
            }

            if (isDate) {
                val = thisApp.formatJSONDateToString(val);
            }

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var amContractServiceViewModel = function (systemText) {

    var self = this;

    var ah = new appHelper(systemText);

    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.toggleReadMode();

        ah.initializeLayout();

        assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        assetDescription = ah.getParameterValueByName(ah.config.fld.assetDescInfo);
        if (assetDescription.length > 70) {
            assetDescription = assetDescription.substring(0, 70) + '...';
        }
        if (assetNoID !== null && assetNoID !== "") {
            $("#infoID").text(assetNoID + "-" + assetDescription);
            self.searchNow();
        }

    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };

    self.assetInfo = function () {
        assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        var reqWoNO = ah.getParameterValueByName(ah.config.fld.reqWONo);
        window.location.href = ah.config.url.aimsInformation + "?ASSETNO=" + assetNoID + "&REQNO=" + reqWoNO;
    };
    self.closeModalalert = function () {
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none"
    }
    self.returnHome = function () {

		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		window.location.href = staffDetails.HOME_DEFAULT;
    };

    self.createNew = function () {

        ah.toggleAddMode();

    };

    self.modifySetup = function () {

        ah.toggleEditMode();

    };

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.saveAllChanges = function () {

        $(ah.config.id.saveInfo).trigger('click');

    };

    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            //ah.initializeLayout();
            self.searchNow()
        } else {
            ah.toggleDisplayMode();
            var refID = $("#ASSET_CONTRACTSERVICE_ID").val();
            //alert(refID);
            
            self.readSetupExistID(refID);
            //self.showDebtorDetails();
        }

    };
    self.readSetupExistID = function (refId) {

       
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { ASSET_CONTRACTSERVICE_ID: refId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.showDetails = function (isNotUpdate) {

        if (isNotUpdate) {

            var infoDetails = sessionStorage.getItem(ah.config.skey.infoDetails);

            var jsetupDetails = JSON.parse(infoDetails);


            
            ah.ResetControls();
            ah.LoadJSON(jsetupDetails);
            if (jsetupDetails.TERM_EFFECTIVE == "" || jsetupDetails.TERM_EFFECTIVE == "null" || jsetupDetails.TERM_EFFECTIVE == "0001-01-01T00:00:00" || jsetupDetails.TERM_EFFECTIVE == "1900-01-01T00:00:00") {
                // jsetupDetails.TERM_EFFECTIVE = "0000-00-00T00:00:00";

                $("#TERM_EFFECTIVE").val("");
            }
            if (jsetupDetails.TERM_TERMINATION == "" || jsetupDetails.TERM_TERMINATION == "null" || jsetupDetails.TERM_TERMINATION == "0001-01-01T00:00:00" || jsetupDetails.TERM_TERMINATION == "1900-01-01T00:00:00") {
                //jsetupDetails.TERM_TERMINATION = "0000-00-00T00:00:00";
                $("#TERM_TERMINATION").val("");
            }
            if (jsetupDetails.TERM_REVIEW == "" || jsetupDetails.TERM_REVIEW == "null" || jsetupDetails.TERM_REVIEW == "0001-01-01T00:00:00" || jsetupDetails.TERM_REVIEW == "1900-01-01T00:00:00") {
                //jsetupDetails.TERM_REVIEW = "0000-00-00T00:00:00";
                $("#TERM_REVIEW").val("");
            }
        }

        ah.toggleDisplayMode();
		self.privilegesValidation('NEW');
		self.privilegesValidation('MODIFY');
    };

    self.searchResult = function (jsonData) {


        var sr = jsonData.AM_ASSET_CONTRACTSERVICES;
        // alert(JSON.stringify(sr));
        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {
                var effectiveDate = new Date(sr[o].TERM_EFFECTIVE);
                var terminationDate = new Date(sr[o].TERM_TERMINATION);

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].ASSET_CONTRACTSERVICE_ID, sr[o].CONTRACT_ID,'' , sr[o].PROVIDER);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.typeCoverage, sr[o].TYPE_COVERAGE);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.effective, effectiveDate.toDateString().substring(4, 15));
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.termination, terminationDate.toDateString().substring(4, 15));
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.labour, sr[o].COVERAGE_LABOR);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.parts, sr[o].COVERAGE_PARTSMATERIALS);

                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.detailItem).bind('click', self.readSetupID);
        ah.displaySearchResult();
		self.privilegesValidation('NEW');
    };

    self.readSetupID = function () {

        var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { ASSET_CONTRACTSERVICE_ID: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();
        assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), ASSETNO: assetNoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
	self.privilegesValidation = function (actionVal) {

		var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
		if (actionVal === 'NEW') {
			gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "16" });
			if (gPrivileges.length <= 0) {
				$(ah.config.id.addButtonId).hide();
			}
		} else if (actionVal === 'MODIFY') {
			gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "3" });
			if (gPrivileges.length <= 0) {
				$(ah.config.id.modifyButtonId + ',' + ah.config.id.addButtonIcon).hide();
			}
		}
	}
    self.saveInfo = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var setupDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        setupDetails.CREATED_BY = staffLogonSessions.STAFF_ID;

        if (setupDetails.TERM_EFFECTIVE == "" || setupDetails.TERM_EFFECTIVE == "null" || setupDetails.TERM_EFFECTIVE == "00/00/0000" || setupDetails.TERM_EFFECTIVE == "00-00-0000") {
            setupDetails.TERM_EFFECTIVE = "01/01/1900";
        }
        if (setupDetails.TERM_TERMINATION == "" || setupDetails.TERM_TERMINATION == "null" || setupDetails.TERM_TERMINATION == "00/00/0000" || setupDetails.TERM_TERMINATION == "00-00-0000") {
            setupDetails.TERM_TERMINATION = "01/01/1900";
        }

        if (setupDetails.TERM_REVIEW == "" || setupDetails.TERM_REVIEW == "null" || setupDetails.TERM_REVIEW == "00/00/0000" || setupDetails.TERM_REVIEW == "00-00-0000") {
            setupDetails.TERM_REVIEW = "01/01/1900";
        }
        if (setupDetails.LABOR_RATEPERHOUR == "" || setupDetails.LABOR_RATEPERHOUR == null) {
            setupDetails.LABOR_RATEPERHOUR = "0";
        }

        if (setupDetails.COST_TOTAL == "" || setupDetails.COST_TOTAL == null) {
            setupDetails.COST_TOTAL = "0";
        }
        if (setupDetails.COST_PERPIECE == "" || setupDetails.COST_PERPIECE == null) {
            setupDetails.COST_PERPIECE = "0";
        }
        if (setupDetails.COST_LASTPERIOD == "" || setupDetails.COST_LASTPERIOD == null) {
            setupDetails.COST_LASTPERIOD = "0";
        }

        if (setupDetails.COST_TOTAL < 0 || setupDetails.LABOR_RATEPERHOUR < 0 || setupDetails.COST_PERPIECE < 0 || setupDetails.COST_LASTPERIOD < 0 || setupDetails.LABOR_HOURS < 0) {
            alert('Please double check value entered, negative value are not accepted!');

            return;
        }

     
        if (ah.CurrentMode == ah.config.mode.add) {
            var jDate = new Date();
            var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);

            setupDetails.CREATED_DATE = strDateStart + ' ' + hourmin;
            postData = { AM_ASSET_CONTRACTSERVICES: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            ah.toggleSaveMode();
            setupDetails.ASSET_CONTRACTSERVICE_ID = 0;
            var assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
            setupDetails.ASSET_NO = assetNoID;
            $.post(self.getApi() + ah.config.api.createSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {

            ah.toggleSaveMode();
            postData = { AM_ASSET_CONTRACTSERVICES: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };

            $.post(self.getApi() + ah.config.api.updateSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }

    };

    self.webApiCallbackStatus = function (jsonData) {
         //alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {
            var errorText = jsonData.ERROR.ErrorText;
            var res = errorText.substring(0, 9);

            if (res === "EXISTCODE") {
                ah.toggleAddExistMode();
                $("#CONTRACT_ID").next("span").remove();
                $("#CONTRACT_ID").after("<span style='color:red;padding-left:5px;'>*Contract Id already in use</span>")
                //var alertMsg = 'Sorry the Contract Id is already been used by another Contract Service.'
                //$("#modalAlertMsg").text(alertMsg);
                //var modal = document.getElementById('openModalAlert');
                //modal.style.display = "block";


            } else {
                alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            }

        }
        else if (jsonData.AM_ASSET_CONTRACTSERVICES || jsonData.STAFF || jsonData.SUCCESS) {
            $("#CONTRACT_ID").next("span").remove();
            if (ah.CurrentMode == ah.config.mode.save) {
                sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_ASSET_CONTRACTSERVICES));
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();

                if (jsonData.SUCCESS) {
                    self.showDetails(false);
                }
                else {
                    self.showDetails(true);
                }
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_ASSET_CONTRACTSERVICES));
                self.showDetails(true);
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
                self.searchResult(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {
				
            }

        }
        else {

            alert(systemText.errorTwo);
            // window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};