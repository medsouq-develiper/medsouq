﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divproductID: '.divproductID',
            statusText: '.status-text',
            productDetailItem: '.productDetailItem'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveProduct: '#saveProduct',
            searchResultList: '#searchResultList1',
            searchResultList2: '#searchResultList2',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            txtCriteriaStatus: '#txtCriteriaStatus',
            txtCategory: '#txtCategory',
            frmProductDetails: '#frmProductDetails',
            productID: '#PRODUCT_ID',
            categoryId: '#CATEGORY_ID',
            groupId: '#GROUP_ID',
            serviceID: '#SERVICE_ID',
            saleUnit: '#SALE_UNIT',
            stockUnit: '#STOCK_UNIT',
            successMessage: '#successMessage'
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            dataProductID: 'data-productID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            productDetails: 'PRODUCT_DETAILS',
            searchResult: 'searchResult'
        },
        url: {
            homeIndex: '/Home/Index'
        },
        api: {
            readProduct: '/Inventory/ReadProduct',
            searchAll: '/Inventory/SearchProducts',
            searchLov: '/Setup/SearchInventoryLOV',
            createProduct: '/Inventory/CreateNewProduct',
            updateProduct: '/Inventory/UpdateProductDetails'
        },
        html: {
            // searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal productDetailItem' data-productID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}</a>",
            searchLOVRowHeaderTemplate: "<a href='#' class='search-row-data productDetailItem fc-green' data-productID='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            searchRowTemplate: '<span class="search-row-label fc-light-grey-2">{0}: <span class="search-row-data">{1}</span></span>'
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divStaffPositionID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();
        $(thisApp.config.id.txtCriteriaStatus + ',' + thisApp.config.id.txtCategory).prop(thisApp.config.attr.disabled, false);
        thisApp.CurrentMode = thisApp.config.mode.idle;

    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divStaffPositionID
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatuxText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();
        $(thisApp.config.id.txtCriteriaStatus + ',' + thisApp.config.id.txtCategory).prop(thisApp.config.attr.disabled, false);

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus).hide();
        $(thisApp.config.id.txtCriteriaStatus + ',' + thisApp.config.id.txtCategory).prop(thisApp.config.attr.disabled, false);

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        $(thisApp.config.id.txtCriteriaStatus + ',' + thisApp.config.id.txtCategory).prop(thisApp.config.attr.disabled, false);

    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
        $(thisApp.config.id.txtCriteriaStatus + ',' + thisApp.config.id.txtCategory).prop(thisApp.config.attr.disabled, false);
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divStaffPositionID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

        $(
           thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
           thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);
        $(thisApp.config.id.txtCriteriaStatus + ',' + thisApp.config.id.txtCategory).prop(thisApp.config.attr.disabled, false);

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name),
                type = $el.attr('type');

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var productentryViewModel = function (systemText) {

    var self = this;

    var ah = new appHelper(systemText);

    self.initialize = function () {

        var webApiURL = localStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(localStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(localStorage.getItem(ah.config.skey.staff));

        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.toggleReadMode();
        postData = { LOV: "'PCATEGORY','PGROUP','PROD_UOM'", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

        ah.initializeLayout();

    };

    self.createNewProduct = function () {

        ah.toggleAddMode();

    };

    self.productDetails = function () {

        ah.toggleEditMode();

    };

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.saveAllChanges = function () {

        $(ah.config.id.saveProduct).trigger('click');

    };

    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            self.showStaffPositionDetails();
        }

    };

    self.showProductDetails = function (isNotUpdate) {

        if (isNotUpdate) {

            var productDetails = localStorage.getItem(ah.config.skey.productDetails);

            var jproductDetails = JSON.parse(productDetails);

            ah.ResetControls();
            ah.LoadJSON(jproductDetails);
        }

        ah.toggleDisplayMode();

    };

    self.searchResult = function (jsonData) {

        localStorage.setItem(ah.config.skey.searchResult, JSON.stringify(jsonData.PRODUCT_DETAILS));
        var sr = jsonData.PRODUCT_DETAILS;

        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');
        $(ah.config.id.searchResultList2 + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {

            var htmlstr = '';

            var htmlStr1 = '';
            var htmlStr2 = '';
            var totalrecords = sr.length;
            var itemCount = totalrecords
            var flag = 0;
            var i = 1;
            for (var o in sr) {
                if (itemCount % 2 !== 0) {
                    itemCount++;
                }
                var subtot = itemCount / 2;


                htmlstr += '<li>';
                htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                htmlstr += ah.formatString(ah.config.html.searchLOVRowHeaderTemplate, sr[o].PRODUCT_ID, sr[o].DESCRIPTION, " - ", sr[o].CATEGORY_ID);
                htmlstr += '</span>';

                htmlstr += '</li>';
                if (o >= subtot - 1 && flag == 0) {
                    htmlStr1 += htmlstr;
                    htmlstr = '';
                    flag = 1;
                } else if (o >= sr.length - 1 && flag == 1) {
                    htmlStr2 += htmlstr;
                    htmlstr = '';
                    flag = 2
                }


            }
            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlStr1);
            $(ah.config.id.searchResultList2 + ' ' + ah.config.tag.ul).append(htmlStr2);
        }

        $(ah.config.cls.productDetailItem).bind('click', self.readProductID);
        ah.displaySearchResult();

    };

    self.readProductID = function () {

        var productID = this.getAttribute(ah.config.attr.dataProductID).toString();
        var webApiURL = localStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(localStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
              
        ah.toggleReadMode();
        
        postData = { PRODUCT_ID: productID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readProduct, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.getApi = function () {
        var staffDetails = JSON.parse(localStorage.getItem(ah.config.skey.staff));
        var webApiURL = localStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };

    self.searchNow = function () {

        var webApiURL = localStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(localStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

     
        ah.toggleSearchMode();

        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), STATUS: $(ah.config.id.txtCriteriaStatus).val().toString(),CATEGORY_ID: $(ah.config.id.txtCategory).val().toString(), STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi()+ ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.saveProductDetails = function () {

        var webApiURL = localStorage.getItem(ah.config.skey.webApiUrl);
        var productDetails = ah.ConvertFormToJSON($(ah.config.id.frmProductDetails)[0]);
        var staffLogonSessions = JSON.parse(localStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;


        postData = { PRODUCT_DETAILS: productDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };

        if (ah.CurrentMode == ah.config.mode.add) {

            ah.toggleSaveMode();
            $.post(self.getApi() + ah.config.api.createProduct, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {

            ah.toggleSaveMode();

            $.post(self.getApi() + ah.config.api.updateProduct, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }

    };

    self.webApiCallbackStatus = function (jsonData) {
        //alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.SERVICES || jsonData.PRODUCT_ID || jsonData.PRODUCT_DETAILS || jsonData.SUCCESS || jsonData.LOV_LOOKUPS) {

            if (ah.CurrentMode == ah.config.mode.save) {

                localStorage.setItem(ah.config.skey.productDetails, JSON.stringify(jsonData.PRODUCT_DETAILS));
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();

                if (jsonData.SUCCESS) {
                    self.showProductDetails(false);
                }
                else {
                    self.showProductDetails(true);
                }
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                localStorage.setItem(ah.config.skey.productDetails, JSON.stringify(jsonData.PRODUCT_DETAILS));
                
                self.showProductDetails(true);
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
                self.searchResult(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {

                var searchCat = jsonData.LOV_LOOKUPS;
                searchCat = searchCat.filter(function (item) { return item.CATEGORY === 'PCATEGORY' });
                $.each(searchCat, function (item) {
                    
                    $(ah.config.id.txtCategory)
                        .append($("<option></option>")
                        .attr("value", searchCat[item].LOV_LOOKUP_ID)
                        .text(searchCat[item].DESCRIPTION));
                });

                var category = jsonData.LOV_LOOKUPS;               
                category = category.filter(function (item) { return item.CATEGORY === 'PCATEGORY' });
                
                $.each(category, function (item) {
                    
                    $(ah.config.id.categoryId)
                        .append($("<option></option>")
                        .attr("value", category[item].LOV_LOOKUP_ID)
                        .text(category[item].DESCRIPTION));
                });

                var group = jsonData.LOV_LOOKUPS;
                group = group.filter(function (item) { return item.CATEGORY === 'PGROUP' });
               
                $.each(group, function (item) {
                    
                    $(ah.config.id.groupId)
                        .append($("<option></option>")
                        .attr("value", group[item].LOV_LOOKUP_ID)
                        .text(group[item].DESCRIPTION));
                });
                var unitStr = jsonData.LOV_LOOKUPS;
                unitStr = unitStr.filter(function (item) { return item.CATEGORY === 'PROD_UOM' });

                $.each(unitStr, function (item) {

                    $(ah.config.id.saleUnit)
                        .append($("<option></option>")
                        .attr("value", unitStr[item].LOV_LOOKUP_ID)
                        .text(unitStr[item].DESCRIPTION));
                });

                $.each(unitStr, function (item) {

                    $(ah.config.id.stockUnit)
                        .append($("<option></option>")
                        .attr("value", unitStr[item].LOV_LOOKUP_ID)
                        .text(unitStr[item].DESCRIPTION));
                });


                $.each(jsonData.SERVICES, function (item) {

                    $(ah.config.id.serviceID)
                        .append($("<option></option>")
                        .attr("value", jsonData.SERVICES[item].SERVICE_ID)
                        .text(jsonData.SERVICES[item].DESCRIPTION));
                });

            }

        }
        else {

            alert(systemText.errorTwo);
            window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};