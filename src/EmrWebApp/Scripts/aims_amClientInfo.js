﻿var appHelper = function (systemText) {

	var thisApp = this;

	thisApp.config = {
		cls: {
			contentField: '.field-content-detail',
			contentSearch: '.search-content-detail',
			contentHome: '.home-content-detail',
			liModify: '.liModify',
			liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
			divProgress: '.divProgressScreen',
			liApiStatus: '.liApiStatus',
			notifySuccess: '.notify-success',
			toolBoxRight: '.toolbox-right',
			divClinicDoctorID: '.divClinicDoctorID',
			statusText: '.status-text',
            detailItem: '.detailItem',
            liCreateStore: '.liCreateStore',
            storeContainer: '.storeContainer',
            otherContactRemove: '.otherContactRemove',
		},
		tag: {
			input: 'input',
			textArea: 'textarea',
			select: 'select',
			inputTypeText: 'input[type=text]',
			inputTypeEmail: 'input[type=email]',
			ul: 'ul'
		},
		mode: {
			idle: 0,
			add: 1,
			search: 2,
			searchResult: 3,
			save: 4,
			read: 5,
			display: 6,
			edit: 7
		},
		id: {
			spanUser: '#spanUser',
			contentHeader: '#contentHeader',
			contentSubHeader: '#contentSubHeader',
			saveInfo: '#saveInfo',
			searchResultList: '#searchResultList',
			searchResultSummary: '#searchResultSummary',
		
			frmInfo: '#frmInfo',
			clinicdoctorID: '#CLINIC_DOCTOR_ID',
			clinicID: '#CLINIC_ID',
			staffID: '#STAFF_ID',
			serviceID: '#SERVICE_ID',
            successMessage: '#successMessage',
            imageInfo: '#imageInfo',
            companyimageInfo: '#companyimageInfo',
            otherContactNew: '#otherContactNew',
            searchResultLookUp: '#searchResultLookUp'
		},
		cssCls: {
			viewMode: 'view-mode'
		},
		attr: {
			readOnly: 'readonly',
			disabled: 'disabled',
			selectedIndex: 'selectedIndex',
			datainfoId: 'data-infoID'
		},
		skey: {
			staff: 'STAFF',
			webApiUrl: 'webApiURL',
			staffLogonSessions: 'STAFF_LOGON_SESSIONS',
			searchResult: 'searchResult',
			infoDetails: 'AM_MANUFACTURER'
		},
		url: {
			homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalLookUp: '#openModalLookup',
            createStoreModal: '#openCreateStoreModal',
            openOtherContactModal: '#openOtherContactModal',
            modalClose: '#'
		},
		api: {

			readSetup: '/Setup/ReadClientInfo',
            updateSetup: '/Setup/CreateUpdateClientInfo',
            search: '/Setup/SearchClient',
            createSetup: '/Setup/CreateClient'
		},
		html: {
			searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
			searchRowTemplate: '<span class="search-row-label">{0} <span class="search-row-data">{1}</span></span>'
		}
	};

	thisApp.CurrentMode = thisApp.config.mode.idle;

	thisApp.ResetControls = function () {

		$(thisApp.config.tag.input).val('');
		$(thisApp.config.tag.textArea).val('');
		$(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

	};

	thisApp.initializeLayout = function () {

		thisApp.ResetControls();

		$(
            thisApp.config.id.companyimageInfo + ',' + thisApp.config.id.imageInfo + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' + thisApp.config.id.otherContactNew + ',' + thisApp.config.cls.otherContactRemove + ',' +
			thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
			thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess
		).hide();

		$(thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;
        $(thisApp.config.cls.liCreateStore).hide();

	};

	thisApp.toggleAddMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.add;

		thisApp.ResetControls();
		$(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
		$(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.id.companyimageInfo + ',' + thisApp.config.id.imageInfo + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.id.otherContactNew + ',' + thisApp.config.cls.otherContactRemove).show();

		$(
			thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.liAddNew
		).hide();


		$(
			thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
			thisApp.config.tag.textArea
		).removeClass(thisApp.config.cssCls.viewMode);

		$(
			thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
		).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        $(thisApp.config.cls.storeContainer).hide();

	};

	thisApp.toggleSearchMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.search;

		$(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liAddNew).show();

		$(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liCreateStore + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.liSave
		).hide();

	};

	thisApp.displaySearchResult = function () {

		thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liAddNew).show();
        $(thisApp.config.id.companyimageInfo + ',' + thisApp.config.id.imageInfo + ',' + thisApp.config.cls.liApiStatus + ',' + thisApp.config.id.otherContactNew + ',' + thisApp.config.cls.otherContactRemove).hide();

	};

	thisApp.toggleEditMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.edit;

		$(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
		$(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.id.companyimageInfo + ',' + thisApp.config.id.imageInfo + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.id.otherContactNew + ',' + thisApp.config.cls.otherContactRemove).show();
        $(thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight +',' + thisApp.config.cls.liAddNew).hide();

		$(
			thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
			thisApp.config.tag.textArea
		).removeClass(thisApp.config.cssCls.viewMode);

		$(
			thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
		).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

	};

	thisApp.toggleSaveMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.save;

		$(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
		$(thisApp.config.cls.liSave).hide();
		$(thisApp.config.cls.divProgress).show();
		$(thisApp.config.cls.liApiStatus).show();

	};

	thisApp.toggleReadMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.read;

		$(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
		$(thisApp.config.cls.liApiStatus).show();

		$(
			thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liSave + ',' +
			thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
			thisApp.config.cls.contentField
		).hide();
	};

	thisApp.toggleDisplayMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.display;

		$(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
		$(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

		$(
			thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.storeContainer
		).show();

		$(
            thisApp.config.id.companyimageInfo + ',' + thisApp.config.id.imageInfo + ',' + thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.id.otherContactNew + ',' + thisApp.config.cls.otherContactRemove + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew
		).hide();

		$(
			thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
			thisApp.config.tag.textArea
		).addClass(thisApp.config.cssCls.viewMode);

		$(
			thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
		).prop(thisApp.config.attr.disabled, true);

		$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);

	};

	thisApp.showSavingStatusSuccess = function () {

		$(thisApp.config.cls.notifySuccess).show();
		$(thisApp.config.cls.divProgress).hide();

		setTimeout(function () {
			$(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
		}, 3000);


	};

	thisApp.ConvertFormToJSON = function (form) {

		var array = jQuery(form).serializeArray();
		var json = {};

		jQuery.each(array, function () {
			json[this.name] = this.value || '';
		});

		return json;

	};

	thisApp.getAge = function (birthDate) {

		var seconds = Math.abs(new Date() - birthDate) / 1000;

		var numyears = Math.floor(seconds / 31536000);
		var numdays = Math.floor((seconds % 31536000) / 86400);
		var nummonths = numdays / 30;

		return numyears + " y " + Math.round(nummonths) + " m ";
	};

	thisApp.formatString = function () {

		var stringToFormat = arguments[0];

		for (var i = 0; i <= arguments.length - 2; i++) {
			stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
		}

		return stringToFormat;
	};

	thisApp.LoadJSON = function (jsonData) {

		$.each(jsonData, function (name, val) {
			var $el = $('#' + name),
				type = $el.attr('type');

			switch (type) {
				case 'checkbox':
					$el.attr('checked', 'checked');
					break;
				case 'radio':
					$el.filter('[value="' + val + '"]').attr('checked', 'checked');
					break;
				default:
					var tagNameLCase = String($el.prop('tagName')).toLowerCase();
					if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
						$el.text(val);
					} else {
						$el.val(val);
					}
			}
		});

	};

};

/*Knockout MVVM Namespace - Controls form functionality*/
var clientInfoViewModel = function (systemText) {

	var self = this;

	var ah = new appHelper(systemText);
    self.modelsList = ko.observableArray("");
    var clientCostCenterLOV = ko.observableArray([]);
    var clientCostCenterList = ko.observableArray([]);

    self.otherContactList = ko.observableArray([]);
    self.modalOtherContact = {
        CLIENTCONTACTTYPE: ko.observable(""),
        CLIENTCONTACT: ko.observable("")
    };

	self.initialize = function () {

		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		var postData;

		$(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

		ah.toggleReadMode();
		
        ah.initializeLayout();

		self.search();
	};
	self.getApi = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		//alert(JSON.stringify(staffDetails));
		var api = "";
		if (staffDetails.API === null || staffDetails.API === "") {
			api = webApiURL + "api";
		} else {
			api = staffDetails.API + "api";
		}

		return api
	};
	self.returnHome = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		window.location.href = staffDetails.HOME_DEFAULT;
	};
    self.addModels = function () {
      
       
        window.location.href = ah.config.url.modalLookUp;
    };
    self.removeItem = function (srmodels) {
        self.modelsList.remove(srmodels);
    }
    self.createNew = function () {

        self.otherContactList.removeAll();
		ah.toggleAddMode();

	};
    self.search = function () {
        ah.toggleSearchMode();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        var staffInfo = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        postData = {STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.search, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        self.unsaveStore.DESCRIPTION("");
        self.unsaveStore.CLIENT_CODE("");
        self.unsaveStore.CLIENT_NAME("");
    };

    self.searchResult = function (jsonData) {
        var sr = jsonData.CLIENT_LIST;

        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

       // $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].CLIENT_CODE, sr[o].DESCRIPTION_SHORT, sr[o].DESCRIPTION, '');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.address, sr[o].ADDRESS);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.contactno, sr[o].CONTACT_NO1);

                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.detailItem).bind('click', self.readSetupID);
        ah.displaySearchResult();
    };

	self.modifySetup = function () {

        ah.toggleEditMode();
        if (self.unsaveStore.STORE_NO())
            $(ah.config.cls.liCreateStore).hide();
        else $(ah.config.cls.liCreateStore).show();

	};

	self.signOut = function () {

		window.location.href = ah.config.url.homeIndex;

	};

	self.saveAllChanges = function () {

		$(ah.config.id.saveInfo).trigger('click');

	};

	self.cancelChanges = function () {

        self.search();

	};

    self.showDetails = function (isNotUpdate, jsonData) {

		if (isNotUpdate) {

			var infoDetails = sessionStorage.getItem(ah.config.skey.infoDetails);
			
			var jsetupDetails = JSON.parse(infoDetails);

			ah.ResetControls();
            ah.LoadJSON(jsetupDetails);

           // var path = `/ClientFiles/${}`;

            document.getElementById("CLIENT_IMAGE").src = jsetupDetails.CLIENT_PATH_LOGO;
            document.getElementById("COMPANY_IMAGE").src = jsetupDetails.COMPANY_PATH_LOGO;
           

            var srmodels = clientCostCenterList;
            var htmlstr = '';
            $(ah.config.id.modelsResultList + ' ' + ah.config.tag.tbody).html('');
            self.modelsList.splice(0, 5000);
            if (srmodels.length > 0) {
                for (var o in srmodels) {
                    self.modelsList.push({
                        COSTCENTERCODE: srmodels[o].COSTCENTERCODE,
                        DESCRIPTION: srmodels[o].DESCRIPTION
                    });
                }

            }

            var sr = clientCostCenterLOV;
            $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');
            if (sr.length > 0) {
                var htmlstr = "";
                for (var o in sr) {

                    htmlstr += '<li>';
                    htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                    htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].LOV_LOOKUP_ID, sr[o].DESCRIPTION, "");
                    htmlstr += '</span>';
                    htmlstr += '</li>';

                }

                $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).append(htmlstr);
                $(ah.config.cls.detailItem).bind('click', self.assignModel);
                $("#lookUpnoresult").hide();
            } else {
                $("#lookUpnoresult").show();
            }
		}
        
        ah.toggleDisplayMode();
        if (jsonData.STORE) {
            self.unsaveStore.DESCRIPTION(jsonData.STORE.DESCRIPTION);
            self.unsaveStore.CLIENT_CODE(jsonData.STORE.CLIENT_CODE);
            self.unsaveStore.CLIENT_NAME(jsonData.STORE.DESCRIPTION);
            self.unsaveStore.STORE_NO(jsonData.STORE.STORE_ID);
        } else {
            self.unsaveStore.DESCRIPTION("");
            self.unsaveStore.CLIENT_CODE("");
            self.unsaveStore.CLIENT_NAME("");
            self.unsaveStore.STORE_NO("");
        }

	};

	
    self.assignModel = function () {

        var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfoId).toString());

        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.modelsList);
        var partLists = JSON.parse(jsonObj);

        var costcenterExist = partLists.filter(function (item) { return item.COSTCENTERCODE === infoID.LOV_LOOKUP_ID });

        if (costcenterExist.length > 0) {
            alert('Unable to Proceed. The selected Cost Center was already added.');
            return;
        };

        self.modelsList.push({
            COSTCENTERCODE: infoID.LOV_LOOKUP_ID,
            DESCRIPTION: infoID.DESCRIPTION
           
        });

        return;
    };
	self.readSetupID = function () {

		
		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var postData;

        ah.toggleReadMode();
        var clientID = this.getAttribute(ah.config.attr.datainfoId).toString();
        postData = { CLIENTID: clientID, STAFF_LOGON_SESSIONS: staffLogonSessions };
		$.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

	};

    $('#imageInfo').change(function (e) {
        var files = e.target.files;
        $("#LOGOACTION").val("CLIENT");
        self.loadImages(files);

    });

    $('#companyimageInfo').change(function (e) {
        var files = e.target.files;
        $("#LOGOACTION").val("COMPANY");
        self.loadImages(files);

    });
    self.loadImages = function (files) {

        var formDetails = ah.ConvertFormToJSON($(ah.config.id.frmCategory)[0]);
        if (formDetails.CATEGORY === null || formDetails.CATEGORY === "") {
            formDetails.CATEGORY = 'Others';
        }

        'use strict';
        x = 0;
        var resize = new window.resize();
        resize.init();
        //event.preventDefault();           

        // var files = event.target.files;       
        var array = 0;
        var arrayFileName = new Array;
     
        for (var i in files) {

            if (typeof files[i] !== 'object') return false;

            var file_name = files[i].name;
            arrayFileName.push(file_name);
            var base64 = self.resizeImage(files[i], arrayFileName);

        }


    };
    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.resizeImage = function (file, arrayFileName) {


        var resize = new window.resize();
        resize.init();


        var reader = new FileReader();
        reader.onload = function (readerEvent) {

            self.resizeCanvas(readerEvent.target.result, 1200, 'file', arrayFileName, file);

        }
        reader.readAsDataURL(file);


    };
    self.resizeCanvas = function (dataURL, maxSize, outputType, arrayFileName, file) {

        var _this = this;
        var x = 0;
        var image = new Image();
        image.onload = function (imageEvent) {

            // Resize image
            var canvas = document.createElement('canvas'),
                width = image.width,
                height = image.height;
            if (width > height) {
                if (width > maxSize) {
                    height *= maxSize / width;
                    width = maxSize;
                }
            } else {
                if (height > maxSize) {
                    width *= maxSize / height;
                    height = maxSize;
                }
            }
            canvas.width = width;
            canvas.height = height;
            canvas.getContext('2d').drawImage(image, 0, 0, width, height);

            var newdataURL = canvas.toDataURL('image/jpeg', 0.8)
            self.resizeOutcome(newdataURL, 600, 'dataURL', arrayFileName, file);

            // _this.output(canvas, outputType, callback);

        }

        image.src = dataURL;
    };

    self.resizeOutcome = function (dataURL, maxSize, outputType, arrayFileName, file) {

        var _this = this;

        var image = new Image();
        image.onload = function (imageEvent) {

            // Resize image
            var canvas = document.createElement('canvas'),
                width = image.width,
                height = image.height;
            if (width > height) {
                if (width > maxSize) {
                    height *= maxSize / width;
                    width = maxSize;
                }
            } else {
                if (height > maxSize) {
                    width *= maxSize / height;
                    height = maxSize;
                }
            }
            canvas.width = width;
            canvas.height = height;
            canvas.getContext('2d').drawImage(image, 0, 0, width, height);
            var newdataURL = canvas.toDataURL('image/jpeg', 0.8)

            self.eloaddata("", "", newdataURL, arrayFileName, file);
            // _this.output(canvas, outputType, callback);

        }

        image.src = dataURL;
    };

    self.eloaddata = function (filename, filetype, data, filenameArray, file) {
        var logoAction = $("#LOGOACTION").val();
        if (logoAction == "COMPANY") {
            document.getElementById("COMPANY_IMAGE").src = data;
            $("#COMPANY_LOGO").val("");
        } else {
            document.getElementById("CLIENT_IMAGE").src = data;
            $("#CLIENT_LOGO").val("");
        }
    };

	self.saveInfo = function () {

		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var setupDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var postData;

		setupDetails.CREATED_BY = staffLogonSessions.STAFF_ID;
        var clientCode = $("#CLIENT_CODE").val();

        var frmData = new FormData();

        frmData.append("REF_ID", clientCode);
        frmData.append("pathLocation", "~/ClientFiles");
        var filebase = $("#imageInfo").get(0);
        var filebase1 = $("#companyimageInfo").get(0);
        var file = filebase.files;
        var file1 = filebase1.files;

        if (file.length > 0 || file1.length > 0) {
            frmData.append(file[0].name, file[0]);
            $.ajax({
                url: '/Upload/SaveDocument',
                type: "POST",
                contentType: false,
                processData: false,
                data: frmData,
                success: function (data) {


                    setupDetails.CLIENT_PATH_LOGO = `/ClientFiles/${clientCode + "_" + file[0].name}`;
                    setupDetails.COMPANY_PATH_LOGO = `/ClientFiles/${clientCode + "_" + file1[0].name}`;

                    if (ah.CurrentMode == ah.config.mode.add) {
                        var jDate = new Date();
                        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
                        function time_format(d) {
                            hours = format_two_digits(d.getHours());
                            minutes = format_two_digits(d.getMinutes());
                            return hours + ":" + minutes;
                        }
                        function format_two_digits(n) {
                            return n < 10 ? '0' + n : n;
                        }
                        var hourmin = time_format(jDate);

                        setupDetails.CREATED_DATE = strDateStart + ' ' + hourmin;
                        postData = { AM_CLIENT_INFO: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
                        postData.CLIENTOTHCONTACT = self.otherContactList();
                        ah.toggleSaveMode();
                        //alert(JSON.stringify(postData));
                        $.post(self.getApi() + ah.config.api.createSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
                    }
                    else {

                        var jsonObj = ko.observableArray([]);

                        jsonObj = ko.utils.stringifyJson(self.modelsList);

                        var clientCostCenterList = JSON.parse(jsonObj);

                        if (!self.unsaveStore.STORE_NO()) {
                            var StoreData = {
                                STORE_NO: self.unsaveStore.STORE_NO(),
                                DESCRIPTION: self.unsaveStore.DESCRIPTION(),
                                LOCATION: self.unsaveStore.LOCATION(),
                                CONTACT_NO: self.unsaveStore.CONTACT_NO(),
                                CLIENT_CODE: self.unsaveStore.CLIENT_CODE(),
                                CLIENT_NAME: self.unsaveStore.CLIENT_NAME(),
                                STATUS: self.unsaveStore.STATUS,
                                COSTCENTERFLAG: self.unsaveStore.COSTCENTERFLAG
                            }
                            postData = { AM_CLIENT_INFO: setupDetails, AM_CLIENT_COSTCENTER: clientCostCenterList, STAFF_LOGON_SESSIONS: staffLogonSessions, StoreDetails: StoreData };
                        } else {
                            postData = { AM_CLIENT_INFO: setupDetails, AM_CLIENT_COSTCENTER: clientCostCenterList, STAFF_LOGON_SESSIONS: staffLogonSessions };
                        }

                        postData.CLIENTOTHCONTACT = self.otherContactList();
                        ah.toggleSaveMode();
                        //alert(JSON.stringify(postData));
                        $.post(self.getApi() + ah.config.api.updateSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
                    }
                },
                error: function (err) {
                    alert(error);
                }
            });
        } else {
            if (ah.CurrentMode == ah.config.mode.add) {
                var jDate = new Date();
                var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
                function time_format(d) {
                    hours = format_two_digits(d.getHours());
                    minutes = format_two_digits(d.getMinutes());
                    return hours + ":" + minutes;
                }
                function format_two_digits(n) {
                    return n < 10 ? '0' + n : n;
                }
                var hourmin = time_format(jDate);

                setupDetails.CREATED_DATE = strDateStart + ' ' + hourmin;
                postData = { AM_CLIENT_INFO: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
                postData.CLIENTOTHCONTACT = self.otherContactList();
                ah.toggleSaveMode();
                //alert(JSON.stringify(postData));
                $.post(self.getApi() + ah.config.api.createSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
            }
            else {

                var jsonObj = ko.observableArray([]);

                jsonObj = ko.utils.stringifyJson(self.modelsList);

                var clientCostCenterList = JSON.parse(jsonObj);

                if (!self.unsaveStore.STORE_NO() && self.unsaveStore.DESCRIPTION()) {
                    var StoreData = {
                        STORE_NO: self.unsaveStore.STORE_NO(),
                        DESCRIPTION: self.unsaveStore.DESCRIPTION(),
                        LOCATION: self.unsaveStore.LOCATION(),
                        CONTACT_NO: self.unsaveStore.CONTACT_NO(),
                        CLIENT_CODE: self.unsaveStore.CLIENT_CODE(),
                        CLIENT_NAME: self.unsaveStore.CLIENT_NAME(),
                        STATUS: self.unsaveStore.STATUS,
                        COSTCENTERFLAG: self.unsaveStore.COSTCENTERFLAG
                    }
                    postData = { AM_CLIENT_INFO: setupDetails, AM_CLIENT_COSTCENTER: clientCostCenterList, STAFF_LOGON_SESSIONS: staffLogonSessions, StoreDetails: StoreData };
                } else {
                    postData = { AM_CLIENT_INFO: setupDetails, AM_CLIENT_COSTCENTER: clientCostCenterList, STAFF_LOGON_SESSIONS: staffLogonSessions };
                }

                postData.CLIENTOTHCONTACT = self.otherContactList();
                ah.toggleSaveMode();
                //alert(JSON.stringify(postData));
                $.post(self.getApi() + ah.config.api.updateSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
            }
        }
    };

    self.UploadImage = function (REF_ID) {
        
    };

    self.openOtherContactModal = function () {
        self.modalOtherContact.CLIENTCONTACTTYPE('');
        self.modalOtherContact.CLIENTCONTACT('');

        window.location.href = ah.config.url.openOtherContactModal;
    };

    self.removeOtherContact = function (data) {
        self.otherContactList.remove(data);
    };

    self.addOtherContact = function () {
        self.otherContactList.push({
            CLIENTCONTACTTYPE: ko.observable(self.modalOtherContact.CLIENTCONTACTTYPE()),
            CLIENTCONTACT: ko.observable(self.modalOtherContact.CLIENTCONTACT()),
            CLIENTCONTACTTYPEVALUE: ko.observable(self.getContactTypeValue(self.modalOtherContact.CLIENTCONTACTTYPE()))
        });
        $(ah.config.cls.otherContactRemove).show();

        window.location.href = ah.config.url.modalClose;
    };

    self.getContactTypeValue = function (type) {
        switch (type) {
            case 'E':
                return 'Email';
                break;
            case 'FN':
                return 'Fax Number';
                break;
            case 'MN':
                return 'Mobile Number';
                break;
            case 'TN':
                return 'Telephone Number';
                break;
            case 'P':
                return 'Person';
                break;
            case 'PN':
                return 'Pager Number';
                break;
        }
    };

	self.webApiCallbackStatus = function (jsonData) {
		//alert(JSON.stringify(jsonData));
		if (jsonData.ERROR) {

			alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
			//window.location.href = ah.config.url.homeIndex;

		}
        else if (jsonData.AM_CLIENT_INFO || jsonData.STAFF || jsonData.SUCCESS || jsonData.CLIENT_LIST || jsonData.AM_CLIENT_COSTCENTER || jsonData.CLIENT_LOV || jsonData.STORE) {

			if (ah.CurrentMode == ah.config.mode.save) {
				sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_CLIENT_INFO));
				$(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
				ah.showSavingStatusSuccess();

                if (jsonData.SUCCESS) {
                    self.showDetails(false, jsonData);
				}
                else {
                    self.showDetails(true, jsonData);
				}
			}
			else if (ah.CurrentMode == ah.config.mode.read) {
                sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_CLIENT_INFO));
                clientCostCenterLOV = jsonData.CLIENT_LOV;
                clientCostCenterList = jsonData.AM_CLIENT_COSTCENTER;
                self.showDetails(true, jsonData);

                self.otherContactList.removeAll();
                if (jsonData.CLIENTOTHCONTACTS && jsonData.CLIENTOTHCONTACTS.length) {
                    for (var i = 0; i < jsonData.CLIENTOTHCONTACTS.length; i++) {
                        self.otherContactList.push({
                            CLIENTCONTACTTYPE: ko.observable(jsonData.CLIENTOTHCONTACTS[i].CLIENTCONTACTTYPE),
                            CLIENTCONTACT: ko.observable(jsonData.CLIENTOTHCONTACTS[i].CLIENTCONTACT),
                            CLIENTCONTACTTYPEVALUE: ko.observable(self.getContactTypeValue(jsonData.CLIENTOTHCONTACTS[i].CLIENTCONTACTTYPE))
                        });
                    }
                }

                $(ah.config.cls.otherContactRemove).hide();
			}
			else if (ah.CurrentMode == ah.config.mode.search) {
				self.searchResult(jsonData);
			}
			else if (ah.CurrentMode == ah.config.mode.idle) {

			}

		}
		else {
			alert(systemText.errorTwo);
			//window.location.href = ah.config.url.homeIndex;
		}

    };

    self.unsaveStore = {
        STORE_NO: ko.observable(""),
        DESCRIPTION: ko.observable(""),
        LOCATION: ko.observable(""),
        CONTACT_NO: ko.observable(""),
        CLIENT_CODE: ko.observable(""),
        CLIENT_NAME: ko.observable(""),
        STATUS: "ACTIVE",
        COSTCENTERFLAG: false
    };

    self.createNewStore = function () {
        var clientDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.infoDetails));
        self.unsaveStore.DESCRIPTION(clientDetails.DESCRIPTION + " Store");
        self.unsaveStore.CLIENT_CODE(clientDetails.CLIENT_CODE);
        self.unsaveStore.CLIENT_NAME(clientDetails.DESCRIPTION);
        $("#STORE_NO").prop(ah.config.attr.disabled, true);
        window.location.href = ah.config.url.createStoreModal;
    };

    self.saveStore = function () {
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        self.unsaveStore.CREATED_BY = staffLogonSessions.STAFF_ID;
        self.modifySetup();
        window.location.href = ah.config.url.modalClose;
    };

	self.initialize();
};