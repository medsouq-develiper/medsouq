﻿var appHelper = function (systemText) {
    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]'
        },
        mode: {
            idle: 0,

            homePage: 1,
            searchPage: 2,
            addOrEditPage: 3,

            modalGetStockOnHand: 4,
            modalSearchStockOnHand: 5,
            viewCustomerRequest: 6,
            addOrEditCustomerRequest: 7
        },
        id: {
            spanUser: '#spanUser',
            prSubmitBtn: '#prSubmitBtn',
            divNotify: '#divNotify',
        },

        cssCls: {
            viewMode: 'view-mode'
        },
        tagId: {
            txtGlobalSearchOth: 'txtGlobalSearchOth',
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfo: 'data-info',
            datainfoID: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            defaultCostCenter: '0ed52a42-9019-47d1-b1a0-3fed45f32b8f',
        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalClose: '#',
            openModalFilter: '#openModalFilter',
            openStockOnHandModal: '#openStockOnHandModal',
            openBillingOrVendorInfoModal: '#openBillingOrVendorInfoModal',
        },
        fld: {
            prNo: 'PRNO'
        },
        api: {
            searchLookup: '/Inventory/SearchStores',
            searchStockOnhand: '/Inventory/SearchStockOnhandFromStore',
            createRequest: '/Inventory/CreateNewStocktransaction',
            searchCustomerRequest: '/Inventory/SearchStockTransDetFlag',
            getCustomerRequestByRequest: '/Inventory/GetCustomerRequestByRequest',
            updateCustomerRequestStatus: '/Inventory/UpdateStorkreqStatus',
            getStoresLookup: '/Staff/GetStoresLookup',
            searchCostCenterDetails: '/Setup/SearchCostCenterDetails',
            searchSetupLov: '/Setup/SearchInventoryLOV',
            searchAssetsOnhand: '/AIMS/SearchAssetsOnhand',
            getSystemDefaultByCode: '/Setup/GetSystemDefaultByCode',
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
        }
    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };
};

/*Knockout MVVM Namespace - Controls form functionality*/
var amStockTransactionViewModel = function (systemText) {
    var ah = new appHelper(systemText);
    var self = this;
    var createFlag = false;
    var updateCustomerRequestStatusFlag = false;

    self.modes = ah.config.mode;
    self.pageMode = ko.observable();
    self.currentMode = ko.observable();

    self.showProgress = ko.observable(false);
    self.showProgressScreen = ko.observable(false);
    self.showModalProgress = ko.observable(false);

    self.progressText = ko.observable();
    self.contentHeader = ko.observable();
    self.contentSubHeader = ko.observable();

    self.modalQuickSearch = ko.observable();
    self.modalReason = ko.observable();
    self.filter = {
        pageQuickSearch: ko.observable(''),
        FromDate: ko.observable(''),
        ToDate: ko.observable(''),
        Status: ko.observable(''),
        FromStore: ko.observable(0),
        ToStore: ko.observable(0)
    };

    self.modalStores = ko.observableArray([]);
    self.requestingStores = ko.observableArray([]);
    self.requestingFromStores = ko.observableArray([]);
    self.requestingStoreSOH = ko.observableArray([]);
    self.requestingStoreSOHCopy = ko.observableArray([]);
    self.assignedSOH = ko.observableArray([]);
    self.customerRequests = ko.observableArray([]);
    self.customerRequest = {
        TRANS_ID: ko.observable(),
        REQUEST_DATE: ko.observable(),
        STORE_TO: ko.observable(),
        STORE_FROM: ko.observable(),
        REMARKS: ko.observable(),
        TRANS_STATUS: ko.observable(),

        STORE_TO_NAME: ko.observable(),
        STORE_FROM_NAME: ko.observable(),

        FOREIGN_CURRENCY: ko.observable(),
        SHIP_VIA: ko.observable(),
        TOTAL_AMOUNT: ko.observable(),
        TOTAL_NOITEMS: ko.observable(),
        SUPPLIER_ID: ko.observable(),
        SUPPLIERDESC: ko.observable(),
        SUPPLIER_ADDRESS1: ko.observable(),
        SUPPLIER_CITY: ko.observable(),
        SUPPLIER_STATE: ko.observable(),
        SUPPLIER_ZIPCODE: ko.observable(),
        SUPPLIER_CONTACT_NO1: ko.observable(),
        SUPPLIER_CONTACT_NO2: ko.observable(),
        SUPPLIER_CONTACT: ko.observable(),
        SUPPLIER_EMAIL_ADDRESS: ko.observable(),
        OTHER_INFO: ko.observable(),
        SHIP_DATE: ko.observable(),
        EST_SHIP_DATE: ko.observable(),
        HANDLING_FEE: ko.observable(),
        TRACKING_NO: ko.observable(),
        EST_FREIGHT: ko.observable(),
        FREIGHT: ko.observable(),
        FREIGHT_PAIDBY: ko.observable(),
        FREIGHT_CHARGETO: ko.observable(),
        TAXABLE: ko.observable(),
        TAX_CODE: ko.observable(),
        TAX_AMOUNT: ko.observable(),
        FREIGHT_POLINE: ko.observable(),
        FREIGHT_SEPARATE_INV: ko.observable(),
        INSURANCE_VALUE: ko.observable(),
        PO_BILLCODE_ADDRESS: ko.observable(),
        POINVOICENO: ko.observable(),
        DISCOUNT: ko.observable(),
    };

    self.signOut = function () {
        window.location.href = ah.config.url.homeIndex;
    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.toggleBoolean = function (property, passValue, mode) {
        var value = typeof passValue !== 'undefined' ? passValue : !self[property]();
        self[property](value);

        if (self[property](value) && mode) {
            switch (mode) {
                case 'page':
                    self.progressText('Please wait . . .');
                    break;
                case 'search':
                    self.progressText('Please wait while searching . . .');
                    break;
                case 'save':
                    self.progressText('Please wait while saving . . .');
                    break;
                case 'get':
                    self.progressText('Please wait while getting Customer Purchase Request Information . . .');
                    break;
            }
        }
    };
    self.setObservableValue = function (_object1, _object2) {
        Object.keys(_object2).forEach(function (key) {
            if (ko.isObservable(_object1[key]))
                _object1[key](_object2[key])
            else
                _object1[key] = _object2[key];
        });
    };
    self.getObservableValue = function (data) {
        if (Array.isArray(data)) {
            var returnData = [];
            for (var i = 0; i < data.length; i++) {
                var _object = data[i];
                returnData.push({});
                Object.keys(_object).forEach(function (key) {
                    if (ko.isObservable(_object[key]))
                        returnData[i][key] = _object[key]();
                    else
                        returnData[i][key] = _object[key];
                });
            }
        }
        else if (data instanceof Object) {
            var returnData = {};
            var _object = data;
            Object.keys(_object).forEach(function (key) {
                if (ko.isObservable(_object[key]))
                    returnData[key] = _object[key]();
                else
                    returnData[key] = _object[key];
            });
        }
        else
            returnData = data;

        return returnData;
    };
    self.checkViewMode = function () {
        return self.currentMode() != self.modes.viewCustomerRequest
    };

    self.createNew = function () {
        createFlag = true;
        self.pageMode(self.modes.addOrEditPage);
        self.currentMode(self.modes.idle);
        self.contentHeader(systemText.addModeHeader)
        self.contentSubHeader(systemText.addModeSubHeader);

        self.setObservableValueEmpty(self.customerRequest);

        self.getDefaultCostCenter();

        self.customerRequest.STORE_FROM(self.DefaultCostCenter.STORE_ID);
        self.customerRequest.TRANS_STATUS('OPEN');
        self.customerRequest.REQUEST_DATE(moment().format('DD/MM/YYYY HH:mm'));

        self.resetAssignedSOH();
    };
    self.editCustomerRequest = function () {
        createFlag = false;
        self.currentMode(self.modes.idle);
        self.contentSubHeader(systemText.addModeSubHeader);
    };
    self.updateCustomerRequestStatus = function (status) {
        self.currentMode(self.modes.viewCustomerRequest);
        self.toggleBoolean('showProgress', true, 'save');
        self.toggleBoolean('showProgressScreen', true);
        updateCustomerRequestStatusFlag = true;

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var AM_STOCKTRANSACTION = self.getObservableValue(self.customerRequest);

        AM_STOCKTRANSACTION.TRANS_STATUS = status;
        if (AM_STOCKTRANSACTION.TRANS_STATUS === "REOPEN") {
            AM_STOCKTRANSACTION.TRANS_STATUS = "OPEN";
        }
        AM_STOCKTRANSACTION.REQUEST_DATE = moment(AM_STOCKTRANSACTION.REQUEST_DATE, 'DD/MM/YYYY HH:mm').format();

        postData = { TRANS_ID: AM_STOCKTRANSACTION.TRANS_ID, AM_STOCKTRANSACTIONID: AM_STOCKTRANSACTION.AM_STOCKTRANSACTION_MASTERID, STATUS: AM_STOCKTRANSACTION.TRANS_STATUS, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.updateCustomerRequestStatus, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.clearModalFilter = function () {
        self.filter.FromDate('');
        self.filter.ToDate('');
        self.filter.Status('');
        self.filter.FromStore(0);
        self.filter.ToStore(0);
        $('#modalFromDate').val('');
        $('#modalToDate').val('');
    };
    self.openModalFilter = function () {
        window.location.href = ah.config.url.openModalFilter;
    };
    self.filterSearch = function () {
        self.pageMode(self.modes.searchPage);
        self.toggleBoolean('showProgress', true, 'search');
        self.filter.pageQuickSearch('');
        var FromDate = $('#modalFromDate').val();
        var ToDate = $('#modalToDate').val();
        if (moment(FromDate, 'DD/MM/YYYY', true).isValid())
            self.filter.FromDate(moment(FromDate, 'DD/MM/YYYY').format())
        if (moment(ToDate, 'DD/MM/YYYY', true).isValid())
            self.filter.ToDate(moment(ToDate, 'DD/MM/YYYY').format())

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var filters = self.getObservableValue(self.filter);
        filters.FromStore = filters.FromStore ? filters.FromStore : 0;
        filters.ToStore = filters.ToStore ? filters.ToStore : 0;

        var postData = { SEARCH: "", RETACTION: 'Y', TRANS_STATUS: null, PAGE: '', SEARCH_FILTER: filters, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchCustomerRequest, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.quickSearch = function () {
        self.pageMode(self.modes.searchPage);
        self.toggleBoolean('showProgress', true, 'search');

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        var postData = { SEARCH: self.filter.pageQuickSearch, RETACTION: 'Y', TRANS_STATUS: null, PAGE: '', SEARCH_FILTER: { FromDate: "" }, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchCustomerRequest, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.selectCustomerRequest = function (data) {
        var selectedCustomerRequest = data;
        self.pageMode(self.modes.addOrEditPage);
        self.currentMode(self.modes.viewCustomerRequest);
        self.toggleBoolean('showProgress', true, 'get');
        self.contentHeader(systemText.updateModeHeader)
        self.contentSubHeader(systemText.updateModeSubHeader);

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        var postData = { TRANS_ID: selectedCustomerRequest.TRANS_ID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.getCustomerRequestByRequest, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.openStockOnHandModal = function () {
        if (!self.customerRequest.STORE_TO())
            return alert('Unable to Proceed. Please select Bill To/Vendor Information to proceed.');
        if (!self.customerRequest.STORE_FROM())
            return alert('Unable to Proceed. Please select Requesting From Store to proceed.');
        if (self.customerRequest.STORE_TO() == self.customerRequest.STORE_FROM())
            return alert("Unable to Proceed. The Requesting Store and Requesting From Store should not the same.");

        self.currentMode(self.modes.modalGetStockOnHand);
        self.toggleBoolean('showProgress', true, 'search');

        self.customerRequest.STORE_FROM_NAME(self.requestingFromStores().find(function (item) { return item.STORE_ID == self.customerRequest.STORE_FROM() }).DESCRIPTION);
        self.customerRequest.STORE_TO_NAME(self.requestingStores().find(function (item) { return item.STORE_ID == self.customerRequest.STORE_TO() }).DESCRIPTION);

        self.modalQuickSearch('');

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData = { SEARCH: "", ACTION: "", STORE_FROM: self.customerRequest.STORE_FROM(), STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchStockOnhand, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.modalSearchStock = function (val) {
        self.modalActiveSearching(val);
        self.requestingStoreSOH.removeAll();
        var key = (self.modalQuickSearch() || '').toLowerCase();
        if (self.modalActiveSearching() == 1) {
            if (key)
                self.requestingStoreSOH(self.requestingStoreSOHCopy().filter(function (item) {
                    var idParts = (item.ID_PARTS || '').toLowerCase();
                    var description = (item.DESCRIPTION || '').toLowerCase();
                    var category = (item.CATEGORY || '').toLowerCase();

                    return idParts.includes(key) || description.includes(key) || category.includes(key)
                }));
            else
                self.requestingStoreSOH(self.requestingStoreSOHCopy.slice(0));
        }
        else {
            self.currentMode(self.modes.modalSearchStockOnHand);
            self.toggleBoolean('showModalProgress', true);

            self.getDefaultCostCenter();

            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var postData = { SEARCH: self.modalQuickSearch(), SEARCH_FILTER: { AssetFilterCostCenter: self.DefaultCostCenter.CODE }, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchAssetsOnhand, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
    };
    self.modalAddAllSOH = function () {
        var oldAssignSOH = self.assignedSOH().slice(0);
        for (var i = 0; i < self.requestingStoreSOH().length; i++) {
            var found = oldAssignSOH.find(function (item) { return item.ID_PARTS == self.requestingStoreSOH()[i].ID_PARTS });
            if (!found) {
                var selectedItem = self.requestingStoreSOH()[i];

                var item = {
                    DESCRIPTION: selectedItem.DESCRIPTION,
                    ID_PARTS: selectedItem.ID_PARTS,
                    QTY: ko.observable(1),
                    REQUEST_DATE: moment(self.customerRequest.REQUEST_DATE(), 'DD/MM/YYYY HH:mm').format(),
                    TRANS_TYPE: self.modalActiveSearching() == 1 ? 'SOH' : 'ASSET',
                    STORE_FROM: self.customerRequest.STORE_FROM(),
                    STORE_TO: self.customerRequest.STORE_TO(),
                    TRANS_STATUS: self.customerRequest.TRANS_STATUS(),
                    AM_STOCKTRANSACTIONID: 0,
                    TRANS_ID: self.customerRequest.TRANS_ID,
                    REMARKS: self.customerRequest.REMARKS,
                    TRANSDETFLAG: '',

                    AM_UOM: selectedItem.AM_UOM,
                    STOCK_MIN: selectedItem.MIN,
                    STOCK_MAX: selectedItem.MAX,
                    PRICE: ko.observable(0),
                    TOTAL_PRICE: ko.observable(),
                    VAT_FLAG: ko.observable(false),
                    VAT: ko.observable(''),
                };
                self.calculateTotalPrice(item);
                self.assignedSOH.push(item);
            }
        }
    };
    self.resetAssignedSOH = function () {
        self.assignedSOH([]);
    };
    self.assignStock = function () {
        var selectedItem = this;
        var found = self.assignedSOH().find(function (item) { return item.ID_PARTS == selectedItem.ID_PARTS });
        if (found) return alert('Unable to Proceed. The selected item was already exist.');
        var item = {
            DESCRIPTION: selectedItem.DESCRIPTION,
            ID_PARTS: selectedItem.ID_PARTS,
            QTY: ko.observable(1),
            REQUEST_DATE: moment(self.customerRequest.REQUEST_DATE(), 'DD/MM/YYYY HH:mm').format(),
            TRANS_TYPE: self.modalActiveSearching() == 1 ? 'SOH' : 'ASSET',
            STORE_FROM: self.customerRequest.STORE_FROM(),
            STORE_TO: self.customerRequest.STORE_TO(),
            TRANS_STATUS: self.customerRequest.TRANS_STATUS(),
            AM_STOCKTRANSACTIONID: 0,
            TRANS_ID: self.customerRequest.TRANS_ID,
            REMARKS: self.customerRequest.REMARKS,
            TRANSDETFLAG: '',

            AM_UOM: selectedItem.AM_UOM,
            STOCK_MIN: selectedItem.MIN,
            STOCK_MAX: selectedItem.MAX,
            PRICE: ko.observable(0),
            TOTAL_PRICE: ko.observable(),
            VAT_FLAG: ko.observable(false),
            VAT: ko.observable(''),
        };
        self.calculateTotalPrice(item);
        self.assignedSOH.push(item);
    };
    self.removeAssignStock = function () {
        var selectedItem = this;
        self.assignedSOH.remove(selectedItem);
    };
    self.cancelChanges = function () {
        if (createFlag)
            self.pageMode(self.modes.homePage);
        else
            self.selectCustomerRequest(self.getObservableValue(self.customerRequest));
    };
    self.triggerFormSubmit = function () {
        $(ah.config.id.prSubmitBtn).trigger('click');
    };
    self.saveAllChanges = function () {
        if (!self.assignedSOH().length)
            return alert('Unable to proceed. No item to request. Please click on Add item button and select stock you wish to request.');

        var found = self.assignedSOH().find(function (item) { return !item.QTY() });
        if (found)
            return alert('Please make sure that the ' + found.DESCRIPTION + ' qty value is greater than 0.');

        var found = self.assignedSOH().find(function (item) { return Number(item.QTY()) < 0 });
        if (found)
            return alert('Unable to proceed. Item name: ' + found.DESCRIPTION + ', have negative request qty. Please make sure that there is no negative qty.');

        self.currentMode(self.modes.addOrEditCustomerRequest);
        self.toggleBoolean('showProgress', true, 'save');
        self.toggleBoolean('showProgressScreen', true);

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        var AM_STOCKTRANSACTION = self.getObservableValue(self.customerRequest);
        AM_STOCKTRANSACTION.REQUEST_DATE = moment(AM_STOCKTRANSACTION.REQUEST_DATE, 'DD/MM/YYYY HH:mm').format();
        var AM_STOCKTRANSACTIONS = self.assignedSOH();

        var postData = { ACTION: "", CREATE_DATE: AM_STOCKTRANSACTION.REQUEST_DATE, AM_STOCKTRANSACTION_MASTER: AM_STOCKTRANSACTION, AM_STOCKTRANSACTION: AM_STOCKTRANSACTIONS, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.createRequest, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.initialize = function () {
        window.location.href = ah.config.url.modalClose;

        self.pageMode(self.modes.homePage);
        self.toggleBoolean('showProgress', true, 'page');
        self.toggleBoolean('showProgressScreen', true);

        $(ah.config.id.divNotify).css('display', 'none');

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        var postData = { LOV: "'SHIP_VIA','FOREIGN_CURRENCY'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.when($.post(self.getApi() + ah.config.api.searchSetupLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus)).done(function () {
            var postData = { STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.getStoresLookup, postData).done(function (jsonData) {
                var requestingStores = jsonData.STORES || [];
                requestingStores = requestingStores.filter(function (item) { return item.ALLOWREQUEST_FLAG != "Y" });
                requestingStores.unshift({
                    STORE_ID: null,
                    DESCRIPTION: '- select -'
                });
                self.requestingStores(requestingStores);

                var requestingFromStores = jsonData.STORES || [];
                requestingFromStores = requestingFromStores.filter(function (item) { return item.ALLOWREQUEST_FLAG == "Y" });
                requestingFromStores.unshift({
                    STORE_ID: null,
                    DESCRIPTION: '- select -'
                });
                self.requestingFromStores(requestingFromStores);

                var modalStores = jsonData.STORES || [];
                modalStores.unshift({
                    STORE_ID: null,
                    DESCRIPTION: '- select -'
                });
                self.modalStores(modalStores);
            }).fail(self.webApiCallbackStatus);

            var postData = { code: 'DEFVAT', STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.getSystemDefaultByCode, postData).done(function (jsonData) {
                self.defaultVat = jsonData.SYSTEM_DEFAULT || {};
            }).fail(self.webApiCallbackStatus);
        });
    };


    self.defaultVat = {};
    self.DefaultCostCenter = {};
    self.shipVias = ko.observableArray([]);
    self.foreignCurrencies = ko.observableArray([]);
    self.modalQuickSearch2 = ko.observable();
    self.modalActiveSearching = ko.observable(1);
    self.billingOrVendorList = ko.observableArray([]);
    self.setObservableValueEmpty = function (object) {
        Object.keys(object).forEach(function (key) {
            if (ko.isObservable(object[key]))
                object[key]('')
            else
                object[key] = '';
        });
    }
    self.openBillingOrVendorInfoModal = function () {
        self.modalQuickSearch2('');

        self.modalSearchBillingOrVendor();
        window.location.href = ah.config.url.openBillingOrVendorInfoModal;
    };
    self.modalSearchBillingOrVendor = function () {
        self.billingOrVendorList.removeAll();
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        self.toggleBoolean('showModalProgress', true);

        var postData = { SEARCH: self.modalQuickSearch2(), STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchCostCenterDetails, postData).done(function (jsonData) {
            self.getDefaultCostCenter();

            self.billingOrVendorList((jsonData.COSTCENTER_LIST || []).filter(function (x) { return x.STORE_ID != self.DefaultCostCenter.STORE_ID }));
            self.toggleBoolean('showModalProgress', false);
        }).fail(self.webApiCallbackStatus);
    };
    self.assignBillingOrVendorInfo = function (item) {
        self.customerRequest.SUPPLIER_ID(item.CODEID);
        self.customerRequest.SUPPLIERDESC(item.DESCRIPTION);
        self.customerRequest.SUPPLIER_ADDRESS1(item.ADDRESS);
        self.customerRequest.SUPPLIER_CITY(item.CITY);
        self.customerRequest.SUPPLIER_ZIPCODE(item.ZIP);
        self.customerRequest.SUPPLIER_STATE(item.STATE);
        self.customerRequest.SUPPLIER_CONTACT_NO1(item.CONTACTNO);
        self.customerRequest.SUPPLIER_CONTACT_NO2(item.CONTACTNO2);
        self.customerRequest.SUPPLIER_EMAIL_ADDRESS(item.EMAILADDRESS);

        self.customerRequest.STORE_TO(item.STORE_ID);

        window.location.href = ah.config.url.modalClose;
    };
    self.checkItemVat = function (item) {
        if (!item.VAT_FLAG())
            item.VAT('');
        else
            item.VAT(Number(self.defaultVat.SYSDEFAULTVAL));
    };
    self.calculateTotalPrice = function (item, isPrice) {
        item.TOTAL_PRICE((item.PRICE() || 0) * item.QTY());

        item.TOTAL_PRICE(Math.round(item.TOTAL_PRICE() * 100) / 100);

        self.calculateTotal();
    };
    self.calculateTotal = function () {
        var totalAmountPrice = 0;
        totalAmountPrice = self.assignedSOH().reduce(function (acc, val) { return acc + val.TOTAL_PRICE(); }, 0);
        var inctoPO = self.customerRequest.FREIGHT_POLINE();
        var freightCharge = self.customerRequest.FREIGHT_CHARGETO();
        if (inctoPO == "YES" && freightCharge == "YES") {
            totalAmountPrice = totalAmountPrice + Number(self.customerRequest.FREIGHT());
        } else if (freightCharge == "NO") {
            self.customerRequest.FREIGHT_POLINE(null);
            self.customerRequest.FREIGHT(0);
        }

        totalAmountPrice = Math.round(totalAmountPrice * 100) / 100;
        self.customerRequest.TOTAL_AMOUNT(totalAmountPrice);
    };
    self.getDefaultCostCenter = function () {
        var dCC = JSON.parse(sessionStorage.getItem(ah.config.skey.defaultCostCenter));
        if (dCC.length > 0) {
            self.DefaultCostCenter.CODE = dCC[0].CODEID;
            self.DefaultCostCenter.DESCRIPTION = dCC[0].DESCRIPTION;
            self.DefaultCostCenter.STORE_ID = dCC[0].STORE_ID;
        }
    };

    self.assignedSOHPrint = ko.observableArray([]);
    self.clientDetails = {
        CLIENTNAME: ko.observable(),
        CLIENT_LOGO: ko.observable(),
        COMPANY_LOGO: ko.observable(),
        HEADING: ko.observable(),
        HEADING2: ko.observable(),
        HEADING3: ko.observable(),
        HEADING_OTH: ko.observable(),
        HEADING_OTH2: ko.observable(),
        HEADING_OTH3: ko.observable(),
        CLIENTADDRESS: ko.observable(),
        CLIENTCONTACT: ko.observable()
    };
    self.printPurchaseRequest = function () {
        self.clientDetails.HEADING('Business Schema Trading Company');
        self.clientDetails.COMPANY_LOGO(window.location.origin + '/images/companylogo.png');

        self.assignedSOHPrint.removeAll();
        self.assignedSOHPrint(self.assignedSOH.slice(0));
        if (self.customerRequest.FREIGHT_POLINE() == 'YES') {
            self.assignedSOHPrint.push({
                DESCRIPTION: 'FREIGHT CHARGES',
                QTY: 1,
                PRICE: ko.observable(self.customerRequest.FREIGHT() || 0),
                TOTAL_PRICE: ko.observable(self.customerRequest.FREIGHT() || 0),
            });
        }

        setTimeout(function () {
            window.print();
        }, 1000)
    };
    self.getLookupDescription = function (value, array) {
        return value && array.length && array.find(function (x) { return x.LOV_LOOKUP_ID == value }).DESCRIPTION;
    };
    self.getPODate = function () {
        return moment(self.customerRequest.REQUEST_DATE(), 'DD/MM/YYYY').format('DD-MMM-YYYY');
    };
    self.getSubtotalPrice = function () {
        var itemTotalPrice = self.assignedSOHPrint().reduce(function (acc, val) { return acc + val.TOTAL_PRICE(); }, 0);
        return Math.round(itemTotalPrice * 100) / 100;
    };
    self.getDiscountPrice = function () {
        var itemDiscountPrice = self.getSubtotalPrice() * (Number(self.customerRequest.DISCOUNT()) / 100);
        return Math.round(itemDiscountPrice * 100) / 100;
    };
    self.getVatDescription = function () {
        var array = self.assignedSOHPrint.slice(0).filter(function (x) { return ko.utils.unwrapObservable(x.VAT_FLAG) });
        if (array.length) {
            var filtered = array.reduce(function (x, y) { return x.findIndex(function (e) { return ko.utils.unwrapObservable(e.VAT) == ko.utils.unwrapObservable(y.VAT); }) < 0 ? [].concat(x, [y]) : x; }, []);
            if (self.customerRequest.FREIGHT_POLINE() == 'YES') {
                filtered = filtered.filter(function (x) { return ko.utils.unwrapObservable(x.ID_PARTS) });
                return getVatText();
            }

            if (filtered.length > 1) {
                return getVatText();
            }
            else {
                return 'VAT ' + self.assignedSOHPrint()[0].VAT() + '%';
            }
        }

        function getVatText() {
            var vatText = 'VAT ';
            filtered.forEach(function (item) {
                var foundFilter = array.filter(function (x) { return ko.utils.unwrapObservable(item.VAT) == ko.utils.unwrapObservable(x.VAT) });
                foundFilter.forEach(function (item2) {
                    vatText += ko.utils.unwrapObservable(item2.ID_PARTS) + ', ';
                });
                vatText = vatText.substr(0, vatText.length - 2) + ' ';
                vatText += ko.utils.unwrapObservable(item.VAT) + '%, ';
            });
            vatText = vatText.substr(0, vatText.length - 2) + ' ';
            return vatText;
        }
    };
    self.getVatAmount = function () {
        var vatAmount = 0;
        self.assignedSOHPrint().forEach(function (item) {
            if (ko.utils.unwrapObservable(item.VAT_FLAG)) {
                vatAmount = vatAmount + (ko.utils.unwrapObservable(item.TOTAL_PRICE) * (ko.utils.unwrapObservable(item.VAT) / 100));
            }
        });
        return Math.round(vatAmount * 100) / 100;
    };
    self.getTotalPrice = function () {
        var itemTotalPrice = self.assignedSOHPrint().reduce(function (acc, val) { return acc + val.TOTAL_PRICE(); }, 0);
        itemTotalPrice = (itemTotalPrice - Number(self.getDiscountPrice())) + Number(self.getVatAmount());
        return Math.round(itemTotalPrice * 100) / 100;
    };

    self.webApiCallbackStatus = function (jsonData) {
        if (jsonData.ERROR) {
            self.toggleBoolean('showProgress', false);
            self.toggleBoolean('showProgressScreen', false);

            $(ah.config.id.divNotify).css('display', 'none');

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
        }
        else if (!jsonData.ERROR) {
            switch (self.pageMode()) {
                case self.modes.searchPage:
                    self.toggleBoolean('showProgress', false);
                    self.customerRequests(jsonData.AM_STOCKTRANSACTION);

                    window.location.href = ah.config.url.modalClose;
                    break;

                case self.modes.homePage:
                    self.toggleBoolean('showProgress', false);
                    self.toggleBoolean('showProgressScreen', false);

                    var shipVias = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'SHIP_VIA' });;
                    shipVias.unshift({
                        LOV_LOOKUP_ID: null,
                        DESCRIPTION: '- select -'
                    });
                    self.shipVias(shipVias);

                    var foreignCurrencies = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'FOREIGN_CURRENCY' });;
                    foreignCurrencies.unshift({
                        LOV_LOOKUP_ID: null,
                        DESCRIPTION: '- select -'
                    });
                    self.foreignCurrencies(foreignCurrencies);
                    break;

                case self.modes.addOrEditPage:

                    switch (self.currentMode()) {
                        case self.modes.modalGetStockOnHand:
                            self.toggleBoolean('showProgress', false);
                            self.requestingStoreSOH(jsonData.PRODUCT_STOCKONHAND);
                            self.requestingStoreSOHCopy(self.requestingStoreSOH.slice(0));

                            window.location.href = ah.config.url.openStockOnHandModal;
                            break;

                        case self.modes.modalSearchStockOnHand:
                            self.toggleBoolean('showModalProgress', false);
                            if (jsonData.AM_ASSET_ONHAND) {
                                for (var i = 0; i < jsonData.AM_ASSET_ONHAND.length; i++) {
                                    self.requestingStoreSOH.push({
                                        ID_PARTS: jsonData.AM_ASSET_ONHAND[i].PRODUCT_CODE,
                                        PRODUCT_CODE: jsonData.AM_ASSET_ONHAND[i].PRODUCT_CODE,
                                        DESCRIPTION: jsonData.AM_ASSET_ONHAND[i].DESCRIPTION,
                                        AM_UOM: 'Piece',
                                        QTY: jsonData.AM_ASSET_ONHAND[i].ONHANDCNT
                                    });
                                }
                            }
                            else if (jsonData.PRODUCT_STOCKONHAND)
                                self.requestingStoreSOH(jsonData.PRODUCT_STOCKONHAND);
                            break;

                        case self.modes.addOrEditCustomerRequest:
                            self.currentMode(self.modes.viewCustomerRequest);
                            self.toggleBoolean('showProgress', false);
                            self.toggleBoolean('showProgressScreen', false);

                            $(ah.config.id.divNotify).css('display', 'block');
                            $(ah.config.id.divNotify + ' p').html(systemText.saveSuccessMessage);
                            setTimeout(function () {
                                $(ah.config.id.divNotify).fadeOut(1000);
                            }, 3000);

                            self.selectCustomerRequest(jsonData.AM_STOCKTRANSACTION[0]);
                            break;

                        case self.modes.viewCustomerRequest:
                            if (updateCustomerRequestStatusFlag) {
                                updateCustomerRequestStatusFlag = false;
                                self.toggleBoolean('showProgressScreen', false);

                                $(ah.config.id.divNotify).css('display', 'block');
                                $(ah.config.id.divNotify + ' p').html(systemText.saveSuccessMessage);
                                setTimeout(function () {
                                    $(ah.config.id.divNotify).fadeOut(1000);
                                }, 3000);

                                self.selectCustomerRequest(self.getObservableValue(self.customerRequest));
                            }
                            else {
                                self.toggleBoolean('showProgress', false);
                                jsonData.AM_STOCKTRANSACTIONS = jsonData.AM_STOCKTRANSACTIONS || [];
                                for (var i = 0; i < jsonData.AM_STOCKTRANSACTIONS.length; i++) {
                                    jsonData.AM_STOCKTRANSACTIONS[i].QTY = ko.observable(jsonData.AM_STOCKTRANSACTIONS[i].QTY);
                                    jsonData.AM_STOCKTRANSACTIONS[i].PRICE = ko.observable(jsonData.AM_STOCKTRANSACTIONS[i].PRICE);
                                    jsonData.AM_STOCKTRANSACTIONS[i].TOTAL_PRICE = ko.observable(0);
                                    jsonData.AM_STOCKTRANSACTIONS[i].VAT_FLAG = ko.observable(jsonData.AM_STOCKTRANSACTIONS[i].VAT_FLAG || false);
                                    jsonData.AM_STOCKTRANSACTIONS[i].VAT = ko.observable(jsonData.AM_STOCKTRANSACTIONS[i].VAT);
                                    self.calculateTotalPrice(jsonData.AM_STOCKTRANSACTIONS[i]);
                                }

                                self.setObservableValue(self.customerRequest, jsonData.AM_STOCKTRANSACTION);
                                self.customerRequest.REQUEST_DATE(moment(self.customerRequest.REQUEST_DATE()).format('DD/MM/YYYY HH:mm'));
                                self.assignedSOH(jsonData.AM_STOCKTRANSACTIONS);

                                self.assignedSOHPrint.removeAll();
                                self.assignedSOHPrint(self.assignedSOH.slice(0));
                                if (self.customerRequest.FREIGHT_POLINE() == 'YES') {
                                    self.assignedSOHPrint.push({
                                        DESCRIPTION: 'FREIGHT CHARGES',
                                        QTY: 1,
                                        PRICE: ko.observable(self.customerRequest.FREIGHT() || 0),
                                        TOTAL_PRICE: ko.observable(self.customerRequest.FREIGHT() || 0),
                                    });
                                }
                            }
                            break;
                    }
                    break;
            }
        }
        else {
            self.toggleBoolean('showProgress', false);
            self.toggleBoolean('showProgressScreen', false);

            $(ah.config.id.divNotify).css('display', 'none');

            alert(systemText.errorTwo);
        }
    };

    self.initialize();
};