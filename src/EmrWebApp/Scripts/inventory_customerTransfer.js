﻿var appHelper = function (systemText) {
    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]'
        },
        mode: {
            idle: 0,

            homePage: 1,
            searchPage: 2,
            transactionPage: 3,

            viewCustomerRequest: 4,
            getStockBatchByProdStore: 5,
            transferCustomerRequest: 6,
        },
        id: {
            spanUser: '#spanUser',
            prSubmitBtn: '#prSubmitBtn',
            divNotify: '#divNotify',
        },

        cssCls: {
            viewMode: 'view-mode'
        },
        tagId: {
            txtGlobalSearchOth: 'txtGlobalSearchOth',
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfo: 'data-info',
            datainfoID: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            defaultCostCenter: '0ed52a42-9019-47d1-b1a0-3fed45f32b8f',
        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalClose: '#',
            openModalFilter: '#openModalFilter',
            openStockBatchModal: '#openStockBatchModal'
        },
        fld: {
            prNo: 'PRNO'
        },
        api: {
            transferCustomerRequest: '/Inventory/TransferCustomerRequest',
            searchCustomerRequest: '/Inventory/SearchStockTransDetFlag',
            getCustomerRequestByRequest: '/Inventory/GetCustomerRequestByRequest',
            getStoresLookup: '/Staff/GetStoresLookup',
            getStockBatchByProdStore: '/Inventory/GetStockBatchByProdStore',
            searchSetupLov: '/Setup/SearchInventoryLOV',
            searchAssetsOnhand: '/AIMS/SearchAssetsOnhand',
            searchAssets: '/AIMS/SearchAssets'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
        }
    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };
};

/*Knockout MVVM Namespace - Controls form functionality*/
var amStockTransferViewModel = function (systemText) {
    var ah = new appHelper(systemText);
    var self = this;

    self.modes = ah.config.mode;
    self.pageMode = ko.observable();
    self.currentMode = ko.observable();

    self.showProgress = ko.observable(false);
    self.showProgressScreen = ko.observable(false);
    self.showModalProgress = ko.observable(false);

    self.progressText = ko.observable();

    self.modalQuickSearch = ko.observable();
    self.filter = {
        pageQuickSearch: ko.observable(''),
        FromDate: ko.observable(''),
        ToDate: ko.observable(''),
        Status: ko.observable(''),
        FromStore: ko.observable(0),
        ToStore: ko.observable(0)
    };

    self.stores = ko.observableArray([]);
    self.assignedSOH = ko.observableArray([]);
    self.selectedItem = ko.observable({
        stockBatchs: ko.observableArray([]),
        totalStockDeductable: ko.observable(0)
    });
    self.customerRequests = ko.observableArray([]);
    //self.customerRequest = {
    //    TRANS_ID: ko.observable(),
    //    REQUEST_DATE: ko.observable(),
    //    STORE_TO: ko.observable(),
    //    STORE_FROM: ko.observable(),
    //    REMARKS: ko.observable(),
    //    TRANS_STATUS: ko.observable(),
    //    INVOICENO: ko.observable(),
    //    TRANSFER_REMARKS: ko.observable(),
    //};
    self.customerRequest = {
        TRANS_ID: ko.observable(),
        REQUEST_DATE: ko.observable(),
        STORE_TO: ko.observable(),
        STORE_FROM: ko.observable(),
        REMARKS: ko.observable(),
        TRANS_STATUS: ko.observable(),
        TRANSFER_REMARKS: ko.observable(),

        STORE_TO_NAME: ko.observable(),
        STORE_FROM_NAME: ko.observable(),

        FOREIGN_CURRENCY: ko.observable(),
        SHIP_VIA: ko.observable(),
        TOTAL_AMOUNT: ko.observable(),
        TOTAL_NOITEMS: ko.observable(),
        SUPPLIER_ID: ko.observable(),
        SUPPLIERDESC: ko.observable(),
        SUPPLIER_ADDRESS1: ko.observable(),
        SUPPLIER_CITY: ko.observable(),
        SUPPLIER_STATE: ko.observable(),
        SUPPLIER_ZIPCODE: ko.observable(),
        SUPPLIER_CONTACT_NO1: ko.observable(),
        SUPPLIER_CONTACT_NO2: ko.observable(),
        SUPPLIER_CONTACT: ko.observable(),
        SUPPLIER_EMAIL_ADDRESS: ko.observable(),
        OTHER_INFO: ko.observable(),
        SHIP_DATE: ko.observable(),
        EST_SHIP_DATE: ko.observable(),
        HANDLING_FEE: ko.observable(),
        TRACKING_NO: ko.observable(),
        EST_FREIGHT: ko.observable(),
        FREIGHT: ko.observable(),
        FREIGHT_PAIDBY: ko.observable(),
        FREIGHT_CHARGETO: ko.observable(),
        TAXABLE: ko.observable(),
        TAX_CODE: ko.observable(),
        TAX_AMOUNT: ko.observable(),
        FREIGHT_POLINE: ko.observable(),
        FREIGHT_SEPARATE_INV: ko.observable(),
        INSURANCE_VALUE: ko.observable(),
        PO_BILLCODE_ADDRESS: ko.observable(),
        POINVOICENO: ko.observable(),
        DISCOUNT: ko.observable(),
    };

    self.signOut = function () {
        window.location.href = ah.config.url.homeIndex;
    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.toggleBoolean = function (property, passValue, mode) {
        var value = typeof passValue !== 'undefined' ? passValue : !self[property]();
        self[property](value);

        if (self[property](value) && mode) {
            switch (mode) {
                case 'page':
                    self.progressText('Please wait . . .');
                    break;
                case 'search':
                    self.progressText('Please wait while searching . . .');
                    break;
                case 'save':
                    self.progressText('Please wait while saving . . .');
                    break;
                case 'get':
                    self.progressText('Please wait while getting Customer Transfer Information . . .');
                    break;
            }
        }
    };
    self.setObservableValue = function (_object1, _object2) {
        Object.keys(_object2).forEach(function (key) {
            if (ko.isObservable(_object1[key]))
                _object1[key](_object2[key])
            else
                _object1[key] = _object2[key];
        });
    };
    self.getObservableValue = function (data) {
        if (Array.isArray(data)) {
            var returnData = [];
            for (var i = 0; i < data.length; i++) {
                var _object = data[i];
                returnData.push({});
                Object.keys(_object).forEach(function (key) {
                    if (ko.isObservable(_object[key]))
                        returnData[i][key] = _object[key]();
                    else
                        returnData[i][key] = _object[key];
                });
            }
        }
        else if (data instanceof Object) {
            var returnData = {};
            var _object = data;
            Object.keys(_object).forEach(function (key) {
                if (ko.isObservable(_object[key]))
                    returnData[key] = _object[key]();
                else
                    returnData[key] = _object[key];
            });
        }
        else
            returnData = data;

        return returnData;
    };
    self.checkViewMode = function () {
        return self.customerRequest.TRANS_STATUS() != 'TRANSFER'
    };

    self.clearModalFilter = function () {
        self.filter.FromDate('');
        self.filter.ToDate('');
        self.filter.Status('');
        self.filter.FromStore(0);
        self.filter.ToStore(0);
        $('#modalFromDate').val('');
        $('#modalToDate').val('');
    };
    self.openModalFilter = function () {
        window.location.href = ah.config.url.openModalFilter;
    };
    self.filterSearch = function () {
        self.pageMode(self.modes.searchPage);
        self.toggleBoolean('showProgress', true, 'search');
        self.filter.pageQuickSearch('');
        var FromDate = $('#modalFromDate').val();
        var ToDate = $('#modalToDate').val();
        if (moment(FromDate, 'DD/MM/YYYY', true).isValid())
            self.filter.FromDate(moment(FromDate, 'DD/MM/YYYY').format())
        if (moment(ToDate, 'DD/MM/YYYY', true).isValid())
            self.filter.ToDate(moment(ToDate, 'DD/MM/YYYY').format())

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var filters = self.getObservableValue(self.filter);
        filters.FromStore = filters.FromStore ? filters.FromStore : 0;
        filters.ToStore = filters.ToStore ? filters.ToStore : 0;

        var postData = { SEARCH: "", RETACTION: 'Y', TRANS_STATUS: null, PAGE: 'TRANSFER', SEARCH_FILTER: filters, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchCustomerRequest, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.quickSearch = function () {
        self.pageMode(self.modes.searchPage);
        self.toggleBoolean('showProgress', true, 'search');

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        var postData = { SEARCH: self.filter.pageQuickSearch, RETACTION: 'Y', TRANS_STATUS: null, PAGE: 'TRANSFER', SEARCH_FILTER: { FromDate: "" }, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchCustomerRequest, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.selectCustomerRequest = function (data) {
        var selectedCustomerRequest = data;
        self.pageMode(self.modes.transactionPage);
        self.currentMode(self.modes.viewCustomerRequest);
        self.toggleBoolean('showProgress', true, 'get');

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        var postData = { TRANS_ID: selectedCustomerRequest.TRANS_ID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.getCustomerRequestByRequest, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.openStockBatchModal = function (selectedItem) {
        self.selectedItem(selectedItem);
        self.itemIsAsset(self.selectedItem().TRANS_TYPE == 'ASSET')

        if (self.selectedItem().stockBatchs().length)
            window.location.href = ah.config.url.openStockBatchModal;
        else {
            self.currentMode(self.modes.getStockBatchByProdStore);
            self.toggleBoolean('showProgress', true, 'search');

            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

            if (!self.itemIsAsset()) {
                var postData = { STORE: self.selectedItem().STORE_FROM, PRODUCT_CODE: self.selectedItem().ID_PARTS, STAFF_LOGON_SESSIONS: staffLogonSessions };
                $.post(self.getApi() + ah.config.api.getStockBatchByProdStore, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
            }
            else {
                self.getDefaultCostCenter();

                var postData = { SEARCH: '', SEARCH_FILTER: { AssetFilterCostCenter: self.DefaultCostCenter.CODE, assetDescription: self.selectedItem().DESCRIPTION }, STAFF_LOGON_SESSIONS: staffLogonSessions };
                $.post(self.getApi() + ah.config.api.searchAssets, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
            }
        }
    };
    self.modalCalculateStockDeductable = function (selectedItem) {
        if (!self.itemIsAsset()) {
            if (Number(selectedItem.QTY()) < 0) {
                selectedItem.QTY(0);
                return alert('Stock Deductable shoud greater than 0');
            }

            if (Number(selectedItem.QTY()) > Number(selectedItem.STOCK_ONHAND)) {
                alert('Stock Deductable should not be greater than Stock On Hand');
                totalStockDeductable -= selectedItem.QTY();
                selectedItem.QTY(0);
            }

            var totalStockDeductable = 0;
            for (var i = 0; i < self.selectedItem().stockBatchs().length; i++) {
                totalStockDeductable += Number(self.selectedItem().stockBatchs()[i].QTY());
            }
            if ((Number(totalStockDeductable) + Number(self.selectedItem().QTY_TRANSFER)) > Number(self.selectedItem().QTY)) {
                alert('Requested Quantity Reached.');
                totalStockDeductable -= selectedItem.QTY();
                selectedItem.QTY(0);
            }
            self.selectedItem().totalStockDeductable(totalStockDeductable);
        }
        else {
            var totalStockDeductable = self.selectedItem().totalStockDeductable();
            if (selectedItem.isChecked()) {
                if ((Number(totalStockDeductable) + Number(self.selectedItem().QTY_TRANSFER)) >= Number(self.selectedItem().QTY)) {
                    alert('Requested Quantity Reached.');
                    selectedItem.isChecked(false);
                    return;
                }

                totalStockDeductable++;
            }
            else if (!selectedItem.isChecked() && totalStockDeductable > 0)
                totalStockDeductable--;

            self.selectedItem().totalStockDeductable(totalStockDeductable);
        }
    };
    self.triggerFormSubmit = function () {
        $(ah.config.id.prSubmitBtn).trigger('click');
    };
    self.saveAllChanges = function () {
        var sohList = self.assignedSOH().filter(function (item) { return item.TRANS_TYPE == 'SOH' });
        var assetList = self.assignedSOH().filter(function (item) { return item.TRANS_TYPE == 'ASSET' });
        var assetListPush = [];
        var PRODUCT_STOCKBATCHS = [];
        var hasProductStockBatchs = false;
        for (var i = 0; i < sohList.length; i++) {
            for (var j = 0; j < sohList[i].stockBatchs().length; j++) {
                if (Number(sohList[i].stockBatchs()[j].QTY())) {
                    hasProductStockBatchs = true;
                    PRODUCT_STOCKBATCHS.push(sohList[i].stockBatchs()[j])
                }
            }
        }

        for (var i = 0; i < assetList.length; i++) {
            for (var j = 0; j < assetList[i].stockBatchs().length; j++) {
                if (assetList[i].stockBatchs()[j].isChecked()) {
                    hasProductStockBatchs = true;
                    assetListPush.push(assetList[i].stockBatchs()[j])
                }
            }
        }

        if (hasProductStockBatchs) {
            self.currentMode(self.modes.transferCustomerRequest);
            self.toggleBoolean('showProgress', true, 'save');
            self.toggleBoolean('showProgressScreen', true);

            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var AM_STOCKTRANSACTION = self.getObservableValue(self.customerRequest);
            AM_STOCKTRANSACTION.REQUEST_DATE = moment(AM_STOCKTRANSACTION.REQUEST_DATE, 'DD/MM/YYYY HH:mm').format();

            var postData = { AM_STOCKTRANSACTION: AM_STOCKTRANSACTION, PRODUCT_STOCKBATCHS: PRODUCT_STOCKBATCHS, ASSET_VS: assetListPush, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.transferCustomerRequest, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {
            alert('Unable to proceed. No item to transfer. Please click on transfer button per stock request.');
        }
    };
    self.initialize = function () {
        window.location.href = ah.config.url.modalClose;

        self.pageMode(self.modes.homePage);
        self.toggleBoolean('showProgress', true, 'page');
        self.toggleBoolean('showProgressScreen', true);

        $(ah.config.id.divNotify).css('display', 'none');

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        var postData = { LOV: "'SHIP_VIA','FOREIGN_CURRENCY'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.when($.post(self.getApi() + ah.config.api.searchSetupLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus)).done(function () {
            var postData = { STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.getStoresLookup, postData).done(function (jsonData) {
                jsonData.STORES.unshift({
                    STORE_ID: null,
                    DESCRIPTION: '- select -'
                });
                self.stores(jsonData.STORES || []);
            }).fail(self.webApiCallbackStatus);
        });
    };

    self.itemIsAsset = ko.observable(false);
    self.DefaultCostCenter = {};
    self.shipVias = ko.observableArray([]);
    self.foreignCurrencies = ko.observableArray([]);
    self.modalQuickSearch2 = ko.observable();
    self.modalActiveSearching = ko.observable(1);
    self.billingOrVendorList = ko.observableArray([]);
    self.setObservableValueEmpty = function (object) {
        Object.keys(object).forEach(function (key) {
            if (ko.isObservable(object[key]))
                object[key]('')
            else
                object[key] = '';
        });
    }
    self.openBillingOrVendorInfoModal = function () {
        self.modalQuickSearch2('');

        self.modalSearchBillingOrVendor();
        window.location.href = ah.config.url.openBillingOrVendorInfoModal;
    };
    self.modalSearchBillingOrVendor = function () {
        self.billingOrVendorList.removeAll();
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        self.toggleBoolean('showModalProgress', true);

        var postData = { SEARCH: self.modalQuickSearch2(), STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchCostCenterDetails, postData).done(function (jsonData) {
            self.getDefaultCostCenter();

            self.billingOrVendorList((jsonData.COSTCENTER_LIST || []).filter(function (x) { return x.STORE_ID != self.DefaultCostCenter.STORE_ID }));
            self.toggleBoolean('showModalProgress', false);
        }).fail(self.webApiCallbackStatus);
    };
    self.assignBillingOrVendorInfo = function (item) {
        self.customerRequest.SUPPLIER_ID(item.CODEID);
        self.customerRequest.SUPPLIERDESC(item.DESCRIPTION);
        self.customerRequest.SUPPLIER_ADDRESS1(item.ADDRESS);
        self.customerRequest.SUPPLIER_CITY(item.CITY);
        self.customerRequest.SUPPLIER_ZIPCODE(item.ZIP);
        self.customerRequest.SUPPLIER_STATE(item.STATE);
        self.customerRequest.SUPPLIER_CONTACT_NO1(item.CONTACTNO);
        self.customerRequest.SUPPLIER_CONTACT_NO2(item.CONTACTNO2);
        self.customerRequest.SUPPLIER_EMAIL_ADDRESS(item.EMAILADDRESS);

        self.customerRequest.STORE_TO(item.STORE_ID);

        window.location.href = ah.config.url.modalClose;
    };
    self.addCheckedItem = function (item) {
        item
    };
    self.calculateTotalPrice = function (item, isPrice) {
        item.TOTAL_PRICE((item.PRICE() || 0) * ko.utils.unwrapObservable(item.QTY));

        item.TOTAL_PRICE(Math.round(item.TOTAL_PRICE() * 100) / 100);

        self.calculateTotal();
    };
    self.calculateTotal = function () {
        var totalAmountPrice = 0;
        totalAmountPrice = self.assignedSOH().reduce(function (acc, val) { return acc + val.TOTAL_PRICE(); }, 0);
        var inctoPO = self.customerRequest.FREIGHT_POLINE();
        var freightCharge = self.customerRequest.FREIGHT_CHARGETO();
        if (inctoPO == "YES" && freightCharge == "YES") {
            totalAmountPrice = totalAmountPrice + Number(self.customerRequest.FREIGHT());
        } else if (freightCharge == "NO") {
            self.customerRequest.FREIGHT_POLINE(null);
            self.customerRequest.FREIGHT(0);
        }

        totalAmountPrice = Math.round(totalAmountPrice * 100) / 100;
        self.customerRequest.TOTAL_AMOUNT(totalAmountPrice);
    };
    self.getDefaultCostCenter = function () {
        var dCC = JSON.parse(sessionStorage.getItem(ah.config.skey.defaultCostCenter));
        if (dCC.length > 0) {
            self.DefaultCostCenter.CODE = dCC[0].CODEID;
            self.DefaultCostCenter.DESCRIPTION = dCC[0].DESCRIPTION;
            self.DefaultCostCenter.STORE_ID = dCC[0].STORE_ID;
        }
    };

    self.assignedSOHPrint = ko.observableArray([]);
    self.clientDetails = {
        CLIENTNAME: ko.observable(),
        CLIENT_LOGO: ko.observable(),
        COMPANY_LOGO: ko.observable(),
        HEADING: ko.observable(),
        HEADING2: ko.observable(),
        HEADING3: ko.observable(),
        HEADING_OTH: ko.observable(),
        HEADING_OTH2: ko.observable(),
        HEADING_OTH3: ko.observable(),
        CLIENTADDRESS: ko.observable(),
        CLIENTCONTACT: ko.observable()
    };
    self.printPurchaseRequest = function () {
        self.clientDetails.HEADING('Business Schema Trading Company');
        self.clientDetails.COMPANY_LOGO(window.location.origin + '/images/companylogo.png');

        self.assignedSOHPrint.removeAll();
        self.assignedSOHPrint(self.assignedSOH.slice(0));
        if (self.customerRequest.FREIGHT_POLINE() == 'YES') {
            self.assignedSOHPrint.push({
                DESCRIPTION: 'FREIGHT CHARGES',
                QTY: 1,
                PRICE: ko.observable(self.customerRequest.FREIGHT() || 0),
                TOTAL_PRICE: ko.observable(self.customerRequest.FREIGHT() || 0),
            });
        }

        setTimeout(function () {
            window.print();
        }, 1000)
    };
    self.getLookupDescription = function (value, array) {
        return value && array.length && array.find(function (x) { return x.LOV_LOOKUP_ID == value }).DESCRIPTION;
    };
    self.getPODate = function () {
        return moment(self.customerRequest.REQUEST_DATE(), 'DD/MM/YYYY').format('DD-MMM-YYYY');
    };
    self.getSubtotalPrice = function () {
        var itemTotalPrice = self.assignedSOHPrint().reduce(function (acc, val) { return acc + val.TOTAL_PRICE(); }, 0);
        return Math.round(itemTotalPrice * 100) / 100;
    };
    self.getDiscountPrice = function () {
        var itemDiscountPrice = self.getSubtotalPrice() * (Number(self.customerRequest.DISCOUNT()) / 100);
        return Math.round(itemDiscountPrice * 100) / 100;
    };
    self.getVatDescription = function () {
        var array = self.assignedSOHPrint.slice(0).filter(function (x) { return ko.utils.unwrapObservable(x.VAT_FLAG) });
        if (array.length) {
            var filtered = array.reduce(function (x, y) { return x.findIndex(function (e) { return ko.utils.unwrapObservable(e.VAT) == ko.utils.unwrapObservable(y.VAT); }) < 0 ? [].concat(x, [y]) : x; }, []);
            if (self.customerRequest.FREIGHT_POLINE() == 'YES') {
                filtered = filtered.filter(function (x) { return ko.utils.unwrapObservable(x.ID_PARTS) });
                return getVatText();
            }

            if (filtered.length > 1) {
                return getVatText();
            }
            else {
                return 'VAT ' + self.assignedSOHPrint()[0].VAT() + '%';
            }
        }

        function getVatText() {
            var vatText = 'VAT ';
            filtered.forEach(function (item) {
                var foundFilter = array.filter(function (x) { return ko.utils.unwrapObservable(item.VAT) == ko.utils.unwrapObservable(x.VAT) });
                foundFilter.forEach(function (item2) {
                    vatText += ko.utils.unwrapObservable(item2.ID_PARTS) + ', ';
                });
                vatText = vatText.substr(0, vatText.length - 2) + ' ';
                vatText += ko.utils.unwrapObservable(item.VAT) + '%, ';
            });
            vatText = vatText.substr(0, vatText.length - 2) + ' ';
            return vatText;
        }
    };
    self.getVatAmount = function () {
        var vatAmount = 0;
        self.assignedSOHPrint().forEach(function (item) {
            if (ko.utils.unwrapObservable(item.VAT_FLAG)) {
                vatAmount = vatAmount + (ko.utils.unwrapObservable(item.TOTAL_PRICE) * (ko.utils.unwrapObservable(item.VAT) / 100));
            }
        });
        return Math.round(vatAmount * 100) / 100;
    };
    self.getTotalPrice = function () {
        var itemTotalPrice = self.assignedSOHPrint().reduce(function (acc, val) { return acc + val.TOTAL_PRICE(); }, 0);
        itemTotalPrice = (itemTotalPrice - Number(self.getDiscountPrice())) + Number(self.getVatAmount());
        //itemTotalPrice = itemTotalPrice - (Number(self.getDiscountPrice()) + Number(self.getVatAmount()));
        return Math.round(itemTotalPrice * 100) / 100;
    };

    self.webApiCallbackStatus = function (jsonData) {
        if (jsonData.ERROR) {
            self.toggleBoolean('showProgress', false);
            self.toggleBoolean('showProgressScreen', false);

            $(ah.config.id.divNotify).css('display', 'none');

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
        }
        else if (!jsonData.ERROR) {
            switch (self.pageMode()) {
                case self.modes.searchPage:
                    self.toggleBoolean('showProgress', false);
                    self.customerRequests(jsonData.AM_STOCKTRANSACTION);

                    window.location.href = ah.config.url.modalClose;
                    break;

                case self.modes.homePage:
                    self.toggleBoolean('showProgress', false);
                    self.toggleBoolean('showProgressScreen', false);

                    var shipVias = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'SHIP_VIA' });;
                    shipVias.unshift({
                        LOV_LOOKUP_ID: null,
                        DESCRIPTION: '- select -'
                    });
                    self.shipVias(shipVias);

                    var foreignCurrencies = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'FOREIGN_CURRENCY' });;
                    foreignCurrencies.unshift({
                        LOV_LOOKUP_ID: null,
                        DESCRIPTION: '- select -'
                    });
                    self.foreignCurrencies(foreignCurrencies);
                    break;

                case self.modes.transactionPage:

                    switch (self.currentMode()) {
                        case self.modes.viewCustomerRequest:
                            self.toggleBoolean('showProgress', false);
                            jsonData.AM_STOCKTRANSACTIONS = jsonData.AM_STOCKTRANSACTIONS || [];

                            self.setObservableValue(self.customerRequest, jsonData.AM_STOCKTRANSACTION);
                            self.customerRequest.REQUEST_DATE(moment(self.customerRequest.REQUEST_DATE()).format('DD/MM/YYYY HH:mm'));

                            for (var i = 0; i < jsonData.AM_STOCKTRANSACTIONS.length; i++) {
                                jsonData.AM_STOCKTRANSACTIONS[i].PRICE = ko.observable(jsonData.AM_STOCKTRANSACTIONS[i].PRICE);
                                jsonData.AM_STOCKTRANSACTIONS[i].TOTAL_PRICE = ko.observable(0);
                                jsonData.AM_STOCKTRANSACTIONS[i].VAT_FLAG = ko.observable(jsonData.AM_STOCKTRANSACTIONS[i].VAT_FLAG || false);
                                jsonData.AM_STOCKTRANSACTIONS[i].VAT = ko.observable(jsonData.AM_STOCKTRANSACTIONS[i].VAT);
                                self.calculateTotalPrice(jsonData.AM_STOCKTRANSACTIONS[i]);

                                jsonData.AM_STOCKTRANSACTIONS[i].stockBatchs = ko.observableArray([]);
                                jsonData.AM_STOCKTRANSACTIONS[i].totalStockDeductable = ko.observable(0);
                            }
                            self.assignedSOH(jsonData.AM_STOCKTRANSACTIONS);
                            break;

                        case self.modes.getStockBatchByProdStore:
                            self.toggleBoolean('showProgress', false);
                            if (!self.itemIsAsset()) {
                                jsonData.PRODUCT_STOCKBATCHS = jsonData.PRODUCT_STOCKBATCHS || [];
                                for (var i = 0; i < jsonData.PRODUCT_STOCKBATCHS.length; i++) {
                                    jsonData.PRODUCT_STOCKBATCHS[i].STOCK_ONHAND = jsonData.PRODUCT_STOCKBATCHS[i].STOCK_ONHAND || 0;
                                    jsonData.PRODUCT_STOCKBATCHS[i].QTY = ko.observable(0);
                                    jsonData.PRODUCT_STOCKBATCHS[i].EXPIRY_DATE_DISPLAY = moment(jsonData.PRODUCT_STOCKBATCHS[i].EXPIRY_DATE).format('DD/MM/YYYY');

                                    if (!jsonData.PRODUCT_STOCKBATCHS[i].STOCK_ONHAND || moment().format() > moment(jsonData.PRODUCT_STOCKBATCHS[i].EXPIRY_DATE).format())
                                        jsonData.PRODUCT_STOCKBATCHS[i].viewOnly = true;
                                }
                                self.selectedItem().stockBatchs(jsonData.PRODUCT_STOCKBATCHS);
                            }
                            else {
                                jsonData.AM_ASSET_DETAILS = jsonData.AM_ASSET_DETAILS || [];
                                for (var i = 0; i < jsonData.AM_ASSET_DETAILS.length; i++) {
                                    jsonData.AM_ASSET_DETAILS[i].isChecked = ko.observable(false);
                                }

                                self.selectedItem().stockBatchs(jsonData.AM_ASSET_DETAILS);
                            }

                            window.location.href = ah.config.url.openStockBatchModal;
                            break;

                        case self.modes.transferCustomerRequest:
                            self.currentMode(self.modes.viewCustomerRequest);
                            self.toggleBoolean('showProgress', false);
                            self.toggleBoolean('showProgressScreen', false);

                            $(ah.config.id.divNotify).css('display', 'block');
                            $(ah.config.id.divNotify + ' p').html(systemText.saveSuccessMessage);
                            setTimeout(function () {
                                $(ah.config.id.divNotify).fadeOut(1000);
                            }, 3000);

                            self.selectCustomerRequest(jsonData.AM_STOCKTRANSACTION);
                            break;
                    }
                    break;
            }
        }
        else {
            self.toggleBoolean('showProgress', false);
            self.toggleBoolean('showProgressScreen', false);

            $(ah.config.id.divNotify).css('display', 'none');

            alert(systemText.errorTwo);
        }
    };

    self.initialize();
};