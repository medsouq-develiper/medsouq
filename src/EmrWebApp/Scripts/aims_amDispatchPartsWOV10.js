﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            licrit: '.licrit',
            liDispatch: '.liDispatch',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            detailItem: '.detailItem',
            liassetTag: '.liassetTag',
            wsAsset: '.wsAsset',
            itemRequest: '.itemRequest',
            divInfo: '.divInfo'
        },
        fld: {
            act: 'act',
            action: 'action',
            woNo: 'WONO'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7,
            deleted: 8
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmInfo: '#frmInfo',
            frmInfoInspection: '#frmInfoInspection',
            clinicdoctorID: '#CLINIC_DOCTOR_ID',
            clinicID: '#CLINIC_ID',
            frmwsAsset: '#frmwsAsset',
            frmInfoReading: '#frmInfoReading',
            staffID: '#STAFF_ID',
            saveInfoLog: '#saveInfoLog',
            serviceID: '#SERVICE_ID',
            searchResult1: '#searchResult1',
            successMessage: '#successMessage',
            saveInfoReading: '#saveInfoReading',
            txtGlobalSearchOth: '#txtGlobalSearchOth',
            pageNum: '#pageNum',
            pageCount: '#pageCount',
            loadNumber: '#loadNumber',
            btnRemove: '#btnRemove',
            storeFrom: '#STOREFROM',
            storeTo: '#STORETO',
            searchResultLookUp: '#searchResultLookUp'
        },

        cssCls: {
            viewMode: 'view-mode'
        },
        tagId: {

            txtGlobalSearchOth: 'txtGlobalSearchOth',
            priorPage: 'priorPage',
            nextPage: 'nextPage'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfo: 'data-info',
            datainfoID: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'searchResult',
            infoDetails: 'AM_PARTSREQUEST',
            stockList: 'PRODUCT_STOCKONHAND',
            wsassetList: 'AM_INSPECTIONASSET',
            stockStoreList: 'PRODUCT_STOCKONHAND',
            stockReqStoreList: 'PRODUCT_STOCKONHAND_REQSTORE'
        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalLookUp: 'openModalLookup',
            modalClose: '#',
            modalAddItems: '#openModal',
            modalReading: 'openModalReading',
            modalFilter: '#openModalFilter',
            openModalLookup: '#openModalLookup'
        },
        api: {

            readStockReq: '/Inventory/SearchWOLPartReq',
            searchAll: '/AIMS/SearchWOParts',
            createRequest: '/Inventory/CreateNewStocktransaction',
            updatePartsRequest: '/Inventory/UpdateWOLPartsRequest',
            seachLookUp: '/Inventory/SearchStores',
            readStockReceiveReq: '/Inventory/SearchStockReceiveReqDet',
            searchStockOnhand: '/Inventory/SearchStockOnhandFromStore',
            updatePartsRequestFC: '/Inventory/UpdateWOLPartsRequestForCollect',
            updatePartsRequestDispatch: '/Inventory/UpdateWOLPartsRequestDispatch'

        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
            searchRowHeaderTemplateList: "<a href='#openModal' class='fs-medium-normal detailItem' data-infoAssetID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            searchLOVRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem fc-greendark' data-info ='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} <span>{3}</span></a>",
            searchExistRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem fc-slate-blue' data-info ='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} <span class='fc-green'> {3} </span></a>"
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };
    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liModify + ',' +  thisApp.config.cls.divInfo + ',' + thisApp.config.cls.liDispatch + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.licrit + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess + ',' + thisApp.config.cls.liassetTag + ',' + thisApp.config.cls.itemRequest
        ).hide();

        $(thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;
        $(thisApp.config.cls.divInfo).hide();
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";

    };
    thisApp.toggleDisplayModereq = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        //$(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        //$(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(thisApp.config.cls.contentField + ',' + thisApp.config.cls.searchAsset).show();
   
        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        //$(
        //    thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        //).prop(thisApp.config.attr.readOnly, false);

        //$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };
    thisApp.toggleSearchItems = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        //$(thisApp.config.cls.statusText).text(systemText.searchModeStatuxText);
        //$(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liSave +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();

    };
    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liDispatch + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.liassetTag + ',' + thisApp.config.cls.itemRequest
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };
    thisApp.toggleEditModeModal = function () {


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
    };
    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.liDispatch + ',' +  thisApp.config.cls.licrit + ',' + thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.liassetTag
        ).hide();

    };
    thisApp.toggleSearchModalMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;


    };
    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.divInfo+','+thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.itemRequest).show();
        $(thisApp.config.cls.liDispatch + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.liassetTag).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.liDispatch + ',' + thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.divInfo + ',' + thisApp.config.cls.licrit + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liDispatch + ',' + thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

        $(
           thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
           thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };



    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };
    thisApp.formatJSONDateToStringOth = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[0] + '-' + strDate[1] + '-' + strDate[2].substring(0, 2);


        return dateToFormat;
    };
    thisApp.formatJSONDateToString = function () {

        var dateToFormat = arguments[0];
        var strDate;

        if (dateToFormat === null) return "-";
        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };
    thisApp.formatJSONDateTimeToString = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0] + ' ' + strTime[1].substring(0, 5);


        return dateToFormat;
    };
    thisApp.formatJSONTimeToString = function () {

        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        var timeToFormat = strTime[1].substring(0, 5);


        return timeToFormat;
    };
    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');


            if (isDateTime) {
                val = thisApp.formatJSONDateTimeToString(val);
            }

            if (isDate) {
                val = thisApp.formatJSONDateToString(val);
            }

            var $el = $('#' + name), type = $el.attr('type'), isTime = $el.attr('data-istime');
            if (isTime) {
                if (val === null || val === "") {
                    val = "00:00";
                } else {
                    val = thisApp.formatJSONTimeToString(val);
                }

            }

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var amDispatchPartsWOViewModel = function (systemText) {

    var self = this;
    self.searchKeyword = ko.observable('');
    self.searchFilter = {
        fromDate: ko.observable(''),
        toDate: ko.observable(''),
        status: ko.observable(''),
    }
    self.selected = ko.observable("");
    var workOrderInfo = ko.observable('');
    self.clearFilter = function () {
        self.searchFilter.toDate('');
        self.searchFilter.fromDate('');
        self.searchFilter.status('');
        $('#requestFromDate').val('');
        $('#requestStartDate').val('');
        window.location.href = ah.config.url.modalClose;
    }
    self.searchFilterNow = function () {
        window.location.href = ah.config.url.modalClose;
        self.searchNow();
    }
    self.dateClick = function () {
        $("#dateClickAction").val("ACTIVE");
    };
    self.searchKeyUp = function (d, e) {
        if (e.keyCode == 13) {
            self.searchNow();
        }
    }
    var ah = new appHelper(systemText);

    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

       // ah.toggleReadMode();

        ah.initializeLayout();
        var act = ah.getParameterValueByName(ah.config.fld.action);

        if (act === "return") {
        } else {
            $("#filterStatus").val("PENDING");
            self.searchFilter.status("PENDING");

        }
      
    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.reqList = ko.observableArray("");
    self.showModalFilter = function () {
        window.location.href = ah.config.url.modalFilter;
    };
    self.returnHome = function () {

		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));		
		window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.createNew = function () {
        self.reqList.splice(0, 5000);

        ah.toggleAddMode();

        var jDate = new Date();

        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        var reqDate = strDateStart + ' ' + hourmin;

        // alert(tranStatus);

        $("#REQUEST_DATE").val(reqDate);

        $("#TRANS_STATUS").val("OPEN");

    };
  
    self.pageLoader = function () {
        var senderID = $(arguments[1].currentTarget).attr('id');

        var searchStr = $(ah.config.id.txtGlobalSearchOth).val().toString();

        var pageNum = parseInt($(ah.config.id.pageNum).val());
        var pageCount = parseInt($(ah.config.id.pageCount).val());
        if (isNaN(pageCount)) {
            pageCount = 1;
        }
        if (isNaN(pageNum)) {
            pageNum = 0;
        }

        switch (senderID) {

            case ah.config.tagId.nextPage: {
                pageNum = pageNum + 9;
                pageCount++;
                self.searchItemsResult(searchStr, pageNum);
                break;
            }
            case ah.config.tagId.priorPage: {

                if (pageNum === 0) {
                } else {
                    pageNum = pageNum - 9;
                    if (pageNum < 0) {
                        pageNum = 0;
                    }
                    pageCount = pageCount - 1;
                }

                self.searchItemsResult(searchStr, pageNum);
                break;
            }

            case ah.config.tagId.txtGlobalSearchOth: {
                pageCount = 1;
                pageNum = 0;

                self.searchItemsResult(searchStr, 0);
                break;
            }
        }

        $(ah.config.id.loadNumber).text(pageCount.toString());
        $(ah.config.id.pageCount).val(pageCount.toString());
        $(ah.config.id.pageNum).val(pageNum);

    };
    self.modifySetup = function () {

        ah.toggleEditMode();

    };
    self.cancelChangesModal = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            var refId = $("#AM_INSPECTION_ID").val();
            self.readSetupExistID(refId);

        }

        window.location.href = ah.config.url.modalClose;

    };
    self.forCollection = function (partsRequest) {
        
        if (partsRequest.FOR_COLLECTION_QTY <= 0) {
            self.alertMessage("Unable to proceed. Spart Part " + partsRequest.PART_DESC + " for Collect QTY cannot be less than/equal to zero and empty.");
            return;
        }

        if (parseInt(partsRequest.FOR_COLLECTION_QTY) > parseInt(partsRequest.STOCK_ONHAND)) {
            self.alertMessage("Unable to proceed. Spart Part " + partsRequest.PART_DESC + " for Collect QTY cannot be greater than stock onhand qty.");
            return;
        }

        var forCollectionQty = parseInt(partsRequest.TRANSFER_QTY) + parseInt(partsRequest.FOR_COLLECTION_QTY);
       
        if (forCollectionQty > partsRequest.QTY && partsRequest.TRANSFER_QTY > 0) {
            self.alertMessage("Unable to proceed. Spart Part " + partsRequest.PART_DESC + " (for Collect QTY + Dispatched QTY) cannot be greater than Request QTY  ");
            return;
        }
        if (forCollectionQty > partsRequest.QTY && partsRequest.TRANSFER_QTY == 0) {
            self.alertMessage("Unable to proceed. Spart Part " + partsRequest.PART_DESC + " for Collect QTY  cannot be greater than Request QTY  ");
            return;
        }

      

        self.selected(partsRequest);
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //var itemDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        postData = { AM_PARTSREQUEST: partsRequest, STAFF_LOGON_SESSIONS: staffLogonSessions };


        ah.toggleSaveMode();
        $.post(self.getApi() + ah.config.api.updatePartsRequestFC, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);


    }

    self.forDispatch = function (partsRequest) {

        if (parseInt(partsRequest.FOR_COLLECTION_QTY) > parseInt(partsRequest.STOCK_ONHAND)) {
            self.alertMessage("Unable to proceed. Spart Part " + partsRequest.PART_DESC + " for Dispatch QTY cannot be greater than stock onhand qty.");
            return;
        }

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //var itemDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;




        partsRequest.DISPATCH_BY = staffLogonSessions.STAFF_ID;

        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        strDateStart = strDateStart + ' ' + hourmin;
        partsRequest.DISPATCH_DATE = strDateStart;

        postData = { AM_PARTSREQUEST: partsRequest, STAFF_LOGON_SESSIONS: staffLogonSessions };


        ah.toggleSaveMode();
        $.post(self.getApi() + ah.config.api.updatePartsRequestDispatch, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    }

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };
    self.stockSearchNow = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        var storeFrom = $("#STOREFROM").val();
        if (storeFrom === null || storeFrom === "") {
            alert('Please select From Store to proceed.');
            return;
        }

        var storeTo = $("#STORETO").val();
        if (storeTo === null || storeTo === "") {
            alert('Please select To Store to proceed.');
            return;
        }
        if (storeFrom === storeTo) {
            alert('Unable to proceed. The From Store and To Store should not the same.');
            return;
        }

        ah.toggleSearchItems();
        postData = { SEARCH: "", ACTION: "", STORE_FROM: storeFrom, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchStockOnhand, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = ah.config.url.modalAddItems;

    };
    self.saveAllChanges = function () {

        $(ah.config.id.saveInfo).trigger('click');

    };
    self.addItemReq = function () {

        var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfo));
        //alert(JSON.stringify(infoID));

        var existPart = self.checkExist(infoID.ID_PARTS);
        if (existPart > 0) {
            alert('Unable to Proceed. The selected item was already exist.');
            return;
        }
        var reqDate = $("#REQUEST_DATE").val();
        var tranStatus = $("#TRANS_STATUS").val();
        var fromStore = $("#STOREFROM").val();
        var toStore = $("#STORETO").val();

        self.reqList.push({
            DESCRIPTION: infoID.DESCRIPTION,
            ID_PARTS: infoID.ID_PARTS,
            QTY: 0,
            REQUEST_DATE: reqDate,
            TRANS_TYPE: null,
            STORE_FROM: fromStore,
            STORE_TO: toStore,
            TRANS_STATUS: tranStatus,
            AM_STOCKTRANSACTIONID: 0,

            REMARKS: null,
            TRANSDETFLAG: null

        });


    };
    self.alertMessage = function (refMessage) {
        $("#alertmessage").text(refMessage);
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "block";

    };
    self.closeModalalert = function () {
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none"
    }

    self.enableDisabled = function (reqList) {

  

    };
    self.checkExist = function (idPart) {

        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.reqList);
        var partLists = JSON.parse(jsonObj);

        var partExist = partLists.filter(function (item) { return item.ID_PARTS === idPart });

        return partExist.length;
    };
    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            //var refId = $("#AM_INSPECTION_ID").val();
            //self.readSetupExistID(refId);
        }

    };

    self.showDetails = function (isNotUpdate,jsonData) {

        var act = ah.getParameterValueByName(ah.config.fld.action);
       // if (isNotUpdate) {

            var sr = jsonData;//JSON.parse(sessionStorage.getItem(ah.config.skey.infoDetails));
            //alert(JSON.stringify(sr));
           
            //ah.ResetControls();
            ah.toggleDisplayMode();
            $(ah.config.cls.liModify).hide();

           // alert(JSON.stringify(sr));
            if (sr.length > 0) {
                $("#infoID").text(sr[0].REF_WO);
                self.reqList.splice(0, 5000);
                //if (sr[0].TRANSFER_FLAG === 'F') {
                //    $(ah.config.cls.liModify).hide();
                //    $(ah.config.cls.liDispatch).show();
                //} else if (sr[0].TRANSFER_FLAG === 'D') {
                //    $(ah.config.cls.liModify + ',' + ah.config.cls.liDispatch).hide();
 
                //}
                if (sr[0].TRANSFER_FLAG === 'F' || sr[0].TRANSFER_FLAG === 'D') {
                    $("#TRANSFER_ACTION").val("D");
                }
                $("#workOrderStatus").hide();
                
                var statusCount = 0;
                for (var o in sr) {
                    if (act === "return") {
                        
                        if (sr[o].QTY_FORRETURN == 0) {
                            continue;
                        }
                    }
                    var cttd_qty = 0;
                    var onHand = 0;
                    var transferQTY = 0;
                    var pendingQty = 0;
                    var currStatus = "";
                    var rDate = ah.formatJSONDateToString(sr[o].REQ_DATE);
                    onHand = sr[o].STOCK_ONHAND - sr[o].COMMITTED_QTY;
                    if (onHand < 0) { onHand = 0; }
                    if (sr[o].TRANSFER_QTY === null || sr[o].TRANSFER_QTY === "") { sr[o].TRANSFER_QTY = 0; }
                    if (sr[o].TRANSFER_FLAG === 'F' || sr[o].TRANSFER_FLAG === 'D') {
                        currStatus = "D"
                        transferQTY = sr[o].TRANSFER_QTY;
                    } else {
                        transferQTY = sr[o].QTY;
                    }
                    if (sr[o].QTY_RETURN === null || sr[o].QTY_RETURN === "") {
                        sr[o].QTY_RETURN = 0;
                    }
                    if (sr[o].QTY > sr[o].TRANSFER_QTY && sr[o].TRANSFER_FLAG ==='P') {
                        currStatus = "P"
                        //$(ah.config.cls.liModify).show();
                        $(ah.config.cls.liDispatch).hide();
                        $("#TRANSFER_ACTION").val("P");
                        sr[0].TRANSFER_FLAG = 'P';
                    }
                    if (sr[o].TRANSFER_FLAG === 'F' && statusCount === 0) {
                        //$(ah.config.cls.liDispatch).show();
                        statusCount++;
                    }
                    pendingQty = sr[o].QTY - sr[o].TRANSFER_QTY;
                    if (sr[o].QTY_FORRETURN == null || sr[o].QTY_FORRETURN == "") {
                        sr[o].QTY_FORRETURN = 0;
                    } else {
                        //sr[o].QTY_FORRETURN = sr[o].QTY_FORRETURN - sr[o].QTY_RETURN;
                    }
                    var actionCollection = 1;
                    var actionDispatch = 1;

                   

                    if (onHand == 0 || pendingQty == 0) {
                       
                        actionCollection = 0;
                        actionDispatch = 0;
                    } else {
                        if (sr[o].TRANSFER_FLAG == "F") {
                            actionCollection = 0;
                            actionDispatch = 1;
                        } else {
                            if (pendingQty > 0 && sr[o].TRANSFER_FLAG != "F") {
                                actionCollection = 1;
                                actionDispatch = 0;
                            }
                        }
                    }

                    if (workOrderInfo.WO_STATUS == "CL" || workOrderInfo.WO_STATUS == "FC") {
                        actionCollection = 0;
                        actionDispatch = 0;
                    }

                    self.reqList.push({
                        DESCRIPTION: sr[o].DESCRIPTION,
                        ID_PARTS: sr[o].ID_PARTS,
                        PART_DESC: sr[o].PART_DESC,
                        STORE_DESC: sr[o].STORE_DESC,
                        QTY: sr[o].QTY,
                        REQ_DATE: sr[o].rDate,
                        AM_PARTREQUESTID: sr[o].AM_PARTREQUESTID,
                        STORE_ID: sr[o].STORE_ID,
                        TRANSFER_FLAG: sr[o].TRANSFER_FLAG,
                        CURRSTATUS: currStatus,
                        STATUS: sr[o].STATUS,
                        STOCK_ONHAND: onHand,
                        QTY_RETURN: sr[o].QTY_RETURN,
                        PENDING_QTY: 0,
                        QTY_FORRETURN: sr[o].QTY_FORRETURN,                        
                        TRANSFER_QTY: sr[o].TRANSFER_QTY,
                        REF_WO: sr[o].REF_WO,
                        PART_NUMBERS: sr[o].PART_NUMBERS,
                        FOR_COLLECTION_QTY: sr[o].FOR_COLLECTION_QTY,
                        ACTION_COLLECTION: actionCollection,
                        ACTION_DISPATCH: actionDispatch,
                        DISPATCH_BY: sr[o].DISPATCH_BY,
                        DISPATCH_DATE: sr[o].DISPATCH_DATE
                      

                    })



                }
                


            }

            if (act !== "return") {

                if (workOrderInfo.WO_STATUS == "CL" || workOrderInfo.WO_STATUS == "FC") {
                    $(ah.config.cls.liModify + ',' + ah.config.cls.liDispatch).hide();
                    if (workOrderInfo.WO_STATUS == "CL") {
                        $("#workOrderStatus").text("Unable to Dispatch the work order is already Closed");
                    } else if (workOrderInfo.WO_STATUS == "FC") {
                        $("#workOrderStatus").text("Unable to Dispatch the work order is already For Close");
                    }
                    $("#workOrderStatus").show();
                }

            } else {
                if (workOrderInfo.WO_STATUS != "CL") {
                    $("#workOrderStatus").text("Unable to return the work order must be Closed");
                    $("#workOrderStatus").show();
                } else {
                    var clickReturn = $("#CLICKRETURN").val();
                    if (clickReturn == "N") {
                        $(ah.config.cls.liModify).show();
                    }
                    $("#POSTID").val("RETURN");
                    $(ah.config.cls.liDispatch).hide();
                }

            }
        
        //if (act === "return") {
        //    var clickReturn = $("#CLICKRETURN").val();
        //    if (clickReturn == "N") {
        //        $(ah.config.cls.liModify).show();
        //    }
        //    $("#POSTID").val("RETURN");
        //    $(ah.config.cls.liDispatch).hide();
        //}


    };
    self.showConflictReq = function (jsonData) {

       
        var sr = jsonData.AM_PARTSREQUESTCONFICT;
        window.location.href = ah.config.url.openModalLookup;
        $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');
        
        if (sr.length > 0) {
            //alert(sr[0].STOCK_ONHAND);
            $("#ITEMNAME").text("Item Name: "+sr[0].PRODUCT_DESC);
            $("#ADQTY").hide();
            var htmlstr = "";
            var forCollectQTY = 0;
            for (var o in sr) {
                forCollectQTY = forCollectQTY + parseInt(sr[o].FOR_COLLECTION_QTY);
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, JSON.stringify(sr[o]), "Work Order No", sr[o].REF_WO, "  For Collection QTY: " + sr[o].FOR_COLLECTION_QTY);

                htmlstr += '</li>';

            }

            if (parseInt(sr[0].STOCK_ONHAND) > forCollectQTY) {
                var avForCollect = parseInt(sr[0].STOCK_ONHAND) - forCollectQTY;

                $("#ADQTY").text("You are allow only QTY: (" + avForCollect +") for Collection.");
                $("#ADQTY").show();
            }

            // alert(htmlstr);
            $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).append(htmlstr);
        }
    };
    self.searchItemsResult = function (LovCategory, pageLoad) {

        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.stockList));
        if (LovCategory !== null && LovCategory !== "") {
            var arr = sr;
            var keywords = LovCategory.toUpperCase();
            var filtered = [];

            for (var i = 0; i < sr.length; i++) {
                for (var j = 0; j < keywords.length; j++) {
                    if ((sr[i].DESCRIPTION.toUpperCase()).indexOf(keywords) > -1 || (sr[i].ID_PARTS.toUpperCase()).indexOf(keywords) > -1) {
                        filtered.push(sr[i]);
                        break;
                    }
                }
            }
            sr = filtered;
        }

        $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).html('');

        if (sr.length > 0) {
            $("#page1").show();
            $("#page2").show();
            $("#nextPage").show();
            $("#priorPage").show();
            var totalrecords = sr.length;
            var pagenum = totalrecords / 9;
            if (pagenum < 2 && pagenum > 1) {
                pagenum = 2;
            }
            $(ah.config.id.totNumber).text(pagenum.toString());

            if (pagenum <= 1) {
                $("#page1").hide();
                $("#page2").hide();
                $("#nextPage").hide();
                $("#priorPage").hide();

            }
            var htmlstr = '';
            var o = pageLoad;

            for (var i = 0; i < 9; i++) {
                htmlstr += '<li>';
                htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                if (sr[o].QTY !== null) {
                    htmlstr += ah.formatString(ah.config.html.searchExistRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].ID_PARTS, sr[o].DESCRIPTION, " Available Stock on Hand: " + sr[o].QTY);
                } else {
                    htmlstr += ah.formatString(ah.config.html.searchLOVRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].ID_PARTS, sr[o].DESCRIPTION, "");
                }



                htmlstr += '</span>';
                htmlstr += '</li>';
                o++
                if (totalrecords === o) {
                    break;
                }
            }
            // alert(JSON.stringify(sr));
            //alert(htmlstr);
            $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.detailItem).bind('click', self.addItemReq);
        ah.toggleDisplayModereq();
        //ah.toggleAddItems();
    };
    self.searchResult = function (jsonData) {


        var sr = jsonData.DISPATCH_WOPARTS_V;
       // alert(JSON.stringify(sr));
        var act = ah.getParameterValueByName(ah.config.fld.action);
        
            if (act === "return") {

                var sr = sr.filter(function (item) {
                    return item.MATERIALS_REMARKS ==="Qty for Return" });
            }
      
        //alert(JSON.stringify(sr));
        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {
                if (sr[o].MATERIALS_REMARKS === null || sr[o].MATERIALS_REMARKS === "") {
                    sr[o].MATERIALS_REMARKS = 'Pending';
                }
                var transDate = new Date(sr[o].REQ_DATE);
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].REQ_NO, 'Work Order No: ' + sr[o].REQ_NO, 'Request Date: ' + transDate.toDateString().substring(4, 15), '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total Items: ' + sr[o].NO_ITEMS + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dispatch Status:&nbsp;&nbsp;' + sr[o].MATERIALS_REMARKS);
                //htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.woOrder, sr[o].REQ_NO);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.problem, sr[o].PROBLEM);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.assetInfo, sr[o].ASSET_NO + '-' + sr[o].ASSET_DESC);

                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.detailItem).bind('click', self.readStockRequest);
        ah.displaySearchResult();
       

    };


    self.readStockRequest = function () {
        $("#CLICKRETURN").val("N");
        var infoID = this.getAttribute(ah.config.attr.datainfoID).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
     
    
        ah.toggleReadMode();
       
        postData = { REF_WO: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };

        var act = ah.getParameterValueByName(ah.config.fld.act);

        $.post(self.getApi() + ah.config.api.readStockReq, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
 


    };
    self.readStockRequestExist = function (refId) {

       
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
      

        ah.toggleReadMode();

        postData = { REF_WO: refId, STAFF_LOGON_SESSIONS: staffLogonSessions };

        var act = ah.getParameterValueByName(ah.config.fld.act);

        $.post(self.getApi() + ah.config.api.readStockReq, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);



    };

    self.readSetupExistID = function (refId) {


        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { AM_INSPECTION_ID: refId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
  
    self.statusDispatch = function () {
        self.saveInfo();
    }
    self.statusDispatchedReturn = function () {
        $("#CLICKRETURN").val("Y");
        $(ah.config.cls.liModify).hide();
        self.saveInfo();
    }
    self.searchNow = function () {
        
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();

        var fromDate = $('#requestFromDate').val().split('/');
        if (fromDate.length == 3) {
            self.searchFilter.fromDate(fromDate[2] + "/" + fromDate[1] + "/" + fromDate[0]);
        }
        var toDate = $('#requestStartDate').val().split('/');
        if (toDate.length == 3) {
            self.searchFilter.toDate(toDate[2] + "/" + toDate[1] + "/" + toDate[0] + "  23:00:00.000");
        }

        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), RETACTION: "Y", TRANS_STATUS: null, STAFF_LOGON_SESSIONS: staffLogonSessions, SEARCH_FILTER: self.searchFilter};
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.updateStatus = function (StatusAction) {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var refId = $("#REQUEST_ID").val();
        var refIdline = $("#AM_PRREQITEMID").val();
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        postData = { AM_PRREQITEMID: refIdline, REQUEST_ID: refId, STATUS: StatusAction, STAFF_LOGON_SESSIONS: staffLogonSessions };

        ah.toggleSaveMode();
        $.post(self.getApi() + ah.config.api.updateStatus, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    }
    self.saveInfo = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //var itemDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.reqList);


        var receviceLists = JSON.parse(jsonObj);
  
        var act = ah.getParameterValueByName(ah.config.fld.action);
        
        var transAction = $("#TRANSFER_ACTION").val();
        if (transAction === null || transAction === "") {
            transAction = "X";
        }
        if (act !== 'return') {
            var lThanzero = receviceLists.filter(function (item) { return item.PENDING_QTY.toString() === "-0" || item.PENDING_QTY <= -1 });

            if (lThanzero.length > 0) {

                alert('Unable to proceed. Item name: ' + lThanzero[0].PART_DESC + ', have negative transfer qty.')
                return;
            }
        }
        if (act === 'return') {

            var lThanzero = receviceLists.filter(function (item) { return item.QTY_FORRETURN.toString() === "-0" || item.QTY_FORRETURN <= -1 });

        if (lThanzero.length > 0) {

            alert('Unable to proceed. Item name: ' + lThanzero[0].PART_DESC + ', have negative transfer qty.')
            return;
        }
            //var gthanTransfer = receviceLists.filter(function (item) { return item.TRANSFER_QTY < (parseInt(item.QTY_FORRETURN) + parseInt(item.QTY_RETURN)) });
          
            //    if (gthanTransfer.length > 0) {

            //        alert('Unable to proceed. Item name: ' + gthanTransfer[0].PART_DESC + ', return qty cannot be greater than the transfer qty.')
            //        return;
            //    }
  
        } else {
          //  if (transAction !== "D") {
                var gthanOnhand = receviceLists.filter(function (item) { return item.STOCK_ONHAND < item.PENDING_QTY });
               
                if (gthanOnhand.length > 0) {
                    alert('Unable to proceed. Item name: ' + gthanOnhand[0].PART_DESC + ', Dispatch qty cannot be greater than the stock on hand.')
                    return;
                }

                var gthanOnhand = receviceLists.filter(function (item) { return item.QTY < (parseInt(item.PENDING_QTY) + parseInt(item.TRANSFER_QTY)) });
             
                if (gthanOnhand.length > 0) {
      
                    alert('Unable to proceed. Item name: ' + gthanOnhand[0].PART_DESC + ', Dispatch qty cannot be greater than the Requested qty.')
                    return;
                }
          //  }
           
        }
       

       
        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        strDateStart = strDateStart + ' ' + hourmin;

        actionStr = $("#POSTID").val();
       
        //if (ah.CurrentMode == ah.config.mode.add) {

        postData = { ACTION: actionStr, CREATE_DATE: strDateStart, AM_PARTSREQUEST: receviceLists, STAFF_LOGON_SESSIONS: staffLogonSessions };
         
    
        ah.toggleSaveMode();
        $.post(self.getApi() + ah.config.api.updatePartsRequest, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        //}

    };

    self.webApiCallbackStatus = function (jsonData) {
        //alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            //window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.AM_PARTSREQUESTCONFICT || jsonData.AM_WORK_ORDERS || jsonData.AM_PARTSREQUEST || jsonData.STORES || jsonData.PRODUCT_STOCKONHAND || jsonData.DISPATCH_WOPARTS_V || jsonData.STAFF || jsonData.SUCCESS) {
           
            if (ah.CurrentMode == ah.config.mode.save) {
                //$(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                //ah.showSavingStatusSuccess();

                sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_PARTSREQUEST));
                sessionStorage.setItem(ah.config.skey.stockStoreList, JSON.stringify(jsonData.PRODUCT_STOCKONHAND));
                sessionStorage.setItem(ah.config.skey.stockReqStoreList, JSON.stringify(jsonData.PRODUCT_STOCKONHAND_REQSTORE));

                if (jsonData.SUCCESS) {
                    $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                    ah.showSavingStatusSuccess();
                    $(ah.config.cls.liApiStatus).hide();
                   
                    self.showDetails(false, jsonData.AM_PARTSREQUEST);
                }
                else {
                     
                   
                    if (jsonData.AM_PARTSREQUESTCONFICT != null) {
                        $(ah.config.cls.divProgress).hide();
                        ah.toggleDisplayMode();
                        $(ah.config.cls.liApiStatus).hide();  
                        self.showConflictReq(jsonData);
                    } else {
                        $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                        ah.showSavingStatusSuccess();
                        self.showDetails(true, jsonData.AM_PARTSREQUEST);
                        $(ah.config.cls.liApiStatus).hide();  
                    }

                    
                }

            }
            else if (ah.CurrentMode == ah.config.mode.deleted) {

                $(ah.config.id.successMessage).html(systemText.deletedSuccessMessage);
                ah.showSavingStatusSuccess();
                ah.toggleDisplayMode();
            }
            else if (ah.CurrentMode == ah.config.mode.read) {

                workOrderInfo = jsonData.AM_WORK_ORDERS;
                sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_PARTSREQUEST));
                self.showDetails(true, jsonData.AM_PARTSREQUEST);
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
              
                    self.searchResult(jsonData);
     

            }
            else if (ah.CurrentMode == ah.config.mode.idle) {

                var store = jsonData.STORES;
                $.each(store, function (item) {
                    $(ah.config.id.storeFrom)
                        .append($("<option></option>")
                        .attr("value", store[item].STORE_ID)
                        .text(store[item].DESCRIPTION));
                });
                $.each(store, function (item) {
                    $(ah.config.id.storeTo)
                        .append($("<option></option>")
                        .attr("value", store[item].STORE_ID)
                        .text(store[item].DESCRIPTION));
                });


                var woNo = ah.getParameterValueByName(ah.config.fld.woNo);
                if (woNo != null && woNo != "") {
                    self.readStockRequestExist(woNo);
                }
                
            }

        }
        else {

            alert(systemText.errorTwo);
            //window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};