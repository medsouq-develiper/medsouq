
/*Controls visibility and accessibility of controls*/
var AppHelper = function () {

    var thisApp = this;

    thisApp.config = {
        cls: {
            notifySuccess: '.notify-success',
            notifyError: '.notify-error',
            mobileHide: '.mobileHide',
            detailItem: '.detailItem',
            logonMain: '.logon-main'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID',
            dataItems: 'data-items'
        },
        id: {
            btnSignInStatus: '#btnSignInStatus',
            divSuccess: '#divSuccess',
            btnSignInLabel: '#btnSignInLabel',
            formContainer: '#form-container',
			frmLogin: '#frmLogin',
			frmStaffUser: '#frmStaffUser',
            needHelp: '#needHelp',
            emptyUserId: '#emptyUserId',
            invalidUserId: '#invalidUserId',
            successMail: '#successMail',
            invalidemailId: '#invalidemailId',
            searchResult1: '#searchResult1',
			copyRight: '#copyRight',
			nPassword: '#NEW_PW',
            rPassword: '#RE_PW',
            frmInfoPM: '#frmInfoPM',
            ppmstart: '#ppmstart'
        },
        tag: {
           
            ul: 'ul'
        },
        fld: {
            changepasswordsuccess: 'changepasswordsuccess'
        },
        tagId: {
            btnBlue: 'btnBlue',
            btnGreen: 'btnGreen',
            btnRed: 'btnRed',
            btnBlack: 'btnBlack'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            userApps: 'USER_APPLICATION_V',
            groupPrivileges: 'GROUP_PRIVILEGES',
            cssHomeSite: 'cssHomeSite',
            cssSite: 'cssSite',
            userBuilding: 'USER_BUILDING',
            userClient: 'USER_CLIENT',
            sysDefault: '0ed52a42-9019-47d1-b1a0-3fed45f32b8f'
        },
        url: {
            homeMenu: '/Menuapps/aimsIndex#',
            homeIndex: '',
			modalUserApplications: '#openModalInfo',
			modalUserPassword: '#openModalChangePassword',
            closeModal: '',
            openModalChangePasswordActive: '#openModalChangePasswordActive'
        },
        api: {
            authenticateUser: 'api/Security/AuthenticateUser',
            updatestaffApi: '/Setup/insertSessionDR',
            createMail: 'api/Security/CreateMail',
            updateStaffUser: 'api/Security/updateUserPassword',
            getIPAddress: 'api/clientIpAddress/getClientIp',
            updatechangepw: 'api/Security/updateUserPasswordChange',
            updatepm: 'api/Search/UpdatePMSchedule'

        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}&nbsp;</a>",
            searchRowTemplate: '<span class="search-row-label">{0} <span class="search-row-data">{1}</span></span>'
        }
    };
    thisApp.mobileHide = function () {
        $(thisApp.config.cls.mobileHide).hide();
        $(thisApp.config.id.copyRight).show();
    };
    thisApp.appsShow = function () {
        $(thisApp.config.cls.mobileHide).show();
        $(thisApp.config.id.copyRight).hide();

    };
    thisApp.initializeLayout = function () {

        $(thisApp.config.cls.notifyError).hide();
        $(thisApp.config.id.btnSignInStatus +',' + thisApp.config.id.ppmstart).hide();
        $(thisApp.config.id.divSuccess + ',' + thisApp.config.id.needHelp).hide();
        $(".login-page").css("height", (window.innerHeight - 40) + "px");
    };
    thisApp.needHelp = function () {

        $(thisApp.config.cls.notifyError).hide();
        $(thisApp.config.id.btnSignInStatus + ',' + thisApp.config.id.frmLogin).hide();
        $(thisApp.config.id.needHelp).show();
    };
    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    thisApp.hideMessage = function () {

        $(thisApp.config.id.emptyUserId + ',' + thisApp.config.id.invalidUserId + ',' + thisApp.config.id.successMail + ',' + thisApp.config.id.invalidemailId).hide();
    };

    thisApp.ShowLogonStatusCurrent = function () {

        $(thisApp.config.cls.notifyError).hide();
        $(thisApp.config.id.btnSignInStatus).show();
        $(thisApp.config.id.btnSignInLabel).hide();

    };

    thisApp.ShowLogonStatusFailed = function () {

        $(thisApp.config.id.btnSignInStatus).hide();
        $(thisApp.config.id.btnSignInLabel).show();
        $(thisApp.config.cls.notifyError).show();
        $(thisApp.config.cls.notifyError).fadeOut(3500, function () { });
    };

    thisApp.ShowLogonStatusSuccess = function () {

        $(thisApp.config.id.formContainer).fadeOut(700, function () {
            $(thisApp.config.id.divSuccess).show();
            $(thisApp.config.cls.logonMain).hide();
        });
       
	};
	thisApp.formatJSONDateToString = function () {

		var dateToFormat = arguments[0];
		var strDate;

		if (dateToFormat === null) return "-";
		strDate = String(dateToFormat).split('-', 3);

		dateToFormat = strDate[0] + strDate[1] + strDate[2].substring(0, 2);


		return dateToFormat;
	};

    thisApp.ConvertFormToJSON = function (form) {
        
        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;
        
    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var logonViewModel = function (systemText) {

    var self = this;
   

    self.USER_ID = ko.observable("");
    self.PASSWORD = ko.observable("");
    self.ERROR_MESSAGE = ko.observable("");
    self.SUCCESS_MESSAGE = ko.observable("");
   
    var ah = new AppHelper();
    if (outerWidth >= 600) {
        console.info("showing");
        $("#map1").show();
        $("#map0").show();
        ah.appsShow();
    } else {
        console.info("hiding");
        $("#map1").show();
        $("#map0").show();
        ah.mobileHide();
    };
    $("#changePWSC").hide();
    ah.initializeLayout();
    var Changepasswordsuccess = ah.getParameterValueByName(ah.config.fld.changepasswordsuccess);
    if (Changepasswordsuccess == 'asdfsadfasdxcvuhihsdjkfsdf') {
        $("#changePWSC").show();

    }
	ah.hideMessage();
    self.searchFilter = {
        userid: ko.observable(''),
        curpassword: ko.observable(''),
        newpassword: ko.observable(''),
    }


    window.onresize = function (event) {
        if (event.currentTarget.outerWidth >= 1000) {
            console.info("showing");
            $("#map1").show();
            $("#map0").show();
            ah.appsShow();
        } else {
            console.info("hiding");
            $("#map1").hide();
            ah.mobileHide();
            $("#map0").hide();
        }
        $(".login-page").css("height", (window.innerHeight - 40) + "px");
    };
    self.forgotPassword = function () {
        var logonCredentials = ah.ConvertFormToJSON($(ah.config.id.frmLogin)[0]);
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        ah.hideMessage();
      
        if (logonCredentials.USER_ID === null || logonCredentials.USER_ID === "" || logonCredentials.USER_ID.length < 4) {
            $(ah.config.id.emptyUserId).show();
            return;
        };
        postData = {STAFF: logonCredentials };

        $.post(webApiURL + ah.config.api.createMail, postData).done(self.logonResult).fail(self.logonResult);
    };
    self.needHelp = function () {
        ah.needHelp();
  
    };
    self.schemeColor = function () {

        var senderID = $(arguments[1].currentTarget).attr('id');
        var headerText = '';
        var subHeaderText = '';

        switch (senderID) {
            case ah.config.tagId.btnBlue: {
                sessionStorage.setItem(ah.config.skey.cssHomeSite, "home_blue.css");
                window.location.href = ah.config.url.homeIndex;;
                break;
            }
            case ah.config.tagId.btnGreen: {
                sessionStorage.setItem(ah.config.skey.cssSite, "siteGreen.css");
                sessionStorage.setItem(ah.config.skey.cssHomeSite, "home_green.css");
                window.location.href = ah.config.url.homeIndex;;
                break;
            }

            case ah.config.tagId.btnRed: {
                sessionStorage.setItem(ah.config.skey.cssSite, "siteRed.css");
                sessionStorage.setItem(ah.config.skey.cssHomeSite, "home_red.css");
                window.location.href = ah.config.url.homeIndex;;
                break;
            }
            case ah.config.tagId.btnBlack: {
                sessionStorage.setItem(ah.config.skey.cssSite, "siteBlack.css");
                sessionStorage.setItem(ah.config.skey.cssHomeSite, "home_index.css");
                window.location.href = ah.config.url.homeIndex;;
                break;
            }



        }

    };
    self.changePassword = function () {
        $("#invalidInfo").hide();
        $("#validInfo").hide();
        
        window.location.href = ah.config.url.openModalChangePasswordActive;
    };
	self.validateUser = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));

		var expiryDate = ah.formatJSONDateToString(staffDetails.PASSWORD_EXPIRY);
		
		var jDate = new Date();
		var todayDate = jDate.getFullYear() + ("0" + (jDate.getMonth() + 1)).slice(-2) + ("0" + jDate.getDate()).slice(-2);
		
		
		if (parseInt(expiryDate) < parseInt(todayDate)) {
			ah.ShowLogonStatusFailed();
			self.USER_ID('');
			self.PASSWORD('');
			self.ERROR_MESSAGE(systemText.errorFour);
			return;
		};
		if (staffDetails.STATUS === 'INACTIVE') {
			ah.ShowLogonStatusFailed();
			self.USER_ID('');
			self.PASSWORD('');
			self.ERROR_MESSAGE(systemText.errorThree);
			return;
		}


        if (staffDetails.PROMPT_PASSWORD === 'Y' && staffDetails.PROMPT_PASSWORD !== "") {
            $("#validChangeInfo").hide();
			window.location.href = ah.config.url.modalUserPassword;
			return;
		}
        self.getclientIP();
		
		self.SUCCESS_MESSAGE(systemText.logonSuccessMessage);
		ah.ShowLogonStatusSuccess();
		setTimeout(function () {
			if (staffDetails.HOME_DEFAULT != null && staffDetails.HOME_DEFAULT != "") {
				window.location.href = staffDetails.HOME_DEFAULT;
			} else {
				window.location.href = ah.config.url.homeMenu;
			}
		}, 3000);
    };

    self.updatenewPassword = function () {
        var nPsswrd = $("#NEW_PASSWORD").val();
        var rPsswrd = $("#RE_PASSWORD").val();
        var userid = $("#CUR_USERID").val();
        var curpassword = $("#CUR_PASSWORD").val();

        if (userid === null || userid === "") {
            alert('Unable to proceed. Please provide your user id');
            return;
        }

        if (curpassword === null || curpassword === "") {
            alert('Unable to proceed. Please provide your current password');
            return;
        }

        if (nPsswrd === null || nPsswrd === "") {
            alert('Unable to proceed. Please make sure that the new password entered.');
            return;
        }
        if (rPsswrd === null || rPsswrd === "") {
            alert('Unable to proceed. Please make sure that the re-enter password entered.');
            return;
        }

        if (nPsswrd !== rPsswrd) {
            alert('Unable to proceed. Please make sure re-enter password is the same as new password.');
            return;
        }

        if (nPsswrd.length < 6) {
            alert('Unable to proceed. Please make sure your new password should no be less than 6 digit/character.');
            return;
        }

        self.searchFilter.userid = userid;
        self.searchFilter.curpassword = curpassword;
        self.searchFilter.newpassword = nPsswrd;
        self.ERROR_MESSAGE(systemText.pwLogin);
        var logonCredentials = ah.ConvertFormToJSON($(ah.config.id.frmInfoPM)[0]);
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        postData = { MOREXDK: 'asdfasdfxsde', SEARCH_FILTER: self.searchFilter };
       // window.location.href = ah.config.url.homeIndex;
        $.post(webApiURL + ah.config.api.updatechangepw, postData).done(self.logonResult).fail(self.logonResult);

    }
	self.closeModal = function () {
		window.location.href = ah.config.url.homeIndex;
	};
	self.updatePassword = function () {

        $("#validChangeInfo").hide();
		
		var nPsswrd = $(ah.config.id.nPassword).val();
		var rPsswrd = $(ah.config.id.rPassword).val();
		if (nPsswrd === null || nPsswrd === "") {
			alert('Unable to proceed. Please make sure that the new password entered.');
			return;
		}
		if (rPsswrd === null || rPsswrd === "") {
			alert('Unable to proceed. Please make sure that the re-enter password entered.');
			return;
		}
	
		if (nPsswrd !== rPsswrd) {
			alert('Unable to proceed. Please make sure re-enter password is the same as new password.');
			return;
		}

		if (nPsswrd.length < 6) {
			alert('Unable to proceed. Please make sure your new password should no be less than 6 digit/character.');
			return;
		}
		
		//ah.ShowLogonStatusCurrent();
        var logonCredentials = ah.ConvertFormToJSON($(ah.config.id.frmLogin)[0]);
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        postData = { MOREXDK: nPsswrd, STAFF: logonCredentials };
        //window.location.href = ah.config.url.homeIndex;
        $.post(webApiURL + ah.config.api.updateStaffUser, postData).done(self.logonResult).fail(self.logonResult);
		
	}
	self.applyUpdate = function (userPW) {

	};
    self.loginApp = function () {
        var appInfo = JSON.parse(this.getAttribute(ah.config.attr.datainfoId)); 
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        staffDetails.APP_ID = appInfo.APP_ID;
        if (appInfo.APP_HOMEURL === null || appInfo.APP_HOMEURL === "") {
            alert('The application that you selected is under construction. Please select another application to proceed.');
            window.location.href = ah.config.url.homeIndex;
           
        } else {
			self.validateUser();
        }
    }
    self.getclientIP = function () {

     

      
    };

    self.checkrunPmSchedule = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        postData = { STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(webApiURL + ah.config.api.updatepm, postData).done(self.logonResult).fail(self.logonResult);
    };

    self.authenticateUser = function () {
        
        var logonCredentials = ah.ConvertFormToJSON($(ah.config.id.frmLogin)[0]);
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
       
        ah.ShowLogonStatusCurrent();
        var xkdjedkxisdde = self.Encrypt(logonCredentials.PASSWORD, webApiURL, logonCredentials.USER_ID);
        xkdjedkxisdde = xkdjedkxisdde.toString();
      
        logonCredentials.EMAIL_ADDRESS = webApiURL;
        postData = { MOREXDK: xkdjedkxisdde, STAFF: logonCredentials };
        $.post(webApiURL + ah.config.api.authenticateUser, postData).done(self.logonResult).fail(self.logonResult);
       
    };

    self.Encrypt = function (as_toencrypt, as_key1, as_key2) {
        var ls_ret = "";
        var ls_key = "";
        var li_encrypt_pos = 0;
        var li_key_pos = 0;
        var li_encrypt_len = 0;
        var li_key_len = 0;
        var li_key_asc;
        var li_encrypt_asc;

		as_toencrypt = as_toencrypt.toUpperCase();
	
        li_encrypt_len = as_toencrypt.length;

        ls_key = as_key1 + as_key2;
        li_key_len = ls_key.length;
        var ls_encrypt_bin = ""
        var ls_key_bin = "";
        var bytes = [];
        var iix = 1;

        for (var i = 0; i < as_toencrypt.length; ++i) {
            var res = as_key1.slice(-iix);
            iix++;
            var restcode = res.slice(0, 1);
			var keystr = (as_toencrypt.charCodeAt(i)).toString();
	
            var keyInt = keystr * 2;
            keystr = String.fromCharCode(keyInt);

            ls_key_bin += keystr;
            bytes.push(ls_key_bin);
        }
		//alert(ls_key_bin);
        return ls_key_bin;
    };
    self.InvalidEmail = function () {
       
    };

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.checkApi = function (jsonData) {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var api = "";

        if (staffDetails.API === null || staffDetails.API === "") {
            
        } else {
     
                postData = { STAFF_LOGON_SESSIONS: staffLogonSessions, STAFF: staffDetails };
                $.post(staffDetails.API + "api" + ah.config.api.updatestaffApi, postData).done(self.logonResult).fail(self.logonResult);
        
        }

    }



    self.logonResult = function (jsonData) {
      
		if (jsonData.ERROR) {
            //alert(jsonData.ERROR);
			//alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
			if (jsonData.ERROR.ErrorText === 'License Expired') {
				self.USER_ID('');
				self.PASSWORD('');
				alert(jsonData.ERROR.ErrorText);
				ah.ShowLogonStatusFailed();
            } else if (jsonData.ERROR.ErrorText === 'User not found.' || jsonData.ERROR.ErrorText === 'Wrong Password not found.'){
                self.USER_ID('');
                self.PASSWORD('');
                $("#validInfo").hide();
                $("#invalidInfo").show();
            }else {
				ah.ShowLogonStatusFailed();

				self.USER_ID('');
				self.PASSWORD('');
				self.ERROR_MESSAGE(systemText.errorOne);
				}

            /*invalid userid/password*/
           

        }
        else if (jsonData.PASSWORDUPDATED) {
            $("#invalidInfo").hide();
            $("#validInfo").show();
        }
        else if (jsonData.CHANGEPASSED) {
       
            $("#validChangeInfo").show();
        }
        else if (jsonData.STAFF_LOGON_SESSIONS && jsonData.STAFF && jsonData.GROUP_PRIVILEGES && jsonData.USER_BUILDING && jsonData.USER_CLIENT && jsonData.SYSTEM_DEFAULT) {
            
            /*success*/
             
            sessionStorage.setItem(ah.config.skey.staffLogonSessions, JSON.stringify(jsonData.STAFF_LOGON_SESSIONS));
            sessionStorage.setItem(ah.config.skey.staff, JSON.stringify(jsonData.STAFF));
            sessionStorage.setItem(ah.config.skey.userBuilding, JSON.stringify(jsonData.USER_BUILDING));
            sessionStorage.setItem(ah.config.skey.userClient, JSON.stringify(jsonData.USER_CLIENT));
            sessionStorage.setItem(ah.config.skey.sysDefault, JSON.stringify(jsonData.SYSTEM_DEFAULT));
            
            if (jsonData.PPMSTATUS == "N") {
                $(ah.config.id.ppmstart).show();
                self.checkrunPmSchedule(); 
                return;
            }

            if (jsonData.STAFF.API != null && jsonData.STAFF.API != "") {
                // alert('here');
                sessionStorage.setItem(ah.config.skey.groupPrivileges, JSON.stringify(jsonData.GROUP_PRIVILEGES));
                self.checkApi();
            }
            else {
                var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
                
                sessionStorage.setItem(ah.config.skey.groupPrivileges, JSON.stringify(jsonData.GROUP_PRIVILEGES));
				self.validateUser();    
            }   
        }
        else if (jsonData.PPMDONE) {
            //$(ah.con.id.ppmstart).text = "PPM Schedule creation is done.";
            $(ah.config.id.ppmstart).hide();
            alert('PPM Schedule creation is done.');
            self.validateUser();
        }
        else if (jsonData.SESSIONOK) {
            var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
           
			self.validateUser();
       
        }
        else if (jsonData.EMAILOK) {
            $(ah.config.id.successMail).show();
        }
        else if (jsonData.INVALIDEMAIL) {
            //self.InvalidEmail();
            //alert('here');
            $(ah.config.id.invalidemailId).show();
        }
        else if (jsonData.INVALIDUSERID) {
            $(ah.config.id.invalidUserId).show();
        }
        else{

            /*no server*/
            ah.ShowLogonStatusFailed();

            self.USER_ID('');
            self.PASSWORD('');
            self.ERROR_MESSAGE(systemText.errorTwo);

        }
        
    };
};
