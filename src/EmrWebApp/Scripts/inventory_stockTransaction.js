﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            licrit: '.licrit',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            detailItem: '.detailItem',
            liassetTag: '.liassetTag',
            wsAsset: '.wsAsset',
            itemRequest: '.itemRequest'
        },
        fld: {
            act: 'act'
        },
        tag: {
            input: '.wsAsset input',
            textArea: '.wsAsset textarea',
            select: '.wsAsset select',
            inputTypeText: '.wsAsset input[type=text]',
            inputTypeEmail: '.wsAsset input[type=email]',
            inputTypeNumber: '.wsAsset input[type=number]',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7,
            deleted: 8
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmInfo: '#frmInfo',
            frmInfoInspection: '#frmInfoInspection',
            clinicdoctorID: '#CLINIC_DOCTOR_ID',
            clinicID: '#CLINIC_ID',
            frmwsAsset: '#frmwsAsset',
            frmInfoReading: '#frmInfoReading',
            staffID: '#STAFF_ID',
            saveInfoLog: '#saveInfoLog',
            serviceID: '#SERVICE_ID',
            searchResult1: '#searchResult1',
            successMessage: '#successMessage',
            saveInfoReading: '#saveInfoReading',
            txtGlobalSearchOth: '#txtGlobalSearchOth',
            pageNum: '#pageNum',
            pageCount: '#pageCount',
            loadNumber: '#loadNumber',
            btnRemove: '#btnRemove',
            storeFrom: '#STOREFROM',
            storeTo: '#STORETO',
            alertmessage: '#alertmessage'
        },

        cssCls: {
            viewMode: 'view-mode'
        },
        tagId: {

            txtGlobalSearchOth: 'txtGlobalSearchOth',
            priorPage: 'priorPage',
            nextPage: 'nextPage'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfo: 'data-info',
            datainfoID: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'searchResult',
            infoDetails: 'AM_PURCHASEREQUEST',
            stockList: 'PRODUCT_STOCKONHAND',
            wsassetList: 'AM_INSPECTIONASSET',
            stockStoreList: 'PRODUCT_STOCKONHAND',
            stockReqStoreList: 'PRODUCT_STOCKONHAND_REQSTORE',
            defaultCostCenter: '0ed52a42-9019-47d1-b1a0-3fed45f32b8f',
        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalLookUp: 'openModalLookup',
            modalClose: '#',
            modalAddItems: '#openModal',
            modalReading: 'openModalReading',
            modalFilter: '#openModalFilter'
        },
        api: {

            readStockReq: '/Inventory/SearchStockReqDet',
            searchAll: '/Inventory/SearchStockTransDetFlag',
            createRequest: '/Inventory/CreateNewStocktransaction',
            updateStatus: '/Inventory/UpdateStorkreqStatus',
            seachLookUp: '/Inventory/SearchStores',
            readStockReceiveReq: '/Inventory/SearchStockReceiveReqDet',
            searchStockOnhand: '/Inventory/SearchStockOnhandFromStore',
            searchSetupLov: '/Setup/SearchInventoryLOV',

        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
            searchRowHeaderTemplateList: "<a href='#openModal' class='fs-medium-normal detailItem' data-infoAssetID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            searchLOVRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem fc-greendark' data-info ='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} <span>{3}</span></a>",
            searchExistRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem fc-slate-blue' data-info ='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} <span class='fc-green'> {3} </span></a>"
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };
    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.licrit + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess + ',' + thisApp.config.cls.liassetTag + ',' + thisApp.config.cls.itemRequest
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;
        var modal = document.getElementById('openModalAlert');
        if (modal !== null)
            modal.style.display = "none";

    };
    thisApp.toggleDisplayModereq = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        //$(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        //$(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(thisApp.config.cls.contentField + ',' + thisApp.config.cls.searchAsset).show();
        //$(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.liApiStatus).hide();

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        //$(
        //    thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        //).prop(thisApp.config.attr.readOnly, false);

        //$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        $(".datefield").prop(thisApp.config.attr.disabled, false);

    };
    thisApp.toggleSearchItems = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;


        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liSave +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();
        $(thisApp.config.cls.liSave).hide();

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

    };
    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.liassetTag + ',' + thisApp.config.cls.itemRequest
        ).hide();


        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        $(".datefield").prop(thisApp.config.attr.disabled, false);

    };
    thisApp.toggleEditModeModal = function () {


        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
    };
    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.licrit + ',' + thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.liassetTag
        ).hide();

    };
    thisApp.toggleSearchModalMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;


    };
    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.itemRequest).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.liassetTag).hide();

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        $(".datefield").prop(thisApp.config.attr.disabled, false);
        $(".remove-inv").each(function (i) { $(this).show() });
    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();
        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, true);
        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();

        $(".datefield").prop(thisApp.config.attr.disabled, false);
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.licrit + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, true);
        $(".datefield").prop(thisApp.config.attr.disabled, false);
        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);
        $(".remove-inv").each(function (i) { $(this).hide() });
    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };



    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };
    thisApp.formatJSONDateToStringOth = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[0] + '-' + strDate[1] + '-' + strDate[2].substring(0, 2);


        return dateToFormat;
    };
    thisApp.formatJSONDateToString = function () {

        var dateToFormat = arguments[0];
        var strDate;

        if (dateToFormat === null) return "-";
        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };
    thisApp.formatJSONDateTimeToString = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0] + ' ' + strTime[1].substring(0, 5);


        return dateToFormat;
    };
    thisApp.formatJSONTimeToString = function () {

        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        var timeToFormat = strTime[1].substring(0, 5);


        return timeToFormat;
    };
    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');


            if (isDateTime) {
                val = thisApp.formatJSONDateTimeToString(val);
            }

            if (isDate) {
                val = thisApp.formatJSONDateToString(val);
            }

            var $el = $('#' + name), type = $el.attr('type'), isTime = $el.attr('data-istime');
            if (isTime) {
                if (val === null || val === "") {
                    val = "00:00";
                } else {
                    val = thisApp.formatJSONTimeToString(val);
                }

            }

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var amStockTransactionViewModel = function (systemText) {

    var self = this;
    self.searchKeyword = ko.observable('');
    self.searchFilter = {
        fromDate: ko.observable(''),
        toDate: ko.observable(''),
        status: ko.observable(''),
        fromStore: ko.observable(0),
        toStore: ko.observable(0),
    }
    //self.fromDate = ko.observable();
    //self.toDate = ko.observable();
    //self.status = ko.observable();
    //self.fromStore = ko.observable();
    //self.toStore = ko.observable();
    self.searchKeyUp = function (d, e) {
        if (e.keyCode == 13) {
            self.searchNow();
        }
    }

    var ah = new appHelper(systemText);
    var modal = document.getElementById('openModalAlert');
    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.toggleReadMode();

        ah.initializeLayout();
        var act = ah.getParameterValueByName(ah.config.fld.act);

        if (act === "transfer" || act === "receive") {

            $(ah.config.cls.liAddNew).hide()
        }
        postData = { SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.when($.post(self.getApi() + ah.config.api.seachLookUp, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus)).done(function () {
            var postData = { LOV: "'SHIP_VIA','FOREIGN_CURRENCY'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchSetupLov, postData).done(function (jsonData) {
                var shipVias = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'SHIP_VIA' });;
                shipVias.unshift({
                    LOV_LOOKUP_ID: null,
                    DESCRIPTION: '- select -'
                });
                self.shipVias(shipVias);

                var foreignCurrencies = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'FOREIGN_CURRENCY' });;
                foreignCurrencies.unshift({
                    LOV_LOOKUP_ID: null,
                    DESCRIPTION: '- select -'
                });
                self.foreignCurrencies(foreignCurrencies);
            }).fail(self.webApiCallbackStatus)
        });
    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.reqList = ko.observableArray("");

    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.dateClick = function () {
        $("#dateClickAction").val("ACTIVE");
    };
    self.showModalFilter = function () {

        window.location.href = ah.config.url.modalFilter;
    };
    self.createNew = function () {
        self.reqList.splice(0, 5000);

        ah.toggleAddMode();

        var jDate = new Date();

        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        var reqDate = strDateStart + ' ' + hourmin;

        // alert(tranStatus);

        $("#REQUEST_DATE").val(reqDate);

        $("#TRANS_STATUS").val("OPEN");

    };
    self.searchWONow = function () {
        var refId = $("#REF_WO").val();
        if (refId === null || refId === "0" || refId === " ") {
            alert('Unable to proceed. Please provide a valid Work Order to Proceed.')
            return;
        }
        self.readWOId(refId);
    };
    self.pageLoader = function () {
        var senderID = $(arguments[1].currentTarget).attr('id');

        var searchStr = $(ah.config.id.txtGlobalSearchOth).val().toString();

        var pageNum = parseInt($(ah.config.id.pageNum).val());

        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.stockList));
        var lastPage = Math.ceil(sr.length / 10);

        var pageCount = parseInt($(ah.config.id.pageCount).val());
        if (isNaN(pageCount)) {
            pageCount = 1;
        }
        if (isNaN(pageNum)) {
            pageNum = 0;
        }

        switch (senderID) {

            case ah.config.tagId.nextPage: {

                if (pageCount < lastPage) {
                    pageNum = pageNum + 10;
                    pageCount++;
                    self.searchItemsResult(searchStr, pageNum);
                }
                break;
            }
            case ah.config.tagId.priorPage: {

                if (pageNum === 0) {
                } else {
                    pageNum = pageNum - 10;
                    if (pageNum < 0) {
                        pageNum = 0;
                    }
                    pageCount = pageCount - 1;
                }

                self.searchItemsResult(searchStr, pageNum);
                break;
            }

            case ah.config.tagId.txtGlobalSearchOth: {
                pageCount = 1;
                pageNum = 0;

                self.searchItemsResult(searchStr, 0);
                break;
            }
        }

        $(ah.config.id.loadNumber).text(pageCount.toString());
        $(ah.config.id.pageCount).val(pageCount.toString());
        $(ah.config.id.pageNum).val(pageNum);

    };
    self.modifySetup = function () {

        ah.toggleEditMode();

    };
    self.cancelChangesModal = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            var refId = $("#AM_INSPECTION_ID").val();
            self.readSetupExistID(refId);

        }

        window.location.href = ah.config.url.modalClose;

    };
    self.alertMessage = function (refMessage) {
        $("#alertmessage").text(refMessage);
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "block";

    };
    self.closeAlertModal = function () {
        modal.style.display = "none"
    }
    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };
    self.stockSearchNow = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        var storeFrom = $("#STOREFROM").val();
        if (storeFrom === null || storeFrom === "") {
            return self.alertMessage("Unable to Proceed. Please select From Store to proceed.");
            //alert('Please select From Store to proceed.');
            //return;
        }

        var storeTo = $("#STORETO").val();
        if (storeTo === null || storeTo === "") {
            return self.alertMessage("Unable to Proceed. Please select To Store to proceed.");
            //alert('Please select To Store to proceed.');
            //return;
        }
        if (storeFrom === storeTo) {
            return self.alertMessage("Unable to Proceed. The From Store and To Store should not the same.");
            //alert('Unable to proceed. The From Store and To Store should not the same.');
            //return;
        }

        ah.toggleSearchItems();
        postData = { SEARCH: "", ACTION: "", STORE_FROM: storeFrom, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchStockOnhand, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = ah.config.url.modalAddItems;

    };
    self.saveAllChanges = function () {

        $(ah.config.id.saveInfo).trigger('click');

    };
    self.addItemReq = function () {

        var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfo));
        //alert(JSON.stringify(infoID));

        var existPart = self.checkExist(infoID.ID_PARTS);
        if (existPart > 0) {
            self.alertMessage("Unable to Proceed. The selected item was already exist.");

            return;
        }
        var reqDate = $("#REQUEST_DATE").val();
        var tranStatus = $("#TRANS_STATUS").val();
        var fromStore = $("#STOREFROM").val();
        var toStore = $("#STORETO").val();
        var refId = $("#TRANS_ID").val();
        // alert(reqDate);

        //var strTime = '';
        //var apptDate = reqDate;
        //reqDate = apptDate.substring(6, 10) + '-' + apptDate.substring(3, 5) + '-' + apptDate.substring(0, 2);

        //strTime = String(apptDate).split(' ');
        //strTime = strTime[1].substring(0, 5);
        //reqDate = reqDate + ' ' + strTime;
        //alert(reqDate);
        self.reqList.push({
            DESCRIPTION: infoID.DESCRIPTION,
            ID_PARTS: infoID.ID_PARTS,
            QTY: 0,
            REQUEST_DATE: reqDate,
            TRANS_TYPE: null,
            STORE_FROM: fromStore,
            STORE_TO: toStore,
            TRANS_STATUS: tranStatus,
            AM_STOCKTRANSACTIONID: 0,
            TRANS_ID: refId,
            REMARKS: null,
            TRANSDETFLAG: null
                
        });

        $(
            ah.config.tag.inputTypeNumber + ',' + ah.config.tag.inputTypeText + ',' + ah.config.tag.inputTypeEmail + ',' + ah.config.tag.textArea
        ).prop(ah.config.attr.disabled, false);
        $(".datefield").prop(thisApp.config.attr.disabled, false);

    };
    self.removeItem = function (reqList) {

        var status = $("#TRANS_STATUS").val();
        if (status === "POSTED") {
            alert('Unable to proceed. Sorry you cannot remove, Status is already Posted.');
            return;
        }
        self.reqList.remove(reqList);

    };

    self.checkExist = function (idPart) {

        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.reqList);
        var partLists = JSON.parse(jsonObj);

        var partExist = partLists.filter(function (item) { return item.ID_PARTS === idPart });

        return partExist.length;
    };
    self.cancelChanges = function () {
        var refId = $("#TRANS_ID").val();
        if (refId === null || refId === "" || refId === "0") {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            //var refId = $("#AM_INSPECTION_ID").val();
            //self.readSetupExistID(refId);
        }
        $(
            ah.config.tag.inputTypeNumber + ',' + ah.config.tag.inputTypeText + ',' + ah.config.tag.inputTypeEmail + ',' + ah.config.tag.textArea
        ).prop(ah.config.attr.disabled, true);
        $(
            ah.config.tag.inputTypeNumber + ',' + ah.config.tag.inputTypeText + ',' + ah.config.tag.inputTypeEmail + ',' + ah.config.tag.select + ',' +
            ah.config.tag.textArea
        ).removeClass(ah.config.cssCls.viewMode);
    };

    self.showDetails = function (isNotUpdate) {

        if (isNotUpdate) {

            var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.infoDetails));

            var prInfo = sr.filter(function (item) { return item.TRANSDETFLAG === 'Y' });
            var sStocklist = JSON.parse(sessionStorage.getItem(ah.config.skey.stockStoreList));
            var reqStoreStocklist = JSON.parse(sessionStorage.getItem(ah.config.skey.stockReqStoreList));

            ah.ResetControls();
            ah.toggleDisplayMode();

            var act = ah.getParameterValueByName(ah.config.fld.act);
            if (prInfo.length > 0) {
                var reqDate = ah.formatJSONDateTimeToString(prInfo[0].REQUEST_DATE);

                var strTime = '';
                var apptDate = reqDate;
                reqDate = apptDate.substring(6, 10) + '-' + apptDate.substring(3, 5) + '-' + apptDate.substring(0, 2);

                strTime = String(apptDate).split(' ');
                strTime = strTime[1].substring(0, 5);
                reqDate = reqDate + ' ' + strTime;

                $("#REQUEST_DATE").val(reqDate);
                $("#TRANSDETFLAG").val(prInfo[0].TRANSDETFLAG);
                $("#TRANS_STATUS").val(prInfo[0].TRANS_STATUS);

                $("#AM_STOCKTRANSACTIONID").val(prInfo[0].AM_STOCKTRANSACTIONID);
                $("#TRANS_ID").val(prInfo[0].TRANS_ID);
                $("#STOREFROM").val(prInfo[0].STORE_FROM);
                $("#STORETO").val(prInfo[0].STORE_TO);


                var currStatus = prInfo[0].TRANS_STATUS;

            }


            //alert(JSON.stringify(prInfo));
            if (sr.length > 0) {
                self.reqList.splice(0, 5000);
                for (var o in sr) {
                    var cttd_qty = 0;
                    var onHand = 0;
                    if (act === "receive") {
                        var itemInfo = reqStoreStocklist.filter(function (item) { return item.PRODUCT_CODE === sr[o].ID_PARTS });
                        //  alert(JSON.stringify(itemInfo));
                        if (itemInfo.length > 0) {
                            cttd_qty = itemInfo[0].COMMITTED_QTY;
                            onHand = itemInfo[0].QTY - cttd_qty;
                            if (onHand < 0) {
                                onHand = 0;
                            }
                        }

                    }
                    else {
                        if (act === "request") {
                            $(ah.config.id.contentSubHeader).hide();

                        }
                        var itemInfo = sStocklist.filter(function (item) { return item.PRODUCT_CODE === sr[o].ID_PARTS });

                        if (itemInfo.length > 0) {

                            cttd_qty = itemInfo[0].COMMITTED_QTY;
                            if (cttd_qty === null || cttd_qty === "") {
                                cttd_qty = 0;
                            }

                            onHand = itemInfo[0].QTY - cttd_qty;


                            if (onHand < 0) {
                                onHand = 0;
                            }
                        }

                    }
                    var rDate = ah.formatJSONDateToString(sr[o].REQUEST_DATE);
                    self.reqList.push({
                        DESCRIPTION: sr[o].DESCRIPTION,
                        ID_PARTS: sr[o].ID_PARTS,
                        QTY: sr[o].QTY,
                        REQUEST_DATE: sr[o].REQUEST_DATE,
                        TRANS_ID: sr[o].TRANS_ID,
                        STORE_FROM: sr[o].STORE_FROM,
                        STORE_TO: sr[o].STORE_TO,
                        TRANS_STATUS: sr[o].TRANS_STATUS,
                        AM_STOCKTRANSACTIONID: sr[o].AM_STOCKTRANSACTIONID,
                        ONHAND: onHand,
                        COMMITTED: cttd_qty,
                        QTY_TRANSFER: sr[o].QTY_TRANSFER,
                        REMARKS: sr[o].REMARKS,
                        DELIVER_DATE: sr[o].DELIVER_DATE,
                        DELIVER_BY: sr[o].DELIVER_BY,
                        REQUEST_BY: sr[o].REQUEST_BY,
                        TRANSDETFLAG: sr[o].TRANSDETFLAG,
                        TRANS_TYPE: sr[o].TRANS_TYPE,
                    })

                }

                if (currStatus === 'TRANSFER' || currStatus === 'POSTED' || currStatus === 'CANCELLED' || currStatus === 'RECEIVED') {
                    $(ah.config.cls.liModify).hide();

                    $(".remove-inv").each(function (i) { $(this.remove()) });


                } else {
                    $(ah.config.id.contentSubHeader).show();
                    $(".remove-inv").each(function (i) { $(this).hide() });
                }


            }


            if (act === "transfer" || act === "receive") {
                if (prInfo.length > 0) {

                    if (prInfo[0].TRANS_STATUS === "TRANSFER" && act === "transfer") {
                        $(ah.config.cls.liModify).hide();
                    } else if (prInfo[0].TRANS_STATUS === "RECEIVED" && act === "receive") {
                        $(ah.config.cls.liModify).hide();
                    } else {
                        $(ah.config.cls.liModify).show();
                    }
                }

            }
        } else {
            var cancelAction = $("#CANCELACTION").val();
            if (varcancelAction === "CANCELLED") {
                $(ah.config.cls.liModify).hide();
            }
        }
        var act = ah.getParameterValueByName(ah.config.fld.act);
        if (act === "transfer" || act === "receive") {
            $(ah.config.cls.liAddNew).hide()

        }


    };
    self.searchItemsResult = function (LovCategory, pageLoad) {

        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.stockList));
        if (LovCategory !== null && LovCategory !== "") {
            var arr = sr;
            var keywords = LovCategory.toUpperCase();
            var filtered = [];

            for (var i = 0; i < sr.length; i++) {
                for (var j = 0; j < keywords.length; j++) {
                    if ((sr[i].DESCRIPTION.toUpperCase()).indexOf(keywords) > -1 || (sr[i].ID_PARTS.toUpperCase()).indexOf(keywords) > -1) {
                        filtered.push(sr[i]);
                        break;
                    }
                }
            }
            sr = filtered;
        }

        $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).html('');

        if (sr.length > 0) {
            $("#noresult").hide();
            $("#pageSeparator").show();
            $("#page1").show();
            $("#page2").show();
            $("#nextPage").show();
            $("#priorPage").show();
            var totalrecords = sr.length;
            var pagenum = Math.ceil(totalrecords / 10);
            if (pagenum < 2 && pagenum > 1) {
                pagenum = 2;
            }
            $(ah.config.id.totNumber).text(pagenum.toString());

            if (pagenum <= 1) {
                $("#page1").hide();
                $("#page2").hide();
                $("#pageSeparator").hide();
                $("#nextPage").hide();
                $("#priorPage").hide();
            }
            var htmlstr = '';
            var o = pageLoad;

            for (var i = 0; i < 10; i++) {
                htmlstr += '<li>';
                htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                if (sr[o].QTY !== null) {
                    htmlstr += ah.formatString(ah.config.html.searchExistRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].ID_PARTS, sr[o].DESCRIPTION, " Available Stock on Hand: " + sr[o].QTY);
                } else {
                    htmlstr += ah.formatString(ah.config.html.searchLOVRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].ID_PARTS, sr[o].DESCRIPTION, "");
                }



                htmlstr += '</span>';
                htmlstr += '</li>';
                o++
                if (totalrecords === o) {
                    break;
                }
            }
            // alert(JSON.stringify(sr));
            //alert(htmlstr);
            $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).append(htmlstr);
        }
        else {
            $("#page1").hide();
            $("#page2").hide();
            $("#pageSeparator").hide();
            $("#nextPage").hide();
            $("#priorPage").hide();
            $("#noresult").show();
        }
        $(ah.config.cls.detailItem).bind('click', self.addItemReq);
        ah.toggleDisplayModereq();
        //ah.toggleAddItems();
    };
    self.searchResult = function (jsonData) {


        var sr = jsonData.AM_STOCKTRANSACTION;

        var act = ah.getParameterValueByName(ah.config.fld.act);
        //if (act === "transfer") {
        //    var sr = sr.filter(function (item) { return item.TRANS_STATUS === "POSTED" || item.TRANS_STATUS === "TRANSFER" });

        //} else if (act === "receive") {
        //    var sr = sr.filter(function (item) { return item.TRANS_STATUS === "TRANSFER" || item.TRANS_STATUS === "RECEIVED" });
        //}
        //alert(JSON.stringify(sr));
        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {
                var transDate = new Date(sr[o].REQUEST_DATE);
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].TRANS_ID, sr[o].TRANS_ID, 'Request Date: ' + sr[o].REQUEST_DATE, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total Items: ' + sr[o].ITEMREQ + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Status:&nbsp;&nbsp;' + sr[o].TRANS_STATUS + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Store:&nbsp;&nbsp;' + sr[o].FROM_STOREDESC + '&nbsp;&nbsp;&nbsp;To Store:&nbsp;&nbsp;' + sr[o].TO_STOREDESC);
                //htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.inspectionprocess, sr[o].PROCESS);

                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.detailItem).bind('click', self.readStockRequest);
        ah.displaySearchResult();
        var act = ah.getParameterValueByName(ah.config.fld.act);
        if (act === "transfer" || act === "receive") {
            $(ah.config.cls.liAddNew).hide()
        }

    };


    self.readStockRequest = function () {

        var infoID = this.getAttribute(ah.config.attr.datainfoID).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { TRANS_ID: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };


        var act = ah.getParameterValueByName(ah.config.fld.act);



        if (act === "receive") {
            $.post(self.getApi() + ah.config.api.readStockReceiveReq, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        } else {
            $.post(self.getApi() + ah.config.api.readStockReq, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }

    };
    self.readWOId = function (refID) {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { REQ_NO: refID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readWO, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.readSetupExistID = function (refId) {


        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { AM_INSPECTION_ID: refId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.statusPost = function () {
        $("#TRANS_STATUS").val("POSTED");
        self.saveInfo();
    }
    self.statusPostTransfer = function () {
        $("#TRANS_STATUS").val("TRANSFER");
        var ctr = 0;
        $("td input").each(function () {
            if ($(this).val() == '') {
                ctr++;
            }
        });
        if (ctr > 0) {
            return self.alertMessage("Unable to Proceed. Transfer Qty must have a value must be greater than 0");
            // alert('Unable to proceed. Transfer Qty must have a value    .')
        }
        else {
            self.saveInfo();
        }
    }
    self.statusReceive = function () {
        $("#TRANS_STATUS").val("RECEIVED");
        self.saveInfo();
    }
    self.statusCancel = function () {
        $(ah.config.cls.liModify).hide();
        $("#CANCELACTION").val("CANCELLED");
        self.updateStatus("CANCELLED")


    }

    self.searchFilterNow = function () {
        window.location.href = ah.config.url.modalClose;
        self.searchNow();
    }

    self.clearFilter = function () {
        self.searchFilter.toDate('');
        self.searchFilter.fromDate('');
        self.searchFilter.status('');
        self.searchFilter.toStore(0);
        self.searchFilter.fromStore(0);
        $('#requestFromDate').val('');
        $('#requestStartDate').val('');
        window.location.href = ah.config.url.modalClose;
    }

    self.searchNow = function () {
        self.setObservableValueEmpty(self.customerRequest);

        self.getDefaultCostCenter();

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;
        ah.toggleSearchMode();
        var fromDate = $('#requestFromDate').val().split('/');

        if (fromDate.length == 3) {
            self.searchFilter.fromDate(fromDate[2] + "/" + fromDate[1] + "/" + fromDate[0]);
        }
        var toDate = $('#requestStartDate').val().split('/');

        if (toDate.length == 3) {
            self.searchFilter.toDate(toDate[2] + "/" + toDate[1] + "/" + toDate[0] + "  23:00:00.000");
        }

        postData = { SEARCH: $("#txtGlobalSearch").val(), RETACTION: "Y", TRANS_STATUS: null, PAGE: 'RECEIVE', STAFF_LOGON_SESSIONS: staffLogonSessions, SEARCH_FILTER: self.searchFilter };
        //alert(JSON.stringify(postData));
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.updateStatus = function (StatusAction) {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var refId = $("#TRANS_ID").val();
        var refIdline = $("#AM_STOCKTRANSACTIONID").val();

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        postData = { TRANS_ID: refId, AM_STOCKTRANSACTIONID: refIdline, STATUS: StatusAction, STAFF_LOGON_SESSIONS: staffLogonSessions };

        ah.toggleSaveMode();
        $.post(self.getApi() + ah.config.api.updateStatus, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    }
    self.saveInfo = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var itemDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        var AM_STOCKTRANSACTION = self.getObservableValue(self.customerRequest);
        AM_STOCKTRANSACTION.REQUEST_DATE = moment(AM_STOCKTRANSACTION.REQUEST_DATE, 'DD/MM/YYYY HH:mm').format();

        for (var i = 0; i < self.reqList().length; i++) {
            if (self.reqList()[i].QTY < 1)
                return self.alertMessage("Unable to Proceed. Quantity value must be greater than 0");
            //;// alert("Unable to Proceed. Quantity value must be greater than 0");
        }

        if (itemDetails.TRANS_STATUS == "TRANSFER") {
            for (var i = 0; i < self.reqList().length; i++) {
                if (self.reqList()[i].QTY_TRANSFER < 1)
                    return self.alertMessage("Unable to Proceed. Quantity value must be greater than 0");
                //return alert("Unable to Proceed. Quantity value must be greater than 0");
            }
        }

        //alert('here');
        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.reqList);


        var receviceLists = JSON.parse(jsonObj);
        //
        if (receviceLists.length <= 0) {
            return self.alertMessage("Unable to Proceed. No item to request.");
            //alert('Unable to proceed. No item to request.')
            //return;
        }


        receviceLists[0].TRANSDETFLAG = 'Y';

        var transStatus = $("#TRANS_STATUS").val();

        var existZero = receviceLists.filter(function (item) { return item.QTY === 0 });

        if (transStatus === "POSTED") {
            if (receviceLists.length > 0) {

                for (var o in receviceLists) {
                    receviceLists[o].TRANS_STATUS = "POSTED";
                    AM_STOCKTRANSACTION.TRANS_STATUS = "POSTED";
                }
            }
        } else if (transStatus === "TRANSFER") {

            var gthanOnhand = receviceLists.filter(function (item) { return item.QTY < item.QTY_TRANSFER });

            if (gthanOnhand.length > 0) {

                return self.alertMessage('Unable to proceed. Item name: ' + gthanOnhand[0].DESCRIPTION + ' transfer qty cannot be greater than the request qty.');
                //alert('Unable to proceed. Item name: ' + gthanOnhand[0].DESCRIPTION + ' transfer qty cannot be greater than the request qty.')
                //return;
            }

            var stockgthanOnhand = receviceLists.filter(function (item) { return item.ONHAND < item.QTY_TRANSFER });
            if (stockgthanOnhand.length > 0) {
                return self.alertMessage("Unable to Proceed. Item name: " + gthanOnhand[0].DESCRIPTION + " transfer qty cannot be greater than the Stock On Hand.");
                //alert('Unable to proceed. Item name: ' + stockgthanOnhand[0].DESCRIPTION + ' transfer qty cannot be greater than the Stock On Hand.')
                //return;
            }
            for (var o in receviceLists) {
                receviceLists[o].TRANS_STATUS = "TRANSFER";
                AM_STOCKTRANSACTION.TRANS_STATUS = "TRANSFER";
            }

        } else if (transStatus === "RECEIVED") {
            var copyStatus = receviceLists[0].TRANS_STATUS;
            for (var o in receviceLists) {
                if (copyStatus == 'TRANSFER') {
                    receviceLists[o].TRANS_STATUS = "RECEIVED";
                    AM_STOCKTRANSACTION.TRANS_STATUS = "RECEIVED";
                }
                if (copyStatus == 'PARTIAL TRANSFER') {
                    receviceLists[o].TRANS_STATUS = "PARTIAL RECEIVED";
                    AM_STOCKTRANSACTION.TRANS_STATUS = "PARTIAL RECEIVED";
                }
            }
        }
        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        strDateStart = strDateStart + ' ' + hourmin;

        actionStr = $("#POSTID").val();
        //if (ah.CurrentMode == ah.config.mode.add) {

        postData = { ACTION: actionStr, CREATE_DATE: strDateStart, AM_STOCKTRANSACTION_MASTER: AM_STOCKTRANSACTION, AM_STOCKTRANSACTION: receviceLists, STAFF_LOGON_SESSIONS: staffLogonSessions };
        // alert(JSON.stringify(receviceLists));

        ah.toggleSaveMode();
        $.post(self.getApi() + ah.config.api.createRequest, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        //}

    };


    self.customerRequest = {
        TRANS_ID: ko.observable(),
        REQUEST_DATE: ko.observable(),
        STORE_TO: ko.observable(),
        STORE_FROM: ko.observable(),
        REMARKS: ko.observable(),
        TRANS_STATUS: ko.observable(),
        TRANSFER_REMARKS: ko.observable(),

        STORE_TO_NAME: ko.observable(),
        STORE_FROM_NAME: ko.observable(),

        FOREIGN_CURRENCY: ko.observable(),
        SHIP_VIA: ko.observable(),
        TOTAL_AMOUNT: ko.observable(),
        TOTAL_NOITEMS: ko.observable(),
        SUPPLIER_ID: ko.observable(),
        SUPPLIERDESC: ko.observable(),
        SUPPLIER_ADDRESS1: ko.observable(),
        SUPPLIER_CITY: ko.observable(),
        SUPPLIER_STATE: ko.observable(),
        SUPPLIER_ZIPCODE: ko.observable(),
        SUPPLIER_CONTACT_NO1: ko.observable(),
        SUPPLIER_CONTACT_NO2: ko.observable(),
        SUPPLIER_CONTACT: ko.observable(),
        SUPPLIER_EMAIL_ADDRESS: ko.observable(),
        OTHER_INFO: ko.observable(),
        SHIP_DATE: ko.observable(),
        EST_SHIP_DATE: ko.observable(),
        HANDLING_FEE: ko.observable(),
        TRACKING_NO: ko.observable(),
        EST_FREIGHT: ko.observable(),
        FREIGHT: ko.observable(),
        FREIGHT_PAIDBY: ko.observable(),
        FREIGHT_CHARGETO: ko.observable(),
        TAXABLE: ko.observable(),
        TAX_CODE: ko.observable(),
        TAX_AMOUNT: ko.observable(),
        FREIGHT_POLINE: ko.observable(),
        FREIGHT_SEPARATE_INV: ko.observable(),
        INSURANCE_VALUE: ko.observable(),
        PO_BILLCODE_ADDRESS: ko.observable(),
        POINVOICENO: ko.observable(),
        DISCOUNT: ko.observable(),
    };
    self.setObservableValue = function (_object1, _object2) {
        Object.keys(_object2).forEach(function (key) {
            if (ko.isObservable(_object1[key]))
                _object1[key](_object2[key])
            else
                _object1[key] = _object2[key];
        });
    };
    self.getObservableValue = function (data) {
        if (Array.isArray(data)) {
            var returnData = [];
            for (var i = 0; i < data.length; i++) {
                var _object = data[i];
                returnData.push({});
                Object.keys(_object).forEach(function (key) {
                    if (ko.isObservable(_object[key]))
                        returnData[i][key] = _object[key]();
                    else
                        returnData[i][key] = _object[key];
                });
            }
        }
        else if (data instanceof Object) {
            var returnData = {};
            var _object = data;
            Object.keys(_object).forEach(function (key) {
                if (ko.isObservable(_object[key]))
                    returnData[key] = _object[key]();
                else
                    returnData[key] = _object[key];
            });
        }
        else
            returnData = data;

        return returnData;
    };
    self.checkViewMode = function () {
        return self.customerRequest.TRANS_STATUS() != 'TRANSFER'
    };
    self.shipVias = ko.observableArray([]);
    self.foreignCurrencies = ko.observableArray([]);

    self.calculateTotalPrice = function (item, isPrice) {
        item.TOTAL_PRICE((item.PRICE() || 0) * ko.utils.unwrapObservable(item.QTY));

        item.TOTAL_PRICE(Math.round(item.TOTAL_PRICE() * 100) / 100);

        self.calculateTotal();
    };
    self.calculateTotal = function () {
        var totalAmountPrice = 0;
        totalAmountPrice = self.assignedSOH().reduce(function (acc, val) { return acc + val.TOTAL_PRICE(); }, 0);
        var inctoPO = self.customerRequest.FREIGHT_POLINE();
        var freightCharge = self.customerRequest.FREIGHT_CHARGETO();
        if (inctoPO == "YES" && freightCharge == "YES") {
            totalAmountPrice = totalAmountPrice + Number(self.customerRequest.FREIGHT());
        } else if (freightCharge == "NO") {
            self.customerRequest.FREIGHT_POLINE(null);
            self.customerRequest.FREIGHT(0);
        }

        totalAmountPrice = Math.round(totalAmountPrice * 100) / 100;
        self.customerRequest.TOTAL_AMOUNT(totalAmountPrice);
    };
    self.getDefaultCostCenter = function () {
        var dCC = JSON.parse(sessionStorage.getItem(ah.config.skey.defaultCostCenter));
        if (dCC.length > 0) {
            self.DefaultCostCenter.CODE = dCC[0].CODEID;
            self.DefaultCostCenter.DESCRIPTION = dCC[0].DESCRIPTION;
            self.DefaultCostCenter.STORE_ID = dCC[0].STORE_ID;
        }
    };

    self.DefaultCostCenter = {};
    self.assignedSOH = ko.observableArray([]);
    self.assignedSOHPrint = ko.observableArray([]);
    self.clientDetails = {
        CLIENTNAME: ko.observable(),
        CLIENT_LOGO: ko.observable(),
        COMPANY_LOGO: ko.observable(),
        HEADING: ko.observable(),
        HEADING2: ko.observable(),
        HEADING3: ko.observable(),
        HEADING_OTH: ko.observable(),
        HEADING_OTH2: ko.observable(),
        HEADING_OTH3: ko.observable(),
        CLIENTADDRESS: ko.observable(),
        CLIENTCONTACT: ko.observable()
    };
    self.printPurchaseRequest = function () {
        self.clientDetails.HEADING('Business Schema Trading Company');
        self.clientDetails.COMPANY_LOGO(window.location.origin + '/images/companylogo.png');

        self.assignedSOHPrint.removeAll();
        self.assignedSOHPrint(self.assignedSOH.slice(0));
        if (self.customerRequest.FREIGHT_POLINE() == 'YES') {
            self.assignedSOHPrint.push({
                DESCRIPTION: 'FREIGHT CHARGES',
                QTY: 1,
                PRICE: ko.observable(self.customerRequest.FREIGHT() || 0),
                TOTAL_PRICE: ko.observable(self.customerRequest.FREIGHT() || 0),
            });
        }

        setTimeout(function () {
            window.print();
        }, 1000)
    };
    self.getLookupDescription = function (value, array) {
        return value && array.length && array.find(function (x) { return x.LOV_LOOKUP_ID == value }).DESCRIPTION;
    };
    self.getPODate = function () {
        return moment(self.customerRequest.REQUEST_DATE(), 'DD/MM/YYYY').format('DD-MMM-YYYY');
    };
    self.getSubtotalPrice = function () {
        var itemTotalPrice = self.assignedSOHPrint().reduce(function (acc, val) { return acc + val.TOTAL_PRICE(); }, 0);
        return Math.round(itemTotalPrice * 100) / 100;
    };
    self.getDiscountPrice = function () {
        var itemDiscountPrice = self.getSubtotalPrice() * (Number(self.customerRequest.DISCOUNT()) / 100);
        return Math.round(itemDiscountPrice * 100) / 100;
    };
    self.getVatDescription = function () {
        var array = self.assignedSOHPrint.slice(0).filter(function (x) { return ko.utils.unwrapObservable(x.VAT_FLAG) });
        if (array.length) {
            var filtered = array.reduce(function (x, y) { return x.findIndex(function (e) { return ko.utils.unwrapObservable(e.VAT) == ko.utils.unwrapObservable(y.VAT); }) < 0 ? [].concat(x, [y]) : x; }, []);
            if (self.customerRequest.FREIGHT_POLINE() == 'YES') {
                filtered = filtered.filter(function (x) { return ko.utils.unwrapObservable(x.ID_PARTS) });
                return getVatText();
            }

            if (filtered.length > 1) {
                return getVatText();
            }
            else {
                return 'VAT ' + self.assignedSOHPrint()[0].VAT() + '%';
            }
        }

        function getVatText() {
            var vatText = 'VAT ';
            filtered.forEach(function (item) {
                var foundFilter = array.filter(function (x) { return ko.utils.unwrapObservable(item.VAT) == ko.utils.unwrapObservable(x.VAT) });
                foundFilter.forEach(function (item2) {
                    vatText += ko.utils.unwrapObservable(item2.ID_PARTS) + ', ';
                });
                vatText = vatText.substr(0, vatText.length - 2) + ' ';
                vatText += ko.utils.unwrapObservable(item.VAT) + '%, ';
            });
            vatText = vatText.substr(0, vatText.length - 2) + ' ';
            return vatText;
        }
    };
    self.getVatAmount = function () {
        var vatAmount = 0;
        self.assignedSOHPrint().forEach(function (item) {
            if (ko.utils.unwrapObservable(item.VAT_FLAG)) {
                vatAmount = vatAmount + (ko.utils.unwrapObservable(item.TOTAL_PRICE) * (ko.utils.unwrapObservable(item.VAT) / 100));
            }
        });
        return Math.round(vatAmount * 100) / 100;
    };
    self.getTotalPrice = function () {
        var itemTotalPrice = self.assignedSOHPrint().reduce(function (acc, val) { return acc + val.TOTAL_PRICE(); }, 0);
        itemTotalPrice = itemTotalPrice - (Number(self.getDiscountPrice()) + Number(self.getVatAmount()));
        return Math.round(itemTotalPrice * 100) / 100;
    };
    self.setObservableValueEmpty = function (object) {
        Object.keys(object).forEach(function (key) {
            if (ko.isObservable(object[key]))
                object[key]('')
            else
                object[key] = '';
        });
    }

    self.webApiCallbackStatus = function (jsonData) {
        //alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            //window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.PRODUCT_STOCKONHAND_REQSTORE || jsonData.STORES || jsonData.PRODUCT_STOCKONHAND || jsonData.AM_STOCKTRANSACTION || jsonData.STAFF || jsonData.SUCCESS) {

            if (ah.CurrentMode == ah.config.mode.save) {
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();

                sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_STOCKTRANSACTION));
                sessionStorage.setItem(ah.config.skey.stockStoreList, JSON.stringify(jsonData.PRODUCT_STOCKONHAND));
                sessionStorage.setItem(ah.config.skey.stockReqStoreList, JSON.stringify(jsonData.PRODUCT_STOCKONHAND_REQSTORE));

                if (jsonData.SUCCESS) {
                    $(ah.config.cls.liApiStatus).hide();
                    self.showDetails(false);
                }
                else {
                    $(ah.config.cls.liApiStatus).hide();
                    self.showDetails(true);
                }

            }
            else if (ah.CurrentMode == ah.config.mode.deleted) {
                $(ah.config.id.successMessage).html(systemText.deletedSuccessMessage);
                ah.showSavingStatusSuccess();
                ah.toggleDisplayMode();
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                self.setObservableValue(self.customerRequest, jsonData.AM_STOCKTRANSACTION_MASTER);
                self.customerRequest.REQUEST_DATE(moment(self.customerRequest.REQUEST_DATE()).format('DD/MM/YYYY HH:mm'));
                var list = jsonData.AM_STOCKTRANSACTION;
                for (var i = 0; i < list.length; i++) {
                    list[i].PRICE = ko.observable(list[i].PRICE);
                    list[i].TOTAL_PRICE = ko.observable(0);
                    list[i].VAT_FLAG = ko.observable(list[i].VAT_FLAG || false);
                    list[i].VAT = ko.observable(list[i].VAT);
                    self.calculateTotalPrice(list[i]);
                }
                self.assignedSOH(list);

                sessionStorage.setItem(ah.config.skey.stockStoreList, JSON.stringify(jsonData.PRODUCT_STOCKONHAND));
                sessionStorage.setItem(ah.config.skey.stockReqStoreList, JSON.stringify(jsonData.PRODUCT_STOCKONHAND_REQSTORE));
                sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_STOCKTRANSACTION));
                self.showDetails(true);
                var act = ah.getParameterValueByName(ah.config.fld.act);
                if (act === "transfer" && jsonData.AM_STOCKTRANSACTION[0].TRANS_STATUS == "POSTED") {
                    $("td input").each(function () { $(this).prop("disabled", false) });
                }
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
                if (jsonData.PRODUCT_STOCKONHAND) {
                    sessionStorage.setItem(ah.config.skey.stockList, JSON.stringify(jsonData.PRODUCT_STOCKONHAND));
                    $(ah.config.id.loadNumber).text("1");
                    $(ah.config.id.pageCount).val("1");
                    $(ah.config.id.pageNum).val(0);
                    self.searchItemsResult("", 0);
                    $(ah.config.cls.liApiStatus).hide();
                    $(ah.config.cls.liSave).show();

                } else {
                    self.searchResult(jsonData);
                }


            }
            else if (ah.CurrentMode == ah.config.mode.idle) {

                var store = jsonData.STORES;

                var storefromLov = jsonData.STORES;

                storefromLov = storefromLov.filter(function (item) { return item.ALLOWREQUEST_FLAG === "Y" });
                $.each(storefromLov, function (item) {
                    $(ah.config.id.storeFrom)
                        .append($("<option></option>")
                            .attr("value", store[item].STORE_ID)
                            .text(store[item].DESCRIPTION));
                });
                $.each(store, function (item) {
                    $(ah.config.id.storeTo)
                        .append($("<option></option>")
                            .attr("value", store[item].STORE_ID)
                            .text(store[item].DESCRIPTION));
                });
                $.each(store, function (item) {
                    $("#filterFromStore")
                        .append($("<option></option>")
                            .attr("value", store[item].STORE_ID)
                            .text(store[item].DESCRIPTION));
                });
                $.each(store, function (item) {
                    $("#filterToStore")
                        .append($("<option></option>")
                            .attr("value", store[item].STORE_ID)
                            .text(store[item].DESCRIPTION));
                });
            }

        }
        else {

            alert(systemText.errorTwo);
            //window.location.href = ah.config.url.homeIndex;
        }
        $("input[type='number']").keypress(function (e) {
            if ((e.shiftKey || (e.keyCode < 46 || e.keyCode > 57)) && (e.keyCode < 94 || e.keyCode > 105) || e.keyCode == 8) {
                e.preventDefault();
            }
        });

    };

    self.initialize();
};