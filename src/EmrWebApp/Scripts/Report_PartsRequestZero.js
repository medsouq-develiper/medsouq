﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            divProgress: '.divProgressScreen',
            toolBoxRight: '.toolbox-right',
            detailItem: '.detailItem',
            liPrint: '.liPrint'
        },
        tag: {

            textArea: 'textarea',
            select: 'select',
            tbody: 'tbody',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7,
            advanceSearch: 8,
            print: 9
        },
        tagId: {

        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            ReqReportHeader: '#ReqReportHeader',
            RequestSearchResultCMTable: '#RequestSearchResultCMTable',

            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            printSummary: '#printSummary',
            printSearchResultList: '#printSearchResultList',

            RequestSearchResultList: '#RequestSearchResultList',

            loadingBackground: '#loadingBackground',
            constCenterStrList: '#constCenterStrList',
            supplierStrList: '#supplierStrList',
            MANUFACTURERStrList: '#MANUFACTURERStrList',
            ProblemTypeStrList: '#ProblemTypeStrList',
            ReportSubTitle: '#ReportSubTitle'

        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            datainfoId: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS'


        },
        url: {
            modalFilter: '#openModalFilter',
            modalClose: '#',
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex'
        },
        api: {
            //identify the api controller you want to get you data
            searchLov: '/AIMS/SearchAimsLOV',
            searchAll: '/Reports/RequestedPartsZero',
            readClientInfo: '/Setup/ReadClientInfo'
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };
    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liPrint + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };
    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;
        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();
        $(thisApp.config.cls.contentSearch).show();

    };
    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };
    thisApp.displaySearchResult = function () {
        thisApp.CurrentMode = thisApp.config.mode.searchResult;
        $(thisApp.config.cls.contentSearch).show();
    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var viewTemplateViewModel = function (systemText) {
    var self = this;
    var ah = new appHelper(systemText);
    self.srDataArray = ko.observableArray([]);

    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.initializeLayout();
        var today = moment(new Date()).format("DD/MM/YYYY");

        $('#requestFromDate').val('');
        $('#requestStartDate').val(today);

        self.showLoadingBackground();

        $.when(self.getPageData()).done(function () {
            $(ah.config.id.loadingBackground).removeClass('initializing').addClass('done');
            $('.dark-bg, ' + ah.config.id.loadingBackground).css({
                'top': '90px',
                'z-index': '0'
            });
        });
    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api;
    };

    self.export = function (srtoexportdata, cycle) {
        $("#dvjson").excelexportjs({
            containerid: "dvjson"
            , datatype: 'json'
            , dataset: srtoexportdata
            , columns: getColumns(srtoexportdata)
            , filename: "PartsRequested_" + cycle
        });
    };

    self.exportToExcel = function () {
        //alert(JSON.stringify(self.srDataArray));
        var srarrayOrig = self.srDataArray;
        var srarraylen = srarrayOrig.length;
        var lrow = 6500;
        var cycle = srarraylen / lrow;
        var i;
        var frow = 1;
        if (cycle > 1) {
            for (i = 0; i < cycle; i++) {
                var sraraydata = srarrayOrig.slice(frow, lrow);
                self.export(sraraydata, i);
            }
        } else {
            self.export(srarrayOrig, 0);
        }
    };

    self.exportTableToExel = function () {
        $("#RequestSearchResultCMTable").excelexportjs({
            containerid: "RequestSearchResultCMTable"
            , datatype: 'table'
            , filename: "PartsRequested"
        });
    };

    self.clientDetails = {
        CLIENTNAME: ko.observable(),
        CLIENT_LOGO: ko.observable(),
        COMPANY_LOGO: ko.observable(),
        HEADING: ko.observable(),
        HEADING2: ko.observable(),
        HEADING3: ko.observable(),
        HEADING_OTH: ko.observable(),
        HEADING_OTH2: ko.observable(),
        HEADING_OTH3: ko.observable(),
        CLIENTADDRESS: ko.observable(),
        CLIENTCONTACT: ko.observable()
    };
    self.searchFilter = {
        fromDate: ko.observable(''),
        toDate: ko.observable(''),
        AssetFilterSuupiler: ko.observable(''),
        AssetFilterManufacturer: ko.observable(''),
        FilterStatus: ko.observable(''),
        Filterpmtype: ko.observable(''),
        //status: ko.observable(''),
        CostCenterStr: ko.observable('')
        //ProblemType: ko.observable('')
    };

    self.getPageData = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
        var postData;
        postData = { LOV: "'AIMS_COSTCENTER'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        return $.post(self.getApi() + ah.config.api.searchLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.showLoadingBackground = function () {
        var loadingBackground = $(ah.config.id.loadingBackground);
        loadingBackground.addClass('initializing').removeClass('done');
        var offset = loadingBackground.offset();
        var height = loadingBackground.height();
        var centerY = (offset.top + height / 2) - 120;
        $('.sk-circle').css({
            'margin-top': centerY + 'px',
            'display': 'block'
        });
    };
    self.searchFilterNow = function () {
        window.location.href = ah.config.url.modalClose;
        self.searchNow();
    };

    self.showModalFilter = function () {
        window.location.href = ah.config.url.modalFilter;
    };
    self.clearFilter = function () {
        $('#requestFromDate').val('');
        $('#requestStartDate').val('');
        self.searchFilter.fromDate = "";
        self.searchFilter.toDate = "";
        self.searchFilter.CostCenterStr = "";
        //self.searchFilter.AssetFilterManufacturer = "";
        //self.searchFilter.AssetFilterSuupiler = "";
        //self.searchFilter.status = "";
        //self.searchFilter.ProblemType = "";
        //self.searchFilter.FilterPMType = "";
        //$("#costcenterList").val(null);
        //$("#MANUFACTURERStr").val(null);
        //$("#supplierStr").val(null);
        //$("#filterStatus").val(null);
        //$("#ProblemTypeList").val(null);
        //$("#filterPMType").val(null);
    };
    self.printSummary = function () {
        ah.CurrentMode = ah.config.mode.print;
        document.getElementById("printSummary").classList.add('printSummary');

        if (self.clientDetails.CLIENTNAME() != null) {
            self.print();

        } else {
            var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var postData;

            var staffInfo = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
            postData = { CLIENTID: staffInfo.CLIENT_CODE, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.readClientInfo, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
    };
    self.print = function () {
        //var ReportCrit = $(ah.config.id.ReportCritria).val();
        //var fromDate = $('#requestFromDate').val();
        //var toDate = $('#requestStartDate').val();
        //var textLable = " ";
        //if (fromDate !== null && fromDate !== "") {
        //    textLable += "From: " + fromDate + " To: " + toDate;
        //}
        //if (self.searchFilter.AssetFilterSuupiler() !== "" && self.searchFilter.AssetFilterSuupiler() !== null) {
        //    textLable += " for Supplier: " + self.searchFilter.AssetFilterSuupiler();
        //}
        //if (self.searchFilter.AssetFilterManufacturer() !== "" && self.searchFilter.AssetFilterManufacturer() !== null) {
        //    textLable += " for MANUFACTURER: " + self.searchFilter.AssetFilterManufacturer();
        //}
        //if (self.searchFilter.CostCenterStr() !== "" && self.searchFilter.CostCenterStr() !== null) {
        //    textLable += " for COST CENTERNAME: " + self.searchFilter.CostCenterStr();
        //}
        //if (self.searchFilter.pmtype !== "" && self.searchFilter.pmtype !== null) {
        //    textLable += "Warranty Type By: " + self.searchFilter.pmtype;
        //}
        //if (self.searchFilter.ProblemType !== "" && self.searchFilter.ProblemType !== null) {
        //    textLable += "Type Of Problem: " + self.searchFilter.ProblemType;
        //}

        //if (self.searchFilter.status !== "" && self.searchFilter.status !== null) {
        //    var WoStatus = self.searchFilter.status;
        //    var StatusWo = "Open";
        //    if (WoStatus === "CL") { StatusWo = "Close" }
        //    if (WoStatus === "HA") { StatusWo = "Hold" }
        //    if (WoStatus === "NE") { StatusWo = "New" }
        //    if (WoStatus === "FC") { StatusWo = "For Close" }
        //    if (WoStatus === "PSOP") { StatusWo = "New/Open" }

        //    textLable += " And Status: " + StatusWo;
        //}

        //var TPrice = 0;
        //for (var i = 0; i <= self.sraraydata; i++)
        //{
        //    TPrice = TPrice + self.sraraydata[i];
        //}
        //textLable = "Requested Spare Parts Report             Total Price " + TPrice ;
        //$(ah.config.id.ReportSubTitle).text(textLable);
        //$(ah.config.id.printSearchResultList).html($(ah.config.id.RequestSearchResultCMTable).html());

        setTimeout(function () {
            window.print();

        }, 2000);
    };
    self.dateClick = function () {
        $("#dateClickAction").val("ACTIVE");
    };
    self.searchResultWorkOrder = function () {
        var table;
        //alert(JSON.stringify(self.srDataArray));
        if ($.fn.dataTable.isDataTable(ah.config.id.RequestSearchResultCMTable)) {
            $(ah.config.id.RequestSearchResultCMTable).DataTable().clear().destroy();
        }
        $(ah.config.cls.liPrint).hide();
        if (self.srDataArray.length > 0) {
            $(ah.config.cls.liPrint).show();
        }
        table = $(ah.config.id.RequestSearchResultCMTable).DataTable({
            data: self.srDataArray,
            columns: [
                { data: 'id_parts' },
                { data: 'parts_description' },
                { data: 'req_no' },
                { data: 'req_date' },
                { data: 'asset_no' },
                { data: 'qty' },
                { data: 'price' },
                { data: 'TotalPrice' }
            ],
            "searching": false,
            //"order":true,
            "iDisplayLength": self.srDataArray.length,
            "aLengthMenu": [15, 200, 6500, self.srDataArray.length]

        });
        //var TPrice = 0;
        //for (var i = 0; i <= self.sraraydata.length-1; i++) {
        //    TPrice = TPrice + self.sraraydata[i];
        //}
        //textLable = "Requested Spare Parts Report  Total Price" + TPrice;
        //$(ah.config.id.ReqReportHeader).text(textLable);
    };

    self.searchNow = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();

        var fromDate = $('#requestFromDate').val();

        fromDate = fromDate.split('/');
        if (fromDate.length == 3) {
            //self.searchFilter.fromDate(fromDate[2] + "/" + fromDate[1] + "/" + fromDate[0]);
            self.searchFilter.fromDate(fromDate[0] + "/" + fromDate[1] + "/" + fromDate[2]);
        }
        var toDate = $('#requestStartDate').val().split('/');
        if (toDate.length == 3) {
            //self.searchFilter.toDate(toDate[2] + "/" + toDate[1] + "/" + toDate[0] + "  23:00:00.000");
            self.searchFilter.toDate(toDate[0] + "/" + toDate[1] + "/" + toDate[2] + "  23:00:00.000");
        }

        self.showLoadingBackground();
        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), SEARCH_FILTER: self.searchFilter, RETACTION: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.when($.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus)).done(function () {
            $(ah.config.id.loadingBackground).removeClass('initializing').addClass('done');
        });
    };
    self.webApiCallbackStatus = function (jsonData) {
        //ah.CurrentMode = ah.config.mode.search
        if (jsonData.ERROR) {
            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
        }
        else if (jsonData.AM_CLIENT_INFO || jsonData.WOPARTSREQUEST_REPTLIST || jsonData.STAFF || jsonData.SUCCESS || jsonData.LOV_LOOKUPS) {
            if (ah.CurrentMode == ah.config.mode.search) {
                self.srDataArray = jsonData.WOPARTSREQUEST_REPTLIST;
                self.searchResultWorkOrder(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {
                lovCostCenterArray = jsonData.LOV_LOOKUPS;

                var LOV_CostCenter = jsonData.LOV_LOOKUPS;
                LOV_CostCenter = LOV_CostCenter.filter(function (item) { return item.CATEGORY === 'AIMS_COSTCENTER' });
                $.each(LOV_CostCenter, function (item) {

                    $(ah.config.id.costcenterStrList)
                        .append($("<option>")
                            .attr("value", LOV_CostCenter[item].DESCRIPTION.trim())
                            .attr("id", LOV_CostCenter[item].LOV_LOOKUP_ID));
                });

                lovSupplierArray = jsonData.AM_SUPPLIER;
                $.each(lovSupplierArray, function (item) {

                    $(ah.config.id.supplierStrList)
                        .append($("<option>")
                            .attr("value", lovSupplierArray[item].DESCRIPTION.trim())
                            .attr("id", lovSupplierArray[item].SUPPLIER_ID));
                });
            }
            else if (ah.CurrentMode == ah.config.mode.print) {

                var clientInfo = jsonData.AM_CLIENT_INFO;

                self.clientDetails.CLIENTNAME(clientInfo.DESCRIPTION);
                self.clientDetails.CLIENT_LOGO(clientInfo.CLIENT_LOGO);
                self.clientDetails.COMPANY_LOGO(clientInfo.COMPANY_LOGO);
                self.clientDetails.HEADING(clientInfo.HEADING);
                self.clientDetails.HEADING2(clientInfo.HEADING2);
                self.clientDetails.HEADING3(clientInfo.HEADING3);
                self.clientDetails.HEADING_OTH(clientInfo.HEADING_OTH);
                self.clientDetails.HEADING_OTH2(clientInfo.HEADING_OTH2);
                self.clientDetails.HEADING_OTH3(clientInfo.HEADING_OTH3);
                self.clientDetails.CLIENTADDRESS(clientInfo.ADDRESS + ', ' + clientInfo.CITY + ', ' + clientInfo.ZIPCODE + ', ' + clientInfo.STATE);
                self.clientDetails.CLIENTCONTACT('Tel.No: ' + clientInfo.CONTACT_NO1 + ' Fax No:' + clientInfo.CONTACT_NO2);

                self.print();
            }
        }
        else {
            alert(systemText.errorTwo);
        }
    };
    self.initialize();
};