﻿var appHelper = function (systemText) {

    var thisApp = this;
	
    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divModuleAccessAttributeID: '.divModuleAccessAttributeID',
            statusText: '.status-text',
            modulefunctionDetailItem: '.modulefunctionDetailItem',
            moduleaccessattributeDetailItem: '.moduleaccessattributeDetailItem'
        },
        fld: {
            staffGroupID: 'STAFF_GROUP_ID',
            staffGroupDescription: 'DESCRIPTION'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveModuleAccessAttribute: '#saveModuleAccessAttribute',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmModuleAccessAttribute: '#frmModuleAccessAttribute',
            moduleaccessattributeID: '#MODULE_FUNCTION_ID',
            staffGroupId: '#STAFF_GROUP_ID',
            staffGroupDescription: '#STAFF_GROUP_DESCRIPTION',
            moduleDescription: '#MODULE_DESCRIPTION',
            modulefunctionselected:  '#selected',

            successMessage: '#successMessage'
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            dataModuleID: 'data-moduleID',
            dataModuleFunctionID: 'data-modulefunctionID',
            dataModuleAccessAttributeID: 'data-moduleaccessattributeID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            moduleaccessattributeDetails: 'MODULE_ACCESS_ATTRIBUTE',
            appendmoduleaccessattributeDetails: 'MODULE_ACCESS_ATTRIBUTE_APPEND',
            modulefunctionDetails: 'MODULE_FUNCTIONS',
            searchResult: 'searchResult'
        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            staffGroup: '/setup/staffGroups'
        },
        api: {
            readModuleAccessAttribute: '/Setup/ReadModuleAccessAttribute',
            searchAll: '/Search/SearchModuleAccessAttribute',
            searchModuleFunctionAll: '/Search/SearchModuleFunctions',
            updateModuleAccessAttribute: '/Setup/UpdateModuleAccessAttribute',
			createModuleAccessAttribute: '/Setup/CreateModuleAccessAttribute',
			deleteModuleAccessAttribute: '/Setup/DeleteModuleAccessAttribute'

        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal moduleaccessattributeDetailItem' data-moduleaccessattributeID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}</a>",
            searchRowSubHeaderTemplate: "&nbsp;&nbsp;<span class='fs-medium-normal fc-slate-blue'>{0}</span><br /> ",
            searchRowTemplate: '&nbsp;&nbsp;<span class="search-row-label">{0}</span><br /> ',

            searchModuleFunctionRowHeaderTemplate: "<span class='fs-medium-normal fc-slate-blue'>{1}</span>",
            searchModuleFunctionRowTemplate: "<a href='#' class='search-row-data '>&nbsp;&nbsp;<input type='checkbox' name='selected' id='selected' value='Y' class='search-row-data modulefunctionDetailItem' data-modulefunctionID='{0}' data-moduleID='{1}' ></i>&nbsp;&nbsp;{2} &nbsp;&nbsp;&nbsp;&nbsp; {3}</a>",

            searchModuleFunctionRowHeaderTemplateChecked: "<span class='fs-medium-normal fc-slate-blue'>{1}</span>",
            searchModuleFunctionRowTemplateChecked: "<a href='#' class='search-row-data '>&nbsp;&nbsp;<input type='checkbox' name='selected' id='selected' value='Y' class='search-row-data modulefunctionDetailItem' data-modulefunctionID='{0}' data-moduleID='{1}' checked></i>&nbsp;&nbsp;{2} &nbsp;&nbsp;&nbsp;&nbsp; {3}</a>"
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divModuleAccessAttributeID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };
   

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatuxText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.liApiStatus
        ).hide();

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus).hide();

    };

    thisApp.displaySearchResultRead = function () {

  
        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();
        $(thisApp.config.cls.liModify + ',' +thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus).hide();



    };

    thisApp.toggleEditMode = function () {
        
        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.statusText + ',' + thisApp.config.cls.liApiStatus).hide();

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.contentField
        ).hide();
    };

   

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };



    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name),
                type = $el.attr('type');

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var moduleaccessattributeViewModel = function (systemText) {

    var self = this;

    var ah = new appHelper(systemText);

    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));

        var postData;
        var staffGroupID;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

		//sessionStorage.removeItem("searchResult");
		//sessionStorage.removeItem("MODULE_ACCESS_ATTRIBUTE");
		//sessionStorage.removeItem("MODULE_ACCESS_ATTRIBUTE_APPEND");
		sessionStorage.removeItem("MODULE_FUNCTIONS");
		sessionStorage.setItem(ah.config.skey.searchResult, JSON.stringify(''));
        ah.initializeLayout();

        staffGroupID = ah.getParameterValueByName(ah.config.fld.staffGroupID);
        if (staffGroupID) {
            self.searchNow(staffGroupID);
        }
    };

    self.createNewModuleAccessAttribute = function () {

         ah.toggleEditMode();
       // ah.toggleReadMode();
        sessionStorage.setItem(ah.config.skey.appendmoduleaccessattributeDetails, JSON.stringify(''));
        sessionStorage.setItem(ah.config.skey.moduleaccessattributeDetails, JSON.stringify(''));

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi()+ ah.config.api.searchModuleFunctionAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.modifyModuleAccessAttributeDetails = function () {
        
        ah.toggleEditMode();
		
        var modulefunctionDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.modulefunctionDetails));
        
        if (modulefunctionDetails.length > 0) {
            var jsonData = JSON.parse('{"' + ah.config.skey.modulefunctionDetails + '":' + JSON.stringify(modulefunctionDetails) + '}');
            self.searchModuleFunctionResult(jsonData, 'READ');

        } else {
           
            var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var postData;

            postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchModuleFunctionAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }


    };
    self.returnHome = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		window.location.href = staffDetails.HOME_DEFAULT;
    };

    self.staffGroupInfo = function () {
        var staffGroupId = ah.getParameterValueByName(ah.config.fld.staffGroupID);

        window.location.href = ah.config.url.staffGroup + "?STAFFGROUP=" + staffGroupId;
    }
    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.saveAllChanges = function () {

        $(ah.config.id.saveModuleAccessAttribute).trigger('click');

    };

    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            ah.toggleReadMode();
            var moduleaccessattributeDetails =  JSON.parse(sessionStorage.getItem(ah.config.skey.appendmoduleaccessattributeDetails)) ;
            var jsonData = JSON.parse('{"' + ah.config.skey.moduleaccessattributeDetails + '":' + JSON.stringify(moduleaccessattributeDetails) + '}');
            //alert(JSON.stringify(jsonData));
            self.searchResult(jsonData, 'READ');
        }

    };

    self.showModuleAccessAttributeDetails = function (isNotUpdate) {

        if (isNotUpdate) {

            var moduleaccessattributeDetails = sessionStorage.getItem(ah.config.skey.moduleaccessattributeDetails);

            var jModuleAccessAttribute = JSON.parse(moduleaccessattributeDetails);

            ah.ResetControls();
            ah.LoadJSON(jModuleAccessAttribute);
        }

        ah.toggleReadMode();

    };

    self.searchResult = function (jsonData, action) {

        sessionStorage.setItem(ah.config.skey.searchResult, JSON.stringify(jsonData.MODULE_ACCESS_ATTRIBUTE));
		var sr = jsonData.MODULE_ACCESS_ATTRIBUTE;
		//alert(JSON.stringify(sr));
        

        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));
        
        //alert('srlen'+ sr.length);
        if (sr.length > 0) {
            var htmlstr = '';
            var lastmodule = 'NONE';
           // alert(JSON.stringify(jsonData.MODULE_ACCESS_ATTRIBUTE));
            //filer the staffgroup            
            var module = jsonData.MODULE_ACCESS_ATTRIBUTE.filter(function (item) { return item.ATTRIBUTE === 'MODULE' });
           
            var staffgroupDescription = $(ah.config.id.staffGroupDescription).val().toString();
         //   alert(JSON.stringify(module));
            for (var o in module) {
                htmlstr += '<li>';
                
                if (lastmodule != module[o].GROUP_ID) {
                    if (action === 'READ') {
                        if (o == 0) {
                           
                            if (staffgroupDescription.length == 0 || staffgroupDescription == null) {
                                
                                document.getElementById("STAFF_GROUP_DESCRIPTION").value = module[o].STAFF_GROUP_DESCRIPTION;
                                staffgroupDescription = module[o].STAFF_GROUP_DESCRIPTION
                            }
                        }

                    } else {
                        staffgroupDescription = module[o].STAFF_GROUP_DESCRIPTION;
                    }

                    htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, module[o].GROUP_ID, staffgroupDescription);
                    lastmodule = module[o].GROUP_ID;
                    
                }
                
                htmlstr += ah.formatString(ah.config.html.searchRowSubHeaderTemplate, module[o].DESCRIPTION);

                var functionAttribute = jsonData.MODULE_ACCESS_ATTRIBUTE.filter(function (item) { return item.ATTRIBUTE === 'FUNCTION' && item.MODULE_ID === module[o].MODULE_ID && item.GROUP_ID === module[o].GROUP_ID });
                for (var i in functionAttribute) {

                     htmlstr += ah.formatString(ah.config.html.searchRowTemplate, functionAttribute[i].DESCRIPTION);
                }
                
                htmlstr += '</li>';

            }

             $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

       
       
        if (action == 'READ') {
            $(ah.config.id.searchResultSummary).html(systemText.displayModeHeader + staffgroupDescription);
            ah.displaySearchResultRead();
        } else {
            $(ah.config.cls.moduleaccessattributeDetailItem).bind('click', self.readModuleAccessAttributeByModuleAccessAttributeID);
            ah.displaySearchResult();
        }
        

    };

    self.searchModuleFunctionResult = function (jsonData) {
       
        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        var modulefunctionDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.modulefunctionDetails));
        if (modulefunctionDetails === '') {
            sessionStorage.setItem(ah.config.skey.modulefunctionDetails, JSON.stringify(jsonData.MODULE_FUNCTIONS));
        }
        var sr = jsonData.MODULE_FUNCTIONS;

        
		var searchResultDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.searchResult));

		if (searchResultDetails === '' || searchResultDetails === null) {
			
			
		}
        
       // alert(JSON.stringify(searchResultDetails.length));
        
        var maa = 0;

        if (sr.length > 0) {
            var htmlstr = '';
            var lastmodule = '';
            var staffgroupsId = '';
            
            //var moduleaccessattributeDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.moduleaccessattributeDetails));
			sessionStorage.setItem(ah.config.skey.appendmoduleaccessattributeDetails, JSON.stringify(''));
            var moduleaccessattributeDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.appendmoduleaccessattributeDetails));

            var staffgroupDescription = $(ah.config.id.staffGroupDescription).val().toString();

            for (var o in sr) {
                htmlstr += '<li>';
                if (lastmodule != sr[o].MODULE_ID) {
					if (searchResultDetails !== null && searchResultDetails.length > 0) {
                        var maa = searchResultDetails.filter(function (item) { return item.MODULE_ID === sr[o].MODULE_ID });
                    }
                    if (maa.length > 0) {
						htmlstr += ah.formatString(ah.config.html.searchModuleFunctionRowHeaderTemplateChecked, sr[o].MODULE_FUNCTION_ID, sr[o].MODULE_DESCRIPTION, '');
						
                        for (var i in maa) {
                           
                            
                            if (moduleaccessattributeDetails.length > 0) {
                                moduleaccessattributeDetails.push({ "ATTRIBUTE": maa[i].ATTRIBUTE, "MODULE_ID": maa[i].MODULE_ID, "ATTRIBUTE_ID": maa[i].ATTRIBUTE_ID, "DESCRIPTION": maa[i].DESCRIPTION, "CREATE_BY": maa[i].CREATE_BY, "URL": maa[i].URL, "GROUP_ID": maa[i].GROUP_ID, "DESCRIPTION_OTH": maa[i].DESCRIPTION_OTH, "MODULE_FUNCTION_ID": maa[i].MODULE_FUNCTION_ID });
                            } else {
                                var newAttribute = JSON.stringify({ "ATTRIBUTE": maa[i].ATTRIBUTE, "MODULE_ID": maa[i].MODULE_ID, "ATTRIBUTE_ID": maa[i].ATTRIBUTE_ID, "DESCRIPTION": maa[i].DESCRIPTION, "CREATE_BY": maa[i].CREATE_BY, "URL": maa[i].URL, "GROUP_ID": maa[i].GROUP_ID, "DESCRIPTION_OTH": maa[i].DESCRIPTION_OTH, "MODULE_FUNCTION_ID": maa[i].MODULE_FUNCTION_ID });
                                var moduleaccessattributeDetails = JSON.parse('[' + newAttribute + ']');
                                // 
                            }
 
                            staffgroupsId = maa[i].STAFF_GROUP_DESCRIPTION;
                        }
 
                    } else {
                        htmlstr += ah.formatString(ah.config.html.searchModuleFunctionRowHeaderTemplate,sr[o].MODULE_FUNCTION_ID, sr[o].MODULE_DESCRIPTION, '');
                    }
                    lastmodule = sr[o].MODULE_ID;

                }
				if (searchResultDetails !== null && searchResultDetails.length > 0) {
                    var maa = searchResultDetails.filter(function (item) { return item.MODULE_FUNCTION_ID === sr[o].MODULE_FUNCTION_ID });
                }
                if (maa.length > 0) {

                     htmlstr += ah.formatString(ah.config.html.searchModuleFunctionRowTemplateChecked, sr[o].MODULE_FUNCTION_ID, sr[o].MODULE_ID, sr[o].DESCRIPTION, sr[o].FUNCTION_TYPE.toLowerCase());
                } else {
                    htmlstr += ah.formatString(ah.config.html.searchModuleFunctionRowTemplate, sr[o].MODULE_FUNCTION_ID, sr[o].MODULE_ID, sr[o].DESCRIPTION, sr[o].FUNCTION_TYPE.toLowerCase());
                }
 
                htmlstr += '</li>';

            }
            
            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        // if (staffgroupId == null || staffgroupId == '') {
		
        sessionStorage.setItem(ah.config.skey.moduleaccessattributeDetails, JSON.stringify(moduleaccessattributeDetails));
        sessionStorage.setItem(ah.config.skey.appendmoduleaccessattributeDetails, JSON.stringify(moduleaccessattributeDetails));
       
        $(ah.config.id.searchResultSummary).html(systemText.updateModeHeader + ' ' + staffgroupDescription);

        $(ah.config.cls.modulefunctionDetailItem).bind('click', self.onclickChanged);
       

    };

    self.onclickChanged = function () {
        var modulefunctionID = this.getAttribute(ah.config.attr.dataModuleFunctionID).toString();
        var moduleID = this.getAttribute(ah.config.attr.dataModuleID).toString();
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		
        var appendData = JSON.parse(sessionStorage.getItem(ah.config.skey.appendmoduleaccessattributeDetails));
        
		//alert(JSON.stringify(appendData));
        
        //check if the module is exist
        var rc = '';
        var md = '';
        
        if (appendData.length > 0) {
            md = appendData.filter(function (item) { return item.MODULE_ID === moduleID });
            rc = appendData.filter(function (item) { return item.MODULE_FUNCTION_ID === modulefunctionID });
        }
        
        if (rc.length > 0) {

			for (var o in appendData) {
				//alert(appendData[o].MODULE_ID);
				var moduleId = appendData[o].MODULE_ID;
                if (appendData[o].MODULE_FUNCTION_ID === modulefunctionID) {
                    
                    appendData.splice(o, 1);

                    //slice the module if no function assign...
					module = appendData.filter(function (item) { return item.MODULE_ID === moduleId }); 
                    if (module.length == 1) {
                        for (var i in module) {
                            appendData.splice(i, 1);
                        }

                    }

                    
                }
            }
        } else {
          
            var staffGroupId = '';
            var staffGroupID = ah.getParameterValueByName(ah.config.fld.staffGroupID);
            var staffGroupDescription = ah.getParameterValueByName(ah.config.fld.staffGroupDescription);
            if (staffGroupID.length > 0) {
                staffGroupId = staffGroupID;
            } else {
                staffGroupId = appendData[1].GROUP_ID

            }

            var modulefunctionDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.modulefunctionDetails));
            var mf = modulefunctionDetails.filter(function (item) { return item.MODULE_FUNCTION_ID === modulefunctionID });
            
            if (mf.length > 0) {
                for (var o in mf) {

                    if (md.length == 0) {
                        
                        if (appendData.length > 0) {
                            appendData.push({ "ATTRIBUTE": 'MODULE', "MODULE_ID": moduleID, "ATTRIBUTE_ID": moduleID, "DESCRIPTION": mf[o].MODULE_DESCRIPTION, "CREATE_BY": staffLogonSessions.STAFF_ID, "URL": moduleID, "GROUP_ID": staffGroupId, "DESCRIPTION_OTH": mf[o].DESCRIPTION_OTH, "MODULE_FUNCTION_ID": moduleID});
                        } else {
                            var newAttribute = JSON.stringify({ "ATTRIBUTE": 'MODULE', "MODULE_ID": moduleID, "ATTRIBUTE_ID": moduleID, "DESCRIPTION": mf[o].MODULE_DESCRIPTION, "CREATE_BY": staffLogonSessions.STAFF_ID, "URL": moduleID, "GROUP_ID": staffGroupId, "DESCRIPTION_OTH": mf[o].DESCRIPTION_OTH, "MODULE_FUNCTION_ID": moduleID});
                            
                            var appendData = JSON.parse('['+newAttribute + ']');
                        }
                    }
                    appendData.push({ "ATTRIBUTE": 'FUNCTION', "MODULE_ID": mf[o].MODULE_ID, "ATTRIBUTE_ID": mf[o].FUNCTION_ID, "DESCRIPTION": mf[o].DESCRIPTION, "CREATE_BY": staffLogonSessions.STAFF_ID, "URL": mf[o].URL, "GROUP_ID": staffGroupId, "DESCRIPTION_OTH": mf[o].DESCRIPTION_OTH, "MODULE_FUNCTION_ID": mf[o].MODULE_FUNCTION_ID});
 
                }
            }
        }
        
        sessionStorage.setItem(ah.config.skey.appendmoduleaccessattributeDetails, JSON.stringify(appendData));
        ah.toggleEditMode();
    };


    self.readModuleAccessAttributeByModuleAccessAttributeID = function () {

       
        var moduleaccessattributeID = this.getAttribute(ah.config.attr.dataModuleAccessAttributeID).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        document.getElementById("STAFF_GROUP_ID").value = moduleaccessattributeID;
        ah.toggleReadMode();

        postData = { SEARCH: moduleaccessattributeID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);



    };

    self.searchNow = function (searchQuery) {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        var searchKey;
        var gettt;
        sessionStorage.setItem(ah.config.skey.moduleaccessattributeDetails, JSON.stringify(''));
        sessionStorage.setItem(ah.config.skey.appendmoduleaccessattributeDetails, JSON.stringify(''));
        
        if (searchQuery.length > 0 ) {
            ah.toggleReadMode();
            searchKey = searchQuery.toString();
            document.getElementById("STAFF_GROUP_ID").value = searchKey;
            var staffGroupDescription = ah.getParameterValueByName(ah.config.fld.staffGroupDescription);
            document.getElementById("STAFF_GROUP_DESCRIPTION").value = staffGroupDescription;
            
        } else {
            var staffGroupID = '';
            staffGroupID = ah.getParameterValueByName(ah.config.fld.staffGroupID);
           
            if (staffGroupID.length > 0) {
                ah.toggleReadMode();
                searchKey = staffGroupID;
            } else {
                ah.toggleSearchMode();
                searchKey = $(ah.config.id.txtGlobalSearch).val().toString()
            }
        }
        
        postData = { SEARCH: searchKey, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    

    };

    self.saveModuleAccessAttributeDetails = function () {
        ah.toggleSaveMode()
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        var moduleaccessattributeDetails;
        //alert(JSON.stringify(moduleaccessattributeDetails));

        
        var oldMFunctionAcess = JSON.parse(sessionStorage.getItem(ah.config.skey.moduleaccessattributeDetails));
        var newMFunctionAcess = JSON.parse(sessionStorage.getItem(ah.config.skey.appendmoduleaccessattributeDetails));
		//alert(JSON.stringify(newMFunctionAcess));
		if (newMFunctionAcess.length > 0) {
            postData = { MODULE_ACCESS_ATTRIBUTE: newMFunctionAcess, STAFF_LOGON_SESSIONS: staffLogonSessions };
            
            $.post(self.getApi() + ah.config.api.createModuleAccessAttribute, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        } else {
			var staffGroupID = ah.getParameterValueByName(ah.config.fld.staffGroupID);
			postData = { MODULE_ACCESS_ATTRIBUTE: newMFunctionAcess, GROUP_ID: staffGroupID, STAFF_LOGON_SESSIONS: staffLogonSessions };

			$.post(self.getApi() + ah.config.api.deleteModuleAccessAttribute, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
            
        }
		//alert(JSON.stringify(moduleaccessattributeDetails));
       // 
        
    };

    self.refreshModuleAccessAttributeDetails = function () {
        ah.toggleReadMode();
        var moduleaccessattributeDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.appendmoduleaccessattributeDetails));
		//alert(JSON.stringify(moduleaccessattributeDetails));
		//ah.config.skey.moduleaccessattributeDetails
		//alert('srxx' + moduleaccessattributeDetails.length);
		var Jlocalstorage = JSON.parse('{"MODULE_ACCESS_ATTRIBUTE":' + JSON.stringify(moduleaccessattributeDetails) + '}');
		
        self.searchResult(Jlocalstorage, 'READ');
    };

    self.webApiCallbackStatus = function (jsonData) {
        //alert(JSON.stringify(jsonData));
        
        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
          //  window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.STAFF_GROUP_ID || jsonData.MODULE_FUNCTIONS || jsonData.MODULE_ACCESS_ATTRIBUTE || jsonData.MODULE_ACCESS_ATTRIBUTE_APPEND  || jsonData.MODULES || jsonData.SUCCESS || jsonData.MODULE_ACCESS_ATTRIBUTE) {
           // alert(ah.CurrentMode);

            if (ah.CurrentMode == ah.config.mode.save) {

                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();

                self.refreshModuleAccessAttributeDetails();
 
            }
            else if (ah.CurrentMode == ah.config.mode.read) {

                if (sessionStorage.getItem(ah.config.skey.modulefunctionDetails) === null || sessionStorage.getItem(ah.config.skey.modulefunctionDetails) === '') {
                    sessionStorage.setItem(ah.config.skey.modulefunctionDetails, JSON.stringify(''));
                }

                var sr = jsonData.MODULE_ACCESS_ATTRIBUTE;
                var staffGroupID = ah.getParameterValueByName(ah.config.fld.staffGroupID);

                if (staffGroupID.length > 0 && sr.length == 0) {
                    self.createNewModuleAccessAttribute();
                } else {                  

					
                    sessionStorage.setItem(ah.config.skey.moduleaccessattributeDetails, JSON.stringify(''));
                    sessionStorage.setItem(ah.config.skey.appendmoduleaccessattributeDetails, JSON.stringify(''));

                    self.searchResult(jsonData, 'READ');
                }
            }
            else if (ah.CurrentMode == ah.config.mode.edit) {
               

                if (sessionStorage.getItem(ah.config.skey.modulefunctionDetails) === null || sessionStorage.getItem(ah.config.skey.modulefunctionDetails) === '') {
                    sessionStorage.setItem(ah.config.skey.modulefunctionDetails, JSON.stringify(''));
                }
               
                self.searchModuleFunctionResult(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
                
                if (sessionStorage.getItem(ah.config.skey.modulefunctionDetails) === null || sessionStorage.getItem(ah.config.skey.modulefunctionDetails) === '') {
                    sessionStorage.setItem(ah.config.skey.modulefunctionDetails, JSON.stringify(''));
				}
				//if (sessionStorage.getItem(ah.config.skey.searchResult) === null || sessionStorage.getItem(ah.config.skey.searchResult) === '') {
					
				//}
				alert(JSON.stringify(jsonData));
                self.searchResult(jsonData,'SEARCH');
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {
                $.each(jsonData.MODULES, function (item) {
                    $('#MODULE_ID')
                        .append($("<option></option>")
                        .attr("value", jsonData.MODULES[item].MODULE_ID)
                        .text(jsonData.MODULES[item].DESCRIPTION));
                });


            }

        }
        else {

            alert(systemText.errorTwo);
           // window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};