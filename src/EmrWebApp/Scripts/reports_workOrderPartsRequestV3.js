var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            statusText: '.status-text',
            detailItem: '.detailItem',
            liPrint: '.liPrint'
        },
        tag: {

            textArea: 'textarea',
            select: 'select',
            tbody: 'tbody',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7,
            advanceSearch: 8,
            print: 9
        },
        tagId: {
            tcCode: 'tcCode',
            tcInService: 'tcInService',
            tcDescription: 'tcDescription',
            tcActive: 'tcActive',
            tcMissing: 'tcMissing',
            tcInActive: 'tcInActive',
            tcOtService: 'tcOtService',
            tcRetired: 'tcRetired',
            tcTransfer: 'tcTransfer',
            tcUndifined: 'tcUndifined'
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            searchResultSummaryCount: "#searchResultSummaryCount",
            txtGlobalSearch: '#txtGlobalSearch',
            staffID: '#STAFF_ID',
            serviceID: '#SERVICE_ID',
            successMessage: '#successMessage',
            requestFromDate: '#requestFromDate',
            requestStartDate: '#requestStartDate',
            costcenterStr: '#costcenterStr',
            supplierStr: '#supplierStr',
            filterStatus: '#filterStatus',
            supplierStr: '#supplierStr',
            costcenterStrList: '#costcenterStrList',
            supplierStrList: '#supplierStrList',
            printSummary: '#printSummary',
            printSearchResultList: "#printSearchResultList",
            loadingBackground: '#loadingBackground'
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'AM_ASSETSUMMARY'
        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalClose: '#',
            modalFilter: '#openModalFilter'
        },
        api: {
            searchLov: '/AIMS/SearchAimsLOV',
            searchAll: '/Reports/SearchWOPartsReq',
            readClientInfo: '/Setup/ReadClientInfo'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liPrint + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave).hide();

    };

    thisApp.ConvertFormToJSON = function (form) {
        $(form).find("input:disabled").removeAttr("disabled");
        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name),
                type = $el.attr('type');

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var workOrderPartsRequestViewModel = function (systemText) {

    var self = this;

    var ah = new appHelper(systemText);

    var lovCostCenterArray = ko.observableArray([]);
    var lovSupplierArray = ko.observableArray([]);

    self.assetCategory = {
        count: ko.observable()
    };
    self.clientDetails = {
        CLIENTNAME: ko.observable(),
        CLIENT_LOGO: ko.observable(),
        COMPANY_LOGO: ko.observable(),
        HEADING: ko.observable(),
        HEADING2: ko.observable(),
        HEADING3: ko.observable(),
        HEADING_OTH: ko.observable(),
        HEADING_OTH2: ko.observable(),
        HEADING_OTH3: ko.observable(),
        CLIENTADDRESS: ko.observable(),
        CLIENTCONTACT: ko.observable()
    };
    self.searchFilter = {
        fromDate: ko.observable(''),
        toDate: ko.observable(''),
        CostCenterStr: ko.observable(''),
        SupplierStr: ko.observable(''),
        status: ko.observable('I'),
        woStatus: ko.observable('')
    };
    self.showLoadingBackground = function () {
        var loadingBackground = $(ah.config.id.loadingBackground);
        loadingBackground.addClass('initializing').removeClass('done');
        var offset = loadingBackground.offset();
        var height = loadingBackground.height();
        var centerY = (offset.top + height / 2) - 120;
        $('.sk-circle').css({
            'margin-top': centerY + 'px',
            'display': 'block'
        });
    };
    self.getPageData = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
        var postData;
        postData = { LOV: "'AIMS_COSTCENTER'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        return $.post(self.getApi() + ah.config.api.searchLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());
        
        ah.initializeLayout();
        var today = moment(new Date()).format("DD/MM/YYYY");

        $('#requestFromDate').val(today);
        $('#requestStartDate').val(today);

        self.showLoadingBackground();

        $.when(self.getPageData()).done(function () {
            $(ah.config.id.loadingBackground).removeClass('initializing').addClass('done');
            $('.dark-bg, ' + ah.config.id.loadingBackground).css({
                'top': '90px',
                'z-index': '0'
            });
        });
    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };

    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.createNew = function () {

        ah.toggleAddMode();

    };
    self.searchFilterNow = function () {
        window.location.href = ah.config.url.modalClose;
        self.searchNow();
    }
    self.showModalFilter = function () {
        ah.CurrentMode = ah.config.mode.advanceSearch;
        window.location.href = ah.config.url.modalFilter;
    };
    self.clearFilter = function () {

        $(ah.config.id.requestFromDate).val('');
        $(ah.config.id.requestStartDate).val('');
        $(ah.config.id.costcenterStr).val('');
        $(ah.config.id.supplierStr).val('');
        $(ah.config.id.status).val('');
        self.searchFilter.fromDate = '';
        self.searchFilter.toDate = '';
        self.searchFilter.CostCenterStr = '';
        self.searchFilter.SupplierStr = '';
        self.searchFilter.status = '';
        self.searchFilter.woStatus = '';
    }
    self.modifySetup = function () {

        ah.toggleEditMode();

    };

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.printSummary = function () {
        ah.CurrentMode = ah.config.mode.print;
        document.getElementById("printSummary").classList.add('printSummary');
        var jsonObj = ko.observableArray();


        if (self.clientDetails.CLIENTNAME() != null) {
            self.print();

        } else {
            var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var postData;

            var staffInfo = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
            postData = { CLIENTID: staffInfo.CLIENT_CODE, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.readClientInfo, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
    };
    self.print = function () {

        setTimeout(function () {
            window.print();

        }, 2000);
    };

    self.sortDataTable = function (tableReference, sortColumnIndex, ascORdesc) {
        $(tableReference)
            .unbind('appendCache applyWidgetId applyWidgets sorton update updateCell')
            .removeClass('tablesorter')
            .find('thead th')
            .unbind('click mousedown')
            .removeClass('header headerSortDown headerSortUp');

        var sortedAt = 0;
        if (ascORdesc == "asc" || ascORdesc == "ASC") {
            sortedAt = 0;
        } else {
            sortedAt = 1;
        }
        
        $(tableReference).tablesorter({
            sortList: [[sortColumnIndex, sortedAt]],
            headers: {
                3: { sorter: "customDate" }
            },
            textExtraction: function (node) {
                var index = node.cellIndex;
                var isNumber = false;
                for (var i = 0; i < $(tableReference + ' thead th')[index].attributes.length; i++) {
                    if ($(tableReference + ' thead th')[index].attributes[i].name == 'sort-numbers') {
                        isNumber = true;
                        break;
                    }
                }
                
                if (isNumber) {
                    return $(node).text().replace('$', '').replace(/,/g, '');
                }
                return $(node).text();
            }
        });

        $('.not-sorted').removeClass('header').removeClass('headerSortUp').removeClass('headerSortDown');
    }
    self.searchResult = function (jsonData) {
        
        var sr = jsonData.WOPARTSREQUEST_REPTLIST;

        $(ah.config.id.searchResultList + ' ' + ah.config.tag.tbody).html('');
        $(ah.config.id.printSearchResultList).html('');

        var searchCat = $("#sumCategory").val();

        $(ah.config.id.searchResultSummaryCount).text("Number of Records: " + sr.length);
        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString()));

        self.assetCategory.count("N0. of Records: " + sr.length.toString());

        $(ah.config.cls.liPrint).hide();
        if (sr.length > 0) {
            $(ah.config.cls.liPrint).show();
            var htmlstr = '';

            for (var o in sr) {
                htmlstr += "<tr>";
                
                if (sr[o].id_parts === null) {
                    sr[o].id_parts = '-';
                }
                if (sr[o].parts_description === null) {
                    sr[o].parts_description = '-';
                }
                if (sr[o].req_no === null) {
                    sr[o].req_no = '-';
                }
                if (sr[o].req_date === null) {
                    sr[o].req_date = '-';
                }
                if (sr[o].asset_no === null) {
                    sr[o].asset_no = '-';
                }
                if (sr[o].qty === null) {
                    sr[o].qty = '-';
                }
                if (sr[o].used_qty === null) {
                    sr[o].used_qty = '-';
                }
                if (sr[o].status === null) {
                    sr[o].status = '-';
                }
                if (sr[o].price === null) {
                    sr[o].price = '-';
                }


                var reqDate = moment(sr[o].req_date).format('DD/MM/YYYY');
                var totalPrice = (sr[o].qty * sr[o].price);

                htmlstr += '<td class="a">' + sr[o].id_parts + '</td>';
                htmlstr += '<td class="h">' + sr[o].parts_description + '</td>';
                htmlstr += '<td class="a">' + sr[o].req_no + '</td>';
                htmlstr += '<td class="a">' + reqDate + '</td>';
                htmlstr += '<td class="a">' + sr[o].asset_no + '</td>';
                htmlstr += '<td class="a">' + sr[o].RETURN_QTY + '</td>';
                htmlstr += '<td class="a">' + sr[o].qty + '</td>';
                htmlstr += '<td class="a">' + sr[o].TRANSFER_QTY + '</td>';
                htmlstr += '<td class="a">' + sr[o].used_qty + '</td>';
                htmlstr += '<td class="a">' + sr[o].status + '</td>';
                htmlstr += '<td class="a">' + sr[o].price.toLocaleString('en') + '</td>';
                htmlstr += '<td class="a">' + totalPrice.toLocaleString('en') + '</td>';
                htmlstr += "</tr>";
            }
            
            $(ah.config.id.searchResultList + ' ' + ah.config.tag.tbody).append(htmlstr);
            $(ah.config.id.printSearchResultList).html('');
            $(ah.config.id.printSearchResultList).append($(ah.config.id.searchResultList).html());

            var tableReference = ah.config.id.searchResultList + ' table';
            self.sortDataTable(tableReference, 1, 'asc');
        }
        ah.displaySearchResult();

    };

    sortArray = function () {

    }

    self.readSetupID = function () {

        var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        //postData = { SUPPLIER_ID: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        //$.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.dateClick = function () {
        $("#dateClickAction").val("ACTIVE");
    };
    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();

        var fromDate = $('#requestFromDate').val();

        fromDate = fromDate.split('/');
        if (fromDate.length == 3) {
            self.searchFilter.fromDate(fromDate[2] + "/" + fromDate[1] + "/" + fromDate[0]);
        }
        var toDate = $('#requestStartDate').val().split('/');
        if (toDate.length == 3) {
            self.searchFilter.toDate(toDate[2] + "/" + toDate[1] + "/" + toDate[0] + "  23:00:00.000");
        }

        self.showLoadingBackground();
        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), SEARCH_FILTER: self.searchFilter, RETACTION: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.when($.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus)).done(function () {
            $(ah.config.id.loadingBackground).removeClass('initializing').addClass('done');
        });
    };


    self.webApiCallbackStatus = function (jsonData) {
        // console.log(jsonData);
        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));


        }
        else if (jsonData.AM_CLIENT_INFO || jsonData.WOPARTSREQUEST_REPTLIST || jsonData.STAFF || jsonData.SUCCESS || jsonData.LOV_LOOKUPS) {
            if (ah.CurrentMode == ah.config.mode.search) {

                self.searchResult(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {
                lovCostCenterArray = jsonData.LOV_LOOKUPS;

                var LOV_CostCenter = jsonData.LOV_LOOKUPS;
                LOV_CostCenter = LOV_CostCenter.filter(function (item) { return item.CATEGORY === 'AIMS_COSTCENTER' });
                $.each(LOV_CostCenter, function (item) {

                    $(ah.config.id.costcenterStrList)
                        .append($("<option>")
                            .attr("value", LOV_CostCenter[item].DESCRIPTION.trim())
                            .attr("id", LOV_CostCenter[item].LOV_LOOKUP_ID));
                });

                lovSupplierArray = jsonData.AM_SUPPLIER;
                $.each(lovSupplierArray, function (item) {

                    $(ah.config.id.supplierStrList)
                        .append($("<option>")
                            .attr("value", lovSupplierArray[item].DESCRIPTION.trim())
                            .attr("id", lovSupplierArray[item].SUPPLIER_ID));
                });
            }
            else if (ah.CurrentMode == ah.config.mode.print) {

                var clientInfo = jsonData.AM_CLIENT_INFO;

                self.clientDetails.CLIENTNAME(clientInfo.DESCRIPTION);
                self.clientDetails.CLIENT_LOGO(clientInfo.CLIENT_LOGO);
                self.clientDetails.COMPANY_LOGO(clientInfo.COMPANY_LOGO);
                self.clientDetails.HEADING(clientInfo.HEADING);
                self.clientDetails.HEADING2(clientInfo.HEADING2);
                self.clientDetails.HEADING3(clientInfo.HEADING3);
                self.clientDetails.HEADING_OTH(clientInfo.HEADING_OTH);
                self.clientDetails.HEADING_OTH2(clientInfo.HEADING_OTH2);
                self.clientDetails.HEADING_OTH3(clientInfo.HEADING_OTH3);
                self.clientDetails.CLIENTADDRESS(clientInfo.ADDRESS + ', ' + clientInfo.CITY + ', ' + clientInfo.ZIPCODE + ', ' + clientInfo.STATE);
                self.clientDetails.CLIENTCONTACT('Tel.No: ' + clientInfo.CONTACT_NO1 + ' Fax No:' + clientInfo.CONTACT_NO2);

                self.print();
            }
        }
        else {

            alert(systemText.errorTwo);
            //	window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};