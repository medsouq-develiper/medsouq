﻿var appHelper = function (systemText) {
    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]'
        },
        mode: {
            idle: 0,

            homePage: 1,
            searchPage: 2,
            addOrEditPage: 3,

            viewCompanyPR: 4,
            addOrEditCompanyPR: 5
        },
        id: {
            spanUser: '#spanUser',
            prSubmitBtn: '#prSubmitBtn',
            divNotify: '#divNotify',
        },

        cssCls: {
            viewMode: 'view-mode'
        },
        tagId: {
            txtGlobalSearchOth: 'txtGlobalSearchOth',
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfo: 'data-info',
            datainfoID: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            defaultCostCenter: '0ed52a42-9019-47d1-b1a0-3fed45f32b8f',
        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalClose: '#',
            openModalFilter: '#openModalFilter',
            openReceivePRItem: '#openReceivePRItem',
            openReceiveEquipmentItem: '#openReceiveEquipmentItem',
            openSupplierModal: '#openSupplierModal',
            openProductStockBatches: '#openProductStockBatches',
            openExistingProductBatchesModal: '#openExistingProductBatchesModal',
        },
        fld: {
            prNo: 'PRNO',
            fromMenu: 'xikhkjxhexdher'
        },
        api: {
            searchLookup: '/Inventory/SearchStores',
            createNewStockBatchV2: '/Inventory/CreateNewStockBatchV2',
            searchCompanyPR: '/AIMS/SearchCompanyPR',
            getCompanyPRByRequest: '/AIMS/GetCompanyPRByRequest',
            searchSupplier: '/AIMS/SearchSuppier',
            searchSetupLov: '/Setup/SearchInventoryLOV',
            searchMSVLookUp: '/AIMS/SearchMSVLookUp',
            searchModelManufacturer: '/Search/SearchModelManufacturer',
            getStockBatchByProductAndRequest: '/Inventory/GetStockBatchByProductAndRequest'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
        }
    };

    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };
};

/*Knockout MVVM Namespace - Controls form functionality*/
var purchaseRequestViewModel = function (systemText) {
    var ah = new appHelper(systemText);
    var self = this;

    self.modes = ah.config.mode;
    self.pageMode = ko.observable();
    self.currentMode = ko.observable();

    self.showProgress = ko.observable(false);
    self.showProgressScreen = ko.observable(false);
    self.showModalProgress = ko.observable(false);

    self.progressText = ko.observable();
    self.contentHeader = ko.observable(systemText.addModeHeader);

    self.modalQuickSearch = ko.observable();
    self.modalSelectedPRItem = ko.observable({});
    self.modalSelectedProductStockBatch = ko.observable({});
    self.modalSupplier = ko.observable();
    self.filter = {
        pageQuickSearch: ko.observable(''),
        FromDate: ko.observable(''),
        ToDate: ko.observable(''),
        Status: ko.observable('')
    };

    self.suppliers = ko.observableArray([]);
    self.suppliersCopy = ko.observableArray([]);
    self.existingProductBatches = ko.observableArray([]);
    self.purchaseRequests = ko.observableArray([]);
    self.requestingStores = ko.observableArray([]);
    self.purchaseRequestItems = ko.observableArray([]);
    self.modalassetSerial = ko.observableArray([]);

    self.purchaseRequestData = {
        AM_PRREQITEMID: ko.observable(),
        REMARKS: ko.observable(),
        REF_WO: ko.observable(),
        STATUS: ko.observable(),
        REF_ASSETNO: ko.observable(),
        REQUEST_ID: ko.observable(),
        REQ_BY: ko.observable(),
        REQ_CONTACTNO: ko.observable(),
        REQ_DEPT: ko.observable(),
        REQ_DATE: ko.observable(),
        CREATED_BY: ko.observable(),
        CREATED_DATE: ko.observable(),
        STORE_ID: ko.observable(),
        APPROVED_BY: ko.observable(),
        APPROVED_DATE: ko.observable(),
        CANCELLED_REASON: ko.observable(),
        CANCELLED_BY: ko.observable(),
        CANCELLED_DATE: ko.observable(),
        RETURNSTATUS_REASON: ko.observable(),
        RETURNSTATUS_BY: ko.observable(),
        RETURNSTATUS_DATE: ko.observable(),
        PURCHASEID: ko.observable(),
        PURCHASE_NO: ko.observable(),
        QTY: ko.observable(),
        SUPPLIER_ID: ko.observable(),
        SUPPLIERDESC: ko.observable(),
        PURCHASE_DATE: ko.observable(),
        BILL_ADDRESS: ko.observable(),
        BILL_EMAIL_ADDRESS: ko.observable(),
        BILL_CONTACT_NO1: ko.observable(),
        BILL_CONTACT_NO2: ko.observable(),
        BILL_ZIPCODE: ko.observable(),
        BILL_CITY: ko.observable(),
        BILL_STATE: ko.observable(),
        BILL_SHIPTO: ko.observable(),
        FOREIGN_CURRENCY: ko.observable(),
        SHIP_VIA: ko.observable(),
        TOTAL_AMOUNT: ko.observable(),
        TOTAL_NOITEMS: ko.observable(),
        SUPPLIER_ADDRESS1: ko.observable(),
        SUPPLIER_CITY: ko.observable(),
        SUPPLIER_STATE: ko.observable(),
        SUPPLIER_ZIPCODE: ko.observable(),
        SUPPLIER_CONTACT_NO1: ko.observable(),
        SUPPLIER_CONTACT_NO2: ko.observable(),
        SUPPLIER_CONTACT: ko.observable(),
        SUPPLIER_EMAIL_ADDRESS: ko.observable(),
        OTHER_INFO: ko.observable(),
        SHIP_DATE: ko.observable(),
        EST_SHIP_DATE: ko.observable(),
        HANDLING_FEE: ko.observable(),
        TRACKING_NO: ko.observable(),
        EST_FREIGHT: ko.observable(),
        FREIGHT: ko.observable(),
        FREIGHT_PAIDBY: ko.observable(),
        FREIGHT_CHARGETO: ko.observable(),
        TAXABLE: ko.observable(),
        TAX_CODE: ko.observable(),
        TAX_AMOUNT: ko.observable(),
        FREIGHT_POLINE: ko.observable(),
        FREIGHT_SEPARATE_INV: ko.observable(),
        INSURANCE_VALUE: ko.observable(),
        PO_BILLCODE_ADDRESS: ko.observable(),
        POINVOICENO: ko.observable()
    };

    self.signOut = function () {
        window.location.href = ah.config.url.homeIndex;
    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.toggleBoolean = function (property, passValue, mode) {
        var value = typeof passValue !== 'undefined' ? passValue : !self[property]();
        self[property](value);

        if (self[property](value) && mode) {
            switch (mode) {
                case 'search':
                    self.progressText('Please wait while searching . . .');
                    break;
                case 'save':
                    self.progressText('Please wait while saving . . .');
                    break;
                case 'get':
                    self.progressText('Please wait while getting Purchase Receipt Information . . .');
                    break;
            }
        }
    };
    self.setObservableValue = function (_object1, _object2) {
        Object.keys(_object2).forEach(function (key) {
            if (ko.isObservable(_object1[key]))
                _object1[key](_object2[key])
            else
                _object1[key] = _object2[key];
        });
    };
    self.getObservableValue = function (data) {
        if (Array.isArray(data)) {
            var returnData = [];
            for (var i = 0; i < data.length; i++) {
                var _object = data[i];
                returnData.push({});
                Object.keys(_object).forEach(function (key) {
                    if (ko.isObservable(_object[key]))
                        returnData[i][key] = _object[key]();
                    else
                        returnData[i][key] = _object[key];
                });
            }
        }
        else if (data instanceof Object) {
            var returnData = {};
            var _object = data;
            Object.keys(_object).forEach(function (key) {
                if (ko.isObservable(_object[key]))
                    returnData[key] = _object[key]();
                else
                    returnData[key] = _object[key];
            });
        }
        else
            returnData = data;

        return returnData;
    };
    self.checkViewMode = function () {
        return self.currentMode() != self.modes.viewCompanyPR
    };

    self.clearModalFilter = function () {
        self.filter.FromDate('');
        self.filter.ToDate('');
        self.filter.Status('');
        $('#modalFromDate').val('');
        $('#modalToDate').val('');
    };
    self.openModalFilter = function () {
        window.location.href = ah.config.url.openModalFilter;
    };
    self.filterSearch = function () {
        self.pageMode(self.modes.searchPage);
        self.toggleBoolean('showProgress', true, 'search');
        self.filter.pageQuickSearch('');
        var FromDate = $('#modalFromDate').val();
        var ToDate = $('#modalToDate').val();
        if (moment(FromDate, 'DD/MM/YYYY', true).isValid())
            self.filter.FromDate(moment(FromDate, 'DD/MM/YYYY').format())
        if (moment(ToDate, 'DD/MM/YYYY', true).isValid())
            self.filter.ToDate(moment(ToDate, 'DD/MM/YYYY').format())

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        var postData = { SEARCH: "", SEARCH_FILTER: self.filter, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchCompanyPR, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.quickSearch = function () {
        self.pageMode(self.modes.searchPage);
        self.toggleBoolean('showProgress', true, 'search');

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        var postData = { SEARCH: self.filter.pageQuickSearch, SEARCH_FILTER: { FromDate: "" }, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchCompanyPR, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.selectPurchaseRequest = function (data) {
        var selectedPurchaseRequest = data;
        self.pageMode(self.modes.addOrEditPage);
        self.currentMode(self.modes.viewCompanyPR);
        self.toggleBoolean('showProgress', true);

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        var postData = { REQUEST_ID: selectedPurchaseRequest.REQUEST_ID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.getCompanyPRByRequest, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.receivePRItem = function () {
        if (!self.purchaseRequestData.POINVOICENO()) {
            return alert('Please enter invoice number.');
        }

        var PRItem = self.getObservableValue(this);
        var assetDetails = (PRItem.productAssets && PRItem.productAssets[0]) || {};
        if (PRItem.AM_TYPE_ID == 'AIMS_MSTYPE-3') {
            if (!assetDetails.PRODUCT_CODE) {
                PRItem.PARTDESCRIPTION = ko.observable(PRItem.DESCRIPTION);
                PRItem.PRICE = ko.observable(PRItem.PRICE);
                PRItem.TOTAL_PRICE = ko.observable(0);
                PRItem.QTY_RECEIVED = ko.observable(0);
                PRItem.BOX_QTY_RECEIVED = ko.observable(0);
                PRItem.TOTAL_QTY_RECEIVED = ko.observable(PRItem.TOTAL_QTY_RECEIVED);
                PRItem.BARCODE_ID = ko.observable();
                PRItem.EXPIRY_DATE_DISPLAY = ko.observable();
                PRItem.POINVOICENO = ko.observable(self.purchaseRequestData.POINVOICENO());
                PRItem.INVOICENO = ko.observable(self.purchaseRequestData.POINVOICENO());
                PRItem.MODELCODE = ko.observable();
                PRItem.SUPPLIER = ko.observable(self.purchaseRequestData.SUPPLIER_ID());
                PRItem.SUPPLIERNAME = ko.observable(self.purchaseRequestData.SUPPLIERDESC());

                var dCC = JSON.parse(sessionStorage.getItem(ah.config.skey.defaultCostCenter));

                PRItem.DESCRIPTION = ko.observable(PRItem.DESCRIPTION);
                PRItem.VOLTAGE = ko.observable();
                PRItem.MDMANO = ko.observable();
                PRItem.COST_CENTER = ko.observable(dCC.length && dCC[0].CODEID);
                PRItem.COST_CENTERNAME = ko.observable(dCC.length && dCC[0].DESCRIPTION);
                PRItem.PURCHASE_DATE_DISPLAY = ko.observable(self.purchaseRequestData.PURCHASE_DATE());
                PRItem.PURCHASE_NO = ko.observable(self.purchaseRequestData.REQUEST_ID());
            }
            else {
                PRItem.PARTDESCRIPTION = ko.observable(PRItem.DESCRIPTION);
                PRItem.BOX_QTY_RECEIVED = ko.observable(0);
                PRItem.BARCODE_ID = ko.observable();
                PRItem.EXPIRY_DATE_DISPLAY = ko.observable();
                PRItem.MODELCODE = ko.observable();

                PRItem.TOTAL_PRICE = ko.observable(Number(PRItem.TOTAL_QTY_RECEIVED) * Number(PRItem.PRICE));
                PRItem.PRICE = ko.observable(PRItem.PRICE);
                PRItem.QTY_RECEIVED = ko.observable(PRItem.TOTAL_QTY_RECEIVED);
                PRItem.TOTAL_QTY_RECEIVED = ko.observable(0);
                PRItem.POINVOICENO = ko.observable(assetDetails.POINVOICENO);
                PRItem.INVOICENO = ko.observable(assetDetails.POINVOICENO);
                PRItem.SUPPLIER = ko.observable(assetDetails.SUPPLIER);
                PRItem.SUPPLIERNAME = ko.observable(assetDetails.SUPPLIERNAME);

                PRItem.DESCRIPTION = ko.observable(assetDetails.DESCRIPTION);
                PRItem.VOLTAGE = ko.observable(assetDetails.VOLTAGE);
                PRItem.MDMANO = ko.observable(assetDetails.MDMANO);
                PRItem.COST_CENTER = ko.observable(assetDetails.COST_CENTER);
                PRItem.COST_CENTERNAME = ko.observable(assetDetails.COST_CENTERNAME);
                PRItem.PURCHASE_DATE_DISPLAY = ko.observable(assetDetails.PURCHASE_DATE_DISPLAY);
                PRItem.PURCHASE_NO = ko.observable(assetDetails.PURCHASE_NO);
            }

            self.modalSelectedPRItem(PRItem);

            window.location.href = ah.config.url.openReceiveEquipmentItem;
        }
        else {
            PRItem.PRICE = ko.observable(PRItem.PRICE);
            PRItem.TOTAL_PRICE = ko.observable(0);
            PRItem.QTY_RECEIVED = ko.observable(0);
            PRItem.BOX_QTY_RECEIVED = ko.observable(0);
            PRItem.TOTAL_QTY_RECEIVED = ko.observable(PRItem.TOTAL_QTY_RECEIVED);
            PRItem.BARCODE_ID = ko.observable();
            PRItem.EXPIRY_DATE_DISPLAY = ko.observable();
            PRItem.INVOICENO = ko.observable(self.purchaseRequestData.POINVOICENO());
            PRItem.MODELCODE = ko.observable();
            PRItem.SUPPLIER = ko.observable(self.purchaseRequestData.SUPPLIER_ID());
            PRItem.SUPPLIERNAME = ko.observable(self.purchaseRequestData.SUPPLIERDESC());
            PRItem.isUpdatingBatch = ko.observable(false);
            self.modalSelectedPRItem(PRItem);

            self.modalSupplier('');

            window.location.href = ah.config.url.openReceivePRItem;
        }
    };
    self.searchExistingBNSN = function () {
        if (self.modalSelectedPRItem().BARCODE_ID()) {
            self.toggleBoolean('showProgress', true);

            self.modalSelectedPRItem().isUpdatingBatch(false);
            if (self.modalSelectedPRItem().BATCH_ID) {
                self.modalSelectedPRItem().BATCH_ID = 0;
                self.modalSelectedPRItem().PRICE(0);
                self.modalSelectedPRItem().QTY_RECEIVED(0);
                self.modalSelectedPRItem().BOX_QTY_RECEIVED(0);
                self.modalSelectedPRItem().TOTAL_PRICE(0);
                self.modalSelectedPRItem().EXPIRY_DATE_DISPLAY('');
                self.modalSelectedPRItem().INVOICENO('');
                self.modalSelectedPRItem().MODELCODE('');

                self.modalSupplier('');
            }

            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var postData = { REQUEST_ID: self.purchaseRequestData.REQUEST_ID(), PRODUCT_CODE: self.modalSelectedPRItem().ID_PARTS, BARCODE_ID: self.modalSelectedPRItem().BARCODE_ID(), STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.getStockBatchByProductAndRequest, postData).done(function (jsonData) {
                self.toggleBoolean('showProgress', false);

                var PRODUCT_STOCKBATCH = jsonData.PRODUCT_STOCKBATCH || [];
                for (var i = 0; i < PRODUCT_STOCKBATCH.length; i++) {
                    PRODUCT_STOCKBATCH[i].EXPIRY_DATE_DISPLAY = moment(PRODUCT_STOCKBATCH[i].EXPIRY_DATE).format('DD/MM/YYYY')
                }
                self.existingProductBatches(PRODUCT_STOCKBATCH);

                if (self.existingProductBatches().length)
                    window.location.href = ah.config.url.openExistingProductBatchesModal;
            }).fail(self.webApiCallbackStatus);
        }
    };
    self.assignProductBatch = function () {
        var data = this;
        self.modalSelectedPRItem().isUpdatingBatch(true);
        self.modalSelectedPRItem().BATCH_ID = data.BATCH_ID;
        self.modalSelectedPRItem().PRICE(data.PRICE);
        self.modalSelectedPRItem().QTY_RECEIVED(0);
        self.modalSelectedPRItem().BOX_QTY_RECEIVED(0);
        self.modalSelectedPRItem().TOTAL_PRICE(0);
        self.modalSelectedPRItem().EXPIRY_DATE_DISPLAY(data.EXPIRY_DATE_DISPLAY);
        self.modalSelectedPRItem().INVOICENO(data.INVOICENO);
        self.modalSelectedPRItem().MODELCODE(data.MODELCODE);

        self.suppliers.push({
            SUPPLIER_ID: data.SUPPLIER,
            DESCRIPTION: data.SUPPLIERNAME
        });
        self.modalSupplier(data.SUPPLIER);

        window.location.href = ah.config.url.openReceivePRItem;
    };
    self.removeBarcodeId = function () {
        self.modalSelectedPRItem().BARCODE_ID('');
        window.location.href = ah.config.url.openReceivePRItem;
    };
    self.openSupplierModal = function () {
        self.modalQuickSearch('');

        self.suppliers(self.suppliersCopy());
        window.location.href = ah.config.url.openSupplierModal;
    };
    self.removeSupplier = function () {
        self.modalSelectedPRItem().SUPPLIER = '';
        self.modalSelectedPRItem().SUPPLIERNAME('');
    };
    self.searchSupplier = function (search) {
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        if (search) {
            self.toggleBoolean('showModalProgress', true);
            var postData = { SEARCH: self.modalQuickSearch(), STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchSupplier, postData).done(function (jsonData) {
                self.suppliers(jsonData.AM_SUPPLIER || []);

                self.toggleBoolean('showModalProgress', false);
            }).fail(self.webApiCallbackStatus);
        }
        else
            self.openSupplierModal();
    };
    self.getSupplier = function (searchTerm, sourceArray) {
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData = { SEARCH: searchTerm || '', STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchSupplier, postData).done(function (jsonData) {
            sourceArray(jsonData.AM_SUPPLIER || []);
        }).fail(self.webApiCallbackStatus);
    }
    self.assignSupplier = function () {
        self.modalSelectedPRItem().SUPPLIER = this.SUPPLIER_ID;
        self.modalSelectedPRItem().SUPPLIERNAME(this.DESCRIPTION);

        window.location.href = ah.config.url.openReceivePRItem;
    };
    self.calculateTotalPrice = function () {
        if (!isNaN(self.modalSelectedPRItem().QTY_RECEIVED()) && !isNaN(self.modalSelectedPRItem().PR_REQUEST_QTY) && !isNaN(self.modalSelectedPRItem().TOTAL_QTY_RECEIVED())) {
            if ((Number(self.modalSelectedPRItem().QTY_RECEIVED()) + Number(self.modalSelectedPRItem().TOTAL_QTY_RECEIVED())) > Number(self.modalSelectedPRItem().PR_REQUEST_QTY)) {
                self.modalSelectedPRItem().QTY_RECEIVED(0);
                self.modalSelectedPRItem().TOTAL_PRICE(0);
                return alert(self.modalSelectedPRItem().ID_PARTS + " quantity to receive should not be greter than purchase quantity");
            }

            self.modalSelectedPRItem().TOTAL_PRICE(Number(self.modalSelectedPRItem().QTY_RECEIVED()) * Number(self.modalSelectedPRItem().PRICE()));

            //    alert(self.modalSelectedPRItem().QTY_RECEIVED());

            if (self.modalSelectedPRItem().QTY_RECEIVED() > 0) {
                var qtyRecevied = self.modalSelectedPRItem().QTY_RECEIVED();
                var i = 0;
                self.modalassetSerial.splice(0, 1000);

                do {
                    i++;
                    var data = {
                        NO: i,
                        SERIALNO: null,
                        PRODUCT_CODE: self.modalSelectedPRItem().ID_PARTS
                    }
                    self.modalassetSerial.push(data);
                }
                while (i < qtyRecevied);

            }

        }
    };
    self.addReceivePRItem = function () {
        var selectedPRItem = self.modalSelectedPRItem();
        var index = self.purchaseRequestItems().findIndex(function (item) { return item.ID_PARTS == selectedPRItem.ID_PARTS });
        var assetDetails = (selectedPRItem.productAssets && selectedPRItem.productAssets[0]) || {};
        var stockBatchData = {
            BATCH_ID: selectedPRItem.BATCH_ID || 0,
            PRODUCT_DESC: selectedPRItem.DESCRIPTION,
            PRODUCT_CODE: selectedPRItem.ID_PARTS,
            ID_PARTS: selectedPRItem.ID_PARTS,
            TRANS_ID: 0,
            MIN: 0,
            MAX: 0,
            STATUS: 'FOR POSTING',
            PRICE: selectedPRItem.PRICE(),
            PRICE_DISCOUNT: 0,
            QTY: selectedPRItem.PR_REQUEST_QTY,
            TOTAL_PRICE: selectedPRItem.TOTAL_PRICE(),
            BARCODE_ID: selectedPRItem.BARCODE_ID(),
            INVOICENO: selectedPRItem.INVOICENO(),
            MODELCODE: selectedPRItem.MODELCODE(),
            STORE: self.purchaseRequestData.STORE_ID(),
            EXPIRY_DATE: selectedPRItem.EXPIRY_DATE_DISPLAY() ? moment(selectedPRItem.EXPIRY_DATE_DISPLAY(), 'DD/MM/YYYY').format() : '',
            EXPIRY_DATE_DISPLAY: selectedPRItem.EXPIRY_DATE_DISPLAY(),
            SUPPLIER: selectedPRItem.SUPPLIER(),
            SUPPLIERNAME: selectedPRItem.SUPPLIERNAME(),
            TRANS_ID: self.purchaseRequestData.REQUEST_ID(),
            PURCHASE_NO: self.purchaseRequestData.REQUEST_ID(),
            TOTAL_QTY_RECEIVED: selectedPRItem.TOTAL_QTY_RECEIVED(),
            QTY_RECEIVED: selectedPRItem.QTY_RECEIVED(),
            BOX_QTY_RECEIVED: selectedPRItem.BOX_QTY_RECEIVED(),
            QTY_RETURN: 0,
            RETURN_REASON: null,
            UOM_DESC: selectedPRItem.AM_UOM,
            TYPE_DESC: selectedPRItem.AM_TYPE,
        };
        if (selectedPRItem.AM_TYPE_ID != 'AIMS_MSTYPE-3') {
            self.purchaseRequestItems()[index].TOTAL_QTY_RECEIVED(Number(self.purchaseRequestItems()[index].TOTAL_QTY_RECEIVED()) + Number(selectedPRItem.QTY_RECEIVED()));
            self.purchaseRequestItems()[index].productStockBatches.push(stockBatchData);
        }
        else {
            var serialEmpty = ko.utils.arrayFirst(self.modalassetSerial(), function (item) { return item.SERIALNO == null || item.SERIALNO == "" });

            if (serialEmpty != null) {
                alert("mandatory serial number for each equipment you receive.");
                window.location.href = ah.config.url.openReceiveEquipmentItem;
                return
            }

            var assetData = {
                PRODUCT_CODE: selectedPRItem.ID_PARTS,
                STATUS: 'A',
                PURCHASE_COST: selectedPRItem.PRICE(),
                POINVOICENO: selectedPRItem.POINVOICENO(),
                SUPPLIER: selectedPRItem.SUPPLIER(),
                SUPPLIERNAME: selectedPRItem.SUPPLIERNAME(),
                PURCHASE_NO: selectedPRItem.PURCHASE_NO(),

                DESCRIPTION: selectedPRItem.DESCRIPTION(),
                VOLTAGE: selectedPRItem.VOLTAGE(),
                MDMANO: selectedPRItem.MDMANO(),
                COST_CENTER: selectedPRItem.COST_CENTER(),
                COST_CENTERNAME: selectedPRItem.COST_CENTERNAME(),
                PURCHASE_DATE: selectedPRItem.PURCHASE_DATE_DISPLAY() ? moment(selectedPRItem.PURCHASE_DATE_DISPLAY(), 'DD/MM/YYYY').format() : '',
                PURCHASE_DATE_DISPLAY: selectedPRItem.PURCHASE_DATE_DISPLAY(),
                CATEGORY: self.modalDeviceType(),
                CATEGORYNAME: self.modalDeviceType() ? $('#jqAuto-device-type').val() : '',
                MANUFACTURER: self.modalManufacturer(),
                MANUFACTURERNAME: self.modalManufacturer() ? $('#jqAuto-manufacturer').val() : '',
                MODEL_NO: selectedPRItem.MODELCODE(),//self.modalManufacturer() ? self.modalModel() : '',
                MODEL_NAME: selectedPRItem.MODELCODE()//self.modalManufacturer() && self.modalModel() ? $('#jqAuto-model').val() : '',
            };
            if (!assetDetails.PRODUCT_CODE) {
                self.purchaseRequestItems()[index].TOTAL_QTY_RECEIVED(Number(self.purchaseRequestItems()[index].TOTAL_QTY_RECEIVED()) + Number(selectedPRItem.QTY_RECEIVED()));
                self.purchaseRequestItems()[index].productStockBatches.push(stockBatchData);

                self.purchaseRequestItems()[index].productAssets.push(assetData);
            }
            else {
                self.purchaseRequestItems()[index].TOTAL_QTY_RECEIVED(Number(selectedPRItem.QTY_RECEIVED()));
                self.purchaseRequestItems()[index].productStockBatches()[0] = stockBatchData;

                self.purchaseRequestItems()[index].productAssets()[0] = assetData;
            }
        }

        window.location.href = ah.config.url.modalClose;
    };
    self.viewProductStockBatches = function () {
        self.toggleBoolean('showProgress', true);
        self.modalSelectedProductStockBatch(this);

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData = { REQUEST_ID: self.purchaseRequestData.REQUEST_ID(), PRODUCT_CODE: this.ID_PARTS, BARCODE_ID: '', STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.getStockBatchByProductAndRequest, postData).done(function (jsonData) {
            self.toggleBoolean('showProgress', false);

            var PRODUCT_STOCKBATCH = jsonData.PRODUCT_STOCKBATCH || [];
            for (var i = 0; i < PRODUCT_STOCKBATCH.length; i++) {
                PRODUCT_STOCKBATCH[i].EXPIRY_DATE_DISPLAY = PRODUCT_STOCKBATCH[i].EXPIRY_DATE ? moment(PRODUCT_STOCKBATCH[i].EXPIRY_DATE).format('DD/MM/YYYY') : ''
            }
            self.modalSelectedProductStockBatch().receivedProductStockBatches(PRODUCT_STOCKBATCH);

            var AM_ASSET_DETAILS = jsonData.AM_ASSET_DETAILS || [];
            self.modalSelectedProductStockBatch().receivedProductAssets(AM_ASSET_DETAILS);

            window.location.href = ah.config.url.openProductStockBatches;
        }).fail(self.webApiCallbackStatus);
    };
    self.removeProductStockBatch = function () {
        var selectedProductStockBatch = this;
        var selectedPRItem = self.modalSelectedProductStockBatch();
        var index = self.purchaseRequestItems().findIndex(function (item) { return item.ID_PARTS == selectedPRItem.ID_PARTS });
        self.purchaseRequestItems()[index].TOTAL_QTY_RECEIVED(Number(self.purchaseRequestItems()[index].TOTAL_QTY_RECEIVED()) - Number(selectedProductStockBatch.QTY_RECEIVED));
        self.purchaseRequestItems()[index].productStockBatches.remove(selectedProductStockBatch);
    };
    self.cancelChanges = function () {
        self.pageMode(self.modes.homePage);
    };
    self.triggerFormSubmit = function () {
        $(ah.config.id.prSubmitBtn).trigger('click');
    };
    self.saveAllChanges = function () {
        var PRODUCT_STOCKBATCH = [];
        var AM_ASSET_DETAILS = [];
        var ASSET_SERIALNO = []
        for (var i = 0; i < self.purchaseRequestItems().length; i++) {
            for (var j = 0; j < self.purchaseRequestItems()[i].productStockBatches().length; j++) {
                PRODUCT_STOCKBATCH.push(self.purchaseRequestItems()[i].productStockBatches()[j]);
            }
            for (var j = 0; j < self.purchaseRequestItems()[i].productAssets().length; j++) {
                AM_ASSET_DETAILS.push(self.purchaseRequestItems()[i].productAssets()[j]);
            }
            for (var j = 0; j < self.modalassetSerial().length; j++) {
                ASSET_SERIALNO.push(self.modalassetSerial()[j]);
            }
        }
        if (!PRODUCT_STOCKBATCH.length)
            return alert('Unable to proceed. No item to receive. Please click on Receive from Purchase button and select purchase request you wish to receive.');

        self.currentMode(self.modes.addOrEditCompanyPR);
        self.toggleBoolean('showProgress', true, 'save');
        self.toggleBoolean('showProgressScreen', true);

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        var AM_PURCHASEREQUEST = self.getObservableValue(self.purchaseRequestData);
        AM_PURCHASEREQUEST.REQ_DATE = moment(AM_PURCHASEREQUEST.REQ_DATE, 'DD/MM/YYYY HH:mm').format();
        AM_PURCHASEREQUEST.PURCHASE_DATE = moment(AM_PURCHASEREQUEST.PURCHASE_DATE, 'DD/MM/YYYY HH:mm').format();
        if (AM_PURCHASEREQUEST.EST_SHIP_DATE != null && AM_PURCHASEREQUEST.EST_SHIP_DATE != "") {
            AM_PURCHASEREQUEST.EST_SHIP_DATE = moment(AM_PURCHASEREQUEST.EST_SHIP_DATE, 'DD/MM/YYYY HH:mm').format();
        }


        var postData = { ASSET_SERIALNO: ASSET_SERIALNO, AM_PURCHASEREQUEST: AM_PURCHASEREQUEST, PRODUCT_STOCKBATCH: PRODUCT_STOCKBATCH, AM_ASSET_DETAILS: AM_ASSET_DETAILS, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.createNewStockBatchV2, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.initialize = function () {
        window.location.href = ah.config.url.modalClose;

        self.pageMode(self.modes.homePage);
        self.toggleBoolean('showProgress', true, 'save');
        self.toggleBoolean('showProgressScreen', true);

        $(ah.config.id.divNotify).css('display', 'none');

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        var postData = { SEARCH: '', STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.when($.post(self.getApi() + ah.config.api.searchSupplier, postData).done(function (jsonData) {
            self.suppliersCopy(jsonData.AM_SUPPLIER || []);
        }).fail(self.webApiCallbackStatus)).done(function () {
            var postData = { LOV: "'SHIP_VIA','FOREIGN_CURRENCY'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchSetupLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus)
        });
    };

    self.shipVias = ko.observableArray([]);
    self.foreignCurrencies = ko.observableArray([]);

    self.modalDeviceType = ko.observable();
    self.deviceTypes = ko.observableArray([]);
    self.getDeviceTypes = function (searchTerm, sourceArray) {
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData = { LOV: "'AIMS_DTYPE'", SEARCH: searchTerm.trim() || '', STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchSetupLov, postData).done(function (jsonData) {
            sourceArray(jsonData.LOV_LOOKUPS || []);
        }).fail(self.webApiCallbackStatus);
    }
    self.modalManufacturer = ko.observable();
    self.manufacturers = ko.observableArray([]);
    self.getManufacturers = function (searchTerm, sourceArray) {
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData = { TYPE: "M", SEARCH: searchTerm.trim() || '', STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchMSVLookUp, postData).done(function (jsonData) {
            sourceArray(jsonData.AM_MANUFACTURER || []);
        }).fail(self.webApiCallbackStatus);
    }
    self.modalModel = ko.observable();
    self.models = ko.observableArray([]);
    self.getModels = function (searchTerm, sourceArray) {
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData = { MANUFACTURER_ID: self.modalManufacturer(), SEARCH: searchTerm.trim() || '', STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchModelManufacturer, postData).done(function (jsonData) {
            sourceArray(jsonData.AM_MANUFACTURER_MODEL || []);
        }).fail(self.webApiCallbackStatus);
    }
    self.removeModel = function () {
        self.modalModel('');
    }


    self.calculateTotalPricePrint = function (item) {
        item.TOTAL_PRICE = (item.PRICE || 0) * item.PR_REQUEST_QTY;
    };
    self.assignedSOHPrint = ko.observableArray([]);
    self.clientDetails = {
        CLIENTNAME: ko.observable(),
        CLIENT_LOGO: ko.observable(),
        COMPANY_LOGO: ko.observable(),
        HEADING: ko.observable(),
        HEADING2: ko.observable(),
        HEADING3: ko.observable(),
        HEADING_OTH: ko.observable(),
        HEADING_OTH2: ko.observable(),
        HEADING_OTH3: ko.observable(),
        CLIENTADDRESS: ko.observable(),
        CLIENTCONTACT: ko.observable()
    };
    self.printPurchaseRequest = function () {
        self.clientDetails.HEADING('Business Schema Trading Company');
        self.clientDetails.COMPANY_LOGO(window.location.origin + '/images/companylogo.png');

        self.assignedSOHPrint.removeAll();
        self.assignedSOHPrint(self.purchaseRequestItems.slice(0));
        if (self.purchaseRequestData.FREIGHT_POLINE() == 'YES') {
            self.assignedSOHPrint.push({
                DESCRIPTION: 'FREIGHT CHARGES',
                PR_REQUEST_QTY: 1,
                PRICE: self.purchaseRequestData.FREIGHT() || 0,
                TOTAL_PRICE: self.purchaseRequestData.FREIGHT() || 0,
            });
        }

        setTimeout(function () {
            window.print();
        }, 1000)
    };
    self.getLookupDescription = function (value, array) {
        return value && array.length && array.find(function (x) { return x.LOV_LOOKUP_ID == value }).DESCRIPTION;
    };
    self.getTotalPrice = function () {
        var itemTotalPrice = self.assignedSOHPrint().reduce(function (acc, val) { return acc + val.TOTAL_PRICE; }, 0);
        return itemTotalPrice + Number(self.purchaseRequestData.TAX_AMOUNT() || 0);
    };
    self.getPODate = function () {
        return moment(self.purchaseRequestData.PURCHASE_DATE(), 'DD/MM/YYYY').format('DD-MMM-YYYY');
    };

    self.webApiCallbackStatus = function (jsonData) {
        if (jsonData.ERROR) {
            if (jsonData.ERROR.ErrorText === "Work Order not found.") {
                alert(jsonData.ERROR.ErrorText);
            } else if (jsonData.ERROR.ErrorText === "Asset not found.") {
                alert(jsonData.ERROR.ErrorText);
            } else {
                alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            }

            self.toggleBoolean('showProgress', false);
            self.toggleBoolean('showProgressScreen', false);

            $(ah.config.id.divNotify).css('display', 'none');
        }
        else if (!jsonData.ERROR) {
            switch (self.pageMode()) {
                case self.modes.searchPage:
                    self.toggleBoolean('showProgress', false);

                    jsonData.AM_PURCHASEREQUEST = jsonData.AM_PURCHASEREQUEST.filter(function (item) { return item.STATUS == 'APPROVED' || item.STATUS == 'RECEIVED' || item.STATUS == 'PARTIAL RECEIVED' });
                    self.purchaseRequests(jsonData.AM_PURCHASEREQUEST);

                    window.location.href = ah.config.url.modalClose;
                    break;

                case self.modes.homePage:
                    self.toggleBoolean('showProgress', false);
                    self.toggleBoolean('showProgressScreen', false);

                    var shipVias = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'SHIP_VIA' });;
                    shipVias.unshift({
                        LOV_LOOKUP_ID: null,
                        DESCRIPTION: '- select -'
                    });
                    self.shipVias(shipVias);

                    var foreignCurrencies = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'FOREIGN_CURRENCY' });;
                    foreignCurrencies.unshift({
                        LOV_LOOKUP_ID: null,
                        DESCRIPTION: '- select -'
                    });
                    self.foreignCurrencies(foreignCurrencies);
                    var prReq = ah.getParameterValueByName(ah.config.fld.fromMenu);
                    if (prReq !== null && prReq == "APPROVED") {
                        $("#filterStatus").val("APPROVED");
                        self.filter.Status("APPROVED");
                        self.filterSearch();
                    }

                    break;

                case self.modes.addOrEditPage:

                    switch (self.currentMode()) {
                        case self.modes.addOrEditCompanyPR:
                            self.currentMode(self.modes.viewCompanyPR);
                            self.toggleBoolean('showProgress', false);
                            self.toggleBoolean('showProgressScreen', false);

                            $(ah.config.id.divNotify).css('display', 'block');
                            $(ah.config.id.divNotify + ' p').html(systemText.saveSuccessMessage);
                            setTimeout(function () {
                                $(ah.config.id.divNotify).fadeOut(1000);
                            }, 3000);

                            self.selectPurchaseRequest(jsonData.AM_PURCHASEREQUEST);
                            break;

                        case self.modes.viewCompanyPR:
                            self.toggleBoolean('showProgress', false);
                            jsonData.AM_PURCHASEREQUESITEMS = jsonData.AM_PURCHASEREQUESITEMS || [];
                            for (var i = 0; i < jsonData.AM_PURCHASEREQUESITEMS.length; i++) {
                                jsonData.AM_PURCHASEREQUESITEMS[i].TOTAL_QTY_RECEIVED = ko.observable(jsonData.AM_PURCHASEREQUESITEMS[i].TOTAL_QTY_RECEIVED || 0);
                                jsonData.AM_PURCHASEREQUESITEMS[i].productStockBatches = ko.observableArray([]);
                                jsonData.AM_PURCHASEREQUESITEMS[i].receivedProductStockBatches = ko.observableArray([]);
                                jsonData.AM_PURCHASEREQUESITEMS[i].productAssets = ko.observableArray([]);
                                jsonData.AM_PURCHASEREQUESITEMS[i].receivedProductAssets = ko.observableArray([]);

                                jsonData.AM_PURCHASEREQUESITEMS[i].TOTAL_PRICE = 0;
                                self.calculateTotalPricePrint(jsonData.AM_PURCHASEREQUESITEMS[i]);
                            }

                            self.setObservableValue(self.purchaseRequestData, jsonData.AM_PURCHASEREQUEST);
                            self.purchaseRequestData.REQ_DATE(moment(self.purchaseRequestData.REQ_DATE()).format('DD/MM/YYYY HH:mm'));
                            self.purchaseRequestData.PURCHASE_DATE(moment(self.purchaseRequestData.PURCHASE_DATE()).format('DD/MM/YYYY'));
                            self.purchaseRequestData.EST_SHIP_DATE(self.purchaseRequestData.EST_SHIP_DATE() ? moment(self.purchaseRequestData.EST_SHIP_DATE()).format('DD/MM/YYYY') : '');
                            self.purchaseRequestItems(jsonData.AM_PURCHASEREQUESITEMS || []);
                            break;
                    }
                    break;
            }
        }
        else {
            alert(systemText.errorTwo);

            self.toggleBoolean('showProgress', false);
            self.toggleBoolean('showProgressScreen', false);

            $(ah.config.id.divNotify).css('display', 'none');
        }
    };

    self.initialize();
};