﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',

            statusText: '.status-text',
            detailItem: '.detailItem'
        },
        tag: {

            textArea: 'textarea',
            select: 'select',
            tbody: 'tbody',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7
        },
        tagId: {
            tcCode: 'tcCode',
            tcInService: 'tcInService',
            tcDescription: 'tcDescription',
            tcActive: 'tcActive',
            tcMissing: 'tcMissing',
            tcInActive: 'tcInActive',
            tcOtService: 'tcOtService',
            tcRetired: 'tcRetired',
            tcTransfer: 'tcTransfer',
            tcUndifined: 'tcUndifined'
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',

            staffID: '#STAFF_ID',
            serviceID: '#SERVICE_ID',
            successMessage: '#successMessage'
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'AM_ASSETSUMMARY',

        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalClose: '#',
            modalFilter: '#openModalFilter'
        },
        api: {

            searchAll: '/Reports/SearchPurchaseReqRept'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave).hide();

    };

    thisApp.ConvertFormToJSON = function (form) {
        $(form).find("input:disabled").removeAttr("disabled");
        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name),
                type = $el.attr('type');

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var workOrderByEngrReptViewModel = function (systemText) {

    var self = this;

    var ah = new appHelper(systemText);
    self.searchFilter = {
        fromDate: ko.observable(''),
        toDate: ko.observable(''),
        Status: ko.observable(''),
        POStatus: ko.observable(''),

    }
    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());



        ah.initializeLayout();
        var today = moment(new Date()).format("DD/MM/YYYY");

        $('#requestFromDate').val(today);
        $('#requestStartDate').val(today);
    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };

    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.createNew = function () {

        ah.toggleAddMode();

    };
    self.searchFilterNow = function () {
        window.location.href = ah.config.url.modalClose;
        self.searchNow();
    };
    self.showModalFilter = function () {
        window.location.href = ah.config.url.modalFilter;
    };
    self.clearFilter = function () {

        $('#requestFromDate').val('');
        $('#requestStartDate').val('');
        $('#filterStatus').val('');
        $('#filterPOStatus').val('');
        self.searchFilter.fromDate = '';
        self.searchFilter.toDate = '';
        self.searchFilter.Status = '';
        self.searchFilter.POStatus = '';
    }
    self.modifySetup = function () {

        ah.toggleEditMode();

    };

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };


    self.sortTable = function () {


        var senderID = $(arguments[1].currentTarget).attr('id');

        switch (senderID) {


            case ah.config.tagId.tcInService: {
                self.searchResult('WO_FA');
                break;
            }
            case ah.config.tagId.tcDescription: {
                self.searchResult('DESCRIPTION');
                break;
            }
            case ah.config.tagId.tcActive: {
                self.searchResult('CLOSED');
                break;
            }
            case ah.config.tagId.tcMissing: {
                self.searchResult('WO_HELD');
                break;
            }
            case ah.config.tagId.tcInActive: {
                self.searchResult('WO_NEW');
                break;
            }
            case ah.config.tagId.tcOtService: {
                self.searchResult('WO_OPEN');
                break;
            }
            case ah.config.tagId.tcRetired: {
                self.searchResult('WO_POSTED');
                break;
            }


        }


    };

    self.searchResult = function (jsonData) {


        var sr = jsonData.PURCHASEREQ_REPTLIST

        $(ah.config.id.searchResultList + ' ' + ah.config.tag.tbody).html('');

        var searchCat = $("#sumCategory").val();



        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

      




        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {
                htmlstr += "<tr>";
                if (sr[o].PO_STATUS === null || sr[o].PO_STATUS === "") {
                    sr[o].PO_STATUS = '-';
                }
                if (sr[o].PO_STATUS === null || sr[o].PO_STATUS === "") {
                    sr[o].PO_STATUS = '-';
                }
                if (sr[o].PURCHASE_NO === null || sr[o].PURCHASE_NO === 0) {
                    sr[o].PURCHASE_NO = '-';
                    sr[o].PURCHASE_QTY = '-';
                    sr[o].PURCHASE_UNITPRICE = '-';
                    sr[o].SUPPLIERDESC = '-';
                    sr[o].QTY_RECEIVED = '-';
                }

                htmlstr += '<td class="a">' + sr[o].ID_PARTS + '</td>';
                htmlstr += '<td class="h">' + sr[o].DESCRIPTION + '</td>';
                htmlstr += '<td class="a">' + sr[o].REQ_DATE + '</td>';
                htmlstr += '<td class="a">' + sr[o].REQUEST_ID + '</td>';
                htmlstr += '<td class="a">' + sr[o].STATUS + '</td>';
                htmlstr += '<td class="a">' + sr[o].QTY + '</td>';
                htmlstr += '<td class="a">' + sr[o].PURCHASE_NO + '</td>';
                htmlstr += '<td class="a">' + sr[o].PO_STATUS + '</td>';
                htmlstr += '<td class="a">' + sr[o].PURCHASE_QTY + '</td>';
                htmlstr += '<td class="a">' + sr[o].PURCHASE_UNITPRICE + '</td>';
                htmlstr += '<td class="a">' + sr[o].QTY_RECEIVED + '</td>';
                htmlstr += '<td class="h">' + sr[o].SUPPLIERDESC + '</td>';
                htmlstr += "</tr>";


            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.tbody).append(htmlstr);
        }


        ah.displaySearchResult();

    };

    sortArray = function () {

    }

    self.readSetupID = function () {

        var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        //postData = { SUPPLIER_ID: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        //$.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.dateClick = function () {
        $("#dateClickAction").val("ACTIVE");
    };
    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();

        var fromDate = $('#requestFromDate').val();

        fromDate = fromDate.split('/');
        if (fromDate.length == 3) {
            self.searchFilter.fromDate(fromDate[2] + "/" + fromDate[1] + "/" + fromDate[0]);
        }
        var toDate = $('#requestStartDate').val().split('/');
        if (toDate.length == 3) {
            self.searchFilter.toDate(toDate[2] + "/" + toDate[1] + "/" + toDate[0] + "  23:00:00.000");
        }
        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), SEARCH_FILTER: self.searchFilter, RETACTION: "", STAFF_LOGON_SESSIONS: staffLogonSessions };

        //alert(JSON.stringify(postData));
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };


    self.webApiCallbackStatus = function (jsonData) {

        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));


        }
        else if (jsonData.PURCHASEREQ_REPTLIST || jsonData.STAFF || jsonData.SUCCESS) {
            if (ah.CurrentMode == ah.config.mode.search) {

                self.searchResult(jsonData);
            }


        }
        else {

            alert(systemText.errorTwo);
            //	window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};