﻿var webApiURL = sessionStorage.getItem('webApiURL');
var STAFF_LOGON_SESSIONS = "STAFF_LOGON_SESSIONS";
var STAFF = "STAFF";
var GROUP_PRIVILEGES = "GROUP_PRIVILEGES";
var loginModalId = "authenticationModalSession";
var oldStaffSession;
var authenticationCheckerEncryptPassword = function (as_toencrypt, as_key1, as_key2) {
    var ls_ret = "";
    var ls_key = "";
    var li_encrypt_pos = 0;
    var li_key_pos = 0;
    var li_encrypt_len = 0;
    var li_key_len = 0;
    var li_key_asc;
    var li_encrypt_asc;

    as_toencrypt = as_toencrypt.toUpperCase();

    li_encrypt_len = as_toencrypt.length;

    ls_key = as_key1 + as_key2;
    li_key_len = ls_key.length;
    var ls_encrypt_bin = ""
    var ls_key_bin = "";
    var bytes = [];
    var iix = 1;

    for (var i = 0; i < as_toencrypt.length; ++i) {
        var res = as_key1.slice(-iix);
        iix++;
        var restcode = res.slice(0, 1);
        var keystr = (as_toencrypt.charCodeAt(i)).toString();

        var keyInt = keystr * 2;
        keystr = String.fromCharCode(keyInt);

        ls_key_bin += keystr;
        bytes.push(ls_key_bin);
    }
    return ls_key_bin;
};
var showModalAuthenticationChecker = function () {
    if ($('#' + loginModalId).length) return;

    var loginModal = [
        '<div id="' + loginModalId + '" style="position: fixed; font-family: Arial, Helvetica, sans-serif; top: 0; right: 0; bottom: 0; left: 0; background: rgba(0, 0, 0, 0.8); z-index: 999999; opacity: 0; -webkit-transition: opacity 400ms ease-in; -moz-transition: opacity 400ms ease-in; transition: opacity 400ms ease-in;">',
        '<div style="width: 410px; height: 230px; position: relative; margin: 4% auto; border-radius: 10px; background: #fff;">',
        '<form id="authenticationModalSessionForm" style="display: flex; flex-direction: column; align-items: center; justify-content: center; height: 100%; margin: 0 50px">',
        '<h2 style="font-weight: bold">Please Sign-in</h2><br>',
        '<input type="text" id="authenticationModalSessionUserId" disabled="disabled" style="border-bottom: 2px solid #e4e4e4;border-top: none;border-left: none;border-right: none;background-color: #efefef;font-size: 16px;height: 30px;width: 100%;text-align: center;" ><br>',
        '<input type="password" id="authenticationModalSessionPassword" placeholder="Password Here" required="required" style="border-bottom: 2px solid #e4e4e4;border-top: none;border-left: none;border-right: none;background-color: #efefef;font-size: 16px;height: 30px;width: 100%;text-align: center;"><br>',
        '<div style="display: flex; justify-content: space-between; width: 100%;">',
        '<button type="submit" class="button button-block" style="padding: 0">',
        '<span id="authenticationModalSessionSpinner"><i class="fa fa-cog fa-spin fa-lg"></i> Please wait . . .</span>',
        '<span id="authenticationModalSessionSignIn">Sign In</span>',
        '</button>',
        '<div style="width: 10%"></div>',
        '<a href="' + window.location.origin + '" class="button button-block" style="padding: 0; display: flex; align-items: center; justify-content: center">Back To Log in</button>',
        '</div>',
        '</form>',
        '</div>',
        '</div>'
    ].join("");
    $('body').append(loginModal);
    $('#' + loginModalId + ' #authenticationModalSessionSpinner').css('display', 'none');
    $('#' + loginModalId + ' #authenticationModalSessionSignIn').css('display', 'block');
    setTimeout(function () {
        $('#' + loginModalId).css({
            'opacity': 1
        });
    }, 0);

    $('#authenticationModalSessionForm').submit(function (e) {
        e.preventDefault();
        var logonCredentials = {
            USER_ID: oldStaffSession.USER_ID,
            PASSWORD: $('#' + loginModalId + ' #authenticationModalSessionPassword').val(),
            EMAIL_ADDRESS: webApiURL
        }
        var xkdjedkxisdde = authenticationCheckerEncryptPassword(logonCredentials.PASSWORD, webApiURL, logonCredentials.USER_ID);
        xkdjedkxisdde = xkdjedkxisdde.toString();
        var postData = { MOREXDK: xkdjedkxisdde, STAFF: logonCredentials };
        $('#' + loginModalId + ' #authenticationModalSessionSpinner').css('display', 'block');
        $('#' + loginModalId + ' #authenticationModalSessionSignIn').css('display', 'none');
        $.post(webApiURL + 'api/Security/AuthenticateUser', postData).done(
            function (jsonData) {
                if (jsonData.ERROR) {
                    if (jsonData.ERROR.ErrorText)
                        alert(jsonData.ERROR.ErrorText);
                    else
                        alert('System Error');
                }
                else if (jsonData.STAFF_LOGON_SESSIONS && jsonData.STAFF && jsonData.GROUP_PRIVILEGES) {
                    sessionStorage.setItem(STAFF_LOGON_SESSIONS, JSON.stringify(jsonData.STAFF_LOGON_SESSIONS));
                    sessionStorage.setItem(STAFF, JSON.stringify(jsonData.STAFF));
                    sessionStorage.setItem(GROUP_PRIVILEGES, JSON.stringify(jsonData.GROUP_PRIVILEGES));
                    removeModalAuthenticationChecker();
                }
                $('#' + loginModalId + ' #authenticationModalSessionPassword').val('');
            }).fail(function (jsonData) {
                alert('System Error');
            });
    });
}
var removeModalAuthenticationChecker = function () {
    $('#' + loginModalId).css({
        'opacity': 0
    });
    setTimeout(function () {
        $('#' + loginModalId).remove();
    }, 400);
}
var authenticationChecker = setInterval(function () {
    var staffSession = JSON.parse(sessionStorage.getItem(STAFF_LOGON_SESSIONS));
    if (staffSession && oldStaffSession && staffSession.USER_ID != oldStaffSession.USER_ID) {
        showModalAuthenticationChecker();
        $('#' + loginModalId + ' #authenticationModalSessionUserId').val(oldStaffSession.USER_ID);
        $('#' + loginModalId + ' #authenticationModalSessionUserId').prop("disabled", true);
        $('#' + loginModalId + ' #authenticationModalSessionUserId').prop("readonly", true);
        $('#' + loginModalId + ' #authenticationModalSessionUserId').removeClass();
        $('#' + loginModalId + ' #authenticationModalSessionPassword').prop("disabled", false);
        $('#' + loginModalId + ' #authenticationModalSessionPassword').prop("readonly", false);
        $('#' + loginModalId + ' #authenticationModalSessionPassword').removeClass();

        return;
    }
    if (staffSession) {
        oldStaffSession = staffSession;
    }

    if (!staffSession) {
        showModalAuthenticationChecker();
        $('#' + loginModalId + ' #authenticationModalSessionUserId').val(oldStaffSession.USER_ID);
        $('#' + loginModalId + ' #authenticationModalSessionUserId').prop("disabled", true);
        $('#' + loginModalId + ' #authenticationModalSessionUserId').prop("readonly", true);
        $('#' + loginModalId + ' #authenticationModalSessionUserId').removeClass();
        $('#' + loginModalId + ' #authenticationModalSessionPassword').prop("disabled", false);
        $('#' + loginModalId + ' #authenticationModalSessionPassword').prop("readonly", false);
        $('#' + loginModalId + ' #authenticationModalSessionPassword').removeClass();
    }
}, 400);