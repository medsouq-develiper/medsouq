﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            detailItem: '.detailItem',
            modalBox: '.modalBox',
            modalwidth3: '.modalwidth3'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul',
            table: 'table'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            txtGlobalSearchOth: '#txtGlobalSearchOth',
            frmInfo: '#frmInfo',
            frmitemInfo: '#itemInfo',
            storeCURR: '#STORE',
            supplierCURR: '#SUPPLIER',
            staffID: '#STAFF_ID',
           
            populateData: '#populateData',
            successMessage: '#successMessage',
            itemInfo: '#itemInfo',
            initialTable: '#initialTable',
            itemDescription: '#itemDescription',
            
            infoProductDesc: '#INFOPRODUCT_DESC',
            infoUom: '#INFOUOM',
            addButton: '#addButton',
            updateButton: '#updateButton'
        },
        tagId: {
            page1: 'page1',
            page2: 'page2',
            priorPage: 'priorPage',
            nextPage: 'nextPage',
            txtGlobalSearchOth: 'txtGlobalSearchOth'

        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID',
            dataItems: 'data-items'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'searchResult',
            storeDetails: 'STORES',
            productItems: 'PRODUCT_STOCKONHAND',
            productStockBatch: 'PRODUCT_STOCKBATCH',
            productStockBatchPush: 'PRODUCT_STOCKBATCHPUSH'
        },
        model: {

        },
        url: {
            homeIndex: '/Home/Index',
            
            modalInfo: '/SystemApps/systemDictionary#openModalInfo',
            modalClose: '/SystemApps/systemDictionary#'
        },
        api: {

            readDictionary: '/SystemApps/ReadDictionary',
            searchAll: '/SystemApps/SearchSystemDictionary',
            searchLookUps: '/Inventory/SearchStockBatchRecCatLov',
            searchStockItems: '/Inventory/SearchStockItems',
            createDictionary: '/SystemApps/CreateNewDictionary',
            updateSetup: '/Inventory/UpdateStoreDetails'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchLOVRowHeaderTemplate: "<a href='#openModalInfo' class='fs-medium-normal productDetailItem fc-greendark' data-items='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} <span>{3}</span></a>",
            searchExistRowHeaderTemplate: "<a href='#openModalInfo' class='fs-medium-normal productDetailItem fc-slate-blue' data-items='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} <span class='fc-green'> {3} </span></a>",
            searchDeleteRowHeaderTemplate: "<a href='#'onclick='deleteRow(this)' ><i class='fa fa-trash'></a>",
            searchRowTemplate: '<span class="search-row-label">{0} <span class="search-row-data">{1}</span></span>'
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        //$(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.id.contentHeader
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };
    thisApp.toggleAddItems = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;


        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };
    thisApp.toggledisplayItemInfo = function () {

        $(thisApp.config.cls.modalBox).show();

    };
    thisApp.toggleSearchItems = function () {

        $(thisApp.config.cls.modalBox).hide();

        $(
            thisApp.config.cls.modalwidth3).show();

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatuxText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentField
        ).hide();

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        //$(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        //$(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        //$(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
              thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.contentField
        ).show();

        $(
           thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

       

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };


    thisApp.formatJSONDateToString = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };
    thisApp.formatJSONDateToStringOth = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[0] + '-' + strDate[1] + '-' + strDate[2].substring(0, 2);


        return dateToFormat;
    };
    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name),
                type = $el.attr('type');

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var systemDictionaryViewModel = function (systemText) {
    var self = this;
    var ah = new appHelper(systemText);


    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.toggleReadMode();

        ah.initializeLayout();
        ah.toggleDisplayMode();
       
        var postData;
        postData = { SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
    };



    //ko.applyBindings(new PagedGridModel(initialData));

    self.dictionaryList = ko.observableArray("");
    

    self.select = function (dictionaryList) {
        self.selected(dictionaryList);
    };
    self.selected = ko.observable(self.dictionaryList()[0]);


    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.createNew = function () {
        $("#LASTACTION").val("");
        window.location.href = ah.config.url.modalInfo;

    };

   

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.saveAllChanges = function () {

        $(ah.config.id.saveInfo).trigger('click');

    };
    self.postAllChanges = function () {
        $("#POSTID").val("POST");
        $(ah.config.id.saveInfo).trigger('click');

    };

    self.saveupdateItemInfo = function (dictionaryList) {
        var sr = ah.ConvertFormToJSON($(ah.config.id.frmitemInfo)[0]);

        if (sr.currDESCRIPTION === null || sr.currDESCRIPTION === "") {
            alert('Please provide English to proceed')
            return;
        }
      
        var lastAction = $("#LASTACTION").val();
        if (lastAction === "INFO") {
            var delRow = parseInt($("#delRow").val());

            var todelrow = parseInt(delRow) + 1;;
            table = document.getElementById("itemsReceiveBatch");

            table.deleteRow(todelrow);
            self.dictionaryList.splice(delRow, 1);
            $("#delRow").val("");
        }
        
        alert(sr.currDICTIONARY_ID);
        if (sr.currDICTIONARY_ID === "" || sr.currDICTIONARY_ID === null) {
            sr.currDICTIONARY_ID = "0";
        }
        self.dictionaryList.push({
            DESCRIPTION: sr.currDESCRIPTION,
            DESCRIPTION_OTH: sr.currDESCRIPTION_OTH,
            REMARKS: sr.currREMARKS,
            DICTIONARY_ACTION: "PUSH",
            DICTIONARY_ID: sr.currDICTIONARY_ID
        });
        window.location.href = ah.config.url.modalClose;
        self.saveInfo();
    };

    self.displayDetail = function (dictionaryList) {
        var i = self.dictionaryList.indexOf(dictionaryList);
        $("#delRow").val(i);
        self.selected(dictionaryList);
        $("#currDICTIONARY_ID").val(self.selected().DICTIONARY_ID);
        $("#currDESCRIPTION").val(self.selected().DESCRIPTION);
        $("#currDESCRIPTION_OTH").val(self.selected().DESCRIPTION_OTH);
        $("#currREMARKS").val(self.selected().REMARKS);
        $("#LASTACTION").val("INFO");
        window.location.href = ah.config.url.modalInfo;
    };

    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            self.showDebtorDetails();
        }

    };

    self.showDetails = function (jsonData) {

        
        var dictionaryDetail = jsonData.SYSTEM_DICTIONARY;

            var jsetupDetails = JSON.parse(dictionaryDetail);

            ah.ResetControls();
            ah.LoadJSON(jsetupDetails);
     

        ah.toggleDisplayMode();

    };

    self.searchResult = function (jsonData) {


        var sr = jsonData.PRODUCT_STOCKBATCH;


        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {
                var transDate = new Date(sr[o].TRANS_DATE);
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].TRANS_ID, 'Trans.ID ' + sr[o].TRANS_ID, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;         Trans. Date: ' + transDate.toDateString().substring(4, 15), ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      Total Items ' + sr[o].NOITEMS);

                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.detailItem).bind('click', self.readtransID);
        ah.displaySearchResult();

    };

    self.searchResultItems = function (jsonData) {
        ah.toggleReadMode();

        ah.initializeLayout();
        ah.toggleAddMode();


        ah.toggledisplayItemInfo();
        self.dictionaryList.removeAll();
        var sr = jsonData.SYSTEM_DICTIONARY;

        if (sr.length > 0) {

            for (var o in sr) {

                self.dictionaryList.push({
                    DESCRIPTION: sr[o].DESCRIPTION,
                    DESCRIPTION_OTH: sr[o].DESCRIPTION_OTH,
                    REMARKS: sr[o].REMARKS,
                    DICTIONARY_ID: sr[o].DICTIONARY_ID

                });
            }

        }

    };

  
    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();

        postData = { CATEGORY_ID: "", STATUS: "", SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };

    self.saveInfo = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var itemDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.dictionaryList);


        var updatedLists = JSON.parse(jsonObj);
       
        
        var mf = updatedLists.filter(function (item) { return item.DICTIONARY_ACTION === "PUSH" });
        postData = { SYSTEM_DICTIONARY: mf, STAFF_LOGON_SESSIONS: staffLogonSessions };
       // alert(JSON.stringify(postData));
       
        
         //   ah.toggleSaveMode();
            $.post(self.getApi() + ah.config.api.createDictionary, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        
    };

    self.webApiCallbackStatus = function (jsonData) {
        //alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            //window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.SYSTEM_DICTIONARY || jsonData.STAFF || jsonData.SUCCESS) {

            if (ah.CurrentMode == ah.config.mode.save) {
                //sessionStorage.setItem(ah.config.skey.storeDetails, JSON.stringify(jsonData.STORES));
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();

                //if (jsonData.SUCCESS) {
                //    self.showDetails(false);
                //}
                //else {
                //    self.showDetails(true);
                //}
            }
            else if (ah.CurrentMode == ah.config.mode.edit) {

                self.showDetails(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
                if (jsonData.SYSTEM_DICTIONARY) {
                    self.searchResultItems(jsonData);

                } else {


                }

            }
            else if (ah.CurrentMode == ah.config.mode.idle) {
                

            }

        }
        else {

            alert(systemText.errorTwo);
            //window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};