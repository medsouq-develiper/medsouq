﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            detailParts: '.detailParts',
            statusText: '.status-text',
            detailItem: '.detailItem',
            closeItem: '.closeItem'
        },
        tag: {

            textArea: 'textarea',
            select: 'select',
            tbody: 'tbody',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7
        },
        tagId: {
            tcCode: 'tcCode',
            tcInService: 'tcInService',
            tcDescription: 'tcDescription',
            tcActive: 'tcActive',
            tcMissing: 'tcMissing',
            tcInActive: 'tcInActive',
            tcOtService: 'tcOtService',
            tcRetired: 'tcRetired',
            tcTransfer: 'tcTransfer',
            tcUndifined: 'tcUndifined'
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            searchResultStaffList: '#searchResultStaffList',
            staffID: '#STAFF_ID',
            serviceID: '#SERVICE_ID',
            successMessage: '#successMessage',
            fromToDate: '#fromToDate',
            searchResultAssignTypeList: '#searchResultAssignTypeList',
            searchResultWOList: '#searchResultWOList'
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'AM_ASSETSUMMARY',

        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalClose: '#',
            modalFilter: '#openModalFilter',
            openModalWO: '#openModalWO'
        },
        api: {

            updateWOBulk: '/AIMS/UpdateWorkOrderBulk',
            searchJobType: '/Reports/SearchJobTypeRpt',
            searchCreatedBy: '/Reports/SearchEngrCreatedRept',
            searchAssignTo: '/Reports/SearchAssignToRept',
            searchWOAssign: '/Reports/SearchWorkOrderAssign',
            searchAll: '/Reports/SearchWorkOrderByResponsibleStaff'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
            searchRowTemplateWO: "<span class='search-row - label spareParts  detailItem' data-infoID='{0}'> <span>{1}</span></span>",
            searchRowTemplateWOClose: "<span class='search-row - label closeWO  closeItem' data-infoID='{0}'> <span>{1}</span></span>",
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };
    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };
    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();

    };
    thisApp.formatJSONDateTimeToString = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0] + ' ' + strTime[1].substring(0, 5);


        return dateToFormat;
    };
    thisApp.formatJSONYearDateTimeToString = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[0] + '/' + strDate[1] + '/' + strDate[2].substring(0, 2) + ' ' + strTime[1].substring(0, 5);


        return dateToFormat;
    };
    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave).hide();

    };
    thisApp.formatJSONDateToString = function () {

        var dateToFormat = arguments[0];
        var strDate;

        if (dateToFormat === null) return "";
        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };
    thisApp.ConvertFormToJSON = function (form) {
        $(form).find("input:disabled").removeAttr("disabled");
        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name),
                type = $el.attr('type');

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var woResponsibleStaffViewModel = function (systemText) {

    var self = this;

    var ah = new appHelper(systemText);
    var staffList = ko.observableArray([]);
    var woList = ko.observableArray([]);
    var woToPush = [];
    self.woData = ko.observableArray([]);
    self.searchFilter = {
        fromDate: ko.observable(''),
        toDate: ko.observable(''),
        status: ko.observable(''),
        problemType: ko.observable(''),
        priority: ko.observable(''),
        jobType: ko.observable(''),
        userConstCenter: ko.observable(''),
        woIdList: ko.observable(''),
        assignTo: ko.observable(''),
        assignType: ko.observable('')

    }
    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());



        ah.initializeLayout();
        self.searchNow();
    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.closeModal = function () {
        $("#MODIFYACTION").val("EMPTY");
        window.location.href = ah.config.url.modalClose;
    };
    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.createNew = function () {

        ah.toggleAddMode();

    };
    self.searchFilterNow = function () {
        window.location.href = ah.config.url.modalClose;
        self.searchNow();
    }
    self.showModalFilter = function () {
        window.location.href = ah.config.url.modalFilter;
    };
    self.clearFilter = function () {

        $('#requestFromDate').val('');
        $('#requestStartDate').val('');

    }
    self.modifySetup = function () {

        ah.toggleEditMode();

    };
    self.woModified = function (itemData, indx) {
        var woLoaded = $("#WOLOADED").val();
        if (woLoaded == 'Y') {
            ko.utils.arrayFilter(self.woData(), function (item) {
                if (item.REQ_NO === itemData.REQ_NO) {
                    item.TOPUSH = 'Y';
                }
            });
            //if (checkDate) {
            //    self.alertMessage("Unable ro proceed. Close date cannot be future date.");
            //    return;
            //}
            if (itemData.WO_STATUS == "CL") {
                var jDate = new Date();
                var strDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();
                function time_format(d) {
                    hours = format_two_digits(d.getHours());
                    minutes = format_two_digits(d.getMinutes());
                    return hours + ":" + minutes;
                }
                function format_two_digits(n) {
                    return n < 10 ? '0' + n : n;
                }
                var hourmin = time_format(jDate);

                var closedDate = strDate + ' ' + hourmin;
                itemData.CLOSED_DATE == closedDate;
            }
        }
  
    };
    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };
    self.filterWOSearch = function () {
        var filterSearch = $("#lookUptxtGlobalSearchOth").val();
        self.searchWOResult('1', '100', filterSearch);
    };
    self.searchWo = function () {
        $("#BTNACTION").val("ASSIGN");
        var reqID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfoId).toString());
       
        var reqID = infoID.ASSIGN_TO;
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;
        if (reqID == 'Unassigned') {
            $("#winTitleAssignName").text(infoID.ASSIGN_NAME);
            self.searchFilter.assignTo = null;
        } else {
            self.searchFilter.assignTo = reqID;
            $("#winTitleAssignName").text("Assigned To: " + infoID.ASSIGN_NAME);
        }
        $("#MODIFYACTION").val("MODIFY");
        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), SEARCH_FILTER: self.searchFilter,  STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchWOAssign, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.searchCloseWo = function () {
        $("#BTNACTION").val("CLOSE");
        var reqID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfoId).toString());

        var reqID = infoID.ASSIGN_TO;
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;
        if (reqID == 'Unassigned') {
            $("#winTitleAssignName").text(infoID.ASSIGN_NAME);
            self.searchFilter.assignTo = null;
        } else {
            self.searchFilter.assignTo = reqID;
            $("#winTitleAssignName").text("Assigned To: " + infoID.ASSIGN_NAME);
        }
        $("#MODIFYACTION").val("MODIFY");
        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), SEARCH_FILTER: self.searchFilter, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchWOAssign, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.sortTable = function () {


        var senderID = $(arguments[1].currentTarget).attr('id');

        switch (senderID) {


            case ah.config.tagId.tcInService: {
                self.searchResult('WO_FA');
                break;
            }
            case ah.config.tagId.tcDescription: {
                self.searchResult('ASSIGN_NAME');
                break;
            }
            case ah.config.tagId.tcActive: {
                self.searchResult('WO_COUNT');
                break;
            }
           


        }


    };
    self.privRow = function () {
        var rowNext = $("#NEXTROW").val();
        var privRow = $("#PRIVROW").val();
        var backRow = parseInt(privRow) - 100;
        rowNext = parseInt(rowNext) - 100;

        if (backRow < 0) {
            backRow = 0;
        }

        if (rowNext <= 100) {
            rowNext = 100;
        }
        var filterSearch = $("#lookUptxtGlobalSearchOth").val();

        self.searchWOResult(backRow, rowNext, filterSearch);
    };
    self.nextRow = function () {
        var rowNext = $("#NEXTROW").val();
        $("#PRIVROW").val(rowNext);
        var addRow = parseInt(rowNext) + 100;
        var filterSearch = $("#lookUptxtGlobalSearchOth").val();

        self.searchWOResult(rowNext, addRow, filterSearch);
    };
    self.searchResult = function (sortDet) {
        $(ah.config.id.searchResultWOList).hide();
        $(ah.config.id.searchResultList).show();
        $(ah.config.id.searchResultStaffList).hide();
        $(ah.config.id.searchResultAssignTypeList).hide();
        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.searchResult));

        $(ah.config.id.searchResultList + ' ' + ah.config.tag.tbody).html('');

       

        if (sortDet === 'ASSIGN_NAME') {
            sr.sort(function (a, b) {

                if (a[sortDet] < b[sortDet])
                    return -1;
                if (a[sortDet] > b[sortDet])
                    return 1;
                return 0;
            });
        } else {
            sr.sort(function (a, b) {

                if (a[sortDet] > b[sortDet])
                    return -1;
                if (a[sortDet] < b[sortDet])
                    return 1;
                return 0;
            });
        }




        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {
                htmlstr += "<tr>";
               
                if (sr[o].ASSIGN_NAME === null || sr[o].ASSIGN_NAME === "") {
                    sr[o].ASSIGN_NAME = 'Unassigned';
                    sr[o].ASSIGN_TO = 'Unassigned';
                }
                htmlstr += '<td class="d">' + sr[o].ASSIGN_TO + '</td>';
                htmlstr += '<td class="h">' + sr[o].ASSIGN_NAME + '</td>';
                htmlstr += '<td class="a">' + sr[o].WO_COUNT + '</td>';
                htmlstr += '<td>' + ah.formatString(ah.config.html.searchRowTemplateWO, JSON.stringify(sr[o]), 'Assign/ReAssign') + '</td>';
                if (sr[o].ASSIGN_TO !== "Unassigned") {
                    htmlstr += '<td>' + ah.formatString(ah.config.html.searchRowTemplateWOClose, JSON.stringify(sr[o]), 'Update Status') + '</td>';
                }
               
                htmlstr += "</tr>";


            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.tbody).append(htmlstr);

            $(ah.config.cls.detailItem).bind('click', self.searchWo);
            $(ah.config.cls.closeItem).bind('click', self.searchCloseWo);
        }


        ah.displaySearchResult();
        var modifyACtion = $("#MODIFYACTION").val();
        if (modifyACtion === 'MODIFY') {
            window.location.href = ah.config.url.openModalWO;
        }
        
    };
    
    self.searchWOResult = function (privRow,nextRow, filterSearch) {
        // || (sr[i].PROBTYPEDESC.toUpperCase()).indexOf(keywords) > -1 || (sr[i].REQ_NO.toUpperCase()).indexOf(keywords) > -1 || (sr[i].COSTCENTER_NAME.toUpperCase()).indexOf(keywords) > -1 || (sr[i].PROBLEM.toUpperCase()).indexOf(keywords) > -1
        $("#WOLOADED").val('N');
        var sr = woList;
       
        if (filterSearch !== null && filterSearch !== "") {
           
            //var keywords = filterSearch.toUpperCase();
            //sr = sr.filter(function (item) { return (item.ASSET_NAME.toUpperCase()).indexOf(keywords) > -1 });
           
            //var arr = sr;
            var keywords = filterSearch.toUpperCase();
            var filtered = [];
            var rowNum = 1;
            for (var i = 0; i < sr.length; i++) {
                

                for (var j = 0; j < keywords.length; j++) {
                    if ((sr[i].ASSET_NAME.toUpperCase()).indexOf(keywords) > -1 || (sr[i].PROBTYPEDESC.toUpperCase()).indexOf(keywords) > -1 || (sr[i].REQ_NO.toUpperCase()).indexOf(keywords) > -1 || (sr[i].COSTCENTER_NAME.toUpperCase()).indexOf(keywords) > -1) {
                        sr[i].ROWNUMID = rowNum;
                        filtered.push(sr[i]);
                        
                        rowNum++
                        break;
                    }
                }
            }
            sr = filtered;
        } else {
            
        }
        sr = sr.filter(function (item) { return item.ROWNUMID >= privRow && item.ROWNUMID <= nextRow });

        

        if (sr.length > 0) {
            $("#thStatus").hide();
            $("#thCloseDate").hide();
            $("#thAssignTo").hide();
            $("#thAssignDate").hide();
            
            var btnAction = $("#BTNACTION").val();
            if (btnAction == 'CLOSE') {
                $("#thStatus").show();
                $("#thCloseDate").show();

            } else {
                $("#thAssignTo").show();
                $("#thAssignDate").show();
            }

            self.woData.splice(0, 5000);
            $('#ASSIGNTO').empty();

            var srreload = staffList;

            var srreload = srreload.filter(function (item) { return (item.STATUS === "ACTIVE" && item.SYSTEM_LOOKUP == "Y") || item.STAFF_ID == sr[0].ASSIGN_TO });

            var rowCount = 0;
           // alert(JSON.stringify(staffList));
            for (var o in sr) {
                if (o == 0) {
                    $("#PRIVROW").val(sr[o].ROWNUMID);
                }
              
                rowCount++
                var reqDate = ah.formatJSONDateToString(sr[o].REQ_DATE);
                var assignDate = ah.formatJSONDateToString(sr[o].ASSIGN_DATE);
                self.woData.push({
                    TOPUSH: 'N',
                    ROWCOUNT: sr[o].ROWNUMID,
                    REQ_NO: sr[o].REQ_NO,
                    REQ_DATE: reqDate,
                    PROBTYPEDESC: sr[o].PROBTYPEDESC,
                    PROBLEM: sr[o].PROBLEM,
                    ASSET_NAME: sr[o].ASSET_NAME,
                    COSTCENTER_NAME: sr[o].COSTCENTER_NAME,
                    WO_STATUS: sr[o].WO_STATUS,
                    ASSIGN_TO: sr[o].ASSIGN_TO,
                    ASSIGN_DATE: assignDate,
                    staffLOV: srreload,
                    CLOSED_DATE: null,
                    DISPLAY_CLOSED_DATE: ko.observable(null),
                    BTNTYPE: btnAction
                   
                   
                });
              
                if (rowCount == sr.length) {
                    $("#NEXTROW").val(sr[o].ROWNUMID);
                    $("#WOLOADED").val('Y');
                }
                if (o == 100) {
                    $("#NEXTROW").val(sr[o].ROWNUMID);
                    $("#WOLOADED").val('Y');
                    break;
                }

                
                

            }
    
        } else {
            self.woData.splice(0, 5000);
        }

        window.location.href = ah.config.url.openModalWO;
       
    };
    sortArray = function () {

    }

    self.readSetupID = function () {

        var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();



    };
    self.dateClick = function () {
        $("#dateClickAction").val("ACTIVE");
    };
    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();
      




        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), SEARCH_FILTER: self.searchFilter, RETACTION: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.isValidDate = function (dateString) {
        // First check for the pattern
        var regex_date = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
        var regex_date2 = /^(\d{1,2})-(\d{1,2})-(\d{4})$/;
        if (!regex_date.test(dateString)) {
            if (!regex_date2.test(dateString)) {
                return 'Invalid';
            }
        }
        // Parse the date parts to integers
        var n = dateString.search("-");
        var parts = "";
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var newDateFormat = "";
        if (n <= 0) {
            parts = dateString.split("/");
            var day = parseInt(parts[0], 10);
            var month = parseInt(parts[1], 10);
            var year = parseInt(parts[2], 10);
            newDateFormat = (year + '/' + format_two_digits(month) + '/' + format_two_digits(day)).trim();
        } else {
            parts = dateString.split("-");
            var day = parseInt(parts[0], 10);
            var month = parseInt(parts[1], 10);
            var year = parseInt(parts[2], 10);
            newDateFormat = (year + '/' + format_two_digits(month) + '/' + format_two_digits(day)).trim();

        }

        // Check the ranges of month and year
        if (year < 1000 || year > 3000 || month == 0 || month > 12) {
            return 'Invalid';
        }

        var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        // Adjust for leap years
        if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
            monthLength[1] = 29;
        }

        // Check the range of the day
        if (day > 0 && day <= monthLength[month - 1]) {
            return newDateFormat;
        } else {
            return 'Invalid';
        }
    };
    self.checkIfCloseStatus = function (data, index) {
        setTimeout(function () {
            if (data.WO_STATUS == 'CL') {
                data.DISPLAY_CLOSED_DATE(moment().format('DD/MM/YYYY HH:mm'));
                data.CLOSED_DATE = moment().format('DD/MM/YYYY HH:mm');
            }
            else {
                data.DISPLAY_CLOSED_DATE(null);
                data.CLOSED_DATE = null;
            }
        }, 0)
    };
    self.checkCloseDate = function (data, index) {
        data, index, self.woData();
        if (!data.DISPLAY_CLOSED_DATE()) return;

        data.CLOSED_DATE = data.DISPLAY_CLOSED_DATE();

        if (moment(data.DISPLAY_CLOSED_DATE(), 'DD/MM/YYYY HH:mm', true).isValid()) {
            if (data.DISPLAY_CLOSED_DATE() > moment().format('DD/MM/YYYY HH:mm')) {
                alert('Cannot enter future date');
                data.DISPLAY_CLOSED_DATE(null);
                data.CLOSED_DATE = null;
            }
        } else {
            alert('Please use corrent format (DD/MM/YYYY HH:mm)');
        }
    };
    self.applyUpdate = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        var jsonObj = ko.observableArray([]);

        jsonObj = ko.utils.stringifyJson(self.woData);


        var woModifiedList = JSON.parse(jsonObj);

        woModifiedList = woModifiedList.filter(function (item) { return item.TOPUSH == 'Y' });

        var jDate = new Date();
        var createDate = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);

        var woToPushList = [];
        var btnAction = $("#BTNACTION").val();
        if (btnAction == 'CLOSE') {
            if (woModifiedList.length) {
                for (var i in woModifiedList) {
                    //createDate = self.isValidDate(woModifiedList[i].CLOSED_DATE);
                    if (moment(woModifiedList[i].CLOSED_DATE, 'DD/MM/YYYY HH:mm', true).isValid())
                        createDate = woModifiedList[i].CLOSED_DATE;
                    else
                        createDate = 'Invalid';

                    if (createDate === 'Invalid') {
                        alert("Record NO. " + woModifiedList[i].ROWCOUNT + " Invalid Date Format. It should be (DD/MM/YYYY HH:mm)")
                        return;
                    }
                    woToPushList.push({ REQ_NO: woModifiedList[i].REQ_NO, WO_STATUS: woModifiedList[i].WO_STATUS, CLOSED_DATE: moment(createDate, 'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm') });

                }

            }
        } else {
            if (woModifiedList.length) {
                for (var i in woModifiedList) {
                    if (woModifiedList[i].ASSIGN_DATE != null) {
                        if (woModifiedList[i].ASSIGN_DATE == null || woModifiedList[i].ASSIGN_DATE == "") {
                            woModifiedList[i].ASSIGN_DATE = createDate;
                        } else {
                            createDate = self.isValidDate(woModifiedList[i].ASSIGN_DATE);
                            if (createDate === 'Invalid') {
                                alert("Record NO. " + woModifiedList[i].ROWCOUNT + " Invalid Date Format. It should be (DD/MM/YYYY)")
                                return;
                            }
                        }


                    }
                    woToPushList.push({ REQ_NO: woModifiedList[i].REQ_NO, ASSIGN_TO: woModifiedList[i].ASSIGN_TO, ASSIGN_BY: staffDetails.STAFF_ID, ASSIGN_DATE: moment(createDate, 'DD/MM/YYYY').format('YYYY-MM-DD') });

                }

            }
        }
        if (woToPushList.length == 0) {
            alert('Unable to proceed. No record to be update. ');
            return;
        }
        window.location.href = ah.config.url.modalClose;

        postData = { AM_WORK_ORDER_BULKUPATES: woToPushList, UPDATE_ACTION: btnAction, STAFF_LOGON_SESSIONS: staffLogonSessions };

        $.post(self.getApi() + ah.config.api.updateWOBulk, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.webApiCallbackStatus = function (jsonData) {
        //alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));


        }
        else if (jsonData.STAFFLIST || jsonData.WO_STAFFLIST || jsonData.WO_RESPONSIBLESTAFF || jsonData.STAFF || jsonData.SUCCESS) {
          
            if (ah.CurrentMode == ah.config.mode.search) {
                if (jsonData.WO_STAFFLIST) {
                    
                    self.searchWOResult(jsonData);
                } else if (jsonData.SUCCESS) {
                    $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                    ah.showSavingStatusSuccess();
                    self.searchNow();
                    
                } else {
                    sessionStorage.setItem(ah.config.skey.searchResult, JSON.stringify(jsonData.WO_RESPONSIBLESTAFF));
                    self.searchResult("ASSIGN_NAME");
                }
            } else if (ah.CurrentMode == ah.config.mode.searchResult) {
                if (jsonData.SUCCESS) {
                    $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                    ah.showSavingStatusSuccess();
                    self.searchNow();
                } else {
                    staffList = jsonData.STAFFLIST;
                    woList = jsonData.WO_STAFFLIST;
                    self.searchWOResult('1', '100', '');
                }
                
            }


        }
        else {

            alert(systemText.errorTwo);
            //	window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};