﻿var appHelper = function (systemText) {

	var thisApp = this;

	thisApp.config = {
		cls: {
			contentField: '.field-content-detail',
			contentSearch: '.search-content-detail',
			contentHome: '.home-content-detail',
			liModify: '.liModify',
			liSave: '.liSaveAll',
			liAddNew: '.liAddNew',
			divProgress: '.divProgressScreen',
			liApiStatus: '.liApiStatus',
			notifySuccess: '.notify-success',
			toolBoxRight: '.toolbox-right',
			statusText: '.status-text',
            detailItem: '.detailItem',
            liPrint: '.liPrint'
		},
		tag: {

			textArea: 'textarea',
			select: 'select',
			tbody: 'tbody',
			ul: 'ul'
		},
		mode: {
			idle: 0,
			add: 1,
			search: 2,
			searchResult: 3,
			save: 4,
			read: 5,
			display: 6,
            edit: 7,
            print: 8,
            advanceSearch: 9
		},
		tagId: {
			tcCode: 'tcCode',
            tcDescription: 'tcDescription',
            tcClosed: 'tcClosed',
            tcHeld: 'tcHeld',
            tcNew: 'tcNew',
            tcOpen: 'tcOpen'
		},
		id: {
			spanUser: '#spanUser',
			contentHeader: '#contentHeader',
			contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultPMTypeList: '#searchResultPMTypeList',
			searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            searchResultSummaryCount: "#searchResultSummaryCount",
			txtGlobalSearch: '#txtGlobalSearch',
            searchResultStaffList: '#searchResultStaffList',
			staffID: '#STAFF_ID',
			serviceID: '#SERVICE_ID',
            successMessage: '#successMessage',
            fromToDate: '#fromToDate',
            searchResultAssignTypeList: '#searchResultAssignTypeList',
            searchResultWOList: '#searchResultWOList',
            printSummary: '#printSummary',
            printSearchResultList: "#printSearchResultList",
            costcenterStrList: '#costcenterStrList',
            supplierStrList: '#supplierStrList',
            filterProblemType: '#filterProblemType',
            filterPriority: '#filterPriority',
            filterJobType: '#filterJobType',
            filterAssignTo: '#filterAssignTo',
            loadingBackground: '#loadingBackground'
		},
		cssCls: {
			viewMode: 'view-mode'
		},
		attr: {
			readOnly: 'readonly',
			disabled: 'disabled',
			selectedIndex: 'selectedIndex',
			datainfoId: 'data-infoID'
		},
		skey: {
			staff: 'STAFF',
			webApiUrl: 'webApiURL',
			staffLogonSessions: 'STAFF_LOGON_SESSIONS',
			searchResult: 'AM_ASSETSUMMARY',

		},
		url: {
			homeIndex: '/Home/Index',
			aimsHomepage: '/Menuapps/aimsIndex',
			modalClose: '#',
			modalFilter: '#openModalFilter'
		},
        api: {
            searchPMType: '/Reports/searchPMType',
            searchSupplier: '/Reports/searchSupplier',
            searchAsset: '/Reports/searchAsset',
            searchCostCenter: '/Reports/searchCostCenter',
            searchDeviceType: '/Reports/searchDeviceType',
			searchProblemType: '/Reports/SearchProblemTypeRpt',
            searchJobType: '/Reports/SearchJobTypeRpt',
            searchCreatedBy: '/Reports/SearchEngrCreatedRept',
            searchAssignTo: '/Reports/SearchAssignToRept',
            searchAssignType: '/Reports/SearchAssignTypeRept',
            searchWO: '/Reports/SearchWOTimeRept',
            readClientInfo: '/Setup/ReadClientInfo',
            searchLov: '/AIMS/AimsLOVWOReport',
            getListLOV: '/Search/SearchLOVWOReport'
		},
		html: {
			searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
			searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
		}
	};

	thisApp.CurrentMode = thisApp.config.mode.idle;

	thisApp.ResetControls = function () {

		$(thisApp.config.tag.input).val('');
		$(thisApp.config.tag.textArea).val('');
		$(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

	};

	thisApp.initializeLayout = function () {

		thisApp.ResetControls();

		$(
            thisApp.config.cls.liPrint + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
			thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
			thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess
		).hide();

		$(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

		thisApp.CurrentMode = thisApp.config.mode.idle;

	};

	thisApp.toggleSearchMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.search;

		$(thisApp.config.cls.statusText).text(systemText.searchStatusText);
		$(thisApp.config.cls.liApiStatus).show();

		$(
			thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
			thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
		).hide();

	};
    thisApp.formatJSONDateTimeToString = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0] + ' ' + strTime[1].substring(0, 5);


        return dateToFormat;
    };
    thisApp.formatJSONYearDateTimeToString = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[0] + '/' + strDate[1] + '/' + strDate[2].substring(0, 2) + ' ' + strTime[1].substring(0, 5);


        return dateToFormat;
    };
	thisApp.displaySearchResult = function () {

		thisApp.CurrentMode = thisApp.config.mode.searchResult;

		$(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
		$(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave).hide();

	};

	thisApp.ConvertFormToJSON = function (form) {
		$(form).find("input:disabled").removeAttr("disabled");
		var array = jQuery(form).serializeArray();
		var json = {};

		jQuery.each(array, function () {
			json[this.name] = this.value || '';
		});

		return json;

	};

	thisApp.formatString = function () {

		var stringToFormat = arguments[0];

		for (var i = 0; i <= arguments.length - 2; i++) {
			stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
		}

		return stringToFormat;
	};

	thisApp.LoadJSON = function (jsonData) {

		$.each(jsonData, function (name, val) {
			var $el = $('#' + name),
				type = $el.attr('type');

			switch (type) {
				case 'checkbox':
					$el.attr('checked', 'checked');
					break;
				case 'radio':
					$el.filter('[value="' + val + '"]').attr('checked', 'checked');
					break;
				default:
					var tagNameLCase = String($el.prop('tagName')).toLowerCase();
					if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
						$el.text(val);
					} else {
						$el.val(val);
					}
			}
		});

	};

};

/*Knockout MVVM Namespace - Controls form functionality*/
var woReptSummaryViewModel = function (systemText) {

	var self = this;

    var ah = new appHelper(systemText);
    self.srDataArray = ko.observableArray([]);
    var lovCostCenterArray = ko.observableArray([]);
    var lovSupplierArray = ko.observableArray([]);

    self.assetCategory = {
        title: ko.observable(), 
        count: ko.observable()
    };
    self.clientDetails = {
        CLIENTNAME: ko.observable(),
        CLIENT_LOGO: ko.observable(),
        COMPANY_LOGO: ko.observable(),
        HEADING: ko.observable(),
        HEADING2: ko.observable(),
        HEADING3: ko.observable(),
        HEADING_OTH: ko.observable(),
        HEADING_OTH2: ko.observable(),
        HEADING_OTH3: ko.observable(),
        CLIENTADDRESS: ko.observable(),
        CLIENTCONTACT: ko.observable()
    };
	self.searchFilter = {
		fromDate: ko.observable(''),
		toDate: ko.observable(''),
        CostCenterStr: ko.observable(''),
        SupplierStr: ko.observable(''),
        problemType: ko.observable(''),
        pmtype: ko.observable(''),
        status: ko.observable(''),
        priority: ko.observable(''),
        jobType: ko.observable(''),
        assignTo: ko.observable(''),
        assignType: ko.observable('')
    }
    self.clearFilter = function () {
        $('#requestFromDate').val('');
        $('#requestStartDate').val('');
        self.searchFilter.problemType('');
        self.searchFilter.CostCenterStr('');
        self.searchFilter.SupplierStr('');
        self.searchFilter.problemType('');
        self.searchFilter.pmtype('');
        self.searchFilter.status('');
        self.searchFilter.priority('');
        self.searchFilter.jobType('');
        self.searchFilter.assignTo('');
        self.searchFilter.assignType('');
    }
    self.showLoadingBackground = function () {
        var loadingBackground = $(ah.config.id.loadingBackground);
        loadingBackground.addClass('initializing').removeClass('done');
        var offset = loadingBackground.offset();
        var height = loadingBackground.height();
        var centerY = (offset.top + height / 2) - 120;
        $('.sk-circle').css({
            'margin-top': centerY + 'px',
            'display': 'block'
        });
    };
    self.getPageData = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        postData = { LOV: "'AM_JOBTYPE','AIMS_SERVICEDEPARTMENT','AIMS_SPECIALTY','AIMS_PROBLEMTYPE','AIMS_PRIORITY','AIMS_COSTCENTER'", STAFF_LOGON_SESSIONS: staffLogonSessions };
        return $.post(self.getApi() + ah.config.api.getListLOV, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
	self.initialize = function () {

		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;
        
        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

		ah.initializeLayout();
		var today = moment(new Date()).format("DD/MM/YYYY");
        var jDate = new Date();
        var yearArr = jDate.getFullYear();
        var monthArr = ("0" + ((jDate.getMonth() + 1) - 1)).slice(-2);
        if (monthArr === "00") {
            monthArr = "12";
            yearArr = yearArr - 1;
        }
        var strDate = ("0" + jDate.getDate()).slice(-2) + '/' + monthArr + '/' + yearArr;
        $(ah.config.id.fromToDate).text('From Date: ' + strDate + '    To Date: ' + today);
        $('#requestFromDate').val(strDate);
        $('#requestStartDate').val(today);

        self.showLoadingBackground();

        $.when(self.getPageData()).done(function () {
            $(ah.config.id.loadingBackground).removeClass('initializing').addClass('done');
            $('.dark-bg, ' + ah.config.id.loadingBackground).css({
                'top': '90px',
                'z-index': '0'
            });
        });
    };

	self.getApi = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		//alert(JSON.stringify(staffDetails));
		var api = "";
		if (staffDetails.API === null || staffDetails.API === "") {
			api = webApiURL + "api";
		} else {
			api = staffDetails.API + "api";
		}

        return api;
	};

	self.returnHome = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		window.location.href = staffDetails.HOME_DEFAULT;
	};
	self.createNew = function () {

		ah.toggleAddMode();

	};
    self.searchFilterNow = function () {
        window.location.href = ah.config.url.modalClose;
        self.searchNow();
    };
    self.showModalFilter = function () {
		window.location.href = ah.config.url.modalFilter;
	};
	self.modifySetup = function () {

		ah.toggleEditMode();

	};

	self.signOut = function () {

		window.location.href = ah.config.url.homeIndex;

	};
    
    self.printSummary = function () {
        ah.CurrentMode = ah.config.mode.print;
        document.getElementById("printSummary").classList.add('printSummary');
        var jsonObj = ko.observableArray();


        if (self.clientDetails.CLIENTNAME() != null) {
            self.print();

        } else {
            var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var postData;

            var staffInfo = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
            postData = { CLIENTID: staffInfo.CLIENT_CODE, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.readClientInfo, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
    };
    self.print = function () {
        
        setTimeout(function () {
            window.print();
        }, 2000);
    };

    self.sortDataTable = function (tableReference, sortColumnIndex, ascORdesc) {
        $(tableReference)
            .unbind('appendCache applyWidgetId applyWidgets sorton update updateCell')
            .removeClass('tablesorter')
            .find('thead th')
            .unbind('click mousedown')
            .removeClass('header headerSortDown headerSortUp');

        var sortedAt = 0;
        if (ascORdesc == "asc" || ascORdesc == "ASC") {
            sortedAt = 0;
        } else {
            sortedAt = 1;
        }      
        
        $(tableReference).tablesorter({
            sortList: [[sortColumnIndex, sortedAt]]
        });

        $('.not-sorted').removeClass('header').removeClass('headerSortUp').removeClass('headerSortDown');
    }
    self.sortDataTable2 = function (tableReference, sortColumnIndex, ascORdesc) {
        $(tableReference)
            .unbind('appendCache applyWidgetId applyWidgets sorton update updateCell')
            .removeClass('tablesorter')
            .find('thead th')
            .unbind('click mousedown')
            .removeClass('header headerSortDown headerSortUp');

        var sortedAt = 0;
        if (ascORdesc == "asc" || ascORdesc == "ASC") {
            sortedAt = 0;
        } else {
            sortedAt = 1;
        }

        $(tableReference).tablesorter({
            sortList: [[sortColumnIndex, sortedAt]],
            headers: {
                6: { sorter: "customDateWithTime" },
                7: { sorter: "customDateWithTime" }
            }
        });

        $('.not-sorted').removeClass('header').removeClass('headerSortUp').removeClass('headerSortDown');
    }
    self.searchResultPMType = function (jsonData) {
        var sr = self.srDataArray;
        var fromDate = $('#requestFromDate').val();
        var toDate = $('#requestStartDate').val();
        $(ah.config.id.searchResultPMTypeList).show();
        $(ah.config.id.searchResultList).hide();
        $(ah.config.id.searchResultAssignTypeList).hide();
        $(ah.config.id.searchResultStaffList).hide();
        $(ah.config.id.searchResultWOList).hide();
        $(ah.config.id.searchResultPMTypeList + ' ' + ah.config.tag.tbody).html('');

        var searchCat = $("#sumCategory").val();

        systemText.searchModeHeader = "PM Status Summary From:" + fromDate + " To " + toDate;

        $(ah.config.id.searchResultSummaryCount).text("Number of Records: " + sr.length);
        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString()));

        self.assetCategory.title(systemText.searchModeHeader.toString());
        self.assetCategory.count("No. of Records: " + sr.length.toString());

        $(ah.config.cls.liPrint).hide();
        if (sr.length > 0) {
            $(ah.config.cls.liPrint).show();
            var htmlstr = '';

            for (var o in sr) {
                htmlstr += "<tr>";

                if (sr[o].CLOSESTATUS === null) { sr[o].CLOSESTATUS = "0" }
                if (sr[o].HOLDSTATUS === null) { sr[o].HOLDSTATUS = "0" }
                if (sr[o].NEWSTATUS === null) { sr[o].NEWSTATUS = "0" }
                if (sr[o].OPENSTATUS === null) { sr[o].OPENSTATUS = "0" }
                if (sr[o].POSTEDSTATUS === null) { sr[o].POSTEDSTATUS = "0" }
                htmlstr += '<td class="h">' + sr[o].PM_TYPE + '</td>';
                htmlstr += '<td class="a">' + sr[o].CLOSESTATUS + '</td>';

                htmlstr += '<td class="a">' + sr[o].HOLDSTATUS + '</td>';
                htmlstr += '<td class="a">' + sr[o].NEWSTATUS + '</td>';
                htmlstr += '<td class="a">' + sr[o].OPENSTATUS + '</td>';
                //htmlstr += '<td class="a">' + sr[o].POSTEDSTATUS + '</td>';
                htmlstr += "</tr>";
            }

            $(ah.config.id.searchResultPMTypeList + ' ' + ah.config.tag.tbody).append(htmlstr);
            $(ah.config.id.printSearchResultList).html('');
            $(ah.config.id.printSearchResultList).append($(ah.config.id.searchResultPMTypeList).html());

            var tableReference = ah.config.id.searchResultPMTypeList + ' table';
            self.sortDataTable(tableReference, 0, 'asc');
        }
        ah.displaySearchResult();
    };
    self.searchResult = function (sortDet) {
        var fromDate = $('#requestFromDate').val();
        var toDate = $('#requestStartDate').val();
        $(ah.config.id.searchResultPMTypeList).hide();
        $(ah.config.id.searchResultWOList).hide();
        $(ah.config.id.searchResultList).show();
        $(ah.config.id.searchResultStaffList).hide();
        $(ah.config.id.searchResultAssignTypeList).hide();
        var sr = self.srDataArray;
		//var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.searchResult));
		$(ah.config.id.searchResultList + ' ' + ah.config.tag.tbody).html('');

        var searchCat = $("#sumCategory").val();
        var TxtLable = " From " + fromDate + " To " + toDate;
        if (searchCat === "SP") {
            systemText.searchModeHeader =  "Supplier Status Summary" + TxtLable;
        }
        else if (searchCat === "AS") {
            systemText.searchModeHeader = "Asset Status Summary" + TxtLable;
        }
        else if (searchCat === "ASC") {
            systemText.searchModeHeader = "Cost Center Status Summary" + TxtLable;
        }
        else if (searchCat === "DT") {
            systemText.searchModeHeader = "Device Type Status Summary" + TxtLable;
        }
		else if (searchCat === "CC") {
            systemText.searchModeHeader = "Problem Type Status Summary" + TxtLable;
        }
        else if (searchCat === "RC") {
            systemText.searchModeHeader = "Job Type Status Summary" + TxtLable;
        }
        else if (searchCat === "CB") {
            systemText.searchModeHeader = "Created By Status Summary" + TxtLable;
        }

        $(ah.config.id.searchResultSummaryCount).text("Number of Records: " + sr.length);
        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString()));

        self.assetCategory.title(systemText.searchModeHeader.toString());
        self.assetCategory.count("No. of Records: " + sr.length.toString());
        
        $(ah.config.cls.liPrint).hide();
        if (sr.length > 0) {
            $(ah.config.cls.liPrint).show();
            var htmlstr = '';

            for (var o in sr) {
                htmlstr += "<tr>";
                if (searchCat === "SP") {
                    htmlstr += '<td class="d">' + sr[o].SUPPLIER + '</td>';
                } else if (searchCat === "AS") {
                    htmlstr += '<td class="d">' + sr[o].ASSET_NO + '</td>';

                } else if (searchCat === "ASC") {
                    htmlstr += '<td class="d">' + sr[o].COST_CENTER + '</td>';

                } else if (searchCat === "DT") {
                    htmlstr += '<td class="d">' + sr[o].DTYPE + '</td>';

                } else if (searchCat === "CC") {
                    htmlstr += '<td class="d">' + sr[o].REQ_TYPE + '</td>';

                } else if (searchCat === "RC") {
                    htmlstr += '<td class="d">' + sr[o].JOB_TYPE + '</td>';
                }
                if (sr[o].DESCRIPTION === null || sr[o].DESCRIPTION === "") {
                    sr[o].DESCRIPTION = 'Unassigned';
                }
                htmlstr += '<td class="h">' + sr[o].DESCRIPTION + '</td>';
                htmlstr += '<td class="a">' + sr[o].CLOSESTATUS + '</td>';
                htmlstr += '<td class="a">' + sr[o].HOLDSTATUS + '</td>';
                htmlstr += '<td class="a">' + sr[o].NEWSTATUS + '</td>';
                htmlstr += '<td class="a">' + sr[o].OPENSTATUS + '</td>';
                //htmlstr += '<td class="a">' + sr[o].POSTEDSTATUS + '</td>';
                htmlstr += "</tr>";
                
            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.tbody).append(htmlstr);
            $(ah.config.id.printSearchResultList).html('');
            $(ah.config.id.printSearchResultList).append($(ah.config.id.searchResultList).html());

            var tableReference = ah.config.id.searchResultList + ' table';
            self.sortDataTable(tableReference, 1, 'asc', false);
        }
        ah.displaySearchResult();
        
    };
    self.searchStaffResult = function (jsonData) {
        var sr = self.srDataArray;
        var fromDate = $('#requestFromDate').val();
        var toDate = $('#requestStartDate').val();
        //alert(sr.length);
        //var sr = jsonData.ENGRASSIGNTO_STATUSLIST;
        $(ah.config.id.searchResultPMTypeList).hide();
        $(ah.config.id.searchResultWOList).hide();
        $(ah.config.id.searchResultList).hide();
        $(ah.config.id.searchResultAssignTypeList).hide();
        $(ah.config.id.searchResultStaffList).show();
        $(ah.config.id.searchResultStaffList + ' ' + ah.config.tag.tbody).html('');

        var searchCat = $("#sumCategory").val();

         systemText.searchModeHeader = "Created By Status Summary From "+fromDate+" To "+toDate;

         $(ah.config.id.searchResultSummaryCount).text("Number of Records: " + sr.length);
         $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString()));

         self.assetCategory.title(systemText.searchModeHeader.toString());
         self.assetCategory.count("No. of Records: " + sr.length.toString());
        
         $(ah.config.cls.liPrint).hide();
         if (sr.length > 0) {
             $(ah.config.cls.liPrint).show();
            var htmlstr = '';

             for (var o in sr) {

                htmlstr += "<tr>";
                htmlstr += '<td class="d">' + sr[o].CREATED_BY + '</td>';
                 if (sr[o].CRClose === null) { sr[o].CRClose = "0" }
                 if (sr[o].PMClose === null) { sr[o].PMClose = "0" }
                 if (sr[o].CRHold === null) { sr[o].CRHold = "0" }
                 if (sr[o].PMHold === null) { sr[o].PMHold = "0" }
                 if (sr[o].CRNew === null) { sr[o].CRNew = "0" }
                 if (sr[o].PMNew === null) { sr[o].PMNew = "0" }
                 if (sr[o].CROpen === null) { sr[o].CROpen = "0" }
                 if (sr[o].PMOpen === null) { sr[o].PMOpen = "0" }
                 if (sr[o].CMFCLOSE == null) { sr[o].CMFCLOSE = "0" }
                 if (sr[o].PMFCLOSE == null) { sr[o].PMFCLOSE = "0" }
                 if (sr[o].OverDue == null) { sr[o].OverDue = "0" }
                 if (sr[o].OverDueOpen === null) { sr[o].OverDueOpen = "0" }

                 htmlstr += '<td class="h">' + sr[o].CREATEDBY_FULLNAME + '</td>';
                 htmlstr += '<td class="a">' + sr[o].CRClose + '</td>';
                 htmlstr += '<td class="a">' + sr[o].PMClose + '</td>';
                 htmlstr += '<td class="a">' + sr[o].CRHold + '</td>';
                 htmlstr += '<td class="a">' + sr[o].PMHold + '</td>';
                 htmlstr += '<td class="a">' + sr[o].CRNew + '</td>';
                 htmlstr += '<td class="a">' + sr[o].PMNew + '</td>';
                 htmlstr += '<td class="a">' + sr[o].CROpen + '</td>';
                 htmlstr += '<td class="a">' + sr[o].PMOpen + '</td>';
                 htmlstr += '<td class="a">' + sr[o].CMFCLOSE + '</td>';
                 htmlstr += '<td class="a">' + sr[o].PMFCLOSE + '</td>';
                 htmlstr += '<td class="a">' + sr[o].OverDue + '</td>';
                 htmlstr += '<td class="a">' + sr[o].OverDueOpen + '</td>';


                //if (sr[o].CLOSESTATUS === null) { sr[o].CLOSESTATUS = "0" }
                //if (sr[o].HOLDSTATUS === null) { sr[o].HOLDSTATUS = "0" }
                //if (sr[o].NEWSTATUS === null) { sr[o].NEWSTATUS = "0" }
                //if (sr[o].OPENSTATUS === null) { sr[o].OPENSTATUS = "0" }
                //if (sr[o].POSTEDSTATUS === null) { sr[o].POSTEDSTATUS = "0" }

                //htmlstr += '<td class="h">' + sr[o].CREATEDBY_FULLNAME + '</td>';
                //htmlstr += '<td class="a">' + sr[o].CLOSESTATUS + '</td>';

                //htmlstr += '<td class="a">' + sr[o].HOLDSTATUS + '</td>';
                //htmlstr += '<td class="a">' + sr[o].NEWSTATUS + '</td>';
                //htmlstr += '<td class="a">' + sr[o].OPENSTATUS + '</td>';
                //htmlstr += '<td class="a">' + sr[o].POSTEDSTATUS + '</td>';
                htmlstr += "</tr>";
            }

            $(ah.config.id.searchResultStaffList + ' ' + ah.config.tag.tbody).append(htmlstr);
            $(ah.config.id.printSearchResultList).html('');
            $(ah.config.id.printSearchResultList).append($(ah.config.id.searchResultStaffList).html());

            var tableReference = ah.config.id.searchResultStaffList + ' table';
            self.sortDataTable(tableReference, 1, 'asc', false);
        }
        ah.displaySearchResult();

    };
    self.searchAssignToResult = function (jsonData) {
        var sr = self.srDataArray;
        var fromDate = $('#requestFromDate').val();
        var toDate = $('#requestStartDate').val();
        $(ah.config.id.searchResultPMTypeList).hide();
        $(ah.config.id.searchResultList).hide();
        $(ah.config.id.searchResultAssignTypeList).hide();
        $(ah.config.id.searchResultWOList).hide();
        $(ah.config.id.searchResultStaffList).show();
        $(ah.config.id.searchResultStaffList + ' ' + ah.config.tag.tbody).html('');

        var searchCat = $("#sumCategory").val();

        systemText.searchModeHeader = "Work Order Assign To Status Summary From " + fromDate + " To " + toDate;


        $(ah.config.id.searchResultSummaryCount).text("Number of Records: " + sr.length);
        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString()));

        self.assetCategory.title(systemText.searchModeHeader.toString());
        self.assetCategory.count("No. of Records: " + sr.length.toString());
        
        $(ah.config.cls.liPrint).hide();
        if (sr.length > 0) {
            $(ah.config.cls.liPrint).show();
            var htmlstr = '';

            for (var o in sr) {
                htmlstr += "<tr>";
                var staffName = sr[o].STAFF_FULLNAME;

                if (sr[o].STAFF_FULLNAME === null || sr[o].STAFF_FULLNAME === " ") {
                    staffName = 'Unassigned';
                }
                htmlstr += '<td class="d">' + sr[o].ASSIGN_TO + '</td>';
                if (sr[o].CRClose === null) { sr[o].CRClose = "0" }
                if (sr[o].PMClose === null) { sr[o].PMClose = "0" }
                if (sr[o].CRHold === null) { sr[o].CRHold = "0" }
                if (sr[o].PMHold === null) { sr[o].PMHold = "0" }
                if (sr[o].CRNew === null) { sr[o].CRNew = "0" }
                if (sr[o].PMNew === null) { sr[o].PMNew = "0" }
                if (sr[o].CROpen === null) { sr[o].CROpen = "0" }
                if (sr[o].PMOpen === null) { sr[o].PMOpen = "0" }
                if (sr[o].CMFCLOSE == null) { sr[o].CMFCLOSE = "0" }
                if (sr[o].PMFCLOSE == null) { sr[o].PMFCLOSE = "0" }
                if (sr[o].OverDue == null) { sr[o].OverDue = "0" }
                if (sr[o].OverDueOpen === null) { sr[o].OverDueOpen = "0" }

                htmlstr += '<td class="h">' + staffName + '</td>';
                htmlstr += '<td class="a">' + sr[o].CRClose + '</td>';
                htmlstr += '<td class="a">' + sr[o].PMClose + '</td>';
                htmlstr += '<td class="a">' + sr[o].CRHold + '</td>';
                htmlstr += '<td class="a">' + sr[o].PMHold + '</td>';
                htmlstr += '<td class="a">' + sr[o].CRNew + '</td>';
                htmlstr += '<td class="a">' + sr[o].PMNew + '</td>';
                htmlstr += '<td class="a">' + sr[o].CROpen + '</td>';
                htmlstr += '<td class="a">' + sr[o].PMOpen + '</td>';
                htmlstr += '<td class="a">' + sr[o].CMFCLOSE + '</td>';
                htmlstr += '<td class="a">' + sr[o].PMFCLOSE + '</td>';
                htmlstr += '<td class="a">' + sr[o].OverDue + '</td>';
                htmlstr += '<td class="a">' + sr[o].OverDueOpen + '</td>';
                htmlstr += "</tr>";
            }

            $(ah.config.id.searchResultStaffList + ' ' + ah.config.tag.tbody).append(htmlstr);
            $(ah.config.id.printSearchResultList).html('');
            $(ah.config.id.printSearchResultList).append($(ah.config.id.searchResultStaffList).html());

            var tableReference = ah.config.id.searchResultStaffList + ' table';
            self.sortDataTable(tableReference, 1, 'asc', false);
        }
        ah.displaySearchResult();

    };
    self.searchAssignTypeResult = function (jsonData) {
        var sr = self.srDataArray;
        var fromDate = $('#requestFromDate').val();
        var toDate = $('#requestStartDate').val();
        $(ah.config.id.searchResultPMTypeList).hide();
        $(ah.config.id.searchResultList).hide();
        $(ah.config.id.searchResultAssignTypeList).show();
        $(ah.config.id.searchResultStaffList).hide();
        $(ah.config.id.searchResultWOList).hide();
        $(ah.config.id.searchResultAssignTypeList + ' ' + ah.config.tag.tbody).html('');

        var searchCat = $("#sumCategory").val();

        systemText.searchModeHeader = "Assign Person/Company Status Summary From "+ fromDate + " To " + toDate;

        $(ah.config.id.searchResultSummaryCount).text("Number of Records: " + sr.length);
        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString()));

        self.assetCategory.title(systemText.searchModeHeader.toString());
        self.assetCategory.count("No. of Records: " + sr.length.toString());

        $(ah.config.cls.liPrint).hide();
        if (sr.length > 0) {
            $(ah.config.cls.liPrint).show();
            var htmlstr = '';

            for (var o in sr) {
                htmlstr += "<tr>";

                if (sr[o].ASSIGN_TYPE === null || sr[o].ASSIGN_TYPE === " ") {
                    sr[o].ASSIGN_TYPE = 'Unassigned';
                } else if (sr[o].ASSIGN_TYPE === 'COMPANY') {
                    sr[o].ASSIGN_TYPE = 'Outsource';
                } else if (sr[o].ASSIGN_TYPE === 'PERSON') {
                    sr[o].ASSIGN_TYPE = 'Employee';
                } else {
                    sr[o].ASSIGN_TYPE = 'Unassigned';
                }

                if (sr[o].CLOSESTATUS === null) { sr[o].CLOSESTATUS = "0" }
                if (sr[o].HOLDSTATUS === null) { sr[o].HOLDSTATUS = "0" }
                if (sr[o].NEWSTATUS === null) { sr[o].NEWSTATUS = "0" }
                if (sr[o].OPENSTATUS === null) { sr[o].OPENSTATUS = "0" }
                if (sr[o].POSTEDSTATUS === null) { sr[o].POSTEDSTATUS = "0" }

                htmlstr += '<td class="h">' + sr[o].ASSIGN_TYPE + '</td>';
                htmlstr += '<td class="a">' + sr[o].CLOSESTATUS + '</td>';

                htmlstr += '<td class="a">' + sr[o].HOLDSTATUS + '</td>';
                htmlstr += '<td class="a">' + sr[o].NEWSTATUS + '</td>';
                htmlstr += '<td class="a">' + sr[o].OPENSTATUS + '</td>';

                //htmlstr += '<td class="a">' + sr[o].POSTEDSTATUS + '</td>';
                htmlstr += "</tr>";
            }

            $(ah.config.id.searchResultAssignTypeList + ' ' + ah.config.tag.tbody).append(htmlstr);
            $(ah.config.id.printSearchResultList).html('');
            $(ah.config.id.printSearchResultList).append($(ah.config.id.searchResultAssignTypeList).html());

            var tableReference = ah.config.id.searchResultAssignTypeList + ' table';
            self.sortDataTable(tableReference, 0, 'asc', false);
        }
        ah.displaySearchResult();

    };
    self.searchWOResult = function (jsonData) {
        sr = self.srDataArray;
        var fromDate = $('#requestFromDate').val();
        var toDate = $('#requestStartDate').val();
       // alert(JSON.stringify(sr));
        $(ah.config.id.searchResultPMTypeList).hide();
        $(ah.config.id.searchResultList).hide();
        $(ah.config.id.searchResultAssignTypeList).hide();
        $(ah.config.id.searchResultStaffList).hide();
        $(ah.config.id.searchResultWOList).show();
        $(ah.config.id.searchResultWOList + ' ' + ah.config.tag.tbody).html('');

        var searchCat = $("#sumCategory").val();

        systemText.searchModeHeader = "Waiting Time Details From "+fromDate+" To "+toDate;

        $(ah.config.id.searchResultSummaryCount).text("Number of Records: " + sr.length);
        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString()));

        self.assetCategory.title(systemText.searchModeHeader.toString());
        self.assetCategory.count("No. of Records: " + sr.length.toString());

        $(ah.config.cls.liPrint).hide();
        if (sr.length > 0) {
            $(ah.config.cls.liPrint).show();
            var htmlstr = '';
           
            for (var o in sr) {
                var reqDate = ah.formatJSONYearDateTimeToString(sr[o].REQ_DATE);
                var reqDateDisp = ah.formatJSONDateTimeToString(sr[o].REQ_DATE);
               
                var d = new Date(reqDate);
                var a = new Date();
          
                var today = moment(new Date()).format("DD/MM/YYYY HH:mm");
                if (sr[o].CLOSED_DATE !== null) {
               
                    var closeDate = ah.formatJSONYearDateTimeToString(sr[o].CLOSED_DATE);

                    today = ah.formatJSONDateTimeToString(sr[o].CLOSED_DATE);

                    a = new Date(closeDate);
                }
                var waitingHours = ((parseInt((a - d) / 1000) / 60) / 60).toFixed(2);

                if (sr[o].WO_STATUS === 'NE') {
                    sr[o].WO_STATUS = 'New';
                } else if (sr[o].WO_STATUS === 'OP') {
                    sr[o].WO_STATUS = 'Open';
                } else if (sr[o].WO_STATUS === 'PS') {
                    sr[o].WO_STATUS = 'Posted';
                }
                else if (sr[o].WO_STATUS === 'HA') {
                    sr[o].WO_STATUS = 'On Hold';
                } else if (sr[o].WO_STATUS === 'CL') {
                    sr[o].WO_STATUS = 'Closed';
                }

                htmlstr += "<tr>";
                htmlstr += '<td class="a">' + sr[o].REQ_NO + '</td>';
                htmlstr += '<td class="h">' + sr[o].PROBLEM + '</td>';
                htmlstr += '<td class="d">' + sr[o].PROBTYPEDESC + '</td>';
                htmlstr += '<td class="d">' + sr[o].CREATED_NAME + '</td>';
                htmlstr += '<td class="a">' + sr[o].ASSET_NO + '</td>';
                if (sr[o].WO_STATUS === 'Closed') {
                    htmlstr += '<td class="a fc-green">' + sr[o].WO_STATUS + '</td>';
                    htmlstr += '<td class="a fc-green">' + reqDateDisp + '</td>';
                    htmlstr += '<td class="a fc-green">' + today + '</td>';
                    htmlstr += '<td class="a fc-green">' + waitingHours + '</td>';
                } else {
                    htmlstr += '<td class="a fc-redalert">' + sr[o].WO_STATUS + '</td>';
                    htmlstr += '<td class="a fc-redalert">' + reqDateDisp + '</td>';
                    htmlstr += '<td class="a fc-redalert">' + today + '</td>';
                    htmlstr += '<td class="a fc-redalert">' + waitingHours + '</td>';
                }
                
                
                htmlstr += "</tr>";
            }

            $(ah.config.id.searchResultWOList + ' ' + ah.config.tag.tbody).append(htmlstr);
            $(ah.config.id.printSearchResultList).html('');
            $(ah.config.id.printSearchResultList).append($(ah.config.id.searchResultWOList).html());

            var tableReference = ah.config.id.searchResultWOList + ' table';
            self.sortDataTable2(tableReference, 0, 'asc', false);
        }
        ah.displaySearchResult();

    };

	sortArray = function () {

	}

	self.readSetupID = function () {

		var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var postData;

		ah.toggleReadMode();

	

	};
	self.dateClick = function () {
		$("#dateClickAction").val("ACTIVE");
	};
	self.searchNow = function () {

		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		var postData;

		ah.toggleSearchMode();
		var searchCat = $("#sumCategory").val();
        var apiSearch = "";
        if (searchCat === "PM") {
            apiSearc = ah.config.api.searchPMType;
        } else if (searchCat === "SP") {
            apiSearc = ah.config.api.searchSupplier;
        } else if (searchCat === "AS") {
            apiSearc = ah.config.api.searchAsset;
        } else if (searchCat === "ASC") {
            apiSearc = ah.config.api.searchCostCenter;
        } else if (searchCat === "DT") {
            apiSearc = ah.config.api.searchDeviceType;
        } else if (searchCat === "CC") {
			apiSearc = ah.config.api.searchProblemType;
		} else if (searchCat === "RC") {
			apiSearc = ah.config.api.searchJobType;
        } else if (searchCat === "CB") {
            apiSearc = ah.config.api.searchCreatedBy;
            
        } else if (searchCat === "AT") {
            apiSearc = ah.config.api.searchAssignTo;

        } else if (searchCat === "AP") {
            apiSearc = ah.config.api.searchAssignType;

        } else if (searchCat === "WT") {
            apiSearc = ah.config.api.searchWO;

        }
        
        $(ah.config.id.fromToDate).show();

        var fromDate = $('#requestFromDate').val().split('/');
        var fromArr = "";
        var toArr = "";
		if (fromDate.length == 3) {
            self.searchFilter.fromDate(fromDate[2] + "/" + fromDate[1] + "/" + fromDate[0]);
            fromArr = fromDate[0] + "/" + fromDate[1] + "/" + fromDate[2];
        } else {
            self.searchFilter.fromDate('');

        }
		var toDate = $('#requestStartDate').val().split('/');
		if (toDate.length == 3) {
            self.searchFilter.toDate(toDate[2] + "/" + toDate[1] + "/" + toDate[0] + "  23:00:00.000");
            toArr = toDate[0]+ "/" + toDate[1] + "/" + toDate[2]
        } else {
            self.searchFilter.toDate('');
        }
        if (fromArr === "" && toArr === ""){
            $(ah.config.id.fromToDate).hide();
        }
        $(ah.config.id.fromToDate).text('From Date: ' + fromArr + '    To Date: ' + toArr);
        
        self.showLoadingBackground();
        
		postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), SEARCH_FILTER: self.searchFilter, RETACTION: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.when($.post(self.getApi() + apiSearc, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus)).done(function () {
            $(ah.config.id.loadingBackground).removeClass('initializing').addClass('done');
        });
    };

    self.exportToExcel = function () {
        $("#dvjson").excelexportjs({
            containerid: "dvjson"
            , datatype: 'json'
            , dataset: self.srDataArray
            , columns: getColumns(self.srDataArray)
            , filename: "WorkOrderSummary"
        });
    };

    self.exportTableToExel = function () {
        $("#WorkOrderSearchResultCMTable").excelexportjs({
            containerid: "WorkOrderSearchResultCMTable"
            , datatype: 'table'
            , filename: "WorkOrderSummary"
        });
    };

    self.webApiCallbackStatus = function (jsonData) {
        //console.log(jsonData);
		if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));

		}
        else if (jsonData.LOV_LOOKUPS || jsonData.STAFFLIST || jsonData.AM_PROCEDURES || jsonData.AM_CLIENT_INFO || jsonData.WORD_ORDER_LIST || jsonData.PMType_List || jsonData.ENGRASSIGNTYPE_STATUSLIST || jsonData.ENGRASSIGNTO_STATUSLIST || jsonData.ENGRCREATEDBY_STATUSLIST || jsonData.WO_JOBPROBLEMTYPE_V || jsonData.STAFF || jsonData.SUCCESS) {
            if (ah.CurrentMode == ah.config.mode.search) {
                if (jsonData.PMType_List) {
                    self.srDataArray = jsonData.PMType_List;
                    self.searchResultPMType(jsonData);
                } else if (jsonData.ENGRCREATEDBY_STATUSLIST) {
                    self.srDataArray = jsonData.ENGRCREATEDBY_STATUSLIST;
                    self.searchStaffResult(jsonData);
                } else if (jsonData.ENGRASSIGNTO_STATUSLIST) {
                    self.srDataArray = jsonData.ENGRASSIGNTO_STATUSLIST;
                        self.searchAssignToResult(jsonData);
                } else if (jsonData.ENGRASSIGNTYPE_STATUSLIST) {
                    self.srDataArray = jsonData.ENGRASSIGNTYPE_STATUSLIST;
                    self.searchAssignTypeResult(jsonData);
                } else if (jsonData.WORD_ORDER_LIST) {
                    self.srDataArray = jsonData.WORD_ORDER_LIST;
                    self.searchWOResult(jsonData);
                } else {
                    self.srDataArray = jsonData.WO_JOBPROBLEMTYPE_V;
                    sessionStorage.setItem(ah.config.skey.searchResult, JSON.stringify(jsonData.WO_JOBPROBLEMTYPE_V));
                    self.searchResult('DESCRIPTION');
                }
            }
            else if (ah.CurrentMode == ah.config.mode.print) {

                var clientInfo = jsonData.AM_CLIENT_INFO;

                self.clientDetails.CLIENTNAME(clientInfo.DESCRIPTION);
                self.clientDetails.CLIENT_LOGO(clientInfo.CLIENT_LOGO);
                self.clientDetails.COMPANY_LOGO(clientInfo.COMPANY_LOGO);
                self.clientDetails.HEADING(clientInfo.HEADING);
                self.clientDetails.HEADING2(clientInfo.HEADING2);
                self.clientDetails.HEADING3(clientInfo.HEADING3);
                self.clientDetails.HEADING_OTH(clientInfo.HEADING_OTH);
                self.clientDetails.HEADING_OTH2(clientInfo.HEADING_OTH2);
                self.clientDetails.HEADING_OTH3(clientInfo.HEADING_OTH3);
                self.clientDetails.CLIENTADDRESS(clientInfo.ADDRESS + ', ' + clientInfo.CITY + ', ' + clientInfo.ZIPCODE + ', ' + clientInfo.STATE);
                self.clientDetails.CLIENTCONTACT('Tel.No: ' + clientInfo.CONTACT_NO1 + ' Fax No:' + clientInfo.CONTACT_NO2);

                self.print();
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {

                lovCostCenterArray = jsonData.LOV_LOOKUPS;
                lovCostCenterArray = lovCostCenterArray.filter(function (item) { return item.CATEGORY === 'AIMS_COSTCENTER' });
                $.each(lovCostCenterArray, function (item) {
                    $(ah.config.id.costcenterStrList)
                        .append($("<option>")
                            .attr("value", lovCostCenterArray[item].DESCRIPTION.trim())
                            .attr("id", lovCostCenterArray[item].LOV_LOOKUP_ID));
                });

                lovSupplierArray = jsonData.AM_SUPPLIER;
                $.each(lovSupplierArray, function (item) {
                    $(ah.config.id.supplierStrList)
                        .append($("<option>")
                            .attr("value", lovSupplierArray[item].DESCRIPTION.trim())
                            .attr("id", lovSupplierArray[item].SUPPLIER_ID));
                });

                var aimsProblemType = jsonData.LOV_LOOKUPS;
                aimsProblemType = aimsProblemType.filter(function (item) { return item.CATEGORY === 'AIMS_PROBLEMTYPE' });
                $.each(aimsProblemType, function (item) {
                    $(ah.config.id.filterProblemType)
                        .append($("<option></option>")
                            .attr("value", aimsProblemType[item].LOV_LOOKUP_ID)
                            .text(aimsProblemType[item].DESCRIPTION));
                });

                var aimsPriority = jsonData.LOV_LOOKUPS;
                aimsPriority = aimsPriority.filter(function (item) { return item.CATEGORY === 'AIMS_PRIORITY' });
                $.each(aimsPriority, function (item) {
                    $(ah.config.id.filterPriority)
                        .append($("<option></option>")
                            .attr("value", aimsPriority[item].LOV_LOOKUP_ID)
                            .text(aimsPriority[item].DESCRIPTION));
                });

                var aimsJobType = jsonData.LOV_LOOKUPS;
                aimsJobType = aimsJobType.filter(function (item) { return item.CATEGORY === 'AM_JOBTYPE' });
                $.each(aimsJobType, function (item) {
                    $(ah.config.id.filterJobType)
                        .append($("<option></option>")
                            .attr("value", aimsJobType[item].LOV_LOOKUP_ID)
                            .text(aimsJobType[item].DESCRIPTION));
                });

                var srStaffListWo = jsonData.STAFFLISTWO;
                $.each(srStaffListWo, function (item) {
                    $(ah.config.id.filterAssignTo)
                        .append($("<option></option>")
                            .attr("value", srStaffListWo[item].ASSIGN_TO)
                            .text(srStaffListWo[item].STAFF_FULLNAME));
                });

            }

		}
		else {

			alert(systemText.errorTwo);
			//	window.location.href = ah.config.url.homeIndex;
		}

	};

	self.initialize();
};