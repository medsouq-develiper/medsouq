﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            debtorDetailItem: '.debtorDetailItem',
            helpDeskAction: '.helpDeskAction',
			divInfo: '.divInfo',
			detailItem: '.detailItem',
            searhButton: '.searhButton',
            liClose: '.liClose'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul'
		},
		fld: {
            ticketId: 'TICKETNO',
            newIssue: 'NEWISSUE'
		},
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveMessage: '#saveMessage',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmMessage: '#frmMessage',
            assignTo: '#ASSIGN_TO',
			successMessage: '#successMessage',
			searchResult1: '#searchResult1',
			txtGlobalSearchOth: '#txtGlobalSearchOth',
			issueStatus: '#STATUS',
			issuePriority: '#PRIORITY',
            issueCategory: '#CATEGORY',
            assignDate: '#ASSIGN_DATE',
            reqLocation: '#LOCATION'
        },
        cssCls: {
            viewMode: 'view-mode'
		},
		tagId: {
			txtGlobalSearchOth: 'txtGlobalSearchOth'
		},
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
			dataDebtorID: 'data-debtorID',
			infoID: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'searchResult',
            helpdeskDetails: 'AM_HELPDESK',
            groupPrivileges: 'GROUP_PRIVILEGES',
            userClient: 'USER_CLIENT'
        },
		url: {
			modalAddItems: '#openModal',
            homeIndex: '/Home/Index'
        },
        api: {

            readHelpDesk: '/Emessages/ReadHelpDesk',
			searchAll: '/Emessages/SearchHelpDesk',
            searchAssets: '/AIMS/SearchAssetsActive',
            createNew: '/Emessages/CreateHelpDesk',
            updateHelpDesk: '/Emessages/UpdateHelpDesk',
            searchStaffList: '/Search/SearchHelpDeskLookUp',
            updateAccepted: '/Emessages/UpdateHelpDeskAccept'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal debtorDetailItem' data-debtorID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
			searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
			searchRowHeaderTemplateList: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>"
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };
	thisApp.getParameterValueByName = function (name) {
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search);
		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	};
    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liClose + ',' + thisApp.config.cls.divInfo + ',' +  thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;
        $( thisApp.config.id.issueStatus + ',' + thisApp.config.id.assignTo + ',' + thisApp.config.id.issuePriority + ',' + thisApp.config.id.issueCategory).prop(thisApp.config.attr.disabled, true);

        document.getElementById("REF_WO").disabled = true;
        document.getElementById("WO_STATUS").disabled = true;
        document.getElementById("WO_CLOSEDDATE").disabled = true;

    };
	thisApp.toggleSearchItems = function () {

		thisApp.CurrentMode = thisApp.config.mode.search;

		//$(thisApp.config.cls.statusText).text(systemText.searchModeStatuxText);
		$(thisApp.config.cls.liApiStatus).show();

		$(
			thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.liModify + ',' +
			thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
		).hide();
		document.getElementById("ASSET_NO").disabled = true;
	};
    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

		$(thisApp.config.cls.searhButton + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liClose + ',' +thisApp.config.cls.divInfo + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

		$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

		//$(thisApp.config.id.issueStatus).readOnly=true;
        $(thisApp.config.id.issueStatus + ',' + thisApp.config.id.assignTo + ',' + thisApp.config.id.issuePriority + ',' + thisApp.config.id.issueCategory).prop(thisApp.config.attr.disabled, true);
		document.getElementById("REF_WO").disabled = true;
		document.getElementById("REF_WO").disabled = true;
		document.getElementById("WO_STATUS").disabled = true;
		document.getElementById("WO_CLOSEDDATE").disabled = true;
		

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
             thisApp.config.cls.liSave + ',' + thisApp.config.cls.divInfo + ',' + thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

		$(thisApp.config.cls.searhButton + ',' +thisApp.config.cls.divInfo + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();
        $(thisApp.config.cls.liClose + ',' +thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

		$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.id.issueStatus + ',' + thisApp.config.id.assignTo + ',' + thisApp.config.id.issuePriority + ',' + thisApp.config.id.issueCategory).prop(thisApp.config.attr.disabled, true);

		document.getElementById("REF_WO").disabled = true;
		document.getElementById("WO_STATUS").disabled = true;
		document.getElementById("WO_CLOSEDDATE").disabled = true;
    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liClose + ',' +thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.liClose + ',' +thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.divInfo + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liClose + ',' +thisApp.config.cls.searhButton + ',' +thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

        $(
           thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
           thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.getAge = function (birthDate) {

        var seconds = Math.abs(new Date() - birthDate) / 1000;

        var numyears = Math.floor(seconds / 31536000);
        var numdays = Math.floor((seconds % 31536000) / 86400);
        var nummonths = numdays / 30;

        return numyears + " y " + Math.round(nummonths) + " m ";
    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };
	thisApp.formatJSONTimeToString = function () {

		var dateToFormat = arguments[0];
		var strDate;
		var strTime;

		if (dateToFormat === null) return "-";
		if (dateToFormat.length < 16) return "-";

		strDate = String(dateToFormat).split('-', 3);
		strTime = String(dateToFormat).split('T');

		var timeToFormat = strTime[1].substring(0, 5);


		return timeToFormat;
	};
	thisApp.formatJSONDateToString = function () {

		var dateToFormat = arguments[0];
		var strDate;

		if (dateToFormat === null) return "-";
		strDate = String(dateToFormat).split('-', 3);

		dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


		return dateToFormat;
	};
	thisApp.formatJSONDateTimeToString = function () {


		var dateToFormat = arguments[0];
		var strDate;
		var strTime;

		if (dateToFormat === null) return "";
		if (dateToFormat.length < 16) return "";

		strDate = String(dateToFormat).split('-', 3);
		strTime = String(dateToFormat).split('T');

		dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0] + ' ' + strTime[1].substring(0, 5);


		return dateToFormat;
	};
	thisApp.LoadJSON = function (jsonData) {

		$.each(jsonData, function (name, val) {
			var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');


			if (isDateTime) {
				
				val = thisApp.formatJSONDateTimeToString(val);
			}

			if (isDate) {
				val = thisApp.formatJSONDateToString(val);
			}

			var $el = $('#' + name), type = $el.attr('type'), isTime = $el.attr('data-istime');
			if (isTime) {
				if (val === null || val === "") {
					val = "00:00";
				} else {
					val = thisApp.formatJSONTimeToString(val);
				}

			}

			switch (type) {
				case 'checkbox':
					$el.attr('checked', 'checked');
					break;
				case 'radio':
					$el.filter('[value="' + val + '"]').attr('checked', 'checked');
					break;
				default:
					var tagNameLCase = String($el.prop('tagName')).toLowerCase();
					if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
						$el.text(val);
					} else {
						$el.val(val);
					}
			}
		});

	};

};

/*Knockout MVVM Namespace - Controls form functionality*/
var emessagesViewModel = function (systemText) {

    
    var self = this;

    var ah = new appHelper(systemText);

    $('input:text').keydown(function (e) {
        if (e.keyCode == 222)
            return false;

    });
    $(ah.config.tag.textArea).keydown(function (e) {
        if (e.keyCode == 222)
            return false;

    });

    self.clientInfo = {
        Description: ko.observable('Biomedical Management System'),
        clientLogo: ko.observable('../images/ClientLogo.png'),
        buildingCode: ko.observable(""),
        isVisible: ko.observable(false)
    };
    var modal = document.getElementById('openModalAlert');
	var srDataArray = ko.observableArray([]);
	self.searchFilter = {
		status: ko.observable('I'),
		staffId: ko.observable('')
	};
    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());
        var userClientList = JSON.parse(sessionStorage.getItem(ah.config.skey.userClient));
        if (userClientList.length == 1) {
            self.clientInfo.Description = userClientList[0].Description;
            self.clientInfo.clientLogo = userClientList[0].LogoPath;
            self.clientInfo.isVisible = true;
        }
        ah.toggleReadMode();

        ah.initializeLayout();
        var modal = document.getElementById('openModalAlert');
        if (modal !== null)
            modal.style.display = "none";
        postData = { STAFF_ID: staffLogonSessions.STAFF_ID ,SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchStaffList, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
	};
	self.returnHome = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		window.location.href = staffDetails.HOME_DEFAULT;
	};
    self.createNew = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
      
        ah.toggleAddMode();
		if (self.validatePrivileges('7') === 0 ) {
            $("#helpDeskAction").addClass("disabledInput");
           
        };
        var jDate = new Date();
        var strDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        var reqDate = strDate + ' ' + hourmin;
        if (staffDetails.CONTACT_PRIMARY === null || staffDetails.CONTACT_PRIMARY === "") {
            $("#CONTACT_NO").val(staffDetails.CONTACT_SECONDARY);
        } else {
            $("#CONTACT_NO").val(staffDetails.CONTACT_PRIMARY);
        }
        $("#LOCATION").val(staffDetails.LOCATION);
		$("#ISSUE_DATE").val(reqDate);
		$("#REQ_STAFF_ID").val(staffDetails.STAFF_ID);
		$("#REQ_STAFF_DESC").val(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());
        $("#STATUS").val("OPEN");
		document.getElementById("ISSUE_DATE").readOnly = true;
		if (self.validatePrivileges('18') > 0) {
			$(ah.config.id.issueStatus).prop(ah.config.attr.disabled, false);


		};

    };
	self.searchAssetResult = function (LovCategory, pageLoad) {

		var sr = srDataArray;
		//alert(JSON.stringify(sr));
		if (LovCategory !== null && LovCategory !== "") {
			var arr = sr;
			var keywords = LovCategory.toUpperCase();
			var filtered = [];

			for (var i = 0; i < sr.length; i++) {
				for (var j = 0; j < keywords.length; j++) {
					if ((sr[i].DESCRIPTION.toUpperCase()).indexOf(keywords) > -1 || (sr[i].ASSET_NO.toUpperCase()).indexOf(keywords) > -1) {
						filtered.push(sr[i]);
						break;
					}
				}
			}
			sr = filtered;
		}

		$(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).html('');

		if (sr.length > 0) {
            $("#noresult").hide();
			var htmlstr = '';
		
			for (var o in sr) {
				var assetDescription = sr[o].DESCRIPTION;
				if (assetDescription === null || assetDescription === "" || assetDescription === "null") {

					sr[o].DESCRIPTION = "Undefined"
					assetDescription = "Undefined"
				}
				if (assetDescription.length > 70) {
					assetDescription = assetDescription.substring(0, 70) + '...';
                }
                if (sr[o].CONDITION === null) { sr[o].CONDITION = "" };
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplateList, JSON.stringify(sr[o]), sr[o].ASSET_NO, assetDescription, "");
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.costCenter, sr[o].COSTCENTER_DESC + '&nbsp;&nbsp;');               
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.serialNo, sr[o].SERIAL_NO);
				//htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.buildingName, sr[o].BUILDING) + '&nbsp;&nbsp;';
				//htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.locationName, sr[o].LOCATION + '&nbsp;&nbsp;');
				//htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.conditionName, sr[o].CONDITION + '&nbsp;&nbsp;');
				//htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.statusName, 'Inservice');
				htmlstr += '</li>';
				
			}
	
			$(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).append(htmlstr);
        } else {
            $("#noresult").show();
        }
		
		$(ah.config.cls.detailItem).bind('click', self.populateAssetInfo);
		//ah.toggleDisplayModeAsset();
		//ah.toggleAddItems();
	};
    self.modifyHelpDesk = function () {

        ah.toggleEditMode();
		if (self.validatePrivileges('18') > 0) {
			$(ah.config.id.issueStatus).prop(ah.config.attr.disabled, false);

        };
        if (self.validatePrivileges('19') > 0) {

            $(ah.config.id.assignTo + ',' + ah.config.id.issuePriority + ',' + ah.config.id.issueCategory).prop(ah.config.attr.disabled, false);
        };
    };
    self.alertMessage = function (refMessage) {
        $("#alertmessage").text(refMessage);
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "block";

    };
	self.populateAssetInfo = function () {

		var infoID = JSON.parse(this.getAttribute(ah.config.attr.infoID).toString());
        //alert(JSON.stringify(infoID));

		
		$("#ASSET_NO").val(infoID.ASSET_NO);
		$("#ASSET_DESC").val(infoID.ASSET_NO+'-'+infoID.DESCRIPTION);
        $("#COST_CENTER").val(infoID.COST_CENTER);
   
		
	};
    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };
	self.pageLoader = function () {
		var senderID = $(arguments[1].currentTarget).attr('id');

		var searchStr = $(ah.config.id.txtGlobalSearchOth).val().toString();

		switch (senderID) {

		
			case ah.config.tagId.txtGlobalSearchOth: {
				pageCount = 1;
				pageNum = 0;
                var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
                var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
                var postData = '';
                var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));

                //ah.toggleSearchMode();
                //ah.toggleSearchItems();
                gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "15" });
                if (gPrivileges.length <= 0) {
                    self.searchFilter.staffId = staffLogonSessions.STAFF_ID;
                }

                postData = { SEARCH: searchStr, SEARCH_FILTER: self.searchFilter, STAFF_LOGON_SESSIONS: staffLogonSessions };

                $.post(self.getApi() + ah.config.api.searchAssets, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
                window.location.href = ah.config.url.modalAddItems;
				//self.searchAssetResult(searchStr, 0);
				break;
			}
		}

	

	};
    self.saveAllChanges = function () {
        $(ah.config.id.assignDate + ',' + ah.config.id.issueStatus + ',' + ah.config.id.assignTo + ',' + ah.config.id.issuePriority + ',' + ah.config.id.issueCategory).prop(ah.config.attr.disabled, false);
        $(ah.config.id.saveMessage).trigger('click');

    };
    self.populateAssignDate = function () {
        var jDate = new Date();
        var strDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        var assignDate = strDate + ' ' + hourmin;
        $("#ASSIGN_DATE").val(assignDate);
    };
    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
			self.showInfo();
        }

    };
    self.validatePrivileges = function (privilegeId) {
        var groupPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
        
		var groupPrivileges = groupPrivileges.filter(function (item) { return item.PRIVILEGES_ID === privilegeId });
        return groupPrivileges.length;
    };
	self.showInfo = function (isNotUpdate, jsonData) {
        ah.toggleDisplayMode();
        if (isNotUpdate) {

			var helpdeskDetails = JSON.stringify(jsonData.AM_HELPDESK);//sessionStorage.getItem(ah.config.skey.helpdeskDetails);
			//alert(JSON.stringify(jsonData.AM_ASSET_DETAILS));
			var jdebtorDetails = JSON.parse(helpdeskDetails);

            ah.ResetControls();
            ah.LoadJSON(jdebtorDetails);
			var assetInfo =jsonData.AM_ASSET_DETAILS;
			
			
					$("#ASSET_NO").val(assetInfo.ASSET_NO);
					$("#ASSET_DESC").val(assetInfo.ASSET_NO+'-'+assetInfo.DESCRIPTION);
					$("#COST_CENTER").val(assetInfo.COST_CENTER);
			
			var staffInfo = jsonData.STAFFINFO;

			if (staffInfo !== null) {
				$("#REQ_STAFF_ID").val(staffInfo.STAFF_ID);
				$("#REQ_STAFF_DESC").val(staffInfo.FIRST_NAME.toLowerCase() + ' ' + staffInfo.LAST_NAME.toLowerCase());
			}
			$("#TICKETNO").text(jdebtorDetails.TICKET_ID);
			if (self.validatePrivileges('7') === 0) {
				$("#helpDeskAction").addClass("disabledInput");

			};

			if (self.validatePrivileges('18') > 0) {
				$(ah.config.id.issueStatus).prop(ah.config.attr.disabled, false);
            };
 
			if (self.validatePrivileges('19') > 0) {
				
				$(ah.config.id.assignTo + ',' + ah.config.id.issuePriority + ',' + ah.config.id.issueCategory).prop(ah.config.attr.disabled, false);
            };
           
            if (jdebtorDetails.STATUS == "COMPLETED" && jdebtorDetails.ACCEPTED_BY == null) {
               
                $(ah.config.cls.liClose).show();

            }



        }

       

    };

    self.searchResult = function (jsonData) {


        var sr = jsonData.AM_HELPDESK;
		
        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].TICKET_ID, sr[o].TICKET_ID, sr[o].ISSUE_DETAILS, '');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.asset, sr[o].ASSET_NO + "-" + sr[o].ASSET_DESC);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.contactPerson, sr[o].CONTACT_PERSON);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.contactNo, sr[o].CONTACT_NO);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.location, sr[o].LOCATION);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.status, sr[o].STATUS);

                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.debtorDetailItem).bind('click', self.readticketID);
        ah.displaySearchResult();

    };
    self.closeAlertModal = function () {
        modal.style.display = "none"
    };
    self.readticketID = function () {

        var ticketID = this.getAttribute(ah.config.attr.dataDebtorID).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { TICKET_ID: ticketID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readHelpDesk, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
	self.readticketIDExist = function (ticketID) {

		
		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var postData;

		ah.toggleReadMode();

		postData = { TICKET_ID: ticketID, STAFF_LOGON_SESSIONS: staffLogonSessions };
		$.post(self.getApi() + ah.config.api.readHelpDesk, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

	};
    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();
		if (self.validatePrivileges('8') === 0) {
			self.searchFilter.staffId = staffDetails.STAFF_ID;

		};
		postData = { SEARCH_FILTER: self.searchFilter, SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.assetSearchNow = function () {
        $("#noresult").hide();
		//var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		//var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		//var postData = '';
		//var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));

		////ah.toggleSearchMode();
		////ah.toggleSearchItems();
		//gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "15" });
		//if (gPrivileges.length <= 0) {
		//	self.searchFilter.staffId = staffLogonSessions.STAFF_ID;
		//}
		
		//postData = { SEARCH: "", SEARCH_FILTER: self.searchFilter, STAFF_LOGON_SESSIONS: staffLogonSessions };
	
		//$.post(self.getApi() + ah.config.api.searchAssets, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
		window.location.href = ah.config.url.modalAddItems;

    };

    self.assetSearchListNow = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var postData = '';
		var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));

        var searchStr = $(ah.config.id.txtGlobalSearchOth).val().toString();
		gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "15" });
		if (gPrivileges.length <= 0) {
			self.searchFilter.staffId = staffLogonSessions.STAFF_ID;
		}

        postData = { SEARCH: searchStr, SEARCH_FILTER: self.searchFilter, STAFF_LOGON_SESSIONS: staffLogonSessions };

		$.post(self.getApi() + ah.config.api.searchAssets, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    }
    self.updateAccepted = function () {
        

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
    
        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        updatedDate = strDateStart + ' ' + hourmin;
        var ticketId = $("#TICKET_ID").val();
        ah.toggleSaveMode();
        postData = { TICKET_ID: ticketId, UPDATED_BY: staffDetails.STAFF_ID, UPDATED_DATE: updatedDate, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.updateAccepted, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus); 
    };
    self.saveMessageDetails = function () {


        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var helpDeskDetails = ah.ConvertFormToJSON($(ah.config.id.frmMessage)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        if (helpDeskDetails.ASSET_NO === null || helpDeskDetails.ASSET_NO === "") {
            return self.alertMessage("Unable to Proceed.Asset is mandatory. Please select from the Asset list.");
            //alert('Unable to proceed. No item to request.')
            //return;
        }
        helpDeskDetails.CREATED_BY = staffLogonSessions.STAFF_ID;
        postData = { AM_HELPDESK: helpDeskDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
		var strTime = '';
		var reqDate = helpDeskDetails.ISSUE_DATE;
		var reqcurDate = reqDate.substring(6, 10) + '-' + reqDate.substring(3, 5) + '-' + reqDate.substring(0, 2);
		var apptDateStr = reqcurDate;
		strTime = String(reqDate).split(' ');
		strTime = strTime[1].substring(0, 5);
		reqcurDate = reqcurDate + ' ' + strTime;
        helpDeskDetails.ISSUE_DATE = reqcurDate;

       
        if (helpDeskDetails.ASSIGN_DATE !== null && helpDeskDetails.ASSIGN_DATE !== "" && helpDeskDetails.ASSIGN_DATE !== "-"){
            var assignDate = helpDeskDetails.ASSIGN_DATE;
            reqcurDate = assignDate.substring(6, 10) + '-' + assignDate.substring(3, 5) + '-' + assignDate.substring(0, 2);
            apptDateStr = reqcurDate;
            strTime = String(reqDate).split(' ');
            strTime = strTime[1].substring(0, 5);
            reqcurDate = reqcurDate + ' ' + strTime;
 
            helpDeskDetails.ASSIGN_DATE = reqcurDate;
        }
    
        




        if (ah.CurrentMode == ah.config.mode.add) {
            var jDate = new Date();
            var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);

            helpDeskDetails.CREATED_DATE = strDateStart + ' ' + hourmin;
            helpDeskDetails.TICKET_ID = 0;
            //if (helpDeskDetails.REF_WO === null || helpDeskDetails.REF_WO === "") {
            //    helpDeskDetails.REF_WO = 0;
            //}
            ah.toggleSaveMode();
            $.post(self.getApi() + ah.config.api.createNew, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {

            ah.toggleSaveMode();
            $.post(self.getApi() + ah.config.api.updateHelpDesk, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }

    };

    self.webApiCallbackStatus = function (jsonData) {
        //alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            //window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.SUCCESSNOTIFYUPDATE ||jsonData.STAFF_COSTCENTER_BUILDING || jsonData.STAFFINFO ||jsonData.STAFFLIST || jsonData.AM_ASSET_DETAILS || jsonData.AM_HELPDESK || jsonData.STAFF || jsonData.SERVICES || jsonData.SUCCESS) {
	
            if (ah.CurrentMode == ah.config.mode.save) {

                //sessionStorage.setItem(ah.config.skey.helpdeskDetails, JSON.stringify(jsonData.AM_HELPDESK));
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();
               
                if (jsonData.SUCCESS) {
					self.showInfo(false, jsonData);
                } else if (jsonData.SUCCESSNOTIFYUPDATE) {
                    self.showInfo(false, jsonData);

                }
                else {
                   // ah.toggleDisplayMode();
					self.showInfo(true, jsonData);
                }
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                sessionStorage.setItem(ah.config.skey.helpdeskDetails, JSON.stringify(jsonData.AM_HELPDESK));
				self.showInfo(true, jsonData);
			}
			else if (ah.CurrentMode == ah.config.mode.add || ah.CurrentMode == ah.config.mode.display || ah.CurrentMode == ah.config.mode.edit) {
				srDataArray = jsonData.AM_ASSET_DETAILS;
				self.searchAssetResult("", 0);
			}
			else if (ah.CurrentMode == ah.config.mode.search) {
				srDataArray = jsonData.AM_ASSET_DETAILS;
                self.searchResult(jsonData);
            }
			else if (ah.CurrentMode == ah.config.mode.idle) {
				
                $.each(jsonData.STAFFLIST, function (item) {

                    $(ah.config.id.assignTo)
                        .append($("<option></option>")
                        .attr("value", jsonData.STAFFLIST[item].STAFF_ID)
                        .text(jsonData.STAFFLIST[item].FIRST_NAME + ' ' + jsonData.STAFFLIST[item].LAST_NAME));
				});

                //$.each(jsonData.STAFF_COSTCENTER_BUILDING, function (item) {

                //    $(ah.config.id.reqLocation)
                //        .append($("<option></option>")
                //            .attr("value", jsonData.STAFF_COSTCENTER_BUILDING[item].CODE)
                //            .text(jsonData.STAFF_COSTCENTER_BUILDING[item].DESCRIPTION));
                //});
                
				
				var TicketNo = ah.getParameterValueByName(ah.config.fld.ticketId);
				
				if (TicketNo !== null && TicketNo !== "") {

					self.readticketIDExist(TicketNo);
                }

                var newIssueN = ah.getParameterValueByName(ah.config.fld.newIssue);

                if (newIssueN !== null && newIssueN !== "") {

                    self.createNew();
                }
            }

        }
        else {

            alert(systemText.errorTwo);
           // window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};