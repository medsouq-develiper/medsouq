﻿var appHelper = function (systemText) {
    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]'
        },
        mode: {
            idle: 0,

            homePage: 1,
            searchPage: 2,
            transactionPage: 3,

            searchItems: 4,
            createNewRecall: 5,
            viewSysTransReport: 6,
            searchTransReportItems: 7,
        },
        id: {
            spanUser: '#spanUser',
            prSubmitBtn: '#prSubmitBtn',
            divNotify: '#divNotify',
        },

        cssCls: {
            viewMode: 'view-mode'
        },
        tagId: {
            txtGlobalSearchOth: 'txtGlobalSearchOth',
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfo: 'data-info',
            datainfoID: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            userClient: 'USER_CLIENT'
        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalClose: '#',
            openModalFilter: '#openModalFilter',
            openSupportingDocument: '#openSupportingDocument'
        },
        fld: {
            prNo: 'PRNO'
        },
        api: {
            searchLookup: '/Inventory/SearchStores',
            searchStockOnhand: '/Inventory/SearchStockOnhandFromStore',
            creatSysTransReport: '/Inventory/CreatSysTransReport',
            searchCustomerRequest: '/Inventory/SearchStockTransDetFlag',
            getCustomerRequestByRequest: '/Inventory/GetCustomerRequestByRequest',
            updateCustomerRequestStatus: '/Inventory/UpdateStorkreqStatus',
            searchSetupLov: '/Setup/SearchInventoryLOV',
            searchItemBatchesV: '/Inventory/SearchItemBatchesV',
            searchAssetV: '/Aims/SearchAssetV',
            searchTransReports: '/Reports/SearchTransReports',
            getItemBatchesVByReport: '/Inventory/GetItemBatchesVByReport',
            getAssetVByReport: '/Aims/GetAssetVByReport',
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
        }
    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };
};

/*Knockout MVVM Namespace - Controls form functionality*/
var createRecallViewModel = function (systemText) {
    var ah = new appHelper(systemText);
    var self = this;
    var imageExtensions = new RegExp("(.*?)\.(BMP|bmp|gif|jpg|png|pdf|txt)$");
    var tifExtensions = new RegExp("(.*?)\.(TIF|TIFF|tif|tiff)$");
    var officeExtensions = new RegExp("(.*?)\.(doc|docx|dot|csv|xls|xlsx|xlw|pps|ppsx|ppt|pptx)$");
    var unsupportedExtension = new RegExp("(.*?)\.(dwg|DWG)$");
    var action = '';

    self.modes = ah.config.mode;
    self.pageMode = ko.observable();
    self.currentMode = ko.observable();

    self.showProgress = ko.observable(false);
    self.showProgressScreen = ko.observable(false);

    self.progressText = ko.observable();
    self.contentHeader = ko.observable('');
    self.contentSubHeader = ko.observable('');

    self.documentCategories = ko.observableArray([]);
    self.typesOfAction = ko.observableArray([]);
    self.probabilities = ko.observableArray([]);
    self.detectabilities = ko.observableArray([]);
    self.DDSA = ko.observableArray([]);
    self.QSA = ko.observableArray([]);
    self.recallBys = ko.observableArray([]);
    self.recallTypes = ko.observableArray([]);
    self.HHLevels = ko.observableArray([]);
    self.transReports = ko.observableArray([]);
    self.transReportItems = ko.observableArray([]);
    self.sysTransReport = {
        SEARCHTYPE: ko.observable('Asset'),
        searchKeyText: ko.observable('Serial/Model No.'),
        SEARCHKEY: ko.observable(''),

        ID: ko.observable(''),
        RECALLCATEGORY: ko.observable(''),
        RECALLFIRMREFNO: ko.observable(''),
        RECALLTYPE: ko.observable(''),
        RECALLTIMEFRAME: ko.observable(''),
        RESPONSETIMEFRAME: ko.observable(''),
        PROBABILITY: ko.observable(''),
        DETECTABILITY: ko.observable(''),
        DDSA: ko.observable(''),
        QSA: ko.observable(''),
        HHLEVEL: ko.observable(''),
        REASON: ko.observable(''),
        REPORTSTATUS: ko.observable(''),
        REPORTEDBY: ko.observable(''),
        REPORTEDDATE: ko.observable(''),
        REPORTEDDATEDISPLAY: ko.observable(''),
        CLOSEBY: ko.observable(''),
        CLOSEDATE: ko.observable(''),
        CLOSEDATEDISPLAY: ko.observable(''),
        ACTIONBY: ko.observable(''),
        ACTIONDATE: ko.observable(''),
        ACTIONDATEDISPLAY: ko.observable(''),
        REPORTS: ko.observable(''),
        ACTIONREPORTS: ko.observable(''),
        CLOSEREPORTS: ko.observable(''),
        ACTION: ko.observable(''),
    };
    self.uploadedListWithCategory = ko.observableArray([]);
    self.documentListWithCategory = ko.observableArray([]);
    self.documentList = ko.observableArray([]);
    self.uploadedList = ko.observableArray([]);
    self.uploadedFiles = [];
    self.modalDocumentCategory = ko.observable('');
    self.modalFileData = ko.observable({
        uploaded: {},
        file: null
    });

    self.pageQuickSearch = ko.observable('');

    self.signOut = function () {
        window.location.href = ah.config.url.homeIndex;
    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.toggleBoolean = function (property, passValue, mode) {
        var value = typeof passValue !== 'undefined' ? passValue : !self[property]();
        self[property](value);

        if (self[property](value) && mode) {
            switch (mode) {
                case 'page':
                    self.progressText('Please wait . . .');
                    break;
                case 'search':
                    self.progressText('Please wait while searching . . .');
                    break;
                case 'save':
                    self.progressText('Please wait while saving . . .');
                    break;
                case 'get':
                    self.progressText('Please wait while getting Customer Request Information . . .');
                    break;
            }
        }
    };
    self.setObservableValue = function (_object1, _object2) {
        Object.keys(_object2).forEach(function (key) {
            if (ko.isObservable(_object1[key]))
                _object1[key](_object2[key])
            else
                _object1[key] = _object2[key];
        });
    };
    self.getObservableValue = function (data) {
        if (Array.isArray(data)) {
            var returnData = [];
            for (var i = 0; i < data.length; i++) {
                var _object = data[i];
                returnData.push({});
                Object.keys(_object).forEach(function (key) {
                    if (ko.isObservable(_object[key]))
                        returnData[i][key] = _object[key]();
                    else
                        returnData[i][key] = _object[key];
                });
            }
        }
        else if (data instanceof Object) {
            var returnData = {};
            var _object = data;
            Object.keys(_object).forEach(function (key) {
                if (ko.isObservable(_object[key]))
                    returnData[key] = _object[key]();
                else
                    returnData[key] = _object[key];
            });
        }
        else
            returnData = data;

        return returnData;
    };
    self.checkViewMode = function () {
        return self.sysTransReport.REPORTSTATUS() == 'For Reporting'
    };
    self.checkAcknowledgement = function () {
        var userClientList = JSON.parse(sessionStorage.getItem(ah.config.skey.userClient));
        if (userClientList && userClientList.length && userClientList[0].CLIENT_CODE && self.sysTransReport.REPORTSTATUS() == 'Closed')
            return true;
        return false;
    };
    self.scrollToViewPort = function (elementId) {
        document.getElementById(elementId).scrollIntoView(true);
    };

    self.quickSearch = function () {
        self.pageMode(self.modes.searchPage);
        self.toggleBoolean('showProgress', true, 'search');

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        var postData = { SEARCH: self.pageQuickSearch(), SEARCH_FILTER: { FromDate: "" }, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchTransReports, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.generateDataTable = function () {
        if ($.fn.dataTable.isDataTable('#searchDataTable'))
            $('#searchDataTable').DataTable().clear().destroy();

        var table = $('#searchDataTable').DataTable({
            data: self.transReports(),
            createdRow: function (row, data, dataIndex) {
                $(row).find('.btnCreateReport').on('click', function (event) {
                    event.preventDefault();

                    var id = $(this).closest("tr").find("td:eq(1)").text();
                    var item = self.transReports.slice(0).find(function (item) { return item.ID == id });
                    self.setSelectedSysTransReport(item);
                });
            },
            columns: [
                { data: 'CREATEDDATE' },
                { data: 'ID' },
                { data: 'SEARCHTYPE' },
                { data: 'SEARCHKEY' },
                { data: 'REPORTSTATUS' },
                {
                    data: null,
                    "orderable": false,
                    render: function (data, type, row) {
                        return "<a href='#' class='button-ext btnCreateReport' title='Click to view report'><i class='fa fa-share-square-o'></i></a>";
                    }
                }

            ],
            "searching": false,
            "iDisplayLength": 15,
            "aLengthMenu": [15, 200, 6500, self.transReports().length]
        });
    };
    self.setSelectedSysTransReport = function (item) {
        self.currentMode(self.modes.searchTransReportItems);
        self.pageMode(self.modes.transactionPage);
        self.toggleBoolean('showProgress', true, 'search');

        self.contentHeader(systemText.updateModeHeader);
        self.contentSubHeader('');

        self.sysTransReport.SEARCHTYPE('Asset');
        self.sysTransReport.searchKeyText('Serial/Model No.');
        self.sysTransReport.SEARCHKEY('');
        self.sysTransReport.ID('');
        self.sysTransReport.RECALLCATEGORY('');
        self.sysTransReport.RECALLFIRMREFNO('');
        self.sysTransReport.RECALLTYPE('');
        self.sysTransReport.RECALLTIMEFRAME('');
        self.sysTransReport.RESPONSETIMEFRAME('');
        self.sysTransReport.PROBABILITY('');
        self.sysTransReport.DETECTABILITY('');
        self.sysTransReport.DDSA('');
        self.sysTransReport.QSA('');
        self.sysTransReport.HHLEVEL('');
        self.sysTransReport.REASON('');
        self.sysTransReport.REPORTSTATUS('For Reporting');
        self.sysTransReport.REPORTEDBY('');
        self.sysTransReport.REPORTEDDATE('');
        self.sysTransReport.REPORTEDDATEDISPLAY('');
        self.sysTransReport.CLOSEBY('');
        self.sysTransReport.CLOSEDATE('');
        self.sysTransReport.CLOSEDATEDISPLAY('');
        self.sysTransReport.ACTIONBY('');
        self.sysTransReport.ACTIONDATE('');
        self.sysTransReport.ACTIONDATEDISPLAY('');
        self.sysTransReport.REPORTS('');
        self.sysTransReport.ACTIONREPORTS('');
        self.sysTransReport.CLOSEREPORTS('');
        self.sysTransReport.ACTION('');
        CKEDITOR.instances.REPORTS.setData(self.sysTransReport.REPORTS());
        CKEDITOR.instances.ACTIONREPORTS.setData(self.sysTransReport.ACTIONREPORTS());
        CKEDITOR.instances.CLOSEREPORTS.setData(self.sysTransReport.CLOSEREPORTS());
        CKEDITOR.instances.REPORTS.setReadOnly(false);
        CKEDITOR.instances.ACTIONREPORTS.setReadOnly(true);
        CKEDITOR.instances.CLOSEREPORTS.setReadOnly(true);

        var userClientList = JSON.parse(sessionStorage.getItem(ah.config.skey.userClient));
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData = { SYSREPORTID: item.ID, SEARCH_FILTER: { clientCode: userClientList && userClientList.length ? userClientList[0].CLIENT_CODE : "", SysReportId: item.ID }, STAFF_LOGON_SESSIONS: staffLogonSessions };

        if (item.SEARCHTYPE == 'Asset')
            $.post(self.getApi() + ah.config.api.getAssetVByReport, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        else
            $.post(self.getApi() + ah.config.api.getItemBatchesVByReport, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.cancelChanges = function () {
        self.pageMode(self.modes.homePage);
    };
    self.createNew = function () {
        self.pageMode(self.modes.transactionPage);
        self.currentMode(self.modes.idle);

        self.contentHeader(systemText.updateModeHeader);
        self.contentSubHeader('');

        self.sysTransReport.SEARCHTYPE('Asset');
        self.sysTransReport.searchKeyText('Serial/Model No.');
        self.sysTransReport.SEARCHKEY('');
        self.sysTransReport.ID('');
        self.sysTransReport.RECALLCATEGORY('');
        self.sysTransReport.RECALLFIRMREFNO('');
        self.sysTransReport.RECALLTYPE('');
        self.sysTransReport.RECALLTIMEFRAME('');
        self.sysTransReport.RESPONSETIMEFRAME('');
        self.sysTransReport.PROBABILITY('');
        self.sysTransReport.DETECTABILITY('');
        self.sysTransReport.DDSA('');
        self.sysTransReport.QSA('');
        self.sysTransReport.HHLEVEL('');
        self.sysTransReport.REASON('');
        self.sysTransReport.REPORTSTATUS('For Reporting');
        self.sysTransReport.REPORTEDBY('');
        self.sysTransReport.REPORTEDDATE('');
        self.sysTransReport.REPORTEDDATEDISPLAY('');
        self.sysTransReport.CLOSEBY('');
        self.sysTransReport.CLOSEDATE('');
        self.sysTransReport.CLOSEDATEDISPLAY('');
        self.sysTransReport.ACTIONBY('');
        self.sysTransReport.ACTIONDATE('');
        self.sysTransReport.ACTIONDATEDISPLAY('');
        self.sysTransReport.REPORTS('');
        self.sysTransReport.ACTIONREPORTS('');
        self.sysTransReport.CLOSEREPORTS('');
        self.sysTransReport.ACTION('');
        CKEDITOR.instances.REPORTS.setData(self.sysTransReport.REPORTS());
        CKEDITOR.instances.ACTIONREPORTS.setData(self.sysTransReport.ACTIONREPORTS());
        CKEDITOR.instances.CLOSEREPORTS.setData(self.sysTransReport.CLOSEREPORTS());
        CKEDITOR.instances.REPORTS.setReadOnly(false);
        CKEDITOR.instances.ACTIONREPORTS.setReadOnly(true);
        CKEDITOR.instances.CLOSEREPORTS.setReadOnly(true);

        self.transReportItems([]);
        self.documentListWithCategory([]);
        self.uploadedListWithCategory([]);
        self.documentList([]);
        self.uploadedList([]);
        $('#uploadedInfo').val('');
    };
    self.checkSearchType = function () {
        self.transReportItems([]);
        self.sysTransReport.SEARCHKEY('');
        switch (self.sysTransReport.SEARCHTYPE()) {
            case 'Asset':
                self.sysTransReport.searchKeyText('Serial/Model No.');
                break;
            case 'Parts':
                self.sysTransReport.searchKeyText('Serial/Batch No.');
                break;
            default:
                self.sysTransReport.searchKeyText('Serial/Batch No.');
                break;
        }
    };
    self.searchItems = function () {
        self.currentMode(self.modes.searchItems);
        self.toggleBoolean('showProgress', true, 'search');

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        postData = { SEARCH: self.sysTransReport.SEARCHKEY(), STAFF_LOGON_SESSIONS: staffLogonSessions };

        if (self.sysTransReport.SEARCHTYPE() == 'Asset')
            $.post(self.getApi() + ah.config.api.searchAssetV, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        else
            $.post(self.getApi() + ah.config.api.searchItemBatchesV, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.removeTransItem = function (item) {
        var selectedItem = item;
        self.transReportItems.remove(selectedItem);
    };
    self.triggerFormSubmit = function(a1, a2, a3, actionPerform) {
        action = actionPerform;
        $(ah.config.id.prSubmitBtn).trigger('click');
    };
    self.saveAllChanges = function () {
        if (!self.sysTransReport.SEARCHKEY())
            return alert('Unable to proceed. Search ' + self.sysTransReport.searchKeyText() + ' first.');

        if (!self.transReportItems().length)
            return alert('Unable to proceed. No item to recall.');

        self.currentMode(self.modes.createNewRecall);
        self.toggleBoolean('showProgress', true, 'save');
        self.toggleBoolean('showProgressScreen', true);

        self.sysTransReport.REPORTS(CKEDITOR.instances.REPORTS.getData());

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var systemDefault = JSON.parse(sessionStorage.getItem('0ed52a42-9019-47d1-b1a0-3fed45f32b8f'))[0];
        var SYSTEMTRANSACTION_REPORT = self.getObservableValue(self.sysTransReport);
        SYSTEMTRANSACTION_REPORT.ID = SYSTEMTRANSACTION_REPORT.ID || 0;
        SYSTEMTRANSACTION_REPORT.REPORTSTATUS = action || SYSTEMTRANSACTION_REPORT.REPORTSTATUS;
        action = '';

        var postData = { SYSTEMTRANSACTION_REPORT: SYSTEMTRANSACTION_REPORT, TRANS_ITEMS: self.transReportItems(), SYSTEMTRANSACTION_REPORT_SCANDOCS: self.uploadedList(), TYPE: self.sysTransReport.SEARCHTYPE(), CODEID: systemDefault.CODEID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.creatSysTransReport, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.openSupportingDocument = function() {
        self.modalFileData({
            uploaded: {},
            file: null
        });
        self.modalDocumentCategory('DOC_CATEGORY-1');
        $('#uploadedInfo').val('');

        window.location.href = ah.config.url.openSupportingDocument;
    };
    self.addSupportingDocument = function() {
        var uploaded = self.modalFileData().uploaded;
        uploaded.CATEGORY = self.modalDocumentCategory();

        self.uploadedList.push(uploaded);
        self.uploadedFiles.push(self.modalFileData().file);

        self.uploadedListWithCategory([]);
        for (var i = 0; i < self.documentCategories().length; i++) {
            var filtereds = self.uploadedList().filter(function(item) { return item.CATEGORY == self.documentCategories()[i].LOV_LOOKUP_ID });
            var category = self.documentCategories()[i];
            category.documents = filtereds;
            self.uploadedListWithCategory.push(category);
        }

        window.location.href = ah.config.url.modalClose;
    };
    self.initialize = function () {
        window.location.href = ah.config.url.modalClose;

        self.pageMode(self.modes.homePage);
        self.toggleBoolean('showProgress', true, 'page');
        self.toggleBoolean('showProgressScreen', true);

        $(ah.config.id.divNotify).css('display', 'none');

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        postData = { LOV: "'STOCKBATCH_STATUSTYPE','RECALLBY','HH-Level','RPT_ACTION','PROBABILITY','DETECTABILITY','DDSA','QSA','DOC_CATEGORY'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchSetupLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    $('#uploadedInfo').change(function (e) {
        var files = e.target.files;
        if (files.length != 0) {
            if (imageExtensions.test(files[0].name) || tifExtensions.test(files[0].name) || officeExtensions.test(files[0].name) || unsupportedExtension.test(files[0].name)) {
                var myFile = document.getElementById('uploadedInfo').value;
                var myFileData = document.getElementById('uploadedInfo');
                var fName = null;
                var fileType = null;
                if (myFile.length > 0) {
                    var fName = myFileData.files[0].name;
                    fileType = fName.substring(fName.lastIndexOf('.') + 1, fName.length);
                    $("#SAVEACTION").val(fileType.toUpperCase());
                } else {
                    self.modalFileData({
                        uploaded: {},
                        file: null
                    });
                    return;
                }
                var fnameCopy = fName;
                if (self.sysTransReport.ID())
                    fnameCopy = self.sysTransReport.ID() + '_' + fnameCopy;

                var found = self.uploadedList().find(function(item) { return item.DOCPATH == fName || item.DOCPATH == fnameCopy });
                found = found || self.documentList().find(function(item) { return item.DOCPATH == fName || item.DOCPATH == fnameCopy });
                if (found) {
                    alert('The Document Name is already Exist, please rename or select other file to upload.');
                    document.getElementById("uploadedInfo").value = "";
                    self.modalFileData({
                        uploaded: {},
                        file: null
                    });
                    return;
                }

                self.modalFileData({
                    uploaded: {
                        SYSREPORTID: 0,
                        DOCPATH: fName
                    },
                    file: files[0]
                });
            } else {
                alert('File not supported.');
                $(this).val('');
                self.modalFileData({
                    uploaded: {},
                    file: null
                });
            }
        }
    });

    self.webApiCallbackStatus = function (jsonData) {
        if (jsonData.ERROR) {
            self.toggleBoolean('showProgress', false);
            self.toggleBoolean('showProgressScreen', false);

            $(ah.config.id.divNotify).css('display', 'none');

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
        }
        else if (!jsonData.ERROR) {
            switch (self.pageMode()) {
                case self.modes.homePage:
                    self.toggleBoolean('showProgress', false);
                    self.toggleBoolean('showProgressScreen', false);

                    var recallBys = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'RECALLBY' });;
                    recallBys.unshift({
                        LOV_LOOKUP_ID: null,
                        DESCRIPTION: '- select -'
                    });
                    self.recallBys(recallBys);

                    var recallTypes = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'STOCKBATCH_STATUSTYPE' });;
                    recallTypes.unshift({
                        LOV_LOOKUP_ID: null,
                        DESCRIPTION: '- select -'
                    });
                    self.recallTypes(recallTypes);

                    var HHLevels = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'HH-Level' });;
                    HHLevels.unshift({
                        LOV_LOOKUP_ID: null,
                        DESCRIPTION: '- select -'
                    });
                    self.HHLevels(HHLevels);

                    var typesOfAction = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'RPT_ACTION' });;
                    typesOfAction.unshift({
                        LOV_LOOKUP_ID: null,
                        DESCRIPTION: '- select -'
                    });
                    self.typesOfAction(typesOfAction);

                    var probabilities = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'PROBABILITY' });;
                    probabilities.unshift({
                        LOV_LOOKUP_ID: null,
                        DESCRIPTION: '- select -'
                    });
                    self.probabilities(probabilities);

                    var detectabilities = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'DETECTABILITY' });;
                    detectabilities.unshift({
                        LOV_LOOKUP_ID: null,
                        DESCRIPTION: '- select -'
                    });
                    self.detectabilities(detectabilities);

                    var DDSA = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'DDSA' });;
                    DDSA.unshift({
                        LOV_LOOKUP_ID: null,
                        DESCRIPTION: '- select -'
                    });
                    self.DDSA(DDSA);

                    var QSA = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'QSA' });;
                    QSA.unshift({
                        LOV_LOOKUP_ID: null,
                        DESCRIPTION: '- select -'
                    });
                    self.QSA(QSA);

                    var documentCategories = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'DOC_CATEGORY' });;
                    //documentCategories.unshift({
                    //    LOV_LOOKUP_ID: null,
                    //    DESCRIPTION: '- select -'
                    //});
                    self.documentCategories(documentCategories);
                    break;

                case self.modes.searchPage:
                    self.toggleBoolean('showProgress', false);

                    self.transReports(jsonData.SYSTEMTRANSACTION_REPORTS);
                    self.generateDataTable();
                    break;

                case self.modes.transactionPage:

                    switch (self.currentMode()) {

                        case self.modes.searchTransReportItems:
                            self.currentMode(self.modes.viewSysTransReport);
                            self.toggleBoolean('showProgress', false);

                            self.documentListWithCategory([]);
                            self.uploadedListWithCategory([]);
                            self.documentList([]);
                            self.uploadedList([]);
                            $('#uploadedInfo').val('');

                            self.setObservableValue(self.sysTransReport, jsonData.REPORT_DETAILS);
                            for (var i = 0; i < jsonData.REPORT_SCANDOCS.length; i++) {
                                jsonData.REPORT_SCANDOCS[i].documentURL = window.location.origin + '/TransactionReportFiles/' + jsonData.REPORT_SCANDOCS[i].DOCPATH;
                                self.documentList.push(jsonData.REPORT_SCANDOCS[i]);
                            }

                            for (var i = 0; i < self.documentCategories().length; i++) {
                                var filtereds = self.documentList().filter(function(item) { return item.CATEGORY == self.documentCategories()[i].LOV_LOOKUP_ID });
                                var category = self.documentCategories()[i];
                                category.documents = filtereds;
                                self.documentListWithCategory.push(category);
                            }

                            if (self.sysTransReport.REPORTEDDATE())
                                self.sysTransReport.REPORTEDDATEDISPLAY(moment(self.sysTransReport.REPORTEDDATE()).format('DD/MM/YYYY'));
                            if (self.sysTransReport.ACTIONDATE())
                                self.sysTransReport.ACTIONDATEDISPLAY(moment(self.sysTransReport.ACTIONDATE()).format('DD/MM/YYYY'));
                            if (self.sysTransReport.CLOSEDATE())
                                self.sysTransReport.CLOSEDATEDISPLAY(moment(self.sysTransReport.CLOSEDATE()).format('DD/MM/YYYY'));

                            CKEDITOR.instances.REPORTS.setData(self.sysTransReport.REPORTS());
                            CKEDITOR.instances.ACTIONREPORTS.setData(self.sysTransReport.ACTIONREPORTS());
                            CKEDITOR.instances.CLOSEREPORTS.setData(self.sysTransReport.CLOSEREPORTS());
                            CKEDITOR.instances.REPORTS.setReadOnly(true);
                            CKEDITOR.instances.ACTIONREPORTS.setReadOnly(true);
                            CKEDITOR.instances.CLOSEREPORTS.setReadOnly(true);
                            if (self.sysTransReport.REPORTSTATUS() == "For Reporting")
                                CKEDITOR.instances.REPORTS.setReadOnly(false);

                            self.transReportItems(jsonData.ITEM_BATCHES_V || jsonData.ASSET_V);
                            break;

                        case self.modes.searchItems:
                            self.toggleBoolean('showProgress', false);

                            self.transReportItems(jsonData.ITEM_BATCHES_V || jsonData.ASSET_V);
                            break;

                        case self.modes.createNewRecall:
                            var frmData = new FormData();
                            frmData.append("REF_ID", jsonData.SYSTEMTRANSACTION_REPORT.ID);
                            frmData.append("pathLocation", "~/TransactionReportFiles");
                            self.uploadedFiles.forEach(function (file, i) {
                                frmData.append('file' + i, file);
                            });
                            $.ajax({
                                url: '/Upload/SaveDocument',
                                type: "POST",
                                contentType: false,
                                processData: false,
                                data: frmData,
                                success: function (data) {
                                    self.currentMode(self.modes.viewSysTransReport);
                                    self.toggleBoolean('showProgress', false);
                                    self.toggleBoolean('showProgressScreen', false);

                                    $(ah.config.id.divNotify).css('display', 'block');
                                    $(ah.config.id.divNotify + ' p').html(systemText.saveSuccessMessage);
                                    setTimeout(function () {
                                        $(ah.config.id.divNotify).fadeOut(1000);
                                    }, 3000);

                                    self.setSelectedSysTransReport(jsonData.SYSTEMTRANSACTION_REPORT);
                                },
                                error: function (err) {
                                    alert(error);
                                }
                            });
                            break;
                    }
                    break;
            }
        }
        else {
            self.toggleBoolean('showProgress', false);
            self.toggleBoolean('showProgressScreen', false);

            $(ah.config.id.divNotify).css('display', 'none');

            alert(systemText.errorTwo);
        }
    };

    self.initialize();
};