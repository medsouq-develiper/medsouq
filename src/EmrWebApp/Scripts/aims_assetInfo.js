﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            detailItem: '.detailItem',
            searhButton: '.searhButton',
            lookupDetailItem: '.lookupDetailItem',
            info1: '.info1',
            info2: '.info2',
            info3: '.info3',
            info4: '.info4',
            info5: '.info5',
            info6: '.info6',
            infoRiskLS: '.infoRiskLS',
            infoRiskUMR: '.infoRiskUMR',
            infoRiskME: '.infoRiskME',
            iconDet: '.iconDet',
            searchIcon: '.searchIcon',
            riskScoreInput: '.riskScoreInput',
            pagingDiv: '.pagingDiv',
            liCloneList: '.liCloneList',
            liClone: '.liClone',
            lisort: '.lisort',
            liPrint: '.liPrint',
            liassetstatusdisplay: '.liassetstatusdisplay',
            barcode: '.barcode',
            liDisplayBy: '.liDisplayBy',
            divCostCenter: '.divCostCenter',
            divCompany: '.divCompany'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul',
            tbody: 'tbody',
            inputTypeNumber: 'input[type=number]'
        },
        tagId: {
            btnCSummary: 'btnCSummary',
            btnLCycle: 'btnLCycle',
            btnHOperation: 'btnHOperation',
            btnANotes: 'btnANotes',
            btnSComponent: 'btnSComponent',
            btnAPersonnel: 'btnAPersonnel',
            btnCagetory: 'btnCagetory',
            btnManufacturer: 'btnManufacturer',
            btnManufacturerModel: 'btnManufacturerModel',
            btnSupplier: 'btnSupplier',
            btnAgent: 'btnAgent',
            btnBuilding: 'btnBuilding',
            btnCostCenter: 'btnCostCenter',
            btnResponsibleCenter: 'btnResponsibleCenter',
            btnLocation: 'btnLocation',
            btnDepreciation: 'btnDepreciation',
            lookUpnextPage: 'lookUpnextPage',
            lookUppriorPage: 'lookUppriorPage',
            lookUptxtGlobalSearchOth: 'lookUptxtGlobalSearchOth',
            PaginGnextPage: 'PaginGnextPage',
            PaginGpriorPage: 'PaginGpriorPage',
            searchAllLOVList: 'searchAllLOVList'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,

            save: 4,
            read: 5,
            display: 6,
            edit: 7,
            advanceSearch: 8
        },
        id: {
            assetPrintSearchResultList: '#assetPrintSearchResultList',
            loadingBackground: '#loadingBackground',
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmInfo: '#frmInfo',
            itemInfo: "#itemInfo",
            assetCategory: '#CATEGORY',
            staffID: '#STAFF_ID',
            serviceID: '#SERVICE_ID',
            searchResultLookUp: '#searchResultLookUp',
            successMessage: '#successMessage',
            serviceDept: '#SERV_DEPT',
            assetSpecialty: '#SPECIALTY',
            assetClass: '#ASSET_CLASSIFICATION',
            lovAction: '#LOVACTION',
            assetManufacturer: '#MANUFACTURER',
            assetSupplier: '#SUPPLIER',
            current_Agent: '#Current_Agent',
            assetBuilding: '#BUILDING',
            assetFacility: '#FACILITY',
            assetCostCenter: '#COST_CENTER',
            assetResponsibleCenter: '#RESPONSIBLE_CENTER',
            assetCondition: '#CONDITION',
            assetccCondition: '#COSTCENTERASSETCOND',
            assetLocation: '#LOCATION',
            assetClassType: '#CLASS_TYPE',
            returnWO: '#RETURNWO',
            historyWO: '#historyWO',
            empPrimary: '#PRIMARY_EMPLOYEE',
            empSecondary: '#SECONDARY_EMPLOYEE',
            riskGroup: '#RISKGROUP',
            riskCLA: '#RISKCLA',
            riskCLS: '#RISKCLS',
            riskCMT: '#RISKCMT',
            riskEQF: '#RISKEQF',
            riskESE: '#RISKESE',
            riskEVS: '#RISKEVS',
            riskEVT: '#RISKEVT',
            riskFPF: '#RISKFPF',
            riskLIS: '#RISKLIS',
            riskMAR: '#RISKMAR',
            riskPCA: '#RISKPCA',
            inforiskGroup: '#INFORISKGROUP',
            inforiskCLA: '#INFORISKCLA',
            inforiskCLS: '#INFORISKCLS',
            inforiskCMT: '#INFORISKCMT',
            inforiskEQF: '#INFORISKEQF',
            inforiskESE: '#INFORISKESE',
            inforiskEVS: '#INFORISKEVS',
            inforiskEVT: '#INFORISKEVT',
            inforiskFPF: '#INFORISKFPF',
            inforiskLIS: '#INFORISKLIS',
            inforiskMAR: '#INFORISKMAR',
            inforiskPCA: '#INFORISKPCA',
            btnManufacturerModel: '#btnManufacturerModel',
            inforiskCLAScore: '#INFORISKCLASCORE',
            inforiskCLSScore: '#INFORISKCLSSCORE',
            inforiskCMTScore: '#INFORISKCMTSCORE',
            inforiskEQFScore: '#INFORISKEQFSCORE',
            inforiskESEScore: '#INFORISKESESCORE',
            inforiskEVSScore: '#INFORISKEVSSCORE',
            inforiskEVTScore: '#INFORISKEVTSCORE',
            inforiskFPFScore: '#INFORISKFPFSCORE',
            inforiskLISScore: '#INFORISKLISSCORE',
            inforiskMARScore: '#INFORISKMARSCORE',
            inforiskPCAScore: '#INFORISKPCASCORE',
            riskFactorScore: '#riskFactorScore',
            lookUptxtGlobalSearchOth: '#lookUptxtGlobalSearchOth',
            lookUppageNum: '#lookUppageNum',
            lookUppageCount: '#lookUppageCount',
            warranty_cover: '#WARRANTY_INCLUDE',
            ccwarranty_cover: '#CCWARRANTY_INCLUDE',
            filterCondition: '#filterCondition',
            PaginGpageNum: '#PaginGpageNum',
            PaginGpageCount: '#PaginGpageCount',
            assetModify: '#ASSETMODIFY',
            addButtonIcon: '#addButtonIcon',
            noteHistory: '#noteHistory',
            noteEntry: '#noteEntry',
            woReferenceNotes: '#WO_REFERENCE_NOTES',
            warrantyList: '#warrantyList',
            ccwarrantyList: '#ccwarrantyList',
            addWarranty: '#addWarranty',
            addccWarranty: '#addccWarranty',
            UpdateButtonIcon: '#UpdateButtonIcon',
            addConeButtonIcon: '#addConeButtonIcon',
            sortPage: '#SORTPAGE',
            costcenterStrList: '#costcenterStrList',
            manufacturerStrList: '#manufacturerStrList',
            supplierStrList: '#supplierStrList',
            printSummary: '#printSummary',
            assetPrintSticker: '#assetPrintSticker',
            assetReturnSticker: '#assetReturnSticker',
            assetOutOrderSticker: '#assetOutOrderSticker',
            searchDepreciationCal: '#searchDepreciationCal',
            searchResultList: '#searchResultList',
            searchResultAsset: '#searchResultAsset',
            displayBy: '#DISPLAYTYPE',
            btnCostCenter: '#btnCostCenter',
            constcenterTransId: "#COSTCENTERTRANSID",
            returntoCompany: "#returntoCompany"
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID',
            datalookUp: 'data-lookUp',
            required: 'required="required"'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'searchResult',
            assetDetails: 'AM_ASSET_DETAILS',
            assetManufacturerList: 'AM_MANUFACTURER',
            lovList: 'LOV_LOOKUPS',
            assetSupplierList: 'AM_SUPPLIER',
            assetNO: 'ASSET_NO',
            assetCurrentAgent: 'CURRENTAGENT',
            groupPrivileges: 'GROUP_PRIVILEGES',
            woAssetSummary: 'ASSET_WOSUMMARY_V',
            assetManufacturerModelList: 'AM_MANUFACTURER_MODEL',
            userBuilding: 'USER_BUILDING',
            defaultCostCenter: '0ed52a42-9019-47d1-b1a0-3fed45f32b8f',
            costcenterTransaction: 'AM_ASSET_TRANSACTION',
            userClient: 'USER_CLIENT'
        },
        url: {
            homeIndex: '/Home/Index',
            modalAddItemsInfo: '#openModalInfo',
            modalLookUp: '#openModalLookup',
            aimsPMSchedule: '/aims/amPMSchedule',
            aimsInfoServices: '/aims/amInformationServices',
            aimsContractServices: '/aims/amContractServices',
            aimsWorkOrder: '/aims/amWorkOrder',
            modalClose: '#',
            aimsRiskFactor: '#openModalRiskFactor',
            aimsHomepage: '/Menuapps/aimsIndex',
            aimsDetails: '/aims/assetInfo#',
            aimsImages: '/Upload/UploadFile',
            modalFilter: '#openModalFilter',
            aimsWorkOrderForClose: '/aims/amWorkOrderForClose',
            aimsInformation: '/aims/assetInfo',
            openModalClone: '#openModalClone',
            openModalCloneGenerator: '#openModalCloneGenerator',
            openModelReturnOnHand: '#openModelReturnOnHand'

        },
        fld: {
            assetNoInfo: 'ASSETNO',
            assetDescInfo: 'ASSETDESC',
            reqWONo: 'REQNO',
            fromDashBoard: 'asdfxhsadfsdfx'
        },
        api: {
            readSetup: '/AIMS/ReadAssetInfo',
            searchAll: '/AIMS/SearchAssets',
            searchCostSumm: '/AIMS/SearchAssetConstSummary',
            createSetup: '/AIMS/CreateNewAsset',
            updateSetup: '/AIMS/UpdateAssetInfo',
            searchLov: '/AIMS/SearchAimsLOV',
            searchLovInitialize: '/AIMS/SearchAimsLOVInitialize',
            searchManufacturer: '/AIMS/SearchManufacturerLookUp',
            searchSupplier: '/AIMS/SearchSuppier',
            getListLOV: '/Search/SearchListOfValueCategory',
            searchStaffList: '/Staff/SearchStaffLookUp',
            searchAssetNotes: '/AIMS/SearchAssetNotes',
            searchWONotes: '/AIMS/SearchAssetWONotes',
            searchModel: '/Search/SearchModelManufacturer',
            searchLOVQuery: '/AIMS/SearchAimsLOVQuery',
            createClone: '/AIMS/CreateCloneAssetInfo',
            getCloneLOV: '/Search/SearchListOfValueClone',
            updateAssetClone: '/AIMS/UpdateCloneAssetInfo',
            seachByCloneId: '/AIMS/SearchAssetsCloneId',
            readClientInfo: '/Setup/ReadClientInfo',
            getAssetPartsDP: '/Reports/SearchByAssetPartsYR',
            searchByAssets: '/AIMS/SearchAssets',
            updateTransaction: '/AIMS/UpdateAssetTransactionReturn'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplate: '<span class="search-row-label fc-light-grey-2">{0}: <span class="search-row-data">{1}</span></span>',
            searchExistRowHeaderTemplate: "<a href='#' class='fs-medium-normal lookupDetailItem fc-slate-blue' data-lookUp='{0}' >&nbsp;&nbsp;&nbsp;&nbsp;  <i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} </span></a>",
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liassetstatusdisplay + ',' + thisApp.config.cls.liPrint + ',' + thisApp.config.cls.liClone + ',' + thisApp.config.cls.liCloneList + ',' + thisApp.config.cls.pagingDiv + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess + ',' + thisApp.config.cls.iconDet + ',' + thisApp.config.id.returnWO + ',' + thisApp.config.id.addWarranty + ',' + thisApp.config.id.addccWarranty + ',' + thisApp.config.cls.barcode + ',' + thisApp.config.cls.lisort + ',' + thisApp.config.cls.divCostCenter + ',' +
            thisApp.config.id.returntoCompany
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();
        document.getElementById("LOCATION_DATE").disabled = true;
        document.getElementById("INSERVICE_DATE").disabled = true;
        document.getElementById("CONDITION_DATE").disabled = true;
        document.getElementById("PURCHASE_COST").disabled = true;
        thisApp.CurrentMode = thisApp.config.mode.idle;
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";

    };

    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liClone + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.searhButton + ',' + thisApp.config.id.addWarranty + ',' + thisApp.config.id.addccWarranty).show();

        $(
            thisApp.config.cls.liassetstatusdisplay + ',' + thisApp.config.cls.liCloneList + ',' + thisApp.config.cls.pagingDiv + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.iconDet + ',' + thisApp.config.cls.divProgress + ',' + thisApp.config.cls.lisort + ',' + thisApp.config.cls.liDisplayBy + ',' + thisApp.config.cls.divCostCenter + ',' + thisApp.config.id.btnCostCenter + ',' + thisApp.config.cls.divCompany
        ).hide();
        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        //$(thisApp.config.tag.inputTypeText).prop(thisApp.config.attr.readOnly, false);

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);
        document.getElementById("ASSET_NO").readOnly = true;
        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("LOCATION_DATE").disabled = false;
        document.getElementById("INSERVICE_DATE").disabled = false;
        document.getElementById("CONDITION_DATE").disabled = false;
        document.getElementById("PURCHASE_COST").disabled = false;
        document.getElementById("CATEGORYDESC").disabled = true;
        document.getElementById("MANUFACTURERDESC").disabled = true;
        document.getElementById("SUPPLIERDESC").disabled = true;
        document.getElementById("CURRENTAGENTDESC").disabled = true;
        document.getElementById("BUILDINGDESC").disabled = true;
        document.getElementById("RESPONSIBLE_CENTERDESC").disabled = true;
        document.getElementById("COST_CENTERDESC").disabled = true;
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";
        document.getElementById("warrantyList").style["pointer-events"] = "visible";
        document.getElementById("ccwarrantyList").style["pointer-events"] = "visible";

        document.getElementById("btnCostCenter").style["display"] = "inline-block";
        document.getElementById("btnResponsibleCenter").style["display"] = "none";

    };

    thisApp.toggleAddExistMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;


        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.searhButton).show();

        $(
            thisApp.config.cls.liCloneList + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.statusText +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.iconDet + ',' + thisApp.config.cls.divProgress
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        //$(
        //    thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        // ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("LOCATION_DATE").disabled = false;
        document.getElementById("INSERVICE_DATE").disabled = false;
        document.getElementById("CONDITION_DATE").disabled = false;
        document.getElementById("PURCHASE_COST").disabled = false;

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liDisplayBy).show();

        $(
            thisApp.config.cls.liClone + ',' + thisApp.config.cls.liCloneList + ',' + thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.id.searchResultAsset + ',' + thisApp.config.cls.lisort + ',' + thisApp.config.id.returntoCompany
        ).hide();
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";

    };

    thisApp.displaySearchResultAssets = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $( thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.lisort + ',' + thisApp.config.id.searchResultAsset).show();
        $(thisApp.config.cls.liAddNew + ',' +thisApp.config.cls.liClone + ',' + thisApp.config.cls.liCloneList + ',' + thisApp.config.cls.liApiStatus + ',' + thisApp.config.id.searchResultList).hide();
        $(thisApp.config.cls.lisort + ',' + thisApp.config.cls.liDisplayBy).removeClass(thisApp.config.cssCls.viewMode);
        $(thisApp.config.id.sortPage + ',' + thisApp.config.id.displayBy).prop(thisApp.config.attr.disabled, false);

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $( thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.id.searchResultList).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liClone + ',' + thisApp.config.cls.liCloneList + ',' + thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.lisort + ',' + thisApp.config.id.returntoCompany).hide();
        $(thisApp.config.cls.lisort + ',' + thisApp.config.cls.liDisplayBy).removeClass(thisApp.config.cssCls.viewMode);
        $(thisApp.config.id.sortPage + ',' + thisApp.config.id.displayBy).prop(thisApp.config.attr.disabled, false);

    };
    thisApp.toggleReturnWO = function () {
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.searchIcon + ',' + thisApp.config.id.txtGlobalSearch).hide();
        $(thisApp.config.id.returnWO).show();
    }
    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.searhButton + ',' + thisApp.config.id.addWarranty + ',' + thisApp.config.id.addccWarranty).show();
        $(thisApp.config.cls.liassetstatusdisplay + ',' + thisApp.config.cls.liClone + ',' + thisApp.config.cls.liCloneList + ',' + thisApp.config.cls.pagingDiv + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("LOCATION_DATE").disabled = false;
        document.getElementById("INSERVICE_DATE").disabled = false;
        document.getElementById("CONDITION_DATE").disabled = false;
        document.getElementById("PURCHASE_COST").disabled = false;
        document.getElementById("CATEGORYDESC").disabled = true;
        document.getElementById("MANUFACTURERDESC").disabled = true;
        document.getElementById("SUPPLIERDESC").disabled = true;
        document.getElementById("CURRENTAGENTDESC").disabled = true;
        document.getElementById("BUILDINGDESC").disabled = true;
        document.getElementById("RESPONSIBLE_CENTERDESC").disabled = true;
        document.getElementById("COST_CENTERDESC").disabled = true;
        document.getElementById("warrantyList").style["pointer-events"] = "visible";
        document.getElementById("ccwarrantyList").style["pointer-events"] = "visible";

        document.getElementById("btnResponsibleCenter").style["display"] = "none";

    };
    thisApp.togglehideOthInfo = function () {
        $(thisApp.config.cls.info1 + ',' + thisApp.config.cls.info2 + ',' + thisApp.config.cls.info3 + ',' + thisApp.config.cls.info4 + ',' + thisApp.config.cls.info5 + ',' + thisApp.config.cls.info6).hide();
    };
    thisApp.toggleShowInfo1 = function () {
        $(thisApp.config.cls.info1).show();
    };
    thisApp.toggleShowInfo2 = function () {
        $(thisApp.config.cls.info2).show();
    };
    thisApp.toggleShowInfo3 = function () {
        $(thisApp.config.cls.info3).show();
    };
    thisApp.toggleShowInfo4 = function () {
        $(thisApp.config.cls.info4).show();
    };
    thisApp.toggleShowInfo5 = function () {
        $(thisApp.config.cls.info5).show();
    };
    thisApp.toggleShowInfo6 = function () {
        $(thisApp.config.cls.info6).show();
    };
    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.searhButton + ',' + thisApp.config.id.addWarranty + ',' + thisApp.config.id.addccWarranty
        ).hide();
        document.getElementById("LOCATION_DATE").disabled = true;
        document.getElementById("INSERVICE_DATE").disabled = true;
        document.getElementById("CONDITION_DATE").disabled = true;
        document.getElementById("PURCHASE_COST").disabled = true;
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liassetstatusdisplay + ',' + thisApp.config.cls.liCloneList + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.iconDet + ',' + thisApp.config.id.addWarranty + ',' + thisApp.config.id.addccWarranty + ',' + thisApp.config.cls.divCostCenter
        ).show();

        $(
            thisApp.config.cls.liPrint + ',' + thisApp.config.cls.pagingDiv + ',' + thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.searhButton + ',' + thisApp.config.id.addWarranty + ',' + thisApp.config.id.addccWarranty + ',' + thisApp.config.cls.lisort + ',' + thisApp.config.cls.liDisplayBy
        ).hide();

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        //$(thisApp.config.tag.inputTypeText).prop(thisApp.config.attr.readOnly, true);

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);
        document.getElementById("LOCATION_DATE").disabled = true;
        document.getElementById("INSERVICE_DATE").disabled = true;
        document.getElementById("CONDITION_DATE").disabled = true;
        document.getElementById("PURCHASE_COST").disabled = true;
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";
        document.getElementById("warrantyList").style["pointer-events"] = "none";
        document.getElementById("ccwarrantyList").style["pointer-events"] = "none";

        var infoDetails = sessionStorage.getItem(thisApp.config.skey.assetDetails);
        infoDetails = JSON.parse(infoDetails);
        if (infoDetails.CLONE_ID != null && infoDetails.CLONE_ID != "") {
            $(thisApp.config.cls.liCloneList).show();
        } else {
            $(thisApp.config.cls.liCloneList).hide();
        }
    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.getAge = function (birthDate) {

        var seconds = Math.abs(new Date() - birthDate) / 1000;

        var numyears = Math.floor(seconds / 31536000);
        var numdays = Math.floor((seconds % 31536000) / 86400);
        var nummonths = numdays / 30;

        return numyears + " y " + Math.round(nummonths) + " m ";
    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };
    thisApp.formatJSONDateToString = function () {

        var dateToFormat = arguments[0];
        var strDate;

        if (dateToFormat === null) return "";
        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };

    thisApp.formatJSONTimeToString = function () {

        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        var timeToFormat = strTime[1].substring(0, 5);


        return timeToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');


            if (isDateTime) {
                val = thisApp.formatJSONDateTimeToString(val);
            }

            if (isDate) {
                val = thisApp.formatJSONDateToString(val);
            }

            var $el = $('#' + name), type = $el.attr('type'), isTime = $el.attr('data-istime');
            if (isTime) {
                if (val === null || val === "") {
                    val = "00:00";
                } else {
                    val = thisApp.formatJSONTimeToString(val);
                }

            }

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var assetInfoViewModel = function (systemText) {

    var self = this;
    document.onkeydown = function (e) {
        if (event.keyCode == 123) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
            return false;
        }
    }

    self.clientInfo = {
        Description: ko.observable('Biomedical Management System'),
        clientLogo: ko.observable('../images/ClientLogo.png'),
        buildingCode: ko.observable(""),
        isVisible: ko.observable(false)
    };
    var xElements = document.getElementById("frmInfo").elements.length;
    var ah = new appHelper(systemText);
    var sort = "SORTASSETDESCAC";
    self.mandatory = ko.observable("false");
    
    var curCostCenter = null;
    var srDataArray = ko.observableArray([]);
    var warrantyList = ko.observableArray([]);
    var ccwarrantyList = ko.observableArray([]);
    var buildingList = ko.observableArray([]);
    var costCenterList = ko.observableArray([]);
    var lovCostCenterArray = ko.observableArray([]);
    var lovManufacturerArray = ko.observableArray([]);
    var lovSupplierArray = ko.observableArray([]);
    var partsDPUsedList = ko.observableArray([]);
    var Scostcenter = null;
    var Scloneid = null;
    var Sstatus = null;
    var Scondition = null;
    var SsearchTxt = null;
    var resultHtml = null;
    var Spono = null;
    self.DefaultCostCenter = {
        CODE: ko.observable(""),
        DESCRIPTION: ko.observable("")
    }
    self.CostCenter = {
        Name: ko.observable("")
    };
    self.warrantyLOV = ko.observableArray([]);
    self.ccwarrantyLOV = ko.observableArray([]);
    self.warrantyData = ko.observableArray([]);
    self.ccwarrantyData = ko.observableArray([]);
    self.cloneAssetData = ko.observableArray('');
    self.showRow = ko.observable(true);
    self.isPrint = 1;

   
    self.showLoadingBackground = function () {
        var loadingBackground = $(ah.config.id.loadingBackground);
        loadingBackground.addClass('initializing').removeClass('done');
        var offset = loadingBackground.offset();
        var height = loadingBackground.height();
        var centerY = (offset.top + height / 2) - 120;
        $('.sk-circle').css({
            'margin-top': centerY + 'px',
            'display': 'block'
        });
    };
    self.initialize = function () {


        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());
        var userClientList = JSON.parse(sessionStorage.getItem(ah.config.skey.userClient));
        if (userClientList.length == 1) {
            self.clientInfo.Description = userClientList[0].Description;
            self.clientInfo.clientLogo = userClientList[0].LogoPath;
            self.clientInfo.isVisible = true;
        }
        var dCC = JSON.parse(sessionStorage.getItem(ah.config.skey.defaultCostCenter));
        if (dCC.length > 0) {
            self.DefaultCostCenter.CODE = dCC[0].CODEID;
            self.DefaultCostCenter.DESCRIPTION = dCC[0].DESCRIPTION;
            self.searchFilter.companyAccess = dCC[0].CODEID;
        }
        ah.initializeLayout();

        self.showLoadingBackground();

        $.when(self.getLov()).done(function () {
            $(ah.config.id.loadingBackground).removeClass('initializing').addClass('done');
            $('.dark-bg, ' + ah.config.id.loadingBackground).css({
                'top': '90px',
                'z-index': '0'
            });
        });
    };
    self.alertMessage = function (refMessage) {
        $("#alertmessage").text(refMessage);
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "block";

    };

    self.dateClick = function () {
        $("#dateClickAction").val("ACTIVE");
    };
    self.getLov = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        ah.CurrentMode = ah.config.mode.idle;
        postData = { LOV: "'AM_WARRANTY','AIMS_FACILITY','AIMS_SERVICEDEPARTMENT','AIMS_SPECIALTY','AIMS_CLASS','AIMS-CONDITION','AIMS_LOCATION','AIMS_CLASSTYPE'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchLovInitialize, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    }
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.createNew = function () {

        ah.toggleAddMode();
        var rowShow = true;
        $("#currentRow").val(0);
        var jDate = new Date();
        var createDate = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        self.warrantyData.splice(0, 5000);
        self.warrantyData.push({
            ROWCOUNT: 1,
            FREQUENCY: null,
            FREQUENCY_PERIOD: 0,
            WARRANTY_NOTES: null,
            WARRANTY_EXPIRY: null,
            WARRANTY_INCLUDED: null,
            ID: 0,
            ASSET_NO: null,
            WARRANTY_EXTENDED: null,
            CREATED_BY: staffLogonSessions.STAFF_ID,
            CREATED_DATE: createDate,
            SHOWROW: rowShow,
            warrantyLOV: self.warrantyLOV

        });


        self.ccwarrantyData.splice(0, 5000);
        self.ccwarrantyData.push({
            CCROWCOUNT: 1,
            CCFREQUENCY: null,
            CCFREQUENCY_PERIOD: 0,
            CCWARRANTY_NOTES: null,
            CCWARRANTY_EXPIRY: null,
            CCWARRANTY_INCLUDED: null,
            CCID: 0,
            ASSET_NO: null,
            CCWARRANTY_EXTENDED: null,
           
            CCSHOWROW: rowShow,
            ccwarrantyLOV: self.ccwarrantyLOV

        });


        $("#COST_CENTER").val(self.DefaultCostCenter.CODE);
        $("#COST_CENTERDESC").val(self.DefaultCostCenter.DESCRIPTION);
        $("#warrantyAction").val("Y");
        $("#ccwarrantyAction").val("Y");
        $("#CLONE_ID").val(null);
        $("#CloneIds").text("Clone Id: ");
        self.privilegesValidation('NEWCLONE');
    };
    self.assetCategory = {
        count: ko.observable()
    };
    xElements = document.getElementById("frmInfo").elements.length;
    self.searchFilter = {
        AssetFilterCondition: ko.observable(''),
        toDate: ko.observable(''),
        status: ko.observable('I'),
        staffId: ko.observable(''),
        CloneId: ko.observable(''),
        CostCenterStr: ko.observable(''),
        ManufacturerStr: ko.observable(''),
        SupplierStr: ko.observable(''),
        PO_NO: ko.observable(''),
        AssetFilterCostCenter: ko.observable(''),
        assetDescription: ko.observable(''),
        companyAccess: ko.observable(''),
    };
    self.printStickerDetails = {
        CLIENTNAME: ko.observable(),
        CLIENT_LOGO: ko.observable(),
        ASSET_NO: ko.observable(),
        ASSET_NOBarcode: ko.observable(),
        DESCRIPTION: ko.observable(),
        SERIAL_NO: ko.observable(),
        MODEL: ko.observable(),
        COST_CENTER: ko.observable(),
        LOCATION: ko.observable(),
        ReqDate: ko.observable(),
    };
    self.clientDetails = {
        CLIENTNAME: ko.observable(),
        CLIENT_LOGO: ko.observable(),
        COMPANY_LOGO: ko.observable(),
        HEADING: ko.observable(),
        HEADING2: ko.observable(),
        HEADING3: ko.observable(),
        HEADING_OTH: ko.observable(),
        HEADING_OTH2: ko.observable(),
        HEADING_OTH3: ko.observable(),
        CLIENTADDRESS: ko.observable(),
        CLIENTCONTACT: ko.observable()
    };
    self.searchFilterNow = function () {
        window.location.href = ah.config.url.modalClose;
        self.searchNow();
    };
    self.clearFilter = function () {
        self.searchFilter.AssetFilterCondition('');
        self.searchFilter.toDate('');
        self.searchFilter.status('');
        self.searchFilter.CloneId('');
        self.searchFilter.CostCenterStr('');
        self.searchFilter.ManufacturerStr('');
        self.searchFilter.SupplierStr('');
        self.searchFilter.PO_NO('');
        self.searchFilter.AssetFilterCostCenter('');
        self.searchFilter.assetDescription('');

    };
    self.showModalFilter = function () {
        ah.CurrentMode = ah.config.mode.advanceSearch;
        //$("#cloneid").removeClass(ah.config.cssCls.viewMode);
        var element = document.getElementById("cloneid");
        element.classList.remove("view-mode");
        element.disabled = false;
        var element2 = document.getElementById("costcenterStr");
        element2.classList.remove("view-mode");
        element2.disabled = false;

        var element3 = document.getElementById("filterStatus");
        element3.classList.remove("view-mode");
        element3.disabled = false;

        var element4 = document.getElementById("filterCondition");
        element4.classList.remove("view-mode");
        element4.disabled = false;

        var element4 = document.getElementById("purchaseNo");
        element4.classList.remove("view-mode");
        element4.disabled = false;

        var element2 = document.getElementById("manufacturerStr");
        element2.classList.remove("view-mode");
        element2.disabled = false;

        var element2 = document.getElementById("supplierStr");
        element2.classList.remove("view-mode");
        element2.disabled = false;

        window.location.href = ah.config.url.modalFilter;
    };
    self.infoLovLoader = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var senderID = $(arguments[1].currentTarget).attr('id');
        ah.togglehideOthInfo();
        $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');
        $("#lookUpnoresult").hide();
        $("#searchingLOV").hide();

        $(ah.config.id.lookUptxtGlobalSearchOth).val("");
        switch (senderID) {

            case ah.config.tagId.btnCagetory: {
                $("#lookUpTitle").text("Please Search from Device Type List");
                $(ah.config.id.lovAction).val("CATEGORY");
                $("#lookUpFilter").val('AIMS_DTYPE');

                window.location.href = ah.config.url.modalLookUp;
                break;
            }
            case ah.config.tagId.btnManufacturerModel: {
                var manufacturerDesc = $("#MANUFACTURERDESC").val();
                var manufacturerId = $("#MANUFACTURER").val();
                if (manufacturerId == null || manufacturerId == "") {
                    alert('Unable to proceed. Please select Manufacturer first.');
                    return;
                }
                $("#lookUpTitle").text("Please search Model List for Manufacturer " + manufacturerDesc);
                $(ah.config.id.lovAction).val("MANUFACTURERMODEL");
                $("#lookUpFilter").val('AIMS_DTYPE');
                window.location.href = ah.config.url.modalLookUp;
                break;
            }
            case ah.config.tagId.btnManufacturer: {
                $("#lookUpTitle").text("Please search from Manufacture List");
                $(ah.config.id.lovAction).val("MANUFACTURER");
                window.location.href = ah.config.url.modalLookUp;
                break;
            }
            case ah.config.tagId.btnSupplier: {
                ah.toggleShowInfo3();
                $("#lookUpTitle").text("Please search from Supplier List");
                $(ah.config.id.lovAction).val("SUPPLIER");
                window.location.href = ah.config.url.modalLookUp;
                break;
            }
            case ah.config.tagId.btnAgent: {
                ah.toggleShowInfo3();
                $("#lookUpTitle").text("Please search from Agent List");
                $(ah.config.id.lovAction).val("CurrentAgent");
                window.location.href = ah.config.url.modalLookUp;
                break;
            }
            case ah.config.tagId.btnBuilding: {
                $("#lookUpTitle").text("Please search from Building List");
                $(ah.config.id.lovAction).val("BUILDING");
                $("#lookUpFilter").val('AIMS_BUILDING');
                window.location.href = ah.config.url.modalLookUp;
                break;
            }
            case ah.config.tagId.btnCostCenter: {
                $("#lookUpTitle").text("Please search from Cost Center List");
                $(ah.config.id.lovAction).val("COSTCENTER");
                $("#lookUpFilter").val('AIMS_COSTCENTER');
                window.location.href = ah.config.url.modalLookUp;
                break;
            }
            case ah.config.tagId.btnResponsibleCenter: {

                $("#lookUpTitle").text("Please search from Responsible Department List");
                $(ah.config.id.lovAction).val("RESPONSIBLECENTER");
                $("#lookUpFilter").val('AIMS_COSTCENTER');
                window.location.href = ah.config.url.modalLookUp;
                break;
            }
            case ah.config.tagId.btnLocation: {
                $(ah.config.id.lovAction).val("LOCATION");
                self.displayLookup('AIMS_LOCATION');
                window.location.href = ah.config.url.modalLookUp;
                break;
            }
        }
    };
    self.infoRiskFactor = function () {
        $(ah.config.cls.infoRiskUMR + ',' + ah.config.cls.infoRiskME + ',' + ah.config.cls.infoRiskLS).hide();
        $(ah.config.id.inforiskCLAScore + ',' + ah.config.id.inforiskCLSScore
            + ',' + ah.config.id.inforiskCMTScore
            + ',' + ah.config.id.inforiskEQFScore
            + ',' + ah.config.id.inforiskESEScore
            + ',' + ah.config.id.inforiskEVSScore
            + ',' + ah.config.id.inforiskEVTScore
            + ',' + ah.config.id.inforiskFPFScore
            + ',' + ah.config.id.inforiskLISScore
            + ',' + ah.config.id.inforiskMARScore
            + ',' + ah.config.id.inforiskPCAScore).prop(ah.config.attr.disabled, true);

        var riskGroupId = $("#INFORISKGROUP").val();

        self.infoRiskFactorGetGroup(riskGroupId);


    };
    self.returnToAssetNotes = function () {
        $(ah.config.id.noteHistory).hide();
        $(ah.config.id.noteEntry).show();
    }
    self.infoRiskFactorGetGroup = function (riskGroupId) {
        //alert(riskGroupId);
        if (riskGroupId === 'ME') {
            $(ah.config.cls.infoRiskME).show();
        } else if (riskGroupId === 'UMR') {
            $(ah.config.cls.infoRiskUMR).show();

        } else if (riskGroupId === 'LS') {
            $(ah.config.cls.infoRiskLS).show();
        } else {

        }



        $(ah.config.id.inforiskCLA).val($(ah.config.id.riskCLA).val());
        $(ah.config.id.inforiskCLS).val($(ah.config.id.riskCLS).val());
        $(ah.config.id.inforiskCMT).val($(ah.config.id.riskCMT).val());
        $(ah.config.id.inforiskEQF).val($(ah.config.id.riskEQF).val());
        $(ah.config.id.inforiskESE).val($(ah.config.id.riskESE).val());
        $(ah.config.id.inforiskEVS).val($(ah.config.id.riskEVS).val());
        $(ah.config.id.inforiskEVT).val($(ah.config.id.riskEVT).val());
        $(ah.config.id.inforiskFPF).val($(ah.config.id.riskFPF).val());
        $(ah.config.id.inforiskLIS).val($(ah.config.id.riskLIS).val());
        $(ah.config.id.inforiskMAR).val($(ah.config.id.riskMAR).val());
        $(ah.config.id.inforiskPCA).val($(ah.config.id.riskPCA).val());


        $(ah.config.id.inforiskCLAScore).val($(ah.config.id.riskCLA).val());
        $(ah.config.id.inforiskCLSScore).val($(ah.config.id.riskCLS).val());
        $(ah.config.id.inforiskCMTScore).val($(ah.config.id.riskCMT).val());
        $(ah.config.id.inforiskEQFScore).val($(ah.config.id.riskEQF).val());
        $(ah.config.id.inforiskESEScore).val($(ah.config.id.riskESE).val());
        $(ah.config.id.inforiskEVSScore).val($(ah.config.id.riskEVS).val());
        $(ah.config.id.inforiskEVTScore).val($(ah.config.id.riskEVT).val());
        $(ah.config.id.inforiskFPFScore).val($(ah.config.id.riskFPF).val());
        $(ah.config.id.inforiskLISScore).val($(ah.config.id.riskLIS).val());
        $(ah.config.id.inforiskMARScore).val($(ah.config.id.riskMAR).val());
        $(ah.config.id.inforiskPCAScore).val($(ah.config.id.riskPCA).val());

        window.location.href = ah.config.url.aimsRiskFactor;
    };
    self.othInfoLoader = function () {
        var infoDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.assetDetails));
        self.hideCalendar();
        ah.togglehideOthInfo();
        $(ah.config.id.noteEntry).show();
        ah.toggleEditMode();
        $(ah.config.id.noteHistory).hide();
        var senderID = $(arguments[1].currentTarget).attr('id');
        var assetDescription = infoDetails.DESCRIPTION;
        if (assetDescription.length > 60) {
            assetDescription = assetDescription.substring(0, 60) + '...';
        }
        $("#itemDescription").text(assetDescription);
        $("#costHeader").hide();
        $("#addButtonIcon").show();
        switch (senderID) {

            case ah.config.tagId.btnCSummary: {
                ah.toggleShowInfo1();
                $("#winTitleInfo").text("COST SUMMARY");
                $("#costHeader").show();
                window.location.href = ah.config.url.modalAddItemsInfo;
                break;
            }
            case ah.config.tagId.btnLCycle: {
                ah.toggleShowInfo2();
                $("#winTitleInfo").text("LIFE CYCLE");
                window.location.href = ah.config.url.modalAddItemsInfo;
                break;
            }
            case ah.config.tagId.btnHOperation: {
                ah.toggleShowInfo3();
                $("#winTitleInfo").text("HOURS OF OPERATION");
                window.location.href = ah.config.url.modalAddItemsInfo;
                break;
            }
            case ah.config.tagId.btnANotes: {
                ah.toggleShowInfo4();
                $("#winTitleInfo").text("ASSET NOTES");
                $(ah.config.id.woReferenceNotes).prop(ah.config.attr.disabled, true);
                window.location.href = ah.config.url.modalAddItemsInfo;
                break;
            }



            case ah.config.tagId.btnAPersonnel: {
                ah.toggleShowInfo5();
                $("#winTitleInfo").text("ASSIGN SERVICE PERSONNEL");
                window.location.href = ah.config.url.modalAddItemsInfo;
                var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
                var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
                var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
                var postData;


                postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), STAFF_LOGON_SESSIONS: staffLogonSessions };
                $.post(self.getApi() + ah.config.api.searchStaffList, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
                break;
            }
            case ah.config.tagId.btnDepreciation: {
                ah.toggleShowInfo6();
                $("#winTitleInfo").text("ASSET DEPRECIATION");
                var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
                var setupDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
                var setupDetailsOth = ah.ConvertFormToJSON($(ah.config.id.itemInfo)[0]);
                var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
                var postData;
                var assetNo = $("#ASSET_NO").val();
                postData = { SEARCH: assetNo, STAFF_LOGON_SESSIONS: staffLogonSessions };
                $.post(self.getApi() + ah.config.api.getAssetPartsDP, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);


                window.location.href = ah.config.url.modalAddItemsInfo;
                break;
            }
        }



    };
    self.infoRiskDet = function () {

        var senderID = $(arguments[1].currentTarget).attr('id');


        switch (senderID) {

            case "INFORISKCLA": {
                $(ah.config.id.inforiskCLAScore).val($(ah.config.id.inforiskCLA).val());
                break;
            }
            case "INFORISKCLS": {

                $(ah.config.id.inforiskCLSScore).val($(ah.config.id.inforiskCLS).val());
                break;
            }
            case "INFORISKCMT": {
                $(ah.config.id.inforiskCMTScore).val($(ah.config.id.inforiskCMT).val());
                break;
            }
            case "INFORISKEQF": {
                $(ah.config.id.inforiskEQFScore).val($(ah.config.id.inforiskEQF).val());
                break;
            }
            case "INFORISKESE": {
                $(ah.config.id.inforiskESEScore).val($(ah.config.id.inforiskESE).val());
                break;
            }
            case "INFORISKEVS": {
                $(ah.config.id.inforiskEVSScore).val($(ah.config.id.inforiskEVS).val());
                break;
            }
            case "INFORISKEVT": {
                $(ah.config.id.inforiskEVTScore).val($(ah.config.id.inforiskEVT).val());
                break;
            }
            case "INFORISKLIS": {
                $(ah.config.id.inforiskLISScore).val($(ah.config.id.inforiskLIS).val());
                break;
            }
            case "INFORISKFPF": {
                $(ah.config.id.inforiskFPFScore).val($(ah.config.id.inforiskFPF).val());
                break;
            }
            case "INFORISKMAR": {
                $(ah.config.id.inforiskMARScore).val($(ah.config.id.inforiskMAR).val());
                break;
            }
            case "INFORISKPCA": {
                $(ah.config.id.inforiskPCAScore).val($(ah.config.id.inforiskPCA).val());
                break;
            }
        }



        $("#riskFactorScore").text("RISK Factor: ");
        $("#RISKTOTALSCORE").val("");
        self.riskFactorScoreCalculate();
    };
    self.getAssetNotes = function () {
        $("#notesHeader").text("Asset Record General Notes History");
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));

        var postData;

        var assetNo = $('#ASSET_NO').val();
        postData = { SEARCH: assetNo, STAFF_LOGON_SESSIONS: staffLogonSessions };

        $.post(self.getApi() + ah.config.api.searchAssetNotes, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.getWoNotes = function () {
        $("#notesHeader").text("Work Order Reference Note History");
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));

        var postData;

        var assetNo = $('#ASSET_NO').val();
        postData = { SEARCH: assetNo, STAFF_LOGON_SESSIONS: staffLogonSessions };

        $.post(self.getApi() + ah.config.api.searchWONotes, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.riskFactorScoreCalculate = function () {
        var riskGroupId = $("#INFORISKGROUP").val();
        var riskFactorScore = 0;
        var riskText = '';
        if (riskGroupId === 'ME') {

            var scoreEQF = document.getElementById("INFORISKEQFSCORE");
            scoreEQF = scoreEQF.options[scoreEQF.selectedIndex].text;
            var scoreCLA = document.getElementById("INFORISKCLASCORE");
            scoreCLA = scoreCLA.options[scoreCLA.selectedIndex].text;
            var scoreMAR = document.getElementById("INFORISKMARSCORE");
            scoreMAR = scoreMAR.options[scoreMAR.selectedIndex].text;
            var scoreESE = document.getElementById("INFORISKESESCORE");
            scoreESE = scoreESE.options[scoreESE.selectedIndex].text;
            var scoreEVT = document.getElementById("INFORISKEVTSCORE");
            scoreEVT = scoreEVT.options[scoreEVT.selectedIndex].text;
            if (scoreEQF === null || scoreEQF === "") {
                scoreEQF = 0
            };
            if (scoreCLA === null || scoreCLA === "") {
                scoreCLA = 0
            };
            if (scoreMAR === null || scoreMAR === "") {
                scoreMAR = 0
            };
            if (scoreESE === null || scoreESE === "") {
                scoreESE = 0
            };
            if (scoreEVT === null || scoreEVT === "") {
                scoreEVT = 0
            };
            riskFactorScore = Math.round(parseInt(scoreEQF) + parseInt(scoreCLA) + ((parseInt(scoreMAR) + parseInt(scoreESE) + parseInt(scoreEVT)) / 3));
            riskText = 'Risk/Inclusion Factor: ';
        } else if (riskGroupId === 'UMR') {
            var scoreFPF = document.getElementById("INFORISKFPFSCORE");
            scoreFPF = scoreFPF.options[scoreFPF.selectedIndex].text;
            var scoreCLS = document.getElementById("INFORISKCLSSCORE");
            scoreCLS = scoreCLS.options[scoreCLS.selectedIndex].text;
            var scoreEVS = document.getElementById("INFORISKEVSSCORE");
            scoreEVS = scoreEVS.options[scoreEVS.selectedIndex].text;
            var scoreCMT = document.getElementById("INFORISKCMTSCORE");
            scoreCMT = scoreCMT.options[scoreCMT.selectedIndex].text;
            var scorePCA = document.getElementById("INFORISKPCASCORE");
            scorePCA = scorePCA.options[scorePCA.selectedIndex].text;
            if (scoreFPF === null || scoreFPF === "") {
                scoreFPF = 0
            };
            if (scoreCLS === null || scoreCLS === "") {
                scoreCLS = 0
            };
            if (scoreEVS === null || scoreEVS === "") {
                scoreEVS = 0
            };
            if (scoreCMT === null || scoreCMT === "") {
                scoreCMT = 0
            };
            if (scorePCA === null || scorePCA === "") {
                scorePCA = 0
            };
            riskFactorScore = Math.round(parseInt(scoreFPF) + parseInt(scoreCLS) + parseInt(scoreEVS) + parseInt(scoreCMT) + parseInt(scorePCA));
            riskText = 'RisK Factor: ';

        } else if (riskGroupId === 'LS') {
            var scoreLIS = document.getElementById("INFORISKLISSCORE");
            scoreLIS = scoreLIS.options[scoreLIS.selectedIndex].text;

            if (scoreLIS === null || scoreEQF === "") {
                scoreLIS = 0
            };

            riskFactorScore = scoreLIS;
            riskText = 'Risk Factor: ';
        } else {

        }
        $("#RISKTOTALSCORE").val(riskFactorScore);
        $("#riskFactorScore").text(riskText + riskFactorScore);
    };
    self.displayLookup = function (actionLOV) {

        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.lovList));
        //alert(JSON.stringify(sr))
        sr = sr.filter(function (item) { return item.CATEGORY === actionLOV });

        $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');
        if (sr.length > 0) {
            var htmlstr = "";
            for (var o in sr) {
                if (sr[o].DESCRIPTION.length > 50)
                    sr[o].DESCRIPTION = sr[o].DESCRIPTION.substring(0, 49) + "...";

                htmlstr += '<li>';
                htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                htmlstr += ah.formatString(ah.config.html.searchExistRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].DESCRIPTION);



                htmlstr += '</span>';
                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).append(htmlstr);
        }
        $(ah.config.cls.lookupDetailItem).bind('click', self.assignLookup);

    };
    self.calculateWarratyExpiry = function (item, idx) {
        var period = item.FREQUENCY_PERIOD;
        var frequnit = item.FREQUENCY;
        var warranExpiry = item.WARRANTY_EXPIRY;
        var installDate = $("#CONDITION_DATE").val();
        var noDays = 0;
        var warrantyAction = $("#warrantyAction").val();


        if (period != null && period != "" && frequnit != null && frequnit != "" && warrantyAction == "Y") {
            //   alert(JSON.stringify(item));
            // alert(period);
            if (frequnit === "Y") {
                noDays = parseInt(period) * 365;
            } else if (frequnit === "M") {
                noDays = parseInt(period) * 30;
            } else if (frequnit === "D") {
                noDays = parseInt(period);
            }

            var dattmp = installDate.split('/').reverse().join('/');
            var nwdate = new Date(dattmp);

            nwdate.setDate(nwdate.getDate() + noDays);
            var warrantyExpiry = [nwdate.getDate(), nwdate.getMonth() + 1, nwdate.getFullYear()].join('/');
            //alert(warrantyExpiry);
            document.getElementById("warrantyList").rows[idx + 1].cells[0].children[7].value = warrantyExpiry;
            // item.WARRANTY_EXPIRYDATE = warrantyExpiry;
            ko.utils.arrayFilter(self.warrantyData(), function (itemData) {
                if (item.ID === itemData.ID) {
                    itemData.WARRANTY_EXPIRY = warrantyExpiry;
                }
            });


        }






    };

    self.calculateccWarratyExpiry = function (item, idx) {
        var period = item.CCFREQUENCY_PERIOD;
        var frequnit = item.CCFREQUENCY;
        var warranExpiry = item.CCWARRANTY_EXPIRY;
        var installDate = $("#COSTCENTERINSTALLDATE").val();
        var noDays = 0;
        var warrantyAction = $("#ccwarrantyAction").val();

        if (period != null && period != "" && frequnit != null && frequnit != "" && warrantyAction == "Y") {
            if (frequnit === "Y") {
                noDays = parseInt(period) * 365;
            } else if (frequnit === "M") {
                noDays = parseInt(period) * 30;
            } else if (frequnit === "D") {
                noDays = parseInt(period);
            }

            var dattmp = installDate.split('/').reverse().join('/');
            var nwdate = new Date(dattmp);

            nwdate.setDate(nwdate.getDate() + noDays);
            var warrantyExpiry = [nwdate.getDate(), nwdate.getMonth() + 1, nwdate.getFullYear()].join('/');

            document.getElementById("ccwarrantyList").rows[idx + 1].cells[0].children[7].value = warrantyExpiry;

            ko.utils.arrayFilter(self.ccwarrantyData(), function (itemData) {
                if (item.CCID === itemData.CCID) {
                    itemData.CCWARRANTY_EXPIRY = warrantyExpiry;
                }
            });

        }
    };
    self.displayLookupBuilding = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        postData = { LOV: "'AIMS_BUILDING'", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = ah.config.url.modalLookUp;

    };
    self.nextRecord = function () {
        var curRow = $("#currentRow").val();
        var warrantyCount = self.warrantyData().length;
        warrantyCount = warrantyCount - 1;
        if (curRow == warrantyCount) {
            return;
        }
        curRow++

        var el = document.getElementsByClassName("rowdata");
        for (var i = 0; i < el.length; i++) {
            el[i].classList.remove('showRow');
            el[i].classList.add('hideRow');
        }

        el[curRow].classList.add('showRow');
        $("#currentRow").val(curRow);
    };
    self.ccnextRecord = function () {
        var curRow = $("#cccurrentRow").val();
        var ccwarrantyCount = self.ccwarrantyData().length;
        ccwarrantyCount = ccwarrantyCount - 1;
        if (curRow == ccwarrantyCount) {
            return;
        }
        curRow++

        var el = document.getElementsByClassName("ccrowdata");
        for (var i = 0; i < el.length; i++) {
            el[i].classList.remove('ccshowRow');
            el[i].classList.add('cchideRow');
        }

        el[curRow].classList.add('ccshowRow');
        $("#cccurrentRow").val(curRow);
    };
    self.prevRecord = function () {
        var curRow = $("#currentRow").val();

        if (curRow == 0) {

            return;
        }
        curRow--

        var el = document.getElementsByClassName("rowdata");
        for (var i = 0; i < el.length; i++) {
            el[i].classList.remove('showRow');
            el[i].classList.add('hideRow');
        }

        el[curRow].classList.add('showRow');
        $("#currentRow").val(curRow);
    };
    self.ccprevRecord = function () {
        var curRow = $("#cccurrentRow").val();

        if (curRow == 0) {

            return;
        }
        curRow--

        var el = document.getElementsByClassName("ccrowdata");
        for (var i = 0; i < el.length; i++) {
            el[i].classList.remove('ccshowRow');
            el[i].classList.add('cchideRow');
        }

        el[curRow].classList.add('ccshowRow');
        $("#cccurrentRow").val(curRow);
    };
    self.displayLookupCostCenter = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        postData = { LOV: "'AIMS-COSTCENTER'", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = ah.config.url.modalLookUp;

    };
    self.calculateDepreciation = function () {
        $(ah.config.id.searchDepreciationCal + ' ' + ah.config.tag.tbody).html('');

        var htmlstr = '';

        var installationDate = $("#CONDITION_DATE").val();
        var srparts = partsDPUsedList;
        var x = 1;
        var jDate = new Date();
        var curYear = parseFloat(jDate.getFullYear());
        var calDate = installationDate.substring(6, 10);
        var newYear = "";

        newYear = parseInt(calDate);
        var pCost = $("#PURCHASE_COST").val();
        var dpPercent = $("#DEPRECIATION_PERCENT").val();
        dpPercent = dpPercent / 100;

        var newdpCost = parseFloat(pCost);

        var initialCycle = 10;
        do {
            x++
            var partsCost = 0;

            htmlstr += "<tr>";
            htmlstr += '<td class="a">' + newYear + '</td>';
            htmlstr += '<td class="a">' + newdpCost.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 }) + '</td>';


            if (srparts.length > 0) {
                var srpartsFilter = srparts;

                srpartsFilter = srpartsFilter.filter(function (item) { return item.REQ_YEAR == newYear });
                if (srpartsFilter.length > 0) {
                    partsCost = srpartsFilter[0].COSTBYPARTS;
                } else {
                    partsCost = 0;
                }

            }


            htmlstr += '<td class="a">' + partsCost.toLocaleString('en') + '</td>';
            //htmlstr += '<td class="a">' + subCotract.toLocaleString('en') + '</td>';

            newYear = newYear + 1;
            newdpCost = (newdpCost - (newdpCost * dpPercent)) - parseFloat(partsCost);
            newdpCost = parseFloat(newdpCost);




            htmlstr += '<td class="a">' + newdpCost.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 }) + '</td>';
            htmlstr += "</tr>";
            // newdpCost = parseFloat(Math.round(newdpCost * 100) / 100).toFixed(2);



            if ((newYear - 1) == curYear) {
                $("#DPCURVALUE").text(newdpCost.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 }));
                break;
            }
            if (x == initialCycle) {
                if (curYear >= newYear) {
                    initialCycle++;
                }
            }
        }
        while (x < initialCycle);

        $(ah.config.id.searchDepreciationCal + ' ' + ah.config.tag.tbody).append(htmlstr);


    }
    self.displayLookupManufacturer = function () {
        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.assetManufacturerList));
        //alert(JSON.stringify(sr));
        $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');
        if (sr.length > 0) {
            var htmlstr = "";
            for (var o in sr) {

                if (sr[o].DESCRIPTION.length > 50)
                    sr[o].DESCRIPTION = sr[o].DESCRIPTION.substring(0, 49) + "...";

                htmlstr += '<li>';
                htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                htmlstr += ah.formatString(ah.config.html.searchExistRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].DESCRIPTION);



                htmlstr += '</span>';
                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.lookupDetailItem).bind('click', self.assignLookup);


    };

    self.getClonedList = function () {
        if (ah.CurrentMode == ah.config.mode.add) {
            window.location.href = ah.config.url.openModalClone;
        } else {
            var assetNo = $("#ASSET_NO").val();
            if (assetNo == null || assetNo == "") {
                window.location.href = ah.config.url.openModalClone;
            } else {
                self.searchNowCloneId();
            }


        }
    }
    self.getLastSearch = function () {
        SsearchTxt = $("#txtGlobalSearch").val();
        Scondition = $("#filterCondition").val();
        Sstatus = $("#filterStatus").val();
        Scloneid = $("#cloneid").val();
        Scostcenter = $("#costcenterStr").val();
        Spono = $("#purchaseNo").val();

    };
    self.populateLastsearch = function () {
        $(ah.config.id.txtGlobalSearch).val(SsearchTxt);

        $("#filterCondition").val(Scondition);
        $("#filterStatus").val(Sstatus);
        $("#cloneid").val(Scloneid);
        $("#costcenterStr").val(Scostcenter);
        $("#purchaseNo").val(Spono);


    };
    self.displayLookupSupplier = function () {
        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.assetSupplierList));
        //alert(JSON.stringify(sr));
        $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');
        if (sr.length > 0) {
            var htmlstr = "";
            for (var o in sr) {

                if (sr[o].DESCRIPTION.length > 50)
                    sr[o].DESCRIPTION = sr[o].DESCRIPTION.substring(0, 49) + "...";

                htmlstr += '<li>';
                htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                htmlstr += ah.formatString(ah.config.html.searchExistRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].DESCRIPTION);



                htmlstr += '</span>';
                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.lookupDetailItem).bind('click', self.assignLookup);

    };

    self.modifySetup = function () {
        ah.toggleEditMode();
        xElements = document.getElementById("frmInfo").elements.length;

    };
    self.isValidDate = function (dateString) {
        // First check for the pattern
        var regex_date = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
        var regex_date2 = /^(\d{1,2})-(\d{1,2})-(\d{4})$/;
        if (!regex_date.test(dateString)) {
            if (!regex_date2.test(dateString)) {
                return 'Invalid';
            }
        }
        // Parse the date parts to integers
        var n = dateString.search("-");
        var parts = "";
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var newDateFormat = "";
        if (n <= 0) {
            parts = dateString.split("/");
            var day = parseInt(parts[0], 10);
            var month = parseInt(parts[1], 10);
            var year = parseInt(parts[2], 10);
            newDateFormat = (year + '/' + format_two_digits(month) + '/' + format_two_digits(day)).trim();
        } else {
            parts = dateString.split("-");
            var day = parseInt(parts[0], 10);
            var month = parseInt(parts[1], 10);
            var year = parseInt(parts[2], 10);
            newDateFormat = (year + '/' + format_two_digits(month) + '/' + format_two_digits(day)).trim();

        }

        // Check the ranges of month and year
        if (year < 1000 || year > 3000 || month == 0 || month > 12) {
            return 'Invalid';
        }

        var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        // Adjust for leap years
        if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
            monthLength[1] = 29;
        }

        // Check the range of the day
        if (day > 0 && day <= monthLength[month - 1]) {
            return newDateFormat;
        } else {
            return 'Invalid';
        }
    };
    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };
    self.setAssetNo = function (assetNo) {
        sessionStorage.setItem(ah.config.skey.assetNO, JSON.stringify(assetNo));
    }
    self.getAssetNo = function () {

    }
    self.newNote = function () {
        $("#ERECORD_GENERAL_NOTES").val(null);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        $("#NOTE_RECORDED_BY").val(staffLogonSessions.STAFF_ID);
        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        strDateStart = strDateStart + ' ' + hourmin;
        $("#NOTE_RECORDED_DATE").val(strDateStart);
    };
    self.saveAllChanges = function () {

        $(ah.config.id.saveInfo).trigger('click');

    };

    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {

            ah.toggleDisplayMode();
            //self.showDetails(true);
            // 14/07/2020
        }

    };
    self.searchLOVClone = function () {
        $(ah.config.id.UpdateButtonIcon).hide();
        $(ah.config.id.addConeButtonIcon).show();
        var assetDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var cloneCycle = $("#CLONE_NO").val();
        if (cloneCycle === null || cloneCycle === "" || cloneCycle === '0') {
            alert("Please provide Number of Clone to proceed.")
            return;
        } else {
            if (parseInt(cloneCycle) < 0) {
                alert('Unable to proceed. Make sure that the number is greater than zero.');
                return;
            } else if (parseInt(cloneCycle) > 100) {
                alert('Unable to proceed. Maximum allowed for clone is 100');
                return;

            }
        }

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        postData = { LOV: "'AIMS_BUILDING','AIMS_COSTCENTER'", ASSET_NO: assetDetails.ASSET_NO, SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.getCloneLOV, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.clearRiskFactor = function () {
        $(ah.config.id.inforiskCLAScore).val("");
        $(ah.config.id.inforiskCLSScore).val("");
        $(ah.config.id.inforiskCMTScore).val("");
        $(ah.config.id.inforiskEQFScore).val("");
        $(ah.config.id.inforiskESEScore).val("");
        $(ah.config.id.inforiskEVSScore).val("");
        $(ah.config.id.inforiskEVTScore).val("");
        $(ah.config.id.inforiskFPFScore).val("");
        $(ah.config.id.inforiskLISScore).val("");
        $(ah.config.id.inforiskMARScore).val("");
        $(ah.config.id.inforiskPCAScore).val("");

        $(ah.config.id.inforiskCLA).val("");
        $(ah.config.id.inforiskCLS).val("");
        $(ah.config.id.inforiskCMT).val("");
        $(ah.config.id.inforiskEQF).val("");
        $(ah.config.id.inforiskESE).val("");
        $(ah.config.id.inforiskEVS).val("");
        $(ah.config.id.inforiskEVT).val("");
        $(ah.config.id.inforiskFPF).val("");
        $(ah.config.id.inforiskLIS).val("");
        $(ah.config.id.inforiskMAR).val("");
        $(ah.config.id.inforiskPCA).val("");
    };
    self.applyRiskFactor = function () {
        var riskGroupId = $(ah.config.id.inforiskGroup).val()
        $(ah.config.id.riskGroup).val(riskGroupId);


        $(ah.config.id.riskCLS).val("");
        $(ah.config.id.riskCMT).val("");
        $(ah.config.id.riskEVS).val("");
        $(ah.config.id.riskFPF).val("");
        $(ah.config.id.riskPCA).val("");
        $(ah.config.id.riskEQF).val("");
        $(ah.config.id.riskCLA).val("");
        $(ah.config.id.riskMAR).val("");
        $(ah.config.id.riskESE).val("");
        $(ah.config.id.riskEVT).val("");
        $(ah.config.id.riskLIS).val("");

        if (riskGroupId === 'ME') {
            $(ah.config.id.riskEQF).val($(ah.config.id.inforiskEQF).val());
            $(ah.config.id.riskCLA).val($(ah.config.id.inforiskCLA).val());
            $(ah.config.id.riskMAR).val($(ah.config.id.inforiskMAR).val());
            $(ah.config.id.riskESE).val($(ah.config.id.inforiskESE).val());
            $(ah.config.id.riskEVT).val($(ah.config.id.inforiskEVT).val());
        } else if (riskGroupId === 'UMR') {
            $(ah.config.id.riskCLS).val($(ah.config.id.inforiskCLS).val());
            $(ah.config.id.riskCMT).val($(ah.config.id.inforiskCMT).val());
            $(ah.config.id.riskEVS).val($(ah.config.id.inforiskEVS).val());
            $(ah.config.id.riskFPF).val($(ah.config.id.inforiskFPF).val());
            $(ah.config.id.riskPCA).val($(ah.config.id.inforiskPCA).val());

        } else if (riskGroupId === 'LS') {
            $(ah.config.id.riskLIS).val($(ah.config.id.inforiskLIS).val());

        } else {

        }
        $("#RISKTOTALSCORE").val();
        $("#RISK_INCLUSIONFACTORSCORE").val($("#RISKTOTALSCORE").val());
        $("#RISK_INCLUSIONFACTOR").val($("#RISKTOTALSCORE").val());

        window.location.href = ah.config.url.modalClose;
        //self.saveAllChanges();
    };
    self.regenerateClone = function (jsonData) {
        $(ah.config.cls.liCloneList).show();
        var buildingLOV = jsonData.LOV_LOOKUPS;
        buildingLOV = buildingLOV.filter(function (item) { return item.CATEGORY === 'AIMS_BUILDING' });
        buildingList = buildingLOV;
        var costcenterLOV = jsonData.LOV_LOOKUPS;
        costcenterLOV = costcenterLOV.filter(function (item) { return item.CATEGORY === 'AIMS_COSTCENTER' });
        costCenterList = costcenterLOV;
        var cloneCycle = $("#CLONE_NO").val();
        var jDate = new Date();
        var createDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();

        var assetDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var rowCount = 1;

        var pm = jsonData.AM_ASSET_PMSCHEDULES;

        if (pm == null) {
            $("#PMSCHEDULE").show();
        } else {
            $("#PMSCHEDULE").hide();
        }
        self.cloneAssetData.splice(0, 5000);
        for (i = 0; i < cloneCycle; i++) {

            self.cloneAssetData.push({
                ROWCOUNT: rowCount,
                ASSET_NO: null,
                DESCRIPTION: assetDetails.DESCRIPTION,
                BUILDING: null,
                RESPONSIBLE_CENTER: null,
                COST_CENTER: null,
                LOCATION: null,
                LOCATION_DATE: null,
                SERIAL_NO: null,
                CONDITION_DATE: null,
                CREATED_BY: staffLogonSessions.STAFF_ID,
                CREATED_DATE: createDate,
                buildingLOV: buildingList,
                costcenterLOV: costCenterList,
                responsiblecenterLOV: costCenterList

            });
            rowCount++;
        }
        window.location.href = ah.config.url.openModalClone;

    };
    self.cancelChangesModal = function () {

        self.hideCalendar();

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            var infoDetails = sessionStorage.getItem(ah.config.skey.assetDetails);
            infoDetails = JSON.parse(infoDetails);
            $("#ERECORD_GENERAL_NOTES").val(infoDetails.ERECORD_GENERAL_NOTES);
            var reqWoNO = ah.getParameterValueByName(ah.config.fld.reqWONo);
            if (reqWoNO !== null && reqWoNO !== "") {
                self.readSetupID("ASSETULR");
            }

        }

        //var infoDetails = sessionStorage.getItem(ah.config.skey.assetDetails);
        window.location.href = ah.config.url.modalClose;
        self.privilegesValidation('NEW');
        self.privilegesValidation('MODIFY');
    };


    self.cancelChangesModalClone = function () {

        self.hideCalendar();

        if (ah.CurrentMode == ah.config.mode.add) {

        } else {
            var assetNo = $("#ASSET_NO").val();
            if (assetNo != null && assetNo != "") {
                ah.toggleDisplayMode();
                var infoDetails = sessionStorage.getItem(ah.config.skey.assetDetails);
                infoDetails = JSON.parse(infoDetails);
                $("#ERECORD_GENERAL_NOTES").val(infoDetails.ERECORD_GENERAL_NOTES);
                var reqWoNO = ah.getParameterValueByName(ah.config.fld.reqWONo);
                if (reqWoNO !== null && reqWoNO !== "") {
                    self.readSetupID("ASSETULR");
                }
            }


        }

        //var infoDetails = sessionStorage.getItem(ah.config.skey.assetDetails);
        window.location.href = ah.config.url.modalClose;
        self.privilegesValidation('NEW');
        self.privilegesValidation('MODIFY');
    };
    self.closeModal = function () {
        $(ah.config.id.inforiskGroup).val($(ah.config.id.riskGroup).val());
        $(ah.config.id.inforiskCLA).val($(ah.config.id.riskCLA).val());
        $(ah.config.id.inforiskCLS).val($(ah.config.id.riskCLS).val());
        $(ah.config.id.inforiskCMT).val($(ah.config.id.riskCMT).val());
        $(ah.config.id.inforiskEQF).val($(ah.config.id.riskEQF).val());
        $(ah.config.id.inforiskESE).val($(ah.config.id.riskESE).val());
        $(ah.config.id.inforiskEVS).val($(ah.config.id.riskEVS).val());
        $(ah.config.id.inforiskEVT).val($(ah.config.id.riskEVT).val());
        $(ah.config.id.inforiskFPF).val($(ah.config.id.riskFPF).val());
        $(ah.config.id.inforiskLIS).val($(ah.config.id.riskLIS).val());
        $(ah.config.id.inforiskMAR).val($(ah.config.id.riskMAR).val());
        $(ah.config.id.inforiskPCA).val($(ah.config.id.riskPCA).val());


        $(ah.config.id.inforiskCLAScore).val($(ah.config.id.riskCLA).val());
        $(ah.config.id.inforiskCLSScore).val($(ah.config.id.riskCLS).val());
        $(ah.config.id.inforiskCMTScore).val($(ah.config.id.riskCMT).val());
        $(ah.config.id.inforiskEQFScore).val($(ah.config.id.riskEQF).val());
        $(ah.config.id.inforiskESEScore).val($(ah.config.id.riskESE).val());
        $(ah.config.id.inforiskEVSScore).val($(ah.config.id.riskEVS).val());
        $(ah.config.id.inforiskEVTScore).val($(ah.config.id.riskEVT).val());
        $(ah.config.id.inforiskFPFScore).val($(ah.config.id.riskFPF).val());
        $(ah.config.id.inforiskLISScore).val($(ah.config.id.riskLIS).val());
        $(ah.config.id.inforiskMARScore).val($(ah.config.id.riskMAR).val());
        $(ah.config.id.inforiskPCAScore).val($(ah.config.id.riskPCA).val());
        window.location.href = ah.config.url.modalClose;

    };

    self.applyChanges = function () {
        var purchaseDate = $("#PURCHASE_DATE").val();
        var check = self.isDateFuture(moment(purchaseDate, 'DD/MM/YYYY'), 1);

        if (check) {
            alert('Unable to proceed, Please make sure that the Life Cycle Purchase date entered is not a future date.');
            return;
        };


        self.saveAllChanges();
    };
    self.applyClone = function () {
        window.location.href = ah.config.url.modalClose;
        self.hideCalendar();
        $("#liClone").hide();
        $("#liClone2").hide();

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var setupDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        var assetNO = $("#ASSET_NO").val();
        var jsonObj = ko.observableArray([]);

        jsonObj = ko.utils.stringifyJson(self.cloneAssetData);

        var cloneData = JSON.parse(jsonObj);
        var cloneInfoList = JSON.parse(jsonObj);
        var toUpdate = [];
        for (i in cloneInfoList) {
            var cloneFilter = cloneData;

            //alert(JSON.stringify(cloneFilter));
            //check double entry of serial no
            cloneFilter = cloneFilter.filter(function (item) { return item.SERIAL_NO == cloneInfoList[i].SERIAL_NO && item.ROWCOUNT != cloneInfoList[i].ROWCOUNT });

            if (cloneFilter.length > 0) {
                alert("Unable to proceed. Record No. " + cloneInfoList[i].ROWCOUNT + " have duplicate serial no. with Record. No. " + cloneFilter[0].ROWCOUNT);
                window.location.href = ah.config.url.openModalClone;
                return;
            }

            var locationDate = cloneInfoList[i].LOCATION_DATE;

            cloneInfoList[i].CREATED_DATE = self.isValidDate(cloneInfoList[i].CREATED_DATE);
            if (locationDate != null && locationDate != "") {
                locationDate = self.isValidDate(locationDate);
                if (locationDate === 'Invalid') {
                    alert("Record NO. " + cloneInfoList[i].ROWCOUNT + " Invalid Date Format Location Date. It should be (DD/MM/YYYY)")
                    window.location.href = ah.config.url.openModalClone;
                    return;

                } else {
                    cloneInfoList[i].LOCATION_DATE = locationDate;
                }



            }
            var installationDate = cloneInfoList[i].CONDITION_DATE;

            if (installationDate != null && installationDate != "") {
                installationDate = self.isValidDate(installationDate);
                if (installationDate === 'Invalid') {
                    alert("Record NO. " + cloneInfoList[i].ROWCOUNT + " Invalid Date Format Installation Date. It should be (DD/MM/YYYY)")
                    window.location.href = ah.config.url.openModalClone;
                    return;

                } else {
                    cloneInfoList[i].CONDITION_DATE = installationDate;
                }



            }

            toUpdate.push({ ASSET_NO: cloneInfoList[i].ASSET_NO, BUILDING: cloneInfoList[i].BUILDING, SERIAL_NO: cloneInfoList[i].SERIAL_NO, COST_CENTER: cloneInfoList[i].COST_CENTER, LOCATION_DATE: cloneInfoList[i].LOCATION_DATE, LOCATION: cloneInfoList[i].LOCATION, RESPONSIBLE_CENTER: cloneInfoList[i].RESPONSIBLE_CENTER, CONDITION_DATE: cloneInfoList[i].CONDITION_DATE, CREATED_DATE: cloneInfoList[i].CREATED_DATE, CLONE_ID: cloneInfoList[i].CLONE_ID, CREATED_BY: cloneInfoList[i].CREATED_BY })

        }


        setupDetails.CREATED_BY = staffLogonSessions.STAFF_ID;

        if (setupDetails.INSERVICE_DATE == "" || setupDetails.INSERVICE_DATE == "null" || setupDetails.INSERVICE_DATE == "00/00/0000" || setupDetails.INSERVICE_DATE == "00-00-0000") {
            setupDetails.INSERVICE_DATE = "01/01/1900";
        }

        if (setupDetails.STATUS_DATE == "" || setupDetails.STATUS_DATE == "null" || setupDetails.STATUS_DATE == "00/00/0000" || setupDetails.STATUS_DATE == "00-00-0000") {
            setupDetails.STATUS_DATE = "01/01/1900";
        }

        if (setupDetails.PURCHASE_DATE == "" || setupDetails.PURCHASE_DATE == "null" || setupDetails.PURCHASE_DATE == "00/00/0000" || setupDetails.PURCHASE_DATE == "00-00-0000") {
            setupDetails.PURCHASE_DATE = "01/01/1900";
        }
        if (setupDetails.REPLACEMENT_DATE == "" || setupDetails.REPLACEMENT_DATE == "null" || setupDetails.REPLACEMENT_DATE == "00/00/0000" || setupDetails.REPLACEMENT_DATE == "00-00-0000") {
            setupDetails.REPLACEMENT_DATE = "01/01/1900";
        }
        if (setupDetails.SALVAGE_DATE == "" || setupDetails.SALVAGE_DATE == "null" || setupDetails.SALVAGE_DATE == "00/00/0000" || setupDetails.SALVAGE_DATE == "00-00-0000") {
            setupDetails.SALVAGE_DATE = "01/01/1900";
        }
        if (setupDetails.CONDITION_DATE == "" || setupDetails.CONDITION_DATE == "null" || setupDetails.CONDITION_DATE == "00/00/0000" || setupDetails.CONDITION_DATE == "00-00-0000") {
            setupDetails.CONDITION_DATE = "01/01/1900";
        }
        if (setupDetails.WARRANTY_EXPIRYDATE == "" || setupDetails.WARRANTY_EXPIRYDATE == "null" || setupDetails.WARRANTY_EXPIRYDATE == "00/00/0000" || setupDetails.WARRANTY_EXPIRYDATE == "00-00-0000") {
            setupDetails.WARRANTY_EXPIRYDATE = "01/01/1900";
        }
        if (setupDetails.DEPRECIATED_LIFE === null || setupDetails.DEPRECIATED_LIFE === "") {
            setupDetails.DEPRECIATED_LIFE = 0;
        }


        if (setupDetails.USEFULL_LIFE === null || setupDetails.USEFULL_LIFE === "") {
            setupDetails.USEFULL_LIFE = 0;
        }
        if (setupDetails.HAYEARTODATEOTH === null || setupDetails.HAYEARTODATEOTH === "") {
            setupDetails.HAYEARTODATEOTH = 0;
        }

        if (setupDetails.LIFETODATEOTH === null || setupDetails.LIFETODATEOTH === "") {
            setupDetails.LIFETODATEOTH = 0;
        }
        if (setupDetails.PREVIOUSYROTH === null || setupDetails.PREVIOUSYROTH === "") {
            setupDetails.PREVIOUSYROTH = 0;
        }

        if (setupDetails.YEARTODATEOTH === null || setupDetails.YEARTODATEOTH === "") {
            setupDetails.YEARTODATEOTH = 0;
        }

        if (setupDetails.RISK_INCLUSIONFACTOR === null || setupDetails.RISK_INCLUSIONFACTOR === "") {
            setupDetails.RISK_INCLUSIONFACTOR = 0;
        }

        if (setupDetails.PURCHASE_COST === null || setupDetails.PURCHASE_COST === "") {
            setupDetails.PURCHASE_COST = 0;
        }


        var jsonObjwar = ko.observableArray([]);

        jsonObjwar = ko.utils.stringifyJson(self.warrantyData);

        var warrantyToPushList = [];
        var warrantyInfo = JSON.parse(jsonObjwar);

        var warrantyDate = null;
        if (warrantyInfo.length > 0) {
            for (var i in warrantyInfo) {
                if (warrantyInfo[i].WARRANTY_EXPIRY == null || warrantyInfo.FREQUENCY == null || warrantyInfo.FREQUENCY_PERIOD == null) {





                }
                if (warrantyInfo[i].WARRANTY_EXPIRY != null && warrantyInfo[i].WARRANTY_EXPIRY != "") {
                    var warrantyDate = self.isValidDate(warrantyInfo[i].WARRANTY_EXPIRY);

                    if (warrantyDate === 'Invalid') {
                        alert("Record NO. " + warrantyInfo[i].ROWCOUNT + " Invalid Date Format. It should be (DD/MM/YYYY)")
                        return;
                    }
                }

            }
            warrantyInfo[i].WARRANTY_EXPIRY = warrantyDate;
            warrantyToPushList.push(warrantyInfo[i]);
        }



        var jsonObjwar = ko.observableArray([]);

        jsonObjwar = ko.utils.stringifyJson(self.ccwarrantyData);

        var ccwarrantyToPushList = [];
        var ccwarrantyInfo = JSON.parse(jsonObjwar);

        var ccwarrantyDate = null;
        if (ccwarrantyInfo.length > 0) {
            for (var i in ccwarrantyInfo) {
                if (ccwarrantyInfo[i].WARRANTY_EXPIRY == null || ccwarrantyInfo.FREQUENCY == null || ccwarrantyInfo.FREQUENCY_PERIOD == null) {





                }
                if (ccwarrantyInfo[i].WARRANTY_EXPIRY != null && ccwarrantyInfo[i].WARRANTY_EXPIRY != "") {
                    var ccwarrantyDate = self.isValidDate(ccwarrantyInfo[i].WARRANTY_EXPIRY);

                    if (ccwarrantyDate === 'Invalid') {
                        alert("Record NO. " + ccwarrantyInfo[i].ROWCOUNT + " Invalid Date Format. It should be (DD/MM/YYYY)")
                        return;
                    }
                }

            }
            ccwarrantyInfo[i].WARRANTY_EXPIRY = ccwarrantyDate;
            ccwarrantyToPushList.push(ccwarrantyInfo[i]);
        }



        postData = { AM_ASSET_DETAILS: setupDetails, AM_ASSET_DETAILS_CLONE: toUpdate, ASSET_NO: assetNO, AM_ASSET_WARRANTY: warrantyToPushList, STAFF_LOGON_SESSIONS: staffLogonSessions };

        ah.toggleSaveMode();
        $(ah.config.cls.liApiStatus).hide();
        $.post(self.getApi() + ah.config.api.createClone, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);


    };

    self.assetCostSummary = function () {
        self.hideCalendar();
        var assetNoID = $("#ASSET_NO").val();
        $("#addButtonIcon").hide();
        var infoDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.assetDetails));

        ah.togglehideOthInfo();
        ah.toggleEditMode();

        var assetDescription = infoDetails.DESCRIPTION;
        if (assetDescription.length > 60) {
            assetDescription = assetDescription.substring(0, 60) + '...';
        }
        $("#itemDescription").text(assetDescription);
        ah.toggleShowInfo1();
        $("#winTitleInfo").text("COST SUMMARY");
        $("#costHeader").show();

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;



        postData = { ASSET_NO: assetNoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchCostSumm, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = ah.config.url.modalAddItemsInfo;
    };
    self.newWarranty = function () {

        // 
        var jDate = new Date();
        var createDate = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        if (self.warrantyData().length > 0) {

            ko.utils.arrayFilter(self.warrantyData(), function (item) {
                if (item.SHOWROW === true) {
                    var itemnew = item;

                    item.SHOWROW = false;

                    //alert(JSON.stringify(itemnew));


                    //var inxd = self.warrantyData.indexOf(item);
                    //alert(inxd);
                    // self.warrantyData.replace(self.warrantyData()[0],new itemnew);




                }
            });
        }

        var jsonObj = ko.observableArray([]);

        jsonObj = ko.utils.stringifyJson(self.warrantyData);


        var warrantyLists = JSON.parse(jsonObj);
        var rowCount = warrantyLists.length;
        rowCount++;
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        self.warrantyData.push({
            ROWCOUNT: rowCount,
            FREQUENCY: null,
            FREQUENCY_PERIOD: 0,
            WARRANTY_NOTES: null,
            WARRANTY_EXPIRY: null,
            WARRANTY_INCLUDED: null,
            ID: 0,
            ASSET_NO: null,
            WARRANTY_EXTENDED: null,
            CREATED_BY: staffDetails.STAFF_ID,
            CREATED_DATE: createDate,
            SHOWROW: true,
            warrantyLOV: self.warrantyLOV

        });
        var warrantyCount = self.warrantyData().length;
        warrantyCount = warrantyCount - 1;
        var el = document.getElementsByClassName("rowdata");
        for (var i = 0; i < el.length; i++) {
            el[i].classList.remove('showRow');
            el[i].classList.add('hideRow');
        }
        el[warrantyCount].classList.add('showRow');



        // alert(JSON.stringify(poLists));

    };

    self.newccWarranty = function () {

        // 
        var jDate = new Date();
        var createDate = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        if (self.ccwarrantyData().length > 0) {

            ko.utils.arrayFilter(self.ccwarrantyData(), function (item) {
                if (item.SHOWROW === true) {
                    var itemnew = item;

                    item.SHOWROW = false;
                }
            });
        }

        var jsonObj = ko.observableArray([]);

        jsonObj = ko.utils.stringifyJson(self.ccwarrantyData);


        var ccwarrantyLists = JSON.parse(jsonObj);
        var rowCount = ccwarrantyLists.length;
        rowCount++;
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        self.ccwarrantyData.push({
            CCROWCOUNT: rowCount,
            CCFREQUENCY: null,
            CCFREQUENCY_PERIOD: 0,
            CCWARRANTY_NOTES: null,
            CCWARRANTY_EXPIRY: null,
            CCWARRANTY_INCLUDED: null,
            CCID: 0,
            ASSET_NO: null,
            CCWARRANTY_EXTENDED: null,
            
            CCSHOWROW: true,
            ccwarrantyLOV: self.ccwarrantyLOV

        });
        var warrantyCount = self.ccwarrantyData().length;
        warrantyCount = warrantyCount - 1;
        var el = document.getElementsByClassName("ccrowdata");
        for (var i = 0; i < el.length; i++) {
            el[i].classList.remove('ccshowRow');
            el[i].classList.add('cchideRow');
        }
        el[warrantyCount].classList.add('ccshowRow');

    };
    self.cloneAsset = function () {
        var assetNo = $("#ASSET_NO").val();
        var assetDescription = $("#DESCRIPTION").val();
        $("#autogenerateWOIns").show();
        $("#winTitleClone").text("You are above to clone " + assetNo + '-' + assetDescription);
        $("#winTitleCloneG").text("You are above to clone " + assetNo + '-' + assetDescription);

        self.hideCalendar();
        ah.toggleEditMode();

        window.location.href = ah.config.url.openModalCloneGenerator;
    };

    self.cloneAssetNew = function () {
        $("#isSave").hide();
        var assetDescription = $("#DESCRIPTION").val();
        var deviceType = $("#CATEGORYDESC").val();
        var manufacturerDesc = $("#MANUFACTURERDESC").val();
        var supplierDesc = $("#SUPPLIERDESC").val();
        var purchaseDate = $("#PURCHASE_DATE").val();
        var statusAsset = $("#STATUS").val();



        if (statusAsset == null || statusAsset == "" || assetDescription == null || assetDescription == "" || deviceType == null || deviceType == "" || manufacturerDesc == null || manufacturerDesc == "" || supplierDesc == null || supplierDesc == "" || purchaseDate == null || purchaseDate == "") {

            self.alertMessage("Unable to proceed.Please Provide Description, Device Type, Manufacturer, Supplier, Status and  Purchase Date to continue.");
            return;
        }
        $("#winTitleClone").text("You are above to clone " + assetDescription);
        $("#winTitleCloneG").text("You are above to clone " + assetDescription);

        self.hideCalendar();
        //ah.toggleEditMode();

        window.location.href = ah.config.url.openModalCloneGenerator;
    };
    self.sorting = function () {
        if (srDataArray.length > 0) {
            sortby = $("#SORTPAGE").val();
            self.searchResult(0, sortby);
        }

    };
    self.showDetailsTrans = function () {
        var transDetails = sessionStorage.getItem(ah.config.skey.costcenterTransaction);
        var jsetupDetailInfo = JSON.parse(transDetails);
        if (jsetupDetailInfo.length > 0) {

            $("#COSTCENTERINVOICENO").val(jsetupDetailInfo[0].COSTCENTERINVOICENO);
            $("#COSTCENTERASSETCOND").val(jsetupDetailInfo[0].COSTCENTERASSETCOND);
            $("#COSTCENTERPURCHASECOST").val(jsetupDetailInfo[0].COSTCENTERPURCHASECOST);
           
            if (jsetupDetailInfo[0].COSTCENTERPURCHASEDATE == "" || jsetupDetailInfo[0].COSTCENTERPURCHASEDATE == "null" || jsetupDetailInfo[0].COSTCENTERPURCHASEDATE == "0001-01-01T00:00:00" || jsetupDetailInfo[0].COSTCENTERPURCHASEDATE == "1900-01-01T00:00:00") {
                $("#COSTCENTERPURCHASEDATE").val("");
            }
            else {
                var ccPurchaseDate = ah.formatJSONDateToString(jsetupDetailInfo[0].COSTCENTERPURCHASEDATE);
                $("#COSTCENTERPURCHASEDATE").val(ccPurchaseDate);
            }
            if (jsetupDetailInfo[0].COSTCENTERINSTALLDATE == "" || jsetupDetailInfo[0].COSTCENTERINSTALLDATE == "null" || jsetupDetailInfo[0].COSTCENTERINSTALLDATE == "0001-01-01T00:00:00" || jsetupDetailInfo[0].COSTCENTERINSTALLDATE == "1900-01-01T00:00:00") {
                $("#COSTCENTERINSTALLDATE").val("");
            }
            else {
                var ccPurchaseDate = ah.formatJSONDateToString(jsetupDetailInfo[0].COSTCENTERINSTALLDATE);
                $("#COSTCENTERINSTALLDATE").val(ccPurchaseDate);
            }
        }
        

        
    }
    self.showDetails = function (isNotUpdate) {
        $("#warrantyAction").val();
        $("#ccarrantyAction").val();
        ah.toggleDisplayMode();

        if (isNotUpdate) {

            var infoDetails = sessionStorage.getItem(ah.config.skey.assetDetails);
            var jsetupDetails = JSON.parse(infoDetails);
            var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.lovList));
            //populate device type


            ah.ResetControls();
            ah.toggleDisplayMode();
            ah.LoadJSON(jsetupDetails);

            if (jsetupDetails.COST_CENTER == self.DefaultCostCenter.CODE) {
                $(ah.config.cls.divCostCenter).hide();
            }

            self.showDetailsTrans();
          

            $("#showinservice").hide();
            $("#shownowinservice").hide();
            var assetStatus = jsetupDetails.STATUS;
            if (assetStatus == 'I') {
                $("#assetinservice").text("In Service");
                $("#showinservice").show();
            } else {
                if (assetStatus == 'A') {
                    $("#notassetinservice").text("Active");
                } else if (assetStatus == 'M') {
                    $("#notassetinservice").text("Missing");
                }
                else if (assetStatus == 'N') {
                    $("#notassetinservice").text("In Active");
                }
                else if (assetStatus == 'O') {
                    $("#notassetinservice").text("Out of Service");
                }
                else if (assetStatus == 'R') {
                    $("#notassetinservice").text("Retired");
                }
                else if (assetStatus == 'T') {
                    $("#notassetinservice").text("Transferred");
                }
                else if (assetStatus == 'U') {
                    $("#notassetinservice").text("Undefined");
                }
                $("#shownowinservice").show();
            }


            curCostCenter = jsetupDetails.COST_CENTER;

            if (sr !== null) {
                sr = sr.filter(function (item) { return item.CATEGORY === "AIMS_DTYPE" });

                if (sr.length > 0) {

                    $("#CATEGORYDESC").val(sr[0].DESCRIPTION);
                }
            }
            sr = JSON.parse(sessionStorage.getItem(ah.config.skey.lovList));
            if (sr !== null) {
                sr = sr.filter(function (item) { return item.CATEGORY === "AIMS_BUILDING" });

                if (sr.length > 0) {
                    $("#BUILDINGDESC").val(sr[0].DESCRIPTION);
                }
            }

            sr = costCenterList;
            if (sr !== null) {
                sr = sr.filter(function (item) { return item.LOV_LOOKUP_ID === jsetupDetails.COST_CENTER });

                if (sr.length > 0) {
                    self.CostCenter.Name(sr[0].DESCRIPTION);

                    $("#COST_CENTERDESC").val(sr[0].DESCRIPTION);
                }
            }
            sr = JSON.parse(sessionStorage.getItem(ah.config.skey.lovList));
            if (sr !== null) {
                sr = sr.filter(function (item) { return item.CATEGORY === "AIMS_COSTCENTER" && item.LOV_LOOKUP_ID === jsetupDetails.RESPONSIBLE_CENTER });

                if (sr.length > 0) {
                    $("#RESPONSIBLE_CENTERDESC").val(sr[0].DESCRIPTION);
                }
            }

            sr = JSON.parse(sessionStorage.getItem(ah.config.skey.assetManufacturerList));
            //alert(JSON.stringify(sr));
            if (sr !== null) {
                if (sr.length > 0) {
                    $("#MANUFACTURERDESC").val(sr[0].DESCRIPTION);
                }
            }
            sr = JSON.parse(sessionStorage.getItem(ah.config.skey.assetSupplierList));
            if (sr !== null) {
                if (sr.length > 0) {
                    $("#SUPPLIERDESC").val(sr[0].DESCRIPTION);
                }
            }
            sr = JSON.parse(sessionStorage.getItem(ah.config.skey.assetCurrentAgent));
            if (sr !== null) {
                if (sr.length > 0) {
                    $("#CURRENTAGENTDESC").val(sr[0].DESCRIPTION);
                }
            }
            $(ah.config.id.inforiskGroup).val($(ah.config.id.riskGroup).val());
            $(ah.config.id.inforiskCLA).val($(ah.config.id.riskCLA).val());
            $(ah.config.id.inforiskCLS).val($(ah.config.id.riskCLS).val());
            $(ah.config.id.inforiskCMT).val($(ah.config.id.riskCMT).val());
            $(ah.config.id.inforiskEQF).val($(ah.config.id.riskEQF).val());
            $(ah.config.id.inforiskESE).val($(ah.config.id.riskESE).val());
            $(ah.config.id.inforiskEVS).val($(ah.config.id.riskEVS).val());
            $(ah.config.id.inforiskEVT).val($(ah.config.id.riskEVT).val());
            $(ah.config.id.inforiskFPF).val($(ah.config.id.riskFPF).val());
            $(ah.config.id.inforiskLIS).val($(ah.config.id.riskLIS).val());
            $(ah.config.id.inforiskMAR).val($(ah.config.id.riskMAR).val());
            $(ah.config.id.inforiskPCA).val($(ah.config.id.riskPCA).val());


            $(ah.config.id.inforiskCLAScore).val($(ah.config.id.riskCLA).val());
            $(ah.config.id.inforiskCLSScore).val($(ah.config.id.riskCLS).val());
            $(ah.config.id.inforiskCMTScore).val($(ah.config.id.riskCMT).val());
            $(ah.config.id.inforiskEQFScore).val($(ah.config.id.riskEQF).val());
            $(ah.config.id.inforiskESEScore).val($(ah.config.id.riskESE).val());
            $(ah.config.id.inforiskEVSScore).val($(ah.config.id.riskEVS).val());
            $(ah.config.id.inforiskEVTScore).val($(ah.config.id.riskEVT).val());
            $(ah.config.id.inforiskFPFScore).val($(ah.config.id.riskFPF).val());
            $(ah.config.id.inforiskLISScore).val($(ah.config.id.riskLIS).val());
            $(ah.config.id.inforiskMARScore).val($(ah.config.id.riskMAR).val());
            $(ah.config.id.inforiskPCAScore).val($(ah.config.id.riskPCA).val());

            var riskFactorScore = jsetupDetails.RISK_INCLUSIONFACTOR;
            var riskText = "";
            $("#RISK_INCLUSIONFACTORSCORE").val(riskFactorScore);


            if (jsetupDetails.RISKGROUP === 'ME') {


                riskText = 'Risk/Inclusion Factor: ';
            } else if (jsetupDetails.RISKGROUP === 'UMR') {

                riskText = 'RisK Factor: ';

            } else if (jsetupDetails.RISKGROUP === 'LS') {

                riskText = 'Risk Factor: ';
            } else {

            }
            $("#RISKTOTALSCORE").val(riskFactorScore);
            $("#riskFactorScore").text(riskText + riskFactorScore);



            if (jsetupDetails.LOCATION_DATE == "" || jsetupDetails.LOCATION_DATE == "null" || jsetupDetails.LOCATION_DATE == "0001-01-01T00:00:00" || jsetupDetails.LOCATION_DATE == "1900-01-01T00:00:00") {

                $("#LOCATION_DATE").val("");
            }
            if (jsetupDetails.INSERVICE_DATE == "" || jsetupDetails.INSERVICE_DATE == "null" || jsetupDetails.INSERVICE_DATE == "0001-01-01T00:00:00" || jsetupDetails.INSERVICE_DATE == "1900-01-01T00:00:00") {

                $("#INSERVICE_DATE").val("");

            }
            if (jsetupDetails.STATUS_DATE == "" || jsetupDetails.STATUS_DATE == "null" || jsetupDetails.STATUS_DATE == "0001-01-01T00:00:00" || jsetupDetails.STATUS_DATE == "1900-01-01T00:00:00") {

                $("#STATUS_DATE").val("");
            }

            if (jsetupDetails.CONDITION_DATE == "" || jsetupDetails.CONDITION_DATE == "null" || jsetupDetails.CONDITION_DATE == "0001-01-01T00:00:00" || jsetupDetails.CONDITION_DATE == "1900-01-01T00:00:00") {

                $("#CONDITION_DATE").val("");
            }
            if (jsetupDetails.WARRANTY_EXPIRYDATE == "" || jsetupDetails.WARRANTY_EXPIRYDATE == "null" || jsetupDetails.WARRANTY_EXPIRYDATE == "0001-01-01T00:00:00" || jsetupDetails.WARRANTY_EXPIRYDATE == "1900-01-01T00:00:00") {

                $("#WARRANTY_EXPIRYDATE").val("");
            }

            if (jsetupDetails.PURCHASE_DATE == "" || jsetupDetails.PURCHASE_DATE == "null" || jsetupDetails.PURCHASE_DATE == "0001-01-01T00:00:00" || jsetupDetails.PURCHASE_DATE == "1900-01-01T00:00:00") {

                $("#PURCHASE_DATE").val("");
            }
            if (jsetupDetails.REPLACEMENT_DATE == "" || jsetupDetails.REPLACEMENT_DATE == "null" || jsetupDetails.REPLACEMENT_DATE == "0001-01-01T00:00:00" || jsetupDetails.REPLACEMENT_DATE == "1900-01-01T00:00:00") {

                $("#REPLACEMENT_DATE").val("");
            }
            if (jsetupDetails.SALVAGE_DATE == "" || jsetupDetails.SALVAGE_DATE == "null" || jsetupDetails.SALVAGE_DATE == "0001-01-01T00:00:00" || jsetupDetails.SALVAGE_DATE == "1900-01-01T00:00:00") {

                $("#SALVAGE_DATE").val("");
            }

            if (jsetupDetails.CLONE_ID != null && jsetupDetails.CLONE_ID != "") {
                $(ah.config.cls.liCloneList).show();
            } else {
                $(ah.config.cls.liCloneList).hide();
            }

            self.populatepurchasewarranty(jsetupDetails);
            self.populateCostCenterPurchasewarranty(jsetupDetails);



        } else {
            if (warrantyList.length > 0) {
                var rowCycle = 1;
                var rowShow = true;
                self.warrantyData.splice(0, 5000);

                var warrantyDate = null;
                $("#currentRow").val(0);
                for (var o in warrantyList) {
                    if (o > 0) {
                        rowShow = false;
                    }
                    var warrantyDate = null;


                    warrantyDate = ah.formatJSONDateToString(warrantyList[o].WARRANTY_EXPIRY);
                    self.warrantyData.push({
                        ROWCOUNT: rowCycle,
                        FREQUENCY: warrantyList[o].FREQUENCY,
                        FREQUENCY_PERIOD: warrantyList[o].FREQUENCY_PERIOD,
                        WARRANTY_NOTES: warrantyList[o].WARRANTY_NOTES,
                        WARRANTY_EXPIRY: warrantyDate,
                        WARRANTY_INCLUDED: warrantyList[o].WARRANTY_INCLUDED,
                        ID: warrantyList[o].ID,
                        ASSET_NO: warrantyList[o].ASSET_NO,
                        WARRANTY_EXTENDED: warrantyList[o].WARRANTY_EXTENDED,
                        SHOWROW: rowShow,
                        CREATED_BY: warrantyList[o].CREATED_BY,
                        CREATED_DATE: warrantyList[o].CREATED_DATE,
                        warrantyLOV: self.warrantyLOV

                    });
                    if (rowCycle == warrantyList.length) {

                        $("#warrantyAction").val("Y");
                    }

                    rowCycle++;

                }
                var el = document.getElementsByClassName("rowdata");
                for (var i = 0; i < el.length; i++) {
                    el[i].classList.remove('showRow');
                    el[i].classList.add('hideRow');
                }
                el[0].classList.add('showRow');
            }

            if (ccwarrantyList.length > 0) {
                var rowCycle = 1;
                var rowShow = true;
                self.ccwarrantyData.splice(0, 5000);

                var warrantyDate = null;
                $("#currentRow").val(0);
                for (var o in ccwarrantyList) {
                    if (o > 0) {
                        rowShow = false;
                    }
                    var warrantyDate = null;


                    warrantyDate = ah.formatJSONDateToString(ccwarrantyList[o].CCWARRANTY_EXPIRY);
                    self.ccwarrantyData.push({
                        CCROWCOUNT: rowCycle,
                        CCFREQUENCY: ccwarrantyList[o].CCFREQUENCY,
                        CCFREQUENCY_PERIOD: ccwarrantyList[o].CCFREQUENCY_PERIOD,
                        CCWARRANTY_NOTES: ccwarrantyList[o].CCWARRANTY_NOTES,
                        CCWARRANTY_EXPIRY: warrantyDate,
                        CCWARRANTY_INCLUDED: ccwarrantyList[o].CCWARRANTY_INCLUDED,
                        CCID: ccwarrantyList[o].CCID,
                        ASSET_NO: ccwarrantyList[o].ASSET_NO,
                        CCWARRANTY_EXTENDED: ccwarrantyList[o].CCWARRANTY_EXTENDED,

                        CCSHOWROW: rowShow,
                        ccwarrantyLOV: self.ccwarrantyLOV

                    });
                    if (rowCycle == ccwarrantyList.length) {

                        $("#ccwarrantyAction").val("Y");
                    }

                    rowCycle++;

                }
                var el = document.getElementsByClassName("ccrowdata");
                for (var i = 0; i < el.length; i++) {
                    el[i].classList.remove('ccshowRow');
                    el[i].classList.add('cchideRow');
                }
                el[0].classList.add('ccshowRow');
            }
        }


        var reqWoNO = ah.getParameterValueByName(ah.config.fld.reqWONo);
        if (reqWoNO !== null && reqWoNO !== "") {
            ah.toggleReturnWO();
        } else {
            $("#RETURNWO").hide();
        }
        self.privilegesValidation('NEW');
        self.privilegesValidation('MODIFY');
        self.privilegesValidation('NEWCLONE');
        self.privilegesValidation('CLONELIST');
        self.privilegesValidation('REQUESTOR');
        self.privilegesValidation('MODIFYCOSTCENCOMPANY');
        self.privilegesValidation('RETURNTOCOMPANY');
        
        self.privilegesValidation('COMPANYADMIN');

       

        self.populateLastsearch();
    };


    self.populatepurchasewarranty = function (jsetupDetails) {
        var rowCycle = 1;
        var rowShow = true;
        //populate warranty data

        if (warrantyList.length > 0) {

            self.warrantyData.splice(0, 5000);

            var warrantyDate = null;
            $("#currentRow").val(0);
            for (var o in warrantyList) {
                if (o > 0) {
                    rowShow = false;
                }
                var warrantyDate = null;


                warrantyDate = ah.formatJSONDateToString(warrantyList[o].WARRANTY_EXPIRY);
                self.warrantyData.push({
                    ROWCOUNT: rowCycle,
                    FREQUENCY: warrantyList[o].FREQUENCY,
                    FREQUENCY_PERIOD: warrantyList[o].FREQUENCY_PERIOD,
                    WARRANTY_NOTES: warrantyList[o].WARRANTY_NOTES,
                    WARRANTY_EXPIRY: warrantyDate,
                    WARRANTY_INCLUDED: warrantyList[o].WARRANTY_INCLUDED,
                    ID: warrantyList[o].ID,
                    ASSET_NO: warrantyList[o].ASSET_NO,
                    WARRANTY_EXTENDED: warrantyList[o].WARRANTY_EXTENDED,
                    CREATED_BY: warrantyList[o].CREATED_BY,
                    CREATED_DATE: warrantyList[o].CREATED_DATE,
                    SHOWROW: rowShow,
                    warrantyLOV: self.warrantyLOV

                });
                if (rowCycle == warrantyList.length) {

                    $("#warrantyAction").val("Y");
                }

                rowCycle++;

            }
        } else {
            $("#currentRow").val(0);
            var jDate = new Date();
            var createDate = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));


            self.warrantyData.splice(0, 5000);
            self.warrantyData.push({
                ROWCOUNT: rowCycle,
                FREQUENCY: null,
                FREQUENCY_PERIOD: 0,
                WARRANTY_NOTES: null,
                WARRANTY_EXPIRY: null,
                WARRANTY_INCLUDED: null,
                ID: 0,
                ASSET_NO: jsetupDetails.ASSET_NO,
                WARRANTY_EXTENDED: null,
                CREATED_BY: staffLogonSessions.STAFF_ID,
                CREATED_DATE: createDate,
                SHOWROW: rowShow,
                warrantyLOV: self.warrantyLOV

            });
            $("#warrantyAction").val("Y");
        }


        var el = document.getElementsByClassName("rowdata");
        for (var i = 0; i < el.length; i++) {
            el[i].classList.remove('showRow');
            el[i].classList.add('hideRow');
        }
        el[0].classList.add('showRow');
    };
    self.populateCostCenterPurchasewarranty = function (jsetupDetails) {
        var ccrowShow = true;
        var ccrowCycle = 1;
        if (ccwarrantyList.length > 0) {

            self.ccwarrantyData.splice(0, 5000);

            var ccwarrantyDate = null;
            $("#cccurrentRow").val(0);
            for (var o in ccwarrantyList) {
                if (o > 0) {
                    ccrowShow = false;
                }
                var ccwarrantyDate = null;


                ccwarrantyDate = ah.formatJSONDateToString(ccwarrantyList[o].CCWARRANTY_EXPIRY);
                self.ccwarrantyData.push({
                    CCROWCOUNT: ccrowCycle,
                    CCFREQUENCY: ccwarrantyList[o].CCFREQUENCY,
                    CCFREQUENCY_PERIOD: ccwarrantyList[o].CCFREQUENCY_PERIOD,
                    CCWARRANTY_NOTES: ccwarrantyList[o].CCWARRANTY_NOTES,
                    CCWARRANTY_EXPIRY: ccwarrantyDate,
                    CCWARRANTY_INCLUDED: ccwarrantyList[o].CCWARRANTY_INCLUDED,
                    CCID: ccwarrantyList[o].CCID,
                    ASSET_NO: ccwarrantyList[o].ASSET_NO,
                    CCWARRANTY_EXTENDED: ccwarrantyList[o].CCWARRANTY_EXTENDED,
                    CCSHOWROW: ccrowShow,
                    ccwarrantyLOV: self.ccwarrantyLOV

                });
                if (ccrowCycle == ccwarrantyList.length) {

                    $("#ccwarrantyAction").val("Y");
                }

                ccrowCycle++;

            }
        } else {
            $("#cccurrentRow").val(0);

            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));


            self.ccwarrantyData.splice(0, 5000);
            self.ccwarrantyData.push({
                CCROWCOUNT: ccrowCycle,
                CCFREQUENCY: null,
                CCFREQUENCY_PERIOD: 0,
                CCWARRANTY_NOTES: null,
                CCWARRANTY_EXPIRY: null,
                CCWARRANTY_INCLUDED: null,
                CCID: 0,
                ASSET_NO: jsetupDetails.ASSET_NO,
                CCWARRANTY_EXTENDED: null,
                CCSHOWROW: ccrowShow,
                ccwarrantyLOV: self.ccwarrantyLOV

            });
            $("#ccwarrantyAction").val("Y");
        }


        var el = document.getElementsByClassName("ccrowdata");
        for (var i = 0; i < el.length; i++) {
            el[i].classList.remove('ccshowRow');
            el[i].classList.add('cchideRow');
        }
        el[0].classList.add('ccshowRow');
    };
    self.searchResultConstSummary = function (jsonData) {
        //alert(JSON.stringify(jsonData.ASSET_WOSUMMARY_V));
        var wosummaryinfoDetails = jsonData.ASSET_WOSUMMARY_V;
        if (wosummaryinfoDetails.length > 0) {


            var jsetupDetails = wosummaryinfoDetails[0];

            ah.LoadJSON(jsetupDetails);

        }
        var wocostsummaryinfoDetails = jsonData.ASSET_WOPMOTCOST_V;
        if (wocostsummaryinfoDetails.length > 0) {


            var jsetupDetails = wocostsummaryinfoDetails[0];

            ah.LoadJSON(jsetupDetails);

        }

        var wocontractcostsummaryinfoDetails = jsonData.ASSET_WOCONTRACTCOST_V;
        if (wocontractcostsummaryinfoDetails.length > 0) {


            var jsetupDetails = wocontractcostsummaryinfoDetails[0];

            ah.LoadJSON(jsetupDetails);

        }

        var womaterialcostsummaryinfoDetails = jsonData.ASSET_WOMATERIALCOST_V;
        if (womaterialcostsummaryinfoDetails.length > 0) {


            var jsetupDetails = womaterialcostsummaryinfoDetails[0];

            ah.LoadJSON(jsetupDetails);

        }

        var tCostCurrent = parseInt($("#PM_COST_CURRENT").val()) + parseInt($("#OT_COST_CURRENT").val()) + parseInt($("#M_COST_CURRENT").val()) + parseInt($("#C_COST_CURRENT").val());
        $("#T_COST_CURRENT").val(tCostCurrent);
        var tCostPrevious = parseInt($("#PM_COST_PREVIOUS").val()) + parseInt($("#OT_COST_PREVIOUS").val()) + parseInt($("#M_COST_PREVIOUS").val()) + parseInt($("#C_COST_PREVIOUS").val());
        $("#T_COST_PREVIOUS").val(tCostPrevious);
        var tCostAll = parseInt($("#PM_COST_ALL").val()) + parseInt($("#OT_COST_ALL").val()) + parseInt($("#M_COST_ALL").val()) + parseInt($("#C_COST_ALL").val());
        $("#T_COST_ALL").val(tCostAll);
        self.privilegesValidation('NEW');
        self.privilegesValidation('MODIFY');
        self.privilegesValidation('NEWCLONE');
        self.privilegesValidation('CLONELIST');
    }
    self.pagingLoader = function () {


        var senderID = $(arguments[1].currentTarget).attr('id');



        var pageNum = parseInt($(ah.config.id.PaginGpageNum).val());
        var pageCount = parseInt($(ah.config.id.PaginGpageCount).val());

        //alert(JSON.stringify(srDataArray));
        //sr = JSON.parse(sessionStorage.getItem(ah.config.skey.assetDetails));
        var sr = srDataArray;


        var lastPage = Math.ceil(sr.length / 500);

        if (isNaN(pageCount)) {
            pageCount = 1;
        }
        if (isNaN(pageNum)) {
            pageNum = 0;
        }

        switch (senderID) {

            case ah.config.tagId.PaginGnextPage: {

                if (pageCount < lastPage) {
                    pageNum = pageNum + 500;
                    pageCount++;

                    var loadcount = $("#LASTLOADPAGE").val();
                    var currloadcount = loadcount;
                    loadcount = parseInt(loadcount) + 500;
                    // alert(loadcount);
                    if (sr.length < parseInt(loadcount)) {
                        var needtoLoad = parseInt(loadcount) - sr.length;
                        loadcount = currloadcount;// + needtoLoad;
                        // alert(loadcount);
                    }





                    $("#LASTLOADPAGE").val(loadcount);


                    self.searchResult(loadcount, sort);
                }
                break;
            }
            case ah.config.tagId.PaginGpriorPage: {
                if (pageNum === 1) {
                    return;
                }
                if (pageNum === 0) {
                } else {
                    pageNum = pageNum - 500;
                    if (pageNum < 0) {
                        pageNum = 0;
                    }
                    pageCount = pageCount - 1;
                }
                var loadcount = $("#LASTLOADPAGE").val();
                alert(loadcount);
                if (loadcount > 1000) {
                    loadcount = parseInt(loadcount) - 500;
                } else {
                    loadcount = parseInt(loadcount) - 1000;
                }

                if (loadcount < 0) {
                    loadcount = 0
                }
                // alert(loadcount);
                $("#LASTLOADPAGE").val(loadcount);
                self.searchResult(loadcount, sort);
                break;
            }

        }

        $("#PaginGloadNumber").text(pageCount.toString());
        $("#PaginGpageCount").val(pageCount.toString());
        $("#PaginGpageNum").val(pageNum);

    };
    self.searchResult = function (pageNum, sortby) {

        $(ah.config.id.searchResultAsset + ' ' + ah.config.tag.ul).html('');
        $(ah.config.id.searchDepreciationCal + ' ' + ah.config.tag.tbody).html('');
        var sr = srDataArray;
        resultHtml = null;

        if (sortby == 'SORTASSETDESCAC' && pageNum == 0) {
            sr.sort(function (a, b) {
                var textA = a.DESCRIPTION.toUpperCase();
                var textB = b.DESCRIPTION.toUpperCase();
                textA = textA.substring(0, 3);
                textB = textB.substring(0, 3);
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });
        } else if (sortby == 'SORTASSETDESCDC' && pageNum == 0) {
            sr.sort(function (a, b) {
                var textA = a.DESCRIPTION.toUpperCase();
                var textB = b.DESCRIPTION.toUpperCase();
                textA = textA.substring(0, 3);
                textB = textB.substring(0, 3);
                return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
            });
        } else if (sortby == 'SORTASSETNOAC' && pageNum == 0) {
            sr.sort(function (a, b) {

                if (a["ASSET_NO"] < b["ASSET_NO"])
                    return -1;
                if (a["ASSET_NO"] > b["ASSET_NO"])
                    return 1;
                return 0;
            });
        } else if (sortby == 'SORTASSETNODC' && pageNum == 0) {
            sr.sort(function (a, b) {

                if (a["ASSET_NO"] > b["ASSET_NO"])
                    return -1;
                if (a["ASSET_NO"] < b["ASSET_NO"])
                    return 1;
                return 0;
            });
        } else if (sortby == 'SORTCOSTCENTERAC' && pageNum == 0) {
            sr.sort(function (a, b) {
                var textA = a.COSTCENTER_DESC.toUpperCase();
                var textB = b.COSTCENTER_DESC.toUpperCase();
                textA = textA.substring(0, 3);
                textB = textB.substring(0, 3);
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });
        } else if (sortby == 'SORTCOSTCENTERDC' && pageNum == 0) {
            sr.sort(function (a, b) {
                var textA = a.COSTCENTER_DESC.toUpperCase();
                var textB = b.COSTCENTER_DESC.toUpperCase();
                textA = textA.substring(0, 3);
                textB = textB.substring(0, 3);
                return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
            });
        } else if (sortby == 'SORTSUPPLIERAC' && pageNum == 0) {

            sr.sort(function (a, b) {
                var textA = a.SUPPLIERNAME.toUpperCase();
                var textB = b.SUPPLIERNAME.toUpperCase();
                textA = textA.substring(0, 3);
                textB = textB.substring(0, 3);
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });
        } else if (sortby == 'SORTSUPPLIERDC' && pageNum == 0) {

            sr.sort(function (a, b) {
                var textA = a.SUPPLIERNAME.toUpperCase();
                var textB = b.SUPPLIERNAME.toUpperCase();
                textA = textA.substring(0, 3);
                textB = textB.substring(0, 3);
                return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
            });
        }


        var srLen = sr.length;
        self.assetCategory.count("No. of Records: " + srLen.toString());

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), srLen));
        $(ah.config.cls.liPrint).hide();
        if (sr.length > 0) {
            $(ah.config.cls.liPrint).show();
            var pageNumto = pageNum + 500;
            var pagenum = Math.ceil(sr.length / 500);

            if (pageNum == 0) {
                $("#PaginGloadNumber").text(1);
                $("#PaginGpageCount").val(1);
                $("#PaginGpageNum").val(1);
                $("#LASTLOADPAGE").val(0);
            }

            var index = 200000;
            sr = sr.slice(pageNum, index);

            if (pagenum >= 2) {

                $(ah.config.cls.pagingDiv).show();
            } else {
                $(ah.config.cls.pagingDiv).hide();
            }

            $("#PaginGtotNumber").text(pagenum.toString());

            var htmlstr = '';
            var countsr = 0
            for (var o in sr) {
                countsr++;
                if (sr[o].DESCRIPTION !== null && sr[o].DESCRIPTION !== "") {
                    if (sr[o].DESCRIPTION.length > 100)
                        sr[o].DESCRIPTION = sr[o].DESCRIPTION.substring(0, 99) + "...";
                } else {
                    sr[o].DESCRIPTION = "";
                }
                if (sr[o].LOCATION_DESC === null) {
                    sr[o].LOCATION_DESC = "";
                }
                if (sr[o].CONDITION === null) sr[o].CONDITION = "";

                if (sr[o].STATUS === 'I') {
                    sr[o].STATUS = 'In Service';
                } else if (sr[o].STATUS === 'A') {
                    sr[o].STATUS = 'Active';
                } else if (sr[o].STATUS === 'M') {
                    sr[o].STATUS = 'Missing';
                } else if (sr[o].STATUS === 'N') {
                    sr[o].STATUS = 'In Active';
                } else if (sr[o].STATUS === 'O') {
                    sr[o].STATUS = 'Out of Service';
                } else if (sr[o].STATUS === 'R') {
                    sr[o].STATUS = 'Retired';
                } else if (sr[o].STATUS === 'T') {
                    sr[o].STATUS = 'Transferred';
                } else if (sr[o].STATUS === 'U' || sr[o].STATUS === null || sr[o].STATUS === "") {
                    sr[o].STATUS = 'Undefined';
                }
                if (sr[o].BUILDING === null) {
                    sr[o].BUILDING = ''
                }
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].ASSET_NO, sr[o].ASSET_NO, sr[o].DESCRIPTION, "");
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.supplier, sr[o].SUPPLIERNAME);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.costCenter, sr[o].COSTCENTER_DESC);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.modelName, sr[o].MODEL_NO);

                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.deviceType, sr[o].CATEGORY_DESC);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.conditionName, sr[o].CONDITION);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.serialNo, sr[o].SERIAL_NO);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.statusName, sr[o].STATUS);

                if (sr[o].CLONE_ID != null && sr[o].CLONE_ID != "") {
                    htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.cloneId, sr[o].CLONE_ID);
                }

                htmlstr += '</li>';



            }
            resultHtml = htmlstr;
            $(ah.config.id.searchResultAsset + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.detailItem).bind('click', self.readSetupID);
        ah.displaySearchResultAssets();
        self.privilegesValidation('NEW');
    };
    self.searchResultSummaryList = function (jsonData) {
        var sr = jsonData.AM_ASSET_DETAILS;
        $(ah.config.id.searchResultList + ' ' + ah.config.tag.tbody).html('');
        $(ah.config.id.searchResultSummary).text("Asset" + "  number of records: " + sr.length.toString());

        if (sr.length > 0) {




            var htmlstr = '';

            for (var o in sr) {
                htmlstr += "<tr>";
                htmlstr += '<td class="d detailItem" data-bind="click:readSetupID" data-infoID=' + sr[o].DESCRIPTION + '> <a  href="#"> ' + sr[o].DESCRIPTION + ' </a> </td>';
                htmlstr += '<td class="h detailItem" data-bind="click:readSetupID" data-infoID=' + sr[o].DESCRIPTION + '> <a  href="#">' + sr[o].ONHANDCNT + ' </a> </td>';


                htmlstr += "</tr>";
            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.tbody).append(htmlstr);

            $(ah.config.cls.detailItem).bind('click', self.searchAssetDesc);

        }
        ah.displaySearchResult();
    };
    self.searchResultNotes = function (jsonData) {
        $(ah.config.id.noteEntry).hide();
        $(ah.config.id.noteHistory).show();
        var sr = jsonData.AM_ASSET_NOTES;
        $(ah.config.id.noteHistory + ' ' + ah.config.tag.ul).html('');


        if (sr.length > 0) {
            var htmlstr = "";
            for (var o in sr) {
                if (sr[o].FIRST_NAME === null) {
                    sr[o].FIRST_NAME = "";
                }
                if (sr[o].LAST_NAME === null) {
                    sr[o].LAST_NAME = "";
                }
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].ASSET_NO, sr[o].CREATED_DATE, sr[o].FIRST_NAME, sr[o].LAST_NAME);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.notes, sr[o].NOTES);

                htmlstr += '</li>';

            }

            $(ah.config.id.noteHistory + ' ' + ah.config.tag.ul).append(htmlstr);
        }
    }
    self.searchResultWONotes = function (jsonData) {
        $(ah.config.id.noteEntry).hide();
        $(ah.config.id.noteHistory).show();
        var sr = jsonData.AM_WORK_ORDER_REFNOTES;
        $(ah.config.id.noteHistory + ' ' + ah.config.tag.ul).html('');


        if (sr.length > 0) {
            var htmlstr = "";
            for (var o in sr) {
                if (sr[o].FIRST_NAME === null) {
                    sr[o].FIRST_NAME = "";
                }
                if (sr[o].LAST_NAME === null) {
                    sr[o].LAST_NAME = "";
                }
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].FIRST_NAME, sr[o].CREATED_DATE, sr[o].FIRST_NAME + ' ' + sr[o].LAST_NAME, 'Req.No: ' + sr[o].REQ_NO);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.notes, sr[o].WO_NOTES);

                htmlstr += '</li>';

            }

            $(ah.config.id.noteHistory + ' ' + ah.config.tag.ul).append(htmlstr);
        }
    }
    self.lookUppageLoader = function () {
        //ah.togglehideOthInfo();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData = "";
        var senderID = $(arguments[1].currentTarget).attr('id');
        var searchStr = $(ah.config.id.lookUptxtGlobalSearchOth).val().toString();
        switch (senderID) {

            case ah.config.tagId.searchAllLOVList: {
                $(ah.config.id.lookUptxtGlobalSearchOth).val("");
                searchStr = "";
                break;
            }
        }



        $("#searchingLOV").show();


        var LovAction = $(ah.config.id.lovAction).val();
        if (LovAction == "CATEGORY") {
            postData = { LOV: "'AIMS_DTYPE'", SEARCH: searchStr, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchLOVQuery, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        } else if (LovAction == "MANUFACTURER") {
            postData = { SEARCH: searchStr, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchManufacturer, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        } else if (LovAction == "SUPPLIER") {
            postData = { SEARCH: searchStr, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchSupplier, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        } else if (LovAction == "CurrentAgent") {
            postData = { SEARCH: searchStr, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchSupplier, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        } else if (LovAction == "BUILDING") {
            postData = { LOV: "'AIMS_BUILDING'", SEARCH: searchStr, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchLOVQuery, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        } else if (LovAction == "COSTCENTER") {
            postData = { LOV: "'AIMS_COSTCENTER'", SEARCH: searchStr, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchLOVQuery, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        } else if (LovAction == "RESPONSIBLECENTER") {
            postData = { LOV: "'AIMS_COSTCENTER'", SEARCH: searchStr, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchLOVQuery, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        } else if (LovAction == "MANUFACTURERMODEL") {
            var manufacturerId = $("#MANUFACTURER").val();
            postData = { MANUFACTURER_ID: manufacturerId, SEARCH: searchStr, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchModel, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }


    };
    self.searchItemsLookUpResult = function (LovCategory, pageLoad) {
        var LOVAction = $(ah.config.id.lovAction).val();
        $("#searchingLOV").hide();
        if (LOVAction == "MANUFACTURER") {
            sr = JSON.parse(sessionStorage.getItem(ah.config.skey.assetManufacturerList));
        } else if (LOVAction == "CATEGORY" || $(ah.config.id.lovAction).val() == "BUILDING" || $(ah.config.id.lovAction).val() == "COSTCENTER" || $(ah.config.id.lovAction).val() == "RESPONSIBLECENTER") {

            sr = JSON.parse(sessionStorage.getItem(ah.config.skey.lovList));
            var categoryLookup = $("#lookUpFilter").val();

        } else if (LOVAction == "SUPPLIER") {
            sr = JSON.parse(sessionStorage.getItem(ah.config.skey.assetSupplierList));
        } else if (LOVAction == "CurrentAgent") {
            sr = JSON.parse(sessionStorage.getItem(ah.config.skey.assetSupplierList));
        } else if (LOVAction == "MANUFACTURERMODEL") {
            sr = JSON.parse(sessionStorage.getItem(ah.config.skey.assetManufacturerModelList));
        }


        $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');
        var htmlstr = '';
        $("#lookUpnoresult").hide();
        if (sr.length > 0) {
            $("#noresult").hide();
            for (var o in sr) {
                htmlstr += '<li>';
                htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                htmlstr += ah.formatString(ah.config.html.searchExistRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].DESCRIPTION);

                htmlstr += '</span>';
                htmlstr += '</li>';
            }

            $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).append(htmlstr);
        }
        else {

            $("#lookUpnoresult").show();
        }
        $(ah.config.cls.lookupDetailItem).bind('click', self.assignLookup);
        //ah.toggleDisplayModereq();
        //ah.toggleAddItems();
    };
    self.pmSchedule = function () {
        var assetpDetails = sessionStorage.getItem(ah.config.skey.assetDetails);
        var jassetpDetails = JSON.parse(assetpDetails);
        var reqWoNO = ah.getParameterValueByName(ah.config.fld.reqWONo);
        var installDate = jassetpDetails.CONDITION_DATE;

        window.location.href = encodeURI(ah.config.url.aimsPMSchedule + "?ASSETNO=" + jassetpDetails.ASSET_NO + "&ASSETDESC=" + jassetpDetails.DESCRIPTION + "&REQNO=" + reqWoNO + "&installDate=" + installDate);
    };

    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };

    self.contractService = function () {
        var assetpDetails = sessionStorage.getItem(ah.config.skey.assetDetails);
        var jassetpDetails = JSON.parse(assetpDetails);
        var reqWoNO = ah.getParameterValueByName(ah.config.fld.reqWONo);
        window.location.href = encodeURI(ah.config.url.aimsContractServices + "?ASSETNO=" + jassetpDetails.ASSET_NO + "&ASSETDESC=" + jassetpDetails.DESCRIPTION + "&REQNO=" + reqWoNO);
    };
    self.returnWO = function () {
        var reqWoNO = ah.getParameterValueByName(ah.config.fld.reqWONo);

        var dashBoard = ah.getParameterValueByName(ah.config.fld.fromDashBoard);
        var dsfsdf = "";
        if (dashBoard == "dsdxsesesdfsadfsadfqwerqwerqwersadfsadfasdfasdfsadfsadfsadfasdf") {
            dsfsdf = "dsdxsesesdfsadfsadfqwerqwerqwersadfsadfasdfasdfsadfsadfsadfasdf";
            window.location.href = ah.config.url.aimsWorkOrderForClose + "?REQNO=" + reqWoNO + "&asdfxhsadfsdfx=" + dsfsdf;
        } else {
            window.location.href = ah.config.url.aimsWorkOrder + "?REQNO=" + reqWoNO;
        }


    };
    self.workOrders = function () {
        var assetpDetails = sessionStorage.getItem(ah.config.skey.assetDetails);
        var jassetpDetails = JSON.parse(assetpDetails);
        window.location.href = encodeURI(ah.config.url.aimsWorkOrder + "?ASSETNO=" + jassetpDetails.ASSET_NO + "&ASSETDESC=" + jassetpDetails.DESCRIPTION);
    };

    self.printSummary = function (idx) {
        self.isPrint = idx
        //window.location.href = ah.config.url.openModalPrintPage;
        ah.CurrentMode = ah.config.mode.searchResult;
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        var staffInfo = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        postData = { CLIENTID: staffInfo.CLIENT_CODE, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readClientInfo, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };

    self.printSticker = function (jsonData) {

        var AssetDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.assetDetails));
        var clientInfo = jsonData.AM_CLIENT_INFO;
        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.lovList));

        if (sr !== null) {
            sr = sr.find(function (item) { return item.CATEGORY === "AIMS_COSTCENTER" && item.LOV_LOOKUP_ID === AssetDetails.COST_CENTER });
            if (sr) {
                self.printStickerDetails.COST_CENTER(sr.DESCRIPTION);
            }
        }

        // Generate the QR code using qrcode.js 
        var qrCodeDiv = document.getElementById("qrcode");
        qrCodeDiv.innerHTML = ''; qrCodeDiv.title = '';
        //var qrcontent = "CONTROL NO: " + AssetDetails.ASSET_NO + "\n" +
        //                "EQUIPMENT: " + AssetDetails.DESCRIPTION + "\n" +
        //                "MODEL: " + AssetDetails.MODEL_NO + "\n" +
        //                "SERIAL NO: " + AssetDetails.SERIAL_NO + "\n" +
        //                "DEPARTMENT: " + AssetDetails.SERIAL_NO + "\n" +
        //                "LOCATION: " + AssetDetails.LOCATION;
        var qrcontent = AssetDetails.ASSET_NO;
        //var qrcontent = "Control No: " + AssetDetails.ASSET_NO + "\n" +
        //    "Serial No: " + AssetDetails.SERIAL_NO + "\n" +
        //    "Location: " + AssetDetails.LOCATION;
        var qrCode = new QRCode(qrCodeDiv, {
            text: qrcontent,
            width: 70,
            height: 70,
            colorDark: "#000000",
            colorLight: "#ffffff",
            correctLevel: QRCode.CorrectLevel.H
        });

        //alert(JSON.stringify(AssetDetails));
        self.printStickerDetails.CLIENTNAME(clientInfo.DESCRIPTION);
        self.printStickerDetails.CLIENT_LOGO(clientInfo.CLIENT_LOGO);
        self.printStickerDetails.ASSET_NO(AssetDetails.ASSET_NO);
        self.printStickerDetails.ASSET_NOBarcode("*" + AssetDetails.ASSET_NO + "*");
        self.printStickerDetails.DESCRIPTION(AssetDetails.DESCRIPTION);
        self.printStickerDetails.MODEL(AssetDetails.MODEL_NO);
        self.printStickerDetails.SERIAL_NO(AssetDetails.SERIAL_NO);
        self.printStickerDetails.COST_CENTER(AssetDetails.COSTCENTER_DESC);
        self.printStickerDetails.LOCATION(AssetDetails.LOCATION);

        document.getElementById("printSticker").classList.add('printSticker');
        document.getElementById("printSummary").style.display = "none";
        document.getElementById("returnSticker").style.display = "none";

        $(ah.config.id.assetPrintSticker).html();

        setTimeout(function () {
            window.location.href = ah.config.url.modalClose;
            window.onafterprint = function () { document.getElementById("printSummary").style.display = ""; }
            window.onafterprint = function () { document.getElementById("returnSticker").style.display = ""; }
            window.print();
            //qrCode.clear();
        }, 1000);
    };

    self.returnSticker = function (jsonData) {
        var AssetDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.assetDetails));
        var clientInfo = jsonData.AM_CLIENT_INFO;
        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.lovList));
        var jDate = new Date();
        var NewDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();

        if (sr !== null) {
            sr = sr.find(function (item) { return item.CATEGORY === "AIMS_COSTCENTER" && item.LOV_LOOKUP_ID === AssetDetails.COST_CENTER });
            if (sr) {
                self.printStickerDetails.COST_CENTER(sr.DESCRIPTION);
            }
        }

        var costCenterDetails = jsonData.CLIENT_COSTCENTER;
        if (costCenterDetails !== null) {
            costCenterDetails = costCenterDetails.find(function (item) { return item.LOV_LOOKUP_ID === AssetDetails.COST_CENTER });
            if (costCenterDetails) {
                self.printStickerDetails.COST_CENTER(costCenterDetails.DESCRIPTION);
            }
        }

        // Generate the QR code using qrcode.js 
        var qrCodeDiv = document.getElementById("qrcodeReturn");
        qrCodeDiv.innerHTML = ''; qrCodeDiv.title = '';
        var qrcontent = AssetDetails.ASSET_NO;
        //var qrcontent = "Control No: " + AssetDetails.ASSET_NO + "\n" +
        //    "Serial No: " + AssetDetails.SERIAL_NO + "\n" +
        //    "Location: " + AssetDetails.LOCATION;
        var qrCode = new QRCode(qrCodeDiv, {
            text: qrcontent,
            width: 60,
            height: 60,
            colorDark: "#000000",
            colorLight: "#ffffff",
            correctLevel: QRCode.CorrectLevel.H
        });

        self.printStickerDetails.CLIENTNAME(clientInfo.DESCRIPTION);
        self.printStickerDetails.CLIENT_LOGO(clientInfo.CLIENT_LOGO);
        self.printStickerDetails.ASSET_NO(AssetDetails.ASSET_NO);
        self.printStickerDetails.ASSET_NOBarcode("*" + AssetDetails.ASSET_NO + "*");
        self.printStickerDetails.DESCRIPTION(AssetDetails.DESCRIPTION);
        self.printStickerDetails.SERIAL_NO(AssetDetails.SERIAL_NO);
        self.printStickerDetails.ReqDate(NewDate);
        document.getElementById("returnSticker").classList.add('returnSticker');
        document.getElementById("printSticker").style.display = "none";
        document.getElementById("printSummary").style.display = "none";

        $(ah.config.id.assetReturnSticker).html();

        setTimeout(function () {
            window.location.href = ah.config.url.modalClose;
            window.onafterprint = function () { document.getElementById("printSummary").style.display = ""; }
            window.onafterprint = function () { document.getElementById("printSticker").style.display = ""; }
            window.print();
        }, 1000);
    };


    self.print = function (jsonData) {
        var clientInfo = jsonData.AM_CLIENT_INFO;

        self.clientDetails.CLIENTNAME(clientInfo.DESCRIPTION);
        self.clientDetails.CLIENT_LOGO(clientInfo.CLIENT_LOGO);
        self.clientDetails.COMPANY_LOGO(clientInfo.COMPANY_LOGO);
        self.clientDetails.HEADING(clientInfo.HEADING);
        self.clientDetails.HEADING2(clientInfo.HEADING2);
        self.clientDetails.HEADING3(clientInfo.HEADING3);
        self.clientDetails.HEADING_OTH(clientInfo.HEADING_OTH);
        self.clientDetails.HEADING_OTH2(clientInfo.HEADING_OTH2);
        self.clientDetails.HEADING_OTH3(clientInfo.HEADING_OTH3);
        self.clientDetails.CLIENTADDRESS(clientInfo.ADDRESS + ', ' + clientInfo.CITY + ', ' + clientInfo.ZIPCODE + ', ' + clientInfo.STATE);
        self.clientDetails.CLIENTCONTACT('Tel.No: ' + clientInfo.CONTACT_NO1 + ' Fax No:' + clientInfo.CONTACT_NO2);

        document.getElementById("printSummary").classList.add('printSummary');
        document.getElementById("printSticker").style.display = "none";
        document.getElementById("returnSticker").style.display = "none";

        $(ah.config.id.assetPrintSearchResultList).html('');
        $(ah.config.id.assetPrintSearchResultList).append($(ah.config.id.searchResultAsset).html()); //searchResultList
        setTimeout(function () {
            window.location.href = ah.config.url.modalClose;
            window.onafterprint = function () { document.getElementById("printSticker").style.display = ""; }
            window.onafterprint = function () { document.getElementById("returnSticker").style.display = ""; }

            window.print();
        }, 6000);


    };
    self.imagesLoader = function () {
        var assetpDetails = sessionStorage.getItem(ah.config.skey.assetDetails);
        var jassetpDetails = JSON.parse(assetpDetails);
        var reqWoNO = ah.getParameterValueByName(ah.config.fld.reqWONo);
        window.location.href = encodeURI(ah.config.url.aimsImages + "?ASSETNO=" + jassetpDetails.ASSET_NO + "&ASSETDESC=" + jassetpDetails.DESCRIPTION + "&REQNO=" + reqWoNO);
    };
    self.documentLoader = function () {
        var assetpDetails = sessionStorage.getItem(ah.config.skey.assetDetails);
        var jassetpDetails = JSON.parse(assetpDetails);
        var reqWoNO = ah.getParameterValueByName(ah.config.fld.reqWONo);
        window.location.href = encodeURI(ah.config.url.aimsImages + "?ASSETNO=" + jassetpDetails.ASSET_NO + "&ASSETDESC=" + jassetpDetails.DESCRIPTION + "&REQNO=" + reqWoNO);
    };
    self.infoServices = function () {
        var assetpDetails = sessionStorage.getItem(ah.config.skey.assetDetails);
        var jassetpDetails = JSON.parse(assetpDetails);
        var reqWoNO = ah.getParameterValueByName(ah.config.fld.reqWONo);
        window.location.href = encodeURI(ah.config.url.aimsInfoServices + "?ASSETNO=" + jassetpDetails.ASSET_NO + "&ASSETDESC=" + jassetpDetails.DESCRIPTION + "&REQNO=" + reqWoNO);
    };

    self.searchLookupResult = function (jsonData) {

        var sr = jsonData.LOV_LOOKUPS;
        //alert(JSON.stringify(sr));
        $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');
        if (sr.length > 0) {
            var htmlstr = "";
            for (var o in sr) {

                if (sr[o].DESCRIPTION.length > 50)
                    sr[o].DESCRIPTION = sr[o].DESCRIPTION.substring(0, 49) + "...";

                htmlstr += '<li>';
                htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                htmlstr += ah.formatString(ah.config.html.searchExistRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].DESCRIPTION);



                htmlstr += '</span>';
                htmlstr += '</li>';

            }

            //alert(htmlstr);
            $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.lookupDetailItem).bind('click', self.assignLookup);
        //ah.toggleDisplayMode();
        //ah.toggleAddItems();
    };

    self.searchLookupResultmanufacturer = function (jsonData) {

        var sr = jsonData.AM_MANUFACTURER;

        $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');
        if (sr.length > 0) {
            var htmlstr = "";
            for (var o in sr) {

                if (sr[o].DESCRIPTION.length > 50)
                    sr[o].DESCRIPTION = sr[o].DESCRIPTION.substring(0, 49) + "...";

                htmlstr += '<li>';
                htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                htmlstr += ah.formatString(ah.config.html.searchExistRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].DESCRIPTION);



                htmlstr += '</span>';
                htmlstr += '</li>';

            }

            //alert(htmlstr);
            $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        // $(ah.config.cls.lookupDetailItem).bind('click', self.assignLookup);
        //ah.toggleDisplayMode();
        //ah.toggleAddItems();
    };

    self.searchLookupResultsupplier = function (jsonData) {

        var sr = jsonData.AM_SUPPLIER;

        $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');
        if (sr.length > 0) {
            var htmlstr = "";
            for (var o in sr) {

                if (sr[o].DESCRIPTION.length > 50)
                    sr[o].DESCRIPTION = sr[o].DESCRIPTION.substring(0, 49) + "...";

                htmlstr += '<li>';
                htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                htmlstr += ah.formatString(ah.config.html.searchExistRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].DESCRIPTION);



                htmlstr += '</span>';
                htmlstr += '</li>';

            }

            //alert(htmlstr);
            $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.lookupDetailItem).bind('click', self.assignLookupSupplier);
        //ah.toggleDisplayMode();
        //ah.toggleAddItems();
    };
    self.searchResultClone = function (jsonData) {

        var sr = jsonData.AM_ASSET_DETAILS_CLONE;

        var rowCount = 1;
        self.cloneAssetData.splice(0, 5000);
        if (sr.length > 0) {
            $("#CloneIds").text("Clone Id: " + sr[0].CLONE_ID + "   ");
            for (var o in sr) {
                var locationDate = ah.formatJSONDateToString(sr[o].LOCATION_DATE);
                var conditionDate = ah.formatJSONDateToString(sr[o].CONDITION_DATE);

                self.cloneAssetData.push({
                    ROWCOUNT: rowCount,
                    ASSET_NO: sr[o].ASSET_NO,
                    BUILDING: sr[o].BUILDING,
                    RESPONSIBLE_CENTER: sr[o].RESPONSIBLE_CENTER,
                    COST_CENTER: sr[o].COST_CENTER,
                    LOCATION: sr[o].LOCATION,
                    LOCATION_DATE: locationDate,
                    SERIAL_NO: sr[o].SERIAL_NO,
                    CLONE_ID: sr[0].CLONE_ID,
                    CONDITION_DATE: conditionDate,
                    buildingLOV: buildingList,
                    costcenterLOV: costCenterList,
                    responsiblecenterLOV: costCenterList

                });
                rowCount++;
            }
        }

        window.location.href = ah.config.url.openModalClone;



    };


    self.searchResultCloneList = function (jsonData) {
        self.searchFilter.CloneId = null;
        $("#autogenerateWOIns").hide();
        $("#addConeButtonIcon").hide();
        var buildingLOV = jsonData.LOV_LOOKUPS;
        buildingLOV = buildingLOV.filter(function (item) { return item.CATEGORY === 'AIMS_BUILDING' });
        buildingList = buildingLOV;
        var costcenterLOV = jsonData.LOV_LOOKUPS;
        costcenterLOV = costcenterLOV.filter(function (item) { return item.CATEGORY === 'AIMS_COSTCENTER' });
        costCenterList = costcenterLOV;

        var sr = jsonData.AM_ASSET_DETAILSCLONE;

        var rowCount = 1;
        self.cloneAssetData.splice(0, 5000);
        if (sr.length > 0) {
            $("#winTitleCloneG").text("Clone List for " + sr[0].DESCRIPTION);
            $("#CloneIds").text("Clone Id: " + sr[0].CLONE_ID + "   ");
            for (var o in sr) {


                var locationDate = ah.formatJSONDateToString(sr[o].LOCATION_DATE);
                var conditionDate = ah.formatJSONDateToString(sr[o].CONDITION_DATE);

                self.cloneAssetData.push({
                    ROWCOUNT: rowCount,
                    ASSET_NO: sr[o].ASSET_NO,
                    BUILDING: sr[o].BUILDING,
                    RESPONSIBLE_CENTER: sr[o].RESPONSIBLE_CENTER,
                    COST_CENTER: sr[o].COST_CENTER,
                    LOCATION: sr[o].LOCATION,
                    LOCATION_DATE: locationDate,
                    SERIAL_NO: sr[o].SERIAL_NO,
                    CONDITION_DATE: conditionDate,
                    CLONE_ID: sr[0].CLONE_ID,
                    buildingLOV: buildingList,
                    costcenterLOV: costCenterList,
                    responsiblecenterLOV: costCenterList

                });
                rowCount++;
            }
        }


        window.location.href = ah.config.url.openModalClone;



    };

    self.assignLookup = function () {
        var lookUpInfo = JSON.parse(this.getAttribute(ah.config.attr.datalookUp));
        var lovData = $(ah.config.id.lovAction).val();

        switch (lovData) {

            case "CATEGORY": {
                $(ah.config.id.assetCategory).val(lookUpInfo.LOV_LOOKUP_ID);
                $("#CATEGORYDESC").val(lookUpInfo.DESCRIPTION);
                break;
            }
            case "MANUFACTURER": {
                $(ah.config.id.assetManufacturer).val(lookUpInfo.MANUFACTURER_ID);
                $("#MANUFACTURERDESC").val(lookUpInfo.DESCRIPTION);
                break;
            }
            case "SUPPLIER": {

                $(ah.config.id.assetSupplier).val(lookUpInfo.SUPPLIER_ID);
                $("#SUPPLIERDESC").val(lookUpInfo.DESCRIPTION);
                break;
            }
            case "CurrentAgent": {
                $(ah.config.id.current_Agent).val(lookUpInfo.SUPPLIER_ID);
                $("#CURRENTAGENTDESC").val(lookUpInfo.DESCRIPTION);
                break;
            }
            case "BUILDING": {
                $(ah.config.id.assetBuilding).val(lookUpInfo.LOV_LOOKUP_ID);
                $("#BUILDINGDESC").val(lookUpInfo.DESCRIPTION);
                break;
            }

            case "MANUFACTURERMODEL": {
                $("#MODEL_NO").val(lookUpInfo.MODEL);
                $("#MODEL_NAME").val(lookUpInfo.DESCRIPTION);
                break;
            }

            case "COSTCENTER": {
                $(ah.config.id.assetCostCenter).val(lookUpInfo.LOV_LOOKUP_ID);
                $("#COST_CENTERDESC").val(lookUpInfo.DESCRIPTION);
                self.CostCenter.Name(lookUpInfo.DESCRIPTION);
                if (lookUpInfo.LOV_LOOKUP_ID != curCostCenter && lookUpInfo.LOV_LOOKUP_ID != self.DefaultCostCenter.CODE) {
                    $(ah.config.cls.divCostCenter).show();
                    document.getElementById("COSTCENTERPURCHASEDATE").setAttribute("required", "required");
                    document.getElementById("COSTCENTERINVOICENO").setAttribute("required", "required");
                    document.getElementById("COSTCENTERPURCHASECOST").setAttribute("required", "required");
                    document.getElementById("COSTCENTERASSETCOND").setAttribute("required", "required");
                    document.getElementById("COSTCENTERINSTALLDATE").setAttribute("required", "required");
                } else {
                    document.getElementById("COSTCENTERPURCHASEDATE").removeAttribute("required");
                    document.getElementById("COSTCENTERINVOICENO").removeAttribute("required");
                    document.getElementById("COSTCENTERPURCHASECOST").removeAttribute("required");
                    document.getElementById("COSTCENTERASSETCOND").removeAttribute("required");
                    document.getElementById("COSTCENTERINSTALLDATE").removeAttribute("required");
                    $(ah.config.cls.divCostCenter).hide();
                }
                break;
            }
            case "RESPONSIBLECENTER": {
                $(ah.config.id.assetResponsibleCenter).val(lookUpInfo.LOV_LOOKUP_ID);
                $("#RESPONSIBLE_CENTERDESC").val(lookUpInfo.DESCRIPTION);
                break;
            }
            case "LOCATION": {
                $(ah.config.id.assetLocation).val(lookUpInfo.LOV_LOOKUP_ID);
                break;
            }

        }
    };



    self.assignLookupSupplier = function () {
        var lookUpInfo = this.getAttribute(ah.config.attr.datalookUp);


        $("#SUPPLIER").val(JSON.parse(lookUpInfo).DESCRIPTION);
    };

    self.readSetupID = function (action) {

        var assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        var infoID = "";
        if (assetNoID !== null && assetNoID !== "") {
            infoID = assetNoID
        } else {
            infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
        }

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { ASSET_NO: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.readAssetNo = function () {


        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();
        assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        postData = { ASSET_NO: assetNoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.getNow = function () {
        var now = moment();

        return now;
    };
    self.isDateFuture = function (testDate, tolerance) {
        var now = self.getNow();
        return testDate.diff(now, 'days') >= tolerance;
    };
    self.isDateFutreCompare = function (date1, date2) {
        if (date1 < date2) {
            return true;
        }
        return false;

    }
    self.searchNow = function () {
        self.getLastSearch();
        assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);       
        if (assetNoID !== null && assetNoID !== "") {
            urlText = 'ljllk';
            history.pushState('', 'New Page Title', ah.config.url.aimsDetails);
        }


        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
        var postData;

        gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID == "15" });
        if (gPrivileges.length <= 0) {
            self.searchFilter.staffId = staffLogonSessions.STAFF_ID;
        }

        gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID == "59" });
        if (gPrivileges.length > 0) {
            self.searchFilter.companyAccess = "BSCADMIN";
        }

        ah.toggleSearchMode();
        var searchStr = $(ah.config.id.txtGlobalSearch).val().trim();
        postData = { SEARCH_FILTER: self.searchFilter, SEARCH: searchStr.toString(), STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);


    };
    self.searchAdvanceLOV = function () {
        if (lovCostCenterArray.length == 0 || lovManufacturerArray.length == 0 || lovSupplierArray.length == 0) {
            var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
            var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
            var postData;
            postData = { LOV: "'AIMS_COSTCENTER'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
    };
    self.searchAssetDesc = function () {
        alert('here');
    };
    self.searchNowCloneId = function () {
        var cloneId = $("#CLONE_ID").val();
        if (cloneId == null || cloneId == "") {
            return;
        } else {
            self.searchFilter.CloneId = cloneId;
            self.searchFilter.status = null;
        }

        assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        if (assetNoID !== null && assetNoID !== "") {
            urlText = 'ljllk';
            history.pushState('', 'New Page Title', ah.config.url.aimsDetails);
        }


        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
        var postData;

        gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "15" });
        if (gPrivileges.length <= 0) {
            self.searchFilter.staffId = staffLogonSessions.STAFF_ID;
        }

        //ah.toggleSearchMode();

        postData = { SEARCH_FILTER: self.searchFilter, SEARCH: "", LOV: "'AIMS_BUILDING','AIMS_COSTCENTER'", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.seachByCloneId, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.closeModalalert = function () {
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none"
    }
    self.hideCalendar = function () {
        var elementExists = document.getElementById("tcal");
        if (elementExists != null) {
            document.getElementById("tcal").style.visibility = null;
        }

        document.getElementById("LOCATION_DATE").className = "datefield tcal tcalInput"
        document.getElementById("INSERVICE_DATE").className = "datefield tcal tcalInput"
        document.getElementById("CONDITION_DATE").className = "datefield tcal tcalInput"
        document.getElementById("PURCHASE_DATE").className = "datefield tcal tcalInput"
        //document.getElementById("WARRANTY_EXPIRYDATE").className = "datefield tcal tcalInput"
        document.getElementById("REPLACEMENT_DATE").className = "datefield tcal tcalInput"
        document.getElementById("SALVAGE_DATE").className = "datefield tcal tcalInput"
        document.getElementById("INVENTORY_DATE").className = "datefield tcal tcalInput"
    }
    self.updateAssetClone = function () {
        window.location.href = ah.config.url.modalClose;
        self.hideCalendar();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var setupDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var setupDetailsOth = ah.ConvertFormToJSON($(ah.config.id.itemInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        var assetNO = $("#ASSET_NO").val();
        var jsonObj = ko.observableArray([]);

        jsonObj = ko.utils.stringifyJson(self.cloneAssetData);
        var toUpdate = [];

        var cloneInfoList = JSON.parse(jsonObj);
        var cloneData = JSON.parse(jsonObj);

        for (i in cloneInfoList) {
            var cloneFilter = cloneData;

            //alert(JSON.stringify(cloneFilter));
            //check double entry of serial no
            cloneFilter = cloneFilter.filter(function (item) { return item.SERIAL_NO == cloneInfoList[i].SERIAL_NO && item.ROWCOUNT != cloneInfoList[i].ROWCOUNT });

            if (cloneFilter.length > 0) {
                alert("Unable to proceed. Record No. " + cloneInfoList[i].ROWCOUNT + " have duplicate serial no. with Record. No. " + cloneFilter[0].ROWCOUNT);
                window.location.href = ah.config.url.openModalClone;
                return;
            }


            var locationDate = cloneInfoList[i].LOCATION_DATE;

            cloneInfoList[i].CREATED_DATE = self.isValidDate(cloneInfoList[i].CREATED_DATE);
            if (locationDate != null && locationDate != "") {
                locationDate = self.isValidDate(locationDate);
                if (locationDate === 'Invalid') {
                    alert("Record NO. " + cloneInfoList[i].ROWCOUNT + " Invalid Date Format Location Date. It should be (DD/MM/YYYY)")
                    window.location.href = ah.config.url.openModalClone;
                    return;

                } else {
                    cloneInfoList[i].LOCATION_DATE = locationDate;
                }



            }
            var installationDate = cloneInfoList[i].CONDITION_DATE;

            if (installationDate != null && installationDate != "") {
                installationDate = self.isValidDate(installationDate);
                if (installationDate === 'Invalid') {
                    alert("Record NO. " + cloneInfoList[i].ROWCOUNT + " Invalid Date Format Installation Date. It should be (DD/MM/YYYY)")
                    window.location.href = ah.config.url.openModalClone;
                    return;

                } else {

                    cloneInfoList[i].CONDITION_DATE = installationDate;
                }



            }
            toUpdate.push({ ASSET_NO: cloneInfoList[i].ASSET_NO, BUILDING: cloneInfoList[i].BUILDING, SERIAL_NO: cloneInfoList[i].SERIAL_NO, COST_CENTER: cloneInfoList[i].COST_CENTER, LOCATION_DATE: cloneInfoList[i].LOCATION_DATE, LOCATION: cloneInfoList[i].LOCATION, RESPONSIBLE_CENTER: cloneInfoList[i].RESPONSIBLE_CENTER, CONDITION_DATE: cloneInfoList[i].CONDITION_DATE, CREATED_DATE: null, CLONE_ID: cloneInfoList[i].CLONE_ID })

        }

        //alert(JSON.stringify(toUpdate));
        postData = { AM_ASSET_DETAILS_CLONE: toUpdate, STAFF_LOGON_SESSIONS: staffLogonSessions };
        ah.toggleSaveMode();

        $.post(self.getApi() + ah.config.api.updateAssetClone, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);


    };

    self.returnOnHand = function () {
        $("#mdedefaultCC").text("This will Automatically return to " + self.DefaultCostCenter.DESCRIPTION);
        $("#MRETURNTYPE").prop(ah.config.attr.disabled, false);
        $("#MREASON").prop(ah.config.attr.disabled, false);
        window.location.href = ah.config.url.openModelReturnOnHand;
    };

    self.savereturnOnHand = function () {
        $(ah.config.id.returntoCompany).hide();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
       //var setupDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        var infoDetails = sessionStorage.getItem(ah.config.skey.assetDetails);
        var jsetupDetails = JSON.parse(infoDetails);

        $("#CATEGORYDESC").val(self.DefaultCostCenter.DESCRIPTION);
        
        var assetTransaction = {
            ASSET_NO: $("#ASSET_NO").val(),
            RETURNEDREASON: $("#MRETURNTYPE").val(),
            RETURNEDREMARKS: $("#MREASON").val(),
            TRANSID: $("#COSTCENTERTRANSID").val(),
            COSTCENTERCODE: self.DefaultCostCenter.CODE
        }
        

        ah.toggleSaveMode();
        postData = { AM_ASSET_DETAILS: jsetupDetails, AM_ASSET_TRANSACTION: assetTransaction, COSTCENTER: self.DefaultCostCenter.CODE, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.updateTransaction, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };


  
    self.saveInfo = function () {
        var x = document.getElementById("frmInfo").elements.length;

        if (x < xElements) {
            self.alertMessage("Unable to proceed. Please refresh the screen deleting element from html is prohibited.");
            return;
        }
        self.hideCalendar();

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var setupDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var setupDetailsOth = ah.ConvertFormToJSON($(ah.config.id.itemInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        setupDetails.CREATED_BY = staffLogonSessions.STAFF_ID;

        if (setupDetails.COSTCENTERPURCHASECOST === null || setupDetails.COSTCENTERPURCHASECOST === "") {
            setupDetails.COSTCENTERPURCHASECOST = 0;
        }


        var assetTransaction = {
            ASSET_NO: setupDetails.ASSET_NO,
            COSTCENTERINSTALLDATE: setupDetails.COSTCENTERINSTALLDATE,
            COSTCENTERCODE: setupDetails.COST_CENTER,
            COSTCENTERPURCHASEDATE: setupDetails.COSTCENTERPURCHASEDATE,
            COSTCENTERPURCHASECOST: setupDetails.COSTCENTERPURCHASECOST,
            COSTCENTERASSETCOND: setupDetails.COSTCENTERASSETCOND,
            COSTCENTERINVOICENO: setupDetails.COSTCENTERINVOICENO
        }




        var jsonObj = ko.observableArray([]);

        jsonObj = ko.utils.stringifyJson(self.warrantyData);

        var warrantyToPushList = [];
        var warrantyInfo = JSON.parse(jsonObj);

        var warrantyDate = null;

        if (warrantyInfo.length > 0) {
            for (var i in warrantyInfo) {
                if (warrantyInfo[i].WARRANTY_EXPIRY == null || warrantyInfo.FREQUENCY == null || warrantyInfo.FREQUENCY_PERIOD == null) {





                }
                if (warrantyInfo[i].WARRANTY_EXPIRY != null && warrantyInfo[i].WARRANTY_EXPIRY != "") {
                    var warrantyDate = self.isValidDate(warrantyInfo[i].WARRANTY_EXPIRY);

                    if (warrantyDate === 'Invalid') {
                        alert("Record NO. " + warrantyInfo[i].ROWCOUNT + " Invalid Date Format. It should be (DD/MM/YYYY)")
                        return;
                    }
                }

                warrantyInfo[i].WARRANTY_EXPIRY = warrantyDate;
                warrantyToPushList.push(warrantyInfo[i]);

            }

        }



        var check = self.isDateFuture(moment(setupDetails.LOCATION_DATE, 'DD/MM/YYYY'), 1);



        check = self.isDateFuture(moment(setupDetails.PURCHASE_DATE, 'DD/MM/YYYY'), 1);
        if (check) {
            self.alertMessage("Unable to proceed, Please make sure that the Purchase date entered is not a future date.");

            return;
        };


        if (setupDetails.CONDITION_DATE != null && setupDetails.PURCHASE_DATE != null) {

            var checkreqDate = self.isDateFutreCompare(moment(setupDetails.CONDITION_DATE, 'DD/MM/YYYY hh:mm:ss'), moment(setupDetails.PURCHASE_DATE, 'DD/MM/YYYY hh:mm:ss'));
            if (checkreqDate) {
                self.alertMessage("Unable to proceed, Condition Date cannot be less than purchase date.");

                return;
            };
        }

        var inventoryDateCheck = self.isDateFuture(moment(setupDetails.INVENTORY_DATE, 'DD/MM/YYYY'), 1);
        if (inventoryDateCheck) {
            self.alertMessage("Unable to proceed, Please make sure that the Inventory date entered is not a future date.");
            return;
        };
        if (setupDetails.INVENTORY_DATE != null && setupDetails.INSERVICE_DATE != null) {

            var checkreqDate = self.isDateFutreCompare(moment(setupDetails.INSERVICE_DATE, 'DD/MM/YYYY hh:mm:ss'), moment(setupDetails.INVENTORY_DATE, 'DD/MM/YYYY hh:mm:ss'));
            if (checkreqDate) {
                self.alertMessage("Unable to proceed, Inventory Date should be before the In Service Date.");

                return;
            };
        }
        if (setupDetails.INVENTORY_DATE != null && assetTransaction.COSTCENTERINSTALLDATE != null) {

            var checkreqDate = self.isDateFutreCompare(moment(setupDetails.COSTCENTERINSTALLDATE, 'DD/MM/YYYY hh:mm:ss'), moment(setupDetails.INVENTORY_DATE, 'DD/MM/YYYY hh:mm:ss'));
            if (checkreqDate) {
                self.alertMessage("Unable to proceed, Inventory Date should be before the Installation Date.");

                return;
            };
        }

        if (setupDetails.LOCATION_DATE == "" || setupDetails.LOCATION_DATE == "null" || setupDetails.LOCATION_DATE == "00/00/0000" || setupDetails.LOCATION_DATE == "00-00-0000") {
            setupDetails.LOCATION_DATE = "01/01/1900";
        }



        if (setupDetails.INSERVICE_DATE == "" || setupDetails.INSERVICE_DATE == "null" || setupDetails.INSERVICE_DATE == "00/00/0000" || setupDetails.INSERVICE_DATE == "00-00-0000") {
            setupDetails.INSERVICE_DATE = "01/01/1900";
        }

        if (setupDetails.STATUS_DATE == "" || setupDetails.STATUS_DATE == "null" || setupDetails.STATUS_DATE == "00/00/0000" || setupDetails.STATUS_DATE == "00-00-0000") {
            setupDetails.STATUS_DATE = "01/01/1900";
        }

        if (setupDetails.PURCHASE_DATE == "" || setupDetails.PURCHASE_DATE == "null" || setupDetails.PURCHASE_DATE == "00/00/0000" || setupDetails.PURCHASE_DATE == "00-00-0000") {
            setupDetails.PURCHASE_DATE = "01/01/1900";
        }
        if (setupDetails.REPLACEMENT_DATE == "" || setupDetails.REPLACEMENT_DATE == "null" || setupDetails.REPLACEMENT_DATE == "00/00/0000" || setupDetails.REPLACEMENT_DATE == "00-00-0000") {
            setupDetails.REPLACEMENT_DATE = "01/01/1900";
        }
        if (setupDetails.SALVAGE_DATE == "" || setupDetails.SALVAGE_DATE == "null" || setupDetails.SALVAGE_DATE == "00/00/0000" || setupDetails.SALVAGE_DATE == "00-00-0000") {
            setupDetails.SALVAGE_DATE = "01/01/1900";
        }
        if (setupDetails.CONDITION_DATE == "" || setupDetails.CONDITION_DATE == "null" || setupDetails.CONDITION_DATE == "00/00/0000" || setupDetails.CONDITION_DATE == "00-00-0000") {
            setupDetails.CONDITION_DATE = "01/01/1900";
        }
        if (setupDetails.WARRANTY_EXPIRYDATE == "" || setupDetails.WARRANTY_EXPIRYDATE == "null" || setupDetails.WARRANTY_EXPIRYDATE == "00/00/0000" || setupDetails.WARRANTY_EXPIRYDATE == "00-00-0000") {
            setupDetails.WARRANTY_EXPIRYDATE = "01/01/1900";
        }
        if (setupDetails.INVENTORY_DATE == "" || setupDetails.INVENTORY_DATE == "null" || setupDetails.INVENTORY_DATE == "00/00/0000" || setupDetails.INVENTORY_DATE == "00-00-0000") {
            setupDetails.INVENTORY_DATE = "01/01/1900";
        }
        if (setupDetails.DEPRECIATED_LIFE === null || setupDetails.DEPRECIATED_LIFE === "") {
            setupDetails.DEPRECIATED_LIFE = 0;
        }

        if (setupDetails.DEPRECIATION_PERCENT == null || setupDetails.DEPRECIATION_PERCENT == "") {
            setupDetails.DEPRECIATION_PERCENT = 10;
        }
        if (setupDetails.USEFULL_LIFE === null || setupDetails.USEFULL_LIFE === "") {
            setupDetails.USEFULL_LIFE = 0;
        }
        if (setupDetails.HAYEARTODATEOTH === null || setupDetails.HAYEARTODATEOTH === "") {
            setupDetails.HAYEARTODATEOTH = 0;
        }

        if (setupDetails.LIFETODATEOTH === null || setupDetails.LIFETODATEOTH === "") {
            setupDetails.LIFETODATEOTH = 0;
        }
        if (setupDetails.PREVIOUSYROTH === null || setupDetails.PREVIOUSYROTH === "") {
            setupDetails.PREVIOUSYROTH = 0;
        }

        if (setupDetails.YEARTODATEOTH === null || setupDetails.YEARTODATEOTH === "") {
            setupDetails.YEARTODATEOTH = 0;
        }

        if (setupDetails.RISK_INCLUSIONFACTOR === null || setupDetails.RISK_INCLUSIONFACTOR === "") {
            setupDetails.RISK_INCLUSIONFACTOR = 0;
        }

        if (setupDetails.PO_DATE != null || setupDetails.PO_DATE != "") {
            setupDetails.PO_DATE = setupDetails.PO_DATE + "@";
        }


        if (setupDetails.PURCHASE_COST === null || setupDetails.PURCHASE_COST === "") {
            setupDetails.PURCHASE_COST = 0;
        }

        if (ah.CurrentMode == ah.config.mode.add) {
            var jDate = new Date();
            var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);

            setupDetails.CREATED_DATE = strDateStart + ' ' + hourmin;
            postData = { AM_ASSET_DETAILS: setupDetails, AM_ASSET_WARRANTY: warrantyToPushList, STAFF_LOGON_SESSIONS: staffLogonSessions };
            ah.toggleSaveMode();
            // alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.createSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {



            var jsonCCObj = ko.observableArray([]);

            jsonCCObj = ko.utils.stringifyJson(self.ccwarrantyData);

            var ccwarrantyToPushList = [];
            var ccwarrantyInfo = JSON.parse(jsonCCObj);

            var ccwarrantyDate = null;

            if (ccwarrantyInfo.length > 0) {
                for (var i in ccwarrantyInfo) {
                    if (ccwarrantyInfo[i].CCWARRANTY_EXPIRY == null || ccwarrantyInfo.CCWARRANTY_EXPIRY == null || ccwarrantyInfo.CCWARRANTY_EXPIRY == null) {





                    }
                    if (ccwarrantyInfo[i].CCWARRANTY_EXPIRY != null && ccwarrantyInfo[i].CCWARRANTY_EXPIRY != "") {
                        var ccwarrantyDate = self.isValidDate(ccwarrantyInfo[i].CCWARRANTY_EXPIRY);

                        if (ccwarrantyDate === 'Invalid') {
                            alert("Record NO. " + ccwarrantyInfo[i].CCROWCOUNT + " Cost Center Warranty invalid Date Format. It should be (DD/MM/YYYY)")
                            return;
                        }
                    }

                    ccwarrantyInfo[i].CCWARRANTY_EXPIRY = ccwarrantyDate;
                    ccwarrantyToPushList.push(ccwarrantyInfo[i]);

                }

            }


            ah.toggleSaveMode();
            postData = { AM_ASSET_DETAILS: setupDetails, AM_ASSET_TRANSACTION: assetTransaction, AM_ASSET_WARRANTY: warrantyToPushList, AM_ASSET_COSTCENTER_WARRANTY: ccwarrantyToPushList, STAFF_LOGON_SESSIONS: staffLogonSessions };
            // alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.updateSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }

    };
    self.privilegesValidation = function (actionVal) {

        var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
        if (actionVal === 'NEW') {
            gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "16" });
            if (gPrivileges.length <= 0) {
                $(ah.config.cls.liAddNew).hide();
            } else {
                $(ah.config.cls.liAddNew).show();
            }
        }
        else if (actionVal === 'MODIFY') {
            gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "3" });
            if (gPrivileges.length <= 0) {
                $(ah.config.id.assetModify + ',' + ah.config.id.addButtonIcon).hide();
            }
        }
        else if (actionVal === 'NEWCLONE') {
            gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "29" });

            if (gPrivileges.length <= 0) {

                $("#liClone").hide();
                $("#liClone2").hide();
            }
        }
        else if (actionVal === 'CLONELIST') {
            gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "30" });

            if (gPrivileges.length <= 0) {

                $("#liCloneList").hide();
            }
        }
        else if (actionVal === 'REQUESTOR') {
            gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "40" });

            if (gPrivileges.length > 0) {
                $(ah.config.cls.liAddNew + ',' + ah.config.cls.liModify).hide();
                $(ah.config.id.assetModify + ',' + ah.config.id.addButtonIcon).hide();

                $(ah.config.cls.iconDet).hide();

                $("#liCloneList").hide();
                $("#liClone").hide();
                $("#liClone2").hide();

                $("#historyWO").show();
            }
        } else if (actionVal === "MODIFYCOSTCENCOMPANY") {
            gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "50" });

            if (gPrivileges.length > 0) {
                $(ah.config.cls.liAddNew + ',' + ah.config.cls.liModify).hide();
                $(ah.config.id.assetModify + ',' + ah.config.id.addButtonIcon).hide();

                $(ah.config.cls.iconDet).hide();

                $("#liCloneList").hide();
                $("#liClone").hide();
                $("#liClone2").hide();
                $("#returntoCompany").hide();
                $("#historyWO").show();
            }
        }
        else if (actionVal === "RETURNTOCOMPANY") {
            gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "52" });

            if (gPrivileges.length > 0) {
               // $(ah.config.cls.liAddNew + ',' + ah.config.cls.liModify).hide();
                $(ah.config.cls.liAddNew + ',' + ah.config.id.assetModify + ',' + ah.config.id.addButtonIcon).hide();
                $(ah.config.id.returntoCompany).show();
                $(ah.config.cls.iconDet).hide();

                $("#liCloneList").hide();
                $("#liClone").hide();
                $("#liClone2").hide();
                $("#historyWO").show();
            }
        }

        else if (actionVal === "COMPANYADMIN") {
            gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "59" });

            if (gPrivileges.length > 0) {
                document.getElementById("warrantyList").style["pointer-events"] = "visible";
                document.getElementById("ccwarrantyList").style["pointer-events"] = "visible";
                $(ah.config.cls.divCompany).show();
            } else {
                document.getElementById("warrantyList").style["pointer-events"] = "none";
                document.getElementById("ccwarrantyList").style["pointer-events"] = "none";
                $(ah.config.cls.divCompany).hide();
            }
        }


    }
    self.webApiCallbackStatus = function (jsonData) {
       
        //alert(JSON.stringify(jsonData.AM_MANUFACTURER));
        if (jsonData.ERROR) {
            var errorText = jsonData.ERROR.ErrorText;
            var res = errorText.substring(0, 12);

            if (res === "ASSETNOEXIST") {
                ah.toggleAddExistMode();

                $("#ASSET_NO").next("span").remove();
                $("#ASSET_NO").after("<span style='color:red;padding-left:5px;'>* Asset No./Tag already in use</span>")

            } else {
                alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            }


        }
        else if (jsonData.AM_ASSET_TRANSACTION || jsonData.PARTS_GROUPBYASSETYR_V || jsonData.AM_CLIENT_INFO || jsonData.AM_ASSET_DETAILS || jsonData.AM_ASSET_DETAILSCLONE || jsonData.AM_ASSET_PMSCHEDULES || jsonData.AM_ASSET_DETAILS_CLONE || jsonData.CLONE || jsonData.AM_ASSET_WARRANTY || jsonData.AM_MANUFACTURER_MODEL || jsonData.AM_WORK_ORDER_REFNOTES || jsonData.AM_ASSET_NOTES || jsonData.RISK_CATEGORYFACTOR || jsonData.STAFFLIST || jsonData.ASSET_WOMATERIALCOST_V || jsonData.ASSET_WOCONTRACTCOST_V || jsonData.ASSET_WOPMOTCOST_V || jsonData.ASSET_WOSUMMARY_V || jsonData.AM_SUPPLIER || jsonData.AM_MANUFACTURER || jsonData.LOV_LOOKUPS || jsonData.AM_ASSET_DETAILS || jsonData.STAFF || jsonData.SUCCESS) {

            $("#ASSET_NO").next("span").remove();

            if (ah.CurrentMode == ah.config.mode.save) {
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();

                if (jsonData.SUCCESS) {
                    if (jsonData.AM_ASSET_WARRANTY) {
                        warrantyList = jsonData.AM_ASSET_WARRANTY;
                        ccwarrantyList = jsonData.AM_ASSET_COSTCENTER_WARRANTY;
                    }

                    self.showDetails(false);
                    window.location.href = ah.config.url.modalClose;
                } else if (jsonData.AM_ASSET_DETAILS_CLONE) {
                    $(ah.config.id.UpdateButtonIcon).show();
                    $(ah.config.id.addConeButtonIcon).hide();
                    self.searchResultClone(jsonData);
                }
                else {
                    sessionStorage.setItem(ah.config.skey.woAssetSummary, JSON.stringify(jsonData.ASSET_WOSUMMARY_V));
                    sessionStorage.setItem(ah.config.skey.assetDetails, JSON.stringify(jsonData.AM_ASSET_DETAILS));
                    if (jsonData.AM_ASSET_WARRANTY) {
                        warrantyList = jsonData.AM_ASSET_WARRANTY;
                        ccwarrantyList = jsonData.AM_ASSET_COSTCENTER_WARRANTY;
                    }
                    self.showDetails(true);
                }
            }
            else if (ah.CurrentMode == ah.config.mode.searchResult) {
                if (self.isPrint == 3)
                    self.returnSticker(jsonData);
                if (self.isPrint == 2)
                    self.printSticker(jsonData);
                if (self.isPrint == 1)
                    self.print(jsonData);
                if (self.isPrint == 4)
                    self.OutOrderSticker(jsonData);
            }

            else if (ah.CurrentMode == ah.config.mode.read) {
                sessionStorage.setItem(ah.config.skey.assetDetails, JSON.stringify(jsonData.AM_ASSET_DETAILS));
                sessionStorage.setItem(ah.config.skey.costcenterTransaction, JSON.stringify(jsonData.AM_ASSET_TRANSACTION));
                sessionStorage.setItem(ah.config.skey.woAssetSummary, JSON.stringify(jsonData.ASSET_WOSUMMARY_V));
                sessionStorage.setItem(ah.config.skey.lovList, JSON.stringify(jsonData.LOV_LOOKUPS));
                sessionStorage.setItem(ah.config.skey.assetSupplierList, JSON.stringify(jsonData.AM_SUPPLIER));
                sessionStorage.setItem(ah.config.skey.assetCurrentAgent, JSON.stringify(jsonData.CURRENTAGENT));
                sessionStorage.setItem(ah.config.skey.assetManufacturerList, JSON.stringify(jsonData.AM_MANUFACTURER));
                costCenterList = jsonData.CLIENT_COSTCENTER;
                warrantyList = jsonData.AM_ASSET_WARRANTY;
                ccwarrantyList = jsonData.AM_ASSET_COSTCENTER_WARRANTY;


                self.showDetails(true);
            }
            else if (ah.CurrentMode == ah.config.mode.advanceSearch) {
                lovCostCenterArray = jsonData.LOV_LOOKUPS;
                $.each(lovCostCenterArray, function (item) {

                    $(ah.config.id.costcenterStrList)
                        .append($("<option>")
                            .attr("value", lovCostCenterArray[item].DESCRIPTION.trim())
                            .attr("id", lovCostCenterArray[item].LOV_LOOKUP_ID));
                });

                lovManufacturerArray = jsonData.AM_MANUFACTURER;
                $.each(lovManufacturerArray, function (item) {

                    $(ah.config.id.manufacturerStrList)
                        .append($("<option>")
                            .attr("value", lovManufacturerArray[item].DESCRIPTION.trim())
                            .attr("id", lovManufacturerArray[item].MANUFACTURER_ID));
                });

                lovSupplierArray = jsonData.AM_SUPPLIER;
                $.each(lovSupplierArray, function (item) {

                    $(ah.config.id.supplierStrList)
                        .append($("<option>")
                            .attr("value", lovSupplierArray[item].DESCRIPTION.trim())
                            .attr("id", lovSupplierArray[item].SUPPLIER_ID));
                });

            }
            else if (ah.CurrentMode == ah.config.mode.display || ah.CurrentMode == ah.config.mode.edit) {

                if (jsonData.STAFFLIST) {
                    var empPrimarycurr = $(ah.config.id.empPrimary).val();
                    var empSecondarycurr = $(ah.config.id.empSecondary).val();
                    document.getElementById("PRIMARY_EMPLOYEE").options.length = 0;

                    $.each(jsonData.STAFFLIST, function (item) {

                        $(ah.config.id.empPrimary)
                            .append($("<option></option>")
                                .attr("value", jsonData.STAFFLIST[item].STAFF_ID)
                                .text(jsonData.STAFFLIST[item].FIRST_NAME + ' ' + jsonData.STAFFLIST[item].LAST_NAME));
                    });
                    document.getElementById("SECONDARY_EMPLOYEE").options.length = 0;
                    $.each(jsonData.STAFFLIST, function (item) {

                        $(ah.config.id.empSecondary)
                            .append($("<option></option>")
                                .attr("value", jsonData.STAFFLIST[item].STAFF_ID)
                                .text(jsonData.STAFFLIST[item].FIRST_NAME + ' ' + jsonData.STAFFLIST[item].LAST_NAME));
                    });

                    $(ah.config.id.empPrimary).val(empPrimarycurr);
                    $(ah.config.id.empSecondary).val(empSecondarycurr);
                } else if (jsonData.CLONE) {
                    self.regenerateClone(jsonData);
                } else if (jsonData.PARTS_GROUPBYASSETYR_V) {
                    partsDPUsedList = jsonData.PARTS_GROUPBYASSETYR_V;
                    self.calculateDepreciation();
                } else if (jsonData.AM_ASSET_DETAILSCLONE) {
                    self.searchResultCloneList(jsonData);
                } else if (jsonData.LOV_LOOKUPS) {

                    sessionStorage.setItem(ah.config.skey.lovList, JSON.stringify(jsonData.LOV_LOOKUPS));

                    self.searchItemsLookUpResult("", 0);

                } else if (jsonData.AM_SUPPLIER) {
                    sessionStorage.setItem(ah.config.skey.assetSupplierList, JSON.stringify(jsonData.AM_SUPPLIER));
                    $("#lookUploadNumber").text("1");
                    $("#lookUppageCount").val("1");
                    $("#lookUppageNum").val(0);
                    self.searchItemsLookUpResult("", 0);

                } else if (jsonData.current_Agent) {
                    sessionStorage.setItem(ah.config.skey.assetSupplierList, JSON.stringify(jsonData.current_Agent));
                    $("#lookUploadNumber").text("1");
                    $("#lookUppageCount").val("1");
                    $("#lookUppageNum").val(0);
                    self.searchItemsLookUpResult("", 0);
                } else if (jsonData.AM_MANUFACTURER) {
                    sessionStorage.setItem(ah.config.skey.assetManufacturerList, JSON.stringify(jsonData.AM_MANUFACTURER));
                    //$("#lookUploadNumber").text("1");
                    //$("#lookUppageCount").val("1");
                    //$("#lookUppageNum").val(0);
                    self.searchItemsLookUpResult("", 0);
                } else if (jsonData.AM_MANUFACTURER_MODEL) {
                    sessionStorage.setItem(ah.config.skey.assetManufacturerModelList, JSON.stringify(jsonData.AM_MANUFACTURER_MODEL));
                    $("#lookUploadNumber").text("1");
                    $("#lookUppageCount").val("1");
                    $("#lookUppageNum").val(0);
                    self.searchItemsLookUpResult("", 0);

                } else if (jsonData.AM_ASSET_NOTES) {
                    self.searchResultNotes(jsonData);
                } else if (jsonData.AM_WORK_ORDER_REFNOTES) {
                    self.searchResultWONotes(jsonData);

                } else {
                    self.searchResultConstSummary(jsonData);
                }

            }
            else if (ah.CurrentMode == ah.config.mode.search || ah.CurrentMode == ah.config.mode.add) {
                if (jsonData.CLONE) {
                    self.regenerateClone(jsonData);
                } else if (jsonData.LOV_LOOKUPS) {
                    sessionStorage.setItem(ah.config.skey.lovList, JSON.stringify(jsonData.LOV_LOOKUPS));
                    $("#lookUploadNumber").text("1");
                    $("#lookUppageCount").val("1");
                    $("#lookUppageNum").val(0);
                    self.searchItemsLookUpResult("", 0);
                    //elf.searchLookupResult(jsonData);

                } else if (jsonData.AM_SUPPLIER) {
                    sessionStorage.setItem(ah.config.skey.assetSupplierList, JSON.stringify(jsonData.AM_SUPPLIER));
                    $("#lookUploadNumber").text("1");
                    $("#lookUppageCount").val("1");
                    $("#lookUppageNum").val(0);
                    self.searchItemsLookUpResult("", 0);

                } else if (jsonData.AM_MANUFACTURER) {
                    sessionStorage.setItem(ah.config.skey.assetManufacturerList, JSON.stringify(jsonData.AM_MANUFACTURER));
                    $("#lookUploadNumber").text("1");
                    $("#lookUppageCount").val("1");
                    $("#lookUppageNum").val(0);
                    self.searchItemsLookUpResult("", 0);
                } else if (jsonData.AM_MANUFACTURER_MODEL) {
                    sessionStorage.setItem(ah.config.skey.assetManufacturerModelList, JSON.stringify(jsonData.AM_MANUFACTURER_MODEL));
                    $("#lookUploadNumber").text("1");
                    $("#lookUppageCount").val("1");
                    $("#lookUppageNum").val(0);
                    self.searchItemsLookUpResult("", 0);
                } else if (jsonData.AM_ASSET_DETAILS) {
                    $("#PaginGloadNumber").text("1");
                    $("#PaginGpageCount").val("1");
                    $("#PaginGpageNum").val(0);
                    $("#LASTLOADPAGE").val(0);
                    srDataArray = jsonData.AM_ASSET_DETAILS;
                    var sortby = $("#SORTPAGE").val();
                    self.searchResult(0, sortby);

                } else {

                    self.searchResultSummaryList(jsonData);
                }

            }

            else if (ah.CurrentMode == ah.config.mode.idle) {
                var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
                var gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "16" });
                if (gPrivileges.length <= 0) {
                    $(ah.config.cls.liAddNew).hide();
                } else {
                    $(ah.config.cls.liAddNew).show();
                }

                sessionStorage.setItem(ah.config.skey.lovList, JSON.stringify(jsonData.LOV_LOOKUPS));
                var serviceDeptLOV = jsonData.LOV_LOOKUPS;
                serviceDeptLOV = serviceDeptLOV.filter(function (item) { return item.CATEGORY === 'AIMS_SERVICEDEPARTMENT' });

                $.each(serviceDeptLOV, function (item) {

                    $(ah.config.id.serviceDept)
                        .append($("<option></option>")
                            .attr("value", serviceDeptLOV[item].LOV_LOOKUP_ID)
                            .text(serviceDeptLOV[item].DESCRIPTION));
                });

                var specialtyLOV = jsonData.LOV_LOOKUPS;
                specialtyLOV = specialtyLOV.filter(function (item) { return item.CATEGORY === 'AIMS_SPECIALTY' });

                $.each(specialtyLOV, function (item) {

                    $(ah.config.id.assetSpecialty)
                        .append($("<option></option>")
                            .attr("value", specialtyLOV[item].LOV_LOOKUP_ID)
                            .text(specialtyLOV[item].DESCRIPTION));
                });
                var specialtyLOV = jsonData.LOV_LOOKUPS;
                specialtyLOV = specialtyLOV.filter(function (item) { return item.CATEGORY === 'AIMS_FACILITY' });

                $.each(specialtyLOV, function (item) {

                    $(ah.config.id.assetFacility)
                        .append($("<option></option>")
                            .attr("value", specialtyLOV[item].LOV_LOOKUP_ID)
                            .text(specialtyLOV[item].DESCRIPTION));
                });

                var assetClassLOV = jsonData.LOV_LOOKUPS;
                assetClassLOV = assetClassLOV.filter(function (item) { return item.CATEGORY === 'AIMS_CLASS' });

                $.each(assetClassLOV, function (item) {

                    $(ah.config.id.assetClass)
                        .append($("<option></option>")
                            .attr("value", assetClassLOV[item].LOV_LOOKUP_ID)
                            .text(assetClassLOV[item].DESCRIPTION));
                });

                var assetClassTypeLOV = jsonData.LOV_LOOKUPS;
                assetClassTypeLOV = assetClassTypeLOV.filter(function (item) { return item.CATEGORY === 'AIMS_CLASSTYPE' });

                $.each(assetClassTypeLOV, function (item) {

                    $(ah.config.id.assetClassType)
                        .append($("<option></option>")
                            .attr("value", assetClassTypeLOV[item].LOV_LOOKUP_ID)
                            .text(assetClassTypeLOV[item].DESCRIPTION));
                });

                var assetConditionLOV = jsonData.LOV_LOOKUPS;
                assetConditionLOV = assetConditionLOV.filter(function (item) { return item.CATEGORY === 'AIMS-CONDITION' });

                $.each(assetConditionLOV, function (item) {

                    $(ah.config.id.assetCondition)
                        .append($("<option></option>")
                            .attr("value", assetConditionLOV[item].LOV_LOOKUP_ID)
                            .text(assetConditionLOV[item].DESCRIPTION));


                });

                //Cost Center Condition
                $.each(assetConditionLOV, function (item) {

                    $(ah.config.id.assetccCondition)
                        .append($("<option></option>")
                            .attr("value", assetConditionLOV[item].LOV_LOOKUP_ID)
                            .text(assetConditionLOV[item].DESCRIPTION));


                });




                $.each(assetConditionLOV, function (item) {

                    $(ah.config.id.filterCondition)
                        .append($("<option></option>")
                            .attr("value", assetConditionLOV[item].LOV_LOOKUP_ID)
                            .text(assetConditionLOV[item].DESCRIPTION));


                });
                var assetLocationLOV = jsonData.LOV_LOOKUPS;
                assetLocationLOV = assetLocationLOV.filter(function (item) { return item.CATEGORY === 'AIMS_LOCATION' });

                $.each(assetLocationLOV, function (item) {

                    $(ah.config.id.assetLocation)
                        .append($("<option></option>")
                            .attr("value", assetLocationLOV[item].LOV_LOOKUP_ID)
                            .text(assetLocationLOV[item].DESCRIPTION));
                });

                var assetLocationLOV = jsonData.LOV_LOOKUPS;
                assetLocationLOV = assetLocationLOV.filter(function (item) { return item.CATEGORY === 'AM_WARRANTY' });


                for (var o in assetLocationLOV) {

                    self.warrantyLOV.push({
                        DESCRIPTION: assetLocationLOV[o].DESCRIPTION,
                        LOV_LOOKUP_ID: assetLocationLOV[o].LOV_LOOKUP_ID,

                    });


                }

                //cost center warranty
                for (var o in assetLocationLOV) {

                    self.ccwarrantyLOV.push({
                        DESCRIPTION: assetLocationLOV[o].DESCRIPTION,
                        LOV_LOOKUP_ID: assetLocationLOV[o].LOV_LOOKUP_ID,

                    });


                }

                var riskFactorLookupList = jsonData.RISK_CATEGORYFACTOR;

                var riskFactorLookup = riskFactorLookupList.filter(function (item) { return item.RISK_CATEGORYCODE === 'CLA' });
                $.each(riskFactorLookup, function (item) {
                    if (riskFactorLookup[item].RISK_DESCRIPTION.length > 30)
                        riskFactorLookup[item].RISK_DESCRIPTION = assetSupplierLOV[item].RISK_DESCRIPTION.substring(0, 29) + "...";

                    $(ah.config.id.inforiskCLA)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_DESCRIPTION));

                    $(ah.config.id.inforiskCLAScore)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_SCORE));
                });



                riskFactorLookup = riskFactorLookupList.filter(function (item) { return item.RISK_CATEGORYCODE === 'CLS' });
                $.each(riskFactorLookup, function (item) {
                    if (riskFactorLookup[item].RISK_DESCRIPTION.length > 30)
                        riskFactorLookup[item].RISK_DESCRIPTION = assetSupplierLOV[item].RISK_DESCRIPTION.substring(0, 29) + "...";

                    $(ah.config.id.inforiskCLS)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_DESCRIPTION));
                    $(ah.config.id.inforiskCLSScore)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_SCORE));
                });

                riskFactorLookup = riskFactorLookupList.filter(function (item) { return item.RISK_CATEGORYCODE === 'CMT' });
                $.each(riskFactorLookup, function (item) {
                    if (riskFactorLookup[item].RISK_DESCRIPTION.length > 30)
                        riskFactorLookup[item].RISK_DESCRIPTION = assetSupplierLOV[item].RISK_DESCRIPTION.substring(0, 29) + "...";

                    $(ah.config.id.inforiskCMT)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_DESCRIPTION));
                    $(ah.config.id.inforiskCMTScore)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_SCORE));
                });

                riskFactorLookup = riskFactorLookupList.filter(function (item) { return item.RISK_CATEGORYCODE === 'EQF' });
                $.each(riskFactorLookup, function (item) {
                    if (riskFactorLookup[item].RISK_DESCRIPTION.length > 30)
                        riskFactorLookup[item].RISK_DESCRIPTION = assetSupplierLOV[item].RISK_DESCRIPTION.substring(0, 29) + "...";

                    $(ah.config.id.inforiskEQF)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_DESCRIPTION));
                    $(ah.config.id.inforiskEQFScore)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_SCORE));
                });
                riskFactorLookup = riskFactorLookupList.filter(function (item) { return item.RISK_CATEGORYCODE === 'ESE' });
                $.each(riskFactorLookup, function (item) {
                    if (riskFactorLookup[item].RISK_DESCRIPTION.length > 30)
                        riskFactorLookup[item].RISK_DESCRIPTION = assetSupplierLOV[item].RISK_DESCRIPTION.substring(0, 29) + "...";

                    $(ah.config.id.inforiskESE)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_DESCRIPTION));
                    $(ah.config.id.inforiskESEScore)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_SCORE));
                });
                riskFactorLookup = riskFactorLookupList.filter(function (item) { return item.RISK_CATEGORYCODE === 'EVS' });
                $.each(riskFactorLookup, function (item) {
                    if (riskFactorLookup[item].RISK_DESCRIPTION.length > 30)
                        riskFactorLookup[item].RISK_DESCRIPTION = assetSupplierLOV[item].RISK_DESCRIPTION.substring(0, 29) + "...";

                    $(ah.config.id.inforiskEVS)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_DESCRIPTION));
                    $(ah.config.id.inforiskEVSScore)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_SCORE));
                });
                riskFactorLookup = riskFactorLookupList.filter(function (item) { return item.RISK_CATEGORYCODE === 'EVT' });
                $.each(riskFactorLookup, function (item) {
                    if (riskFactorLookup[item].RISK_DESCRIPTION.length > 30)
                        riskFactorLookup[item].RISK_DESCRIPTION = assetSupplierLOV[item].RISK_DESCRIPTION.substring(0, 29) + "...";

                    $(ah.config.id.inforiskEVT)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_DESCRIPTION));
                    $(ah.config.id.inforiskEVTScore)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_SCORE));
                });
                riskFactorLookup = riskFactorLookupList.filter(function (item) { return item.RISK_CATEGORYCODE === 'FPF' });
                $.each(riskFactorLookup, function (item) {
                    if (riskFactorLookup[item].RISK_DESCRIPTION.length > 30)
                        riskFactorLookup[item].RISK_DESCRIPTION = assetSupplierLOV[item].RISK_DESCRIPTION.substring(0, 29) + "...";

                    $(ah.config.id.inforiskFPF)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_DESCRIPTION));
                    $(ah.config.id.inforiskFPFScore)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_SCORE));
                });
                riskFactorLookup = riskFactorLookupList.filter(function (item) { return item.RISK_CATEGORYCODE === 'LIS' });
                $.each(riskFactorLookup, function (item) {
                    if (riskFactorLookup[item].RISK_DESCRIPTION.length > 30)
                        riskFactorLookup[item].RISK_DESCRIPTION = assetSupplierLOV[item].RISK_DESCRIPTION.substring(0, 29) + "...";

                    $(ah.config.id.inforiskLIS)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_DESCRIPTION));
                    $(ah.config.id.inforiskLISScore)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_SCORE));
                });
                riskFactorLookup = riskFactorLookupList.filter(function (item) { return item.RISK_CATEGORYCODE === 'MAR' });
                $.each(riskFactorLookup, function (item) {
                    if (riskFactorLookup[item].RISK_DESCRIPTION.length > 30)
                        riskFactorLookup[item].RISK_DESCRIPTION = assetSupplierLOV[item].RISK_DESCRIPTION.substring(0, 29) + "...";

                    $(ah.config.id.inforiskMAR)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_DESCRIPTION));
                    $(ah.config.id.inforiskMARScore)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_SCORE));
                });
                riskFactorLookup = riskFactorLookupList.filter(function (item) { return item.RISK_CATEGORYCODE === 'PCA' });
                $.each(riskFactorLookup, function (item) {
                    if (riskFactorLookup[item].RISK_DESCRIPTION.length > 30)
                        riskFactorLookup[item].RISK_DESCRIPTION = assetSupplierLOV[item].RISK_DESCRIPTION.substring(0, 29) + "...";

                    $(ah.config.id.inforiskPCA)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_DESCRIPTION));
                    $(ah.config.id.inforiskPCAScore)
                        .append($("<option></option>")
                            .attr("value", riskFactorLookup[item].RISK_FACTORCODE)
                            .text(riskFactorLookup[item].RISK_SCORE));
                });
                assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
                if (assetNoID !== null && assetNoID !== "") {

                    self.readSetupID("ASSETULR");
                }


                var retired = ah.getParameterValueByName(ah.config.fld.fromDashBoard);

                if (retired == "RETIRED") {
                    self.searchFilter.status = "R";
                    $("#filterStatus").val("R");
                    urlText = 'ljllk';
                    history.pushState('', 'New Page Title', ah.config.url.aimsDetails);
                    self.searchNow();                   
                    return;

                }
            }

        }
        else {

            alert(systemText.errorTwo);
            //window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};