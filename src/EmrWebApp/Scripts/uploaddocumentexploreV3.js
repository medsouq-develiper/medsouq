﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liNew: '.liNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            documentList: '.documentList',
            statusText: '.status-text',
            docNoItem: '.docNoItem',
            docItemFile: '.docItemFile',
            imageItem: '.imageItem',
            transactionList: '.transaction-list',
            alertsList: '.alerts-list',
            fieldList: '.field-list',
            itemnameText: '.itemnameText',
            lovlistText: '.LovListText',
            patDob: '.PATDOB',
            fieldVisitlist: '.field-visitlist',
            tabHeader: '.tabHeader',
            liDelete: '.liDelete',
            defaultPicture: '.defaultPicture',
            iFrame: '.iFrame',
            liDeleteFile: '.liDeleteFile'

        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul',
            table: 'table'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',

            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            txtDateFrom: '#txtDateFrom',
            eventID: '#EVENT_ID',
            successMessage: '#successMessage',
            searchDocumentList: '#searchDocumentList',
            searchItems2: '#searchItems2',
            searchResultList: '#searchResultList',
            itemlistId: '#itemlistId',
            itemName: '#itemName',
            patientID: '#PATIENT_ID',
            frmCategory: '#frmCategory',
            addButtonId: '#addButtonId',
            modifyButtonId: '#modifyButtonId',
            docLibraryCategory: '#DOCCATEGORY',
            filterDeviceTypeList: '#filterDeviceTypeList',
            deviceType: '#DEVICETYPECODE',
            manufacturerCode: '#MANUFACTURERCODE',
            modelCode: '#MODELCODE',
            searchkey: '#SEARCHKEY'


        },
        tagId: {

            oeInstruction: 'INSTRUCTION'

        },
        cssCls: {
            viewMode: 'view-mode',
            searchRowDivider: 'search-row-divider',
            alignLeft: 'align-left',
            alignRight: 'align-right'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            dataDocNoID: 'data-docNoID',
            dataitemInfo: 'data-itemInfo',
            datalistitemInfo: 'data-listitemInfo',
            senderID: 'id'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            groupPrivileges: 'GROUP_PRIVILEGES',
            searchResult: 'searchResult',
            scanDocs: 'SCANDOCS',
            categoryDetails: 'SCANDOCS_CATEGORY'

        },
        fld: {
            fldzdexdsesxxlz: 'zdexdsesxxlz',
            fldAssetID: 'ASSETNO',
            assetDescInfo: 'ASSETDESC',
            fldEventID: 'eventID',
            fldsearchPatient: 'searchPatient',
            reqWONo: 'REQNO'
        },
        url: {
            homeIndex: '/Home/Index',
            orderEntry: '/orders/OrderEntry?',
            aimsInformation: '/aims/assetInfo',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalUpload: '#openModalUpload',
            closeModal: '#'
        },
        api: {

            searchInit: '/DocumentLibrary/SearchDocumentLibraryInit',
            createDocument: '/DocumentLibrary/UploadDocument',
            searchInfo: '/DocumentLibrary/SearchDocumentLibraryByDocNo',
            getDocumentNo: '/DocumentLibrary/AssignDocumentNo',
            deleteFile: '/DocumentLibrary/DeleteFile',
            getClientRules: '/ClientRules/GetClientRules',
            searchLovInitialize: '/AIMS/SearchAimsLOVInitialize',
            search: '/DocumentLibrary/SearchDocumentLibrary',
            getDocumentLov: '/DocumentLibrary/GetDocumentLOV',
            getFilesByCriteria: '/DocumentLibrary/SearchDocumentLibraryCriteria'

        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal docNoItem' data-docNoID='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            searchRowTemplate: '<span class="search-row-label fc-light-grey-2">{0}: <span class="search-row-data">{1}</span></span>',
            searchRowTableTemplate: '<span class="search-row-data">{0}</span>',
            searchListRowHeaderTemplate: "<a href='#' class='search-row-data imageItem fc-slate-blue' data-listitemInfo='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            searchRowHeaderUploadTemplate: "<p class='fs-medium-normal docItemFile' data-docNoID='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</p>",
        }
    };

    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();


        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.notifySuccess + ',' + thisApp.config.id.frmCategory + ',' + thisApp.config.cls.liDelete + ',' + thisApp.config.cls.documentList + ',' + thisApp.config.cls.iFrame + ',' + thisApp.config.cls.liNew + ',' + thisApp.config.cls.liDeleteFile).hide(); 

        $(thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome + ',' + thisApp.config.cls.defaultPicture).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";
        $('#iFrame').css('height', (window.innerHeight - 275) + 'px');

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.iFrame).hide();
        $(thisApp.config.id.itemlistId + ',' + thisApp.config.id.searchItems1 + ',' + thisApp.config.cls.defaultPicture).show();



    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liDelete + ',' + thisApp.config.cls.documentList).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.fieldList).show();
        $(thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.liDelete).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };
    thisApp.toggleSaveImage = function () {
        $(thisApp.config.cls.liSave).show();
    };
    thisApp.togglenewDoc = function () {
        
    };
    thisApp.toggleConsultRedMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;


        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.fieldList + ',' + thisApp.config.id.rptDetail + ',' + thisApp.config.cls.fieldVisitlist).show();
        $(thisApp.config.id.searchOrders).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();
       
        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divServiceID + ',' + thisApp.config.cls.documentList + ',' + thisApp.config.cls.liSave
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };


    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.documentList).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liDelete).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.id.frmCategory
        ).hide();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        //$(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        //$(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divPatientAssessmentID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.fieldList
        ).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);
        self.initialize();

    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.formatJSONDateTimeToString = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 16) return "-";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0] + ' ' + strTime[1].substring(0, 5);


        return dateToFormat;
    };

    thisApp.formatJSONDateToString = function () {

        var dateToFormat = arguments[0];
        var strDate;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 10) return "-";
        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');

            if (isDateTime) {
                val = thisApp.formatJSONDateTimeToString(val);
            }

            if (isDate) {
                val = thisApp.formatJSONDateToString(val);
            }

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var imageDocumentExploreViewModel = function (systemText) {

    var self = this;
    var uploadedFiles = [];
    var srDataArray = ko.observableArray([]);
    var x = 0;
    var ah = new appHelper(systemText);
    var modal = document.getElementById('openModalAlert');
    var clientRules = {};
    var imageExtensions = new RegExp("(.*?)\.(BMP|bmp|gif|jpg|png|pdf|txt)$");
    var tifExtensions = new RegExp("(.*?)\.(TIF|TIFF|tif|tiff)$");
    var officeExtensions = new RegExp("(.*?)\.(doc|docx|dot|csv|xls|xlsx|xlw|pps|ppsx|ppt|pptx)$");
    var unsupportedExtension = new RegExp("(.*?)\.(dwg|DWG)$");
    var pathFile = null;
    var documentCategory = null;
    var searchKey = null;
    var FileAssigned = null;
    self.srManufacturerDataArray = ko.observableArray([]);
    self.ToUploadFilesArray = ko.observableArray([]);
    self.srModelDataArray = ko.observableArray([]);
    self.FilesExist = ko.observableArray([]);
   

    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));


        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());
        window.sessionStorage.removeItem("SCANDOCS");
        ah.toggleReadMode();
        ah.initializeLayout();

        $("#DOCCATEGORY").empty();
        $(ah.config.id.docLibraryCategory)
            .append($("<option></option>")
                .attr("value", "")
                .text("-Select-"));

       
        self.searchNowInit();
    };

    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };


    self.signOut = function () {
        window.location.href = ah.config.url.homeIndex;
    };

    self.searchFilter = {
        status: ko.observable('I'),
        staffId: ko.observable('')
      
    };

    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            self.FileDetail.RecordNo(null);
            self.FileDetail.DocCategory(null);
            self.FileDetail.SearchKey(null);
            self.FileDetail.DeviceCode(null);
            self.FileDetail.ManufacturerCode(null);
            self.FileDetail.ModelCode(null);
            self.ToUploadFilesArray.removeAll();
            self.FilesExist.removeAll();
            ah.initializeLayout();
        }

    };
   
    self.newUpload = function () {
      
        ah.toggleAddMode();
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        pathFile = null;
        documentCategory = null;
        searchKey = null;


        self.FileDetail.RecordNo(null);
        self.FileDetail.DocCategory(null);
        self.FileDetail.SearchKey(null);
        self.FileDetail.DeviceCode(null);
        self.FileDetail.ManufacturerCode(null);
        self.FileDetail.ModelCode(null);

        self.ToUploadFilesArray.removeAll();
        self.FilesExist.removeAll();
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";
        window.sessionStorage.removeItem("SCANDOCS");
        document.getElementById("uploadedInfo").value = "";
        $("#SAVEACTION").val('PDF');
    
        $("#uploadTitle").text('Upload File');
        $("#uploadedInfo").show();
        $(ah.config.id.frmCategory).show();
        $(ah.config.cls.liDelete).hide();
    

        postData = { STAFF_LOGON_SESSIONS: staffLogonSessions};
        $.post(self.getApi() + ah.config.api.getDocumentLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

        //
    };
   
    self.FileDetail = {
        RecordNo: ko.observable(null),
        DocCategory: ko.observable(null),
        SearchKey: ko.observable(null),
        DeviceCode: ko.observable(null),
        ManufacturerCode: ko.observable(null),
        ModelCode: ko.observable(null),
    };

    $('#uploadedInfo').change(function (e) {
        var files = e.target.files;
        if (files.length != 0) {
            if (imageExtensions.test(files[0].name) || tifExtensions.test(files[0].name) || officeExtensions.test(files[0].name) || unsupportedExtension.test(files[0].name)) {
                var myFile = document.getElementById('uploadedInfo').value;
                var myFileData = document.getElementById('uploadedInfo');
                var fName = null;
                var fileType = null;
                //alert(myFile.length);
                if (myFile.length > 0) {
                    var fName = myFileData.files[0].name;
                    fileType = fName.substring(fName.lastIndexOf('.') + 1, fName.length);
                    var fileBytes = myFileData.files[0].size;
                    var decimals = 2;
                    var dm = decimals < 0 ? 0 : decimals;
                    var fileSizeget = parseInt(Math.floor(Math.log(fileBytes) / Math.log(1024)));
                    var fileSize = parseFloat((fileBytes / Math.pow(1024, fileSizeget)).toFixed(dm))

                 
                   
                    $("#SAVEACTION").val(fileType.toUpperCase());
                } else {
                    return;
                }
                fName = '_' + fName;
                if (srDataArray.length > 0) {
                    var tempSrDataArray = srDataArray.filter(function (item) { return item.FILENAME === fName });
                    if (tempSrDataArray.length > 0) {
                        alert('The Document Name is already Exist, please rename or select other file to upload.');
                        document.getElementById("uploadedInfo").value = "";
                        return;
                    }
                }

                ah.toggleSaveImage();
                self.eloaddata(fName, fileType, "", 0, files, fileSize);
            } else {
                alert('File not supported.');
                $(this).val('');
            }
        }
    });

    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.resizeImage = function (file, arrayFileName) {


        var resize = new window.resize();
        resize.init();


        var reader = new FileReader();
        reader.onload = function (readerEvent) {

            self.resizeCanvas(readerEvent.target.result, 1200, 'file', arrayFileName, file);

        }
        reader.readAsDataURL(file);


    };
    self.resizeCanvas = function (dataURL, maxSize, outputType, arrayFileName, file) {

        var _this = this;
        var x = 0;
        var image = new Image();
        image.onload = function (imageEvent) {

            // Resize image
            var canvas = document.createElement('canvas'),
                width = image.width,
                height = image.height;
            if (width > height) {
                if (width > maxSize) {
                    height *= maxSize / width;
                    width = maxSize;
                }
            } else {
                if (height > maxSize) {
                    width *= maxSize / height;
                    height = maxSize;
                }
            }
            canvas.width = width;
            canvas.height = height;
            canvas.getContext('2d').drawImage(image, 0, 0, width, height);

            var newdataURL = canvas.toDataURL('image/jpeg', 0.8)
            self.resizeOutcome(newdataURL, 600, 'dataURL', arrayFileName, file);

            // _this.output(canvas, outputType, callback);

        }

        image.src = dataURL;
    };

    self.resizeOutcome = function (dataURL, maxSize, outputType, arrayFileName, file) {

        var _this = this;

        var image = new Image();
        image.onload = function (imageEvent) {

            // Resize image
            var canvas = document.createElement('canvas'),
                width = image.width,
                height = image.height;
            if (width > height) {
                if (width > maxSize) {
                    height *= maxSize / width;
                    width = maxSize;
                }
            } else {
                if (height > maxSize) {
                    width *= maxSize / height;
                    height = maxSize;
                }
            }
            canvas.width = width;
            canvas.height = height;
            canvas.getContext('2d').drawImage(image, 0, 0, width, height);
            var newdataURL = canvas.toDataURL('image/jpeg', 0.8)

            self.eloaddata("", "", newdataURL, arrayFileName, file);
            // _this.output(canvas, outputType, callback);

        }

        image.src = dataURL;
    };
   

    self.DeviceChanged = function (obj, event) {

        self.FileDetail.RecordNo(null);
        self.FileDetail.DocCategory(null);
        self.FileDetail.SearchKey(null);
        self.FileDetail.DeviceCode(null);
        self.FileDetail.ManufacturerCode(null);
        self.FileDetail.ModelCode(null);
        self.FilesExist.removeAll();
        $(ah.config.id.searchkey).prop(ah.config.attr.readOnly, false);
        var deviceTypeVal = $("#DEVICETYPECODE").val();

        $("#MANUFACTURERCODE").empty();
        
        $(ah.config.id.manufacturerCode)
            .append($("<option></option>")
                .attr("value","")
                .text("-Select-"));
        $("#MODELCODE").empty();
        $(ah.config.id.modelCode)
            .append($("<option></option>")
                .attr("value", "")
                .text("-Select-"));

        var manufacturerLOV = self.srManufacturerDataArray;
       
        manufacturerLOV = manufacturerLOV.filter(function (item) { return item.CATEGORY == deviceTypeVal });

        $.each(manufacturerLOV, function (item) {

            $(ah.config.id.manufacturerCode)
                .append($("<option></option>")
                    .attr("value", manufacturerLOV[item].MANUFACTURER)
                    .text(manufacturerLOV[item].MANUFACTURERNAME));
        });
    };
    self.ManufacturerChanged = function (obj, event) {
        self.FileDetail.RecordNo(null);
        self.FileDetail.DocCategory(null);
        self.FileDetail.SearchKey(null);
        self.FileDetail.DeviceCode(null);
        self.FileDetail.ManufacturerCode(null);
        self.FileDetail.ModelCode(null);
        self.FilesExist.removeAll();
        $(ah.config.id.searchkey).prop(ah.config.attr.readOnly, false);
        $("#MODELCODE").empty();
        $(ah.config.id.modelCode)
            .append($("<option></option>")
                .attr("value", "")
                .text("-Select-"));

        var manufacturerVal = $("#MANUFACTURERCODE").val();
        var deviceTypeVal = $("#DEVICETYPECODE").val();
        var modelLOV = self.srModelDataArray;
        modelLOV = modelLOV.filter(function (item) { return item.MANUFACTURER == manufacturerVal && item.CATEGORY == deviceTypeVal });

        $.each(modelLOV, function (item) {

            $(ah.config.id.modelCode)
                .append($("<option></option>")
                    .attr("value", modelLOV[item].MODEL_NO)
                    .text(modelLOV[item].MODEL_NO));
        });
    }

    self.eloaddata = function (filename, filetype, data, filenameArray, file, fileSize) {
        var file_name = file.name;

        var file_type = 'JPG';
       
        file_name = filename;
        file_type = filetype;

        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());

            return hours + ":" + minutes;
        }

        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);
        var strDate = hourmin + ' ' + strDateStart;
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        var appendDatafilter = self.ToUploadFilesArray();
        if (appendDatafilter.length > 0) {
            var existFile = appendDatafilter.filter(function (item) { return item.FILENAME == file_name });
            if (existFile.length > 0) {
                alert('The Document Name is already Exist, please rename or select other file to upload.');
                document.getElementById("uploadedInfo").value = "";
                return;
            }
        }
        uploadedFiles.push($('#uploadedInfo')[0].files[0]);

        self.ToUploadFilesArray.push({
            FILENAME: file_name,
            FILESIZE: fileSize,
            FILETYPE: file_type,
            DOCCATEGORY: 'ASSETDOC',
            CREATED_BY: staffLogonSessions.STAFF_ID,
            CREATED_DATE: strDate
        });
    };
   
  
    self.showDocumentList = function (jsonData) {
        var defaultpic = "/images/counter2.gif";
        $('#iFrame').attr('src', defaultpic);
        $('#iFrameNewTab').attr('href', defaultpic).html('Full View');
        $('#iFrameName').html("");
        $(ah.config.cls.documentList + ',' + ah.config.cls.defaultPicture + ',' + ah.config.cls.iFrame).hide(); 

        var htmlStr;

        //  ah.ResetControls();
        var rd = jsonData.DOCUMENT_LIST;

        if (rd.length == 1) {
            FileAssigned = rd[0];
            self.readSingleDocument(rd[0].PATHFILE, rd[0].FILENAME, rd[0].DOCID);
            return;
        }
        $(ah.config.cls.defaultPicture).show();
        $(ah.config.cls.documentList).show();
        $(ah.config.id.searchDocumentList + ' ' + ah.config.tag.ul).html('');
        var htmlstr = "";

        for (var o in rd) {
            htmlstr += '<li>';
            htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
            htmlstr += ah.formatString(ah.config.html.searchListRowHeaderTemplate, JSON.stringify(rd[o]), '  ' + rd[o].FILENAME.substring(0, 30), "", "");
            htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.fileSize, rd[o].FILESIZE+ " KB");
            htmlstr += '</span>';

            htmlstr += '</li>';

        }

        $(ah.config.id.searchDocumentList + ' ' + ah.config.tag.ul).append(htmlstr);

        $(ah.config.cls.imageItem).bind('click', self.readDocument);
        //ah.displaySearchResult();
        //ah.toggleDisplayMode();
        //self.privilegesValidation('NEW');


    };

    self.saveAllChanges = function () {
        var assetNoInfo = ah.getParameterValueByName(ah.config.fld.fldAssetID);
        var frmData = new FormData();
        var filebase = $("#documentInfo").get(0);
        var files = filebase.files;
        frmData.append("docFilename", assetNoInfo);
        frmData.append(files[0].name, files[0]);
        $.ajax({
            url: '/Upload/SaveDocument',
            type: "POST",
            contentType: false,
            processData: false,
            data: frmData,
            success: function (data) {
                alert('Success Upload');
            },
            error: function (err) {
                alert(error);
            }
        });


        //End of the document ready function...

    };
  
    self.closeIframe = function () {
        $(ah.config.cls.defaultPicture).show();
        $(ah.config.cls.iFrame).hide();
    
        //var defaultpic = "/images/counter2.gif";
        //$('#iFrame').attr('src', defaultpic);
        //$('#iFrameNewTab').attr('href', defaultpic).html('Full View');
        //$('#iFrameName').html("");
        //$(ah.config.cls.iFrame).hide(); 
    };
    self.readSingleDocument = function (pathFile, FileName, docId) {
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";
        $(ah.config.cls.defaultPicture).hide(); 
        $(ah.config.cls.iFrame).show();
        $('#iFrame').show();
        var path = `/${pathFile}/${FileName}`;

        if (imageExtensions.test(FileName)) {
            $('#iFrame').attr('src', path);
            $('#iFrameNewTab').attr('href', path).html('Full View');
            $('#iFrameName').html(FileName);
        }
        else if (tifExtensions.test(FileName)) {
            var xhr = new XMLHttpRequest();
            xhr.responseType = 'arraybuffer';
            xhr.open('GET', path);
            xhr.onload = function (e) {
                var buffer = xhr.response;
                var tiff = new Tiff({ buffer: buffer });
                var data = tiff.toDataURL();
                $('#iFrame').attr('src', data);
                $('#iFrameNewTab').attr('href', path).html('Click here to download.');
                $('#iFrameName').html(FileName);
            };
            xhr.send();
        }
        else if (officeExtensions.test(FileName)) {

            var clientRule = clientRules;
            if (clientRule != null) {
                if (clientRule.STATUS == "A") {
                    $('#iFrame').attr('src', `${clientRule.RULE_VALUE}${FileName}`);
                    $('#iFrameNewTab').attr('href', `${clientRule.RULE_VALUE}${FileName}`);
                    $('#iFrameName').html(FileName);
                } else {
                    $('#iFrameNewTab').attr('href', path).html(`File not supported, click here to download.`);

                    $('#iFrameName').html(imageDetails.FILENAME);
                }
            } else {
                $('#iFrameNewTab').attr('href', path).html(`File not supported, click here to download.`);

                $('#iFrameName').html(FileName);
            }

        }
        else if (unsupportedExtension.test(FileName)) {
            $('#iFrameNewTab').attr('href', path).html(`File not supported, click here to download.`);
            $('#iFrameName').html(FileName);
            $('#iFrame').hide();
        }

    };
    self.readDocument = function () {
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";
        var imageDetails = JSON.parse(this.getAttribute(ah.config.attr.datalistitemInfo));
        FileAssigned = imageDetails;
        $(ah.config.cls.defaultPicture).hide(); 
        $(ah.config.cls.iFrame).show();
        $('#iFrame').show();
        var path = `/${imageDetails.PATHFILE}/${imageDetails.FILENAME}`;
   
        if (imageExtensions.test(imageDetails.FILENAME)) {
            $('#iFrame').attr('src', path);
            $('#iFrameNewTab').attr('href', path).html('Full View');
            $('#iFrameName').html(imageDetails.FILENAME);
        }
        else if (tifExtensions.test(imageDetails.FILENAME)) {
            var xhr = new XMLHttpRequest();
            xhr.responseType = 'arraybuffer';
            xhr.open('GET', path);
            xhr.onload = function (e) {
                var buffer = xhr.response;
                var tiff = new Tiff({ buffer: buffer });
                var data = tiff.toDataURL();
                $('#iFrame').attr('src', data);
                $('#iFrameNewTab').attr('href', path).html('Click here to download.');
                $('#iFrameName').html(imageDetails.FILENAME);
            };
            xhr.send();
        }
        else if (officeExtensions.test(imageDetails.FILENAME)) {

            var clientRule = clientRules;
            if (clientRule != null) {
                if (clientRule.STATUS == "A") {
                    $('#iFrame').attr('src', `${clientRule.RULE_VALUE}${imageDetails.FILENAME}`);
                    $('#iFrameNewTab').attr('href', `${clientRule.RULE_VALUE}${imageDetails.FILENAME}`);
                    $('#iFrameName').html(imageDetails.FILENAME);
                } else {
                    $('#iFrameNewTab').attr('href', path).html(`File not supported, click here to download.`);

                    $('#iFrameName').html(imageDetails.FILENAME);
                }
            } else {
                $('#iFrameNewTab').attr('href', path).html(`File not supported, click here to download.`);

                $('#iFrameName').html(imageDetails.FILENAME);
            }

        }
        else if (unsupportedExtension.test(imageDetails.FILENAME)) {
            $('#iFrameNewTab').attr('href', path).html(`File not supported, click here to download.`);
            $('#iFrameName').html(imageDetails.FILENAME);
            $('#iFrame').hide();
        }


        //clientRules.RULE_VALUE
        //clientRules.STATUS
    };

    
  
    self.searchResult = function (jsonData) {
        self.privilegesValidation("NEW");

        var sr = jsonData.DOCUMENTLIBRARY_LIST;

        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');


        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].DOCNO, "", sr[o].DEVICENAME, "   &nbsp;&nbsp;&nbsp;&nbsp;    Model: " + sr[o].MODELCODE);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.docNumber, sr[o].DOCNO);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.manufacturerDesc, sr[o].MANUFACTURERNAME);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.docCategory, sr[o].DOCCATEGORY);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.numberFile, sr[o].NOOFFILE);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.searchKey, sr[o].SEARCHKEY);
                

                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.docNoItem).bind('click', self.getDocument);
     //   ah.displaySearchResult();

    };

    self.getDocument = function () {
        var fldRefID = this.getAttribute(ah.config.attr.dataDocNoID).toString();
        ah.toggleSearchMode();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        postData = { DOCNO: fldRefID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchInfo, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    }

    self.alertMessage = function (refMessage) {
        $("#alertmessage").text(refMessage);
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "block";

    };

    self.closeAlertModal = function () {
        modal.style.display = "none"
    }
    self.cancelChangesModal = function () {
        window.location.href = ah.config.url.closeModal;
        uploadedFiles = [];
    }

    self.deleteFile = function () {
        if (FileAssigned != null) {
            self.alertMessage("Are you sure you want to permanently delete this file: " + FileAssigned.FILENAME+ "?");
        }

        
    };
    self.deleteUploadFile = function () {
        self.ToUploadFilesArray.remove(this);
    };

    deleteFileNow = function () {
        
        if (FileAssigned != null) {
            ah.toggleSaveMode()
            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            postData = { DOCID: FileAssigned.DOCID, STAFF_LOGON_SESSIONS: staffLogonSessions };

            $.post(self.getApi() + ah.config.api.deleteFile, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        } else {
            alert("No file to be delete. Please refesh your screen.");
        }
    };

    self.UploadFiles = function (REF_ID) {
        window.location.href = ah.config.url.closeModal;
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        var frmData = new FormData();
        var folderName = "~/" + pathFile;
        frmData.append("REF_ID", REF_ID);
        frmData.append("pathLocation", folderName);
        uploadedFiles.forEach((file, i) => {
            frmData.append(`file${i}`, file);
        });
        $.ajax({
            url: '/Upload/SaveDocumentUpload',
            type: "POST",
            contentType: false,
            processData: false,
            data: frmData,
            success: function (data) {
                if (data.success == false) {
                    alert(data.message);

                    return;
                }
                var DocsSave = self.ToUploadFilesArray();//JSON.parse(sessionStorage.getItem(ah.config.skey.scanDocs));

                var deviceType = $("#DEVICETYPECODE").val();
                var manufacturerCode = $("#MANUFACTURERCODE").val();
                var modelCode = $("#MODELCODE").val();


                postData = { MODELCODE: modelCode ,DEVICECODE: deviceType ,MANUFACTURERCODE: manufacturerCode, SEARCHKEY: searchKey, FOLDERFILE: pathFile ,DOCCATEGORY:documentCategory, SEQNO: REF_ID, DOCUMENTLIBRARY: DocsSave, STAFF_LOGON_SESSIONS: staffLogonSessions };

                $.when($.post(self.getApi() + ah.config.api.createDocument, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus)).done(function () {
                    uploadedFiles = [];
                });
            },
            error: function (err) {
                alert(error);
            }
        });
    };
    self.searchNowInit = function () {
        FileAssigned = null;
        var defaultpic = "/images/counter2.gif";
        $('#iFrame').attr('src', defaultpic);
        $('#iFrameNewTab').attr('href', defaultpic).html('Full View');
        $('#iFrameName').html("");
        $(ah.config.cls.documentList).hide();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleSearchMode();

        postData = {
            SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), SEARCH_FILTER: self.searchFilter, LOV: "'LIBRARYDOC_TYPE'", STAFF_LOGON_SESSIONS: staffLogonSessions
        };

        $.post(self.getApi() + ah.config.api.searchInit, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.searchNow = function () {
        FileAssigned = null;
        var defaultpic = "/images/counter2.gif";
        $('#iFrame').attr('src', defaultpic);
        $('#iFrameNewTab').attr('href', defaultpic).html('Full View');
        $('#iFrameName').html("");
        $(ah.config.cls.documentList).hide();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleSearchMode();
     
        postData = {
            SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), SEARCH_FILTER: self.searchFilter, STAFF_LOGON_SESSIONS: staffLogonSessions };

        $.post(self.getApi() + ah.config.api.search, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.searchCriteria = function() {
        var docCategory = $("#DOCCATEGORY").val();
        var deviceCode = $("#DEVICETYPECODE").val();
        var manufacturerCode = $("#MANUFACTURERCODE").val();
        var modelCode = $("#MODELCODE").val();
        self.FileDetail.RecordNo(null);
        self.FileDetail.DocCategory(null);
        self.FileDetail.SearchKey(null);
        self.FileDetail.DeviceCode(null);
        self.FileDetail.ManufacturerCode(null);
        self.FileDetail.ModelCode(null);
        self.FilesExist.removeAll();
        $(ah.config.id.searchkey).prop(ah.config.attr.readOnly, false);
        if (docCategory != null && modelCode != null && modelCode != "" && docCategory !="") {
            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var postData;

            postData = { DOCCATEGORY: docCategory , MODELCODE: modelCode ,MANUFACTURERCODE: manufacturerCode ,DEVICETYPECODE: deviceCode, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.getFilesByCriteria, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }



    };

    self.searchItems = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleSearchMode();

        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), CATEGORY: "", ACTION: "ORDERLIST", STAFF_LOGON_SESSIONS: staffLogonSessions };

        $.post(self.getApi() + ah.config.api.searchItems, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.privilegesValidation = function (actionVal) {

        var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
        if (actionVal === 'NEW') {
            gPrivileges = gPrivileges.filter(function (item) { return item.PRIVILEGES_ID === "45" });
            if (gPrivileges.length > 0) {
                $(ah.config.cls.liNew + ',' + ah.config.cls.liDeleteFile).show();
            }
        }
    }
    self.saveAllFiles = function () {
        var saveAction = $("#SAVEACTION").val();
        var documentDetails = ah.ConvertFormToJSON($(ah.config.id.frmCategory)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        
        pathFile = "DocumentLibrary/"+documentDetails.DOCCATEGORY.replace(/\s/g, '');
        documentCategory = documentDetails.DOCCATEGORY;
        searchKey = documentDetails.SEARCHKEY;
        var scanDocsSave = self.ToUploadFilesArray();
        if (scanDocsSave.length <= 0) {
            alert('Unable to proceed. There is no data to be save.')
            return;
        }
        ah.toggleSaveMode()


        if (self.FileDetail.RecordNo() != null) {
            self.UploadFiles(self.FileDetail.RecordNo());
        } else {
            postData = { STAFF_LOGON_SESSIONS: staffLogonSessions };

            $.post(self.getApi() + ah.config.api.getDocumentNo, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }

       

    };

    self.triggerSaveFiles = function () {
        $('#saveAllFiles').trigger('click');
    };

    self.webApiCallbackStatus = function (jsonData) {
        //console.log(jsonData);
        //alert(JSON.stringify(jsonData));

        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));

            // window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.SCANDOCS || jsonData.LOV_LOOKUPS || jsonData.SCANDOCS_CATEGORYIMAGELIST || jsonData.SUCCESS || jsonData.SUCCESSDELETED || jsonData.ClientRules || jsonData.SEQ_DOCUMENT || jsonData.DOCUMENTLIBRARY || jsonData.DOCUMENTLIBRARY_LIST || jsonData.DOCUMENT_LIST || jsonData.DEVICE_TYPE || jsonData.MANUFACTURER || jsonData.DOCUMENTFILE_LIST) {

            if (ah.CurrentMode == ah.config.mode.save) {
                if (jsonData.SEQ_DOCUMENT) {
                    self.UploadFiles(jsonData.SEQ_DOCUMENT);
                } else if (jsonData.SUCCESSDELETED) {

                    $(ah.config.id.successMessage).html(systemText.deleteSuccessMessage);

                    // ah.showSavingStatusSuccess();
                    $(ah.config.cls.notifySuccess).show();
                    $(ah.config.cls.divProgress + ',' + ah.config.cls.liApiStatus).hide();

                    setTimeout(function () {
                        $(ah.config.cls.notifySuccess).fadeOut(1000, function () { });
                        modal.style.display = "none"
                        self.searchNow();
                    }, 500);
                } else {
                    if (jsonData.SUCCESS) {
                       
                    } else {

                    }

                    $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);

                    // ah.showSavingStatusSuccess();
                    $(ah.config.cls.notifySuccess).show();
                    $(ah.config.cls.divProgress).hide();
              
                    setTimeout(function () {
                        $(ah.config.cls.notifySuccess).fadeOut(1000, function () { });
                        self.initialize();
                    }, 1500);

                }
            }
            else if (ah.CurrentMode == ah.config.mode.read) {


                

            }
            else if (ah.CurrentMode == ah.config.mode.add) {


                if (jsonData.DOCUMENTFILE_LIST) {
                   // $("#SEARCHKEY").val(null);
                    self.FilesExist(jsonData.DOCUMENTFILE_LIST);
                    var filesExist = self.FilesExist();
                    if (filesExist.length > 0) {
                        var fileInfo = filesExist[0];
                        self.FileDetail.RecordNo(fileInfo.DOCNO);
                       
                        $("#SEARCHKEY").val(fileInfo.SEARCHKEY);
                        $(ah.config.id.searchkey).prop(ah.config.attr.readOnly, true);
                    }
                    return;

                }

                self.srModelDataArray = jsonData.ASSETMODEL;

                if (jsonData.MANUFACTURER) {
                    self.srManufacturerDataArray = jsonData.MANUFACTURER;
                }

                if (jsonData.DEVICE_TYPE) {
                    var deviceTypeList = jsonData.DEVICE_TYPE;

                    $.each(deviceTypeList, function (item) {

                        $(ah.config.id.deviceType)
                            .append($("<option>")
                                .attr("value", deviceTypeList[item].CATEGORY)
                                .text(deviceTypeList[item].DESCRIPTION));
                    });
                }

                window.location.href = ah.config.url.modalUpload;

            }
            else if (ah.CurrentMode == ah.config.mode.search) {
                if (jsonData.LOV_LOOKUPS) {
                    var documentLibraryLOV = jsonData.LOV_LOOKUPS;
                    $.each(documentLibraryLOV, function (item) {

                        $(ah.config.id.docLibraryCategory)
                            .append($("<option></option>")
                                .attr("value", documentLibraryLOV[item].DESCRIPTION.trim())
                                .text(documentLibraryLOV[item].DESCRIPTION));
                    });
                }

                

                if (jsonData.DOCUMENTLIBRARY_LIST) {
                    self.searchResult(jsonData);
                }
                if (jsonData.DOCUMENT_LIST) {
                    self.showDocumentList(jsonData);
                }
                
                
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {
               

            }

            if (jsonData.ClientRules) {
                clientRules = jsonData.ClientRules[0];
            }

        }
        else {

            alert(systemText.errorTwo);
            //window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};