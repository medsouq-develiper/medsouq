﻿/*Controls visibility and accessibility of controls*/
var AppHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentSearch: '.search-content-detail',
            modulefunctionDetailItem: '.modulefunctionDetailItem',
            mobilefunctionDetailItem: '.mobilefunctionDetailItem',
            mobileHide: '.mobileHide',
            moduleClass: '.moduleclass',
            mobileClass: '.mobileClass',
            detailItem: '.detailItem',
            liApiStatus: '.liApiStatus',
            liExpiryNotify: '.liExpiryNotify',
            detailClose: '.detailClose',
            lirunPM: '.lirunPM',
            detailrequest: '.detailrequest'
  
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            tbody: 'tbody',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul'
           
        },
        tagId: {
            btnBlue: 'btnBlue',
            btnGreen: 'btnGreen',
            btnRed: 'btnRed',
			btnBlack: 'btnBlack',
			openIssue: 'openIssue',
			completedIssue: 'completedIssue',
			cancelledIssue: 'cancelledIssue'

        },
        id: {
            searchMenu: '#searchMenu',
            myLinks: '#myLinks',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            urlID: '#URL',
            searchResult1: '#searchResult1',
            spanUser: '#spanUser',
            totalPatients: '#totalPatients',
            totalbookP: '#totalbookP',
            totalbookT: '#totalbookT',
            totalbookF: '#totalbookF',
            sesson1: '#sesson1',
            sesson2: '#sesson2',
            sesson3: '#sesson3',
			colorScheme: '#colorScheme',
			searchResultHDRList: '#searchResultHDRList',
			userGroupMenu: '#userGroupMenu',
            helpDeskNew: '#helpDeskNew',
            expiryNotification: '#expiryNotification',
            searchResultNotify: '#searchResultNotify',
            woLabourInfoQV: '#woLabourInfoQV',
            assetWarrantyQV: '#assetWarrantyQV',
            searchResultListQVParts: '#searchResultListQVParts',
            clientName: '#clientName',
            headerMod: '#headerMod',
            welcomeUser: '#welcomeUser',
            searchTransResultList: '#searchTransResultList'
  
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            dataurlID: 'data-urlID',
            datainfoId: 'data-infoID',
            datamoduleID: 'data-moduleID',
            dataBadge: 'data-badge',
            dataInfo: 'data-info'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7

        },
        skey: {
            staff:'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            modulefunctionDetails: 'MODULE_FUNCTIONS',
            searchResult: 'searchResult',
            userApps: 'USER_APPLICATION_V',
			cssSite: 'cssSite',
            groupPrivileges: 'GROUP_PRIVILEGES',
            loginDirective: 'loginDirective',
            userClient: 'USER_CLIENT'
        },
        url: {
            homeIndex: '/Home/Index',
            menuApps: '/Menuapps/aimsIndex',
			modalUserApplications: '#openModalInfo',
            aimsInformation: '/aims/assetInfo',
            assetOnHand:'/aims/assetonhand',
            aimsWorkOrder: '/aims/amWorkOrder',
            aimsWorkOrderClose: '/aims/amWorkOrderForClose',
            hdrInfo: '/Emessages',
            openModal: '#openModal',
            openModalX: 'openModal',
            openPurchaseRequest: '/aims/amPurchaseRequest',
            openPurchaseOrder: '/aims/amPurchaseOrder',
            modalClose: '#',
            openDispathch: '/aims/amDispatchPartsWO',
            openModalChangePasswordActive: '#openModalChangePasswordActive',
            openModalQRCode: '#openModalQRCode',
            homePasswordChange: '/Home?changepasswordsuccess=asdfsadfasdxcvuhihsdjkfsdf',
            openModalQuickView: '#openModalQuickView',
            aimsWorkOrderHold: '/aims/amWorkOrder?bstbxyxkktpk=onhold',
            purchaseRequestR: '/aims/amPurchaseRequest?xikhkjxhexdher=REQ',
            purchaseRequestA: '/aims/amPurchaseRequest?xikhkjxhexdher=APPROVED',
            stockOnhand: '/inventory/stockOnHand?xikhkjxhexdher=zero',
            openAimsInformationRetired: '/aims/assetInfo?asdfxhsadfsdfx=RETIRED'
        },
        api: {
            getLookupList: '/Setup/GetModulesLookUpTables',
            searchAll: '/Search/SearchMenuAccess',
            updatestaffApi: '/Setup/insertSessionDR',
			authenticateUser: '/Security/AuthenticateUser',
			searchAllNoDashBoard: '/Search/SearchMenuAccessNoDashBoard',
            searchAllHDRDashBoard: '/Search/SearchMenuAccessHDRDashBoard',
            searchAllCSTDashBoard: '/Search/SearchMenuAccessCSTDashBoard',           

            searchAllENGRDashBoard: '/Search/SearchMenuAccessENGRDashBoard',
            searchAllNotification: '/Reports/SearchNotification',
            updateNotify: '/Setup/UpdateNotify',
            updatechangepw: '/Security/updateUserPasswordChange',
            updatepm: '/Search/UpdatePMSchedule',
            searchWOQuickView: '/AIMS/ReadWOQuickView',
            insertUserAction: "/Search/InsertUserAction"
            //SearchModuleFunctions'
      
        },
        html: {
            searchRowHeaderTemplate: "<h3><a href='#'class='moduleclass' data-moduleID='{0}'><span class='  fs-medium-normal fc-greendark' id='headerMod'>{1}</span></a></h3> \n ", //
            searchRowTemplate: "<a href='#' class=' fc-green search-row-data modulefunctionDetailItem mobilefunctionDetailItem' data-urlID='{0}'>&nbsp;&nbsp;<i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} &nbsp;&nbsp;&nbsp;&nbsp; </a>",
            searchRowTemplateMod: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}&nbsp;</a>",
			searchRowHeaderTemplateList: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
			searchRowTemplateList: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
            searchRowTemplateENGList: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
			searchRowHeaderTemplateENGR: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplateENGR: '<span class="search-row-label fc-light-grey-2">{0}: <span class="search-row-data">{1}</span></span>',
            searchRowTemplateModify: "<span class='search-row - label modifyWO  detailItem' data-infoID='{0}'> <span>{1}</span></span>",
            searchRowTemplateClose: "<span class='search-row - label closeWO  detailClose' data-infoID='{0}'> <span>{1}</span></span>",
            searchRowHeaderTemplateNotifyList: "<a href='#' class='fs-medium-normal detailItem fc-slate-blue' data-info ='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} <span class='fc-green'> {3} </span></a>",
            searchRowTemplatenotify: "<span class='search-row - label closeWO  detailClose' data-infoID='{0}'> <span>{1}</span></span>",
            searchRowTemplateQuickView: "<span style='color:white; background-color:green;padding-left: 10px;padding-right: 10px;cursor: pointer;'   class='search-row - label  detailrequest' data-infoID='{0}'> <span>{1}</span></span>",
            searchRowHeaderTemplateMenu: "<h3><a href='#'class='moduleclass' data-moduleID='{0}'><span class='  fs-medium-normal' id='headerMod'><i class='fa fa-chevron-circle-right'></i>&nbsp;&nbsp;{1}</span></a></h3> \n ", //
            searchRowHeaderTemplateMenuMobile: "<h3><a href='#'class='mobileClass' data-moduleID='{0}'><span class='  fs-medium-normal' id='headerMod'><i class='fa fa-chevron-circle-right'></i>&nbsp;&nbsp;{1}</span></a></h3> \n ", //
            searchRowHeaderTransTemplate: "<a href='#' class='fs-medium-normal fc-slate-blue detailItem' data-infoID='{0}'>&nbsp;&nbsp;{1} - {2} {3}</a>",
			}
    };
    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus).hide();

    };
    thisApp.mobileHide = function () {

        //$(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.mobileHide).hide();
       // $('#colorScheme').removeAttribute("class");
        document.getElementById("colorScheme").removeAttribute("class");
        document.getElementById("colorScheme").setAttribute("class", "toolbox-left flex-item-1");
        document.getElementById("userInfo").removeAttribute("class");
        document.getElementById("userInfo").setAttribute("class", "align-left");
        //$(thisApp.config.id.colorScheme).setAttribute("class", "toolbox-left flex-item-1");

    };
    thisApp.appsShow = function () {

        //$(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.mobileHide).show();

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        //$(
        //    thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
        //    thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        //).hide();

    };
    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();

    };
	thisApp.formatJSONDateTimeToString = function () {


		var dateToFormat = arguments[0];
		var strDate;
		var strTime;

		if (dateToFormat === null) return "-";
		if (dateToFormat.length < 16) return "-";

		strDate = String(dateToFormat).split('-', 3);
		strTime = String(dateToFormat).split('T');

		dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0] + ' ' + strTime[1].substring(0, 5);


		return dateToFormat;
	};
    thisApp.formatJSONDateToString = function () {

        var dateToFormat = arguments[0];
        var strDate;

        if (dateToFormat === null) return "-";
        if (dateToFormat.length < 10) return "-";
        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };
    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');

            if (isDateTime) {
                val = thisApp.formatJSONDateTimeToString(val);
            }

            if (isDate) {
                val = thisApp.formatJSONDateToString(val);
            }

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};
/*Knockout MVVM Namespace - Controls form functionality*/
var menuappsViewModel = function (systemText) {
      
    var self = this;
	var ah = new AppHelper(systemText);
	var srDataArray = ko.observableArray([]);
    var srHDRArray = ko.observableArray([]);
    var srWOEngrCount = ko.observableArray([]);
    var DashBoard = '';
    var submenuUrl = null;
    
    self.clientInfo = {
        Description: ko.observable(''),
        clientLogo: ko.observable(''),
        isVisible: ko.observable(false),
        clientCode: ko.observable(''),
        costCenter: ko.observable('')
    };

    self.searchFilter = {
        AssetFilterCondition: ko.observable(''),
        toDate: ko.observable(''),
        status: ko.observable(''),
        staffId: ko.observable(''),
        notifyStaffId: ko.observable(''),
        notifyStatus: ko.observable(''),
        userid: ko.observable(''),
        curpassword: ko.observable(''),
        newpassword: ko.observable(''),
        ProblemType:ko.observable(''),
        HelpDeskRequestStatus: ko.observable("'OPEN','PENDING','ONHOLD'")
    };
    self.woDetails = {
        WO_STATUS: ko.observable(),
        SPECIALTY: ko.observable(),
        SERV_DEPT: ko.observable(),
        REQ_TYPE: ko.observable(),
        REQ_REMARKS: ko.observable(),
        REQ_NO: ko.observable(0),
        REQ_DATE: ko.observable(),
        REQ_BY: ko.observable(),
        REQUESTER_CONTACT_NO: ko.observable(),
        PROBLEM: ko.observable(),
        JOB_URGENCY: ko.observable(),
        JOB_TYPE: ko.observable(),
        JOB_DURATION: ko.observable(),
        JOB_DUE_DATE: ko.observable(),
        ASSIGN_TYPE: ko.observable(),
        ASSIGN_TO: ko.observable(),
        ASSET_NO: ko.observable(),
        COST_CENTER: ko.observable(),
        DESCRIPTION: ko.observable(),
        LOCATION: ko.observable(),
        DEVICE_TYPE: ko.observable(),
        STATUS: ko.observable(),
        RESPONSIBLE_CENTER: ko.observable(),
        MODEL_NAME: ko.observable(),
        MODEL_NO: ko.observable(),
        SERIAL_NO: ko.observable(),
        WARRANTY_NOTES: ko.observable(),
        WARRANTY_INCLUDE: ko.observable(),
        WARRANTY_EXPIRYDATE: ko.observable(),
        TERM_PERIOD: ko.observable(),
        RISK_INCLUSIONFACTOR: ko.observable(),
        REQTICKETNO: ko.observable(),
        CLIENTNAME: ko.observable(),
        CLIENTADDRESS: ko.observable(),
        CLIENTCONTACT: ko.observable(),
        WOL_EMPLOYEE: ko.observable(),
        WOL_HOURS: ko.observable(),
        WOL_BILLABLE: ko.observable(),
        WOL_LABOUR_DATE: ko.observable(),
        WOL_ACTION: ko.observable(),
        COMPANY_LOGO: ko.observable(),
        CLIENT_LOGO: ko.observable(),
        HEADING: ko.observable(),
        HEADING2: ko.observable(),
        HEADING3: ko.observable(),
        HEADING_OTH: ko.observable(),
        HEADING_OTH2: ko.observable(),
        HEADING_OTH3: ko.observable(),
        MANUFACTURER: ko.observable(),
        WOACTIONLAST: ko.observable(),
        PM_TYPE: ko.observable(),
        INSERVICE_DATE: ko.observable(),
        PM_FREQUENCY: ko.observable(),
        FAULT_FINDING: ko.observable()
    };
	self.getIpAddress = function () {
		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		//alert(webApiURL + 'api/IpAddress/getIpAddress');
		$.post(webApiURL + 'api/IpAddress/getIpAddress').done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
	};
    self.initialize = function () {
        $("#nodifyId").hide();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var userClientList = JSON.parse(sessionStorage.getItem(ah.config.skey.userClient)); 
        var postData;
        $(ah.config.cls.liExpiryNotify).hide();
        var result = new Date();
        var expiredResult = result.setDate(result.getDate());
        result = result.setDate(result.getDate() + 10);
       
        var expiryDate = new Date(staffLogonSessions.LOGINEXPIRY);
        var expiryresult = new Date(expiryDate);
        expiryresult = expiryresult.setDate(expiryresult.getDate() - 10);
        var newExpiry = expiryDate.setDate(expiryDate.getDate() - 10);
        newExpiry = new Date(newExpiry);

        if (expiredResult > newExpiry) {
            newExpiry = newExpiry.toDateString();
            $(ah.config.id.expiryNotification).text('License Expired : ' + newExpiry);
            $(ah.config.cls.liExpiryNotify).show();
        } else if (result >= expiryresult){
            newExpiry = newExpiry.toDateString();
            $(ah.config.id.expiryNotification).text('License Expiry On : ' + newExpiry);
            $(ah.config.cls.liExpiryNotify).show();
        }

        
        

        //alert(JSON.stringify(staffLogonSessions));
        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());
        $(ah.config.id.welcomeUser).text("Welcome "+staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());
        

        if (userClientList.length == 1) {
            self.clientInfo.Description = userClientList[0].Description;
            self.clientInfo.clientLogo = userClientList[0].LogoPath;
            self.clientInfo.isVisible = true;
            self.clientInfo.clientCode = userClientList[0].CLIENT_CODE;
        }
      

      //  ah.toggleReadMode();
       
        postData = { STAFF_LOGON_SESSIONS: staffLogonSessions };
        // $.post(webApiURL + ah.config.api.getLookupList, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        sessionStorage.removeItem("AM_ASSET_DETAILS");
        sessionStorage.removeItem("AM_MANUFACTURER");
        sessionStorage.removeItem("AM_PARTSREQUEST");
        sessionStorage.removeItem("AM_PURCHASEREQUEST");
        sessionStorage.removeItem("AM_SUPPLIER");
        sessionStorage.removeItem("AM_WORK_ORDERS");
		sessionStorage.removeItem("ASSET_WOSUMMARY_V");
		sessionStorage.removeItem("AM_ASSETSUMMARY");
		sessionStorage.removeItem("AM_PURCHASEORDERITEMS");
		sessionStorage.removeItem("AM_INSPECTION");
		sessionStorage.removeItem("MODULE_ACCESS_ATTRIBUTE");
		sessionStorage.removeItem("MODULE_ACCESS_ATTRIBUTE_APPEND");
		sessionStorage.removeItem("MODULE_FUNCTIONS");
        sessionStorage.removeItem("LOV_LOOKUPS");
        sessionStorage.removeItem("PRODUCT_STOCKBATCH");
        sessionStorage.removeItem("PRODUCT_STOCKBATCHPUSH");
        sessionStorage.removeItem("PRODUCT_STOCKONHAND");
        sessionStorage.removeItem("PRODUCT_STOCKONHAND_REQSTORE");

        var menuHeight = $(".menu-container").innerHeight();
        $(".board-content").css("height", (menuHeight - 55) + "px");
       
        if (outerWidth >= 726) {
            console.info("showing");
            ah.appsShow();
        } else {
            
            console.info("hiding");
            ah.mobileHide();
        }
		$(ah.config.id.userGroupMenu).hide();
        self.searchNow();

        $.ajax({
            url: '/Languange/getLanguange',
            type: "GET",
            async: false,
            contentType: false,
            processData: false,
            data: {
                localize: ""
            },
            success: function (data) {
                if (data === 'ar') {
                    $("#languangeId").text("English");
                    $("#LANGID").val("ARABIC");
                } else {
                    $("#languangeId").text("العربية");
                    $("#LANGID").val("ENGLISH");
                }
            },
            error: function (err) {

            }
        });
		
    };
    window.onresize = function (event) {
        if (event.currentTarget.outerWidth >= 726) {
            ah.appsShow();
        } else {
            ah.mobileHide();
        }
        
        var menuHeight = $(".menu-container").innerHeight();
        $(".board-content").css("height", (menuHeight - 55) + "px");
    };
    self.schemeColor = function () {
        
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var senderID = $(arguments[1].currentTarget).attr('id');
        var headerText = '';
        var subHeaderText = '';
        
        switch (senderID) {
            case ah.config.tagId.btnBlue:{
                sessionStorage.setItem(ah.config.skey.cssSite, "siteBlue.css");
                
                if (staffDetails.HOME_DEFAULT != null && staffDetails.HOME_DEFAULT != "") {
                    
                    window.location.href = staffDetails.HOME_DEFAULT;
                } else {
                    
                    window.location.href = ah.config.url.menuApps;
                }
               
                break;
            }
            case ah.config.tagId.btnGreen: {
                sessionStorage.setItem(ah.config.skey.cssSite, "siteGreen.css");
                if (staffDetails.HOME_DEFAULT != null && staffDetails.HOME_DEFAULT != "") {
                    window.location.href = staffDetails.HOME_DEFAULT;
                } else {
                    window.location.href = ah.config.url.menuApps;
                }
                break;
            }

            case ah.config.tagId.btnRed: {
                sessionStorage.setItem(ah.config.skey.cssSite, "siteRed.css");
                if (staffDetails.HOME_DEFAULT != null && staffDetails.HOME_DEFAULT != "") {
                    window.location.href = staffDetails.HOME_DEFAULT;
                } else {
                    window.location.href = ah.config.url.menuApps;
                }
                break;
            }
            case ah.config.tagId.btnBlack: {
                sessionStorage.setItem(ah.config.skey.cssSite, "siteBlack.css");
                if (staffDetails.HOME_DEFAULT != null && staffDetails.HOME_DEFAULT != "") {
                    window.location.href = staffDetails.HOME_DEFAULT;
                } else {
                    window.location.href = ah.config.url.menuApps;
                }
                break;
            }           
        }
       
    };
    self.changePassword = function () {
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
       
        $("#CUR_USERID").val(staffLogonSessions.USER_ID);
        $("#invalidInfo").hide();
        $("#validInfo").hide();

        window.location.href = ah.config.url.openModalChangePasswordActive;
    };
    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };
    self.PolicyGuide = function () {

        var mywin = window.open("Policy.pdf", "width=960,height=764");

        // $(mywin.document.body).html(contents);

    };
    self.userGuide = function () {
       
        var mywin = window.open("userguide.pdf", "width=960,height=764");

       // $(mywin.document.body).html(contents);

    };
    self.quickuserGuide = function () {

        var mywin = window.open("quickuserguide.pdf", "width=960,height=764");

        // $(mywin.document.body).html(contents);

    };
    self.flyerGuide = function () {

        var mywin = window.open("digitalDoctorFlyer.pdf", "width=960,height=764");

    };
    self.regModule = function () {
        window.location.href = ah.config.url.patientIndex;
	};
    self.pageLoader = function () {
        var senderID = $(arguments[1].currentTarget).attr('id');
      

        switch (senderID) {

            case ah.config.tagId.openIssue: {
                $(ah.config.id.searchResultHDRList + ' ' + ah.config.tag.ul).html('');
                self.searchFilter.HelpDeskRequestStatus = "'OPEN','PENDING','ONHOLD'";
                self.searchNow();
                $.ajax({
                    url: '/Languange/getLanguange',
                    type: "GET",
                    async: false,
                    contentType: false,
                    processData: false,
                    data: {
                        localize: ""
                    },
                    success: function (data) {
                        if (data === 'ar') {
                            $('#searchHeaderText').text('طلبات العمل الغير مغلقة ');
                        }
                        else {
                            $('#searchHeaderText').text('Open/Pending/On-Hold Issue');
                        }
                    },
                    error: function (err) {

                    }
                });

                break;
            }
            case ah.config.tagId.completedIssue: {
                $(ah.config.id.searchResultHDRList + ' ' + ah.config.tag.ul).html('');
                self.searchFilter.HelpDeskRequestStatus = "'COMPLETED'";
                self.searchNow();
                //$('#searchHeaderText').text('Completed Issue');
                $.ajax({
                    url: '/Languange/getLanguange',
                    type: "GET",
                    async: false,
                    contentType: false,
                    processData: false,
                    data: {
                        localize: ""
                    },
                    success: function (data) {
                        if (data === 'ar') {
                            $('#searchHeaderText').text('طلبات العمل المغلقة ');
                        }
                        else {
                            $('#searchHeaderText').text('Completed Issue');

                        }
                    },
                    error: function (err) {

                    }
                });
                break;
            }

            case ah.config.tagId.cancelledIssue: {
                $(ah.config.id.searchResultHDRList + ' ' + ah.config.tag.ul).html('');
                self.searchFilter.HelpDeskRequestStatus = "'CANCELLED'";
                self.searchNow();
                //$('#searchHeaderText').text('Cancelled Issue');
                $.ajax({
                    url: '/Languange/getLanguange',
                    type: "GET",
                    async: false,
                    contentType: false,
                    processData: false,
                    data: {
                        localize: ""
                    },
                    success: function (data) {
                        if (data === 'ar') {
                            $('#searchHeaderText').text('طلبات ملغاة');
                        }
                        else {
                            $('#searchHeaderText').text('Cancelled Issue');
                        }
                    },
                    error: function (err) {

                    }
                });
                break;
            }
        }

    };

	self.pageLoaderENGR = function () {
		var senderID = $(arguments[1].currentTarget).attr('id');


		switch (senderID) {
            case ah.config.tagId.openIssue: {
                //$('#searchHeaderText').text('Pending Work Order');
                self.searchFilter.Status = "PSOPFC";
                self.searchNow();
                $.ajax({
                    url: '/Languange/getLanguange',
                    type: "GET",
                    async: false,
                    contentType: false,
                    processData: false,
                    data: {
                        localize: ""
                    },
                    success: function (data) {
                        if (data === 'ar') {
                            $('#searchHeaderText').text('طلبات غير مغلقة');
                        }
                        else {
                            $('#searchHeaderText').text('Pending Work Order');
                        }
                    },
                    error: function (err) {

                    }
                });
                break;
			}
			case ah.config.tagId.completedIssue: {
                //$('#searchHeaderText').text('Closed Work Order');
                self.searchFilter.Status = "CL";
                self.searchNow();
                $.ajax({
                    url: '/Languange/getLanguange',
                    type: "GET",
                    async: false,
                    contentType: false,
                    processData: false,
                    data: {
                        localize: ""
                    },
                    success: function (data) {
                        if (data === 'ar') {
                            $('#searchHeaderText').text('طلبات مغلقة');
                        }
                        else {
                            $('#searchHeaderText').text('Closed Work Order');
                        }
                    },
                    error: function (err) {
                    }
                });

				break;
			}

            case ah.config.tagId.cancelledIssue: {
                self.searchFilter.Status = "HA";
                //$('#searchHeaderText').text('On-Hold Work Order');
                $.ajax({
                    url: '/Languange/getLanguange',
                    type: "GET",
                    async: false,
                    contentType: false,
                    processData: false,
                    data: {
                        localize: ""
                    },
                    success: function (data) {
                        if (data === 'ar') {
                            //$('#searchHeaderText').text('طلبات العمل المفتوحة ' + openPendingCount + '');
                            //$('#searchHeaderText').text('طلبات العمل المغلقة ' + completedCount + '');
                            $('#searchHeaderText').text('طلبات منتظرة');
                        }
                        else {
                            $('#searchHeaderText').text('On-Hold Work Order');
                            //$('#completedIssue').text('Closed Work Order ' + completedCount + '');
                            //$('#cancelledIssue').text('On-Hold Work Order ' + cancelledCount + '');

                        }
                    },
                    error: function (err) {

                    }
                });
                self.searchNow();

				break;
			}
		}

	};
    self.toggleApps = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        
        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.userApps));
        
        $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).html
        if (sr.length > 0) {
            var htmlstr = '';
            for (var o in sr) {

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateMod, JSON.stringify(sr[o]), sr[o].DESCRIPTION);

                htmlstr += '</li>';

            }

            $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).append(htmlstr);
            $(ah.config.cls.detailItem).bind('click', self.swicthApp);
            window.location.href = window.location.href + "#openModalInfo";
            
        }
    };
    self.getNotificationList = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        ah.toggleSearchMode(); 
        self.searchFilter.notifyStaffId = staffDetails.STAFF_ID;
        self.searchFilter.notifyStatus = 'A';
        
        postData = { SEARCH_FILTER: self.searchFilter, SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAllNotification, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus); 
    };
    self.searchNotificationResult = function (jsonData) {
        ah.displaySearchResult();
        $(ah.config.cls.liAddNew).hide();
        var sr = jsonData.NOTIFICATION_LIST;
         //alert(JSON.stringify(sr));
        $(ah.config.id.searchResultNotify + ' ' + ah.config.tag.ul).html('');

        

        if (sr.length > 0) {
            //$(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));
            var htmlstr = '';

            for (var o in sr) {
               
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplateNotifyList, JSON.stringify(sr[o]), '', sr[o].CREATED_DATE,"");
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateENGR, systemText.searchModeLabels.msg, sr[o].NOTIFY_MESSAGE);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplatenotify, sr[o].NOTIFY_ID, 'Notified');
                htmlstr += '</li>';

            }
            $(ah.config.id.searchResultNotify + ' ' + ah.config.tag.ul).append(htmlstr);
        }
      
        var hrefLocation = window.location.href.replace("#","");
        
        window.location.href = hrefLocation+ ah.config.url.openModal;
        $(ah.config.cls.detailItem).bind('click', self.notifyAction);
        $(ah.config.cls.detailClose).bind('click', self.updatenotification);
 
    };
    self.notifyAction = function () {
    
        var notifyInfo = JSON.parse(this.getAttribute(ah.config.attr.dataInfo));/* this.getAttribute(ah.config.attr.datainfoId).toString();	*/
   
        if (notifyInfo.NOTIFY_ID === 3) {
            window.location.href = ah.config.url.openPurchaseRequest + "?PRNO=" + notifyInfo.REF_CODE;
        } else if (notifyInfo.NOTIFY_ID === 4){
            window.location.href = ah.config.url.openPurchaseOrder + "?PONO=" + notifyInfo.REF_CODE;
            
        } else if (notifyInfo.NOTIFY_ID === 1) {
            window.location.href = ah.config.url.aimsWorkOrder + "?REQNO=" + notifyInfo.REF_CODE + "&ASSETNO=" + '' + "&REQWOAC=sdfsdfxdfwefsd";

        } else if (notifyInfo.NOTIFY_ID === 5) {
            window.location.href = ah.config.url.openDispathch + "?WONO=" + notifyInfo.REF_CODE + "&action=dispatch";

        }


        
    };
    self.updatenotification = function () {
        window.location.href = ah.config.url.modalClose;
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var reqID = this.getAttribute(ah.config.attr.datainfoId).toString();
        self.searchFilter.notifyStaffId = staffDetails.STAFF_ID;
        self.searchFilter.notifyStatus = 'A';

        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        updatedDate = strDateStart + ' ' + hourmin;


        postData = { SEARCH_FILTER: self.searchFilter, NOTIFY_ID: reqID, UPDATED_BY: staffDetails.STAFF_ID, UPDATED_DATE: updatedDate, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.updateNotify, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus); 
    };
	self.assetInfo = function () {
		window.location.href = ah.config.url.aimsInformation;
	};
    self.swicthApp = function () {
        var appInfo = JSON.parse(this.getAttribute(ah.config.attr.datainfoId));
        //var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        staffDetails.APP_ID = appInfo.APP_ID;
        sessionStorage.setItem(ah.config.skey.staff, JSON.stringify(staffDetails));        
        window.location.href = appInfo.APP_HOMEURL;
    
    }
    self.woOnhold = function () {
        window.location.href = ah.config.url.aimsWorkOrderHold;
    };
    self.openWorkOrder = function () {
        window.location.href = ah.config.url.aimsWorkOrder;
    };
    self.openAssetOnHand = function () {
        window.location.href = ah.config.url.assetOnHand;
    };
    self.assetInfoScreen = function () {
        window.location.href = ah.config.url.aimsInformation;
        
    };
    self.openAssetInfoScreenRetired = function () {
        window.location.href = ah.config.url.openAimsInformationRetired;

    };
    self.purchaseReqReq = function () {
        window.location.href = ah.config.url.purchaseRequestR;

    };
    self.purchaseReqAPV = function () {
        window.location.href = ah.config.url.purchaseRequestA;

    };
    self.stockOnhandZero = function () {
        window.location.href = ah.config.url.stockOnhand;
    };

    self.checkrunPmSchedule = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        postData = { STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.updatepm, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);


    };
    self.DisplaySearchResult = function (sr) {
        var html = $(ah.config.id.searchMenu).html('');
        var html = $(ah.config.id.myLinks).html('');
       
      
        if (sr.length > 0) {
            if (sr.length === 0) {
                sr = "";
                sr = jsonData.MODULE_ACCESS_ATTRIBUTE;
            }

            var htmlstr = '';
            var htmlstrfin = '';
            var lastmodule = '';
            var iix = 0;
            var ix = 1;
            var xxy = 0;
            htmlstrfin += '<ul>';
            htmlstr += '<ul>';
           
            
            var pModule = sr.filter(function (item) { return item.ATTRIBUTE === "MODULE" });
           
            for (var o in pModule) {
                if ($("#LANGID").val() === "ARABIC") {
                    pModule[o].DESCRIPTION = pModule[o].DESCRIPTION_OTH;
                };
                htmlstrfin += '<li>';
                htmlstrfin += ah.formatString(ah.config.html.searchRowHeaderTemplateMenu, 'MOD-' + pModule[o].MODULE_ID, pModule[o].DESCRIPTION) + '\n';
                htmlstrfin += '<ul style="display: none;" id="' + pModule[o].MODULE_ID + '">\n';

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplateMenuMobile, 'MOD-' + pModule[o].MODULE_ID, pModule[o].DESCRIPTION) + '\n';
                htmlstr += '<ul style="display: none;" id="mob-' + pModule[o].MODULE_ID + '">\n';
                
                var pSubMenu = sr.filter(function (item) { return item.MODULE_ID === pModule[o].MODULE_ID && item.ATTRIBUTE !== "MODULE" });
                for (var o in pSubMenu) {
                    if ($("#LANGID").val() === "ARABIC") {
                        pSubMenu[o].DESCRIPTION = pSubMenu[o].DESCRIPTION_OTH;
                    };
                    htmlstrfin += '<li>';
                    htmlstrfin += ah.formatString(ah.config.html.searchRowTemplate, JSON.stringify(pSubMenu[o]), pSubMenu[o].DESCRIPTION);
                    htmlstrfin += '</li>\n';

                    htmlstr += '<li>';
                    htmlstr += ah.formatString(ah.config.html.searchRowTemplate, JSON.stringify(pSubMenu[o]), pSubMenu[o].DESCRIPTION);
                    htmlstr += '</li>\n';
                }
                htmlstrfin += '</ul>\n';
                htmlstrfin += '</li>\n';

                htmlstr += '</ul>\n';
                htmlstr += '</li>\n';
               

            }
            htmlstrfin +='</ul>';
            htmlstr +='</ul>';
			$(ah.config.id.searchMenu).append(htmlstrfin);
            $(ah.config.id.myLinks).append(htmlstr);
			$(ah.config.id.userGroupMenu).show();
        } else {
            $(ah.config.id.userGroupMenu).hide();
            document.getElementById("menuappsPadding").style.marginLeft = 0;
            document.getElementById("topnav").style.display= "none";
        }
        $(ah.config.cls.moduleClass).bind('click', self.readModuleId);
        $(ah.config.cls.mobileClass).bind('click', self.readMobileId);
        $(ah.config.cls.modulefunctionDetailItem).bind('click', self.readModuleFunctionByModuleFunctionID);
        ah.displaySearchResult();
    };
    self.searchResult = function (jsonData) {
        //return;
        sessionStorage.setItem(ah.config.skey.searchResult, JSON.stringify(jsonData.MODULE_ACCESS_ATTRIBUTE));
        var sr = jsonData.MODULE_ACCESS_ATTRIBUTE;
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));


        var srt = jsonData.SYSTEM_TRANSACTION;

        $(ah.config.id.searchTransResultList + ' ' + ah.config.tag.ul).html('');

        var srtFilter = jsonData.SYSTEM_TRANSACTION;
     
        var uq = [];
        var a = [];
        for (i = 0; i < srtFilter.length; i++) {
            if (uq.indexOf(srtFilter[i].REFCODE) === -1) {
                uq.push(srtFilter[i].REFCODE);
                a.push(srtFilter[i]);

            }
        }


        if (uq.length > 0) {
            var htmlstr = '';

            for (var o in a) {


                var newArray = srt.filter(function (el) {
                    return el.REFCODE == a[o].REFCODE;
                });


                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTransTemplate, a[o].PROCEDURE_ID, a[o].REFCODE, a[o].REFDESCRIPTION, '');
                for (var i in newArray) {

                    htmlstr += '<p>';
                    htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.status, newArray[i].MESSAGE);
                    htmlstr += '</p>';
                }
                htmlstr += '</li>';
            }

            $(ah.config.id.searchTransResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }


        var db = jsonData.DASHBOARD_V;
        var nodifyInfo = jsonData.NOTIFYCOUNT;
      
        if (nodifyInfo > 0) {
            $("#nodifyId").show();
            var e = document.getElementById("nodifyInfo");
            e.setAttribute("data-badge", nodifyInfo);
        } else {
            $("#nodifyId").hide();
        }
     
            $(ah.config.cls.lirunPM).hide();
     
		if (typeof (db) !== 'undefined') {
		
		
			if (db.length > 0) {
			   // alert(JSON.stringify(db));
				var htmlstr = '';

				for (var o in db) {
					if (db[o].RPT_TYPE === "PR_REQUEST") {
						$("#totalPREQ").text(db[o].PR_REQ);
						continue;
					}
					if (db[o].RPT_TYPE === "STOCK_ONHAND") {
						$("#totalOnHand").text(db[o].PR_REQ);
						continue;
					}
					if (db[o].RPT_TYPE === "PM_OVERDUE30") {
						$("#totalPM").text(db[o].PR_REQ);
						continue;
					}
					if (db[o].RPT_TYPE === "WO_PENDING_PM") {
						$("#totalWOPending").text(db[o].PR_REQ);
						continue;
                    }
                    if (db[o].RPT_TYPE === "WO_PENDING_OTH") {
                        $("#totalWOPendingOTH").text(db[o].PR_REQ);
                        continue;
                    }
                    

					if (db[o].RPT_TYPE === "WO_ONHOLD") {
						$("#totalOnHold").text(db[o].PR_REQ);
						continue;
					}
					if (db[o].RPT_TYPE === "WO_OVERDUE_PM") {
						$("#totalOverDue").text(db[o].PR_REQ);
						continue;
                    }
                    if (db[o].RPT_TYPE === "WO_OVERDUE_OTH") {
                        $("#totalOverDueOTH").text(db[o].PR_REQ);
                        continue;
                    }
                    

					if (db[o].RPT_TYPE === "ASSET") {
						$("#totalAsset").text(db[o].PR_REQ);
						continue;
                    }
                    if (db[o].RPT_TYPE === "ASSETONHAND") {
                        $("#totalAssetOnhand").text(db[o].PR_REQ);
                        continue;
                    }
					if (db[o].RPT_TYPE === "STOCK_EXPIRY30") {
						$("#totaltoReceive").text(db[o].PR_REQ);
						continue;
                    }
                    if (db[o].RPT_TYPE === "TOTAL_RECALL") {
                        $("#totalRecall").text(db[o].PR_REQ);
                        continue;
                    }
                    if (db[o].RPT_TYPE === "RETIRED") {
                        $("#totalAssetCondemned").text(db[o].PR_REQ);
                        continue;
                    }
                    

                    
				}
			}
 
        }
        self.DisplaySearchResult(sr);


	};
	self.searchHDRResult = function (statusAction,jsonData) {
		//return;
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		var sr = jsonData.MODULE_ACCESS_ATTRIBUTE;

        var nodifyInfo = jsonData.NOTIFYCOUNT;
        if (nodifyInfo > 0) {
            $("#nodifyId").show();
            var e = document.getElementById("nodifyInfo");
            e.setAttribute("data-badge", nodifyInfo);
        } else {
            $("#nodifyId").hide();
        }


		//alert(JSON.stringify(jsonData.ASSET_COUNT));
        self.populateHRD(statusAction, jsonData);
        $.ajax({
            url: '/Languange/getLanguange',
            type: "GET",
            async: false,
            contentType: false,
            processData: false,
            data: {
                localize: ""
            },
            success: function (data) {
                if (data === 'ar') {
                    $('#inserviceAsset').text('عدد المعدات في الخدمة ' + jsonData.ASSET_COUNT + '');
                }
                else {
                    $('#inserviceAsset').text('In-Service Assets ' + jsonData.ASSET_COUNT + '');
                }
            },
            error: function (err) {

            }
        });
        self.DisplaySearchResult(sr);


    };
    self.searchCSTResult = function (statusAction, jsonData) {
        //return;
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var sr = jsonData.MODULE_ACCESS_ATTRIBUTE;

        var nodifyInfo = jsonData.NOTIFYCOUNT;
        if (nodifyInfo > 0) {
            $("#nodifyId").show();
            var e = document.getElementById("nodifyInfo");
            e.setAttribute("data-badge", nodifyInfo);
        } else {
            $("#nodifyId").hide();
        }



        var ccDashBoard = jsonData.DASHBOARD_CLIENT;
        if (ccDashBoard != null) {
            $("#totalCostReq").text(ccDashBoard.COSTUMER_REQ);
            $("#totalCostRec").text(ccDashBoard.COSTUMER_REC);
            $("#totalAssests").text(ccDashBoard.ASSET_TOTAL);
            $("#StockItems").text(ccDashBoard.STOCK_ONHAND);
            $("#OutOfStock").text(ccDashBoard.OUTOFSTOCK);
            $("#totalNearExpiry").text(ccDashBoard.STOCK_NEAREXPIRY);
            $("#totalTickets").text(ccDashBoard.TICKET_TOTAL);
            $("#totalRecall").text(ccDashBoard.RECALL_TOTAL);
        }

        var srt = jsonData.SYSTEM_TRANSACTION;

        $(ah.config.id.searchTransResultList + ' ' + ah.config.tag.ul).html('');

        var srtFilter = jsonData.SYSTEM_TRANSACTION;

        var uq = [];
        var a = [];
        for (i = 0; i < srtFilter.length; i++) {
            if (uq.indexOf(srtFilter[i].REFCODE) === -1) {
                uq.push(srtFilter[i].REFCODE);
                a.push(srtFilter[i]);

            }
        }


        if (uq.length > 0) {
            var htmlstr = '';

            for (var o in a) {


                var newArray = srt.filter(function (el) {
                    return el.REFCODE == a[o].REFCODE;
                });


                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTransTemplate, a[o].PROCEDURE_ID, a[o].REFCODE, a[o].REFDESCRIPTION, '');
                for (var i in newArray) {

                    htmlstr += '<p>';
                    htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.status, newArray[i].MESSAGE);
                    htmlstr += '</p>';
                }
                htmlstr += '</li>';
            }

            $(ah.config.id.searchTransResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

       
        self.DisplaySearchResult(sr);


    };
    self.goToSubModule = function (action) {
        if (action == "RECALL") {
            window.location.href =  "/reports/systemTransactionReporting?recall=b62cd054-7d8a-4c96-b761-2119496c8b88";
        };
    };
	self.searchENGRResult = function (statusAction, jsonData) {
		//return;
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		var sr = jsonData.MODULE_ACCESS_ATTRIBUTE;

        var db = jsonData.DASHBOARD_V;
        var nodifyInfo = jsonData.NOTIFYCOUNT;       
        if (nodifyInfo > 0) {
            $("#nodifyId").show();
            var e = document.getElementById("nodifyInfo");
            e.setAttribute("data-badge", nodifyInfo);
        } else {
            $("#nodifyId").hide();
        }

		//alert(JSON.stringify(jsonData.AM_HELPDESKLIST));
		var hdCount = jsonData.AM_HELPDESKLIST.length;



		self.populateENGR(statusAction);
        $.ajax({
            url: '/Languange/getLanguange',
            type: "GET",
            async: false,
            contentType: false,
            processData: false,
            data: {
                localize: ""
            },
            success: function (data) {
                if (data === 'ar') {
                    $('#inserviceAsset').text('عدد المعدات في الخدمة ' + jsonData.ASSET_COUNT + '');
                    $(ah.config.id.helpDeskNew).text('طلبات جديدة ' + hdCount + '');
                }
                else {
                    $('#inserviceAsset').text('In-Service Assets ' + jsonData.ASSET_COUNT + '');
                    $(ah.config.id.helpDeskNew).text('New Issue ' + hdCount + '');
                }
            },
            error: function (err) {

            }
        });
        self.DisplaySearchResult(sr);
    };
    self.showQuickView = function (jsonData) {

        var jsetupDetails = jsonData.WORK_ORDER_QUICKVIEW_V;
        $("#WOCLOSEDDATE").hide();

        if (jsetupDetails.QVWO_STATUS == 'NE') {
            jsetupDetails.QVWO_STATUS = 'New'
        } else if (jsetupDetails.QVWO_STATUS == 'OP') {
            jsetupDetails.QVWO_STATUS = 'Open'
        } else if (jsetupDetails.QVWO_STATUS == 'HA') {
            jsetupDetails.QVWO_STATUS = 'On Hold'
        } else if (jsetupDetails.QVWO_STATUS == 'CL') {
            jsetupDetails.QVWO_STATUS = 'Close'
        } else if (jsetupDetails.QVWO_STATUS == 'FC') {
            jsetupDetails.QVWO_STATUS = 'For Close'
        }
        if (jsetupDetails.QVCLOSED_DATE == 'CL') {
            $("#WOCLOSEDDATE").show();
        }

        if (jsetupDetails.QVJOB_DUE_DATE == "01/01/1900 00:00" || jsetupDetails.QVJOB_DUE_DATE == "null" || jsetupDetails.QVJOB_DUE_DATE == "0001-01-01T00:00:00" || jsetupDetails.QVJOB_DUE_DATE == "1900-01-01T00:00:00") {
            //var jobDueDate = ah.formatJSONDateTimeToString("1900-01-01T00:00:00");
            jsetupDetails.QVJOB_DUE_DATE = "";
        }
        ah.LoadJSON(jsetupDetails);

        var jsetupAssetDetails = jsonData.ASSET_QUICKVIEW_V;
        if (jsetupAssetDetails.AQV_STATUS == 'I') {
            jsetupAssetDetails.AQV_STATUS = 'In Service'
        } else if (jsetupAssetDetails.AQV_STATUS == 'A') {
            jsetupAssetDetails.AQV_STATUS = 'Active'
        } else if (jsetupAssetDetails.AQV_STATUS == 'M') {
            jsetupAssetDetails.AQV_STATUS = 'Missing'
        } else if (jsetupAssetDetails.AQV_STATUS == 'N') {
            jsetupAssetDetails.AQV_STATUS = 'Inactive'
        } else if (jsetupAssetDetails.AQV_STATUS == 'O') {
            jsetupAssetDetails.AQV_STATUS = 'Out of Service'
        } else if (jsetupAssetDetails.AQV_STATUS == 'R') {
            jsetupAssetDetails.AQV_STATUS = 'Retired'
        } else if (jsetupAssetDetails.AQV_STATUS == 'T') {
            jsetupAssetDetails.AQV_STATUS = 'Transferred'
        } else if (jsetupAssetDetails.AQV_STATUS == 'U') {
            jsetupAssetDetails.AQV_STATUS = 'Undefined'
        }
        $("#PMINFO").show();
        if (jsetupAssetDetails.AQV_PMCOUNT == 0) {
            $("#PMINFO").hide();

        }

        if (jsetupAssetDetails.AQV_INSTALLATION_DATE == "01/01/1900" || jsetupAssetDetails.AQV_INSTALLATION_DATE == "null" || jsetupAssetDetails.AQV_INSTALLATION_DATE == "0001-01-01T00:00:00" || jsetupAssetDetails.AQV_INSTALLATION_DATE == "1900-01-01T00:00:00") {

            jsetupAssetDetails.AQV_INSTALLATION_DATE = "";
        }


        ah.LoadJSON(jsetupAssetDetails);
        srparts = jsonData.AM_PARTSREQUEST;

        $("#norecParts").hide();
        $("#norecLabour").hide();
        $("#QVWONOX").text(jsetupDetails.QVWONO);
        $(ah.config.id.woLabourInfoQV).show();
        $(ah.config.id.searchResultListQVParts).show();
        $(ah.config.id.assetWarrantyQV).show();

        var assetWarrantyList = jsonData.AM_ASSET_WARRANTY;

        $(ah.config.id.assetWarrantyQV + ' ' + ah.config.tag.tbody).html('');
        var htmlstrWR = ""
        if (assetWarrantyList.length > 0) {


            for (var o in assetWarrantyList) {
                var warTerm = assetWarrantyList[o].FREQUENCY_PERIOD + ' ' + assetWarrantyList[o].FREQUENCY;
                if (assetWarrantyList[o].WARRANTYDESC == null) {
                    assetWarrantyList[o].WARRANTYDESC = "";
                }

                htmlstrWR += "<tr>";
                htmlstrWR += '<td class="">' + ah.formatJSONDateToString(assetWarrantyList[o].WARRANTY_EXPIRY) + '</td>';
                htmlstrWR += '<td class="">' + assetWarrantyList[o].WARRANTYDESC + '</td>';
                htmlstrWR += '<td class="">' + warTerm + '</td>';
                htmlstrWR += '<td class="">' + assetWarrantyList[o].WARRANTY_NOTES + '</td>';

                htmlstrWR += "</tr>";
            }

            $(ah.config.id.assetWarrantyQV + ' ' + ah.config.tag.tbody).append(htmlstrWR);
        } else {
            $(ah.config.id.assetWarrantyQV).hide();

        }

        var woLabourList = jsonData.WORK_ORDER_LABOUR;
        $(ah.config.id.woLabourInfoQV + ' ' + ah.config.tag.tbody).html('');
        var htmlstrLB = ""

        
        if (woLabourList.length > 0) {

            for (var o in woLabourList) {



                htmlstrLB += "<tr>";
                htmlstrLB += '<td class="h">' + woLabourList[o].EMPLOYEE + '</td>';
                htmlstrLB += '<td class="a">' + ah.formatJSONDateTimeToString(woLabourList[o].LABOUR_DATE) + '</td>';

                htmlstrLB += "</tr>";
                htmlstrLB += "<tr>";
                htmlstrLB += '<td style="width:100%;" align="left" colspan="4"><strong>Action Details</strong></td>';
                htmlstrLB += "</tr>";
                htmlstrLB += "<tr>";
                htmlstrLB += '<td style="border-top:none;border-bottom:none; width:100%;" colspan="4">' + woLabourList[o].LABOUR_ACTION + '</td>';
                htmlstrLB += "</tr>";

            }


            $(ah.config.id.woLabourInfoQV + ' ' + ah.config.tag.tbody).append(htmlstrLB);
        } else {
            $(ah.config.id.woLabourInfoQV).hide();
            $("#norecLabour").show();
        }



        $(ah.config.id.searchResultListQVParts + ' ' + ah.config.tag.tbody).html('');
        var srparts = jsonData.AM_PARTSREQUEST;
        var htmlstr = ""

        if (srparts.length > 0) {
            sparePartsCount = 1;
            $(ah.config.id.searchResultListParts).show();
            for (var o in srparts) {

                var billable = 'Yes';
                if (srparts[o].BILLABLE === "false") {
                    billable = 'No';
                }
                if (srparts[o].PRODUCT_DESC === null || srparts[o].PRODUCT_DESC === "") {
                    srparts[o].PRODUCT_DESC = srparts[o].DESCRIPTION;
                    srparts[o].ID_PARTS = 'Supplier'
                }
                if (srparts[o].USED_QTY == null) {
                    srparts[o].USED_QTY = 0;
                }

                htmlstr += "<tr>";
                htmlstr += '<td class="a">' + srparts[o].ID_PARTS + '</td>';
                htmlstr += '<td class="">' + srparts[o].PART_NUMBERS + '</td>';
                htmlstr += '<td class="h">' + srparts[o].PRODUCT_DESC + '</td>';
                htmlstr += '<td class="a">' + srparts[o].STATUS + '</td>';
                htmlstr += '<td class="a">' + srparts[o].QTY_RETURN + '</td>';
                htmlstr += '<td class="a">' + srparts[o].USED_QTY + '</td>';
                htmlstr += '<td class="a">' + srparts[o].TRANSFER_QTY + '</td>';
                htmlstr += '<td class="a">' + srparts[o].QTY + '</td>';
                htmlstr += '<td class="a">' + srparts[o].PRICE + '</td>';
                htmlstr += '<td class="a">' + srparts[o].EXTENDED + '</td>';
                htmlstr += '<td class="a">' + billable + '</td>';
                htmlstr += "</tr>";
            }

            $(ah.config.id.searchResultListQVParts + ' ' + ah.config.tag.tbody).append(htmlstr);
        } else {
            $(ah.config.id.searchResultListQVParts).hide();
            $("#norecParts").show();
        }
        
        window.location.href = ah.config.url.openModalQuickView;
    }
    self.populateHRD = function (statusAction, jsonData) {
        var hdr = srDataArray;
        $(ah.config.id.searchResultHDRList + ' ' + ah.config.tag.ul).html('');
        if (hdr.length > 0) {
            //alert(JSON.stringify(hdr));

            var htmlstr = '';
            //var openPendingCount = 0;
            //var completedCount = 0;
            //var cancelledCount = 0;
            //var inserviceAssetCount = 0;
            for (var o in hdr) {
               
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplateList, hdr[o].TICKET_ID, hdr[o].TICKET_ID, hdr[o].ISSUE_DETAILS, '');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateList, systemText.searchModeLabels.ticketCreatedDate, ah.formatJSONDateTimeToString(hdr[o].ISSUE_DATE));
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateList, systemText.searchModeLabels.asset, hdr[o].ASSET_NO + "-" + hdr[o].ASSET_DESC);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateList, systemText.searchModeLabels.contactPerson, hdr[o].CONTACT_PERSON);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateList, systemText.searchModeLabels.contactNo, hdr[o].CONTACT_NO);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateList, systemText.searchModeLabels.costcenter, hdr[o].COSTCENTER_DESC);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateList, systemText.searchModeLabels.location, hdr[o].LOCATION);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateList, systemText.searchModeLabels.status, hdr[o].STATUS);

                htmlstr += '</li>';
            }
            $.ajax({
                url: '/Languange/getLanguange',
                type: "GET",
                async: false,
                contentType: false,
                processData: false,
                data: {
                    localize: ""
                },
                success: function (data) {
                    if (data === 'ar') {
                        $('#openIssue').text('طلبات العمل الغير مغلقة ' + jsonData.REQUESTERPENDING + '');
                        $('#completedIssue').text('طلبات العمل المغلقة ' + jsonData.REQUESTERCOMPLETED + '');
                        $('#cancelledIssue').text('طلبات عمل ملغاة ' + jsonData.REQUESTERCANCELLED + '');
                        //if (jsonData.REQUESTERPENDING > 0) { $('#searchHeaderText').text('طلبات غير مغلقة'); }
                        //if (jsonData.REQUESTERCOMPLETED > 0) { $('#searchHeaderText').text('طلبات العمل المغلقة'); }
                        //if (jsonData.jsonData.REQUESTERCANCELLED > 0) { $('#searchHeaderText').text('طلبات عمل ملغاة'); }
                    }
                    else {
                        $('#openIssue').text('Open/Pending/OnHold Issue ' + jsonData.REQUESTERPENDING + '');
                        $('#completedIssue').text('Completed Issue ' + jsonData.REQUESTERCOMPLETED + '');
                        $('#cancelledIssue').text('Cancelled Issue ' + jsonData.REQUESTERCANCELLED + '');
                        //if (jsonData.REQUESTERPENDING > 0) { $('#searchHeaderText').text('Pending Work order'); }
                        //if (jsonData.REQUESTERCOMPLETED > 0) { $('#searchHeaderText').text('Closed Work Order'); }
                        //if (jsonData.jsonData.REQUESTERCANCELLED > 0) { $('#searchHeaderText').text('Cancelled Request'); }

                    }
                },
                error: function (err) {

                }
            });


            $(ah.config.id.searchResultHDRList + ' ' + ah.config.tag.ul).append(htmlstr);
        }
        $(ah.config.cls.detailItem).bind('click', self.hdrInfo);
    };
    self.changeLanguange = function () {
        var myvar = 'en';
        var frmData = new FormData();
        frmData.append("languange", myvar);
        $.ajax({
            url: '/Languange/changeLanguange',
            type: "POST",
            async: false,
            contentType: false,
            processData: false,  
            data: {
                localize: myvar
            },
            success: function (data) {
               
               
                var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
                if (staffDetails.HOME_DEFAULT != null && staffDetails.HOME_DEFAULT != "") {

                    window.location.href = staffDetails.HOME_DEFAULT;
                } else {

                    window.location.href = ah.config.url.menuApps;
                }
                if (data === 'ar') {
                    $("#languangeId").text("العربية");
                } else {
                    $("#languangeId").text("English");
                }
            },
            error: function (err) {
               
            }
        });
    };
    self.scanQR = function (fn) {
        window.location.href = ah.config.url.openModalQRCode;
        var myqr = document.getElementById('qrId')
        var lastResult,countResults = 0;

        function onScanSuccess(decodeText, decodeResult) {
            if (decodeText !== lastResult) {
                ++countResults;
                lastResult = decodeText;
                alert("your qr is: " + decodeText, decodeResult)
            }
        }
        var htmlscanner = new Html5QrcodeScanner('qrId', { fps: 10, qrbox: 250 })
        htmlscanner.render(onScanSuccess)
        //if (document.readyState === "complete" || document.readyState === "interactive") {
        //    setTimeout(fn, 1)
        //} else {
        //    document.addEventListener("DOMContentLoaded", fn)
        //}
    };
	self.validatePrivileges = function (privilegeId) {
		var groupPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));

		var groupPrivileges = groupPrivileges.filter(function (item) { return item.PRIVILEGES_ID === privilegeId });
		return groupPrivileges.length;
	};
    self.populateHRDNEW = function () {
		var hdr = srHDRArray;
		$(ah.config.id.searchResultHDRList + ' ' + ah.config.tag.ul).html('');
		$('#searchHeaderText').text('Help Desk (New Issue Assigned)');
        var hdr = srDataArray;

        //alert(JSON.stringify(hdr));
        $(ah.config.id.searchResultHDRList + ' ' + ah.config.tag.ul).html('');
        var allowedClose = 'NO';


        if (hdr.length > 0) {

            var htmlstr = '';

			for (var o in hdr) {
	
				htmlstr += '<li>';
				htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplateList, hdr[o].TICKET_ID, hdr[o].TICKET_ID, hdr[o].ISSUE_DETAILS, '');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateENGList, systemText.searchModeLabels.contactPerson, hdr[o].ISSUE_DATE);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateENGList, systemText.searchModeLabels.asset, hdr[o].ASSET_NO + "-" + hdr[o].ASSET_DESC);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateENGList, systemText.searchModeLabels.contactPerson, hdr[o].CONTACT_PERSON);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateENGList, systemText.searchModeLabels.contactNo, hdr[o].CONTACT_NO);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateENGList, systemText.searchModeLabels.location, hdr[o].LOCATION);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateENGList, systemText.searchModeLabels.status, hdr[o].STATUS);
               
				htmlstr += '</li>';



			}
	
			$(ah.config.id.searchResultHDRList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

		$(ah.config.cls.detailItem).bind('click', self.hdrInfo);
	};
	self.populateENGR = function (statusAction) {
        var hdr = srDataArray;
       
		//alert(JSON.stringify(hdr));
        $(ah.config.id.searchResultHDRList + ' ' + ah.config.tag.ul).html('');
        var allowedClose = 'NO';
        if (self.validatePrivileges('26') > 0) {
            allowedClose = "YES";

        };
        if (self.validatePrivileges('27') > 0) {
            allowedClose = "YESALL";

        };
		if (hdr.length > 0) {


			var htmlstr = '';
			var openPendingCount = 0;
			var completedCount = 0;
			var cancelledCount = 0;
			var inserviceAssetCount = 0;
            
            for (var o in hdr) {
               
				var statusDesc = "";
               
				//if (statusAction === 'PENDING') {

    //                if (hdr[o].WO_STATUS !== "NE" && hdr[o].WO_STATUS !== "OP" && hdr[o].WO_STATUS !== "PS" && hdr[o].WO_STATUS !== "FC") {
						
				//		continue;

				//	}
				//} else if (statusAction === 'HOLD') {
				//	if (hdr[o].WO_STATUS !== "HA") {
				//	continue;

				//	}
				//} else if (statusAction === 'COMPLETED') {
				//	if (hdr[o].WO_STATUS !== "CL") {

				//		continue;

				//	}
				//}
				var assetDesc = "";
				if (hdr[o].DESCRIPTION === null || hdr[o].DESCRIPTION === "") {
					assetDesc = "WorkSheet Inspection"
				} else {
					assetDesc = hdr[o].DESCRIPTION
                }

                if (hdr[o].WO_STATUS === 'CL') {
                    statusDesc = 'Closed';
                } else if (hdr[o].WO_STATUS === 'OP') {
                    statusDesc = 'Open';
                }
                else if (hdr[o].WO_STATUS === 'HA') {
                    statusDesc = 'On Hold';
                } else if (hdr[o].WO_STATUS === 'NE') {
                    statusDesc = 'New';
                } else if (hdr[o].WO_STATUS === 'PS') {
                    statusDesc = 'Posted';
                } else if (hdr[o].WO_STATUS === 'FC') {
                    statusDesc = 'For Close';
                }
				if (hdr[o].REQ_BY === null) {
					hdr[o].REQ_BY = '';
				}
                htmlstr += '<li>';
                htmlstr += '<span style="width:100%;">';
                htmlstr += '<span style="width:90%;" class="fc-slate-blue">';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplateENGR, hdr[o].REQ_NO, hdr[o].ASSET_NO, assetDesc, "");
               
                htmlstr += '</span>';
                htmlstr += '<span style="width:10%; text-align:right;" class="no-print">';
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateQuickView, hdr[o].REQ_NO, 'Quick View');
                htmlstr += '</span>';
                htmlstr += '</span>';

				htmlstr += ah.formatString(ah.config.html.searchRowTemplateENGR, systemText.searchModeLabels.woOrder, hdr[o].REQ_NO);
				htmlstr += ah.formatString(ah.config.html.searchRowTemplateENGR, systemText.searchModeLabels.problem, hdr[o].PROBLEM);
				htmlstr += ah.formatString(ah.config.html.searchRowTemplateENGR, systemText.searchModeLabels.requestor, hdr[o].REQ_BY);
				htmlstr += ah.formatString(ah.config.html.searchRowTemplateENGR, systemText.searchModeLabels.reqDate, ah.formatJSONDateTimeToString(hdr[o].REQ_DATE));
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateENGR, systemText.searchModeLabels.statusName, statusDesc);
			
				if (hdr[o].AM_INSPECTION_ID !== null && hdr[o].AM_INSPECTION_ID > 0) {
					htmlstr += ah.formatString(ah.config.html.searchRowTemplateENGR, systemText.searchModeLabels.workSheet, hdr[o].AM_INSPECTION_ID.toString() + '-' + hdr[o].INSPECTION_TYPE);
                }
                if (hdr[o].WO_STATUS !== "CL") {
                    if (allowedClose === "YES" && hdr[o].WO_STATUS === "FC") {
                        htmlstr += ah.formatString(ah.config.html.searchRowTemplateClose, hdr[o].REQ_NO, 'Close');
                    } else {
                        if (allowedClose === "YESALL") {
                            htmlstr += ah.formatString(ah.config.html.searchRowTemplateClose, hdr[o].REQ_NO, 'Close');
                        }
                    }

                    htmlstr += ah.formatString(ah.config.html.searchRowTemplateModify, hdr[o].REQ_NO, 'Modify');
                }
                
                
				htmlstr += '</li>';



            }
            if (srWOEngrCount.length > 0) {
                openPendingCount = srWOEngrCount[0].NEWOPENSTATUS;
                completedCount = srWOEngrCount[0].CLOSESTATUS;
                cancelledCount = srWOEngrCount[0].HOLDSTATUS;
            }
            // Start
            $.ajax({
                url: '/Languange/getLanguange',
                type: "GET",
                async: false,
                contentType: false,
                processData: false,
                data: {
                    localize: ""
                },
                success: function (data) {
                    if (data === 'ar') {
                    $('#openIssue').text('طلبات العمل المفتوحة ' + openPendingCount + '');
                    $('#completedIssue').text('طلبات العمل المغلقة ' + completedCount + '');
                    $('#cancelledIssue').text('طلبات عمل في الانتظار ' + cancelledCount + '');
                    }
                    else
                    {
                        $('#openIssue').text('Pending Work Order ' + openPendingCount + '');
                        $('#completedIssue').text('Closed Work Order ' + completedCount + '');
                        $('#cancelledIssue').text('On-Hold Work Order ' + cancelledCount + '');
                    }
                },
                error: function (err) {

                }
            });
            // End
			$(ah.config.id.searchResultHDRList + ' ' + ah.config.tag.ul).append(htmlstr);
		}
        $(ah.config.cls.detailItem).bind('click', self.woReqInfo);
        $(ah.config.cls.detailrequest).bind('click', self.readquickview);
        $(ah.config.cls.detailClose).bind('click', self.woReqCloseInfo);
    };
    self.readquickview = function () {
        
        var reqID = this.getAttribute(ah.config.attr.datainfoId).toString();


        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        postData = { REQ_NO: reqID, STAFF_LOGON_SESSIONS: staffLogonSessions, SEARCH_FILTER: self.searchFilter, STATUS: "" };
        $.post(self.getApi() + ah.config.api.searchWOQuickView, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    }
	self.hdrInfo = function () {

		var ticketId = this.getAttribute(ah.config.attr.datainfoId).toString();
	
		window.location.href = ah.config.url.hdrInfo + "?TICKETNO=" + ticketId;
	};
	self.woReqInfo = function () {
		var reqID = this.getAttribute(ah.config.attr.datainfoId).toString();		
		window.location.href = ah.config.url.aimsWorkOrder + "?REQNO=" + reqID + "&ASSETNO=" + '' + "&REQWOAC=sdfsdfxdfwefsd";
    }
    self.woReqCloseInfo = function () {
        var reqID = this.getAttribute(ah.config.attr.datainfoId).toString();
        window.location.href = ah.config.url.aimsWorkOrderClose + "?REQNO=" + reqID + "&ASSETNO=" + '' + "&REQWOAC=sdfsdfxdfwefsd" +"&asdfxhsadfsdfx=dsdxsesesdfsadfsadfqwerqwerqwersadfsadfasdfasdfsadfsadfsadfasdf";
    }
    self.readModuleId = function () {
        var moduleID = this.getAttribute(ah.config.attr.datamoduleID).toString();
        var res = moduleID.substring(4);
        $("#searchMenu ul ul").slideUp();
        document.getElementById(res).removeAttribute("style");
        document.getElementById(res).setAttribute("style", "display: block;");
    };
    self.readMobileId = function () {
        var moduleID = this.getAttribute(ah.config.attr.datamoduleID).toString();
        var res = moduleID.substring(4);
        $("#myLinks ul ul").slideUp();
        document.getElementById(res).removeAttribute("style");
        document.getElementById("mob-" + res).setAttribute("style", "display: block;");
    };
    self.readModuleFunctionByModuleFunctionID = function () {

        var submenu = JSON.parse(this.getAttribute(ah.config.attr.dataurlID).toString()); //JSON.parse(this.getAttribute(ah.config.attr.dataurlID));/ /
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
       
        //alert(webApiURL);
       // window.location.href = ubmenu.URL;
        //submenuUrl = submenu.URL;
        //var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        //postData = { ATTRIBUTEID: submenu.ATTRIBUTE_ID, SCREENID: submenu.DESCRIPTION, STAFF_LOGON_SESSIONS: staffLogonSessions };
        //$.when($.post(self.getApi() + ah.config.api.insertUserAction, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus)).done(function () {
         window.location.href = submenu.URL;
        // });

    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL+"api";
        } else {
            api = staffDetails.API + "api";
        }
        
        return api
    };
    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        ah.toggleSearchMode();       
        var searchText = "";
		DashBoard = staffDetails.HOME_DEFAULT;
        self.searchFilter.notifyStaffId = staffDetails.STAFF_ID;
              
		if (DashBoard === "/Menuapps/BSTaimsIndex")
		{
			postData = { SEARCH: "", SYSTEMID: searchText, MENU_GROUP: staffDetails.STAFF_GROUP_ID, STAFF_LOGON_SESSIONS: staffLogonSessions };
			$.post(self.getApi() + ah.config.api.searchAllNoDashBoard, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus); 
		} else if (DashBoard === "/Menuapps/HDRaimsIndex") {
			if (self.validatePrivileges('8') === 0) {
				self.searchFilter.staffId = staffDetails.STAFF_ID;
			};
            postData = { SEARCH_FILTER: self.searchFilter, SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), SYSTEMID: searchText, MENU_GROUP: staffDetails.STAFF_GROUP_ID, STAFF_LOGON_SESSIONS: staffLogonSessions };
			$.post(self.getApi() + ah.config.api.searchAllHDRDashBoard, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus); 
			
        } else if (DashBoard === "/Menuapps/ENGRaimsIndex") {
            if (self.validatePrivileges('8') === 0) {
                self.searchFilter.staffId = staffDetails.STAFF_ID;
                if (self.searchFilter.Status == "" || self.searchFilter.Status == null) {
                    self.searchFilter.Status = "PSOPFC";
                }
            };
            postData = { SEARCH_FILTER: self.searchFilter, SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), SYSTEMID: searchText, MENU_GROUP: staffDetails.STAFF_GROUP_ID, STAFF_LOGON_SESSIONS: staffLogonSessions };
			$.post(self.getApi() + ah.config.api.searchAllENGRDashBoard, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus); 
        } else if (DashBoard === "/Menuapps/CSTaimsIndex") {
            if (self.validatePrivileges('8') === 0) {
                self.searchFilter.staffId = staffDetails.STAFF_ID;
                self.searchFilter.clientCode = self.clientInfo.clientCode;
            };
            postData = { SEARCH_FILTER: self.searchFilter, SEARCHTRANS: $(ah.config.id.txtGlobalSearch).val().toString(), SEARCH: "", SYSTEMID: searchText, MENU_GROUP: staffDetails.STAFF_GROUP_ID, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchAllCSTDashBoard, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

        } else {
   
            postData = { SEARCH_FILTER: self.searchFilter, SEARCHTRANS: $(ah.config.id.txtGlobalSearch).val().toString(), SEARCH: "", SYSTEMID: searchText, MENU_GROUP: staffDetails.STAFF_GROUP_ID, STAFF_LOGON_SESSIONS: staffLogonSessions };
			$.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus); 
		}

		//
    };
   
    self.FilterPm = function () {
        // preventive maintenance
        self.searchFilter.ProblemType = "PM";
        self.searchNow();
        self.searchFilter.ProblemType = "";
    };

    self.FilterCm = function () {
        // corrective maintenance
        self.searchFilter.ProblemType = "CM";
        self.searchNow();
        self.searchFilter.ProblemType = "";
    };

    self.FilterRt = function () {
        // routine check
        self.searchFilter.ProblemType = "IN";
        self.searchNow();
        self.searchFilter.ProblemType = "";
    };

    self.FilterOth = function () {
        // Others Work Order
        self.searchFilter.ProblemType = "Oth";
        self.searchNow();
        self.searchFilter.ProblemType = "";
    };

    self.updatenewPassword = function () {
        var nPsswrd = $("#NEW_PASSWORD").val();
        var rPsswrd = $("#RE_PASSWORD").val();
        var userid = $("#CUR_USERID").val();
        var curpassword = $("#CUR_PASSWORD").val();

        if (userid === null || userid === "") {
            alert('Unable to proceed. Please provide your user id');
            return;
        }

        if (curpassword === null || curpassword === "") {
            alert('Unable to proceed. Please provide your current password');
            return;
        }

        if (nPsswrd === null || nPsswrd === "") {
            alert('Unable to proceed. Please make sure that the new password entered.');
            return;
        }
        if (rPsswrd === null || rPsswrd === "") {
            alert('Unable to proceed. Please make sure that the re-enter password entered.');
            return;
        }

        if (nPsswrd !== rPsswrd) {
            alert('Unable to proceed. Please make sure re-enter password is the same as new password.');
            return;
        }

        if (nPsswrd.length < 6) {
            alert('Unable to proceed. Please make sure your new password should no be less than 6 digit/character.');
            return;
        }

        self.searchFilter.userid = userid;
        self.searchFilter.curpassword = curpassword;
        self.searchFilter.newpassword = nPsswrd;
       // self.ERROR_MESSAGE(systemText.pwLogin);
        //var logonCredentials = ah.ConvertFormToJSON($(ah.config.id.frmInfoPM)[0]);
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        postData = { MOREXDK: 'asdfasdfxsde', SEARCH_FILTER: self.searchFilter };
        // window.location.href = ah.config.url.homeIndex;
        $.post(self.getApi() + ah.config.api.updatechangepw, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus); 
        //$.post(webApiURL + ah.config.api.updatechangepw, postData).done(self.logonResult).fail(self.logonResult);

    }

    self.webApiCallbackStatus = function (jsonData) {
       // alert(JSON.stringify(jsonData.UPDATEPMDONE));
        if (jsonData.ERROR) {
       

            if (jsonData.ERROR.ErrorText === 'User not found.' || jsonData.ERROR.ErrorText === 'Wrong Password not found.') {
                $("#validInfo").hide();
                $("#invalidInfo").show();
            } else {
              alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            }
            //.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.SESSIONOK) {

            self.searchNow();
        }
        
        else if (jsonData.PASSWORDUPDATED) {
           
            window.location.href = ah.config.url.homePasswordChange;
            $("#invalidInfo").hide();
            $("#validInfo").show();
        }
        else if (jsonData.PMDONE_UPDATED) {  
            alert(JSON.stringify(jsonData.PMDONE_UPDATED));
        }
        else if (jsonData.CLIENTIPHOST) {
            
        }
        else if (jsonData.TOKEN) {
            window.location.href = submenuUrl;
        }
        else if (jsonData.DASHBOARD_CLIENT || jsonData.WORK_ORDER_QUICKVIEW_V || jsonData.NOTIFICATION_LIST || jsonData.NOTIFYCOUNT || jsonData.WORK_ORDERLIST || jsonData.ASSET_COUNT || jsonData.AM_HELPDESKLIST || jsonData.DASHBOARD_V || jsonData.MODULE_FUNCTION_ID || jsonData.MODULE_ACCESS_ATTRIBUTE || jsonData.MODULE_FUNCTIONS || jsonData.MODULES || jsonData.SUCCESS || jsonData.MODULE_FUNCTIONS || jsonData.SYSTEM_TRANSACTION) {
            
            

			if (ah.CurrentMode == ah.config.mode.search) {
				if (DashBoard === "/Menuapps/HDRaimsIndex") {
					srDataArray = jsonData.AM_HELPDESKLIST;
					self.searchHDRResult('PENDING',jsonData);
                } else if (DashBoard === "/Menuapps/ENGRaimsIndex") {
                    if (jsonData.NOTIFICATION_LIST) {
                        self.searchNotificationResult(jsonData);
                    } else {
                        srDataArray = jsonData.WORK_ORDERLIST;
                        srHDRArray = jsonData.AM_HELPDESKLIST;
                        srWOEngrCount = jsonData.WORKORDERCOUNT;
                        self.searchENGRResult('PENDING', jsonData);
                    }
					
					
                } else if (jsonData.NOTIFICATION_LIST) {
                    
                    self.searchNotificationResult(jsonData);
                } else if (DashBoard === "/Menuapps/CSTaimsIndex") {
                    srDataArray = jsonData.AM_HELPDESKLIST;
                    self.searchCSTResult('PENDING', jsonData);
                }else {
					self.searchResult(jsonData);
				}
                
            }
            else if (ah.CurrentMode == ah.config.mode.searchResult) {
              
                if (jsonData.WORK_ORDER_QUICKVIEW_V) {
                    self.showQuickView(jsonData);
                } else if (jsonData.NOTIFICATION_LIST){
                    self.searchNotificationResult(jsonData);
                }
               
                
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {
                $.each(jsonData.MODULES, function (item) {
                    $('#MODULE_ID')
                        .append($("<option></option>")
                        .attr("value", jsonData.MODULES[item].MODULE_ID)
                        .text(jsonData.MODULES[item].DESCRIPTION));
                });
             
               


            }

        }
        else {

          //  alert(systemText.errorTwo);
           // window.location.href = ah.config.url.homeIndex;
        }

    };
    self.OpenMenu = function () {
        var x = document.getElementById("myLinks");
        if (x.style.display === "flex") {
            x.style.display = "none";
        } else {
            x.style.display = "flex";
        }
    }
    self.initialize();

};
