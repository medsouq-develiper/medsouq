﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            statusText: '.status-text',
            detailItem: '.detailItem',
            liPrint: '.liPrint'
        },
        tag: {

            textArea: 'textarea',
            select: 'select',
            tbody: 'tbody',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7,
            print: 8,
            advanceSearch: 9
        },
        tagId: {
            tcCode: 'tcCode',
            tcInService: 'tcInService',
            tcDescription: 'tcDescription',
            tcActive: 'tcActive',
            tcMissing: 'tcMissing',
            tcInActive: 'tcInActive',
            tcOtService: 'tcOtService',
            tcRetired: 'tcRetired',
            tcTransfer: 'tcTransfer',
            tcUndifined: 'tcUndifined'
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            searchResultSummaryCount: "#searchResultSummaryCount",
            txtGlobalSearch: '#txtGlobalSearch',
            staffID: '#STAFF_ID',
            serviceID: '#SERVICE_ID',
            successMessage: '#successMessage',
            printSummary: '#printSummary',
            costcenterStrList: '#costcenterStrList',
            manufacturerStrList: '#manufacturerStrList',
            supplierStrList: '#supplierStrList',
            printSearchResultList: "#printSearchResultList",
            loadingBackground: '#loadingBackground'
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'AM_ASSETSUMMARY',

        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalClose: '#',
            modalFilter: '#openModalFilter'
        },
        api: {
            searchAll: '/Reports/SearchPMScheduleRept',
            readClientInfo: '/Setup/ReadClientInfo',
            searchLov: '/AIMS/SearchAimsLOV',
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liPrint + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave).hide();

    };

    thisApp.ConvertFormToJSON = function (form) {
        $(form).find("input:disabled").removeAttr("disabled");
        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name),
                type = $el.attr('type');

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var AssetPMScheduleReptViewModel = function (systemText) {

    var self = this;

    var ah = new appHelper(systemText);

    var lovCostCenterArray = ko.observableArray([]);
    var lovManufacturerArray = ko.observableArray([]);
    var lovSupplierArray = ko.observableArray([]);

    self.assetPMSchedule = {
        count: ko.observable()
    };
    self.clientDetails = {
        CLIENTNAME: ko.observable(),
        CLIENT_LOGO: ko.observable(),
        COMPANY_LOGO: ko.observable(),
        HEADING: ko.observable(),
        HEADING2: ko.observable(),
        HEADING3: ko.observable(),
        HEADING_OTH: ko.observable(),
        HEADING_OTH2: ko.observable(),
        HEADING_OTH3: ko.observable(),
        CLIENTADDRESS: ko.observable(),
        CLIENTCONTACT: ko.observable()
    };
    self.searchFilter = {
        fromDate: ko.observable(null),
        toDate: ko.observable(null),
        Status: ko.observable(null),
        POStatus: ko.observable(null),
        interval: ko.observable(null),
        frequency: ko.observable(null),
        CostCenterStr: ko.observable(''),
        ManufacturerStr: ko.observable(''),
        SupplierStr: ko.observable(''),
        pmtype: ko.observable('')
    }
    self.showLoadingBackground = function () {
        var loadingBackground = $(ah.config.id.loadingBackground);
        loadingBackground.addClass('initializing').removeClass('done');
        var offset = loadingBackground.offset();
        var height = loadingBackground.height();
        var centerY = (offset.top + height / 2) - 120;
        $('.sk-circle').css({
            'margin-top': centerY + 'px',
            'display': 'block'
        });
    };
    self.getData = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
        var postData;

        postData = { LOV: "'AIMS_COSTCENTER'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        return $.post(self.getApi() + ah.config.api.searchLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());



        ah.initializeLayout();
        
        document.getElementById("pmfrequency").disabled = true;
        document.getElementById("pminterval").disabled = true;
        //$('#PMFREQUENCY').val(null);
        //$('#pminterval').val(null);
        var today = moment(new Date()).format("DD/MM/YYYY");
        $('#requestStartDate').val(today);

        self.showLoadingBackground();

        $.when(self.getData()).done(function () {
            $(ah.config.id.loadingBackground).removeClass('initializing').addClass('done');
            $('.dark-bg, ' + ah.config.id.loadingBackground).css({
                'top': '90px',
                'z-index': '0'
            });
        });
    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };

    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.createNew = function () {

        ah.toggleAddMode();

    };
    self.searchFilterNow = function () {
        window.location.href = ah.config.url.modalClose;
        self.searchNow();
    }
    self.showModalFilter = function () {
        ah.CurrentMode = ah.config.mode.advanceSearch;

        window.location.href = ah.config.url.modalFilter;
    };
    self.clearFilter = function () {
        self.searchFilter.pmtype('');
        self.searchFilter.CostCenterStr('');
        self.searchFilter.ManufacturerStr('');
        self.searchFilter.SupplierStr('');
        $('#filterStatus').val('');
        $('#filterPOStatus').val('');
        self.searchFilter.fromDate = '';
        self.searchFilter.toDate = '';
        self.searchFilter.Status = '';
        self.searchFilter.POStatus = '';
        self.searchFilter.frequency = '';
        self.searchFilter.interval = '';
        $('#pmfrequency').val('');
        $('#pminterval').val(''); 
        $('#requestFromDate').val('');
        $('#requestStartDate').val('');
        document.getElementById("requestFromDate").disabled = false;
        document.getElementById("requestStartDate").disabled = false;
        document.getElementById("pmfrequency").disabled = false;
        document.getElementById("pminterval").disabled = false;
    }
    self.modifySetup = function () {

        ah.toggleEditMode();

    };
    self.fromDateS = function () {
      
        $('#pmfrequency').val('');
        $('#pminterval').val('');
        document.getElementById("requestFromDate").disabled = false;
        document.getElementById("requestStartDate").disabled = false;
        document.getElementById("pmfrequency").disabled = true;
        document.getElementById("pminterval").disabled = true;
    };
    self.toDateS = function () {
        $('#pmfrequency').val('');
        $('#pminterval').val('');
        document.getElementById("requestFromDate").disabled = false;
        document.getElementById("requestStartDate").disabled = false;
        document.getElementById("pmfrequency").disabled = true;
        document.getElementById("pminterval").disabled = true;
    };
    self.frequencyS = function () {
    
        $('#requestFromDate').val('');
        $('#requestStartDate').val('');
        document.getElementById("requestFromDate").disabled = true;
        document.getElementById("requestStartDate").disabled = true;
        document.getElementById("pmfrequency").disabled = false;
        document.getElementById("pminterval").disabled = false;
    };
    self.periodS = function () {
     
        $('#requestFromDate').val('');
        $('#requestStartDate').val('');
        document.getElementById("requestFromDate").disabled = true;
        document.getElementById("requestStartDate").disabled = true;
        document.getElementById("pmfrequency").disabled = false;
        document.getElementById("pminterval").disabled = false;
    };
    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.printSummary = function () {
        ah.CurrentMode = ah.config.mode.print;
        document.getElementById("printSummary").classList.add('printSummary');
        var jsonObj = ko.observableArray();


        if (self.clientDetails.CLIENTNAME() != null) {
            self.print();

        } else {
            var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var postData;

            var staffInfo = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
            postData = { CLIENTID: staffInfo.CLIENT_CODE, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.readClientInfo, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
    };
    self.print = function () {

        setTimeout(function () {
            window.print();

        }, 2000);
    };
    
    self.sortDataTable = function (tableReference, sortColumnIndex, ascORdesc) {
        $(tableReference)
            .unbind('appendCache applyWidgetId applyWidgets sorton update updateCell')
            .removeClass('tablesorter')
            .find('thead th')
            .unbind('click mousedown')
            .removeClass('header headerSortDown headerSortUp');

        var sortedAt = 0;
        if (ascORdesc == "asc" || ascORdesc == "ASC") {
            sortedAt = 0;
        } else {
            sortedAt = 1;
        }

        $(tableReference).tablesorter({
            sortList: [[sortColumnIndex, sortedAt]],
            headers: {
                3: { sorter: "customDate" },
                8: { sorter: "customDate" }
            }
        });

        $('.not-sorted').removeClass('header').removeClass('headerSortUp').removeClass('headerSortDown');
    }
    self.searchResult = function (jsonData) {
        
        var sr = jsonData.PMSCHEDULE
      
        $(ah.config.id.searchResultList + ' ' + ah.config.tag.tbody).html('');
        $(ah.config.id.printSearchResultList).html('');

        var searchCat = $("#sumCategory").val();

        $(ah.config.id.searchResultSummary).text('PM Schedule By Due Date');
        $(ah.config.id.searchResultSummaryCount).text('Number of Records: ' + sr.length);
        self.assetPMSchedule.count('No. of Records: ' + sr.length);

        $(ah.config.cls.liPrint).hide();
        if (sr.length > 0) {
            $(ah.config.cls.liPrint).show();
            var htmlstr = '';

            for (var o in sr) {
                htmlstr += "<tr>"; 
               
                htmlstr += '<td class="d">' + sr[o].ASSET_NO + '</td>';
                htmlstr += '<td class="h">' + sr[o].DESCRIPTION + '</td>';
                htmlstr += '<td class="a">' + sr[o].PM_TYPE + '</td>';
                htmlstr += '<td class="a">' + sr[o].NEXT_PM_DUE + '</td>';
                htmlstr += '<td class="a">' + sr[o].FREQUENCY_PERIOD + '</td>';
                htmlstr += '<td class="a">' + sr[o].INTERVAL + '</td>';
                htmlstr += '<td class="a">' + sr[o].DAYCOUNT + '</td>';
                htmlstr += '<td class="a">' + sr[o].LAST_REQ_NO + '</td>';
                htmlstr += '<td class="a">' + sr[o].LAST_WOREQDATE + '</td>';
                htmlstr += "</tr>";
            }
            
            $(ah.config.id.searchResultList + ' ' + ah.config.tag.tbody).append(htmlstr);
            $(ah.config.id.printSearchResultList).html('');
            $(ah.config.id.printSearchResultList).append($(ah.config.id.searchResultList).html());

            var tableReference = ah.config.id.searchResultList + ' table';
            self.sortDataTable(tableReference, 1, 'asc');
        }
        ah.displaySearchResult();

    };

    sortArray = function () {

    }

    self.readSetupID = function () {

        var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        //postData = { SUPPLIER_ID: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        //$.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.dateClick = function () {
        $("#dateClickAction").val("ACTIVE");
    };
    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();

        var fromDate = $('#requestFromDate').val().split('/');
     
        if (fromDate.length == 3) {
            self.searchFilter.fromDate = fromDate[2] + "/" + fromDate[1] + "/" + fromDate[0];
            
       
        } else {
            self.searchFilter.fromDate = null;

        }
        var toDate = $('#requestStartDate').val().split('/');
        if (toDate.length == 3) {
            self.searchFilter.toDate = toDate[2] + "/" + toDate[1] + "/" + toDate[0] + "  23:00:00.000";
        } else {
            self.searchFilter.toDate = null;
        }

        self.showLoadingBackground();

        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), SEARCH_FILTER: self.searchFilter, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.when($.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus)).done(function () {
            $(ah.config.id.loadingBackground).removeClass('initializing').addClass('done');
        });
    };


    self.webApiCallbackStatus = function (jsonData) {

        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));

        }
        else if (jsonData.AM_CLIENT_INFO || jsonData.PMSCHEDULE || jsonData.STAFF || jsonData.SUCCESS || jsonData.LOV_LOOKUPS || jsonData.AM_MANUFACTURER || jsonData.AM_SUPPLIER) {
            if (ah.CurrentMode == ah.config.mode.search) {
                self.searchResult(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {
                lovCostCenterArray = jsonData.LOV_LOOKUPS;
                $.each(lovCostCenterArray, function (item) {
                    $(ah.config.id.costcenterStrList)
                        .append($("<option>")
                            .attr("value", lovCostCenterArray[item].DESCRIPTION.trim())
                            .attr("id", lovCostCenterArray[item].LOV_LOOKUP_ID));
                });

                lovManufacturerArray = jsonData.AM_MANUFACTURER;
                $.each(lovManufacturerArray, function (item) {

                    $(ah.config.id.manufacturerStrList)
                        .append($("<option>")
                            .attr("value", lovManufacturerArray[item].DESCRIPTION.trim())
                            .attr("id", lovManufacturerArray[item].MANUFACTURER_ID));
                });

                lovSupplierArray = jsonData.AM_SUPPLIER;
                $.each(lovSupplierArray, function (item) {

                    $(ah.config.id.supplierStrList)
                        .append($("<option>")
                            .attr("value", lovSupplierArray[item].DESCRIPTION.trim())
                            .attr("id", lovSupplierArray[item].SUPPLIER_ID));
                });
            }
            else if (ah.CurrentMode == ah.config.mode.print) {

                var clientInfo = jsonData.AM_CLIENT_INFO;

                self.clientDetails.CLIENTNAME(clientInfo.DESCRIPTION);
                self.clientDetails.CLIENT_LOGO(clientInfo.CLIENT_LOGO);
                self.clientDetails.COMPANY_LOGO(clientInfo.COMPANY_LOGO);
                self.clientDetails.HEADING(clientInfo.HEADING);
                self.clientDetails.HEADING2(clientInfo.HEADING2);
                self.clientDetails.HEADING3(clientInfo.HEADING3);
                self.clientDetails.HEADING_OTH(clientInfo.HEADING_OTH);
                self.clientDetails.HEADING_OTH2(clientInfo.HEADING_OTH2);
                self.clientDetails.HEADING_OTH3(clientInfo.HEADING_OTH3);
                self.clientDetails.CLIENTADDRESS(clientInfo.ADDRESS + ', ' + clientInfo.CITY + ', ' + clientInfo.ZIPCODE + ', ' + clientInfo.STATE);
                self.clientDetails.CLIENTCONTACT('Tel.No: ' + clientInfo.CONTACT_NO1 + ' Fax No:' + clientInfo.CONTACT_NO2);

                self.print();
            }

        }
        else {

            alert(systemText.errorTwo);
            //	window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};