﻿var appHelper = function (systemText) {

    var thisApp = this;
    //
    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            detailItem: '.detailItem'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeNumber: 'input[type=number]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7,
            advanceSearch: 8,
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmInfo: '#frmInfo',
            staffID: '#STAFF_ID',
            successMessage: '#successMessage',
            searchResultListTV: '#SearchResultListTV',
            displayType: '#DISPLAYTYPE',
            searchTransTableTV: '#searchTransTableTV',
            costcenterStrList: '#costcenterStrList',
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'searchResult',
            infoDetails: 'AM_SUPPLIER',
            userClient: 'USER_CLIENT'
        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalFilter: '#openModalFilter',
            modalClose: '#'
        },
        fld: {
            fromDashBoard: 'recall',
           
        },
        api: {

            readReport: '/Setup/ReadSysTransReport',
            searchAll: '/Reports/SearchSystemTransactionReporting',
            createSysTransReport: '/Setup/CreateSysTransReport',
            updateSysTransReport: '/Setup/UpdateSysTransReport',
            updateSetup: '/AIMS/UpdatePMProcedureInfo',
            searchLov: '/AIMS/SearchAimsLOV',
            searchSetupLov: '/Setup/SearchInventoryLOV',
            getSysTransData: '/Reports/GetSysTransData',
            getItemBatchesVByReport: '/Inventory/GetItemBatchesVByReport',
            getAssetVByReport: '/Aims/GetAssetVByReport',
            acknowledgeSysReport: '/Reports/AcknowledgeSysReport',
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
            searchRowTemplateRed: "<span class='search-row-label'>{0}: <span class='fc-red'>{1}</span></span>",
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        //thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.liApiStatus
        ).hide();


        //$(
        //    thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
        //    thisApp.config.tag.textArea
        //).removeClass(thisApp.config.cssCls.viewMode);

        //$(
        //    thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        //).prop(thisApp.config.attr.disabled, false);

        //$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
     

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("PROCEDURE_CODE").disabled = true;

    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleAddExistMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;


        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.searhButton).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.statusText +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.iconDet + ',' + thisApp.config.cls.divProgress
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
    };

    thisApp.toggleDisplayMode = function () {
        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {
        $(form).find("input:disabled").removeAttr("disabled");
        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.getAge = function (birthDate) {

        var seconds = Math.abs(new Date() - birthDate) / 1000;

        var numyears = Math.floor(seconds / 31536000);
        var numdays = Math.floor((seconds % 31536000) / 86400);
        var nummonths = numdays / 30;

        return numyears + " y " + Math.round(nummonths) + " m ";
    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };
    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name),
                type = $el.attr('type');

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var sysTransactionReportingViewModel = function (systemText) {

    var self = this;

    var ah = new appHelper(systemText);
    var searchDisplayBy = null;
    var imageExtensions = new RegExp("(.*?)\.(BMP|bmp|gif|jpg|png|pdf|txt)$");
    var tifExtensions = new RegExp("(.*?)\.(TIF|TIFF|tif|tiff)$");
    var officeExtensions = new RegExp("(.*?)\.(doc|docx|dot|csv|xls|xlsx|xlw|pps|ppsx|ppt|pptx)$");
    var unsupportedExtension = new RegExp("(.*?)\.(dwg|DWG)$");
    self.selectedSystemTransaction = ko.observable({});
    self.clientInfo = {
        Description: ko.observable('Biomedical Management System'),
        clientLogo: ko.observable('../images/ClientLogo.png'),
        buildingCode: ko.observable(""),
        isVisible: ko.observable(false)
    };
    self.recallBys = ko.observableArray([]);
    self.recallTypes = ko.observableArray([]);
    self.selectedTransReport = {
        SEARCHTYPE: ko.observable(''),
        searchKeyText: ko.observable(''),
        SEARCHKEY: ko.observable(''),
        ID: ko.observable(''),
        RECALLCATEGORY: ko.observable(''),
        RECALLFIRMREFNO: ko.observable(''),
        RECALLTYPE: ko.observable(''),
        RECALLTIMEFRAME: ko.observable(''),
        RESPONSETIMEFRAME: ko.observable(''),
        PROBABILITY: ko.observable(''),
        DETECTABILITY: ko.observable(''),
        DDSA: ko.observable(''),
        QSA: ko.observable(''),
        HHLEVEL: ko.observable(''),
        REASON: ko.observable(''),
        REPORTSTATUS: ko.observable(''),
        REPORTEDBY: ko.observable(''),
        REPORTEDDATE: ko.observable(''),
        REPORTEDDATEDISPLAY: ko.observable(''),
        CLOSEBY: ko.observable(''),
        CLOSEDATE: ko.observable(''),
        CLOSEDATEDISPLAY: ko.observable(''),
        ACTIONBY: ko.observable(''),
        ACTIONDATE: ko.observable(''),
        ACTIONDATEDISPLAY: ko.observable(''),
        REPORTS: ko.observable(''),
        ACTIONREPORTS: ko.observable(''),
        CLOSEREPORTS: ko.observable(''),
        ACTION: ko.observable(''),
        ACKNOWLEDGEFLAG: ko.observable(false),
        ACKNOWLEDGEFLAG1: ko.observable(false),
        ACKNOWLEDGEFLAG2: ko.observable(false),
        ACKNOWLEDGEFLAG3: ko.observable(false),
    };
    self.transactionReport = {
        RECALLCATEGORY: ko.observable(''),
        RECALLTYPE: ko.observable(''),
        REPORTEDBY: ko.observable(''),
        REPORTEDDATE: ko.observable(''),
        REPORTEDDATEDISPLAY: ko.observable(''),
        APPROVEDBY: ko.observable(''),
        APPROVEDDATE: ko.observable(''),
        APPROVEDDATEDISPLAY: ko.observable(''),
        REPORTS: ko.observable(''),
        REPORTSTATUS: ko.observable(''),
        ACTION: ko.observable(''),
    };
    self.searchFilter = {
        fromDate: ko.observable(''),
        toDate: ko.observable(''),
        CostCenterStr: ko.observable(''),
        ItemName: ko.observable(''),
        Category: ko.observable(''),
        SerialBatch: ko.observable(''),
        ItemModel: ko.observable(''),
        TransType: ko.observable(''),
        Reporting: ko.observable('Y'),
        clientCode: ko.observable('')
    }


    self.scrollToViewPort = function (elementId) {
        document.getElementById(elementId).scrollIntoView(true);
    };
    self.documentCategories = ko.observableArray([]);
    self.typesOfAction = ko.observableArray([]);
    self.probabilities = ko.observableArray([]);
    self.detectabilities = ko.observableArray([]);
    self.DDSA = ko.observableArray([]);
    self.QSA = ko.observableArray([]);
    self.recallBys = ko.observableArray([]);
    self.recallTypes = ko.observableArray([]);
    self.HHLevels = ko.observableArray([]);
    self.uploadedListWithCategory = ko.observableArray([]);
    self.documentListWithCategory = ko.observableArray([]);
    self.documentList = ko.observableArray([]);
    self.uploadedList = ko.observableArray([]);
    self.uploadedFiles = [];

    self.transReportItems = ko.observableArray([]);
    self.setObservableValue = function (_object1, _object2) {
        Object.keys(_object2).forEach(function (key) {
            if (ko.isObservable(_object1[key]))
                _object1[key](_object2[key])
            else
                _object1[key] = _object2[key];
        });
    };
    self.getObservableValue = function (data) {
        if (Array.isArray(data)) {
            var returnData = [];
            for (var i = 0; i < data.length; i++) {
                var _object = data[i];
                returnData.push({});
                Object.keys(_object).forEach(function (key) {
                    if (ko.isObservable(_object[key]))
                        returnData[i][key] = _object[key]();
                    else
                        returnData[i][key] = _object[key];
                });
            }
        }
        else if (data instanceof Object) {
            var returnData = {};
            var _object = data;
            Object.keys(_object).forEach(function (key) {
                if (ko.isObservable(_object[key]))
                    returnData[key] = _object[key]();
                else
                    returnData[key] = _object[key];
            });
        }
        else
            returnData = data;

        return returnData;
    };
    self.srDataArray = ko.observableArray([]);
    self.srDataArraySum = ko.observableArray([]);
    var lovCostCenterArray = ko.observableArray([]);
    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());
        var userClientList = JSON.parse(sessionStorage.getItem(ah.config.skey.userClient));
        if (userClientList.length == 1) {
            self.clientInfo.Description = userClientList[0].Description;
            self.clientInfo.clientLogo = userClientList[0].LogoPath;
            self.clientInfo.isVisible = true;
            self.searchFilter.clientCode(userClientList[0].CLIENT_CODE);

        }
        ah.toggleReadMode();

        ah.initializeLayout();

        postData = { LOV: "'STOCKBATCH_STATUSTYPE','RECALLBY','HH-Level','RPT_ACTION','PROBABILITY','DETECTABILITY','DDSA','QSA','DOC_CATEGORY'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchSetupLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
   

    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.createNew = function () {

        ah.toggleAddMode();

    };

    self.modifyReport = function () {

        ah.toggleEditMode();

    };

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.saveAllChanges = function () {

        $(ah.config.id.saveInfo).trigger('click');

    };
    self.checkViewMode = function () {
        return false;
    }

    self.cancelChanges = function () {
        self.transactionReport.RECALLCATEGORY('');
        self.transactionReport.RECALLTYPE('');
        self.transactionReport.REPORTEDBY('');
        self.transactionReport.REPORTEDDATE('');
        self.transactionReport.REPORTEDDATEDISPLAY('');
        self.transactionReport.APPROVEDBY('');
        self.transactionReport.APPROVEDDATE('');
        self.transactionReport.APPROVEDDATEDISPLAY('');
        self.transactionReport.REPORTS('');
        self.transactionReport.REPORTSTATUS('');
        self.transactionReport.ACTION('');
        self.transactionReport.ID = 0;
      
        self.documentList([]);
        self.uploadedList([]);
        $('#uploadedInfo').val('');

        ah.initializeLayout();
    };
    self.clearFilter = function () {
        self.searchFilter.toDate('');
        self.searchFilter.fromDate('');
        $('#requestFromDate').val('');
        $('#requestStartDate').val('');

        self.searchFilter.CostCenterStr('');
        self.searchFilter.ItemName('');
        self.searchFilter.Category('');
        self.searchFilter.SerialBatch('');
        self.searchFilter.ItemModel('');
        self.searchFilter.TransType('');

    }
    self.showModalFilter = function () {
        ah.CurrentMode = ah.config.mode.advanceSearch;
        var element = document.getElementById("filterStatus");
        element.classList.remove("view-mode");
        document.getElementById("filterStatus").disabled = false;
        window.location.href = ah.config.url.modalFilter;
    };
    self.showDetails = function (isNotUpdate) {

        if (isNotUpdate) {

            var infoDetails = sessionStorage.getItem(ah.config.skey.infoDetails);

            var jsetupDetails = JSON.parse(infoDetails);

            ah.ResetControls();
            ah.LoadJSON(jsetupDetails);
            $("#PROCEDURE_CODE").val(jsetupDetails.PROCEDURE_ID);
        }

        ah.toggleDisplayMode();

    };

    self.searchResult = function () {
        ah.displaySearchResult();
        if (searchDisplayBy == "TV") {
            self.searchResultTV();
            return;
        }

        $(ah.config.id.searchResultList).show();
        $(ah.config.id.searchResultListTV).hide();
        var sr = self.srDataArraySum;

        var srden = self.srDataArray;


        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text("Company Transaction Report");

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {


                var newArray = srden.filter(function (el) {
                    return el.REFCODE == sr[o].REFCODE;
                });


                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].PROCEDURE_ID, sr[o].REFCODE, sr[o].REFDESCRIPTION, '');
                for (var i in newArray) {

                    if (newArray[i].SYSTRANSID == "11" || newArray[i].SYSTRANSID == "9") {
                        htmlstr += '<p>';
                        htmlstr += ah.formatString(ah.config.html.searchRowTemplateRed, systemText.searchModeLabels.status, newArray[i].MESSAGE);
                        htmlstr += '</p>';
                    } else {
                        htmlstr += '<p>';
                        htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.status, newArray[i].MESSAGE);
                        htmlstr += '</p>';
                    }
                }
                htmlstr += '</li>';
            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }



    };
    self.searchResultTV = function (jsonData) {
        $(ah.config.id.searchResultListTV).show();
        $(ah.config.id.searchResultList).hide();
        var table;
        var srden = self.srDataArray;

        if ($.fn.dataTable.isDataTable(ah.config.id.searchTransTableTV)) {
            $(ah.config.id.searchTransTableTV).DataTable().clear().destroy();
        }
        table = $(ah.config.id.searchTransTableTV).DataTable({
            data: srden,
            createdRow: function (row, data, dataIndex) {
                $(row).find('.btnViewReport').on('click', function (event) {
                    event.preventDefault();

                    var item = table.row($(this).parents('tr')).data();
                    self.setSelectedSysTransReport({ SYSREPORTID: item.SYSREPORTID, SEARCHTYPE: item.REFTYPE });
                });
            },
            columns: [
                { data: 'SYSREPORTID' },
                { data: 'CREATEDDATE' },
                { data: 'REFCOSTCENTERDESCRIPTION' },
                { data: 'REFCODE' },
                { data: 'REFDESCRIPTION' },
                { data: 'REFTYPE' },
                { data: 'REFBATCHSERIALNO' },
                { data: 'REFQTY' },
                { data: 'REFUOM' },
                { data: 'REASONTYPE' },
                { data: 'MESSAGE' },
                { data: 'REPORTSTATUS' },
                {
                    data: null,
                    "orderable": false,
                    render: function (data, type, row) {
                        return "<a href='#' class='button-ext btnViewReport' title='Click to view a report'><i class='fa fa-share-square-o'></i></a>";
                    }
                }
            ],
            "searching": false,
            //"order":true,
            "iDisplayLength": 15,
            "aLengthMenu": [15, 200, 6500, srden.length]
        });
    };
    self.setSelectedSysTransReport = function (item) {
        ah.toggleReadMode();

        self.selectedTransReport.SEARCHTYPE('');
        self.selectedTransReport.searchKeyText('');
        self.selectedTransReport.SEARCHKEY('');
        self.selectedTransReport.ID('');
        self.selectedTransReport.RECALLCATEGORY('');
        self.selectedTransReport.RECALLFIRMREFNO('');
        self.selectedTransReport.RECALLTYPE('');
        self.selectedTransReport.RECALLTIMEFRAME('');
        self.selectedTransReport.RESPONSETIMEFRAME('');
        self.selectedTransReport.PROBABILITY('');
        self.selectedTransReport.DETECTABILITY('');
        self.selectedTransReport.DDSA('');
        self.selectedTransReport.QSA('');
        self.selectedTransReport.HHLEVEL('');
        self.selectedTransReport.REASON('');
        self.selectedTransReport.REPORTSTATUS('For Reporting');
        self.selectedTransReport.REPORTEDBY('');
        self.selectedTransReport.REPORTEDDATE('');
        self.selectedTransReport.REPORTEDDATEDISPLAY('');
        self.selectedTransReport.CLOSEBY('');
        self.selectedTransReport.CLOSEDATE('');
        self.selectedTransReport.CLOSEDATEDISPLAY('');
        self.selectedTransReport.ACTIONBY('');
        self.selectedTransReport.ACTIONDATE('');
        self.selectedTransReport.ACTIONDATEDISPLAY('');
        self.selectedTransReport.REPORTS('');
        self.selectedTransReport.ACTIONREPORTS('');
        self.selectedTransReport.CLOSEREPORTS('');
        self.selectedTransReport.ACTION('');
        self.selectedTransReport.ACKNOWLEDGEFLAG(false);
        self.selectedTransReport.ACKNOWLEDGEFLAG1(false);
        self.selectedTransReport.ACKNOWLEDGEFLAG2(false);
        self.selectedTransReport.ACKNOWLEDGEFLAG3(false);

        var userClientList = JSON.parse(sessionStorage.getItem(ah.config.skey.userClient));
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData = { SYSREPORTID: item.SYSREPORTID, SEARCH_FILTER: { clientCode: userClientList && userClientList.length ? userClientList[0].CLIENT_CODE : "", SysReportId: item.SYSREPORTID }, STAFF_LOGON_SESSIONS: staffLogonSessions };

        if (item.SEARCHTYPE == 'Asset')
            $.post(self.getApi() + ah.config.api.getAssetVByReport, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        else
            $.post(self.getApi() + ah.config.api.getItemBatchesVByReport, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };

    self.dateClick = function () {
        $("#dateClickAction").val("ACTIVE");
    };

    self.searchFilterNow = function () {
        window.location.href = ah.config.url.modalClose;
        self.searchNow();
    }
    self.searchAdvanceLOV = function () {
        if (lovCostCenterArray.length == 0 || lovManufacturerArray.length == 0 || lovSupplierArray.length == 0) {
            var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
            var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
            var postData;
            postData = { LOV: "'AIMS_COSTCENTER'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
    };

    self.searchNow = function () {
        self.transactionReport.RECALLCATEGORY('');
        self.transactionReport.RECALLTYPE('');
        self.transactionReport.REPORTEDBY('');
        self.transactionReport.REPORTEDDATE('');
        self.transactionReport.REPORTEDDATEDISPLAY('');
        self.transactionReport.APPROVEDBY('');
        self.transactionReport.APPROVEDDATE('');
        self.transactionReport.APPROVEDDATEDISPLAY('');
        self.transactionReport.REPORTS('');
        self.transactionReport.REPORTSTATUS('');
        self.transactionReport.ACTION('');
        self.transactionReport.ID = 0;

        self.documentList([]);
        self.uploadedList([]);
        $('#uploadedInfo').val('');

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();

        searchDisplayBy = $(ah.config.id.displayType).val();

        var fromDate = $('#requestFromDate').val().split('/');

        if (fromDate.length == 3) {
            self.searchFilter.fromDate(fromDate[2] + "/" + fromDate[1] + "/" + fromDate[0]);
        }
        var toDate = $('#requestStartDate').val().split('/');
        if (toDate.length == 3) {
            self.searchFilter.toDate(toDate[2] + "/" + toDate[1] + "/" + toDate[0] + "  23:00:00.000");
        }


        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), SEARCH_FILTER: self.searchFilter, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };

    self.saveInfo = function () {
        ah.toggleSaveMode();
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        self.transactionReport.REPORTS(CKEDITOR.instances.REPORTS.getData());

        var SYSTEMTRANSACTION_REPORT = self.getObservableValue(self.transactionReport);
        SYSTEMTRANSACTION_REPORT.SysTransId = self.selectedSystemTransaction().SYSTRANSID;

        var postData = { SYSTEMTRANSACTION_REPORT: SYSTEMTRANSACTION_REPORT, SYSTEMTRANSACTION_REPORT_SCANDOCS: self.uploadedList(), STAFF_LOGON_SESSIONS: staffLogonSessions };
        if (SYSTEMTRANSACTION_REPORT.ID)
            $.post(self.getApi() + ah.config.api.updateSysTransReport, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        else
            $.post(self.getApi() + ah.config.api.createSysTransReport, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.acknowledge = function () {
        if (!self.selectedTransReport.ACKNOWLEDGEFLAG1() || !self.selectedTransReport.ACKNOWLEDGEFLAG2() || !self.selectedTransReport.ACKNOWLEDGEFLAG3())
            return alert('Please check all the acknowledgements.');

        ah.toggleSaveMode();

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var SYSTEMTRANSACTION_REPORT = self.getObservableValue(self.selectedTransReport);

        var postData = { SYSTEMTRANSACTION_REPORT: SYSTEMTRANSACTION_REPORT, TRANS_ITEMS: self.transReportItems(), STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.acknowledgeSysReport, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.checkAcknowledgement = function () {
        var userClientList = JSON.parse(sessionStorage.getItem(ah.config.skey.userClient));
        if (userClientList && userClientList.length && userClientList[0].CLIENT_CODE && (self.selectedTransReport.ID() || self.selectedTransReport.REPORTSTATUS() == 'For Reporting' || self.selectedTransReport.REPORTSTATUS() == 'For Action' || self.selectedTransReport.REPORTSTATUS() == 'For Closure'))
            return true;
        return false
    };

    self.webApiCallbackStatus = function (jsonData) {
        //  alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {
            if (jsonData.ERROR.ErrorText.includes("Duplicate")) {
                ah.toggleAddExistMode();

            } else if (jsonData.ERROR.ErrorText == "Report not found.") {
                self.createNew();
            }
            else {
                alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
               
            }

        }
        else if (jsonData.LOV_LOOKUPS || jsonData.TRANSACTION_DETAILS || jsonData.TRANSACTION_SUMMARY || jsonData.SUCCESS || jsonData.REPORT_DETAILS || jsonData.ITEM_BATCHES_V || jsonData.ASSET_V) {

            if (ah.CurrentMode == ah.config.mode.save) {
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();

                $(ah.config.id.contentHeader).html(systemText.displayModeHeader);
                $(ah.config.id.contentSubHeader).html('');

                $(
                    ah.config.cls.liApiStatus + ',' + ah.config.cls.contentSearch + ',' +
                    ah.config.cls.contentHome
                ).hide();
                $(ah.config.cls.contentField).show();

                self.setSelectedSysTransReport({ SYSREPORTID: jsonData.REPORT_DETAILS.ID, SEARCHTYPE: jsonData.REPORT_DETAILS.SEARCHTYPE });
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                $(ah.config.id.contentHeader).html(systemText.displayModeHeader);
                $(ah.config.id.contentSubHeader).html('');

                $(
                    ah.config.cls.liApiStatus + ',' + ah.config.cls.contentSearch + ',' +
                    ah.config.cls.contentHome
                ).hide();
                $('.liCancel, ' + ah.config.cls.contentField).show();

                self.documentListWithCategory([]);
                self.uploadedListWithCategory([]);
                self.documentList([]);
                self.uploadedList([]);
                $('#uploadedInfo').val('');

                self.setObservableValue(self.selectedTransReport, jsonData.REPORT_DETAILS || {});
                for (var i = 0; i < jsonData.REPORT_SCANDOCS.length; i++) {
                    jsonData.REPORT_SCANDOCS[i].documentURL = window.location.origin + '/TransactionReportFiles/' + jsonData.REPORT_SCANDOCS[i].DOCPATH;
                    self.documentList.push(jsonData.REPORT_SCANDOCS[i]);
                }

                for (var i = 0; i < self.documentCategories().length; i++) {
                    var filtereds = self.documentList().filter(function (item) { return item.CATEGORY == self.documentCategories()[i].LOV_LOOKUP_ID });
                    var category = self.documentCategories()[i];
                    category.documents = filtereds;
                    self.documentListWithCategory.push(category);
                }

                if (self.selectedTransReport.REPORTEDDATE())
                    self.selectedTransReport.REPORTEDDATEDISPLAY(moment(self.selectedTransReport.REPORTEDDATE()).format('DD/MM/YYYY'));
                if (self.selectedTransReport.ACTIONDATE())
                    self.selectedTransReport.ACTIONDATEDISPLAY(moment(self.selectedTransReport.ACTIONDATE()).format('DD/MM/YYYY'));
                if (self.selectedTransReport.CLOSEDATE())
                    self.selectedTransReport.CLOSEDATEDISPLAY(moment(self.selectedTransReport.CLOSEDATE()).format('DD/MM/YYYY'));

                CKEDITOR.instances.REPORTS.setData(self.selectedTransReport.REPORTS());
                CKEDITOR.instances.ACTIONREPORTS.setData(self.selectedTransReport.ACTIONREPORTS());
                CKEDITOR.instances.CLOSEREPORTS.setData(self.selectedTransReport.CLOSEREPORTS());
                CKEDITOR.instances.REPORTS.setReadOnly(true);
                CKEDITOR.instances.ACTIONREPORTS.setReadOnly(true);
                CKEDITOR.instances.CLOSEREPORTS.setReadOnly(true);

                self.transReportItems(jsonData.ITEM_BATCHES_V || jsonData.ASSET_V);
            }
            else if (ah.CurrentMode == ah.config.mode.advanceSearch) {
                lovCostCenterArray = jsonData.LOV_LOOKUPS;
                $.each(lovCostCenterArray, function (item) {

                    $(ah.config.id.costcenterStrList)
                        .append($("<option>")
                            .attr("value", lovCostCenterArray[item].DESCRIPTION.trim())
                            .attr("id", lovCostCenterArray[item].LOV_LOOKUP_ID));
                });

            }

            else if (ah.CurrentMode == ah.config.mode.search) {
               
                self.srDataArray = jsonData.TRANSACTION_DETAILS;
                self.searchResult(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {
                var recallBys = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'RECALLBY' });;
                recallBys.unshift({
                    LOV_LOOKUP_ID: null,
                    DESCRIPTION: '- select -'
                });
                self.recallBys(recallBys);

                var recallTypes = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'STOCKBATCH_STATUSTYPE' });;
                recallTypes.unshift({
                    LOV_LOOKUP_ID: null,
                    DESCRIPTION: '- select -'
                });
                self.recallTypes(recallTypes);

                var HHLevels = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'HH-Level' });;
                HHLevels.unshift({
                    LOV_LOOKUP_ID: null,
                    DESCRIPTION: '- select -'
                });
                self.HHLevels(HHLevels);

                var typesOfAction = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'RPT_ACTION' });;
                typesOfAction.unshift({
                    LOV_LOOKUP_ID: null,
                    DESCRIPTION: '- select -'
                });
                self.typesOfAction(typesOfAction);

                var probabilities = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'PROBABILITY' });;
                probabilities.unshift({
                    LOV_LOOKUP_ID: null,
                    DESCRIPTION: '- select -'
                });
                self.probabilities(probabilities);

                var detectabilities = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'DETECTABILITY' });;
                detectabilities.unshift({
                    LOV_LOOKUP_ID: null,
                    DESCRIPTION: '- select -'
                });
                self.detectabilities(detectabilities);

                var DDSA = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'DDSA' });;
                DDSA.unshift({
                    LOV_LOOKUP_ID: null,
                    DESCRIPTION: '- select -'
                });
                self.DDSA(DDSA);

                var QSA = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'QSA' });;
                QSA.unshift({
                    LOV_LOOKUP_ID: null,
                    DESCRIPTION: '- select -'
                });
                self.QSA(QSA);

                var documentCategories = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'DOC_CATEGORY' });;
                //documentCategories.unshift({
                //    LOV_LOOKUP_ID: null,
                //    DESCRIPTION: '- select -'
                //});
                self.documentCategories(documentCategories);
              
                
                if (ah.getParameterValueByName(ah.config.fld.fromDashBoard) == "b62cd054-7d8a-4c96-b761-2119496c8b88") {
                    self.searchNow();
                }
               

            }

        }
        else {

            alert(systemText.errorTwo);
            //window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};