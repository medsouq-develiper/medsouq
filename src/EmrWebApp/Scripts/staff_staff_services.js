﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divStaffServiceID: '.divStaffServiceID',
            statusText: '.status-text',
            staffserviceDetailItem: '.staffserviceDetailItem'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul'
        },
        mode:{
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveStaffService: '#saveStaffService',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmStaffService: '#frmStaffService',
            staffserviceID: '#STAFF_SERVICE_ID',
            staffID: '#STAFF_ID',
            serviceID: '#SERVICE_ID',
            successMessage: '#successMessage'
        },
        cssCls:{
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            dataStaffServiceID: 'data-staffserviceID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            patientDetails: 'PATIENT_DETAILS',
            searchResult: 'searchResult'
        },
        url: {
            homeIndex: '/Home/Index'
        },
        api: {
            getLookupList: '/Setup/GetStaffServicesLookUpTables',
            readStaffService: '/Setup/ReadStaffService',
            searchAll: '/Search/SearchStaffServices',
            createStaffService: '/Setup/CreateStaffService',
            updateStaffService: '/Setup/UpdateStaffService'
        },
        html:{
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal staffserviceDetailItem' data-staffserviceID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplate: '<span class="searchRowLabel">{0}: <span class="searchRowData">{1}</span></span>'
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);
        
    };

    thisApp.initializeLayout = function () {
        
        thisApp.ResetControls();

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divStaffServiceID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;
       
        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divStaffServiceID
        ).hide();
        
        
        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatuxText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

    };

    thisApp.toggleSaveMode = function () {
        
        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();
        
        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divStaffServiceID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

        $(
           thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
           thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000); 
        

    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.getAge = function (birthDate) {

        var seconds = Math.abs(new Date() - birthDate) / 1000;

        var numyears = Math.floor(seconds / 31536000);
        var numdays = Math.floor((seconds % 31536000) / 86400);
        var nummonths = numdays / 30;
        
        return numyears + " y " + Math.round(nummonths) + " m ";
    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for(var i = 0; i <= arguments.length - 2 ; i++){
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);           
        }

        return stringToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name),
                type = $el.attr('type');

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var staffservicesViewModel = function (systemText) {

    var self = this;

    var ah = new appHelper(systemText);

    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));

        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.toggleReadMode();
        
        postData = { STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi()+ ah.config.api.getLookupList, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

        ah.initializeLayout();

    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.createNewStaffService = function () {

        ah.toggleAddMode();

    };

    self.modifyStaffServiceDetails = function () {

        ah.toggleEditMode();

    };

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.saveAllChanges = function () {

        $(ah.config.id.saveStaffService).trigger('click');

    };

    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            self.showStaffServiceDetails();
        }

    };

    self.showStaffServiceDetails = function (isNotUpdate) {

        if (isNotUpdate) {

            var staffserviceDetails = sessionStorage.getItem(ah.config.skey.staffserviceDetails);

            var jStaffService = JSON.parse(staffserviceDetails);
           
            ah.ResetControls();
            ah.LoadJSON(jStaffService);
        }

        ah.toggleDisplayMode();

    };

    self.searchResult = function (jsonData) {

        sessionStorage.setItem(ah.config.skey.searchResult, JSON.stringify(jsonData.STAFF_SERVICES));
        var sr = jsonData.STAFF_SERVICES;

        $(ah.config.id.searchResultList  + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {
               
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].STAFF_SERVICE_ID, sr[o].STAFF_SERVICE_ID, sr[o].FIRST_NAME.toLowerCase(), sr[o].LAST_NAME.toLowerCase());
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.servicename, sr[o].SERVICE_DESCRIPTION);
                
                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.staffserviceDetailItem).bind('click', self.readStaffServiceByStaffServiceID);
        ah.displaySearchResult();

    };

    self.readStaffServiceByStaffServiceID = function () {

        var staffserviceID = this.getAttribute(ah.config.attr.dataStaffServiceID).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();
       
        postData = { STAFF_SERVICE_ID: staffserviceID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readStaffService, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleSearchMode();

        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.saveStaffServiceDetails = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffserviceDetails = ah.ConvertFormToJSON($(ah.config.id.frmStaffService)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        staffserviceDetails.CREATED_BY = staffLogonSessions.STAFF_ID;
        postData = { STAFF_SERVICES: staffserviceDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };

        if (ah.CurrentMode == ah.config.mode.add) {

            ah.toggleSaveMode();           
            $.post(self.getApi() + ah.config.api.createStaffService, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {

            ah.toggleSaveMode();
            $.post(self.getApi() + ah.config.api.updateStaffService, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }

    };

    self.webApiCallbackStatus = function (jsonData) {
      //  alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
           // window.location.href = ah.config.url.homeIndex;
           
        }
        else if (jsonData.STAFF_SERVICE_ID || jsonData.STAFF_SERVICES || jsonData.STAFF || jsonData.SERVICES || jsonData.SUCCESS || jsonData.STAFF_SERVICES) {

            if (ah.CurrentMode == ah.config.mode.save) {

                sessionStorage.setItem(ah.config.skey.staffserviceDetails, JSON.stringify(jsonData.STAFF_SERVICES));
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();
                
                if (jsonData.SUCCESS){
                    self.showStaffServiceDetails(false);
                }
                else {
                    self.showStaffServiceDetails(true);
                }
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                sessionStorage.setItem(ah.config.skey.staffserviceDetails, JSON.stringify(jsonData.STAFF_SERVICES));
                self.showStaffServiceDetails(true);
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
                self.searchResult(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {
                
                $.each(jsonData.STAFF, function (item) {
                    $(ah.config.id.staffID)
                        .append($("<option></option>")
                        .attr("value", jsonData.STAFF[item].STAFF_ID)
                        .text(jsonData.STAFF[item].FIRST_NAME +' ' + jsonData.STAFF[item].LAST_NAME));
                });
                $.each(jsonData.SERVICES, function (item) {
                    $(ah.config.id.serviceID)
                        .append($("<option></option>")
                        .attr("value", jsonData.SERVICES[item].SERVICE_ID)
                        .text(jsonData.SERVICES[item].DESCRIPTION));
                });
            }

        }
        else {

            alert(systemText.errorTwo);
          //  window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};