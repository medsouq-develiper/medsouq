﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divcostcenterID: '.divcostcenterID',
            statusText: '.status-text',
            costcenterDetailItem: '.costcenterDetailItem',
            liCreateStore: '.liCreateStore',
            otherContactRemove: '.otherContactRemove',
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmCostCenter: '#frmCostCenter',
            successMessage: '#successMessage',
            imageInfo: '#imageInfo',
            otherContactNew: '#otherContactNew',
            COSTCENTERTYPE: '#COSTCENTERTYPE',
            storeInfo: '#storeInfo',
            CLIENT_CONTAINER: '#CLIENT_CONTAINER',
            SUBCLIENT_CONTAINER: '#SUBCLIENT_CONTAINER',
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datacostcenterID: 'data-costcenterID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            costCenterDetails: 'COSTCENTER_DETAILS',
            searchResult: 'searchResult'
        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            createStoreModal: '#openCreateStoreModal',
            openOtherContactModal: '#openOtherContactModal',
            modalClose: '#'
        },
        api: {
            readCostCenter: '/Setup/ReadCostCenter',
            searchAll: '/Setup/SearchCostCenter',
            createCostCenter: '/Setup/CreateCostCenter',
            updateCostCenter: '/Setup/UpdateCostCenter'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal costcenterDetailItem' data-costcenterID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}&nbsp;-&nbsp;{2}</a>",
            searchRowTemplate: '<span class="search-row-label fc-light-grey-2">{0}: <span class="search-row-data">{1}</span></span>'
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' + thisApp.config.id.imageInfo + ',' + thisApp.config.id.otherContactNew + ',' + thisApp.config.cls.otherContactRemove +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divcostcenterID + ',' + thisApp.config.cls.notifySuccess + ',' + thisApp.config.cls.liCreateStore
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.id.imageInfo + ',' + thisApp.config.id.otherContactNew + ',' + thisApp.config.cls.otherContactRemove).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divcostcenterID + ',' + thisApp.config.cls.liCreateStore
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        $(thisApp.config.id.CLIENT_CONTAINER).hide();
        $(thisApp.config.id.SUBCLIENT_CONTAINER).hide();

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.id.imageInfo + ',' + thisApp.config.id.otherContactNew + ',' + thisApp.config.cls.otherContactRemove).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.id.imageInfo + ',' + thisApp.config.id.otherContactNew + ',' + thisApp.config.cls.otherContactRemove).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("CODEID").disabled = true;

    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;
        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divcostcenterID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.id.imageInfo + ',' + thisApp.config.id.otherContactNew + ',' + thisApp.config.cls.otherContactRemove
        ).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);

        if ($(thisApp.config.id.COSTCENTERTYPE).val() === "MC") {
            $(thisApp.config.id.CLIENT_CONTAINER).show();
            $(thisApp.config.id.SUBCLIENT_CONTAINER).hide();
        } else if ($(thisApp.config.id.COSTCENTERTYPE).val() === "SC") {
            $(thisApp.config.id.CLIENT_CONTAINER).hide();
            $(thisApp.config.id.SUBCLIENT_CONTAINER).show();
        } else {
            $(thisApp.config.id.CLIENT_CONTAINER).hide();
            $(thisApp.config.id.SUBCLIENT_CONTAINER).hide();
        }

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {

        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name),
                type = $el.attr('type');

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });
    };
};

/*Knockout MVVM Namespace - Controls form functionality*/
var costcentersViewModel = function (systemText) {

    var self = this;

    var ah = new appHelper(systemText);
    self.costcenterClient = {
        DESCRIPTION: ko.observable("")};

    self.unsaveStore = {
        STORE_NO: ko.observable(""),
        DESCRIPTION: ko.observable(""),
        LOCATION: ko.observable(""),
        CONTACT_NO: ko.observable(""),
        CLIENT_CODE: ko.observable(""),
        CLIENT_NAME: ko.observable(""),
        STATUS: "ACTIVE",
        COSTCENTERFLAG: true
    };

    self.otherContactList = ko.observableArray([]);
    self.modalOtherContact = {
        CLIENTCONTACTTYPE: ko.observable(""),
        CLIENTCONTACT: ko.observable("")
    };

    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;
        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.toggleReadMode();
        ah.initializeLayout();
        window.location.href = ah.config.url.modalClose
    };

    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };

    self.createNew = function () {
        self.otherContactList.removeAll();
        ah.toggleAddMode();

    };

    self.modifyCostCenter = function () {

        ah.toggleEditMode();
        if ($(ah.config.id.COSTCENTERTYPE).val() == "MC") {
            if (!self.unsaveStore.DESCRIPTION())
                $(ah.config.cls.liCreateStore).show();
        }
    };

    $('#imageInfo').change(function (e) {
        var files = e.target.files;
        self.loadImages(files);
    });

    self.loadImages = function (files) {

        'use strict';
      //  x = 0;
        var resize = new window.resize();
        resize.init();
        //event.preventDefault();           

        // var files = event.target.files;       
        var array = 0;
        var arrayFileName = new Array;

        for (var i in files) {
            if (typeof files[i] !== 'object') return false;

            var file_name = files[i].name;
            arrayFileName.push(file_name);
            var base64 = self.resizeImage(files[i], arrayFileName);
        }
    };

    self.resizeImage = function (file, arrayFileName) {

        var resize = new window.resize();
        resize.init();
        var reader = new FileReader();
        reader.onload = function (readerEvent) {
            self.resizeCanvas(readerEvent.target.result, 1200, 'file', arrayFileName, file);
        }
        reader.readAsDataURL(file);
    };
    self.resizeCanvas = function (dataURL, maxSize, outputType, arrayFileName, file) {

        var _this = this;
        var x = 0;
        var image = new Image();
        image.onload = function (imageEvent) {

            // Resize image
            var canvas = document.createElement('canvas'),
                width = image.width,
                height = image.height;
            if (width > height) {
                if (width > maxSize) {
                    height *= maxSize / width;
                    width = maxSize;
                }
            } else {
                if (height > maxSize) {
                    width *= maxSize / height;
                    height = maxSize;
                }
            }
            canvas.width = width;
            canvas.height = height;
            canvas.getContext('2d').drawImage(image, 0, 0, width, height);

            var newdataURL = canvas.toDataURL('image/jpeg', 0.8)
            self.resizeOutcome(newdataURL, 600, 'dataURL', arrayFileName, file);

            // _this.output(canvas, outputType, callback);
        }
        image.src = dataURL;
    };


    self.resizeOutcome = function (dataURL, maxSize, outputType, arrayFileName, file) {

        var _this = this;
        var image = new Image();
        image.onload = function (imageEvent) {

            // Resize image
            var canvas = document.createElement('canvas'),
                width = image.width,
                height = image.height;
            if (width > height) {
                if (width > maxSize) {
                    height *= maxSize / width;
                    width = maxSize;
                }
            } else {
                if (height > maxSize) {
                    width *= maxSize / height;
                    height = maxSize;
                }
            }
            canvas.width = width;
            canvas.height = height;
            canvas.getContext('2d').drawImage(image, 0, 0, width, height);
            var newdataURL = canvas.toDataURL('image/jpeg', 0.8)

            self.eloaddata("", "", newdataURL, arrayFileName, file);
            // _this.output(canvas, outputType, callback);

        }
        image.src = dataURL;
    };

    self.eloaddata = function (filename, filetype, data, filenameArray, file) {
        document.getElementById("COSTCENTER_IMAGE").src = data;
    };

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.saveAllChanges = function () {

        $(ah.config.id.saveInfo).trigger('click');

    };

    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
            self.searchNow();
        } else {
            ah.toggleDisplayMode();
            self.showCostCenterDetails(true);
        }
    };

    self.showCostCenterDetails = function (isNotUpdate, data) {

        if (isNotUpdate) {
            var costcenterDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.costCenterDetails));
            ah.ResetControls();
            ah.LoadJSON(costcenterDetails);
            if (data && data.STORES) {
                self.unsaveStore.DESCRIPTION(data.STORES.DESCRIPTION);
                self.unsaveStore.STORE_NO(data.STORES.STORE_ID);
            }
            if (data && data.CLIENT)
                self.costcenterClient.DESCRIPTION(data.CLIENT.DESCRIPTION);
            document.getElementById("COSTCENTER_IMAGE").src = costcenterDetails.LOGOPATH;
        }
        ah.toggleDisplayMode();

    };

    self.searchResult = function (jsonData) {

        var sr = jsonData.COSTCENTER_LIST;
        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');
        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';
            for (var o in sr) {
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].CODEID, sr[o].CODEID, sr[o].DESCRIPTION, '');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.address, sr[o].ADDRESS);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.contactNo, sr[o].CONTACTNO);

                htmlstr += '</li>';
            }
            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.costcenterDetailItem).bind('click', self.readCostCenter);
        ah.displaySearchResult();

    };

    self.readCostCenter = function () {

        var costcenterID = this.getAttribute(ah.config.attr.datacostcenterID).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        ah.toggleReadMode();
        postData = { CODEID: costcenterID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readCostCenter, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }
        return api
    };
    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        ah.toggleSearchMode();
        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        $(ah.config.cls.liSave).hide();
        $(ah.config.cls.liCreateStore).hide();
        self.unsaveStore.DESCRIPTION("");
        self.costcenterClient.DESCRIPTION("");

    };

    self.saveInfo = function () {
        document.getElementById("CODEID").disabled = false;
        var costcenterDetails = ah.ConvertFormToJSON($(ah.config.id.frmCostCenter)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        var costcenterCode = "CC"+ $("#CODEID").val();
        var frmData = new FormData();

        frmData.append("REF_ID", costcenterCode);
        frmData.append("pathLocation", "~/ClientFiles");
        var filebase = $("#imageInfo").get(0);
        var file = filebase.files;

        if (file.length > 0) {
            frmData.append(file[0].name, file[0]);
            $.ajax({
                url: '/Upload/SaveDocument',
                type: "POST",
                contentType: false,
                processData: false,
                data: frmData,
                success: function (data) {
                    costcenterDetails.LOGOPATH = `/ClientFiles/${costcenterCode + "_" + file[0].name}`;

                    if (ah.CurrentMode == ah.config.mode.add) {
                        postData = { COSTCENTER_DETAILS: costcenterDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
                        postData.CLIENTOTHCONTACT = self.otherContactList();
                        ah.toggleSaveMode();
                        $.post(self.getApi() + ah.config.api.createCostCenter, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
                    }
                    else {
                        if (!self.unsaveStore.STORE_NO() && self.unsaveStore.DESCRIPTION()) {
                            var StoreData = {
                                STORE_NO: self.unsaveStore.STORE_NO(),
                                DESCRIPTION: self.unsaveStore.DESCRIPTION(),
                                LOCATION: self.unsaveStore.LOCATION(),
                                CONTACT_NO: self.unsaveStore.CONTACT_NO(),
                                CLIENT_CODE: self.unsaveStore.CLIENT_CODE(),
                                CLIENT_NAME: self.unsaveStore.CLIENT_NAME(),
                                STATUS: self.unsaveStore.STATUS,
                                COSTCENTERFLAG: self.unsaveStore.COSTCENTERFLAG
                            }
                            postData = { COSTCENTER_DETAILS: costcenterDetails, STAFF_LOGON_SESSIONS: staffLogonSessions, StoreDetails: StoreData };
                        } else {
                            postData = { COSTCENTER_DETAILS: costcenterDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
                        }
                        postData.CLIENTOTHCONTACT = self.otherContactList();
                        ah.toggleSaveMode();
                        $.post(self.getApi() + ah.config.api.updateCostCenter, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
                    }
                },
                error: function (err) {
                    alert(error);
                }
            });
        } else {

            
            if (ah.CurrentMode == ah.config.mode.add) {
                postData = { COSTCENTER_DETAILS: costcenterDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
                postData.CLIENTOTHCONTACT = self.otherContactList();
                ah.toggleSaveMode();
                $.post(self.getApi() + ah.config.api.createCostCenter, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
            }
            else {
                if (!self.unsaveStore.STORE_NO() && self.unsaveStore.DESCRIPTION()) {
                    var StoreData = {
                        STORE_NO: self.unsaveStore.STORE_NO(),
                        DESCRIPTION: self.unsaveStore.DESCRIPTION(),
                        LOCATION: self.unsaveStore.LOCATION(),
                        CONTACT_NO: self.unsaveStore.CONTACT_NO(),
                        CLIENT_CODE: self.unsaveStore.CLIENT_CODE(),
                        CLIENT_NAME: self.unsaveStore.CLIENT_NAME(),
                        STATUS: self.unsaveStore.STATUS,
                        COSTCENTERFLAG: self.unsaveStore.COSTCENTERFLAG
                    }
                    postData = { COSTCENTER_DETAILS: costcenterDetails, STAFF_LOGON_SESSIONS: staffLogonSessions, StoreDetails: StoreData };
                } else {
                    postData = { COSTCENTER_DETAILS: costcenterDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
                }
                postData.CLIENTOTHCONTACT = self.otherContactList();
                ah.toggleSaveMode();
                $.post(self.getApi() + ah.config.api.updateCostCenter, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
            }
        }
    };

    self.createNewStore = function () {
        var sessionDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.costCenterDetails));
        self.unsaveStore.DESCRIPTION(sessionDetails.DESCRIPTION + " Store");
        self.unsaveStore.CLIENT_CODE(sessionDetails.CODEID);
        self.unsaveStore.CLIENT_NAME(sessionDetails.DESCRIPTION);
        window.location.href = ah.config.url.createStoreModal;
    };

    self.openOtherContactModal = function () {
        self.modalOtherContact.CLIENTCONTACTTYPE('');
        self.modalOtherContact.CLIENTCONTACT('');

        window.location.href = ah.config.url.openOtherContactModal;
    };

    self.removeOtherContact = function (data) {
        self.otherContactList.remove(data);
    };

    self.addOtherContact = function () {
        self.otherContactList.push({
            CLIENTCONTACTTYPE: ko.observable(self.modalOtherContact.CLIENTCONTACTTYPE()),
            CLIENTCONTACT: ko.observable(self.modalOtherContact.CLIENTCONTACT()),
            CLIENTCONTACTTYPEVALUE: ko.observable(self.getContactTypeValue(self.modalOtherContact.CLIENTCONTACTTYPE()))
        });

        window.location.href = ah.config.url.modalClose;
    };

    self.getContactTypeValue = function (type) {
        switch (type) {
            case 'E':
                return 'Email';
                break;
            case 'FN':
                return 'Fax Number';
                break;
            case 'MN':
                return 'Mobile Number';
                break;
            case 'TN':
                return 'Telephone Number';
                break;
            case 'P':
                return 'Person';
                break;
            case 'PN':
                return 'Pager Number';
                break;
        }
    };

    self.webApiCallbackStatus = function (jsonData) {

        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.COSTCENTER_DETAILS || jsonData.COSTCENTER_LIST || jsonData.SUCCESS || jsonData.STORES || jsonData.CLIENT) {

            if (ah.CurrentMode == ah.config.mode.save) {

                sessionStorage.setItem(ah.config.skey.costCenterDetails, JSON.stringify(jsonData.COSTCENTER_DETAILS));
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();

                if (jsonData.SUCCESS) {
                    self.showCostCenterDetails(false);
                }
                else {
                    self.showCostCenterDetails(true);
                }
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                sessionStorage.setItem(ah.config.skey.costCenterDetails, JSON.stringify(jsonData.COSTCENTER_DETAILS));
                self.showCostCenterDetails(true, jsonData);

                self.otherContactList.removeAll();
                if (jsonData.CLIENTOTHCONTACTS && jsonData.CLIENTOTHCONTACTS.length) {
                    for (var i = 0; i < jsonData.CLIENTOTHCONTACTS.length; i++) {
                        self.otherContactList.push({
                            CLIENTCONTACTTYPE: ko.observable(jsonData.CLIENTOTHCONTACTS[i].CLIENTCONTACTTYPE),
                            CLIENTCONTACT: ko.observable(jsonData.CLIENTOTHCONTACTS[i].CLIENTCONTACT),
                            CLIENTCONTACTTYPEVALUE: ko.observable(self.getContactTypeValue(jsonData.CLIENTOTHCONTACTS[i].CLIENTCONTACTTYPE))
                        });
                    }
                }

                $(ah.config.cls.otherContactRemove).hide();
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
                self.searchResult(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {

            }
        }
        else {
            alert(systemText.errorTwo);
            window.location.href = ah.config.url.homeIndex;
        }
    };

    self.saveStore = function () {
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        self.unsaveStore.CREATED_BY = staffLogonSessions.STAFF_ID;
        self.modifyCostCenter();
        window.location.href = ah.config.url.modalClose;
    };

    self.initialize();
};