﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            detailItem: '.detailItem',
            info1: '.info1',
            info2: '.info2',
            info3: '.info3',
            info4: '.info4',
            info5: '.info5',
            info6: '.info6',
            info7: '.info7',
            liassetInfo: '.liassetInfo',
            searchAsset: '.searchAsset',
			otherButton: '.otherButton',
            pagingDiv: '.pagingDiv',
            detailParts: '.detailParts',
            printDetails: '.printDetails',
            liPrint: '.liPrint',
            lisort: '.lisort',
            detailrequest: '.detailrequest',
            barcode: '.barcode'
        },
        fld: {
            assetNoInfo: 'ASSETNO',
            assetDescInfo: 'ASSETDESC',
            reqNo: 'REQNO',
			reqWOAction: 'REQWOAC',
            newWorkOrder: 'asdfxhsadf',
            otherAsset: 'expxgt',
            fromMenuOnhold: 'bstbxyxkktpk'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            tbody: 'tbody',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul',
            tr: 'tr'
        },
        tagId: {
            btnContract: 'btnContract',
            btnLCycle: 'btnLCycle',
            btnHOperation: 'btnHOperation',
            btnANotes: 'btnANotes',
            btnSComponent: 'btnSComponent',
            btnAPersonnel: 'btnAPersonnel',
            btnCagetory: 'btnCagetory',
            btnManufacturer: 'btnManufacturer',
            btnSupplier: 'btnSupplier',
            btnBuilding: 'btnBuilding',
            btnCostCenter: 'btnCostCenter',
            btnLogHistory: 'btnLogHistory',
            btnpdf: 'btnpdf',
            btnResponsibleCenter: 'btnResponsibleCenter',
            btnLocation: 'btnLocation',
            txtGlobalSearchOth: 'txtGlobalSearchOth',
            priorPage: 'priorPage',
			nextPage: 'nextPage',
			PaginGnextPage: 'PaginGnextPage',
            PaginGpriorPage: 'PaginGpriorPage',
            txtGlobalSearchOthspartParts: 'txtGlobalSearchOthspartParts',
            searchSupplierId: 'searchSupplierId',
            searchStaffId: 'searchStaffId',
            lookUptxtGlobalSearchOth: 'lookUptxtGlobalSearchOth',
            assetMultiCC: 'assetMultiCC',
            assetAll: 'assetAll',
            searchCostCenterId: 'searchCostCenterId'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7,
            advanceSearch:8
        },
        id: {
            assetPrintSearchResultList: '#assetPrintSearchResultList',
            loadingBackground: '#loadingBackground',
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            searchResult1: '#searchResult1',
            searchResult2: '#searchResult2',
            frmInfo: '#frmInfo',
            clinicdoctorID: '#CLINIC_DOCTOR_ID',
            clinicID: '#CLINIC_ID',
            statusLogList: '#statusLogList',
            staffID: '#STAFF_ID',
            serviceID: '#SERVICE_ID',
            assetInfobtn: '#assetInfobtn',
            successMessage: '#successMessage',
            servDept: '#SERV_DEPT',
            woPriority: '#JOB_URGENCY',
            woProblemType: '#REQ_TYPE',
            woSpecialty: '#SPECIALTY',
            txtGlobalSearchOth: '#txtGlobalSearchOth',
            totNumber: '#totNumber',
            loadNumber: '#loadNumber',
            pageCount: '#pageCount',
            pageNum: '#pageNum',
			staffAssignTo: '#ASSIGN_TO',
			searchStatus: '#SEARCH_STATUS',
			PaginGpageNum: '#PaginGpageNum',
			PaginGpageCount: '#PaginGpageCount',
			jobType: '#JOB_TYPE',
            searchHelpDeskResult: '#searchHelpDeskResult',
            modifyButton: '#modifyButton',
            noteHistory: '#noteHistory',
            noteEntry: '#noteEntry',
            woReferenceNotes: '#WO_REFERENCE_NOTESREF',
            addButtonIcon: '#addButtonIcon',
            assetNOTES: '#ERECORD_GENERAL_NOTES',
            woStatus: '#WO_STATUS',
            newNotes: '#newNotes',
            supportingDocs: '#supportingDocs',
            searchTicket: '#searchTicket',
            searchAsset: '#searchAsset',
            searchResultListParts: '#searchResultListParts',
            closeDate: '#CLOSED_DATE',
            lblClosedDate: '#lblClosedDate',
            printSummary: '#printSummary',
            PrintBulkBtn:'#PrintBulkBtn',
            printBulk: '#printBulk',
            searchResultPartsOnhand: '#searchResultPartsOnhand',
            filterAssignTo: '#filterAssignTo',
            filterAssignToList: '#filterAssignToList',
            filterJobType: '#filterJobType',
            txtGlobalSearchOthspartParts: '#txtGlobalSearchOthspartParts',
            saveMaterialInfo: '#saveMaterialInfo',
            searchResultListPartstBody: '#searchResultListPartstBody',
            wolabourinfo: '#woLabourInfo',
            woMaterialListData: '#woMaterialListData',
            searchResultListQVParts: '#searchResultListQVParts',
            woLabourInfoQV: '#woLabourInfoQV',
            assetWarrantyQV: '#assetWarrantyQV',
            lovassignto: '#LOVASSIGNTO',
            assignOutsourceDev: '#ASSIGN_OUTSOURCEDEV',
            searchResultLookUp: '#searchResultLookUp',
            lookUptxtGlobalSearchOth: '#lookUptxtGlobalSearchOth',
            filterCostCenterList: '#filterCostCenterList',
            filterSupplierList: '#filterSupplierList',
            searchCostCenterId: '#searchCostCenterId',
            WoPrintSticker: '#WoPrintSticker',
            WoOutOrderSticker: '#WoOutOrderSticker',
            WoInitialSticker: '#WoInitialSticker',
            OutOrderSticker: '#OutOrderSticker',
            PMSticker: '#PMSticker',
            InWoSticker: '#InWoSticker'
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID',
            closedDate: 'CLOSED_DATE',
            datainfoPartID: 'data-infoPartID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'searchResult',
            woDetails: 'AM_WORK_ORDERS',
            assetItems: 'AM_ASSET_DETAILS',
			assetList: 'AM_ASSET_DETAILS',
			groupPrivileges: 'GROUP_PRIVILEGES',
        },
        url: {
            homeIndex: '/Home/Index',
            modalAssetSearch: '#openModal',
            aimsInformation: '/aims/assetInfo',
            modalAddItemsInfo: '#openModalInfo',
            modalClose: '#',
            aimWorkOrderLabour: '/aims/amWorkOrderLabour',
            aimsHomepage: '/Menuapps/aimsIndex',
            aimsWOSearch: '/aims/amWorkOrder',
			modalFilter: '#openModalFilter',
            modalhdr: '#openModalHelpDesk',
            openModalInHouse: '#openModalInHouse',
            openModalPrintPage: '#openModalPrintPage',
            modalAddItems: '#openModalInHouseParts',
            openModalQuickView: '#openModalQuickView',
            openModalLookup: '#openModalLookup',
            aimsInformationOth: '/aims/amAssetOth',
        },
        lfor: {
            closedDate: 'CLOSED_DATE'
        },
        api: {
            readSetup: '/AIMS/ReadWO',
            searchAll: '/AIMS/SearchWO',
            searchAssets: '/AIMS/SearchAssetsWOActive',
            createSetup: '/AIMS/CreateNewWO',
            updateSetup: '/AIMS/UpdateWO',
            getListLOV: '/Search/SearchLOVWorkOrder',
            readAssetNo: '/AIMS/ReadAssetInfo',
            searchContract: '/AIMS/SearchAssetContractService',
            searchStaffList: '/Staff/SearchStaffLookUp',
			readWSInspection: '/AIMS/ReadWSInfo',
            searchHelpDeskNew: '/Search/SearchHelpDeskNew',
            searchAssetNotes: '/AIMS/SearchAssetNotes',
            searchWONotes: '/AIMS/SearchWONotes',
            searchwoMaterial: '/Inventory/SearchWOPartsIncUse',
            createDocs: '/AIMS/CreateAssetWODocs',
            searchWOStatus: '/AIMS/SearchWOStatus',
            readAssetInfoV: '/AIMS/RedAssetInfoV',
            partssearchAll: '/Inventory/SearchWOIDPartsRequested',
            serachWoList: '/Reports/SearchWODetails',
            createMaterials: '/Inventory/CreateNewWOLMaterial',
            searchPartsList: '/Inventory/SearchPartsActiveLists',
            searchWOQuickView: '/AIMS/ReadWOQuickView',
            deleteWOSpare: '/Inventory/deleteWorkOrderSpartParts',
            searchSupplier: '/AIMS/SearchSuppierLOV',
            searchAdvanceLov: '/Search/SearchAdvanceWOLov',
            searchLOVQuery: '/AIMS/SearchAimsLOVCostCenter'
        },
        html: {
            searchRowHeaderTemplate: "<a style='max-width:93%;display: inline;'href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right no-print'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowHeaderTemplateListLOV: "<a href='#' class=' fc-slate-blue  fs-medium-normal detailItem' data-infoID='{0}'><i class=' fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowHeaderTemplateLOV: "<a 'href='#' class='fs-small-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right no-print'></i>&nbsp;&nbsp;{1} {2} {3}</a>",
            searchRowHeaderTemplateSR: "<a style='max-width:93%;display: inline;' href='#' class='fs-medium-normal fc-green detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right no-print fc-green'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplateP: '<span class="search-row-label fc-light-grey-2">{0}: <span class="search-row-data">{1}</span></span>',
            searchRowTemplate: '<span class="search-row-label fc-light-grey-2">{0}: <span class="search-row-data">{1}</span></span>',
            searchRowTemplateC: '<span class="search-row-label fc-light-grey-2">{0}: <span class=" fc-green">{1}</span></span>',
            searchRowTemplateSpareParts: "<span style='background-color:#fcb304;padding-left: 10px;padding-right: 10px;cursor: pointer;' class='search-row - label   detailParts' data-infoID='{0}'> <span>{1}</span></span>",
            searchRowTemplateQuickView: "<span style='color:white; background-color:green;padding-left: 10px;padding-right: 10px;cursor: pointer;'   class='search-row - label  detailrequest' data-infoID='{0}'> <span>{1}</span></span>",
            searchRowHeaderDOCTemplateList: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            searchRowHeaderTemplateList: "<a href='#openModalInHouseParts' class='fs-medium-normal fc-slate-blue  detailItem' data-infoPartID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
            searchLOVRowHeaderTemplate: "<a href='#openModalInfo' class='fs-medium-normal productDetailItem fc-greendark' data-items='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} <span>{3}</span></a>",
            searchRowTemplateLOV: '<span class="search-row-label fc-light-grey-2">{0}: <span class="search-row-data " style="color: black;">{1}</span></span>',
            searchRowTemplateParts: '<span class="search-row-label fc-light-grey-2">{0}: <span class="search-row-data" style="color:black;">{1}</span></span>',
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;
    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.id.searchCostCenterId + ',' +thisApp.config.cls.pagingDiv + ',' +thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.barcode + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess
            + ',' + thisApp.config.cls.liassetInfo + ',' + thisApp.config.cls.searchAsset + ',' + thisApp.config.cls.otherButton + ',' + thisApp.config.cls.liPrint).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();
        
        thisApp.CurrentMode = thisApp.config.mode.idle;
        document.getElementById("JOB_DUE_DATE").disabled = true;
        document.getElementById("JOB_DURATION").disabled = true;
        document.getElementById("ASSET_NO").disabled = true;
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";

    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.searchAsset).show();

        $(
            thisApp.config.id.assignOutsourceDev + ',' + thisApp.config.cls.lisort + ',' + thisApp.config.cls.pagingDiv + ',' +thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.otherButton + ',' + thisApp.config.id.closeDate + ',' + thisApp.config.id.lblClosedDate + ',' + thisApp.config.cls.liPrint
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("JOB_DUE_DATE").disabled = false;
        document.getElementById("JOB_DURATION").disabled = false;
        $("#assetInfoDet").hide();
        document.getElementById("ASSET_NO").disabled = true;
        document.getElementById("REQ_ID").disabled = true;
        $("#REQ_ID").hide();
        $("#REQ_ID").next("br").remove();
        $("label[for='REQ_ID']").hide();
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none";
    };
    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    thisApp.toggleAddItems = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;


        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("JOB_DUE_DATE").disabled = false;
        document.getElementById("JOB_DURATION").disabled = false;
        document.getElementById("ASSET_NO").disabled = true;

    };
    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();
		$(
			thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
			thisApp.config.tag.textArea
		).removeClass(thisApp.config.cssCls.viewMode);
        document.getElementById("ASSET_NO").disabled = true;
    };
    thisApp.toggleSearchItems = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        //$(thisApp.config.cls.statusText).text(systemText.searchModeStatuxText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();
        document.getElementById("ASSET_NO").disabled = true;
    };
    thisApp.formatJSONDateTimeToString = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;
     
        if (dateToFormat === null) return "";
        if (dateToFormat.length < 16) return "";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0] + ' ' + strTime[1].substring(0, 5);


        return dateToFormat;
    };
    thisApp.displaySearchResult = function () {
        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.lisort + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
		$(thisApp.config.cls.liApiStatus).hide();
		
		$(
			thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
			thisApp.config.tag.textArea
		).removeClass(thisApp.config.cssCls.viewMode);

		$(
			thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
		).prop(thisApp.config.attr.disabled, false);

		$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
    };

    thisApp.togglehideOthInfo = function () {
        $(thisApp.config.cls.info7 + ',' +thisApp.config.cls.info6 + ',' +thisApp.config.cls.info1 + ',' + thisApp.config.cls.info2 + ',' + thisApp.config.cls.info3 + ',' + thisApp.config.cls.info4 + ',' + thisApp.config.cls.info5).hide();
    };
    thisApp.toggleShowInfo1 = function () {
        $(thisApp.config.cls.info1).show();
    };
    thisApp.toggleShowInfo2 = function () {
        $(thisApp.config.cls.info2).show();
    };
    thisApp.toggleShowInfo3 = function () {
        $(thisApp.config.cls.info3).show();
    };
    thisApp.toggleShowInfo4 = function () {
        $(thisApp.config.cls.info4).show();
    };
    thisApp.toggleShowInfo5 = function () {
        $(thisApp.config.cls.info5).show();
    };
    thisApp.toggleShowInfo6 = function () {
        $(thisApp.config.cls.info6).show();
    };
    thisApp.toggleShowInfo7 = function () {
        $(thisApp.config.cls.info7).show();
    };
    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.searchAsset + ',' + thisApp.config.cls.otherButton).show();
        $(thisApp.config.id.searchCostCenterId + ',' +thisApp.config.cls.pagingDiv + ',' +thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("JOB_DUE_DATE").disabled = false;
        document.getElementById("JOB_DURATION").disabled = false;
        document.getElementById("ASSET_NO").disabled = true;
        document.getElementById("REQ_ID").disabled = true;
        

    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();
        document.getElementById("ASSET_NO").disabled = true;

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.otherButton).show();

        $(
            thisApp.config.id.assignOutsourceDev + ',' + thisApp.config.cls.pagingDiv + ',' +thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.searchAsset + ',' + thisApp.config.cls.searchTicket + ',' +thisApp.config.cls.liPrint
        ).hide();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.otherButton + ',' + thisApp.config.id.closeDate + ',' + thisApp.config.id.lblClosedDate
        ).show();

        $(
            thisApp.config.id.searchCostCenterId + ',' +thisApp.config.cls.lisort + ',' + thisApp.config.cls.pagingDiv + ',' +thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.searchAsset + ',' + thisApp.config.cls.searchTicket + ',' + thisApp.config.cls.liPrint
        ).hide();

        $(
           thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
           thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);
        document.getElementById("JOB_DUE_DATE").disabled = true;
        document.getElementById("JOB_DURATION").disabled = true;
        document.getElementById("ASSET_NO").disabled = true;

    };

    thisApp.toggleDisplayModeAsset = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        //$(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        //$(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.searchAsset).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.liApiStatus).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("ASSET_NO").disabled = true;

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {
        $(form).find(":disabled").removeAttr("disabled");
        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

   
    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };
    thisApp.formatJSONDateToString = function () {

        var dateToFormat = arguments[0];
        var strDate;

        if (dateToFormat === null) return "";
        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };

    thisApp.formatJSONTimeToString = function () {

        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "";
        if (dateToFormat === "") return "";
        if (dateToFormat.length < 16) return "";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        var timeToFormat = strTime[1].substring(0, 5);


        return timeToFormat;
    };
    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');


            if (isDateTime) {
                val = thisApp.formatJSONDateTimeToString(val);
            }

            if (isDate) {
                val = thisApp.formatJSONDateToString(val);
            }

            var $el = $('#' + name), type = $el.attr('type'), isTime = $el.attr('data-istime');
            if (isTime) {
                if (val === null || val === "") {
                    val = "00:00";
                } else {
                    val = thisApp.formatJSONTimeToString(val);
                }

            }

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var woInfoViewModel = function (systemText) {
    var self = this;
    //$(document).bind("contextmenu", function (e) {
    //    e.preventDefault();
    //});
    //$(document).keydown(function (e) {
    //    if (e.which === 123) {
    //        return false;
    //    }
    //});
    self.isPrint = 1;

    document.onkeydown = function (e) {
        if (event.keyCode == 123) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
            return false;
        }
        if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
            return false;
        }
    }
    var xElements = document.getElementById("frmInfo").elements.length;

    self.partsWLList = ko.observableArray("");
    self.partsWOList = ko.observableArray("");
    self.partsAddWOList = ko.observableArray("");
    self.warrantyData = ko.observableArray([]);
    var srStaffList = ko.observableArray([]);
    var supplierListLOV = ko.observableArray([]);
    var resultHtml = "";
    var sort = "WONO";
    self.modifiedPartRequest = ko.observable("");
    var assetList = ko.observableArray([]);
    var workORderList = ko.observableArray([]);
    var srDataArray = ko.observableArray([]);
    var srOnhandDataArray = ko.observableArray([]);
    var clientInfo = ko.observableArray([]);
    var srProblemType = ko.observableArray([]);
    var srPriorityType = ko.observableArray([]);
    var assetIdList = "";
    var woIdList = "";
    var sparePartsCount = 0;
    var staffListLOV = ko.observableArray([]);
    var costCenterListLOV = ko.observableArray([]);
    var workOrderInfo = ko.observableArray("");
    var assetInfoData = "";

    self.assetPMSchedule = {
        count: ko.observable()
    };
    self.processInfo = {
        filetype: ko.observable(''),
        statusBy: ko.observable(''),
        StatusDate: ko.observable(''),
        category: ko.observable(''),
        refid: ko.observable(''),
        filename: ko.observable(''),
        process: ko.observable('')
    };
    self.searchKeyword = ko.observable('');
    self.searchAssetFilter = {
        status: ko.observable('I'),
        staffId: ko.observable(''),
        assetNo: ko.observable(''),
        groupCode: ko.observable('')
    };
    self.searchFilter = {
        fromDate: ko.observable(''),
        toDate: ko.observable(''),
        status: ko.observable(''),
        problemType: ko.observable(''),
        priority: ko.observable(''),
        jobType: ko.observable(''),
        userConstCenter: ko.observable(''),
        woIdList: ko.observable(''),
        assignTo: ko.observable(''),
        assignType: ko.observable(''),
        unAssigned: ko.observable(''),
        overDue: ko.observable(''),
        pmtype: ko.observable(''),
        searchTxt: ko.observable(''),
        SupplierStr: ko.observable(''),
        CostCenterStr: ko.observable(''),
        staffStatus: ko.observable('')

    };

    var SfromDate = null;
    var StoDate = null;
    var Sstatus = null;
    var SproblemType = null;
    var Spriority = null;
    var SjobType = null;
    var SassignTo = null;
    var SassignType = null;
    var Spmtype = null;
    var SsearchTxt = null;
    self.clearFilter = function () {
        self.searchFilter.toDate('');
        self.searchFilter.fromDate('');
        self.searchFilter.status = "";
        self.searchFilter.problemType = "";
        self.searchFilter.assignType = "";
        self.searchFilter.jobType = "";
        self.searchFilter.assignTo = "";
        self.searchFilter.priority = "";
        self.searchFilter.SupplierStr = "";
        self.searchFilter.CostCenterStr = "";
        self.searchFilter.staffStatus = "";
        $('#requestFromDate').val('');
        $('#requestStartDate').val('');
        $("#filterStatus").val(null);
        $("#filterProblemType").val(null);
        $("#filterJobType").val(null);
        $("#filterAssignType").val(null);
        $("#filterAssignTo").val(null);
        $("#filterPriority").val(null);
        $("#filterPMType").val(null);
        self.searchFilter.pmtype = "";

        $("#filterCostCenter").val(null);
        $("#filterSupplier").val(null);


        //window.location.href = ah.config.url.modalClose;
    }
    self.searchFilterNow = function () {
        window.location.href = ah.config.url.modalClose;
        self.searchNow();
    }
    self.dateClick = function () {
        $("#dateClickAction").val("ACTIVE");
    };
    self.searchKeyUp = function (d, e) {
        if (e.keyCode == 13) {
            self.searchNow();
        }
    }
    self.onScroll = function () {
        var elmnt = document.getElementById("searchResultList");
        var x = elmnt.scrollLeft;
        var y = elmnt.scrollTop;

    };
    self.woDetails = {
        WO_STATUS: ko.observable(),
        SPECIALTY: ko.observable(),
        SERV_DEPT: ko.observable(),
        REQ_TYPE: ko.observable(),
        REQ_REMARKS: ko.observable(),
        REQ_NO: ko.observable(0),
        REQ_DATE: ko.observable(),
        REQ_BY: ko.observable(),
        REQUESTER_CONTACT_NO: ko.observable(),
        PROBLEM: ko.observable(),
        JOB_URGENCY: ko.observable(),
        JOB_TYPE: ko.observable(),
        JOB_DURATION: ko.observable(),
        JOB_DUE_DATE: ko.observable(),
        ASSIGN_TYPE: ko.observable(),
        ASSIGN_TO: ko.observable(),
        ASSET_NO: ko.observable(),
        COST_CENTER: ko.observable(),
        DESCRIPTION: ko.observable(),
        LOCATION: ko.observable(),
        DEVICE_TYPE: ko.observable(),
        STATUS: ko.observable(),
        RESPONSIBLE_CENTER: ko.observable(),
        MODEL_NAME: ko.observable(),
        MODEL_NO: ko.observable(),
        SERIAL_NO: ko.observable(),
        WARRANTY_NOTES: ko.observable(),
        WARRANTY_INCLUDE: ko.observable(),
        WARRANTY_EXPIRYDATE: ko.observable(),
        TERM_PERIOD: ko.observable(),
        RISK_INCLUSIONFACTOR: ko.observable(),
        REQTICKETNO: ko.observable(),
        CLIENTNAME: ko.observable(),
        CLIENTADDRESS: ko.observable(),
        CLIENTCONTACT: ko.observable(),
        WOL_EMPLOYEE: ko.observable(),
        WOL_HOURS: ko.observable(),
        WOL_BILLABLE: ko.observable(),
        WOL_LABOUR_DATE: ko.observable(),
        WOL_ACTION: ko.observable(),
        COMPANY_LOGO: ko.observable(),
        CLIENT_LOGO: ko.observable(),
        HEADING: ko.observable(),
        HEADING2: ko.observable(),
        HEADING3: ko.observable(),
        HEADING_OTH: ko.observable(),
        HEADING_OTH2: ko.observable(),
        HEADING_OTH3: ko.observable(),
        MANUFACTURER: ko.observable(),
        WOACTIONLAST: ko.observable(),
        PM_TYPE: ko.observable(),
        INSERVICE_DATE: ko.observable(),
        PM_FREQUENCY: ko.observable(),
        FAULT_FINDING: ko.observable()
    };
    self.assetCategory = {
        count: ko.observable()
    };
    self.clientDetails = {
        CLIENTNAME: ko.observable(),
        CLIENT_LOGO: ko.observable(),
        COMPANY_LOGO: ko.observable(),
        HEADING: ko.observable(),
        HEADING2: ko.observable(),
        HEADING3: ko.observable(),
        HEADING_OTH: ko.observable(),
        HEADING_OTH2: ko.observable(),
        HEADING_OTH3: ko.observable(),
        CLIENTADDRESS: ko.observable(),
        CLIENTCONTACT: ko.observable()
    };

    var ah = new appHelper(systemText);

    self.showLoadingBackground = function () {
        var loadingBackground = $(ah.config.id.loadingBackground);
        loadingBackground.addClass('initializing').removeClass('done');
        var offset = loadingBackground.offset();
        var height = loadingBackground.height();
        var centerY = (offset.top + height / 2) - 120;
        $('.sk-circle').css({
            'margin-top': centerY + 'px',
            'display': 'block'
        });
    };
    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.toggleReadMode();
        var assetNoIDd = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        var dsdfx = ah.getParameterValueByName(ah.config.fld.reqWOAction);
        if (assetNoIDd !== null && assetNoIDd !== "") {

        } else {
            if (dsdfx === null || dsdfx === "") {
                $("#assetInfoDet").hide();


            }

        }
        sessionStorage.removeItem("searchResult");
        //if (self.validatePrivileges('40') === 0) {
        // $("#PrintBulkBtn").hide();
        //}
        $("#confirmBtn").hide();
        $("#confirmMSG").hide();
        $("#filterPMType").hide();
        $("#filterPMTypelable").hide();
        ah.initializeLayout();

        if (self.validatePrivileges('6') === 0) {
            $(ah.config.cls.liAddNew).hide();
        };
        $("#LOVASSIGNTO").hide();

        self.showLoadingBackground();
       
        postData = { LOV: "'AM_JOBTYPE','AIMS_SERVICEDEPARTMENT','AIMS_SPECIALTY','AIMS_PROBLEMTYPE','AIMS_PRIORITY'", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.when($.post(self.getApi() + ah.config.api.getListLOV, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus)).done(function () {
            $(ah.config.id.loadingBackground).removeClass('initializing').addClass('done');
            $('.dark-bg, ' + ah.config.id.loadingBackground).css({
                'top': '90px',
                'z-index': '0'
            });
        });


    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.createNew = function () {

        ah.toggleAddMode();
        sparePartsCount = 0;

        var jDate = new Date();
        var strDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        var reqDate = strDate + ' ' + hourmin;

        $(ah.config.id.searchTicket).show();
        $(ah.config.id.searchAsset).show();
        $("#REQ_DATE").val(reqDate);
        $("#JOB_DURATION").val("0");
        $("#WO_STATUS").val("NE");
        var assetNoIDd = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        if (assetNoIDd !== null && assetNoIDd !== "") {
            self.readAssetNo(assetNoIDd);
        }

        $(ah.config.id.searchResultListParts + ' ' + ah.config.tag.tbody).html('');

    };
    self.showModalFilter = function () {
        if (ah.CurrentMode == ah.config.mode.idle) {
            ah.CurrentMode = ah.config.mode.searchResult;
        }

        var element = document.getElementById("requestFromDate");
        element.classList.remove("view-mode");
        element.disabled = false;
        var element = document.getElementById("requestStartDate");
        element.classList.remove("view-mode");
        element.disabled = false;
        var element = document.getElementById("filterProblemType");
        element.classList.remove("view-mode");
        element.disabled = false;
        var element = document.getElementById("filterPMType");
        element.classList.remove("view-mode");
        element.disabled = false;
        var element = document.getElementById("filterStatus");
        element.classList.remove("view-mode");
        element.disabled = false;
        var element = document.getElementById("filterPriority");
        element.classList.remove("view-mode");
        element.disabled = false;
        var element = document.getElementById("filterJobType");
        element.classList.remove("view-mode");
        element.disabled = false;
        var element = document.getElementById("filterAssignTo");
        element.classList.remove("view-mode");
        element.disabled = false;
        var element = document.getElementById("filterAssignType");
        element.classList.remove("view-mode");
        element.disabled = false;

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));

        postData = { STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAdvanceLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);


        window.location.href = ah.config.url.modalFilter;
    };
    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.displayPMcol = function () {
        var problemType = $("#filterProblemType").val();
        if (problemType == 'PM') {
            $("#filterPMType").show();
            $("#filterPMTypelable").show();

        } else {
            $("#filterPMType").hide();
            $("#filterPMTypelable").hide();
            $("#filterPMType").val(null);
            self.searchFilter.pmtype = null;

        }
    }
    self.alertMessage = function (refMessage) {
        $("#alertmessage").text(refMessage);
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "block";

    };
    self.searchStaffLovAssignTo = function () {
        //
        if (staffListLOV.length > 0) {
            $("#LOVASSIGNTO").show();
        } else {
            var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
            var postData;
            var assignToLOV = $("#LOVASSIGNTODESC").val();
            postData = { SEARCH: assignToLOV, STAFF_LOGON_SESSIONS: staffLogonSessions };

            $.post(self.getApi() + ah.config.api.searchStaffList, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }

    };
    self.ValidateFirstName = function () {
        // $("#LOVASSIGNTO").hide();

    };
    self.printpageCount = function () {

    };
    self.searchResultHelpDesk = function (jsonData) {
        var sr = jsonData.AM_HELPDESKLIST;

        $(ah.config.id.searchHelpDeskResult + ' ' + ah.config.tag.ul).html('');



        if (sr.length > 0) {
            var htmlstr = '';
            $('#noresultHDR').hide();
            for (var o in sr) {

                var reqDate = ah.formatJSONDateTimeToString(sr[o].ISSUE_DATE);
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, JSON.stringify(sr[o]), "Ticket No.&nbsp; " + sr[o].TICKET_ID, "Issue Date&nbsp; " + reqDate, "");
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.problem, sr[o].ISSUE_DETAILS + "&nbsp;&nbsp; ");
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.priority, sr[o].PRIORITY + "&nbsp;&nbsp; ");


                htmlstr += '</li>';

            }


        }
        $(ah.config.id.searchHelpDeskResult + ' ' + ah.config.tag.ul).append(htmlstr);
        $(ah.config.cls.detailItem).bind('click', self.populateWOHelpDesk);

    };
    self.searchResult = function (jsonData) {

        var sr = jsonData.PMSCHEDULE

        $(ah.config.id.searchResultList + ' ' + ah.config.tag.tbody).html('');
        $(ah.config.id.printSearchResultList).html('');

        var searchCat = $("#sumCategory").val();

        $(ah.config.id.searchResultSummary).text('PM Schedule By Due Date');
        $(ah.config.id.searchResultSummaryCount).text('Number of Records: ' + sr.length);
        self.assetPMSchedule.count('No. of Records: ' + sr.length);

        $(ah.config.cls.liPrint).hide();
        if (sr.length > 0) {
            $(ah.config.cls.liPrint).show();
            var htmlstr = '';

            for (var o in sr) {
                htmlstr += "<tr>";

                htmlstr += '<td class="d">' + sr[o].ASSET_NO + '</td>';
                htmlstr += '<td class="h">' + sr[o].DESCRIPTION + '</td>';
                htmlstr += '<td class="a">' + sr[o].PM_TYPE + '</td>';
                htmlstr += '<td class="a">' + sr[o].NEXT_PM_DUE + '</td>';
                htmlstr += '<td class="a">' + sr[o].FREQUENCY_PERIOD + '</td>';
                htmlstr += '<td class="a">' + sr[o].INTERVAL + '</td>';
                htmlstr += '<td class="a">' + sr[o].DAYCOUNT + '</td>';
                htmlstr += '<td class="a">' + sr[o].LAST_REQ_NO + '</td>';
                htmlstr += '<td class="a">' + sr[o].LAST_WOREQDATE + '</td>';
                htmlstr += "</tr>";
            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.tbody).append(htmlstr);
            $(ah.config.id.printSearchResultList).html('');
            $(ah.config.id.printSearchResultList).append($(ah.config.id.searchResultList).html());

            var tableReference = ah.config.id.searchResultList + ' table';
            self.sortDataTable(tableReference, 1, 'asc');
        }
        ah.displaySearchResult();

    };
    self.getAssetNotes = function () {
        $("#notesHeader").text("Asset Record General Notes History");
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));

        var postData;

        var assetNo = $('#ASSET_NO').val();
        postData = { SEARCH: assetNo, STAFF_LOGON_SESSIONS: staffLogonSessions };

        $.post(self.getApi() + ah.config.api.searchAssetNotes, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.getWoNotes = function () {
        $("#notesHeader").text("Work Order Reference Note History");
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));

        var postData;

        var assetNo = $('#REQ_NO').val();
        postData = { SEARCH: assetNo, STAFF_LOGON_SESSIONS: staffLogonSessions };

        $.post(self.getApi() + ah.config.api.searchWONotes, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.newNote = function () {
        $("#WO_REFERENCE_NOTESREF").val(null);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        $("#WONOTE_RECORDED_BY").val(staffLogonSessions.STAFF_ID);
        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        strDateStart = strDateStart + ' ' + hourmin;
        $(ah.config.id.woReferenceNotes).prop(ah.config.attr.disabled, false);
        var woNotesInfo = staffLogonSessions.STAFF_ID + '@' + strDateStart;

        $("#woNotesEntered").text("Enter By: " + woNotesInfo);

        $("#WONOTE_RECORDED_DATE").val(strDateStart);
    };
    self.returnToAssetNotes = function () {
        $(ah.config.id.noteHistory).hide();
        $(ah.config.id.noteEntry).show();
    }
    self.populateWOHelpDesk = function () {
        var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfoId).toString());
        //alert(JSON.stringify(infoID));
        $(ah.config.id.searchAsset).hide();
        $("#TICKET_ID").val(infoID.TICKET_ID);
        $("#PROBLEM").val(infoID.ISSUE_DETAILS);
        $("#REQUESTER_CONTACT_NO").val(infoID.CONTACT_NO);
        $("#REQ_BY").val(infoID.FIRST_NAME + " " + infoID.LAST_NAME + "/" + infoID.CONTACT_PERSON) ;
        $("#REQUESTER_CONTACT_NO").val(infoID.CONTACT_NO);

        $("#ASSET_NO").val(infoID.ASSET_NO);
        $("#DESCRIPTION").val(infoID.ASSET_DESC);
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        self.searchAssetFilter.assetNo = infoID.ASSET_NO;
        
        postData = { SEARCH: "", ASSET_NO: infoID.ASSET_NO, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readAssetInfoV, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.pageLoader = function () {
        var senderID = $(arguments[1].currentTarget).attr('id');

        var searchStr = $(ah.config.id.txtGlobalSearchOth).val().toString();

        $("#noresult").hide();
        self.searchAssetFilter.groupCode("");
        switch (senderID) {

            case ah.config.tagId.nextPage: {
                var sr = assetList;
                var lastPage = Math.ceil(sr.length / 10)

                if (pageCount < lastPage) {
                    pageNum = pageNum + 10;
                    pageCount++;
                    self.searchItemsResult(searchStr, pageNum);
                }

                break;
            }
            case ah.config.tagId.priorPage: {

                if (pageNum === 0) {
                } else {
                    pageNum = pageNum - 10;
                    if (pageNum < 0) {
                        pageNum = 0;
                    }
                    pageCount = pageCount - 1;
                }

                self.searchItemsResult(searchStr, pageNum);
                break;
            }

            case ah.config.tagId.txtGlobalSearchOth: {

                self.assetSearchListNow();

                break;
            }
            case ah.config.tagId.assetAll: {

                self.assetSearchListNow();

                break;
            }
            case ah.config.tagId.assetMultiCC: {
                self.searchAssetFilter.groupCode("MULTICC");
                self.assetSearchListNow();

                break;
            }
        }

        $(ah.config.id.loadNumber).text(pageCount.toString());
        $(ah.config.id.pageCount).val(pageCount.toString());
        $(ah.config.id.pageNum).val(pageNum);

    };
    self.othInfoLoader = function () {
        var infoDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.assetDetails));

        ah.togglehideOthInfo();
        $(ah.config.id.noteHistory + ',' + ah.config.id.addButtonIcon).hide();
        var senderID = $(arguments[1].currentTarget).attr('id');
        var assetDescription = $("#DESCRIPTION").val();
        if (assetDescription.length > 60) {
            assetDescription = assetDescription.substring(0, 60) + '...';
        }
        $("#itemDescription").text(assetDescription);
        $("#costHeader").hide();
        $(ah.config.id.noteEntry).show();
        $(ah.config.id.noteHistory).hide();
        switch (senderID) {
            case ah.config.tagId.btnContract: {
                ah.toggleShowInfo1();
                $("#winTitleInfo").text("CONTRACT LIST");
                $("#costHeader").show();
                self.searchContractNow();
                window.location.href = ah.config.url.modalAddItemsInfo;
                break;
            }
            case ah.config.tagId.btnLogHistory: {
                ah.toggleShowInfo7();
                $("#winTitleInfo").text("Status Log");
                $("#costHeader").show();
                self.searchStatusLogNow();
                window.location.href = ah.config.url.modalAddItemsInfo;
                break;
            }
            case ah.config.tagId.btnpdf: {
                ah.toggleShowInfo6();
                $("#winTitleInfo").text("Supporting Documents");
                $("#OTHSAVE").val("DOCS");
                var woStatus = $("#WO_STATUS").val();
                $("#costHeader").show();
                $(ah.config.id.addButtonIcon).show();
                $(ah.config.id.supportingDocs + ' ' + ah.config.tag.ul).html('');
                var sr = srDataArray;
                if (sr.length > 0) {

                    var htmlstr = '';

                    for (var o in sr) {

                        htmlstr += '<li>';
                        htmlstr += ah.formatString(ah.config.html.searchRowHeaderDOCTemplateList, sr[o].FILE_NAME, sr[o].FILE_NAME, '', '');

                        htmlstr += '</li>';

                    }


                    $(ah.config.cls.detailItem).bind('click', self.readDoc);
                }
                $(ah.config.id.supportingDocs + ' ' + ah.config.tag.ul).append(htmlstr);
                if (self.validatePrivileges('20') === 0 && woStatus === 'CL') {
                    $(ah.config.id.addButtonIcon + ',' + ah.config.id.newNotes).hide();
                    $("#uploadPDF").hide();
                }

                window.location.href = ah.config.url.modalAddItemsInfo;
                break;
            }
            case ah.config.tagId.btnLCycle: {
                ah.toggleShowInfo2();
                $("#winTitleInfo").text("WARRANTY");
                window.location.href = ah.config.url.modalAddItemsInfo;
                break;
            }
            case ah.config.tagId.btnHOperation: {
                ah.toggleShowInfo3();
                $("#winTitleInfo").text("HOURS OF OPERATION");
                window.location.href = ah.config.url.modalAddItemsInfo;
                break;
            }
            case ah.config.tagId.btnANotes: {
                ah.toggleShowInfo4();
                $("#winTitleInfo").text("NOTES");
                $("#OTHSAVE").val("NOTES");
                $(ah.config.id.addButtonIcon).show();

                var woStatus = $("#WO_STATUS").val();
                if (self.validatePrivileges('20') === 0 && woStatus === 'CL') {

                    $(ah.config.id.addButtonIcon + ',' + ah.config.id.newNotes).hide();
                }

                window.location.href = ah.config.url.modalAddItemsInfo;
                break;
            }
            case ah.config.tagId.btnAPersonnel: {
                ah.toggleShowInfo5();
                $("#winTitleInfo").text("ASSIGNED SERVICE PERSONNEL");
                window.location.href = ah.config.url.modalAddItemsInfo;
                break;
            }
        }
    };
    self.readDoc = function () {

        var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var mywin = window.open("/UploadedFiles/" + infoID, "width=960,height=764");
    };
    self.assetSearchNow = function () {
        $("#page1").hide();
        $("#page2").hide();
        $("#pageSeparator").hide();
        $("#nextPage").hide();
        $("#priorPage").hide();
        $("#noresult").hide();
        $("#searchingAssetLOV").hide();
        window.location.href = ah.config.url.modalAssetSearch;

    };
    self.SearchNowHelpDesk = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        //self.searchFilter.status();

        //ah.toggleSearchMode();
        if (self.validatePrivileges('21') === 0) {
            self.searchAssetFilter.staffId = staffDetails.STAFF_ID;
        }
        postData = { MENU_GROUP: "", SYSTEMID: "", SEARCH: "", SEARCH_FILTER: self.searchAssetFilter, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchHelpDeskNew, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = ah.config.url.modalhdr;

    };
    self.workOrderLabour = function () {

        var assetNoIDd = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        var xsersdfsdf = "";
        if (assetNoIDd === null || assetNoIDd === "") {
            xsersdfsdf = "sdfsdfxdfwefsd";
        }
        var assetNo = "";
        var assetNoDesc = ""

        assetNo = $("#ASSET_NO").val();
        assetNoDesc = $("#DESCRIPTION").val();

        var reqWoNO = ah.getParameterValueByName(ah.config.fld.reqNo);

        var assignToName = $("#ASSIGN_TONAME").val();
        var assignToOutsource = $("#ASSIGN_OUTSOURCEDESC").val();

        window.location.href = encodeURI(ah.config.url.aimWorkOrderLabour + "?ASSETNO=" + assetNo + "&ASSETDESC=" + assetNoDesc + "&REQNO=" + workOrderInfo.REQ_NO + "&ASSIGNTO=" + workOrderInfo.ASSIGN_TO + "&ASSIGNDESC=" + assignToName + "&REQTOR=" + workOrderInfo.REQ_BY + "&REQCONTACT=" + workOrderInfo.REQUESTER_CONTACT_NO + "&REQDATE=" + workOrderInfo.REQ_DATE + "&REQTYPE=" + workOrderInfo.REQ_TYPE + "&REQSTATUS=" + workOrderInfo.WO_STATUS + "&REQWOAC=" + xsersdfsdf + "&REQDESC=" + workOrderInfo.PROBLEM);
    };
    self.modifySetup = function () {

        ah.toggleEditMode();
        if (self.validatePrivileges('23') === 0) {

            $(ah.config.id.closeDate).prop(ah.config.attr.readOnly, true);
        } else {
            $(ah.config.id.closeDate).prop(ah.config.attr.readOnly, false);
        }
        var woStatus = $("#WO_STATUS").val();
        if (self.validatePrivileges('20') === 0 && woStatus === 'CL') {

            $(ah.config.id.woStatus).prop(ah.config.attr.disabled, true);
        }
        var ticketNo = $("#TICKET_ID").val();
        if (ticketNo > 0) {
            $(ah.config.id.searchTicket).hide();
            $(ah.config.id.searchAsset).hide();
        } else {
            $(ah.config.id.searchTicket).show();
            $(ah.config.id.searchAsset).show();
        }

        if (self.validatePrivileges('33') === 0) {
            document.getElementById("ASSIGN_TO").disabled = true;
            $("#searchStaffId").hide();
        }

        //$('#ASSIGN_TO option').remove();

        //document.getElementById("ASSIGN_TO").options.length = 0;
        //var currAssign = $('#ASSIGN_TO').val();
        //$('#ASSIGN_TO').empty();
        //var srreload = srStaffList;       

        //var srreload = srreload.filter(function (item) { return (item.STATUS === "ACTIVE" && item.SYSTEM_LOOKUP == "Y") || item.STAFF_ID == currAssign });

        //if (srreload.length > 0) {
        //    $(ah.config.id.staffAssignTo)
        //        .append($("<option></option>")
        //            .attr("value", "")
        //            .text("-select-"));

        //    $.each(srreload, function (item) {

        //        $(ah.config.id.staffAssignTo)
        //            .append($("<option></option>")
        //                .attr("value", srreload[item].STAFF_ID)
        //                .text(srreload[item].FIRST_NAME + ' ' + srreload[item].LAST_NAME));
        //    });
        //}


        //$('#ASSIGN_TO').val(currAssign);
    };
    self.applyWONotes = function () {
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var othAction = $("#OTHSAVE").val();

        if (othAction === "NOTES") {
            $("#WONOTE_RECORDED_BY").val(staffLogonSessions.STAFF_ID);
            var jDate = new Date();
            var strDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();
        //    I suggest to Add ...     }
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }

            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);
            var modifyBy = staffLogonSessions.STAFF_ID;
            var modifyDate = strDate + ' ' + hourmin;
            //$("#WONOTE_RECORDED_DATE").val(modifyDate);
            var refNotes = $("#WO_REFERENCE_NOTESREF").val();
            if (refNotes === null || refNotes === "") {

                alert('Please enter a valid notes to proceed.');
                return;
            }
            $("#WO_REFERENCE_NOTES").val(refNotes);

            $("#SAVEACTION").val('SAVEINFO');
            $(ah.config.id.saveInfo).trigger('click');
        } else {

            var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
            var setupDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var postData;
            ah.toggleSaveMode();
            var myFile = document.getElementById('pdffile').value;
            var myFileData = document.getElementById('pdffile');

            var fName = null;
            //alert(myFile.length);
            if (myFile.length > 0) {
                var fName = myFileData.files[0].name;
            }
            var refId = $("#REQ_NO").val();
            self.processInfo.statusBy = staffLogonSessions.STAFF_ID;
            self.processInfo.filename = fName;
            self.processInfo.filetype = 'pdf';
            self.processInfo.category = 'ASSETWO';
            self.processInfo.refid = refId;
            var jDate = new Date();
            var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);

            self.processInfo.StatusDate = strDateStart + ' ' + hourmin;
            postData = { MODIFYINFO: self.processInfo, SCANDOCS: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            // alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.createDocs, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

        }
    };
    self.cancelForCollection = function (dataContext) {
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var jDate = new Date();
        var strDate = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        var cancelCollectDate = strDate + ' ' + hourmin;


        dataContext.TRANSFER_FLAG = "CFC";
        dataContext.FOR_COLLECTION_CANCELDATE = cancelCollectDate;
        dataContext.FOR_COLLECTION_CANCELBY = staffLogonSessions.STAFF_ID;

        $("#CANCELCOLLECT").text("You are above to cancel for collection spare part " + dataContext.ID_PARTS + " " + dataContext.PRODUCT_DESC + ". Click Apply button to save your changes.");
        $("#CANCELCOLLECT").show();
        $("#receivepartBtn").hide();
        $("#confirmBtn").hide();
        $("#NOREC").hide();
    };

    self.printWorkOrder = function (Idx) {
        self.isPrint = Idx;



        //ah.CurrentMode = ah.config.mode.searchResult;



        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;
        document.getElementById("printSummary").classList.remove('printSummary');
        document.getElementById("printDetails").classList.add('printSummary');
        document.getElementById("printBulk").classList.remove('printSummary');
        var reqId = document.getElementById("REQ_NO").value;
        postData = { CLIENTID: staffDetails.CLIENT_CODE, SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), REQNO: reqId, STAFF_LOGON_SESSIONS: staffLogonSessions, SEARCH_FILTER: self.searchFilter, STATUS: "" };
        $.post(self.getApi() + ah.config.api.searchwoMaterial, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        //window.print();
    };
    self.printBulk = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;
        self.searchFilter.woIdList(woIdList);
        postData = { CLIENTID: staffDetails.CLIENT_CODE, SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions, SEARCH_FILTER: self.searchFilter };
        $.post(self.getApi() + ah.config.api.serachWoList, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = ah.config.url.openModalPrintPage;
    };
    self.checkProblemType = function () {
        var problemType = $("#REQ_TYPE").val();
        if (problemType == 'SR') {
            $("#ASSET_NO").attr("required", false);
            $("#searchAsset").hide();
            $("#ASSIGN_TYPE").val("PERSON");
            $("#ASSIGN_TYPE").attr("required", false);
            $("#JOB_URGENCY").attr("required", false);
            $("#JOB_DUE_DATE").attr("required", false);
            $("#JOB_URGENCY").val(null);
            $("#JOB_DUE_DATE").val(null);
            $("#JOB_DURATION").val(null);

        } else {
            $("#ASSET_NO").attr("required", true);
            $("#ASSIGN_TYPE").attr("required", true);
            $("#JOB_URGENCY").attr("required", true);
            $("#JOB_DUE_DATE").attr("required", true);
            $("#searchAsset").show();
            var wodefaultpriority = srProblemType.find(function (i) { return i.LOV_LOOKUP_ID == problemType });
            if (wodefaultpriority != null) {
                var wodefaultduration = srPriorityType.find(function (i) { return i.LOV_LOOKUP_ID == wodefaultpriority.WO_DURATION });
                if (wodefaultduration != null && wodefaultduration != "") {
                    if (wodefaultduration.WO_DURATION > 0) {
                        $("#JOB_URGENCY").val(wodefaultduration.WO_DURATION);
                        self.calculateDueDate(wodefaultduration.WO_DURATION);
                    } else {
                        $("#JOB_URGENCY").val(null);
                        $("#JOB_DUE_DATE").val(null);
                        $("#JOB_DURATION").val(null);
                    }
                } else {
                    $("#JOB_URGENCY").val(null);
                    $("#JOB_DUE_DATE").val(null);
                    $("#JOB_DURATION").val(null);
                }


            } else {
                $("#JOB_URGENCY").val(null);
                $("#JOB_DUE_DATE").val(null);
                $("#JOB_DURATION").val(null);
            }
        }


    };

    self.calculateDueDate = function (woDuration) {
        var reqdate = $("#REQ_DATE").val();

        var new_date = moment(reqdate, "DD-MM-YYYY").add(woDuration, 'days');
        new_date = moment(new_date).format('DD/MM/YYYY');
        $("#JOB_DURATION").val(woDuration);
        $("#JOB_DUE_DATE").val(new_date);

    };

    self.calculateDurationByDueDate = function () {
        var duedate = $("#JOB_DUE_DATE").val();
        var reqdate = $("#REQ_DATE").val();

    };
    self.calculateDurationByPriority = function () {
        var prorityType = $("#JOB_URGENCY").val();
        var wodefaultdurationpriority = srPriorityType.find(function (i) { return i.LOV_LOOKUP_ID == prorityType });
        if (wodefaultdurationpriority != null) {
            if (wodefaultdurationpriority.WO_DURATION > 0) {
                self.calculateDueDate(wodefaultdurationpriority.WO_DURATION);
            } else {
                $("#JOB_DUE_DATE").val(null);
                $("#JOB_DURATION").val(null);
            }

        }
    };
    self.getspareParts = function () {
        var reqID = this.getAttribute(ah.config.attr.datainfoId).toString();

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        postData = { CLIENTID: staffDetails.CLIENT_CODE, SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), REQNO: reqID, STAFF_LOGON_SESSIONS: staffLogonSessions, SEARCH_FILTER: self.searchFilter, STATUS: "" };
        $.post(self.getApi() + ah.config.api.searchwoMaterial, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.assignTypeChecker = function () {
        var assignType = $("#ASSIGN_TYPE").val();
        if (assignType == "COMPANY") {
            $(ah.config.id.assignOutsourceDev).show();
        } else {
            $(ah.config.id.assignOutsourceDev).hide();
        }
    };
    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.saveAllChanges = function () {
        $("#SAVEACTION").val('SAVEINFO');
        $(ah.config.id.saveInfo).trigger('click');

    };

    self.cancelChanges = function () {
        $("#REQ_ID").show();
        $("label[for='REQ_ID']").show();
        var assetNoIDx = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        $("#ASSET_NO+a").next("span").remove();
        var reqId = $("#REQ_NO").val();
        if (reqId === null || reqId === "") {
            if (assetNoIDx !== null && assetNoIDx !== "") {
                $("#infoID").text(assetNoID + "-" + assetDescription);
                $("#assetInfobtn").show();
                $("#assetInfoDet").hide();
                self.searchNow();
            } else {
                ah.initializeLayout();
            }

        } else {
            ah.toggleDisplayMode();
            self.readWOId(reqId);
        }

    };
    self.cancelChangesModal = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            //ah.toggleDisplayMode();
            //self.showDetails();
        }
        window.location.href = ah.config.url.modalClose;

    };
    self.infoLovLoader = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        var senderID = $(arguments[1].currentTarget).attr('id');
        $("#searchingLOVList").hide();
        $("#lookUpnoresult").hide();
        $("#searchAllLOVList").hide();
        $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');
        var searchStr = $(ah.config.id.lookUptxtGlobalSearchOth).val();
        switch (senderID) {
            case ah.config.tagId.searchSupplierId: {
                $(ah.config.id.lookUptxtGlobalSearchOth).val("");
                $("#LOVBYACION").val("SUPPIER");
                $("#searchAllLOVList").show();
                $("#lookUpTitle").text("Please search/select from active supplier list");
                break;
            }
            case ah.config.tagId.searchStaffId: {
                $(ah.config.id.lookUptxtGlobalSearchOth).val("");
                $("#lookUpTitle").text("Please select from active staff list");
                $("#LOVBYACION").val("STAFF");
                postData = { SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
                $.post(self.getApi() + ah.config.api.searchStaffList, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
                break;
            }
            case ah.config.tagId.searchCostCenterId: {
                $(ah.config.id.lookUptxtGlobalSearchOth).val("");
                $("#lookUpTitle").text("Please select from active Cost Center list");
                $("#LOVBYACION").val("COSTCENTER");
                break;
            }
            case ah.config.tagId.lookUptxtGlobalSearchOth: {
                var lovByAction = $("#LOVBYACION").val();
                if (lovByAction == "SUPPIER") {

                    if (supplierListLOV.length > 0) {
                        self.searchResultSupplierListLov(searchStr);
                    } else {
                        self.supplierSearch();
                    }

                } else if (lovByAction == "STAFF") {
                    self.searchResultStaffListLov(searchStr);
                } else if (lovByAction == "COSTCENTER") {
                    if (costCenterListLOV.length > 0) {

                        self.searchResultCostCenterListLov(searchStr);
                    } else {
                        self.costCenterSearch();
                    }

                }
                break;
            }
        }
        window.location.href = ah.config.url.openModalLookup;
    };
    self.loadDataPrint = function (data) {
        // var infoAsset = JSON.parse(sessionStorage.getItem(ah.config.skey.assetItems));

        if (assetInfoData !== null) {
            if (assetInfoData.ASSET_NO !== null && assetInfoData.ASSET_NO !== "") {
                self.woDetails.ASSET_NO(assetInfoData.ASSET_NO);
                self.woDetails.RISK_INCLUSIONFACTOR(assetInfoData.RISK_INCLUSIONFACTOR);
                self.woDetails.COST_CENTER(assetInfoData.COSTCENTER_DESC);
                self.woDetails.MANUFACTURER(assetInfoData.MANUFACTURER_DESC);
                self.woDetails.DESCRIPTION(assetInfoData.DESCRIPTION);
                self.woDetails.LOCATION(assetInfoData.LOCATION);
                self.woDetails.DEVICE_TYPE(assetInfoData.DTYPE_DESC);
                self.woDetails.RESPONSIBLE_CENTER(assetInfoData.RESPONSIBLE_CENTER);
                self.woDetails.STATUS(assetInfoData.STATUS);
                self.woDetails.MODEL_NAME(assetInfoData.MODEL_NAME);
                self.woDetails.MODEL_NO(assetInfoData.MODEL_NO);
                self.woDetails.SERIAL_NO(assetInfoData.SERIAL_NO);
                self.woDetails.WARRANTY_NOTES(assetInfoData.WARRANTY_NOTES);
                self.woDetails.WARRANTY_INCLUDE(assetInfoData.WARRANTY_DESC);
                self.woDetails.WARRANTY_EXPIRYDATE(ah.formatJSONDateTimeToString(assetInfoData.WARRANTY_EXPIRYDATE));
                self.woDetails.TERM_PERIOD(assetInfoData.TERM_PERIOD + ' ' + assetInfoData.WARRANTY_FREQUNIT);

                self.woDetails.PM_TYPE(assetInfoData.PM_TYPE);
                self.woDetails.INSERVICE_DATE(ah.formatJSONDateToString(assetInfoData.INSERVICE_DATE));
                self.woDetails.PM_FREQUENCY(assetInfoData.FREQUENCY_PERIOD + ' ' + assetInfoData.INTERVAL);
            }
        }
        var costCenter = $("#COST_CENTER").val();
        self.woDetails.COST_CENTER(costCenter);

        var faultFinding = $("#FAULT_FINDING").val();
        self.woDetails.FAULT_FINDING(faultFinding);
        //warranty
        var jsonObj = ko.observableArray([]);
        jsonObj = ko.utils.stringifyJson(self.warrantyData);
        var sr = JSON.parse(jsonObj);
        $("#warranty2").hide();
        $("#warranty3").hide();
        $("#warrantyNotes").text("");
        $("#warrantyInclude").text("");
        $("#warrantyExpiry").text("");
        $("#warrantyTerm").text("");
        if (sr.length) {
            for (var o in sr) {
                if (o == 0) {
                    $("#warrantyNotes").text(sr[o].WARRANTY_NOTES);
                    $("#warrantyInclude").text(sr[o].WARRANTY_INCLUDED);
                    $("#warrantyExpiry").text(sr[o].WARRANTY_EXPIRY);
                    $("#warrantyTerm").text(sr[o].FREQUENCY_PERIOD + ' ' + sr[o].FREQUENCY);
                }
                if (o == 1) {
                    $("#warranty2").show();
                    $("#warrantyNotes2").text(sr[o].WARRANTY_NOTES);
                    $("#warrantyInclude2").text(sr[o].WARRANTY_INCLUDED);
                    $("#warrantyExpiry2").text(sr[o].WARRANTY_EXPIRY);
                    $("#warrantyTerm2").text(sr[o].FREQUENCY_PERIOD + ' ' + sr[o].FREQUENCY);
                }
                if (o == 2) {
                    $("#warranty3").show();
                    $("#warrantyNotes3").text(sr[o].WARRANTY_NOTES);
                    $("#warrantyInclude3").text(sr[o].WARRANTY_INCLUDED);
                    $("#warrantyExpiry3").text(sr[o].WARRANTY_EXPIRY);
                    $("#warrantyTerm3").text(sr[o].FREQUENCY_PERIOD + ' ' + sr[o].FREQUENCY);
                }

            }
        }
        var x = document.getElementById("WO_STATUS").selectedIndex;
        var y = document.getElementById("WO_STATUS").options;
        var yDesc = y[x].text;
        self.woDetails.WO_STATUS(yDesc);

        var woStatus = $("#WO_STATUS").val();

        if (woStatus == "CL") {

            var closeDate = $("#CLOSED_DATE").val();

            $("#closeDateText").text(closeDate);
        } else {
            $("#closeDateText").text("");
        }
        var assignToName = $("#ASSIGN_TONAME").val();
        //var ax = document.getElementById("ASSIGN_TO").selectedIndex;
        //var ay = document.getElementById("ASSIGN_TO").options;
        //var ayDesc = ay[ax].text;   
        //if (ayDesc === "- select -") {
        //    ayDesc = "";
        //}
        self.woDetails.ASSIGN_TO(assignToName);

        var ax = document.getElementById("ASSIGN_TYPE").selectedIndex;
        var ay = document.getElementById("ASSIGN_TYPE").options;

        //if (ax !== -1) {
        //    var ayDesc = ay[ax].text;
        //    self.woDetails.ASSIGN_TYPE(ayDesc);
        //}

        if (ax === -1) {

        } else {
            var ayDesc = ay[ax].text;
            self.woDetails.ASSIGN_TYPE(ayDesc);
        }
        ayDesc = document.getElementById("JOB_DUE_DATE").value;
        self.woDetails.JOB_DUE_DATE(ayDesc);

        //self.woDetails.JOB_DURATION(data.JOB_DURATION);
        var ax = document.getElementById("JOB_TYPE").selectedIndex;
        var ay = document.getElementById("JOB_TYPE").options;
        if (ax === -1) {

        } else {
            var ayDesc = ay[ax].text;

            if (ayDesc === "- select -") {
                ayDesc = "";
            }

            self.woDetails.JOB_TYPE(ayDesc);
        }
        //var ayDesc = ay[ax].text; 

        //if (ayDesc === "- select -") {
        //    ayDesc = "";
        //}
        self.woDetails.JOB_TYPE(ayDesc);

        ax = document.getElementById("JOB_URGENCY").selectedIndex;
        ay = document.getElementById("JOB_URGENCY").options;
        if (ax === -1) {

        } else {
            ayDesc = ay[ax].text;
            if (ayDesc === "- select -") {
                ayDesc = "";
            }
        }
        self.woDetails.JOB_URGENCY(ayDesc);

        ayDesc = document.getElementById("PROBLEM").value;
        self.woDetails.PROBLEM(ayDesc);
        ayDesc = document.getElementById("REQ_BY").value;
        //self.woDetails.REQUESTER_CONTACT_NO(data.REQUESTER_CONTACT_NO);
        self.woDetails.REQ_BY(ayDesc);

        ayDesc = document.getElementById("REQUESTER_CONTACT_NO").value;
        self.woDetails.REQUESTER_CONTACT_NO(ayDesc);

        ayDesc = document.getElementById("REQ_DATE").value;
        self.woDetails.REQ_DATE(ayDesc);
        ayDesc = document.getElementById("REQ_NO").value;
        self.woDetails.REQ_NO(ayDesc);

        ayDesc = document.getElementById("TICKET_ID").value;
        self.woDetails.REQTICKETNO(ayDesc);
        //self.woDetails.REQ_REMARKS(data.REQ_REMARKS);

        ax = document.getElementById("REQ_TYPE").selectedIndex;
        ay = document.getElementById("REQ_TYPE").options;
        ayDesc = ay[ax].text;
        self.woDetails.REQ_TYPE(ayDesc);

        ax = document.getElementById("SERV_DEPT").selectedIndex;
        ay = document.getElementById("SERV_DEPT").options;
        ayDesc = ay[ax].text;
        if (ayDesc === "- select -") {
            ayDesc = "";
        }

        self.woDetails.SERV_DEPT(ayDesc);
        //self.woDetails.SPECIALTY(data.SPECIALTY);
    };
    self.getAssetEmp = function () {

        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var assignType = $("#ASSIGN_TYPE").val();

        if (assignType === "PERSON") {
            $("#ASSIGN_TO").val(staffDetails.STAFF_ID);
        } else {
            $("#ASSIGN_TO").val(null);
        }
    };
    self.showDetails = function (isNotUpdate, jsonData) {
        ah.toggleDisplayMode();
        sparePartsCount = 0;
        // alert(self.searchFilter.status);

        if (isNotUpdate) {
            var jsetupDetails = jsonData.AM_WORK_ORDERS;//sessionStorage.getItem(ah.config.skey.woDetails);
            workOrderInfo = jsetupDetails;
            //var jsetupDetails = infoDetails;
            //alert(JSON.stringify(jsetupDetails));


            if (jsetupDetails.ASSIGN_TYPE == "COMPANY") {
                $(ah.config.id.assignOutsourceDev).show();
            } else {
                $(ah.config.id.assignOutsourceDev).hide();
            }

            var strTime = '';

            //alert(jsetupDetails.REQ_DATE);
            var reqDate = ah.formatJSONDateTimeToString(jsetupDetails.REQ_DATE);
            var closeDate = ah.formatJSONDateTimeToString(jsetupDetails.CLOSED_DATE);
            jsetupDetails.REQ_DATE = reqDate;
            jsetupDetails.CLOSED_DATE = closeDate;

            ah.ResetControls();
            $("#CURRSTATUS").val(jsetupDetails.WO_STATUS);
            var infoAsset = jsonData.AM_ASSET_DETAILS;//JSON.parse(sessionStorage.getItem(ah.config.skey.assetItems));
            assetInfoData = infoAsset;
           // alert(JSON.stringify(assetInfoData));
            if (infoAsset !== null) {
                self.populateAssetInfo(jsonData);
                //ah.LoadJSON(infoAsset);
                //$("#ASSETSTATUS").val(infoAsset.STATUS);
            }
            if (jsetupDetails.REQ_TYPE == "IA") {
                $("#senEngrSig").hide();
                $("#senEngrSigLbl").hide();
            } else {
                $("#senEngrSig").show();
                $("#senEngrSigLbl").show();
            }
            //if (jsetupDetails.WO_STATUS == "CL") {
            $("#COST_CENTER").val(jsetupDetails.ASSET_COSTCENTER);
            $("#LOCATION").val(jsetupDetails.ASSET_LOCATION);
            //  }

            if (self.validatePrivileges('22') === 0 && jsetupDetails.WO_STATUS !== "CL") {
                $("#WO_STATUS option[value='CL']").remove();
                $("#lblClosedDate").hide();
                $("#CLOSED_DATE").hide();

            } else if (jsetupDetails.WO_STATUS == "CL") {

                $("#lblClosedDate").show();
                $("#CLOSED_DATE").show();
            } else {
                $("#lblClosedDate").hide();
                $("#CLOSED_DATE").hide();
            }

            if (jsetupDetails.AM_INSPECTION_ID === 0) {
                jsetupDetails.AM_INSPECTION_ID = null;
            }
            
            $("#WO_REFERENCE_NOTESREF").val(jsetupDetails.WO_REFERENCE_NOTES);
            if (jsetupDetails.WONOTE_RECORDED_DATE !== "" && jsetupDetails.WONOTE_RECORDED_DATE !== "null" && jsetupDetails.WONOTE_RECORDED_DATE !== "0001-01-01T00:00:00" && jsetupDetails.WONOTE_RECORDED_DATE !== "1900-01-01T00:00:00") {
                var woNotesInfo = ah.formatJSONDateTimeToString(jsetupDetails.WONOTE_RECORDED_DATE);
                woNotesInfo = jsetupDetails.WONOTE_RECORDED_BY + '@' + woNotesInfo;

                $("#woNotesEntered").text("Last Entered By: " + woNotesInfo);
            }
            ah.LoadJSON(jsetupDetails);

            if (jsetupDetails.REQ_TYPE == 'CM') {
                $(ah.config.id.OutOrderSticker).show();
            } else {
                $(ah.config.id.OutOrderSticker).hide();
            }
            if (jsetupDetails.REQ_TYPE == 'IA') {
                $(ah.config.id.InWoSticker).show();
            } else {
                $(ah.config.id.InWoSticker).hide();
            }
            if (jsetupDetails.REQ_TYPE == 'PM') {
                $(ah.config.id.PMSticker).show();
            } else {
                $(ah.config.id.PMSticker).hide();
            }
           // alert(JSON.stringify(jsetupDetails));

            var staffInfo = jsonData.STAFF_INFO;

            if (staffInfo.length > 0) {
                $("#ASSIGN_TONAME").val(staffInfo[0].STAFF_FULLNAME);
            }
            var supplierInfo = jsonData.AM_SUPPLIERLIST;
            if (supplierInfo.length > 0) {
                $("#ASSIGN_OUTSOURCEDESC").val(supplierInfo[0].DESCRIPTION);
            }
            if (jsetupDetails.TICKET_ID > 0) {
                $(ah.config.id.searchTicket).hide();
                $(ah.config.id.searchAsset).hide();
            }
            $("#REQ_ID").val(jsetupDetails.REQ_NO);
            if (jsetupDetails.JOB_DUE_DATE == "" || jsetupDetails.JOB_DUE_DATE == "null" || jsetupDetails.JOB_DUE_DATE == "0001-01-01T00:00:00" || jsetupDetails.JOB_DUE_DATE == "1900-01-01T00:00:00") {
                //var jobDueDate = ah.formatJSONDateTimeToString("1900-01-01T00:00:00");
                $("#JOB_DUE_DATE").val("");
            }

            if (self.validatePrivileges('20') === 0 && jsetupDetails.WO_STATUS === 'CL') {

                $(ah.config.id.modifyButton).hide();
            }
            var warrantyList = jsonData.AM_ASSET_WARRANTY;
            self.warrantyData.splice(0, 5000);
            if (warrantyList.length > 0) {
                var warrantyDate = null;
                $("#currentRow").val(0);
                for (var o in warrantyList) {
                    if (o > 0) {
                        rowShow = false;
                    }
                    var warrantyDate = null;
                    warrantyDate = ah.formatJSONDateToString(warrantyList[o].WARRANTY_EXPIRY);
                    self.warrantyData.push({

                        FREQUENCY: warrantyList[o].FREQUENCY,
                        FREQUENCY_PERIOD: warrantyList[o].FREQUENCY_PERIOD,
                        WARRANTY_NOTES: warrantyList[o].WARRANTY_NOTES,
                        WARRANTY_EXPIRY: warrantyDate,
                        WARRANTY_INCLUDED: warrantyList[o].WARRANTYDESC,
                        ID: warrantyList[o].ID,
                        ASSET_NO: warrantyList[o].ASSET_NO,
                        WARRANTY_EXTENDED: warrantyList[o].WARRANTY_EXTENDED,
                        warrantyLOV: null

                    });
                }
            }
            //alert(JSON.stringify(jsonData.AM_PARTSREQUEST));
            //populate parts
            $(ah.config.id.searchResultListParts + ' ' + ah.config.tag.tbody).html('');
            var srparts = jsonData.AM_PARTSREQUEST;
            var htmlstr = ""

            if (srparts.length > 0) {
                sparePartsCount = 1;
                $(ah.config.id.searchResultListParts).show();
                for (var o in srparts) {
                    var qtyactu = 0;

                    if (jsetupDetails.PARTS_CONFIRMED == "Y") {
                        if (srparts[o].STATUS != "Received") {
                            qtyactu = srparts[o].QTY;
                        } else {
                            qtyactu = srparts[o].USED_QTY;
                        }

                    } else {
                        if (srparts[o].USED_QTY == 0 && srparts[o].STATUS == "Received") {
                            qtyactu = srparts[o].QTY;
                        } else {

                            if (srparts[o].USED_QTY > 0) {

                                qtyactu = srparts[o].USED_QTY;
                            } else {

                                qtyactu = srparts[o].QTY;
                            }

                        }
                    }

                    if (srparts[o].PRODUCT_DESC === null || srparts[o].PRODUCT_DESC === "") {
                        srparts[o].PRODUCT_DESC = srparts[o].DESCRIPTION;
                        srparts[o].ID_PARTS = 'Supplier'
                    }
                    htmlstr += "<tr>";
                    htmlstr += '<td class="a">' + srparts[o].ID_PARTS + '</td>';
                    htmlstr += '<td class="">' + srparts[o].PART_NUMBERS + '</td>';
                    htmlstr += '<td class="h">' + srparts[o].PRODUCT_DESC + '</td>';
                    htmlstr += '<td class="a">' + qtyactu + '</td>';
                    htmlstr += "</tr>";
                }

                $(ah.config.id.searchResultListParts + ' ' + ah.config.tag.tbody).append(htmlstr);
            } else {
                $(ah.config.id.searchResultListParts).hide();
            }
            $(ah.config.id.searchTicket).hide();
        }
        var assetNoIDd = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        var dfsdx = ah.getParameterValueByName(ah.config.fld.reqWOAction);

        if ((assetNoIDd !== null && assetNoIDd !== "" && assetNoIDd !== " ")) {
            $("#assetInfoDet").hide();
        } else {
            $("#assetInfobtn").hide();
            $("#assetInfoDet").show();
        }
        if (self.validatePrivileges('6') === 0) {
            $(ah.config.cls.liAddNew).hide();
        }
        //if (self.validatePrivileges('40') === 0) {
        //    $("#openModalQuickView").hide();
        //}
        if (self.validatePrivileges('5') === 0) {
            var reqType = $("#REQ_TYPE").val();
            if (reqType == "SR") {
                if (self.validatePrivileges('51') === 0) {
                    $(ah.config.id.modifyButton).hide();
                }
            } else {
                $(ah.config.id.modifyButton).hide();
            }
        }
        self.populateLastsearch();
    };

    self.showAssetDetails = function (jsonData) {
        //alert(isNotUpdate);
       
        var infoAsset = jsonData.AM_ASSET_DETAILS/*JSON.parse(sessionStorage.getItem(ah.config.skey.assetItems))*/;
           // var lov = jsonData.LOV_LOOKUPS;
          
            ah.LoadJSON(infoAsset);
            
            if (infoAsset.GROUP_CODE == "MULTICC"){
                $(ah.config.id.searchCostCenterId).show();
            } else {
                $(ah.config.id.searchCostCenterId).hide();
            }
            $("#COST_CENTER").val(infoAsset.COSTCENTER_DESC);
            $("#ASSET_COSTCENTERCODE").val(infoAsset.COST_CENTER);
            $("#CATEGORY").val(infoAsset.DTYPE_DESC);
            $("#ASSETSTATUS").val(infoAsset.STATUS);
            //if (lov !== null) {
            //    lov = sr.filter(function (item) { return item.CATEGORY === "AIMS_DTYPE" });

            //    if (sr.length > 0) {
            //        $("#CATEGORY").val(sr[0].DESCRIPTION);
            //    }
            //}
	};
	self.pagingLoader = function () {
		var senderID = $(arguments[1].currentTarget).attr('id');

		var pageNum = parseInt($(ah.config.id.PaginGpageNum).val());
		var pageCount = parseInt($(ah.config.id.PaginGpageCount).val());
        sr = workORderList;//JSON.parse(sessionStorage.getItem(ah.config.skey.woDetails));
		var lastPage = Math.ceil(sr.length / 500);

		if (isNaN(pageCount)) {
			pageCount = 1;
		}
		if (isNaN(pageNum)) {
			pageNum = 0;
		}

		switch (senderID) {

			case ah.config.tagId.PaginGnextPage: {

				if (pageCount < lastPage) {
					pageNum = pageNum + 500;
					pageCount++;
                    var loadcount = $("#LASTLOADPAGE").val();
                    loadcount = parseInt(loadcount) + 500;
                    $("#LASTLOADPAGE").val(loadcount);
                    self.searchResult(loadcount,sort);
				}
				break;
			}
			case ah.config.tagId.PaginGpriorPage: {

				if (pageNum === 0) {
				} else {
					pageNum = pageNum - 500;
					if (pageNum < 0) {
						pageNum = 0;
					}
					pageCount = pageCount - 1;
				}

                var loadcount = $("#LASTLOADPAGE").val();
                loadcount = parseInt(loadcount) - 500;
                if (loadcount < 0){
                    loadcount = 0
                }
                $("#LASTLOADPAGE").val(loadcount);


                self.searchResult(loadcount,sort);
				break;
			}

		}

		$("#PaginGloadNumber").text(pageCount.toString());
		$("#PaginGpageCount").val(pageCount.toString());
        $("#PaginGpageNum").val(pageNum,sort);

    };
    self.spartPartsLoader = function () {
        var senderID = $(arguments[1].currentTarget).attr('id');

        var searchStr = $("#txtGlobalSearchOthspartParts").val().toString();
        switch (senderID) {

            case ah.config.tagId.txtGlobalSearchOthspartParts: {
                pageCount = 1;
                pageNum = 0;
                $('#searchingLOV').show();
                    var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
                    var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

                    //ah.toggleSearchItems();
                    var wo_reqId = $("#REQ_ID").val();
                    postData = { SEARCH: searchStr, REQ_NO: wo_reqId, STAFF_LOGON_SESSIONS: staffLogonSessions };
                    $.post(self.getApi() + ah.config.api.searchPartsList, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
                    window.location.href = encodeURI(ah.config.url.modalAddItems);
                    $("#loadNumber1").text(1);
            
               // self.searchPartsMaterialOnhandResult(searchStr);
                break;
            }
        }
    };

    self.searchSpartParts = function () {
        $('#searchingLOV').show();
        var searchStr = $("#txtGlobalSearchOthspartParts").val().toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        //ah.toggleSearchItems();
        var wo_reqId = $("#REQ_ID").val();
        postData = { SEARCH: searchStr, REQ_NO: wo_reqId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchPartsList, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = encodeURI(ah.config.url.modalAddItems);
        $("#loadNumber1").text(1);
    };
    self.sorting = function () {
        if (workORderList.length > 0) {
            sortby = $("#SORTPAGE").val();
            self.searchResult(0, sortby);
        }
        
    };
    self.closePartsModal = function () {
        window.location.href = ah.config.url.modalClose;
      
        var refId = $("#REQ_ID").val();
       
        self.readWOId(refId);
    };
	self.searchResult = function (pageNum,sortby) {
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var sr = workORderList;//JSON.parse(sessionStorage.getItem(ah.config.skey.woDetails));
        if (sortby == 'SORTDATEDC' && pageNum == 0) {
         
            sr.sort(function (a, b) {

                if (a["REQ_DATE"] > b["REQ_DATE"])
                    return -1;
                if (a["REQ_DATE"] < b["REQ_DATE"])
                    return 1;
                return 0;
            });
        } else if (sortby == 'SORTDATEAC' && pageNum == 0){
            sr.sort(function (a, b) {

                if (a["REQ_DATE"] < b["REQ_DATE"])
                    return -1;
                if (a["REQ_DATE"] > b["REQ_DATE"])
                    return 1;
                return 0;
            });
        } else if (sortby == 'SORTASSETNOAC' && pageNum == 0) {
            sr.sort(function (a, b) {
                if (a["ASSET_NO"] < b["ASSET_NO"])
                    return -1;
                if (a["ASSET_NO"] > b["ASSET_NO"])
                    return 1;
                return 0;
            });
        } else if (sortby == 'SORTASSETNODC' && pageNum == 0) {
            sr.sort(function (a, b) {
                if (a["ASSET_NO"] > b["ASSET_NO"])
                    return -1;
                if (a["ASSET_NO"] < b["ASSET_NO"])
                    return 1;
                return 0;
            });
        } else if (sortby == 'SORTASSETDESCAC' && pageNum == 0) {
        sr.sort(function (a, b) {
            var textA = a.DESCRIPTION.toUpperCase();
            var textB = b.DESCRIPTION.toUpperCase();
            textA = textA.substring(0, 3);
            textB = textB.substring(0, 3);
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });
        } else if (sortby == 'SORTASSETDESCDC' && pageNum == 0) {
            sr.sort(function (a, b) {
                var textA = a.DESCRIPTION.toUpperCase();
                var textB = b.DESCRIPTION.toUpperCase();
                textA = textA.substring(0, 3);
                textB = textB.substring(0, 3);
                return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
            });
        } else if (sortby == 'SORTCOSTCENTERAC' && pageNum == 0) {
            sr.sort(function (a, b) {
                var textA = a.COSTCENTERNAME.toUpperCase();
                var textB = b.COSTCENTERNAME.toUpperCase();
                textA = textA.substring(0, 3);
                textB = textB.substring(0, 3);
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });
        }   else if (sortby == 'SORTCOSTCENTERDC' && pageNum == 0) {
            sr.sort(function (a, b) {
                var textA = a.COSTCENTERNAME.toUpperCase();
                var textB = b.COSTCENTERNAME.toUpperCase();
                textA = textA.substring(0, 3);
                textB = textB.substring(0, 3);
                return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
            });
 
        } else if (sortby == 'SORTSUPPLIERAC' && pageNum == 0) {
            sr.sort(function (a, b) {
                var textA = a.SUPPIERNAME.toUpperCase();
                var textB = b.SUPPIERNAME.toUpperCase();
                textA = textA.substring(0, 3);
                textB = textB.substring(0, 3);
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });
        } else if (sortby == 'SORTSUPPLIERDC' && pageNum == 0) {
            sr.sort(function (a, b) {
                var textA = a.SUPPIERNAME.toUpperCase();
                var textB = b.SUPPIERNAME.toUpperCase();
                textA = textA.substring(0, 3);
                textB = textB.substring(0, 3);
                return (textA > textB) ? -1 : (textA < textB) ? 1 : 0;
            });
        }
       
        if (pageNum == 0) {
            $(ah.config.id.loadNumber).text(1);
        }
      
        if (staffLogonSessions.STAFF_ID == "ADMIN") {
            //alert(JSON.stringify(sr));
        }

        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        self.assetCategory.count("No. of Records: " + sr.length.toString());

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));
        $("#PrintBulkBtn").hide();
        if (sr.length > 0) {

            self.woDetails.CLIENTNAME(clientInfo.DESCRIPTION);
            self.woDetails.CLIENT_LOGO(clientInfo.CLIENT_LOGO);
            self.woDetails.COMPANY_LOGO(clientInfo.COMPANY_LOGO);
            self.woDetails.HEADING(clientInfo.HEADING);
            self.woDetails.HEADING2(clientInfo.HEADING2);
            self.woDetails.HEADING3(clientInfo.HEADING3);
            self.woDetails.HEADING_OTH(clientInfo.HEADING_OTH);
            self.woDetails.HEADING_OTH2(clientInfo.HEADING_OTH2);
            self.woDetails.HEADING_OTH3(clientInfo.HEADING_OTH3);
            self.woDetails.CLIENTADDRESS(clientInfo.ADDRESS + ', ' + clientInfo.CITY + ', ' + clientInfo.ZIPCODE + ', ' + clientInfo.STATE);
            self.woDetails.CLIENTCONTACT('Tel.No: ' + clientInfo.CONTACT_NO1 + ' Fax No:' + clientInfo.CONTACT_NO2);
            $(ah.config.cls.liPrint).show();
            $("#PrintBulkBtn").hide();
            woIdList = "";
			var htmlstr = '';
			var pageNumto = pageNum + 500;
			var pagenum = (Math.ceil(sr.length / 500)) - 1;
           
            var index = 200000;           
            sr = sr.slice(pageNum, index);

			//sr = sr.filter(function (item) { return item.ROWNUMID >= pageNum && item.ROWNUMID <= pageNumto });
          
			if (pagenum >= 2) {

				$(ah.config.cls.pagingDiv).show();
			} else {
				$(ah.config.cls.pagingDiv).hide();
			}

			$("#PaginGtotNumber").text(pagenum.toString());
            var countsr= 0
            for (var o in sr) {
                countsr++;
				var assetDesc = "";
				if (sr[o].REQ_TYPE == 'SR'){
					assetDesc = "Stock Request"
				} else {
					assetDesc = sr[o].DESCRIPTION
				}

				if (sr[o].WO_STATUS === 'CL') {
					sr[o].WO_STATUS = 'Closed';
				} else if (sr[o].WO_STATUS === 'OP') {
					sr[o].WO_STATUS = 'Open';
				}
                else if (sr[o].WO_STATUS === 'HA') {
					sr[o].WO_STATUS = 'On Hold';
				} else if (sr[o].WO_STATUS === 'NE') {
					sr[o].WO_STATUS = 'New';
				} else if (sr[o].WO_STATUS === 'PS') {
					sr[o].WO_STATUS = 'Posted';
                } else if (sr[o].WO_STATUS === 'FC') {
                    sr[o].WO_STATUS ='For Close';
                }
				if (sr[o].REQ_BY === null) {
					sr[o].REQ_BY = '';
                }
                if (countsr === sr.length) {
                    if (sr[o].ASSET_NO !== null && sr[o].ASSET_NO !== "") {
                        assetIdList += "'" + sr[o].ASSET_NO + "'";
                    }
                    
                    woIdList += "'" + sr[o].REQ_NO + "'";
                } else {
                    if (sr[o].ASSET_NO !== null && sr[o].ASSET_NO !== "") {
                        assetIdList += "'" + sr[o].ASSET_NO + "',";
                    }
                    woIdList += "'" + sr[o].REQ_NO + "',";
                }
                
                htmlstr += '<li>';

                htmlstr += '<span style="width:100%;">';
                    htmlstr += '<span style="width:90%;" class="fc-slate-blue">';
                    if (sr[o].DESCRIPTION === null || sr[o].DESCRIPTION === "") {
                   
                        htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplateSR, sr[o].REQ_NO, sr[o].ASSET_NO, assetDesc, "");
                    } else {
                        htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].REQ_NO, sr[o].ASSET_NO, assetDesc, "");
                    }
                    htmlstr += '</span>';
                if (self.validatePrivileges('40') === 0) {
                    htmlstr += '<span style="width:10%; text-align:right;" class="no-print">';
                    htmlstr += ah.formatString(ah.config.html.searchRowTemplateQuickView, sr[o].REQ_NO, 'Quick View');
                    htmlstr += '</span>';
                }
                htmlstr += '</span>';
                
                htmlstr += '<span style="width:100%;">';
                    htmlstr += '<span style="width:90%;">';
                    htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.woOrder, sr[o].REQ_NO);
                    htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.supplier, sr[o].SUPPIERNAME);
                    htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.costCenter, sr[o].COSTCENTERNAME);
                    htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.problem, sr[o].PROBLEM);
                    htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.requestor, sr[o].REQ_BY);
                    htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.reqDate, ah.formatJSONDateTimeToString(sr[o].REQ_DATE));
                    htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.assignTo, sr[o].STAFF_FULLNAME);
                    var dueDate =  ah.formatJSONDateToString(sr[o].JOB_DUE_DATE);
                    if (dueDate != "01/01/1900") {
                        htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.dueDate, ah.formatJSONDateToString(sr[o].JOB_DUE_DATE));
                    }
                    if (sr[o].WO_STATUS === "Closed") {
                        htmlstr += ah.formatString(ah.config.html.searchRowTemplateC, systemText.searchModeLabels.statusName, sr[o].WO_STATUS);
                    } else {
                        htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.statusName, sr[o].WO_STATUS);
                    }
                    if (sr[o].AM_INSPECTION_ID !== null && sr[o].AM_INSPECTION_ID > 0) {
                        htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.workSheet, sr[o].AM_INSPECTION_ID.toString() + '-' + sr[o].INSPECTION_TYPE);
                    } 
                    htmlstr += '</span>';
                    htmlstr += '<span style="width:10%; text-align:right;" class="no-print">';
                    if (sr[o].MATERIALS_REMARKS == 'Ready for Collection') {
                        htmlstr += ah.formatString(ah.config.html.searchRowTemplateSpareParts, sr[o].REQ_NO, 'Ready for Collection');
                    }
                    if (sr[o].SPAREPARTS > 0) {
                        htmlstr += ah.formatString(ah.config.html.searchRowTemplateSpareParts, sr[o].REQ_NO, 'Spare Parts: ' + sr[o].SPAREPARTS);
                    }
                    htmlstr += '</span>';
                htmlstr += '</span>';
                htmlstr += '</li>';
                if (countsr === 500) {
                    break;
                }
            }
            resultHtml = htmlstr;
            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        } else {
            $(ah.config.cls.liPrint).hide();
            $(ah.config.id.PrintBulkBtn).hide();
        }
        //$(ah.config.cls.detailrequest).bind('click', self.readquickview);
        if (self.validatePrivileges('40') === 0) {
            $(ah.config.cls.detailItem).bind('click', self.readSetupID);
            $(ah.config.cls.detailrequest).bind('click', self.readquickview);
            $("#PrintBulkBtn").show();
        };
        $(ah.config.cls.detailParts).bind('click', self.getspareParts);
        ah.displaySearchResult();
        if (self.validatePrivileges('6') === 0) {
            $(ah.config.cls.liAddNew).hide();
        };
    };
    self.readquickview = function () {
        var reqID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        postData = {REQ_NO: reqID, STAFF_LOGON_SESSIONS: staffLogonSessions, SEARCH_FILTER: self.searchFilter, STATUS: "" };
        $.post(self.getApi() + ah.config.api.searchWOQuickView, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    
    }
    self.searchResultNotes = function (jsonData) {
        $(ah.config.id.noteEntry).hide();
        $(ah.config.id.noteHistory).show();
        var sr = jsonData.AM_ASSET_NOTES;
        $(ah.config.id.noteHistory + ' ' + ah.config.tag.ul).html('');
        if (sr.length > 0) {
            var htmlstr = "";
            for (var o in sr) {
                if (sr[o].FIRST_NAME === null) {
                    sr[o].FIRST_NAME = "";
                }
                if (sr[o].LAST_NAME === null) {
                    sr[o].LAST_NAME = "";
                }
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].ASSET_NO, sr[o].CREATED_DATE, sr[o].FIRST_NAME, sr[o].LAST_NAME);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.notes, sr[o].NOTES);

                htmlstr += '</li>';
            }

            $(ah.config.id.noteHistory + ' ' + ah.config.tag.ul).append(htmlstr);
        }
    }
    self.searchResultWONotes = function (jsonData) {
        $(ah.config.id.noteEntry).hide();
        $(ah.config.id.noteHistory).show();
        var sr = jsonData.AM_WORK_ORDER_REFNOTES;
        $(ah.config.id.noteHistory + ' ' + ah.config.tag.ul).html('');


        if (sr.length > 0) {
            var htmlstr = "";
            for (var o in sr) {
                if (sr[o].FIRST_NAME === null) {
                    sr[o].FIRST_NAME = "";
                }
                if (sr[o].LAST_NAME === null) {
                    sr[o].LAST_NAME = "";
                }
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].FIRST_NAME, sr[o].CREATED_DATE, sr[o].FIRST_NAME, sr[o].LAST_NAME);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.notes, sr[o].WO_NOTES);

                htmlstr += '</li>';

            }

            $(ah.config.id.noteHistory + ' ' + ah.config.tag.ul).append(htmlstr);
        }
    }
    self.searchResultWOStatus = function (jsonData) {

        var sr = jsonData.AM_WORK_ORDERS_STATUS_LOG;

        $(ah.config.id.statusLogList + ' ' + ah.config.tag.ul).html('');


        if (sr.length > 0) {
            var htmlstr = "";
            for (var o in sr) {
                if (sr[o].FIRST_NAME === null) {
                    sr[o].FIRST_NAME = "";
                }
                if (sr[o].LAST_NAME === null) {
                    sr[o].LAST_NAME = "";
                }
                if (sr[o].WO_STATUS === 'NE') {
                    sr[o].WO_STATUS = 'New';
                } else if (sr[o].WO_STATUS === 'CL') {
                    sr[o].WO_STATUS = 'Closed';
                }
                else if (sr[o].WO_STATUS === 'OP') {
                    sr[o].WO_STATUS = 'Open';
                }
                else if (sr[o].WO_STATUS === 'FC') {
                    sr[o].WO_STATUS = 'For Close';
                }
                else if (sr[o].WO_STATUS === 'PS') {
                    sr[o].WO_STATUS = 'Posted';
                }
                else if (sr[o].WO_STATUS === 'HA') {
                    sr[o].WO_STATUS = 'On-Hold';
                }

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].RECORDED_DATE, sr[o].WO_STATUS, sr[o].FIRST_NAME, sr[o].LAST_NAME + '@' + sr[o].RECORDED_DATE);

                htmlstr += '</li>';

            }

            $(ah.config.id.statusLogList + ' ' + ah.config.tag.ul).append(htmlstr);
        }
    };

    self.printSummary = function (jsonData)  {
        self.clientDetails.CLIENTNAME(clientInfo.DESCRIPTION);
        self.clientDetails.CLIENT_LOGO(clientInfo.CLIENT_LOGO);
        self.clientDetails.COMPANY_LOGO(clientInfo.COMPANY_LOGO);
        self.clientDetails.HEADING(clientInfo.HEADING);
        self.clientDetails.HEADING2(clientInfo.HEADING2);
        self.clientDetails.HEADING3(clientInfo.HEADING3);
        self.clientDetails.HEADING_OTH(clientInfo.HEADING_OTH);
        self.clientDetails.HEADING_OTH2(clientInfo.HEADING_OTH2);
        self.clientDetails.HEADING_OTH3(clientInfo.HEADING_OTH3);
        self.clientDetails.CLIENTADDRESS(clientInfo.ADDRESS + ', ' + clientInfo.CITY + ', ' + clientInfo.ZIPCODE + ', ' + clientInfo.STATE);
        self.clientDetails.CLIENTCONTACT('Tel.No: ' + clientInfo.CONTACT_NO1 + ' Fax No:' + clientInfo.CONTACT_NO2);

        window.location.href = ah.config.url.openModalPrintPage;
        
       //document.getElementById("printDetails").classList.add('printDetails');
        document.getElementById("printSummary").classList.remove('printSummary'); 
       // document.getElementById("printBulk").classList.remove('printSummary');
        document.getElementById("printDetails").style.display = "";
        document.getElementById("printSticker").style.display = "none";
        document.getElementById("OutOfOrderSticker").style.display = "none";
        document.getElementById("InitialAccpSticker").style.display = "none";

        $(ah.config.id.assetPrintSearchResultList).html('');    
        $(ah.config.id.assetPrintSearchResultList).append($(ah.config.id.searchResultList).html());
        setTimeout(function () {
            window.location.href = ah.config.url.modalClose;
            window.print();
           
        }, 4000);
        //setTimeout(function () { window.print(); }, 3000);
        //window.print();
       // window.location.href = ah.config.url.modalClose;
    };

    self.printStickerDetails = {
        CLIENT_LOGO: ko.observable(),
        CLIENTNAME :ko.observable(),
        woOrder: ko.observable(),
        ReqDate: ko.observable(),
        ASSET_NO: ko.observable(),
        ASSET_NOBarcode: ko.observable(),
        DESCRIPTION: ko.observable(),
        NEXT_PM_DUE: ko.observable(),
        PM_TYPE: ko.observable(),
        COST_CENTER: ko.observable(),
        LOCATION: ko.observable(),
        serialNo: ko.observable(),
    };

    self.printSticker = function (jsonData) {
        var AssetDetails = assetInfoData;
        var clientInfo = jsonData.AM_CLIENT_INFO;
        //var RD = document.getElementById("REQ_DATE").value;
        var jDate = new Date();
        var NewDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();
        var woOrder = workOrderInfo.REQ_NO;

        //alert(JSON.stringify(workOrderInfo));
        self.printStickerDetails.CLIENT_LOGO(clientInfo.CLIENT_LOGO);
        self.printStickerDetails.PM_TYPE(AssetDetails.PM_TYPE);
        self.printStickerDetails.ASSET_NO(AssetDetails.ASSET_NO);
        self.printStickerDetails.ASSET_NOBarcode("*" + AssetDetails.ASSET_NO + "*");
        self.printStickerDetails.DESCRIPTION(AssetDetails.DESCRIPTION);
        self.printStickerDetails.NEXT_PM_DUE(AssetDetails.NextPmDue);
        self.printStickerDetails.LOCATION(AssetDetails.LOCATION);
        self.printStickerDetails.COST_CENTER(AssetDetails.COSTCENTER_DESC);
        self.printStickerDetails.woOrder(woOrder);
        self.printStickerDetails.ReqDate(NewDate);

        document.getElementById("printSticker").classList.add('printSummary');       
        document.getElementById("printDetails").classList.remove('printSummary');
        document.getElementById("printBulk").classList.remove('printSummary');
        document.getElementById("OutOfOrderSticker").classList.remove('printSummary');       
        document.getElementById("InitialAccpSticker").classList.remove('printSummary');

        $(ah.config.id.WoPrintSticker).html();

        setTimeout(function () {
            window.location.href = ah.config.url.modalClose;
            window.onafterprint = function () { document.getElementById("printDetails").style.display = ""; }
            window.print();
        }, 1000);
    };

    self.OutOfOrderSticker = function (jsonData) {
        var AssetDetails = assetInfoData;
        var clientInfo = jsonData.AM_CLIENT_INFO;
        //var RD = document.getElementById("REQ_DATE").value;
        //var SerialNo = document.getElementById("SERIAL_NO").Value;
        var AssetDesc = document.getElementById("DESCRIPTION").value;
        var woOrder = workOrderInfo.REQ_NO;
        //alert(JSON.stringify(AssetDetails));
        self.printStickerDetails.CLIENT_LOGO(clientInfo.CLIENT_LOGO);
        self.printStickerDetails.CLIENTNAME(clientInfo.DESCRIPTION);
        self.printStickerDetails.ASSET_NO(AssetDetails.ASSET_NO);
        self.printStickerDetails.ASSET_NOBarcode("*" + AssetDetails.ASSET_NO + "*");
        self.printStickerDetails.DESCRIPTION(AssetDesc);
        self.printStickerDetails.woOrder(woOrder);
        self.printStickerDetails.serialNo(AssetDetails.SERIAL_NO);

        document.getElementById("OutOfOrderSticker").classList.add('printSummary');
        document.getElementById("printDetails").classList.remove('printSummary');
        document.getElementById("printBulk").classList.remove('printSummary');
        document.getElementById("printSticker").classList.remove('printSummary');       
        document.getElementById("InitialAccpSticker").classList.remove('printSummary');

        $(ah.config.id.WoOutOrderSticker).html();

        setTimeout(function () {
            window.location.href = ah.config.url.modalClose;
            window.onafterprint = function () { document.getElementById("printDetails").style.display = ""; }
            window.print();
        }, 1000);
    };

    self.InitialAccpSticker = function (jsonData) {
        var AssetDetails = assetInfoData;
        var clientInfo = jsonData.AM_CLIENT_INFO;
        //var RD = document.getElementById("REQ_DATE").value;
        //var SerialNo = document.getElementById("SERIAL_NO").Value;
        var jDate = new Date();
        var NewDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();

        //var Assign = document.getElementById("ASSIGN_by").value;
        //var Assign = workOrderInfo.Assign_To;
        //alert(JSON.stringify(workOrderInfo));
        self.printStickerDetails.CLIENT_LOGO(clientInfo.CLIENT_LOGO);
        self.printStickerDetails.CLIENTNAME(clientInfo.DESCRIPTION);
        self.printStickerDetails.ASSET_NO(AssetDetails.ASSET_NO);
        self.printStickerDetails.ASSET_NOBarcode("*" + AssetDetails.ASSET_NO + "*");
        self.printStickerDetails.DESCRIPTION(workOrderInfo.ASSIGN_TO);
        self.printStickerDetails.woOrder(NewDate);
        //self.printStickerDetails.serialNo(AssetDetails.SERIAL_NO);
        self.printStickerDetails.NEXT_PM_DUE(AssetDetails.NextPmDue);

        document.getElementById("InitialAccpSticker").classList.add('printSummary');
        document.getElementById("printDetails").classList.remove('printSummary');
        document.getElementById("printBulk").classList.remove('printSummary');
        document.getElementById("printSticker").classList.remove('printSummary');
        document.getElementById("OutOfOrderSticker").classList.remove('printSummary');

        $(ah.config.id.WoInitialSticker).html();

        setTimeout(function () {
            window.location.href = ah.config.url.modalClose;
            window.onafterprint = function () { document.getElementById("printDetails").style.display = ""; }
            window.print();
        }, 1000);
    };

    self.searchContractResult = function (jsonData) {


        var sr = jsonData.AM_ASSET_CONTRACTSERVICES;
       
        $(ah.config.id.searchResult2 + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].CONTRACT_ID, "Contract ID&nbsp; " + sr[o].CONTRACT_ID, "Coverage&nbsp; " + sr[o].TYPE_COVERAGE, "");
                //htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.typeCoverage, sr[o].TYPE_COVERAGE);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.provider, sr[o].PROVIDER + "&nbsp;&nbsp; ");
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.labour, sr[o].COVERAGE_LABOR + "&nbsp;&nbsp; ");
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.parts, sr[o].COVERAGE_PARTSMATERIALS);
                htmlstr += '</li>';
            }

            $(ah.config.id.searchResult2 + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        //$(ah.config.cls.detailItem).bind('click', self.readSetupID);
        ////ah.toggleDisplayMode();
        ////ah.toggleAddItems();

    };
    self.searchItemsResult = function (LovCategory) {
        var sr = assetList;
        
        if (LovCategory !== null && LovCategory !== "") {
            var arr = sr;
            var keywords = LovCategory.toUpperCase();
            var filtered = [];

            for (var i = 0; i < sr.length; i++) {
                for (var j = 0; j < keywords.length; j++) {
                    if ((sr[i].DESCRIPTION.toUpperCase()).indexOf(keywords) > -1 || (sr[i].ASSET_NO.toUpperCase()).indexOf(keywords) > -1) {
                        filtered.push(sr[i]);
                        break;
                    }
                }
            }
            sr = filtered;
        }
        
        $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).html('');
        $("#noresult").hide();
        if (sr.length > 0) {
          
            var htmlstr = '';
            
			
            for (var o in sr) {
				var assetDescription = sr[o].DESCRIPTION;
				if (assetDescription === null || assetDescription === "" || assetDescription === "null") {
					
					sr[o].DESCRIPTION = "Undefined"
					assetDescription = "Undefined"
				}
                if (assetDescription.length > 70) {
                    assetDescription = assetDescription.substring(0, 70) + '...';
                }
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplateListLOV, JSON.stringify(sr[o]), sr[o].ASSET_NO, assetDescription, "");
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateLOV, systemText.searchModeLabels.costCenter, sr[o].COSTCENTER_DESC + '&nbsp;&nbsp;');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateLOV, systemText.searchModeLabels.supplier, sr[o].SUPPLIERNAME + '&nbsp;&nbsp;');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateLOV, systemText.searchModeLabels.serialNo, sr[o].SERIAL_NO);
                htmlstr += '</li>';
                
            }
            $("#searchingAssetLOV").hide();
            $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).append(htmlstr);
        }
        else {
            $("#searchingAssetLOV").hide();
            $("#noresult").show();
        }
        $(ah.config.cls.detailItem).bind('click', self.populateAssetInfoById);
        //ah.toggleDisplayModeAsset();
        //ah.toggleAddItems();
    };
    self.searchPartsOnhandResult = function (jsonData) {
        var wopmPartsr = jsonData.AM_PARTSREQUEST;
       
        if (wopmPartsr.length > 0) {
            $("#partWORENO").text(wopmPartsr[0].REF_WO);
            self.partsWOList.splice(0, 5000);  
            for (var o in wopmPartsr) {
                var billable = 'Yes';
                if (wopmPartsr[o].BILLABLE === "false") {
                    billable = 'No';
                }

                if (wopmPartsr[o].PRODUCT_DESC != null && wopmPartsr[o].PRODUCT_DESC.length > 60)
                    wopmPartsr[o].PRODUCT_DESC = wopmPartsr[o].PRODUCT_DESC.substring(0, 59) + "..."
                if (wopmPartsr[o].PRODUCT_DESC === null || wopmPartsr[o].PRODUCT_DESC === "") {
                    wopmPartsr[o].PRODUCT_DESC = wopmPartsr[o].DESCRIPTION;
                    wopmPartsr[o].ID_PARTS = 'Supplier';
                }
              
                self.partsWOList.push({
                        PRODUCT_DESC: wopmPartsr[o].PRODUCT_DESC,
                        DESCRIPTION: wopmPartsr[o].DESCRIPTION,
                        ID_PARTS: wopmPartsr[o].ID_PARTS,
                        STORE_DESC: wopmPartsr[o].STORE_DESC,
                        STORE_ID: wopmPartsr[o].STORE_ID,
                        BILLABLE: billable,
                        PRICE: wopmPartsr[o].PRICE,
                        STATUSREQ: wopmPartsr[o].STATUS,
                        TRANSFER_QTY: wopmPartsr[o].TRANSFER_QTY,
                        TRANSFER_FLAG: wopmPartsr[o].TRANSFER_FLAG,
                        EXTENDED: wopmPartsr[o].EXTENDED,
                        //REQUEST_ID: wopmPartsr[o].REQUEST_ID,
                        REF_WO: wopmPartsr[o].REF_WO,
                        WO_LABOURTYPE: wopmPartsr[o].WO_LABOURTYPE,
                        STATUS: wopmPartsr[o].STATUS,
                        QTY_RETURN: wopmPartsr[o].QTY_RETURN,
                        QTY_FORRETURN: wopmPartsr[o].QTY_FORRETURN,
                        REF_ASSETNO: wopmPartsr[o].REF_ASSETNO,
                        AM_WORKORDER_LABOUR_ID: wopmPartsr[o].AM_WORKORDER_LABOUR_ID,
                        PART_NUMBERS: wopmPartsr[o].PART_NUMBERS,
                        PART_MODELS: wopmPartsr[o].PART_MODELS,
                        QTY: wopmPartsr[o].QTY,
                        USED_QTY: wopmPartsr[o].USED_QTY
                    });
               

            }
           
        }
        window.location.href = ah.config.url.openModalInHouse;
    };
    self.searchWoMaterialResult = function (jsonData) {
       
        var wopmPartsr = jsonData.AM_PARTSREQUEST;
        
       // var clientInformation = jsonData.AM_CLIENT_INFO;
     
        var htmlstr = '';
        htmlstr += "<tr>";
        self.woDetails.WOACTIONLAST("");
        self.woDetails.CLIENTNAME(clientInfo.DESCRIPTION);
        self.woDetails.CLIENT_LOGO(clientInfo.CLIENT_LOGO);
        self.woDetails.COMPANY_LOGO(clientInfo.COMPANY_LOGO);
        self.woDetails.HEADING(clientInfo.HEADING);
        self.woDetails.HEADING2(clientInfo.HEADING2);
        self.woDetails.HEADING3(clientInfo.HEADING3);
        self.woDetails.HEADING_OTH(clientInfo.HEADING_OTH);
        self.woDetails.HEADING_OTH2(clientInfo.HEADING_OTH2);
        self.woDetails.HEADING_OTH3(clientInfo.HEADING_OTH3);
        self.woDetails.CLIENTADDRESS(clientInfo.ADDRESS + ', ' + clientInfo.CITY + ', ' + clientInfo.ZIPCODE + ', ' + clientInfo.STATE);
        self.woDetails.CLIENTCONTACT('Tel.No: ' + clientInfo.CONTACT_NO1 + ' Fax No:' + clientInfo.CONTACT_NO2);
       
            var wolInformation = jsonData.AM_WORKORDER_LABOUR;
            $(ah.config.id.wolabourinfo + ' ' + ah.config.tag.tbody).html('');
            //alert(wolInformation.length);
            if (wolInformation.length > 0) {
                $("#woLabourInfoListDetails").show();
                //alert(wolInformation[0].LABOUR_ACTION);
                self.woDetails.WOACTIONLAST(wolInformation[0].LABOUR_ACTION);
                var htmlstrTxt = "";
                for (var x in wolInformation) {
                    

                    var employee = null;
                    if (wolInformation[x].EMPLOYEE === null || wolInformation[x].EMPLOYEE === "") {
                        //alert(wolInformation[0].SUPPLIER);
                        employee = wolInformation[x].SUPPLIER;
                    } else {
                        employee = wolInformation[x].EMPLOYEE;
                    }
                    if (wolInformation[x].BILLABLE === 'Y') {
                        wolInformation[x].BILLABLE = 'Yes';
                    } else {
                        wolInformation[x].BILLABLE = 'No';
                    }



                    htmlstrTxt += "<tr>";
                    htmlstrTxt += '<td >' + employee + '</td>';
                    htmlstrTxt += '<td >' + wolInformation[x].HOURS + '</td>';

                    htmlstrTxt += '<td >' + wolInformation[x].BILLABLE + '</td>';
                    htmlstrTxt += '<td >' + ah.formatJSONDateTimeToString(wolInformation[x].LABOUR_DATE)+ '</td>';
                   
                    htmlstrTxt += "</tr>";
                    htmlstrTxt += '<tr > <td style="width:100%;" align="left" colspan="4">Action Details</td> </tr>';
                    htmlstrTxt += '<tr > <td style="width:100%;" align="left" colspan="4">' + wolInformation[x].LABOUR_ACTION +'</td> </tr>';
                   
                    htmlstrTxt += '</tr >';
                    
                }
                $(ah.config.id.wolabourinfo + ' ' + ah.config.tag.tbody).append(htmlstrTxt);

            } else {
                $("#woLabourInfoListDetails").hide();
            }
            $(ah.config.id.woMaterialListData + ' ' + ah.config.tag.tbody).html('');

            if (wopmPartsr.length > 0) {
                var htmlstrTxtM = "";
                $("#woMaterialInfo").show();

                //self.partsWLList.splice(0, 5000);
                ////$("#REQUEST_ID").val(wopmPartsr[0].REQUEST_ID);
              
                for (var o in wopmPartsr) {
                    var dispatchDate = "";
                    if (wopmPartsr[o].TRANSFER_QTY > 0) {
                        if (wopmPartsr[o].DISPATCH_DATE != null) {
                            dispatchDate = ah.formatJSONDateTimeToString(wopmPartsr[o].DISPATCH_DATE);
                        } else {
                            dispatchDate = $("#REQ_DATE").val();
                        }
                    }
                    

                    
                    var qtyactu = 0;
                    var confirmFlag = $("#PARTS_CONFIRMED").val();
                    if (confirmFlag == "Y") {
                        if (wopmPartsr[o].STATUS != "Received") {
                            qtyactu = wopmPartsr[o].QTY;
                        } else {
                            qtyactu = wopmPartsr[o].USED_QTY;
                        }

                    } else {
                        if (wopmPartsr[o].USED_QTY == 0 && wopmPartsr[o].STATUS == "Received") {
                            qtyactu = wopmPartsr[o].QTY;
                        } else {

                            if (wopmPartsr[o].USED_QTY > 0) {

                                qtyactu = wopmPartsr[o].USED_QTY;
                            } else {

                                qtyactu = wopmPartsr[o].QTY;
                            }

                        }

                    }


                    // if (wopmPartsr[o].DESCRIPTION !== null && wopmPartsr[o].DESCRIPTION !== "") {
                    if (wopmPartsr[o].PRODUCT_DESC === null || wopmPartsr[o].PRODUCT_DESC === "") {
                        wopmPartsr[o].PRODUCT_DESC = wopmPartsr[o].DESCRIPTION;
                        wopmPartsr[o].ID_PARTS = 'Supplier'
                    }
                    htmlstrTxtM += "<tr>";
                    htmlstrTxtM += '<td >' + wopmPartsr[o].ID_PARTS + '</td>';
                    htmlstrTxtM += '<td >' + wopmPartsr[o].PART_NUMBERS + '</td>';
                    htmlstrTxtM += '<td >' + wopmPartsr[o].PRODUCT_DESC + '</td>';
                    htmlstrTxtM += '<td >' + dispatchDate + '</td>';
                    htmlstrTxtM += '<td >' + qtyactu + '</td>';
                    htmlstrTxtM += '<td ></td>';

                    htmlstrTxtM += "</tr>";


                    //self.partsWLList.push({
                    //    PRODUCT_DESC: wopmPartsr[o].PRODUCT_DESC,
                    //    DESCRIPTION: wopmPartsr[o].DESCRIPTION,
                    //    ID_PARTS: wopmPartsr[o].ID_PARTS,
                    //    QTY: wopmPartsr[o].QTY

                    //});
                    // }


                }
                    //}
                $(ah.config.id.woMaterialListData + ' ' + ah.config.tag.tbody).append(htmlstrTxtM);
                }
            else {
                $("#woMaterialInfo").hide();

            }
            
            self.loadDataPrint("");
         htmlstr += "</tr>";
        
         var bulkPrint = $("#BULKPRINT").val();
         if (bulkPrint !== "BULKPRINT") {
             self.printWO();//setTimeout(function () { window.print(); }, 1000);
         } else {
           
           
         }
            //
        
    };
    self.searchPartsMaterialOnhandResult = function (LovCategory) {

        var sr = srOnhandDataArray;
        var woStatus = $("#WO_STATUS").val();
        var confirmParts = $("#PARTS_CONFIRMED").val();
        
        var wono = $("#REQ_ID").val();    
        $("#partWONO").text(wono);
        var assetdet = $("#ASSET_NO").val() + ' ' + $("#DESCRIPTION").val();
        $("#partAssetInfo").text(assetdet);
        $(ah.config.id.searchResultPartsOnhand + ' ' + ah.config.tag.ul).html('');
        $("#NOREC").hide();
        $("#CANCELCOLLECT").hide();
        
           
        if (LovCategory === "LOAD") {
            var wopmPartsr = srDataArray;


            $("#confirmBtn").hide();
            $("#confirmMSG").hide();
            $("#receivepartBtn").hide();
            self.partsAddWOList.splice(0, 5000);
            $("#confirmStatus").text("");
            if (wopmPartsr.length > 0) {
                if (confirmParts == "Y") {
                    $("#confirmStatusNA").hide();
                    $("#confirmStatus").text("Work Order ready for close");
                    $("#confirmStatus").show();

                } else {
                    $("#confirmStatus").hide();
                    $("#confirmStatusNA").text("Work Order is not ready for close");
                    $("#confirmStatusNA").show();
                }
                
                $("#confirmMSG").show();
                //alert(JSON.stringify(wopmPartsr));
                
                //$("#REQUEST_ID").val(wopmPartsr[0].REQUEST_ID);
                var isReceived = 0;
                for (var o in wopmPartsr) {
                    var billable = true;
                    if (wopmPartsr[o].BILLABLE === "false") {
                        billable = false;
                    }

                    if (wopmPartsr[o].STATUS == "Dispatch") {
                        $("#receivepartBtn").show();
                        $("#confirmBtn").hide();
                    }
                    if (wopmPartsr[o].STATUS == "Received") {
                     
                        $("#confirmBtn").show();
                    }
                  
                    if (self.validatePrivileges('35') > 0) {
                        $("#receivepartBtn").show();
                        $("#confirmBtn").show();
                    }


                    if (wopmPartsr[o].PRODUCT_DESC != null && wopmPartsr[o].PRODUCT_DESC.length > 60)
                        wopmPartsr[o].PRODUCT_DESC = wopmPartsr[o].PRODUCT_DESC.substring(0, 59) + "..."
                    if (wopmPartsr[o].PR_REQUEST_ID === "" || wopmPartsr[o].PR_REQUEST_ID === null) { wopmPartsr[o].PR_REQUEST_ID = 0 }
                    var reqTOT = 0;

                    var tet =  wopmPartsr[o].QTY_RETURN + wopmPartsr[o].USED_QTY;

                    if (wopmPartsr[o].TRANSFER_QTY <= tet) {
                    
                        reqTOT = wopmPartsr[o].QTY_RETURN + wopmPartsr[o].USED_QTY;
                    }
                    


                    if (wopmPartsr[o].USED_QTY == "" || wopmPartsr[o].USED_QTY == null) {
                        wopmPartsr[o].USED_QTY = 0;
                    }
                    if (wopmPartsr[o].USED_QTY == 0 && woStatus != 'CL' && confirmParts != 'Y') {
                       
                        wopmPartsr[o].USED_QTY = null;
                        
                    }
                   
                    self.partsAddWOList.push({
                            PRODUCT_DESC: wopmPartsr[o].PRODUCT_DESC,
                            DESCRIPTION: wopmPartsr[o].DESCRIPTION,
                            ID_PARTS: wopmPartsr[o].ID_PARTS,
                            STORE_DESC: wopmPartsr[o].STORE_DESC,
                            STORE_ID: wopmPartsr[o].STORE_ID,
                            BILLABLE: billable,
                            PRICE: wopmPartsr[o].PRICE,
                            STATUSREQ: wopmPartsr[o].STATUS,
                            TRANSFER_QTY: wopmPartsr[o].TRANSFER_QTY,
                            TRANSFER_FLAG: wopmPartsr[o].TRANSFER_FLAG,
                            EXTENDED: wopmPartsr[o].EXTENDED,
                            REF_WO: wopmPartsr[o].REF_WO,
                            WO_LABOURTYPE: wopmPartsr[o].WO_LABOURTYPE,
                            STATUS: wopmPartsr[o].STATUS,
                            QTY_RETURN: wopmPartsr[o].QTY_RETURN,
                            QTY_FORRETURN: wopmPartsr[o].QTY_FORRETURN,
                            FOR_COLLECTION_QTY: wopmPartsr[o].FOR_COLLECTION_QTY,
                            REF_ASSETNO: wopmPartsr[o].REF_ASSETNO,
                            AM_WORKORDER_LABOUR_ID: wopmPartsr[o].AM_WORKORDER_LABOUR_ID,
                            PR_REQUEST_ID: wopmPartsr[o].PR_REQUEST_ID,
                            PR_REQUEST_QTY: wopmPartsr[o].PR_REQUEST_QTY,
                            QTY: wopmPartsr[o].QTY,
                            USED_QTY: wopmPartsr[o].USED_QTY,
                            PART_NUMBERS: wopmPartsr[o].PART_NUMBERS,
                            PART_MODELS: wopmPartsr[o].PART_MODELS,
                            WOSTATUS: woStatus,
                            TOTUSED: reqTOT,
                            FOR_COLLECTION_CANCELBY: wopmPartsr[o].FOR_COLLECTION_CANCELBY,
                            FOR_COLLECTION_CANCELDATE: wopmPartsr[o].FOR_COLLECTION_CANCELDATE,
                            LAST_MODIFIEDBY: wopmPartsr[o].LAST_MODIFIEDBY,
                            LAST_MODIFIEDDATE: wopmPartsr[o].LAST_MODIFIEDDATE
                        });
            

                }
            }
        }
        if (woStatus === 'CL') {
            $("#addButtonModal").hide();
            $("#sparePartsList").hide();
            $("#confirmBtn").hide();
            $("#receivepartBtn").hide();
           
            
        }
        window.location.href = encodeURI(ah.config.url.modalAddItems);
       
    };
    self.searchResultStaffListLov = function (LovCategory) {
        var sr = staffListLOV;
        if (LovCategory !== null && LovCategory !== "") {
            var arr = sr;
            var keywords = LovCategory.toUpperCase();
            var filtered = [];

            for (var i = 0; i < sr.length; i++) {
                for (var j = 0; j < keywords.length; j++) {
                    if ((sr[i].FIRST_NAME.toUpperCase()).indexOf(keywords) > -1 || (sr[i].LAST_NAME.toUpperCase()).indexOf(keywords) > -1) {
                        filtered.push(sr[i]);
                        break;
                    }
                }
            }
            sr = filtered;
        }
        //alert(JSON.stringify(sr));
        $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');

        if (sr.length > 0) {
            var htmlstr = "";
            for (var o in sr) {
              
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, JSON.stringify(sr[o]), "", sr[o].FIRST_NAME, sr[o].LAST_NAME);
               
                htmlstr += '</li>';

            }
           // alert(htmlstr);
            $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).append(htmlstr);
            $(ah.config.cls.detailItem).bind('click', self.populateAssignTo);
        }
       
    };
    self.populateAssignTo = function () {

        var reqID = JSON.parse(this.getAttribute(ah.config.attr.datainfoId).toString());
        $("#ASSIGN_TONAME").val(reqID.FIRST_NAME + ' ' + reqID.LAST_NAME);
        $("#ASSIGN_TO").val(reqID.STAFF_ID);


    }
    self.searchResultSupplierListLov = function (LovCategory) {
        var sr = supplierListLOV;
       
        if (LovCategory !== null && LovCategory !== "") {
            var arr = sr;
            var keywords = LovCategory.toUpperCase();
            var filtered = [];

            for (var i = 0; i < sr.length; i++) {
                for (var j = 0; j < keywords.length; j++) {
                    if ((sr[i].DESCRIPTION.toUpperCase()).indexOf(keywords) > -1 || (sr[i].SUPPLIER_ID.toUpperCase()).indexOf(keywords) > -1) {
                        filtered.push(sr[i]);
                        break;
                    }
                }
            }
            sr = filtered;
        }

        //alert(JSON.stringify(sr));
        $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');

        if (sr.length > 0) {
            var htmlstr = "";
            for (var o in sr) {

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, JSON.stringify(sr[o]), "", sr[o].DESCRIPTION,"");

                htmlstr += '</li>';

            }
            // alert(htmlstr);
            $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).append(htmlstr);
            $(ah.config.cls.detailItem).bind('click', self.populateAssignOutsource);
        }

    };
    self.searchResultCostCenterListLov = function (LovCategory) {
        var sr = costCenterListLOV;

        if (LovCategory !== null && LovCategory !== "") {
            var arr = sr;
            var keywords = LovCategory.toUpperCase();
            var filtered = [];

            for (var i = 0; i < sr.length; i++) {
                for (var j = 0; j < keywords.length; j++) {
                    if ((sr[i].DESCRIPTION.toUpperCase()).indexOf(keywords) > -1 || (sr[i].LOV_LOOKUP_ID.toUpperCase()).indexOf(keywords) > -1) {
                        filtered.push(sr[i]);
                        break;
                    }
                }
            }
            sr = filtered;
        }

        //alert(JSON.stringify(sr));
        $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');

        if (sr.length > 0) {
            var htmlstr = "";
            for (var o in sr) {

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, JSON.stringify(sr[o]), "", sr[o].DESCRIPTION, "");

                htmlstr += '</li>';

            }
            // alert(htmlstr);
            $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).append(htmlstr);
            $(ah.config.cls.detailItem).bind('click', self.populateCostCenter);
        }

    };
    self.populateCostCenter= function () {

        var reqID = JSON.parse(this.getAttribute(ah.config.attr.datainfoId).toString());
      
        $("#COST_CENTER").val(reqID.DESCRIPTION);
        $("#ASSET_COSTCENTERCODE").val(reqID.LOV_LOOKUP_ID);


    }
    self.populateAssignOutsource = function () {

        var reqID = JSON.parse(this.getAttribute(ah.config.attr.datainfoId).toString());
        $("#ASSIGN_OUTSOURCEDESC").val(reqID.DESCRIPTION);
        $("#ASSIGN_OUTSOURCE").val(reqID.SUPPLIER_ID);


    }
    self.closeModalalert = function () {
        var modal = document.getElementById('openModalAlert');
        modal.style.display = "none"
    }

    self.showQuickView = function (jsonData) {

        var jsetupDetails = jsonData.WORK_ORDER_QUICKVIEW_V;
        $("#WOCLOSEDDATE").hide();
        
        if (jsetupDetails.QVWO_STATUS == 'NE') {
            jsetupDetails.QVWO_STATUS = 'New'
        } else if (jsetupDetails.QVWO_STATUS == 'OP') {
            jsetupDetails.QVWO_STATUS = 'Open'
        } else if (jsetupDetails.QVWO_STATUS == 'HA') {
            jsetupDetails.QVWO_STATUS = 'On Hold'
        } else if (jsetupDetails.QVWO_STATUS == 'CL') {
            jsetupDetails.QVWO_STATUS = 'Close'
        } else if (jsetupDetails.QVWO_STATUS == 'FC') {
            jsetupDetails.QVWO_STATUS = 'For Close'
        }
        if (jsetupDetails.QVCLOSED_DATE == 'CL') {
            $("#WOCLOSEDDATE").show();
        }

        if (jsetupDetails.QVJOB_DUE_DATE == "01/01/1900 00:00" || jsetupDetails.QVJOB_DUE_DATE == "null" || jsetupDetails.QVJOB_DUE_DATE == "0001-01-01T00:00:00" || jsetupDetails.QVJOB_DUE_DATE == "1900-01-01T00:00:00") {
            //var jobDueDate = ah.formatJSONDateTimeToString("1900-01-01T00:00:00");
            jsetupDetails.QVJOB_DUE_DATE = "";
        }
        ah.LoadJSON(jsetupDetails);

        var jsetupAssetDetails = jsonData.ASSET_QUICKVIEW_V;
        if (jsetupAssetDetails.AQV_STATUS == 'I') {
            jsetupAssetDetails.AQV_STATUS = 'In Service'
        } else if (jsetupAssetDetails.AQV_STATUS == 'A') {
            jsetupAssetDetails.AQV_STATUS = 'Active'
        } else if (jsetupAssetDetails.AQV_STATUS == 'M') {
            jsetupAssetDetails.AQV_STATUS = 'Missing'
        } else if (jsetupAssetDetails.AQV_STATUS == 'N') {
            jsetupAssetDetails.AQV_STATUS = 'Inactive'
        } else if (jsetupAssetDetails.AQV_STATUS == 'O') {
            jsetupAssetDetails.AQV_STATUS = 'Out of Service'
        } else if (jsetupAssetDetails.AQV_STATUS == 'R') {
            jsetupAssetDetails.AQV_STATUS = 'Retired'
        } else if (jsetupAssetDetails.AQV_STATUS == 'T') {
            jsetupAssetDetails.AQV_STATUS = 'Transferred'
        } else if (jsetupAssetDetails.AQV_STATUS == 'U') {
            jsetupAssetDetails.AQV_STATUS = 'Undefined'
        }
        $("#PMINFO").show();
        if (jsetupAssetDetails.AQV_PMCOUNT == 0) {
            $("#PMINFO").hide();

        }
       
        if (jsetupAssetDetails.AQV_INSTALLATION_DATE == "01/01/1900" || jsetupAssetDetails.AQV_INSTALLATION_DATE == "null" || jsetupAssetDetails.AQV_INSTALLATION_DATE == "0001-01-01T00:00:00" || jsetupAssetDetails.AQV_INSTALLATION_DATE == "1900-01-01T00:00:00") {
           
            jsetupAssetDetails.AQV_INSTALLATION_DATE = "";
        }


        ah.LoadJSON(jsetupAssetDetails);
        srparts = jsonData.AM_PARTSREQUEST;
       
        $("#norecParts").hide();
        $("#norecLabour").hide();
        $("#QVWONOX").text(jsetupDetails.QVWONO);
        $(ah.config.id.woLabourInfoQV).show();
        $(ah.config.id.searchResultListQVParts).show();
        $(ah.config.id.assetWarrantyQV).show();

        var assetWarrantyList = jsonData.AM_ASSET_WARRANTY;

        $(ah.config.id.assetWarrantyQV + ' ' + ah.config.tag.tbody).html('');
        var htmlstrWR = ""
        if (assetWarrantyList.length > 0) {
         
          
            for (var o in assetWarrantyList) {
                var warTerm = assetWarrantyList[o].FREQUENCY_PERIOD+ ' ' + assetWarrantyList[o].FREQUENCY;
                if (assetWarrantyList[o].WARRANTYDESC == null) {
                    assetWarrantyList[o].WARRANTYDESC = "";
                }

                htmlstrWR += "<tr>";
                htmlstrWR += '<td class="">' + ah.formatJSONDateToString(assetWarrantyList[o].WARRANTY_EXPIRY) + '</td>';
                htmlstrWR += '<td class="">' + assetWarrantyList[o].WARRANTYDESC + '</td>';
                htmlstrWR += '<td class="">' + warTerm + '</td>';
                htmlstrWR += '<td class="">' + assetWarrantyList[o].WARRANTY_NOTES + '</td>';
               
                htmlstrWR += "</tr>";
            }

            $(ah.config.id.assetWarrantyQV + ' ' + ah.config.tag.tbody).append(htmlstrWR);
        } else {
            $(ah.config.id.assetWarrantyQV).hide();
            
        }

        var woLabourList = jsonData.WORK_ORDER_LABOUR;
        $(ah.config.id.woLabourInfoQV + ' ' + ah.config.tag.tbody).html('');
        var htmlstrLB = ""
        if (woLabourList.length > 0) {

            for (var o in woLabourList) {

              

                htmlstrLB += "<tr>";
                htmlstrLB += '<td class="h">' + woLabourList[o].EMPLOYEE + '</td>';
                //htmlstrLB += '<td class="d">' + woLabourList[o].HOURS + '</td>';
                //htmlstrLB += '<td class="d">' + woLabourList[o].BILLABLE + '</td>';
                htmlstrLB += '<td class="a">' + ah.formatJSONDateToString(woLabourList[o].LABOUR_DATE) + '</td>';
               
                htmlstrLB += "</tr>";
                htmlstrLB += "<tr>";
                htmlstrLB += '<td style="width:100%;" align="left" colspan="4"><strong>Action Details</strong></td>';
                htmlstrLB += "</tr>";
                htmlstrLB += "<tr>";
                htmlstrLB += '<td style="border-top:none;border-bottom:none; width:100%;" colspan="4">' + woLabourList[o].LABOUR_ACTION+'</td>';
                htmlstrLB += "</tr>";

            }


            $(ah.config.id.woLabourInfoQV + ' ' + ah.config.tag.tbody).append(htmlstrLB);
        } else {
            $(ah.config.id.woLabourInfoQV).hide();
            $("#norecLabour").show();
        }



        $(ah.config.id.searchResultListQVParts + ' ' + ah.config.tag.tbody).html('');
        var srparts = jsonData.AM_PARTSREQUEST;
        var htmlstr = "";

            if (srparts.length > 0) {
                sparePartsCount = 1;
                $(ah.config.id.searchResultListParts).show();
                for (var o in srparts) {

                    var billable = 'Yes';
                    if (srparts[o].BILLABLE === "false") {
                        billable = 'No';
                    }
                    if (srparts[o].PRODUCT_DESC === null || srparts[o].PRODUCT_DESC === "") {
                        srparts[o].PRODUCT_DESC = srparts[o].DESCRIPTION;
                        srparts[o].ID_PARTS = 'Supplier'
                    }
                    if (srparts[o].USED_QTY == null) {
                        srparts[o].USED_QTY = 0;
                    }

                    htmlstr += "<tr>";
                    htmlstr += '<td class="a">' + srparts[o].ID_PARTS + '</td>';
                    htmlstr += '<td class="">' + srparts[o].PART_NUMBERS + '</td>';
                    htmlstr += '<td class="h">' + srparts[o].PRODUCT_DESC + '</td>';
                    htmlstr += '<td class="a">' + srparts[o].STATUS + '</td>';
                    htmlstr += '<td class="a">' + srparts[o].QTY_RETURN + '</td>';
                    htmlstr += '<td class="a">' + srparts[o].USED_QTY + '</td>';
                    htmlstr += '<td class="a">' + srparts[o].TRANSFER_QTY + '</td>';
                    htmlstr += '<td class="a">' + srparts[o].QTY + '</td>';
                    htmlstr += '<td class="a">' + srparts[o].PRICE + '</td>';
                    //htmlstr += '<td class="a">' + '' + '</td>';
                    htmlstr += '<td class="a">' + srparts[o].EXTENDED + '</td>';
                    htmlstr += '<td class="a">' + billable + '</td>';
                    htmlstr += "</tr>";
                }

            $(ah.config.id.searchResultListQVParts + ' ' + ah.config.tag.tbody).append(htmlstr);
        } else {
            $(ah.config.id.searchResultListQVParts).hide();
            $("#norecParts").show();
        }
    
        window.location.href = ah.config.url.openModalQuickView;
    }
    self.searchSparePartsOnhandResult = function (jsonData) {
        $('#searchingLOV').hide();
        var sr = jsonData.PARTS_STOCKONHAND_VI;

       // alert(JSON.stringify(sr));
        //var woStatus = $("#WO_STATUS").val();
        //if (self.validatePrivileges('20') === 0 && woStatus === 'CL') {
        //    $("#addButtonModal").hide();
        //    $("#sparePartsList").hide();
        //}


        $(ah.config.id.searchResultPartsOnhand + ' ' + ah.config.tag.ul).html('');

        if (sr.length > 0) {

            var htmlstr = '';
            //alert(JSON.stringify(sr));
            for (var o in sr) {
                var partDescription = sr[o].PRODUCT_DESC;
                if (partDescription.length > 50) {
                    partDescription = partDescription.substring(0, 50) + '...';
                }
                // alert(partDescription);
                if (sr[o].PART_COST === null || sr[o].PART_COST === "") {
                    sr[o].PART_COST = 0;
                }
                if (sr[o].AVAILABLE_STOCK === null || sr[o].AVAILABLE_STOCK === "") {
                    sr[o].AVAILABLE_STOCK = "0";
                } 
                if (sr[o].PART_MODELS === null) {
                    sr[o].PART_MODELS = "";
                }

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplateList, JSON.stringify(sr[o]), sr[o].PRODUCT_CODE, partDescription, '');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateParts, systemText.searchModeLabels.partNo, sr[o].PART_NUMBERS +"&nbsp;&nbsp;");
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateParts, systemText.searchModeLabels.partModels, sr[o].PART_MODELS + "&nbsp;&nbsp;");
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateParts, systemText.searchModeLabels.storeDesc, sr[o].STORE_DESC +"&nbsp;&nbsp;");
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateParts, systemText.searchModeLabels.availableStock, sr[o].QTY +"&nbsp;&nbsp;");
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateParts, systemText.searchModeLabels.partCost, sr[o].PART_COST);

                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultPartsOnhand + ' ' + ah.config.tag.ul).append(htmlstr);
            $(ah.config.cls.detailItem).bind('click', self.addWOPart);
        }

        
    };
    self.removeItem = function (partsAddWOList) {
        self.partsAddWOList.remove(partsAddWOList);
    };
    self.addWOPart = function () {
        
        var worequestId = $("#REQ_ID").val();
        var wo_LabourType = 'In House';
        var wo_LabourId = 0;
        //var requestId = $("#REQ_ID").val();
        var assetNoID = $("#ASSET_NO").val();

      
            var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfoPartID));
            // var pmScheduleId = $("#AM_ASSET_PMSCHEDULE_ID").val();         
            var existPart = self.checkExist(infoID.PRODUCT_CODE);
            if (existPart > 0) {
                alert('Unable to Proceed. The selected spare part was already exist in Work Order Labour Materials.');
                return;
            }

            if (infoID.PRODUCT_DESC.length > 60)
                infoID.PRODUCT_DESC = infoID.PRODUCT_DESC.substring(0, 59) + "...";

            if (infoID.STORE_DESC.length > 40)
                infoID.STORE_DESC = infoID.STORE_DESC.substring(0, 39) + "...";
          
            self.partsAddWOList.push({
                PRODUCT_DESC: infoID.PRODUCT_DESC,
                DESCRIPTION: infoID.DESCRIPTION,
                ID_PARTS: infoID.PRODUCT_CODE,
                STORE_DESC: infoID.STORE_DESC,
                //STORE_ID: infoID.STORE_ID,
                BILLABLE: false,
                PRICE: infoID.PART_COST,
                //REQUEST_ID: requestId,
                STATUSREQ: 'Request',
                QTY_RETURN:0,
                TRANSFER_QTY: 0,
                EXTENDED: 0,
                STORE_ID: infoID.STORE,  //   duplicated
                REF_WO: worequestId,
                WO_LABOURTYPE: wo_LabourType,
                STATUS: 'Request',
                REF_ASSETNO: assetNoID,
                AM_WORKORDER_LABOUR_ID: wo_LabourId,
                PR_REQUEST_ID: 0,
                QTY: 0,
                USED_QTY: null,
                WOSTATUS: 'NE',
                TOTUSED: 0,
                PART_NUMBERS: infoID.PART_NUMBERS,
                PART_MODELS: infoID.PART_MODELS,
                FOR_COLLECTION_QTY: 0,
                FOR_COLLECTION_CANCELBY: null,
                FOR_COLLECTION_CANCELDATE: null,
                LAST_MODIFIEDBY: null,
                LAST_MODIFIEDDATE: null
            });
    
            $("#PARTS_CONFIRMED").val('N');
            $("#confirmBtn").hide();

    };
    self.checkExist = function (idPart) {

        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.partsAddWOList);
        var partLists = JSON.parse(jsonObj);

        var partExist = partLists.filter(function (item) { return item.ID_PARTS === idPart });

        return partExist.length;
    };
    self.printWO = function () {
        document.getElementById("printDetails").classList.add('printSummary');
        document.getElementById("printSticker").classList.remove('printSummary');
        document.getElementById("printBulk").classList.remove('printSummary');
        document.getElementById("OutOfOrderSticker").classList.remove('printSummary');
        document.getElementById("InitialAccpSticker").classList.remove('printSummary');
        
        setTimeout(function () { window.print(); }, 1000);

      //  window.print();

    }
    self.populateBulkPrint = function (jsonData) {
       
        var sr = jsonData.WORD_ORDER_LIST;
        $(ah.config.id.printBulk + ' ' + ah.config.tag.tbody).html('');
        if (sr.length > 0) {

            var clientInformation = jsonData.AM_CLIENT_INFO;
            
            self.woDetails.CLIENTNAME(clientInformation.DESCRIPTION);
            self.woDetails.CLIENTADDRESS(clientInformation.ADDRESS + ', ' + clientInformation.CITY + ', ' + clientInformation.ZIPCODE + ', ' + clientInformation.STATE);
            self.woDetails.CLIENTCONTACT('Tel.No: ' + clientInformation.CONTACT_NO1 + ' Fax No:' + clientInformation.CONTACT_NO2);

            var srcount = sr.length;
            var countloop = 0;
            var htmlstr = '';
            for (var o in sr) {
                countloop++;
                if (sr[o].ASSET_NO !== null) {
                    if (sr[o].ASSET_NO !== null && sr[o].ASSET_NO !== "") {

                        self.woDetails.ASSET_NO(sr[o].ASSET_NO);
                        
                        self.woDetails.COST_CENTER(sr[o].COSTCENTER);
                        self.woDetails.DESCRIPTION(sr[o].ASSETDESCRIPTION);
                        self.woDetails.LOCATION(sr[o].LOCATION);
                        self.woDetails.DEVICE_TYPE(sr[o].DEVICETYPE);
                        self.woDetails.RESPONSIBLE_CENTER(sr[o].RESPCENTER);
                        self.woDetails.STATUS(sr[o].STATUS);
                        self.woDetails.MODEL_NO(sr[o].MODEL_NO);
                        self.woDetails.SERIAL_NO(sr[o].SERIAL_NO);
                        //self.woDetails.WARRANTY_NOTES(sr[o].WARRANTY_NOTES);
                        //self.woDetails.WARRANTY_INCLUDE(sr[o].WARRANTYINCLUDED);
                        //self.woDetails.WARRANTY_EXPIRYDATE(ah.formatJSONDateToString(sr[o].WARRANTY_EXPIRYDATE));
                        //self.woDetails.TERM_PERIOD(sr[o].TERM_PERIOD + ' ' + sr[o].WARRANTY_FREQUNIT);
                    }
                }
                var wr = jsonData.WARRANTYLIST;
                
                wr = wr.filter(function (item) { return item.ASSET_NO == sr[o].ASSET_NO });
               
                $("#warranty2").hide();
                $("#warranty3").hide();
                $("#warrantyNotes").text("");
                $("#warrantyInclude").text("");
                $("#warrantyExpiry").text("");
                $("#warrantyTerm").text("");

                if (wr.length) {
                   // alert(JSON.stringify(wr));

                    for (var j in wr) {
                        if (j == 0) {
                            $("#warrantyNotes").text(wr[j].WARRANTY_NOTES);
                            $("#warrantyInclude").text(wr[j].WARRANTYDESC);
                            $("#warrantyExpiry").text(ah.formatJSONDateToString(wr[j].WARRANTY_EXPIRY));
                            $("#warrantyTerm").text(wr[j].FREQUENCY_PERIOD + ' ' + wr[j].FREQUENCY);
                        }
                        if (j == 1) {
                            $("#warranty2").show();
                            $("#warrantyNotes2").text(wr[j].WARRANTY_NOTES);
                            $("#warrantyInclude2").text(wr[j].WARRANTYDESC);
                            $("#warrantyExpiry2").text(ah.formatJSONDateToString(wr[j].WARRANTY_EXPIRY));
                            $("#warrantyTerm2").text(wr[j].FREQUENCY_PERIOD + ' ' + wr[j].FREQUENCY);
                        }
                        if (j == 2) {
                            $("#warranty3").show();
                            $("#warrantyNotes3").text(wr[j].WARRANTY_NOTES);
                            $("#warrantyInclude3").text(wr[j].WARRANTYDESC);
                            $("#warrantyExpiry3").text(ah.formatJSONDateToString(wr[j].WARRANTY_EXPIRY));
                            $("#warrantyTerm3").text(wr[j].FREQUENCY_PERIOD + ' ' + wr[j].FREQUENCY);
                        }

                    }
                }
                if (sr[o].WO_STATUS === 'CL') {
                    sr[o].WO_STATUS = 'Closed';
                } else if (sr[o].WO_STATUS === 'OP') {
                    sr[o].WO_STATUS = 'Open';
                }
                else if (sr[o].WO_STATUS === 'HA') {
                    sr[o].WO_STATUS = 'On Hold';
                } else if (sr[o].WO_STATUS === 'FA') {
                    sr[o].WO_STATUS = 'HELD';
                } else if (sr[o].WO_STATUS === 'NE') {
                    sr[o].WO_STATUS = 'New';
                } else if (sr[o].WO_STATUS === 'PS') {
                    sr[o].WO_STATUS = 'Posted';
                } else if (sr[o].WO_STATUS === 'FC') {
                    sr[o].WO_STATUS = 'For Close';
                }
                if (sr[o].REQ_BY === null) {
                    sr[o].REQ_BY = '';
                }
                
                self.woDetails.WO_STATUS(sr[o].WO_STATUS);
                self.woDetails.ASSIGN_TO(sr[o].STAFFNAME);
         
                self.woDetails.JOB_DUE_DATE(ah.formatJSONDateToString(sr[o].JOB_DUE_DATE));
                self.woDetails.JOB_TYPE(sr[o].JOBTYPEDESC);
                //self.woDetails.JOB_URGENCY(sr[o].STAFFNAME);
                self.woDetails.PROBLEM(sr[o].PROBLEM);
                self.woDetails.REQ_BY(sr[o].REQ_BY);
                self.woDetails.REQ_DATE(ah.formatJSONDateTimeToString(sr[o].REQ_DATE));
                self.woDetails.REQ_NO(sr[o].REQ_NO);
                self.woDetails.REQTICKETNO(sr[o].TICKET_ID);
                self.woDetails.REQ_TYPE(sr[o].PROBLEMTYPE);
                self.woDetails.SERV_DEPT(sr[o].SERVDEPARTMENT);

                var wolInformation = jsonData.AM_WORKORDER_LABOURLIST;
               
                var curwolInformation = wolInformation.filter(function (item) { return item.REQ_NO == sr[o].REQ_NO });
                $(ah.config.id.wolabourinfo + ' ' + ah.config.tag.tbody).html('');

                if (curwolInformation.length > 0) {
                    var htmlstrwo = null;
                    $("#woLabourInfoListDetails").show();
                    for (var x in curwolInformation) {
                        var employee = null;
                        if (curwolInformation[x].EMPLOYEE === null || curwolInformation[x].EMPLOYEE === "") {
                            //alert(wolInformation[0].SUPPLIER);
                            employee = curwolInformation[x].SUPPLIER;
                        } else {
                            employee = curwolInformation[x].EMPLOYEE;
                        }
                        if (curwolInformation[x].BILLABLE === 'Y') {
                            curwolInformation[x].BILLABLE = 'Yes';
                        } else {
                            curwolInformation[x].BILLABLE = 'No';
                        }

                        htmlstrwo += "<tr>";
                        htmlstrwo += '<td >' + employee + '</td>';
                        htmlstrwo += '<td >' + curwolInformation[x].HOURS + '</td>';

                        htmlstrwo += '<td >' + curwolInformation[x].BILLABLE + '</td>';
                        htmlstrwo += '<td >' + ah.formatJSONDateToString(curwolInformation[x].LABOUR_DATE) + '</td>';

                        htmlstrwo += "</tr>";
                        htmlstrwo += '<tr > <td style="width:100%;" align="left" colspan="4">Action Details</td> </tr>';
                        htmlstrwo += '<tr > <td style="width:100%;" align="left" colspan="4">' + curwolInformation[x].LABOUR_ACTION + '</td> </tr>';

                        htmlstrwo += '</tr >';

                    }
                    $(ah.config.id.wolabourinfo + ' ' + ah.config.tag.tbody).append(htmlstrwo);

                } else {
                    $("#woLabourInfoListDetails").hide();
                }

                
                var wopmPartsr = jsonData.AM_SPAREPARTSREQUEST;
                var curparts = wopmPartsr.filter(function (item) { return item.REF_WO == sr[o].REQ_NO });
               
                if (curparts.length > 0) {
                    $("#woMaterialInfo").show();
                    self.partsWLList.splice(0, 5000);
                    
                    for (var o in curparts) {

                        var qtyactu = 0;
                        var confirmFlag = $("#PARTS_CONFIRMED").val();
                        if (confirmFlag == "Y") {
                            if (curparts[o].STATUS != "Received") {
                                qtyactu = curparts[o].QTY;
                            } else {
                                qtyactu = curparts[o].USED_QTY;
                            }

                        } else {
                            if (curparts[o].USED_QTY == 0 && curparts[o].STATUS == "Received") {
                                qtyactu = curparts[o].QTY;
                            } else {

                                if (curparts[o].USED_QTY > 0) {

                                    qtyactu = curparts[o].USED_QTY;
                                } else {

                                    qtyactu = curparts[o].QTY;
                                }

                            }

                        }


                        if (curparts[o].PRODUCT_DESC === null || curparts[o].PRODUCT_DESC === "") {
                            curparts[o].PRODUCT_DESC = curparts[o].DESCRIPTION;
                            curparts[o].ID_PARTS = 'Supplier'
                        }
                        self.partsWLList.push({
                            PRODUCT_DESC: curparts[o].PRODUCT_DESC,
                            DESCRIPTION: curparts[o].DESCRIPTION,
                            ID_PARTS: curparts[o].ID_PARTS,
                            QTY: curparts

                        });



                    }
                }
                else {
                    $("#woMaterialInfo").hide();

                }

                var MyDiv1 = document.getElementById('printDetails');
               
                htmlstr += '<tr style="width: 850px; border-color:transparent;" class="page-break">';
                htmlstr += '<td style="width: 850px; border-color:transparent;">';
                htmlstr += MyDiv1.innerHTML;
                htmlstr += "</td>";
                htmlstr += "</tr>";
                
               
                if (countloop === srcount) {
                    //alert(htmlstr);
                    //alert('here');
                    $(ah.config.id.printBulk + ' ' + ah.config.tag.tbody).append(htmlstr);

                    
                    document.getElementById("printSummary").classList.remove('printSummary');
                    document.getElementById("printDetails").classList.remove('printSummary');
                    document.getElementById("printBulk").classList.add('printSummary');
                    window.location.href = ah.config.url.modalClose;
                    setTimeout(function () { window.print(); }, 1000);
                }

            }            
        }
    };
    self.populateAssetInfoById = function () {
        var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfoId).toString());
        
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        self.searchAssetFilter.assetNo = infoID.ASSET_NO;

        postData = { SEARCH: "", ASSET_NO: infoID.ASSET_NO, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readAssetInfoV, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.populateAssetInfo = function (jsonData) {
      
        var infoID = jsonData.AM_ASSET_DETAILS;//JSON.parse(this.getAttribute(ah.config.attr.datainfoId).toString());
      
      //  sessionStorage.setItem(ah.config.skey.assetItems, JSON.stringify(infoID));
        //alert(JSON.stringify(infoID));
      
        $("#ASSET_NO").val(infoID.ASSET_NO);
        $("#DESCRIPTION").val(infoID.DESCRIPTION);
        $("#CATEGORY").val(infoID.DTYPE_DESC);
        $("#SERIAL_NO").val(infoID.SERIAL_NO);
        $("#MODEL_NO").val(infoID.MODEL_NO);
        $("#BUILDING").val(infoID.BUILDING);
        $("#COST_CENTER").val(infoID.COSTCENTER_DESC);
        $("#ASSET_COSTCENTERCODE").val(infoID.COST_CENTER);
        
        
        $("#LOCATION").val(infoID.LOCATION);
        $("#CONDITION").val(infoID.CONDITION);
        $("#ASSETSTATUS").val(infoID.STATUS);
        $("#ASSET_NO+a").next("span").remove();
        $("#ERECORD_GENERAL_NOTES").val(infoID.ERECORD_GENERAL_NOTES);
        $("#WARRANTY_EXPIRYDATE").val(ah.formatJSONDateTimeToString(infoID.WARRANTY_EXPIRYDATE));
        $("#TERM_PERIOD").val(infoID.TERM_PERIOD);
        $("#WARRANTY_INCLUDE").val(infoID.WARRANTY_DESC);
        $("#WARRANTY_NOTES").val(infoID.WARRANTY_NOTES);

        $("#SUN_AM").val(ah.formatJSONTimeToString(infoID.SUN_AM));
        $("#MON_AM").val(ah.formatJSONTimeToString(infoID.MON_AM));
        $("#TUE_AM").val(ah.formatJSONTimeToString(infoID.TUE_AM));
        $("#WED_AM").val(ah.formatJSONTimeToString(infoID.WED_AM));
        $("#THUR_AM").val(ah.formatJSONTimeToString(infoID.THUR_AM));
        $("#FRI_AM").val(ah.formatJSONTimeToString(infoID.FRI_AM));
        $("#SAT_AM").val(ah.formatJSONTimeToString(infoID.SAT_AM));
        $("#SUN_PM").val(ah.formatJSONTimeToString(infoID.SUN_PM));
        $("#MON_PM").val(ah.formatJSONTimeToString(infoID.MON_PM));
        $("#TUE_PM").val(ah.formatJSONTimeToString(infoID.TUE_PM));
        $("#WED_PM").val(ah.formatJSONTimeToString(infoID.WED_PM));
        $("#THUR_PM").val(ah.formatJSONTimeToString(infoID.THUR_PM));
        $("#FRI_PM").val(ah.formatJSONTimeToString(infoID.FRI_PM));
        $("#SAT_PM").val(ah.formatJSONTimeToString(infoID.SAT_PM));
       
        $("#ASSET_COSTCENTERCODEXZ").val(infoID.COST_CENTER);
       
        
    };
    self.statusUpdate = function(){
        var currStatus = $(ah.config.id.woStatus).val();
        if (currStatus === 'FC') {
            if (self.validatePrivileges('32') === 0) {
                self.alertMessage("Unable to proceed. Your are not allowed to update status to for close.");
                var currStatus = $("#CURRSTATUS").val();
                $("#WO_STATUS").val(currStatus);
                return;
            } 

        }
     
        if (currStatus === 'CL') {
            var partsConfirm = $("#PARTS_CONFIRMED").val();
            var reqType = $("#REQ_TYPE").val();
            
         
            if (sparePartsCount > 0 && reqType !="SR") {
                if (partsConfirm !== "Y") {
                    self.alertMessage("Unable to proceed. Please make sure that the spare parts Used QTY is Confirmed.");
                    var currStatus = $("#CURRSTATUS").val();
                    $("#WO_STATUS").val(currStatus);
                    return;
                }
            }
           

            if (self.validatePrivileges('22') === 0) {
                self.alertMessage("Unable to proceed. Your are not allowed to close work order'");
                var currStatus = $("#CURRSTATUS").val();
                $("#WO_STATUS").val(currStatus);
                return;
            } 
            $("#lblClosedDate").show();
            $("#CLOSED_DATE").show();

            var jDate = new Date();
            var strDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);

            var closedDate = strDate + ' ' + hourmin;

            $(ah.config.id.closeDate).val(closedDate);
        } else {
            $(ah.config.id.closeDate).val(null);
            $("#lblClosedDate").hide();
            $("#CLOSED_DATE").hide();
        }
        

    };
    self.readSetupID = function () {
        
        var reqID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        sparePartsCount = 0;
        ah.toggleReadMode();
        $("#REQ_ID").show();
        $("label[for='REQ_ID']").show();
        postData = { REQ_NO: reqID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.readAssetNo = function (assetNo) {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;


        postData = { ASSET_NO: assetNo, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readAssetNo, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

        postData = { ASSET_NO: assetNo, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchPM, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.readWOId = function (refID) {
        
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { REQ_NO: refID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.assetInfo = function () {
        assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        var otherAsset = ah.getParameterValueByName(ah.config.fld.otherAsset);

        if (otherAsset == "otherasset") {
            
            window.location.href = encodeURI(ah.config.url.aimsInformationOth + "?ASSETNO=" + assetNoID);
        } else {
            window.location.href = encodeURI(ah.config.url.aimsInformation + "?ASSETNO=" + assetNoID);
        }
        
    };
    self.assetInfoDet = function () {
        assetNoID = $("#ASSET_NO").val();
        var reqNo = $("#REQ_NO").val();
        var otherAsset = ah.getParameterValueByName(ah.config.fld.otherAsset);
        var groupCode = $("#GROUP_CODE").val();
  
        if (groupCode == "MULTICC") {
            window.location.href = encodeURI(ah.config.url.aimsInformationOth + "?ASSETNO=" + assetNoID + "&REQNO=" + reqNo);
        } else {
            window.location.href = encodeURI(ah.config.url.aimsInformation + "?ASSETNO=" + assetNoID + "&REQNO=" + reqNo);
        }
        
    };
    self.assetSearchListNow = function () {
        var searchStr = $(ah.config.id.txtGlobalSearchOth).val().toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        
        self.searchAssetFilter.assetNo = '';
        $("#searchingAssetLOV").show();

        if (self.validatePrivileges('15') === 0) {
            self.searchAssetFilter.staffId = staffDetails.STAFF_ID;
        }

        postData = { SEARCH: searchStr, SEARCH_FILTER: self.searchAssetFilter, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAssets, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.readWSId = function () {

        
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
		var WSId = $("#AM_INSPECTION_ID").val();
        var isValid = !isNaN(parseFloat(WSId)) && isFinite(WSId);
        if (isValid === false) {
			$("#AM_INSPECTION_ID").val(null);
            alert('Please enter a valid Work Sheet Inspection code to proceed.');
            return;
        }
        postData = { AM_INSPECTION_ID: WSId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readWSInspection, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.populateLastsearch = function () {
        $(ah.config.id.txtGlobalSearch).val(SsearchTxt);
       
        
        $("#filterProblemType").val(SproblemType);
        $("#filterPMType").val(SproblemType);
        $("#filterStatus").val(Sstatus);
        $("#filterPriority").val(Spriority);
        $("#filterJobType").val(SjobType);
        $("#filterAssignTo").val(SassignTo);
        $("#filterAssignType").val(SassignType);
        $("#requestFromDate").val(SfromDate);
        $("#requestStartDate").val(StoDate);

        
    };
    self.getLastSearch = function () {
        SsearchTxt = $("#txtGlobalSearch").val();
        SproblemType = $("#filterProblemType").val();
        Spmtype = $("#filterPMType").val();
        Sstatus = $("#filterStatus").val();
        Spriority = $("#filterPriority").val();
        SjobType = $("#filterJobType").val();
        SassignTo= $("#filterAssignTo").val();
        SassignType = $("#filterAssignType").val();
        SfromDate = $("#requestFromDate").val();
        StoDate = $("#requestStartDate").val();
    };
    self.displayAllSearch = function () {
        var lovByAction = $("#LOVBYACION").val();
        if (lovByAction == "SUPPIER") {
            if (supplierListLOV.length > 0) {
                self.searchResultSupplierListLov("");
            } else {
                self.supplierSearch();
            }

        } else if (lovByAction == "STAFF") {
            self.searchResultStaffListLov("");
        } else if (lovByAction =="COSTCENTER"){
            self.costCenterSearch();
        }
    }
    self.supplierSearch = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        postData = { SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchSupplier, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.costCenterSearch = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;
        postData = { LOV: "'AIMS_COSTCENTER'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchLOVQuery, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.staffSearch = function () {

    };
    self.filterSearchStaff = function () {

       
       
        var parent = document.getElementById("filterAssignToList");
        var childArray = parent.children;
        var cL = childArray.length;
        while (cL > 0) {
            cL--;
            parent.removeChild(childArray[cL]);
        }


        var activeStaff = $("#staffActive").is(":checked");
        var inactiveStaff = $("#staffInactive").is(":checked");
        var loadAction = "";
        var loadStaffList = ko.observableArray([]);
        if (activeStaff == true) {
            loadAction = "A";
        }
        if (inactiveStaff == true) {
            loadAction = "IA";
        }
        if (activeStaff == true && inactiveStaff == true) {
            loadAction = "ALL";
        }
        if (activeStaff == false && inactiveStaff == false) {
            loadAction = "ALL";
        }
      
        if (loadAction == "A") {

            loadStaffList = srStaffList;
            loadStaffList = loadStaffList.filter(function (item) { return item.STATUS === 'ACTIVE' });
            
            $.each(loadStaffList, function (item) {

                $(ah.config.id.filterAssignToList)
                    .append($("<option>")
                        .attr("value", loadStaffList[item].STAFF_FULLNAME)
                        .attr("id", loadStaffList[item].ASSIGN_TO));
            });


        } else if (loadAction == "IA") {
            loadStaffList = srStaffList;
            loadStaffList = loadStaffList.filter(function (item) { return item.STATUS === 'INACTIVE' });

            $.each(loadStaffList, function (item) {

                $(ah.config.id.filterAssignToList)
                    .append($("<option>")
                        .attr("value", loadStaffList[item].STAFF_FULLNAME)
                        .attr("id", loadStaffList[item].ASSIGN_TO));
            });

        } else if (loadAction == "ALL") {

            loadStaffList = srStaffList;
          
            $.each(loadStaffList, function (item) {

                $(ah.config.id.filterAssignToList)
                    .append($("<option>")
                        .attr("value", loadStaffList[item].STAFF_FULLNAME)
                        .attr("id", loadStaffList[item].ASSIGN_TO));
            });

        }


    }
    self.searchNow = function () {
        $("#assetInfoDet").hide();
        self.getLastSearch();

        var reqNO = assetNoID = ah.getParameterValueByName(ah.config.fld.reqNo);
        var assetNoIDd = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
        var dsdfx = ah.getParameterValueByName(ah.config.fld.reqWOAction);
        if (assetNoIDd !== null && assetNoIDd !== "") {
            
        } else {
            urlText = 'ljllk';
            history.pushState('', 'New Page Title', ah.config.url.aimsWOSearch);
        }

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        var overdue = $("#overDue").is(":checked");
        if (overdue == true) {
            var jDate = new Date();
            var dueDate = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);

            self.searchFilter.overDue = dueDate + "  23:00:00.000";
        } else {
            self.searchFilter.overDue = null;
        }
        

        var unassigned = $("#unAssigned").is(":checked");
        self.searchFilter.unAssigned = unassigned;
        //if (overdue == true) {
        //    self.searchFilter.overDue = overdue;
        //}
        //return;


        ah.toggleSearchMode();
        assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);


        var fromDate = $('#requestFromDate').val().split('/');
        if (fromDate.length == 3) {
            self.searchFilter.fromDate(fromDate[2] + "/" + fromDate[1] + "/" + fromDate[0]);
        }
        var toDate = $('#requestStartDate').val().split('/');
        if (toDate.length == 3) {
            self.searchFilter.toDate(toDate[2] + "/" + toDate[1] + "/" + toDate[0] + "  23:00:00.000");
        }
		
        var staffId = "";
        if (self.validatePrivileges('12') === 0) {

            if (self.validatePrivileges('31') === 0) {

                if (self.validatePrivileges('4') === 0) {
                    staffId = staffDetails.STAFF_ID;
                } else {
                    self.searchFilter.userConstCenter = staffDetails.STAFF_ID;
                    self.searchFilter.status = "";
                }
            }
        };
       
        
        if (self.validatePrivileges('6') === 0) {
            $(ah.config.cls.liAddNew).hide();
        };

        var activeStaff = $("#staffActive").is(":checked");
        var inactiveStaff = $("#staffInactive").is(":checked");
        if (activeStaff == true) {
            self.searchFilter.staffStatus = "A";
        }
        if (inactiveStaff == true) {
            self.searchFilter.staffStatus = "IA";
        }
        if (activeStaff == true && inactiveStaff ==true) {
            self.searchFilter.staffStatus = "";
        }
        if (activeStaff == false && inactiveStaff == false) {
            self.searchFilter.staffStatus = "";
        }

        var searchStr = $(ah.config.id.txtGlobalSearch).val().trim()
      
        postData = { CLIENT_ID: staffDetails.CLIENT_CODE, SEARCH: searchStr.toString(), ASSETNO: assetNoID, STAFF_ID: staffId, STAFF_LOGON_SESSIONS: staffLogonSessions, SEARCH_FILTER: self.searchFilter, STATUS: ""};
       
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.partsSearchNow = function () {
        $('#searchingLOV').hide();
        ah.toggleDisplayMode();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        //ah.toggleSearchItems();
        var wo_reqId = $("#REQ_ID").val();
        postData = { SEARCH: "", REQ_NO: wo_reqId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.partssearchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
       
        $("#loadNumber1").text(1);
    };
    self.searchContractNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

       
        var assetNo = $("#ASSET_NO").val();
        if (assetNo === null || assetNo === "") {
            alert('Unable to proceed.Asset must be provided.')
            return;
        }
        ah.CurrentMode = ah.config.mode.search;
        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), ASSETNO: assetNo, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchContract, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.searchStatusLogNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        var reqId = $("#REQ_NO").val();       
        ah.CurrentMode = ah.config.mode.search;
        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), REQ_NO: reqId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchWOStatus, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.UploadImage = function (jsonData) {
        var scanInfo = jsonData.SCANDOCS;

        var fileNameInfo = scanInfo.FILE_NAME;
        var frmData = new FormData();
        var filebase = $("#pdffile").get(0);
        var files = filebase.files;
        frmData.append("docFilename", fileNameInfo);
        frmData.append(files[0].name, files[0]);
        $.ajax({
            url: '/Upload/SaveDocument',
            type: "POST",
            contentType: false,
            processData: false,
            data: frmData,
            success: function (data) {
                alert('Success Upload');
            },
            error: function (err) {
                alert(error);
            }
        });

        var sr = srDataArray;
  
        $(ah.config.id.supportingDocs + ' ' + ah.config.tag.ul).html('');
        if (sr.length > 0) {

            var htmlstr = '';

            for (var o in sr) {

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderDOCTemplateList, sr[o].FILE_NAME, sr[o].FILE_NAME, '', '');

                htmlstr += '</li>';

            }

            $(ah.config.id.supportingDocs + ' ' + ah.config.tag.ul).append(htmlstr);
            $(ah.config.cls.detailItem).bind('click', self.readDoc);
        }
    };
    self.validatePrivileges = function (privilegeId) {
        var groupPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));

        var groupPrivileges = groupPrivileges.filter(function (item) { return item.PRIVILEGES_ID === privilegeId });
        return groupPrivileges.length;
    };
    self.getDateNow = function () {
        var dateNow = moment();
        return dateNow;
    }
    self.isDateFutre = function (date1, date2) {
        if (date1 < date2) {
            return true;
        }
        return false;

    }
    self.materialSaveAllChanges = function () {

        //$(ah.config.id.searchResultListPartstBody + ' ' + ah.config.tag.tr).html('');
        $(ah.config.id.searchResultListParts + ' ' + ah.config.tag.tbody).html('');
        jsonObj = ko.utils.stringifyJson(self.partsAddWOList);
        var wolParts = JSON.parse(jsonObj);
        
        if (wolParts.length > 0) {
            sparePartsCount = 1;
            $(ah.config.id.searchResultListParts).show();
            var htmlstr = '';
            for (var i = 0; i < wolParts.length; i++) {
                htmlstr += "<tr>";
                htmlstr += '<td class="a">' + wolParts[i].ID_PARTS + '</td>';
                htmlstr += '<td class="h">' + wolParts[i].PRODUCT_DESC + '</td>';
                htmlstr += '<td class="a">' + wolParts[i].QTY + '</td>';
                htmlstr += "</tr>";
            }
            $(ah.config.id.searchResultListParts + ' ' + ah.config.tag.tbody).append(htmlstr);

            $("#PARTS_CONFIRMED").val("N");

            self.saveWOLMInfo("APPLY");
        }
        else {
            self.deleteWOLMInfo();
        }
        
       

    };

    self.materialConfirmSaveAllChanges = function () {

        //$(ah.config.id.searchResultListPartstBody + ' ' + ah.config.tag.tr).html('');
        $(ah.config.id.searchResultListParts + ' ' + ah.config.tag.tbody).html('');
        jsonObj = ko.utils.stringifyJson(self.partsAddWOList);
        var wolParts = JSON.parse(jsonObj);
        var htmlstr = '';
        for (var i = 0; i < wolParts.length; i++) {
            if (wolParts[i].USED_QTY > wolParts[i].TRANSFER_QTY) {
                $("#NOREC").text("Warning: Spare Part " + wolParts[i].PRODUCT_DESC + ' Used Qty cannot be greater than dispatched');
                $("#NOREC").show();
                return;
            }
           
            if ((wolParts[i].USED_QTY === null || wolParts[i].USED_QTY === '') && wolParts[i].TRANSFER_QTY > 0) {
                wolParts[i].USED_QTY = 0;
                $("#NOREC").text("Warning: Spare Part " + wolParts[i].PRODUCT_DESC + "Used Qty cannot be empty.");
                $("#NOREC").show();
                return;
            }
            if (wolParts[i].USED_QTY < 0) {
                $("#NOREC").text("Warning: Spare Part " + wolParts[i].PRODUCT_DESC + ' Used Qty cannot be less than zero.');
                $("#NOREC").show();
                return;
            }

            if (wolParts[i].TRANSFER_FLAG =="F") {
                $("#NOREC").text("Warning: Spare Part " + wolParts[i].PRODUCT_DESC + ' is for collection. If you wish to proceed, please cancel qty for collection.');
                $("#NOREC").show();
                return;
            }

            htmlstr += "<tr>";
            htmlstr += '<td class="a">' + wolParts[i].ID_PARTS + '</td>';
            htmlstr += '<td class="h">' + wolParts[i].PRODUCT_DESC + '</td>';
            htmlstr += '<td class="a">' + wolParts[i].QTY + '</td>';
            htmlstr += "</tr>";
        }
        $("#PARTS_CONFIRMED").val("Y");

        $(ah.config.id.searchResultListParts + ' ' + ah.config.tag.tbody).append(htmlstr);

       // $(ah.config.id.saveMaterialInfo).trigger('click');
        self.saveWOLMInfo("CONFIRM");

    };
    self.materialReceiveSaveAllChanges = function () {

        //$(ah.config.id.searchResultListPartstBody + ' ' + ah.config.tag.tr).html('');
        $(ah.config.id.searchResultListParts + ' ' + ah.config.tag.tbody).html('');
        jsonObj = ko.utils.stringifyJson(self.partsAddWOList);
        var wolParts = JSON.parse(jsonObj);
        var htmlstr = '';
        var statusDistpatch = 0;
        for (var i = 0; i < wolParts.length; i++) {
            
            if (wolParts[i].TRANSFER_QTY > 0) {
                statusDistpatch = 1;
            }

            htmlstr += "<tr>";
            htmlstr += '<td class="a">' + wolParts[i].ID_PARTS + '</td>';
            htmlstr += '<td class="h">' + wolParts[i].PRODUCT_DESC + '</td>';
            htmlstr += '<td class="a">' + wolParts[i].QTY + '</td>';
            htmlstr += "</tr>";
        }
        
        if (statusDistpatch == 0) {
            alert('Unable to proceed. There are not parts to receive.')
            return;
        }
       
        $(ah.config.id.searchResultListParts + ' ' + ah.config.tag.tbody).append(htmlstr);

        //$(ah.config.id.saveMaterialInfo).trigger('click');

        self.saveWOLMInfo("RECEIVED");
    };
    self.deleteWOLMInfo = function (actiontaken) {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var itemDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfoParts)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        var reqNo = $("#REQ_ID").val();
        
        postData = { REQ_NO: reqNo,  STAFF_LOGON_SESSIONS: staffLogonSessions };
        //alert(JSON.stringify(postData));
        $.post(self.getApi() + ah.config.api.deleteWOSpare, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

        window.location.href = encodeURI(ah.config.url.modalClose);
    };
    self.saveWOLMInfo = function (actiontaken) {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var itemDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfoParts)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
       
        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.partsAddWOList);
        var wolParts = JSON.parse(jsonObj);
        //alert(JSON.stringify(wolParts));
        for (var i = 0; i < wolParts.length; i++) {
        
            if (wolParts[i].FOR_COLLECTION_QTY == null || wolParts[i].FOR_COLLECTION_QTY == "") {
                wolParts[i].FOR_COLLECTION_QTY = 0;
            }
            if (wolParts[i].USED_QTY == null || wolParts[i].USED_QTY == "") {
                wolParts[i].USED_QTY = 0;
            }
            if (wolParts[i].QTY <= 0 || wolParts[i].QTY == null) {
               
                $("#NOREC").text("Warning: Spare Part " + wolParts[i].PRODUCT_DESC + " request QTY cannot be not empty or less than zero.");
                $("#NOREC").show();
                return;
            }

            if (wolParts[i].TRANSFER_QTY > wolParts[i].QTY) {
                $("#NOREC").text("Warning: Spare Part " + wolParts[i].PRODUCT_DESC + " request QTY cannot be less than dispatch QTY.");
                $("#NOREC").show();
                return;
            }

            if (wolParts[i].QTY_RETURN > 0) {

                var iQty = parseInt(wolParts[i].USED_QTY) + parseInt(wolParts[i].QTY_RETURN);
     
                if (iQty > wolParts[i].TRANSFER_QTY) {
                    $("#NOREC").text("Warning: Spare Part " + wolParts[i].PRODUCT_DESC + " The Used QTY plus Returned QTY cannot be greater than  Dispatch QTY.");
                    $("#NOREC").show();
                    return;
                }
            }

            if (wolParts[i].QTY_RETURN <= 0 || wolParts[i].QTY == null || wolParts[i].QTY == "") {
                wolParts[i].QTY_RETURN = 0;
            }
            if (wolParts[i].AM_WORKORDER_LABOUR_ID <= 0 || wolParts[i].AM_WORKORDER_LABOUR_ID == null || wolParts[i].AM_WORKORDER_LABOUR_ID == "") {
                wolParts[i].AM_WORKORDER_LABOUR_ID = 0;
            }
            if (wolParts[i].QTY_FORRETURN <= 0 || wolParts[i].QTY_FORRETURN == null || wolParts[i].QTY_FORRETURN == "") {
                wolParts[i].QTY_FORRETURN = 0;
            }
            if (wolParts[i].USED_QTY > wolParts[i].TRANSFER_QTY) {
                $("#NOREC").text("Warning: Spare Part " + wolParts[i].PRODUCT_DESC + " The Used QTY cannot be greater than  Dispatch QTY.");
                $("#NOREC").show();
                return;
            }
            if (wolParts[i].EXTENDED <= 0 || wolParts[i].EXTENDED == null || wolParts[i].EXTENDED == "") {
                wolParts[i].EXTENDED = 0;
            }
            if (wolParts[i].EXTENDED <= 0 || wolParts[i].EXTENDED == null || wolParts[i].EXTENDED == "") {
                wolParts[i].EXTENDED = 0;
            }
            if (wolParts[i].TRANSFER_QTY <= 0 || wolParts[i].TRANSFER_QTY == null || wolParts[i].TRANSFER_QTY == "") {
                wolParts[i].TRANSFER_QTY = 0;
            }

            if (wolParts[i].PR_REQUEST_QTY <= 0 || wolParts[i].PR_REQUEST_QTY == null || wolParts[i].PR_REQUEST_QTY == "") {
                wolParts[i].PR_REQUEST_QTY = 0;
            }
            
            
        }

       
        if (wolParts.length <= 0 && actiontaken !="DELETE") {
            return;
        }
    
        ah.toggleSaveMode();
        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        strDateStart = strDateStart + ' ' + hourmin;
        var partsConfirm = $("#PARTS_CONFIRMED").val();
        var actionFlag = "";
        if (partsConfirm === "Y") {
            actionFlag = 'CONFIRM'
        }
        postData = { CREATE_DATE: strDateStart, AM_PARTSREQUEST: wolParts, ACTION: actiontaken, STAFF_LOGON_SESSIONS: staffLogonSessions };
        //alert(JSON.stringify(postData));
        $.post(self.getApi() + ah.config.api.createMaterials, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        
        window.location.href = encodeURI(ah.config.url.modalClose);
    };
    self.saveInfo = function () {
        document.getElementById("ASSIGN_TO").disabled = false;
        var x = document.getElementById("frmInfo").elements.length;
        if (x < xElements) {
            self.alertMessage("Unable to proceed. Please refresh the screen deleting element from html is prohibited.");
            return;
        }

        $(ah.config.id.woStatus).prop(ah.config.attr.disabled, false);
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var setupDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        $("#REQ_ID").show();
        $("label[for='REQ_ID']").show();
        var wsId = $("#AM_INSPECTION_ID").val();
        
        if (setupDetails.REQ_TYPE != 'SR') {

            if ($("#ASSET_NO").val() == "") {
                $("#ASSET_NO+a").next("span").remove();
                $("#ASSET_NO+a").after("<span style='color:red;padding-left:5px'>* This field is required</span>");
                return;
            }
            if (setupDetails.WO_STATUS == "CL" && (setupDetails.ASSIGN_TO == null || setupDetails.ASSIGN_TO == "")) {
                self.alertMessage("Unable to proceed. Please select Assign To...");
                return;
            }
            if (setupDetails.WO_STATUS == "CL" && setupDetails.ASSIGN_TYPE == "COMPANY" && (setupDetails.ASSIGN_OUTSOURCE == "" || setupDetails.ASSIGN_OUTSOURCE == null)) {
                self.alertMessage("Unable to proceed. Please select Supplier.");
                return;
            }

            var checkreqDate = self.isDateFutre(moment(setupDetails.JOB_DUE_DATE, 'DD/MM/YYYY'), moment(setupDetails.REQ_DATE, 'DD/MM/YYYY'));
            if (checkreqDate) {
                self.alertMessage("Unable ro proceed. Due date cannot be less than Requested Date");
                return;
            }

        }
       

        setupDetails.ASSET_LOCATION = setupDetails.LOCATION;
        setupDetails.ASSET_COSTCENTER = setupDetails.COST_CENTER;
        setupDetails.ASSET_COSTCENTERCODE = setupDetails.ASSET_COSTCENTERCODE;
        
        
        var saveAction = $("#SAVEACTION").val();

        if (saveAction != 'SAVEINFO') {
            return;
        }
        var reqBy = setupDetails.REQ_BY.trim();
      
        if (reqBy == "" || reqBy == null) {
            self.alertMessage("Unable to continue please make sure that the Requestor is not empty. Entry with space only is not acceptable.");
            return;
        }

        var reqContact = setupDetails.REQUESTER_CONTACT_NO.trim();

        if (reqContact == "" || reqContact == null) {
            self.alertMessage("Unable to continue please make sure that the Requestor Contact No is not empty. Entry with space only is not acceptable.");
           return;
        }

       var problemDesc = setupDetails.PROBLEM.trim();

       if (problemDesc == "" || problemDesc == null) {
           self.alertMessage("Unable to continue please make sure that the problem is not empty. Entry with space only is not acceptable.");
           return;
       }

        if (setupDetails.RISK_INCLUSIONFACTOR === null || setupDetails.RISK_INCLUSIONFACTOR === "") {
            setupDetails.RISK_INCLUSIONFACTOR = 0;
        }

        if (setupDetails.JOB_DURATION === null || setupDetails.JOB_DURATION === "") {
            setupDetails.JOB_DURATION = 0;
        }
        if (setupDetails.JOB_DUE_DATE == "" || setupDetails.JOB_DUE_DATE == "null" || setupDetails.JOB_DUE_DATE == "00/00/0000" || setupDetails.JOB_DUE_DATE == "00-00-0000") {
            setupDetails.JOB_DUE_DATE = "01/01/1900";
        }


        if(setupDetails.AM_INSPECTION_ID === "" || setupDetails.AM_INSPECTION_ID === null){
            setupDetails.AM_INSPECTION_ID = 0;
        };
       
        var strTime = '';
        var apptDate = setupDetails.REQ_DATE;
        var appointmentDate = apptDate.substring(6, 10) + '-' + apptDate.substring(3, 5) + '-' + apptDate.substring(0, 2);
        var apptDateStr = appointmentDate;
        strTime = String(apptDate).split(' ');
        strTime = strTime[1].substring(0, 5);
        appointmentDate = appointmentDate + ' ' + strTime;
        setupDetails.REQ_DATE = appointmentDate;
        if (setupDetails.TICKET_ID === null || setupDetails.TICKET_ID === ""){
            setupDetails.TICKET_ID = 0;
        }
        if (setupDetails.PURCHASE_COST === null || setupDetails.PURCHASE_COST === "") {
            setupDetails.PURCHASE_COST = 0;
        }
        //(setupDetails.REQ_NO);
        if ((ah.CurrentMode == ah.config.mode.add || ah.CurrentMode == ah.config.mode.display) && (setupDetails.REQ_NO === null || setupDetails.REQ_NO === "")) {
            setupDetails.CREATED_BY = staffLogonSessions.STAFF_ID;
            var jDate = new Date();
            var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);

            setupDetails.CREATED_DATE = strDateStart + ' ' + hourmin;
            postData = { AM_WORK_ORDERS: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            ah.toggleSaveMode();
            //alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.createSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {
            var jDate = new Date();
            var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);
            
            var modifyBy = staffLogonSessions.STAFF_ID;
            var modifyDate = strDateStart + ' ' + hourmin;
            if (setupDetails.CLOSED_DATE !== null && setupDetails.CLOSED_DATE !== "") {
                var dateNow = self.getDateNow();
                var checkDate = self.isDateFutre(dateNow, moment(setupDetails.CLOSED_DATE, 'DD/MM/YYYY hh:mm:ss'));
                if (checkDate) {
                    self.alertMessage("Unable ro proceed. Close date cannot be future date.");
                    return;
                }

                //check the reqest Date
                var reqDate = $("#REQ_DATE").val();
                var checkreqDate = self.isDateFutre(moment(setupDetails.CLOSED_DATE, 'DD/MM/YYYY hh:mm:ss'), moment(reqDate, 'DD/MM/YYYY hh:mm:ss'));
                if (checkreqDate) {
                    self.alertMessage("Unable ro proceed. Close date cannot be less than Requested Date");
                    return;
                }
                var closeDateTime = setupDetails.CLOSED_DATE;
                var closeDate = setupDetails.CLOSED_DATE;
                closeDate = closeDate.substring(6, 10) + '-' + closeDate.substring(3, 5) + '-' + closeDate.substring(0, 2);
                strTime = String(closeDateTime).split(' ');
                strTime = strTime[1].substring(0, 5);
                setupDetails.CLOSED_DATE = closeDate + ' ' + strTime;
            }
           

            ah.toggleSaveMode();

           


            postData = { MODIFY_BY: modifyBy, MODIFY_DATE: modifyDate, AM_WORK_ORDERS: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
         //   alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.updateSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }

    };
    self.webApiCallbackStatus = function (jsonData) {
        //alert(JSON.stringify(jsonData));
        //alert(ah.CurrentMode);
        //console.log(jsonData);
        if (jsonData.ERROR) {
            if (jsonData.ERROR.ErrorText === "Work Sheet not found.") {
               
                $("#AM_INSPECTION_ID").val(null);
                alert(jsonData.ERROR.ErrorText);
            } else {
                alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            }
            
           // window.location.href = ah.config.url.homeIndex;

        } 
        else if (jsonData.LOV_LOOKUPSCOSTCENTER || jsonData.STAFFLISTWO || jsonData.AM_ASSET_LOVLIST || jsonData.STAFF_INFO || jsonData.AM_SUPPLIERLIST || jsonData.AUTOREFRESHPARTS || jsonData.WORK_ORDER_QUICKVIEW_V ||
            jsonData.PARTS_STOCKONHAND_VI || jsonData.WARRANTYLIST || jsonData.AM_ASSET_WARRANTY || jsonData.PARTS_STOCKONHAND_V || jsonData.AM_PARTSREQUEST_LIST || jsonData.AM_WORKORDER_LABOURLIST ||
            jsonData.AM_SPAREPARTSREQUEST || jsonData.WORD_ORDER_LIST || jsonData.AM_WORK_ORDERS_STATUS_LOG || jsonData.SCANDOCS_CATEGORYIMAGELIST || jsonData.SCANDOCS || jsonData.AM_WORKORDER_LABOUR ||
            jsonData.AM_CLIENT_INFO || jsonData.AM_PARTSREQUEST || jsonData.AM_WORK_ORDER_REFNOTES || jsonData.AM_ASSET_NOTES || jsonData.AM_PROCEDURES || jsonData.AM_HELPDESKLIST || jsonData.AM_INSPECTION ||
            jsonData.STAFFLIST || jsonData.AM_ASSET_CONTRACTSERVICES || jsonData.LOV_LOOKUPS || jsonData.AM_WORK_ORDERS || jsonData.AM_ASSET_DETAILS || jsonData.STAFF || jsonData.SUCCESS || jsonData.AM_ASSET_PMSCHEDULES_LIST) {

            if (jsonData.STAFFLISTWO) {
                if (srStaffList.length == 0) {
                    srStaffList = jsonData.STAFFLISTWO;

                    $.each(jsonData.STAFFLISTWO, function (item) {

                        $(ah.config.id.filterAssignToList)
                            .append($("<option>")
                                .attr("value", jsonData.STAFFLISTWO[item].STAFF_FULLNAME)
                                .attr("id", jsonData.STAFFLISTWO[item].ASSIGN_TO));
                    });

                    $.each(jsonData.COST_CENTER_ADV, function (item) {

                        $(ah.config.id.filterCostCenterList)
                            .append($("<option>")
                                .attr("value", jsonData.COST_CENTER_ADV[item].DESCRIPTION.trim())
                                .attr("id", jsonData.COST_CENTER_ADV[item].LOV_LOOKUP_ID));
                    });

                    $.each(jsonData.SUPPLIERLIST_ADV, function (item) {

                        $(ah.config.id.filterSupplierList)
                            .append($("<option>")
                                .attr("value", jsonData.SUPPLIERLIST_ADV[item].DESCRIPTION.trim())
                                .attr("id", jsonData.SUPPLIERLIST_ADV[item].SUPPLIER_ID));
                    });
                }
            }
            //else if (ah.CurrentMode == ah.config.mode.searchResult) {
            //    if (self.isPrint == 2)
            //        self.printSticker(jsonData);
            //    else
            //        self.populateBulkPrint(jsonData);
            //        //self.printSummary(jsonData);
            //}

            if (ah.CurrentMode == ah.config.mode.save) {
               // alert(JSON.stringify(jsonData));
                if (jsonData.AM_WORK_ORDERS) {
                    $("#ASSET_NO+a").next("span").remove();
                    sessionStorage.setItem(ah.config.skey.woDetails, JSON.stringify(jsonData.AM_WORK_ORDERS));
                }
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();

                if (jsonData.SUCCESS) {
  
                    self.showDetails(false, jsonData);
                } else if (jsonData.SCANDOCS) {
                    ah.toggleDisplayMode();
                    srDataArray = jsonData.SCANDOCS_CATEGORYIMAGELIST;
                    self.UploadImage(jsonData);
                    
                } else if (jsonData.AUTOREFRESHPARTS) {
                    ah.toggleDisplayMode();
                    srDataArray = jsonData.AM_PARTSREQUEST_LIST;                   
                    self.searchPartsMaterialOnhandResult('LOAD');
                }else if (jsonData.AM_PARTSREQUEST) {
                    ah.toggleDisplayMode();
                }
                else {
                    sessionStorage.setItem(ah.config.skey.assetItems, JSON.stringify(jsonData.AM_ASSET_DETAILS));
                   
                    clientInfo = jsonData.AM_CLIENT_INFO;
                    self.showDetails(true, jsonData);
                }
            }

            else if (ah.CurrentMode == ah.config.mode.display) {
                if (jsonData.AM_ASSET_NOTES) {
                    self.searchResultNotes(jsonData);
                } else if (jsonData.AM_WORK_ORDER_REFNOTES) {
                    self.searchResultWONotes(jsonData);

                } else if (jsonData.AM_ASSET_DETAILS) {
                    self.populateAssetInfo(jsonData);

                } else if (jsonData.AM_PARTSREQUEST) {
                    //alert(jsonData.AM_CLIENT_INFO.CLIENT_LOGO);
                    document.getElementById("CLIENT_LOGO").src = jsonData.AM_CLIENT_INFO.CLIENT_LOGO;
                    if (self.isPrint == 2)
                        self.printSticker(jsonData);
                    if (self.isPrint == 1)
                        self.searchWoMaterialResult(jsonData);
                    if (self.isPrint == 3)
                        self.OutOfOrderSticker(jsonData);
                    if (self.isPrint == 4)
                        self.InitialAccpSticker(jsonData);

                } else if (jsonData.AM_PARTSREQUEST_LIST) {
                    srDataArray = jsonData.AM_PARTSREQUEST_LIST;
                    srOnhandDataArray = jsonData.PARTS_STOCKONHAND_V;
                    self.searchPartsMaterialOnhandResult('LOAD');

                } else if (jsonData.PARTS_STOCKONHAND_VI){
                    self.searchSparePartsOnhandResult(jsonData);
                }
            }
            else if (ah.CurrentMode == ah.config.mode.searchResult) {
                if (jsonData.WORK_ORDER_QUICKVIEW_V){
                    self.showQuickView(jsonData);
                }else  if (jsonData.AM_PARTSREQUEST) {
                   
                    self.searchPartsOnhandResult(jsonData);
                } else if (jsonData.WORD_ORDER_LIST) {
                    document.getElementById("CLIENT_LOGO").src = jsonData.AM_CLIENT_INFO.CLIENT_LOGO;
                    self.populateBulkPrint(jsonData);
                }
            }
            else if (ah.CurrentMode == ah.config.mode.add) {
                if (jsonData.AM_INSPECTION) {
                   // $("#AM_INSPECTION_DESC").val(jsonData.AM_INSPECTION.AM_INSPECTION_ID + '-' + jsonData.AM_INSPECTION.INSPECTION_TYPE);
                    $("#AM_INSPECTION_ID").val(jsonData.AM_INSPECTION.AM_INSPECTION_ID);
                } else if (jsonData.STAFFLIST){
                    staffListLOV = jsonData.STAFFLIST;
                    self.searchResultStaffListLov("");
                } else if (jsonData.AM_SUPPLIERLIST) {
                    supplierListLOV = jsonData.AM_SUPPLIERLIST;
                    var searchStr = $(ah.config.id.lookUptxtGlobalSearchOth).val();
                    self.searchResultSupplierListLov(searchStr);
                } else if (jsonData.AM_ASSET_LOVLIST) {
                    assetList = jsonData.AM_ASSET_LOVLIST;
                    self.searchItemsResult("", 0);
                } else if (jsonData.AM_HELPDESKLIST) {
                    self.searchResultHelpDesk(jsonData);
                } else if (jsonData.AM_ASSET_DETAILS){
                        self.showAssetDetails(jsonData);
                } else if (jsonData.LOV_LOOKUPSCOSTCENTER){
                    var searchStr = $(ah.config.id.lookUptxtGlobalSearchOth).val();
                    costCenterListLOV = jsonData.LOV_LOOKUPSCOSTCENTER;
                    self.searchResultCostCenterListLov(searchStr);
                }
            }
            else if (ah.CurrentMode == ah.config.mode.edit) {
                if (jsonData.AM_INSPECTION) {
                   // $("#AM_INSPECTION_DESC").val(jsonData.AM_INSPECTION.AM_INSPECTION_ID + '-' + jsonData.AM_INSPECTION.INSPECTION_TYPE);
                    $("#AM_INSPECTION_ID").val(jsonData.AM_INSPECTION.AM_INSPECTION_ID);
                } else if (jsonData.STAFFLIST) {
                    staffListLOV = jsonData.STAFFLIST;
                    self.searchResultStaffListLov("");
                }else if (jsonData.AM_ASSET_LOVLIST) {
                    assetList = jsonData.AM_ASSET_LOVLIST;
                    self.searchItemsResult("", 0);
                } else if (jsonData.AM_SUPPLIERLIST) {
                    supplierListLOV = jsonData.AM_SUPPLIERLIST;
                    var searchStr = $(ah.config.id.lookUptxtGlobalSearchOth).val();
                    self.searchResultSupplierListLov(searchStr);
                } else if (jsonData.AM_HELPDESKLIST) {
                    self.searchResultHelpDesk(jsonData);
                }
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                //sessionStorage.setItem(ah.config.skey.woDetails, JSON.stringify(jsonData.AM_WORK_ORDERS));
                //sessionStorage.setItem(ah.config.skey.assetItems, JSON.stringify(jsonData.AM_ASSET_DETAILS));
                
                srDataArray = jsonData.SCANDOCS_CATEGORYIMAGELIST;
                clientInfo = jsonData.AM_CLIENT_INFO;
                self.showDetails(true, jsonData);
            }

            else if (ah.CurrentMode == ah.config.mode.search) {
              
                if (jsonData.AM_ASSET_DETAILS) {
                   
                    assetList = jsonData.AM_ASSET_DETAILS;
                    //sessionStorage.setItem(ah.config.skey.assetList, JSON.stringify(jsonData.AM_ASSET_DETAILS));
					$("#PaginGloadNumber").text("1");
					$("#PaginGpageCount").val("1");
                    $("#PaginGpageNum").val(0);
                   
                    self.searchItemsResult("", 0);
				} else if (jsonData.AM_ASSET_CONTRACTSERVICES) {

					self.searchContractResult(jsonData);
                } else if (jsonData.AM_WORK_ORDERS_STATUS_LOG) {
                    //alert(JSON.stringify(jsonData));
                    self.searchResultWOStatus(jsonData);
                } else if (jsonData.AM_HELPDESKLIST) {
					self.searchResultHelpDesk(jsonData);
                } else if (jsonData.AM_WORK_ORDERS) {
                    if (jsonData.AM_CLIENT_INFO) {
                        clientInfo = jsonData.AM_CLIENT_INFO;
                        document.getElementById("CLIENT_LOGO_SUM").src = jsonData.AM_CLIENT_INFO.CLIENT_LOGO;
                    }

                    workORderList = jsonData.AM_WORK_ORDERS;
                    sort = "SORTDATEDC";
                    $("#LASTLOADPAGE").val(0);
                    var sortby = $("#SORTPAGE").val();
                    self.searchResult(0, sortby);
                }               
            }

            else if (ah.CurrentMode == ah.config.mode.idle) {
               // alert(JSON.stringify(jsonData.LOV_LOOKUPS));
               
                var serviceDept = jsonData.LOV_LOOKUPS;
                serviceDept = serviceDept.filter(function (item) { return item.CATEGORY === 'AIMS_SERVICEDEPARTMENT' });
               
                $.each(serviceDept, function (item) {

                    $(ah.config.id.servDept)
                        .append($("<option></option>")
                        .attr("value", serviceDept[item].LOV_LOOKUP_ID)
                        .text(serviceDept[item].DESCRIPTION));
                });
				var aimsJobType = jsonData.LOV_LOOKUPS;
				aimsJobType = aimsJobType.filter(function (item) { return item.CATEGORY === 'AM_JOBTYPE' });
				$.each(aimsJobType, function (item) {					
					$(ah.config.id.jobType)
						.append($("<option></option>")
							.attr("value", aimsJobType[item].LOV_LOOKUP_ID)
							.text(aimsJobType[item].DESCRIPTION));
                });
                $.each(aimsJobType, function (item) {
                    $(ah.config.id.filterJobType)
                        .append($("<option></option>")
                            .attr("value", aimsJobType[item].LOV_LOOKUP_ID)
                            .text(aimsJobType[item].DESCRIPTION));
                });

                var aimsPriority = jsonData.LOV_LOOKUPS;
                aimsPriority = aimsPriority.filter(function (item) { return item.CATEGORY === 'AIMS_PRIORITY' });
                srPriorityType = aimsPriority;
                $.each(aimsPriority, function (item) {

                    $(ah.config.id.woPriority)
                        .append($("<option></option>")
                        .attr("value", aimsPriority[item].LOV_LOOKUP_ID)
                            .text(aimsPriority[item].DESCRIPTION));

                    $("#filterPriority")
                        .append($("<option></option>")
                            .attr("value", aimsPriority[item].LOV_LOOKUP_ID)
                            .text(aimsPriority[item].DESCRIPTION));
                });

				var aimsProblemType = jsonData.LOV_LOOKUPS;
				//alert(JSON.stringify(aimsProblemType));
				aimsProblemType = aimsProblemType.filter(function (item) { return item.CATEGORY === 'AIMS_PROBLEMTYPE' });
                srProblemType = aimsProblemType;
                
                $.each(aimsProblemType, function (item) {

                    $(ah.config.id.woProblemType)
                        .append($("<option></option>")
                        .attr("value", aimsProblemType[item].LOV_LOOKUP_ID)
                            .text(aimsProblemType[item].DESCRIPTION));
                    $("#filterProblemType")
                        .append($("<option></option>")
                            .attr("value", aimsProblemType[item].LOV_LOOKUP_ID)
                            .text(aimsProblemType[item].DESCRIPTION));
                });
                var aimsSpecialty = jsonData.LOV_LOOKUPS;
                aimsSpecialty = aimsSpecialty.filter(function (item) { return item.CATEGORY === 'AIMS_SPECIALTY' });

                $.each(aimsSpecialty, function (item) {

                    $(ah.config.id.woSpecialty)
                        .append($("<option></option>")
                        .attr("value", aimsSpecialty[item].LOV_LOOKUP_ID)
                        .text(aimsSpecialty[item].DESCRIPTION));
                });

                assetNoID = ah.getParameterValueByName(ah.config.fld.assetNoInfo);
                assetDescription = ah.getParameterValueByName(ah.config.fld.assetDescInfo);
                if (assetNoID !== null && assetNoID !== "") {
                    $("#infoID").text(assetNoID + "-" + assetDescription);
                    $("#assetInfobtn").show();
                    $("#assetInfoDet").hide();
                    self.searchNow();
                }
                var onHold = ah.getParameterValueByName(ah.config.fld.fromMenuOnhold);
                
                if (onHold == "onhold") {
                    self.searchFilter.status="HA";
                    $("#filterStatus").val("HA");
                    self.searchNow();
                    return;

                }
                var reqWoNO = ah.getParameterValueByName(ah.config.fld.reqNo);
              
                if (reqWoNO !== null && reqWoNO !== "") {
                    self.readWOId(reqWoNO);
				} 
				var newWO = ah.getParameterValueByName(ah.config.fld.newWorkOrder);
				
				if (newWO === 'NEW') {
					
					self.createNew();
                }

                if (self.validatePrivileges('6') === 0) {
                    $(ah.config.cls.liAddNew).hide();
                };
                $("#filterStatus").val('PSOP');
                self.searchFilter.status = 'PSOP';
            }

        }
        else {

            alert(systemText.errorTwo);
            //window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};