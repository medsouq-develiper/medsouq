﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            detailItem: '.detailItem'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            tbody: 'tbody',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmInfo: '#frmInfo',
            clinicdoctorID: '#CLINIC_DOCTOR_ID',
            clinicID: '#CLINIC_ID',
            staffID: '#STAFF_ID',
            serviceID: '#SERVICE_ID',
            successMessage: '#successMessage',
            categoryId: '#CATEGORY',
            typeId: '#TYPE_ID',
            uomId: '#UOM_ID',
            modelsResultList: '#modelsResultList',
            partModelsId: '#partModelsId',
            partAddModels: '#partAddModels',
            btnRemove: '#btnRemove',
            lookUptxtGlobalSearchOth: '#lookUptxtGlobalSearchOth',
            searchResultLookUp: '#searchResultLookUp',

            modalCustomerDiscountForm: '#modalCustomerDiscountForm',
            modalCurrentCost: '#modalCurrentCost',
            modalDiscountType: '#modalDiscountType',
            modalDiscountLabel: '#modalDiscountLabel',
            modalDiscount: '#modalDiscount',
            modalValidityDateFrom: '#modalValidityDateFrom',
            modalValidityDateTo: '#modalValidityDateTo',
            modalStatus: '#modalStatus',
            idParts: '#ID_PARTS'
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'searchResult',
            infoDetails: 'AM_SUPPLIER',
            infoSupplierDetails: 'PARTNO_BYPARTID_V'
        },
        url: {
            aimsHomepage: '/Menuapps/aimsIndex',
            homeIndex: '/Home/Index',
            aimsPartSuppier: '/aims/amPartSupplierDetails',
            modalLookUp: '#openModalLookup',
            modalCustomerDiscount: '#openModalCustomerDiscount',
            closeModal: '#'
        },
        fld: {
            fldpartId: 'PARTID'
        },

        api: {

            readSetup: '/AIMS/ReadAmPart',
            searchAll: '/AIMS/SearchAmParts',
            createSetup: '/AIMS/CreateNewAmPart',
            searchLov: '/Setup/SearchInventoryLOV',
            updateSetup: '/AIMS/UpdateAmPart',
            searchModel: '/AIMS/SearchModel',
            createOrUpdateCustomerDiscount: '/AIMS/CreateOrUpdateCustomerDiscount',
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",

        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };
    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.id.partAddModels + ',' + thisApp.config.id.partModelsId + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("ID_PARTS").disabled = true;


    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();


    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave).hide();
    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.id.btnRemove + ',' + thisApp.config.id.partAddModels + ',' + thisApp.config.id.partModelsId + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

        document.getElementById("ID_PARTS").disabled = true;
    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
        document.getElementById("ID_PARTS").disabled = true;
    };

    thisApp.toggleAddExistMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;


        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.searhButton).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.statusText +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.iconDet + ',' + thisApp.config.cls.divProgress
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.id.partModelsId + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.id.btnRemove + ',' + thisApp.config.id.partAddModels + ',' + thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);
        document.getElementById("ID_PARTS").disabled = true;

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {

        $(form).find(':input:disabled').removeAttr('disabled');
        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });
        return json;

    };

    thisApp.getAge = function (birthDate) {

        var seconds = Math.abs(new Date() - birthDate) / 1000;

        var numyears = Math.floor(seconds / 31536000);
        var numdays = Math.floor((seconds % 31536000) / 86400);
        var nummonths = numdays / 30;

        return numyears + " y " + Math.round(nummonths) + " m ";
    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name),
                type = $el.attr('type');

            switch (type) {
                case 'checkbox':

                    //if (val == 'Y' && val != null && val != "") {
                    //    alert(val);
                    //    $el.setAttr('checked', 'checked')
                    //} else {

                    //    $el.removeAttr('checked');
                    //};
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var ampartsSetupViewModel = function (systemText) {

    var self = this;

    var ah = new appHelper(systemText);
    var srpartModels = ko.observableArray([]);
    var activeCustomerDiscount = {};
    self.customerDiscounts = ko.observableArray([]);
    self.modelsList = ko.observableArray("");
    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.toggleReadMode();

        ah.initializeLayout();
        postData = { LOV: "'PCATEGORY','PGROUP','PROD_UOM','AIMS_MSTYPE','AIMS_MSUOM'", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

        $(ah.config.id.typeId).on('change', function (e) {
            if (this.value == 'AIMS_MSTYPE-2') {
                $(ah.config.id.modelsResultList).show();
            } else {
                $(ah.config.id.modelsResultList).hide();
            }
        });
    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.createNew = function () {

        ah.toggleAddMode();

    };

    self.modifySetup = function () {

        ah.toggleEditMode();

    };

    self.aimsPartSupplier = function () {
        var partId = $("#ID_PARTS").val();
        window.location.href = encodeURI(ah.config.url.aimsPartSuppier + "?PARTID=" + partId);
    };

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.saveAllChanges = function () {

        $(ah.config.id.saveInfo).trigger('click');

    };

    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            $("#DESCRIPTION").next("span").remove();
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            var partId = $("#ID_PARTS").val();
            self.readPartIdref(partId);
            self.showDetails(true);
        }

    };

    self.showDetails = function (isNotUpdate) {

        if (isNotUpdate) {

            var infoDetails = sessionStorage.getItem(ah.config.skey.infoDetails);

            var jsetupDetails = JSON.parse(infoDetails);

            ah.ResetControls();
            ah.LoadJSON(jsetupDetails);

            if ($(ah.config.id.typeId).val() == 'AIMS_MSTYPE-2') {
                $(ah.config.id.modelsResultList).show();
            } else {
                $(ah.config.id.modelsResultList).hide();
            }

            var infoSupplierDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.infoSupplierDetails));

            if (infoSupplierDetails != null) {
                $("#PART_NUMBERS").val(infoSupplierDetails.PART_NUMBERS);
            }
            var srmodels = srpartModels;
            var htmlstr = '';
            $(ah.config.id.modelsResultList + ' ' + ah.config.tag.tbody).html('');
            self.modelsList.splice(0, 5000);
            if (srmodels.length > 0) {
                for (var o in srmodels) {
                    self.modelsList.push({
                        MODEL_ID: srmodels[o].MODEL_ID,
                        MODEL: srmodels[o].MODEL,
                        ID_PARTS: srmodels[o].ID_PARTS,
                        MODEL_NAME: srmodels[o].MODEL_NAME
                    });
                }

            }

        }
        ah.toggleDisplayMode();
    };

    self.removeItem = function (srmodels) {
        self.modelsList.remove(srmodels);
    }

    self.searchResult = function (jsonData) {


        var sr = jsonData.AM_PARTS;

        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {
                if (sr[o].DESCRIPTION !== null && sr[o].DESCRIPTION !== "") {
                    if (sr[o].DESCRIPTION.length > 100)
                        sr[o].DESCRIPTION = sr[o].DESCRIPTION.substring(0, 99) + "...";
                }

                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].ID_PARTS, sr[o].ID_PARTS, sr[o].DESCRIPTION, '');
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.supplierpartNo, sr[o].PART_NUMBERS);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.category, sr[o].CATEGORY);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.ancillary, sr[o].ANCILLARY);

                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.detailItem).bind('click', self.readSetupID);
        ah.displaySearchResult();

    };
    self.searchResultLookupModels = function (jsonData) {
        var sr = jsonData.AM_MANUFACTURER_MODEL;
        //alert(JSON.stringify(sr));
        $("#lookUpnoresult").hide();
        $("#searchingLOV").hide();
        $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');
        if (sr.length > 0) {
            var htmlstr = "";
            for (var o in sr) {

                //if (sr[o].DESCRIPTION.length > 50)
                //    sr[o].DESCRIPTION = sr[o].DESCRIPTION.substring(0, 49) + "...";

                htmlstr += '<li>';
                htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].MODEL, sr[o].MODEL_NAME, "");
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.manufacurerDesc, sr[o].DESCRIPTION_MFTR);


                htmlstr += '</span>';
                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).append(htmlstr);
            $(ah.config.cls.detailItem).bind('click', self.assignModel);
        } else {
            $("#lookUpnoresult").show();
        }




    };
    self.assignModel = function () {

        var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfoId).toString());
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        var createdDate = strDateStart + ' ' + hourmin;


        self.modelsList.push({
            MODEL_ID: infoID.MODEL_ID,
            MODEL: infoID.MODEL,
            ID_PARTS: infoID.ID_PARTS,
            MODEL_NAME: infoID.MODEL_NAME,
            ID: 0,
            CREATED_BY: staffLogonSessions.STAFF_ID,
            CREATED_DATE: createdDate
        });

        return;
    };
    self.readSetupID = function () {

        var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { ID_PARTS: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.readPartIdref = function (refId) {


        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { ID_PARTS: refId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.readPartID = function () {

        var partId = ah.getParameterValueByName(ah.config.fld.fldpartId);
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { ID_PARTS: partId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();

        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.addModels = function () {
        $("#searchingLOV").hide();
        $("#lookUpnoresult").hide();
        $(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');
        window.location.href = ah.config.url.modalLookUp;
    };
    self.addCustomerDiscount = function () {
        if (parseFloat($('#PART_COST').val()) > 0) {
            $(ah.config.id.modalCurrentCost).val($('#PART_COST').val());
            self.newCustomerDiscount();

            window.location.href = ah.config.url.modalCustomerDiscount;
        } else {
            alert('Current Cost should greater than 0');
        }
    };
    self.newCustomerDiscount = function () {
        $(ah.config.id.modalCustomerDiscountForm + ' input, ' + ah.config.id.modalCustomerDiscountForm + ' select').prop(ah.config.attr.disabled, false);
        $(ah.config.id.modalCurrentCost).prop(ah.config.attr.disabled, true);

        activeCustomerDiscount = {};
        $(ah.config.id.modalDiscountType).val('Percent').change();
        $(ah.config.id.modalDiscount).val('');
        $(ah.config.id.modalValidityDateFrom).val('');
        $(ah.config.id.modalValidityDateTo).val('');
        $(ah.config.id.modalStatus).val('Active').change();
    };
    self.editCustomerDiscount = function () {
        $(ah.config.id.modalCustomerDiscountForm + ' input, ' + ah.config.id.modalCustomerDiscountForm + ' select').prop(ah.config.attr.disabled, false);
        $(ah.config.id.modalCurrentCost).prop(ah.config.attr.disabled, true);

        activeCustomerDiscount = this;
        $(ah.config.id.modalDiscountType).val(this.discount_type).change();
        $(ah.config.id.modalDiscount).val(this.discount);
        $(ah.config.id.modalValidityDateFrom).val(this.validity_date_from);
        $(ah.config.id.modalValidityDateTo).val(this.validity_date_to);
        $(ah.config.id.modalStatus).val(this.status).change();
    };
    self.calculatePercentage = function () {
        var modalCurrentCost = $(ah.config.id.modalCurrentCost);
        var modalDiscountType = $(ah.config.id.modalDiscountType);
        var modalDiscount = $(ah.config.id.modalDiscount);
        if (modalDiscountType.val() == 'Percent' && modalCurrentCost.val() && modalDiscount.val() && parseFloat(modalDiscount.val()) > 100) {
            alert('The maximum percent of Percentage Discount is 100.');
            modalDiscount.val('');
        }
        else if (modalDiscountType.val() == 'Amount' && modalCurrentCost.val() && modalDiscount.val() && parseFloat(modalCurrentCost.val()) < parseFloat(modalDiscount.val())) {
            alert('Amount Discount should greater than Current Cost.');
            modalDiscount.val('');
        }
    };
    self.checkDiscountType = function () {
        var modalDiscountType = $(ah.config.id.modalDiscountType);
        var modalDiscount = $(ah.config.id.modalDiscount);
        if (modalDiscountType.val() == 'Percent') {
            $(ah.config.id.modalDiscountLabel).text('Discount (%):');
            modalDiscount.val('');
        }
        else {
            $(ah.config.id.modalDiscountLabel).text('Discount:');
            modalDiscount.val('');
        }
    };
    self.dateClick = function () {
        $("#dateClickAction").val("ACTIVE");
    };
    self.closeModalCustomerDiscount = function () {
        window.location.href = ah.config.url.closeModal;
    };
    self.saveModalCustomerDiscount = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var modalCustomerDiscountForm = ah.ConvertFormToJSON($(ah.config.id.modalCustomerDiscountForm)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var am_prod_customer_discount = {
            am_part_id: $('#ID_PARTS').val(),
            discount_type: modalCustomerDiscountForm.modalDiscountType,
            discount: modalCustomerDiscountForm.modalDiscount,
            validity_date_from: moment(modalCustomerDiscountForm.modalValidityDateFrom, 'DD/MM/YYYY').format(),
            validity_date_to: moment(modalCustomerDiscountForm.modalValidityDateTo, 'DD/MM/YYYY').format(),
            status: modalCustomerDiscountForm.modalStatus,
        };
        if (activeCustomerDiscount.id) {
            am_prod_customer_discount.id = activeCustomerDiscount.id;
        }

        postData = { am_prod_customer_discount: am_prod_customer_discount, STAFF_LOGON_SESSIONS: staffLogonSessions };
        ah.toggleSaveMode();
        $.post(self.getApi() + ah.config.api.createOrUpdateCustomerDiscount, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.lookUppageLoader = function () {
        $("#searchingLOV").show();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;
        var searchModel = $(ah.config.id.lookUptxtGlobalSearchOth).val().toString()
        postData = { SEARCH: searchModel, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchModel, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    }
    self.saveInfo = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var setupDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        setupDetails.CREATED_BY = staffLogonSessions.STAFF_ID;
        if ($("#STOCK_ITEM_FLAG").is(":checked") === true) { setupDetails.STOCK_ITEM_FLAG = 'Y' };


        if (ah.CurrentMode == ah.config.mode.add) {
            var jDate = new Date();
            var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
            function time_format(d) {
                hours = format_two_digits(d.getHours());
                minutes = format_two_digits(d.getMinutes());
                return hours + ":" + minutes;
            }
            function format_two_digits(n) {
                return n < 10 ? '0' + n : n;
            }
            var hourmin = time_format(jDate);

            setupDetails.CREATED_DATE = strDateStart + ' ' + hourmin;
            postData = { AM_PARTS: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            ah.toggleSaveMode();
            // alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.createSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {

            var jsonObj = ko.observableArray([]);

            jsonObj = ko.utils.stringifyJson(self.modelsList);

            var partModelsInfo = JSON.parse(jsonObj);


            ah.toggleSaveMode();
            postData = { AM_PARTS_MODELDETAILS: partModelsInfo, AM_PARTS: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
            //alert(JSON.stringify(postData));
            $.post(self.getApi() + ah.config.api.updateSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }

    };

    self.webApiCallbackStatus = function (jsonData) {
        // alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {
            if (jsonData.ERROR.ErrorText.includes("Duplicate")) {
                ah.toggleAddExistMode();
                $("#DESCRIPTION").next("span").remove();
                $("#DESCRIPTION").after("<span style='color:red;padding-left:5px;'>* Description already in use</span>");

            }
            else {
                alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
                // window.location.href = ah.config.url.homeIndex;
            }

        }
        else if (jsonData.AM_MANUFACTURER_MODEL || jsonData.AM_PARTS_MODELDETAILS || jsonData.PARTNO_BYPARTID_V || jsonData.AM_PARTS || jsonData.STAFF || jsonData.LOV_LOOKUPS || jsonData.SUCCESS) {
            $("#DESCRIPTION").next("span").remove();
            if (ah.CurrentMode == ah.config.mode.save) {
                sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_PARTS));
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();

                if (jsonData.SUCCESS) {
                    self.showDetails(false);
                }
                else {
                    self.showDetails(true);
                }
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                srpartModels = ko.observableArray([]);
                sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_PARTS));
                sessionStorage.setItem(ah.config.skey.infoSupplierDetails, JSON.stringify(jsonData.PARTNO_BYPARTID_V));
                srpartModels = jsonData.AM_PARTS_MODELDETAILS;
                self.customerDiscounts.splice(0, self.customerDiscounts().length);
                if (jsonData.AM_PROD_CUSTOMER_DISCOUNT && jsonData.AM_PROD_CUSTOMER_DISCOUNT.length) {
                    jsonData.AM_PROD_CUSTOMER_DISCOUNT.forEach(function (item) {
                        item.validity_date_from = moment(item.validity_date_from).format('DD/MM/YYYY');
                        item.validity_date_to = moment(item.validity_date_to).format('DD/MM/YYYY');
                        self.customerDiscounts.push(item);
                    });
                }
                self.showDetails(true);
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
                self.searchResult(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.edit) {
                self.searchResultLookupModels(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {
                var category = jsonData.LOV_LOOKUPS;
                category = category.filter(function (item) { return item.CATEGORY === 'PCATEGORY' });
                $.each(category, function (item) {
                    $(ah.config.id.categoryId)
                        .append($("<option></option>")
                            .attr("value", category[item].LOV_LOOKUP_ID)
                            .text(category[item].DESCRIPTION));
                });

                type = jsonData.LOV_LOOKUPS.filter(function (item) { return item.CATEGORY === 'AIMS_MSTYPE' });
                $.each(type, function (item) {
                    $(ah.config.id.typeId)
                        .append($("<option></option>")
                            .attr("value", type[item].LOV_LOOKUP_ID)
                            .text(type[item].DESCRIPTION));
                });

                uom = jsonData.LOV_LOOKUPS.filter(function (item) { return item.CATEGORY === 'AIMS_MSUOM' });
                $.each(uom, function (item) {
                    $(ah.config.id.uomId)
                        .append($("<option></option>")
                            .attr("value", uom[item].LOV_LOOKUP_ID)
                            .text(uom[item].DESCRIPTION));
                });

                var partId = ah.getParameterValueByName(ah.config.fld.fldpartId);

                if (partId !== null && partId !== "") {
                    self.readPartID();
                }
            }

        }
        else if (jsonData.am_prod_customer_discount) {
            jsonData.am_prod_customer_discount.validity_date_from = moment(jsonData.am_prod_customer_discount.validity_date_from).format('DD/MM/YYYY');
            jsonData.am_prod_customer_discount.validity_date_to = moment(jsonData.am_prod_customer_discount.validity_date_to).format('DD/MM/YYYY');

            if (jsonData.am_prod_customer_discount.status == 'Active') {
                var found = self.customerDiscounts().find(function (item) { return item.status == 'Active' && item.id != jsonData.am_prod_customer_discount.id });
                if (found) {
                    var newItem = {};
                    Object.keys(found).forEach(function (key) {
                        newItem[key] = found[key];
                    });
                    newItem.status = 'Inactive';
                    self.customerDiscounts.replace(found, newItem);
                }
            }

            var found = self.customerDiscounts().find(function (item) { return item.id == jsonData.am_prod_customer_discount.id });
            if (found) self.customerDiscounts.replace(found, jsonData.am_prod_customer_discount);
            else self.customerDiscounts.push(jsonData.am_prod_customer_discount);

            $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
            ah.showSavingStatusSuccess();
            self.showDetails(false);
            window.location.href = ah.config.url.closeModal;
        }
        else {

            alert(systemText.errorTwo);
            //  window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};