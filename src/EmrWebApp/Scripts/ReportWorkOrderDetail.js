﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            divProgress: '.divProgressScreen',
            toolBoxRight: '.toolbox-right',
            detailItem: '.detailItem',
            liPrint: '.liPrint'
        },
        tag: {

            textArea: 'textarea',
            select: 'select',
            tbody: 'tbody',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            search: 2,
            searchResult: 3,
            print: 8
        },
        tagId: {

        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',

            WorkOrderSearchResultCMTable: '#WorkOrderSearchResultCMTable',

            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            printSummary: '#printSummary',
            printSearchResultList: '#printSearchResultList',

            WorkOrderSearchResultList: '#WorkOrderSearchResultList',

            loadingBackground: '#loadingBackground',
            constCenterStrList: '#constCenterStrList',
            supplierStrList: '#supplierStrList',
            MANUFACTURERStrList: '#MANUFACTURERStrList',
            ProblemTypeStrList:'#ProblemTypeStrList',
            ReportSubTitle:'#ReportSubTitle'

        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            datainfoId: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS'


        },
        url: {
            modalFilter: '#openModalFilter',
            modalClose: '#',
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex'
        },
        api: {
            //identify the api controller you want to get you data
            readClientInfo: '/Setup/ReadClientInfo',
            searchLov: '/WorkOrderDetailRpt/SearchNewLOVList',
            searchCMWorkOrder: '/WorkOrderDetailRpt/ExampleSearchWorkOrderRpt'
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };
    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liPrint + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };
    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;
        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();
        $(thisApp.config.cls.contentSearch).show();

    };
    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };
    thisApp.displaySearchResult = function () {
        thisApp.CurrentMode = thisApp.config.mode.searchResult;
        $(thisApp.config.cls.contentSearch).show();
    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var viewTemplateViewModel = function (systemText) {
    var self = this;
    var ah = new appHelper(systemText);
    self.srDataArray = ko.observableArray([]);

    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.initializeLayout();
        var today = moment(new Date()).format("DD/MM/YYYY");

        $('#requestFromDate').val('');
        $('#requestStartDate').val(today);
       
        self.showLoadingBackground();

        $.when(self.getPageData()).done(function () {
            $(ah.config.id.loadingBackground).removeClass('initializing').addClass('done');
            $('.dark-bg, ' + ah.config.id.loadingBackground).css({
                'top': '90px',
                'z-index': '0'
            });
        });
    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api;
    };

    self.export = function (srtoexportdata, cycle) {
        $("#dvjson").excelexportjs({
            containerid: "dvjson"
            , datatype: 'json'
            , dataset: srtoexportdata
            , columns: getColumns(srtoexportdata)
            , filename: "WorkOrderDetail_" + cycle
        });
    };

    self.exportToExcel = function () {
        //alert(JSON.stringify(self.srDataArray));
        var srarrayOrig = self.srDataArray;
        var srarraylen = srarrayOrig.length;
        var lrow = 6500;
        var cycle = srarraylen / lrow;
        var i;
        var frow = 1;
        //Rename column before export
        var srarrayNew = srarrayOrig.map(rec => {
            return {
                'Work Order No': rec.req_no,
                'Asset No': rec.asset_no,
                'Asset Description': rec.Description,
                'Model No/Name': rec.model_no,
                'Request Date': rec.ReqDate,
                'Due Date': rec.JobDueDate,
                'Cost Center': rec.CostCenter,
                'Manufacturer': rec.manufacturer,
                'Supplier': rec.Supplier,
                'Current Agent': rec.Current_Agent,
                'Status': rec.wo_status,
                'Problem Type': rec.PROBLEM_TYPEDESC,
                'Assign To': rec.USER_ID,
                'Serial No': rec.serial_no,
                'Warranty Type': rec.PM_TYPE,
                'Close Date': rec.ClosedDate

            }
        });
        for (var i = 0; i < srarrayNew.length; i++) {
            Object.keys(srarrayNew[i]).forEach(function (field) {
                if (srarrayNew[i][field] == null) {
                    srarrayNew[i][field] = "";
                }
            })
        };
        if (cycle > 1) {
            for (i = 0; i < cycle; i++) {
                var sraraydata = srarrayNew.slice(frow, lrow);
                self.export(sraraydata, i);
            }
        } else {           
            self.export(srarrayNew, 0);
        }
    };

    self.exportTableToExel = function () {
        $("#WorkOrderSearchResultCMTable").excelexportjs({
            containerid: "WorkOrderSearchResultCMTable"
            , datatype: 'table'
            , filename: "WorkOrderDetail"
        });
    };

    self.clientDetails = {
        CLIENTNAME: ko.observable(),
        CLIENT_LOGO: ko.observable(),
        COMPANY_LOGO: ko.observable(),
        HEADING: ko.observable(),
        HEADING2: ko.observable(),
        HEADING3: ko.observable(),
        HEADING_OTH: ko.observable(),
        HEADING_OTH2: ko.observable(),
        HEADING_OTH3: ko.observable(),
        CLIENTADDRESS: ko.observable(),
        CLIENTCONTACT: ko.observable()
    };
    self.searchFilter = {
        fromDate: ko.observable(''),
        toDate: ko.observable(''),
        AssetFilterSuupiler: ko.observable(''),
        AssetFilterManufacturer:ko.observable(''),
        FilterStatus: ko.observable(''),
        Filterpmtype: ko.observable(''),
        //status: ko.observable(''),
        CostCenterStr: ko.observable(''),
        ProblemType:ko.observable('')
    };

    self.getPageData = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
        var postData;

        postData = { LOV: "'AIMS_COSTCENTER'", LOVJ: "'AIMS_PROBLEMTYPE'", STAFF_LOGON_SESSIONS: staffLogonSessions };
        return $.post(self.getApi() + ah.config.api.searchLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.showLoadingBackground = function () {
        var loadingBackground = $(ah.config.id.loadingBackground);
        loadingBackground.addClass('initializing').removeClass('done');
        var offset = loadingBackground.offset();
        var height = loadingBackground.height();
        var centerY = (offset.top + height / 2) - 120;
        $('.sk-circle').css({
            'margin-top': centerY + 'px',
            'display': 'block'
        });
    };
    self.searchFilterNow = function () {
        window.location.href = ah.config.url.modalClose;
        self.searchNow();
    };

    self.showModalFilter = function () {
        window.location.href = ah.config.url.modalFilter;
    };
    self.clearFilter = function () {
        $('#requestFromDate').val('');
        $('#requestStartDate').val('');
        self.searchFilter.fromDate = "";
        self.searchFilter.toDate = "";
        self.searchFilter.CostCenterStr = "";
        self.searchFilter.AssetFilterManufacturer="";
        self.searchFilter.AssetFilterSuupiler="";
        self.searchFilter.status="";
        self.searchFilter.ProblemType="";
        self.searchFilter.FilterPMType="";
        $("#costcenterList").val(null);
        $("#MANUFACTURERStr").val(null);
        $("#supplierStr").val(null);
        $("#filterStatus").val(null);
        $("#ProblemTypeList").val(null);
        $("#filterPMType").val(null);
    };
    self.printSummary = function () {
        ah.CurrentMode = ah.config.mode.print;
        document.getElementById("printSummary").classList.add('printSummary');

        if (self.clientDetails.CLIENTNAME() != null) {
            self.print();

        } else {
            var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var postData;

            var staffInfo = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
            postData = { CLIENTID: staffInfo.CLIENT_CODE, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.readClientInfo, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
    };
    self.print = function () {
        //var ReportCrit = $(ah.config.id.ReportCritria).val();
        var fromDate = $('#requestFromDate').val();
        var toDate = $('#requestStartDate').val();
        var textLable =" " ;
        if (fromDate !== null && fromDate !== "") {
            textLable += "From: " + fromDate + " To: " + toDate;
        }
        if (self.searchFilter.AssetFilterSuupiler() !== "" && self.searchFilter.AssetFilterSuupiler() !== null)
        {
            textLable += " for Supplier: " + self.searchFilter.AssetFilterSuupiler();
        }
        if (self.searchFilter.AssetFilterManufacturer() !== "" && self.searchFilter.AssetFilterManufacturer() !== null)
        {
            textLable += " for MANUFACTURER: " + self.searchFilter.AssetFilterManufacturer();
        }
        if (self.searchFilter.CostCenterStr() !== "" && self.searchFilter.CostCenterStr() !== null)
        {
            textLable += " for COST CENTERNAME: " + self.searchFilter.CostCenterStr();
        }
        if (self.searchFilter.pmtype !== "" && self.searchFilter.pmtype !==null)
        {
            textLable += "Warranty Type By: " + self.searchFilter.pmtype;
        }
        if (self.searchFilter.ProblemType() !== "" && self.searchFilter.ProblemType() !== null)
        {
            textLable += "Type Of Problem: " + self.searchFilter.ProblemType;
        }

        if (self.searchFilter.status !== "" && self.searchFilter.status !== null)
            {
            var WoStatus = self.searchFilter.status;
                var StatusWo = "Open";
                if (WoStatus === "CL") { StatusWo = "Close" }
                if (WoStatus === "HA") { StatusWo = "Hold" }
                if (WoStatus === "NE") { StatusWo = "New" }
                if (WoStatus === "FC") { StatusWo = "For Close" }
                if (WoStatus === "PSOP") { StatusWo = "New/Open" }

                textLable += " And Status: " + StatusWo;
            }
            textLable = "Work Order Report: " + textLable;
           
            $(ah.config.id.ReportSubTitle).text(textLable);
            $(ah.config.id.printSearchResultList).html($(ah.config.id.WorkOrderSearchResultCMTable).html());
         
        setTimeout(function () {
            window.print();

        }, 2000);
    };
    self.dateClick = function () {
        $("#dateClickAction").val("ACTIVE");
    };
    self.searchResultWorkOrder = function () {
        var table;

        if ($.fn.dataTable.isDataTable(ah.config.id.WorkOrderSearchResultCMTable)) {
            $(ah.config.id.WorkOrderSearchResultCMTable).DataTable().clear().destroy();
        }
        $(ah.config.cls.liPrint).hide();
        if (self.srDataArray.length > 0) {
            $(ah.config.cls.liPrint).show();
        }
        table = $(ah.config.id.WorkOrderSearchResultCMTable).DataTable({
            data: self.srDataArray,
            columns: [
                { data: 'req_no' },
                { data: 'asset_no' },
                { data: 'Description' },
                { data: 'model_no' },
                { data: 'ReqDate' },
                { data: 'JobDueDate' },
                { data: 'CostCenter' },
                { data: 'manufacturer' },
                { data: 'Supplier' },
                { data: 'Current_Agent'},
                { data: 'wo_status' },
                { data: 'PROBLEM_TYPEDESC' },
                { data: 'USER_ID' },
                { data: 'serial_no' },
                { data: 'PM_TYPE' },
                { data: 'ClosedDate'}
            ], 
            "searching": false,
            //"order":true,
            "iDisplayLength": 15,
            "aLengthMenu": [15, 200, 6500, self.srDataArray.length]

        });          
    };

    self.searchNow = function () {
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;
        ah.toggleSearchMode();

        var fromDate = $('#requestFromDate').val(); 

        fromDate = fromDate.split('/');
        if (fromDate.length == 3){
            self.searchFilter.fromDate(fromDate[2] + "/" + fromDate[1] + "/" + fromDate[0]);
        }
        var toDate = $('#requestStartDate').val().split('/');
        if (toDate.length == 3) {
            self.searchFilter.toDate(toDate[2] + "/" + toDate[1] + "/" + toDate[0] + "  23:00:00.000");
        }
        var apiSearch = null;
        apiSearch = ah.config.api.searchCMWorkOrder;
        $(ah.config.id.searchResultSummary).text("Work Order Details Report");
        $(ah.config.id.WorkOrderSearchResultList).show();
       

        self.showLoadingBackground();
        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), SEARCH_FILTER: self.searchFilter, STAFF_LOGON_SESSIONS: staffLogonSessions };
       
        $.when($.post(self.getApi() + apiSearch, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus)).done(function () {
            $(ah.config.id.loadingBackground).removeClass('initializing').addClass('done');
        });
    };
    self.webApiCallbackStatus = function (jsonData) {

        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));

        }
        else if (jsonData.AM_CMWorkOrdLIST || jsonData.AM_CLIENT_INFO  || jsonData.JobTypeLov || jsonData.COSTCENTERLOV || jsonData.SUPPLIERLISTLOV || jsonData.MANUFACTURERLOV) {
            if (ah.CurrentMode == ah.config.mode.search) {
                if (jsonData.AM_CMWorkOrdLIST) {
                    self.srDataArray = jsonData.AM_CMWorkOrdLIST;
                    self.searchResultWorkOrder();
                }
            }
            else if (ah.CurrentMode == ah.config.mode.print) {

                var clientInfo = jsonData.AM_CLIENT_INFO;

                self.clientDetails.CLIENTNAME(clientInfo.DESCRIPTION);
                self.clientDetails.CLIENT_LOGO(clientInfo.CLIENT_LOGO);
                self.clientDetails.COMPANY_LOGO(clientInfo.COMPANY_LOGO);
                self.clientDetails.HEADING(clientInfo.HEADING);
                self.clientDetails.HEADING2(clientInfo.HEADING2);
                self.clientDetails.HEADING3(clientInfo.HEADING3);
                self.clientDetails.HEADING_OTH(clientInfo.HEADING_OTH);
                self.clientDetails.HEADING_OTH2(clientInfo.HEADING_OTH2);
                self.clientDetails.HEADING_OTH3(clientInfo.HEADING_OTH3);
                self.clientDetails.CLIENTADDRESS(clientInfo.ADDRESS + ', ' + clientInfo.CITY + ', ' + clientInfo.ZIPCODE + ', ' + clientInfo.STATE);
                self.clientDetails.CLIENTCONTACT('Tel.No: ' + clientInfo.CONTACT_NO1 + ' Fax No:' + clientInfo.CONTACT_NO2);

                self.print();
            } else if (ah.CurrentMode == ah.config.mode.idle) {

                var lovCostCenterArray = jsonData.COSTCENTERLOV;
                $.each(lovCostCenterArray, function (item) {
                    $(ah.config.id.constCenterStrList)
                        .append($("<option>")
                            .attr("value", lovCostCenterArray[item].DESCRIPTION.trim())
                            .attr("id", lovCostCenterArray[item].LOV_LOOKUP_ID));
                });

                var lovSupplier = jsonData.SUPPLIERLISTLOV;
               // alert(JSON.stringify(lovSupplier));
                $.each(lovSupplier, function (item) {
                    $(ah.config.id.supplierStrList)
                        .append($("<option>")
                            .attr("value", lovSupplier[item].DESCRIPTION.trim())
                            .attr("id", lovSupplier[item].SUPPLIER_ID));
                });

                var lovManuf = jsonData.MANUFACTURERLOV;
                // alert(JSON.stringify(lovSupplier));
                $.each(lovManuf, function (item) {
                    $(ah.config.id.MANUFACTURERStrList)
                        .append($("<option>")
                            .attr("value", lovManuf[item].DESCRIPTION.trim())
                            .attr("id", lovManuf[item].manufacturer_id));
                });

                var lovJobTypeArray = jsonData.JobTypeLov;
                //alert(JSON.stringify(lovJobTypeArray));
                $.each(lovJobTypeArray, function (item) {
                    $(ah.config.id.ProblemTypeStrList)
                        .append($("<option>")
                            .attr("value", lovJobTypeArray[item].DESCRIPTION.trim())
                            .attr("id", lovJobTypeArray[item].LOV_LOOKUP_ID));
                });
                //alert(JSON.stringify($(ah.config.id.JobTypeStrList)));
            }

        }
        else {

            alert(systemText.errorTwo);

        }

    };

    self.initialize();
};