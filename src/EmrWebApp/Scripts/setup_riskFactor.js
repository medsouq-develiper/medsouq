﻿var appHelper = function (systemText) {

	var thisApp = this;

	thisApp.config = {
		cls: {
			contentField: '.field-content-detail',
			contentSearch: '.search-content-detail',
			contentHome: '.home-content-detail',
			liModify: '.liModify',
			liSave: '.liSaveAll',
			liAddNew: '.liAddNew',
			divProgress: '.divProgressScreen',
			liApiStatus: '.liApiStatus',
			notifySuccess: '.notify-success',
			toolBoxRight: '.toolbox-right',
			divClinicDoctorID: '.divClinicDoctorID',
			statusText: '.status-text',
			detailItem: '.detailItem'
		},
		tag: {
			input: 'input',
			textArea: 'textarea',
			select: 'select',
			inputTypeText: 'input[type=text]',
			inputTypeNumber: 'input[type=number]',
			inputTypeEmail: 'input[type=email]',
			ul: 'ul'
		},
		mode: {
			idle: 0,
			add: 1,
			search: 2,
			searchResult: 3,
			save: 4,
			read: 5,
			display: 6,
			edit: 7
		},
		id: {
			spanUser: '#spanUser',
			contentHeader: '#contentHeader',
			contentSubHeader: '#contentSubHeader',
			saveInfo: '#saveInfo',
			searchResultList: '#searchResultList',
			searchResultSummary: '#searchResultSummary',
			txtGlobalSearch: '#txtGlobalSearch',
			frmInfo: '#frmInfo',
			riskCategory: '#RISK_CATEGORYCODE',
			riskCat: '#riskCat',
			staffID: '#STAFF_ID',
			
			successMessage: '#successMessage'
		},
		cssCls: {
			viewMode: 'view-mode'
		},
		attr: {
			readOnly: 'readonly',
			disabled: 'disabled',
			selectedIndex: 'selectedIndex',
			datainfoId: 'data-infoID'
		},
		skey: {
			staff: 'STAFF',
			webApiUrl: 'webApiURL',
			staffLogonSessions: 'STAFF_LOGON_SESSIONS',
			searchResult: 'searchResult',
			infoDetails: 'RISK_CATEGORYFACTOR'
		},
		url: {
			homeIndex: '/Home/Index',
			aimsHomepage: '/Menuapps/aimsIndex',
			modalInfo: '#openModalInfo',
			modalClose: '#'
		},
		api: {

			readSetup: '/AIMS/ReadRFSInfo',
			searchAll: '/AIMS/SearchRFS',
			createSetup: '/AIMS/CreateNewRFS',
			updateSetup: '/AIMS/UpdateRFSInfo',
			searchRiskFactorGroup: '/AIMS/SearchRFGS'
		},
		html: {
			searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
			searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
		}
	};

	thisApp.CurrentMode = thisApp.config.mode.idle;

	thisApp.ResetControls = function () {

		$(thisApp.config.tag.input).val('');
		$(thisApp.config.tag.textArea).val('');
		$(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

	};

	thisApp.initializeLayout = function () {

		thisApp.ResetControls();

		$(
			thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
			thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
			thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess
		).hide();

		$(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

		thisApp.CurrentMode = thisApp.config.mode.idle;

	};

	thisApp.toggleAddMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.add;

		//thisApp.ResetControls();
		//$(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
		//$(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

		$(thisApp.config.cls.contentField).show();

		$(
			thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
			thisApp.config.cls.contentHome
		).hide();


		$(
			thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
			thisApp.config.tag.textArea
		).removeClass(thisApp.config.cssCls.viewMode);

		$(
			thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
		).prop(thisApp.config.attr.disabled, false);

		$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
		
	};

	thisApp.toggleSearchMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.search;

		$(thisApp.config.cls.statusText).text(systemText.searchStatusText);
		$(thisApp.config.cls.liApiStatus).show();

		$(
			thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
			thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
		).hide();

	};

	thisApp.displaySearchResult = function () {

		thisApp.CurrentMode = thisApp.config.mode.searchResult;

		$(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
		$(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave).hide();

	};

	thisApp.toggleEditMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.edit;

		//$(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
		//$(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

		$(thisApp.config.cls.contentField).show();
		$(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

		$(
			thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
			thisApp.config.tag.textArea
		).removeClass(thisApp.config.cssCls.viewMode);

		$(
			thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
		).prop(thisApp.config.attr.disabled, false);

		$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
		

	};

	thisApp.toggleSaveMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.save;

		$(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
		$(thisApp.config.cls.liSave).hide();
		$(thisApp.config.cls.divProgress).show();
		$(thisApp.config.cls.liApiStatus).show();

	};

	thisApp.toggleAddExistMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.add;


		$(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
		$(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

		$(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.searhButton).show();

		$(
			thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.statusText +
			thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.iconDet + ',' + thisApp.config.cls.divProgress
		).hide();


		$(
			thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
			thisApp.config.tag.textArea
		).removeClass(thisApp.config.cssCls.viewMode);

		$(
			thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
		).prop(thisApp.config.attr.readOnly, false);

		$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
	};

	thisApp.toggleReadMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.read;

		$(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
		$(thisApp.config.cls.liApiStatus).show();

		$(
			thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
			thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
			thisApp.config.cls.contentField
		).hide();
	};

	thisApp.toggleDisplayMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.display;

		$(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
		$(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

		$(
			thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
			thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField
		).show();

		$(
			thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
			thisApp.config.cls.contentHome
		).hide();

		$(
			thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
			thisApp.config.tag.textArea
		).addClass(thisApp.config.cssCls.viewMode);

		$(
			thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
		).prop(thisApp.config.attr.disabled, true);

		$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);
		

	};

	thisApp.showSavingStatusSuccess = function () {

		$(thisApp.config.cls.notifySuccess).show();
		$(thisApp.config.cls.divProgress).hide();

		setTimeout(function () {
			$(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
		}, 3000);


	};

	thisApp.ConvertFormToJSON = function (form) {
		$(form).find("input:disabled").removeAttr("disabled");
		var array = jQuery(form).serializeArray();
		var json = {};

		jQuery.each(array, function () {
			json[this.name] = this.value || '';
		});

		return json;

	};

	thisApp.getAge = function (birthDate) {

		var seconds = Math.abs(new Date() - birthDate) / 1000;

		var numyears = Math.floor(seconds / 31536000);
		var numdays = Math.floor((seconds % 31536000) / 86400);
		var nummonths = numdays / 30;

		return numyears + " y " + Math.round(nummonths) + " m ";
	};

	thisApp.formatString = function () {

		var stringToFormat = arguments[0];

		for (var i = 0; i <= arguments.length - 2; i++) {
			stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
		}

		return stringToFormat;
	};

	thisApp.LoadJSON = function (jsonData) {

		$.each(jsonData, function (name, val) {
			var $el = $('#' + name),
				type = $el.attr('type');

			switch (type) {
				case 'checkbox':
					$el.attr('checked', 'checked');
					break;
				case 'radio':
					$el.filter('[value="' + val + '"]').attr('checked', 'checked');
					break;
				default:
					var tagNameLCase = String($el.prop('tagName')).toLowerCase();
					if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
						$el.text(val);
					} else {
						$el.val(val);
					}
			}
		});

	};

};

/*Knockout MVVM Namespace - Controls form functionality*/
var amriskFactorViewModel = function (systemText) {

	var self = this;

	var ah = new appHelper(systemText);

	self.initialize = function () {

		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		var postData;

		$(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

		ah.toggleReadMode();

		ah.initializeLayout();
		postData = { SEARCH:"", STAFF_LOGON_SESSIONS: staffLogonSessions };
		$.post(self.getApi() + ah.config.api.searchRiskFactorGroup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus)

	};
	self.getApi = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		//alert(JSON.stringify(staffDetails));
		var api = "";
		if (staffDetails.API === null || staffDetails.API === "") {
			api = webApiURL + "api";
		} else {
			api = staffDetails.API + "api";
		}

		return api
	};

	self.returnHome = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		window.location.href = staffDetails.HOME_DEFAULT;
	};
	self.createNew = function () {

		ah.toggleAddMode();
		var riskCatSelected = $(ah.config.id.riskCat).val();
		$("#RISK_CATEGORYCODE").val(riskCatSelected);
		$("#RISK_DESCRIPTION").val("");
		$("#RISK_SCORE").val(0);
		window.location.href = ah.config.url.modalInfo;

	};

	self.modifySetup = function () {

		ah.toggleEditMode();

	};

	self.signOut = function () {

		window.location.href = ah.config.url.homeIndex;

	};

	self.saveAllChanges = function () {

		$(ah.config.id.saveInfo).trigger('click');

	};

	self.cancelChanges = function () {

		if (ah.CurrentMode == ah.config.mode.add) {
			ah.initializeLayout();
			$("#RISK_FACTORCODE").next("span").remove();
		} else {
			ah.toggleDisplayMode();
			self.showDetails(true);
		}

	};

	self.showDetails = function (isNotUpdate) {
		
		if (isNotUpdate) {

			var infoDetails = sessionStorage.getItem(ah.config.skey.infoDetails);

			var jsetupDetails = JSON.parse(infoDetails);

			//ah.ResetControls();
			ah.LoadJSON(jsetupDetails);
			window.location.href = ah.config.url.modalInfo;
		} else {
			
		}

		ah.toggleEditMode();
		

	};

	self.searchResult = function (jsonData) {


		var sr = jsonData.RISK_CATEGORYFACTOR;

		$(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

		$(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

		if (sr.length > 0) {
			var htmlstr = '';

			for (var o in sr) {

				if (sr[o].RISK_DESCRIPTION.length > 100)
					sr[o].RISK_DESCRIPTION = sr[o].RISK_DESCRIPTION.substring(0, 99) + "...";

				htmlstr += '<li>';
				htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].RISK_FACTORCODE, sr[o].RISK_FACTORCODE, sr[o].RISK_DESCRIPTION, '');
				htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.riskCagetory, sr[o].RISK_CATEGORY);
				htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.riskScore, sr[o].RISK_SCORE);
				htmlstr += '</li>';

			}

			$(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
		}

		$(ah.config.cls.detailItem).bind('click', self.readSetupID);
		ah.displaySearchResult();

	};

	self.readSetupID = function () {

		var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var postData;

		ah.toggleReadMode();

		postData = { RISK_FACTORCODE: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
		$.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

	};
	self.closeModal = function () {
		window.location.href = ah.config.url.modalClose;
		self.searchNow();
	};

	self.searchNow = function () {

		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		var postData;

		ah.toggleSearchMode();

		postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), RISK_CATEGORYCODE: $(ah.config.id.riskCat).val().toString(), STAFF_LOGON_SESSIONS: staffLogonSessions };
		$.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
	};

	self.saveInfo = function () {

		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var setupDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var postData;

		setupDetails.CREATED_BY = staffLogonSessions.STAFF_ID;

		//self.closeModal();

		if (ah.CurrentMode == ah.config.mode.add) {
			setupDetails.RISK_FACTORCODE = 0;
			var jDate = new Date();
			var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
			function time_format(d) {
				hours = format_two_digits(d.getHours());
				minutes = format_two_digits(d.getMinutes());
				return hours + ":" + minutes;
			}
			function format_two_digits(n) {
				return n < 10 ? '0' + n : n;
			}
			var hourmin = time_format(jDate);

			setupDetails.CREATED_DATE = strDateStart + ' ' + hourmin;
			postData = { RISK_CATEGORYFACTOR: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
			ah.toggleSaveMode();
			//alert(JSON.stringify(postData));
			$.post(self.getApi() + ah.config.api.createSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
		}
		else {

			ah.toggleSaveMode();
			postData = { RISK_CATEGORYFACTOR: setupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
			//alert(JSON.stringify(postData));
			$.post(self.getApi() + ah.config.api.updateSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
		}

	};

	self.webApiCallbackStatus = function (jsonData) {
		//alert(JSON.stringify(jsonData));
		if (jsonData.ERROR) {
			
	
				alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
				window.location.href = ah.config.url.homeIndex;
	

		}
		else if (jsonData.RISK_CATEGORYGROUP || jsonData.RISK_CATEGORYFACTOR || jsonData.STAFF || jsonData.SUCCESS) {
			
			if (ah.CurrentMode == ah.config.mode.save) {
				sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.RISK_CATEGORYFACTOR));
				$(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
				ah.showSavingStatusSuccess();

				if (jsonData.SUCCESS) {
					self.showDetails(false);
				}
				else {
					self.showDetails(true);
				}
			}
			else if (ah.CurrentMode == ah.config.mode.read) {
				sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.RISK_CATEGORYFACTOR));
				self.showDetails(true);
			}
			else if (ah.CurrentMode == ah.config.mode.search) {
				self.searchResult(jsonData);
			}
			else if (ah.CurrentMode == ah.config.mode.idle) {
				var category = jsonData.RISK_CATEGORYGROUP;
				

				$.each(category, function (item) {

					$(ah.config.id.riskCategory)
						.append($("<option></option>")
							.attr("value", category[item].RISK_CATEGORYCODE)
							.text(category[item].RISK_CATEGORY));
				});
				$.each(category, function (item) {

					$(ah.config.id.riskCat)
						.append($("<option></option>")
							.attr("value", category[item].RISK_CATEGORYCODE)
							.text(category[item].RISK_CATEGORY));
				});
			}

		}
		else {

			alert(systemText.errorTwo);
			window.location.href = ah.config.url.homeIndex;
		}

	};

	self.initialize();
};