﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divInfo: '.divInfo',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divClinicDoctorID: '.divClinicDoctorID',
            statusText: '.status-text',
            detailItem: '.detailItem',
            productDetailItem: '.productDetailItem',
            modalBox: '.modalBox',
            modalwidth3: '.modalwidth3',
            liAddItem: '.liAddItem',
            detailParts: '.detailParts',
            modalwidth3: '.modalwidth3'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            ul: 'ul',
            table: 'table'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            txtGlobalSearchOth: '#txtGlobalSearchOth',
            frmInfo: '#frmInfo',
            frmitemInfo: '#itemInfo',
            storeCURR: '#STORE',
            supplierCURR: '#SUPPLIER',
            staffID: '#STAFF_ID',
            totNumber: '#totNumber',
            loadNumber: '#loadNumber',
            searchResult1: '#searchResult1',
            pageNum: '#pageNum',
            pageCount: '#pageCount',
            populateData: '#populateData',
            successMessage: '#successMessage',
            itemInfo: '#itemInfo',
            initialTable: '#initialTable',
            itemDescription: '#itemDescription',
            searchResult11: '#searchResult11',
            infoStore: '#INFOSTORE',
            infoSupplier: '#INFOSUPPLIER',
            infoSupplier_r: '#INFOSUPPLIER_R',
            infoProductId: '#INFOPRODUCT_ID',
            infoProductDesc: '#INFOPRODUCT_DESC',
            addButton: '#addButton',
            updateButton: '#updateButton',
            btnRemove: '#btnRemove',
            btnModify: '#btnModify',
            loadNumber1: '#loadNumber1',
            pageCount1: '#pageCount1',
            pageNum1: '#pageNum1',
            txtGlobalSearchOth1: '#txtGlobalSearchOth1',
            InfoText: '#InfoText',
            totNumber1: '#totNumber1',
            itemDescriptionReturn: '#itemDescriptionReturn',
            frmitemInfoReturn: '#itemInfoReturn',
            returnButton: '#returnButton'
        
        },
        tagId: {
            page1: 'page1',
            page2: 'page2',
            priorPage: 'priorPage',
            nextPage: 'nextPage',
            txtGlobalSearchOth: 'txtGlobalSearchOth',
            page11: 'page11',
            page21: 'page21',
            priorPage1: 'priorPage1',
            nextPage1: 'nextPage1',
            txtGlobalSearchOth1: 'txtGlobalSearchOth1'
           

        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID',
            dataItems: 'data-items'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'searchResult',
            storeDetails: 'STORES',
            productItems: 'PRODUCT_STOCKONHAND',
            productStockBatch: 'PRODUCT_STOCKBATCH',
            productStockBatchPush: 'PRODUCT_STOCKBATCHPUSH',
            purchaseList: 'AM_PURCHASEREQUEST',
            groupPrivileges: 'GROUP_PRIVILEGES',
        },
        url: {
            homeIndex: '/Home/Index',
            modalAddItems: '/inventory/stockBatchReceive#openModal',
            openModalPOParts: '/inventory/stockBatchReceive#openModalPOParts',
            modalOpenPurchase: '/inventory/stockBatchReceive#openModalPurchase',
            modalAddItemsInfo: '/inventory/stockBatchReceive#openModalInfo',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalClose: '/inventory/stockBatchReceive#',
            modalFilter: '#openModalFilter',
            openModalReturn: '/inventory/stockBatchReceive#openModalReturn'
        },
        api: {

            readTransIDList: '/Inventory/SearchStockBatchReceiveItems',
            searchAll: '/Inventory/SearchStockBatchReceive',
            searchLookUps: '/Inventory/SearchStockBatchRecCatLov',
            searchStockItems: '/Inventory/SearchStockItems',
            createStockBatchReceive: '/Inventory/CreateNewStockBatchReceive',
            updateSetup: '/Inventory/UpdateStoreDetails',
			readPurchaseReq: '/AIMS/SearchPurchaseOrderItemBatch',
            searchPurchaseReq: '/AIMS/SearchPOItemsApprove',
            updatePurchaseReturn: '/Inventory/UpdatePurchaseReturn'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchLOVRowHeaderTemplate: "<p  class='fs-medium-normal productDetailItem fc-greendark' data-items='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} <span>{3}</span></p>",
            searchExistRowHeaderTemplate: "<p  class='fs-medium-normal productDetailItem fc-slate-blue' data-items='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} <span class='fc-green'> {3} </span></p>",
            searchDeleteRowHeaderTemplate: "<a href='#'onclick='deleteRow(this)' ><i class='fa fa-trash'></a>",
            //searchRowTemplate: '<span class="search-row-label">{0} <span class="search-row-data">{1}</span></span>&nbsp;&nbsp;'
            searchRowTemplateSpareParts: "<span class='search-row - label spareParts  detailParts' data-infoID='{0}'> <span>{1}</span></span>",
            searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>&nbsp;&nbsp;&nbsp;&nbsp;"
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liAddItem + ',' + thisApp.config.id.InfoText + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess + ',' + thisApp.config.cls.divInfo
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;
        document.getElementById("INFOSTORE").disabled = true;
    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);
        $(thisApp.config.id.contentHeader).show();
        $(thisApp.config.id.contentSubHeader).show();
        $(thisApp.config.cls.liAddItem + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.id.InfoText + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("INFOSTORE").disabled = true;
    };

    thisApp.toggleAddItems = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID
        ).hide();


        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        document.getElementById("INFOSTORE").disabled = true;
        document.getElementById("INFOSUPPLIER").disabled = false;
    };
    thisApp.toggledisplayItemInfo= function () {
       
        $(thisApp.config.cls.modalBox).show();

        $(thisApp.config.cls.modalwidth3).hide();
    };
    thisApp.toggleSearchItems = function () {

        $(thisApp.config.cls.modalBox).hide();

        $(
            thisApp.config.cls.modalwidth3).show();
    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.liAddItem + ',' + thisApp.config.id.InfoText + ',' + thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.liSave + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.divInfo
        ).hide();

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus).hide();
        document.getElementById("INFOSTORE").disabled = true;
    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight).hide();

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("INFOSTORE").disabled = true;
    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + 
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
        document.getElementById("INFOSTORE").disabled = true;
        document.getElementById("INFOSUPPLIER").disabled = false;
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

        $(
            thisApp.config.id.infoSupplier_r +','+thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
           thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.id.infoSupplier_r + ',' +thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.readOnly, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);
        $(thisApp.config.id.infoSupplier_r).disabled = true;
        document.getElementById("INFOSTORE").disabled = true;
        document.getElementById("INFOSUPPLIER").disabled = false;
    };
    thisApp.toggleOpenDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liSave + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.divInfo
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

       
        document.getElementById("INFOSTORE").disabled = true;
       

    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {
        $(form).find(":disabled").removeAttr("disabled");
        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.getAge = function (birthDate) {

        var seconds = Math.abs(new Date() - birthDate) / 1000;

        var numyears = Math.floor(seconds / 31536000);
        var numdays = Math.floor((seconds % 31536000) / 86400);
        var nummonths = numdays / 30;

        return numyears + " y " + Math.round(nummonths) + " m ";
    };
    thisApp.formatJSONDateToString = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "";

        strDate = String(dateToFormat).split('-', 3);

        dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


        return dateToFormat;
    };
    thisApp.formatJSONDateToStringOth = function () {


        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "";
        if (dateToFormat.length < 16) return "";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        dateToFormat = strDate[0] + '-' + strDate[1] + '-' + strDate[2].substring(0, 2);


        return dateToFormat;
    };
    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.formatJSONTimeToString = function () {

        var dateToFormat = arguments[0];
        var strDate;
        var strTime;

        if (dateToFormat === null) return "";
        if (dateToFormat.length < 16) return "";

        strDate = String(dateToFormat).split('-', 3);
        strTime = String(dateToFormat).split('T');

        var timeToFormat = strTime[1].substring(0, 5);


        return timeToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name),
                type = $el.attr('type');

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var stockbatchReceiveViewModel = function (systemText) {
    var self = this;
    var ah = new appHelper(systemText);
    self.searchKeyword = ko.observable('');
    self.partsWOList = ko.observableArray("");
    self.searchFilter = {
        fromDate: ko.observable(''),
        toDate: ko.observable(''),
        status: ko.observable(''),
    }
    self.clearFilter = function () {
        self.searchFilter.toDate('');
        self.searchFilter.fromDate('');
        self.searchFilter.status('');
        $('#requestFromDate').val('');
        $('#requestStartDate').val('');
        window.location.href = ah.config.url.modalClose;
    }
    self.searchFilterNow = function () {
        window.location.href = ah.config.url.modalClose;
        self.searchNow();
    }
    self.searchKeyUp = function (d, e) {
        if (e.keyCode == 13) {
            self.searchNow();
        }
    }
    sessionStorage.removeItem("PRODUCT_STOCKBATCHPUSH");
    var appendData = JSON.parse(sessionStorage.getItem(ah.config.skey.productStockBatchPush));
    if (appendData === null || appendData === "") {
        var newAttribute = JSON.stringify({ "PRODUCT_DESC": 'Testing', "ID_PARTS": '', "SUPPLIER": '', "STORE": '', "BARCODE_ID": '0', "QTY": '0', "PRICE": '0', "PRICE_DISCOUNT": '0', "STATUS": '', "TRANS_ID": '', "BARCODE_ID": '', "EXPIRY_DATE": '', "TRANS_DATE": '', "CREATED_BY": '', "CREATED_DATE": '', "TOTAL_PRICE": '0' });
        var appendData = JSON.parse('[' + newAttribute + ']');
        sessionStorage.setItem(ah.config.skey.productStockBatchPush, JSON.stringify(appendData));
    };
    self.itemsReceive = ko.observableArray("");
    self.dateDisabled = ko.observable(false);
    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        ah.toggleReadMode();

        ah.initializeLayout();

        var postData;
        postData = { SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchLookUps, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.showModalFilter = function () {

        window.location.href = ah.config.url.modalFilter;
    };
    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.addItemToReceive = function () {
        if ($("#INFOQTY").val() == "" || $("#INFOQTY").val() == "0") {
            return alert("QTY must have a value greater than 0");
        }
        var sr = ah.ConvertFormToJSON($(ah.config.id.frmitemInfo)[0]);
        if (sr.INFOPRICE === null || sr.INFOPRICE === "") {
            alert('Please provide price to proceed')
            return;
        }
        if (sr.INFOPRICE === null || sr.INFOPRICE === "") {
            alert('Please provide price to proceed')
            return;
        }
        if (sr.INFOSUPPLIER === null || sr.INFOSUPPLIER === "") {
            alert('Please provide supplier to proceed')
            return;
        }
        if (sr.INFOBARCODE_ID === null || sr.INFOBARCODE_ID === "") {
            alert('Please provide batch no. to proceed')
            return;
        }
        if (sr.INFOEXPIRY_DATE === null || sr.INFOEXPIRY_DATE === "") {
            sr.INFOEXPIRY_DATE = "01/01/1900";
        } else {
            returnStr = self.isValidDate(sr.INFOEXPIRY_DATE, 'INS');
            if (returnStr === 'Invalid') {
                alert('Unable to proceed, Please make sure that the date format provided in Expiry Date is correct (dd/mm/yyyy)');
                return;
            } else {

                sr.INFOEXPIRY_DATE = returnStr;
            }
        }

        var storeDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.storeDetails));

        var storeId = parseInt($("#STORE").val());
        var storeDesc = "";
        storeDetails = storeDetails.filter(function (item) { return item.STORE_ID === storeId });
        if (storeDetails.length > 0) {
            storeDesc = storeDetails[0].DESCRIPTION;
        }
        var transId = $("#TRANS_ID").val();
        self.itemsReceive.push({
            PRODUCT_DESC: sr.INFOPRODUCT_DESC,
            PRODUCT_CODE: sr.INFOPRODUCT_ID,
            ID_PARTS: sr.INFOPRODUCT_ID,
            TRANS_ID: transId,
            MIN: sr.INFOMIN,
            MAX: sr.INFOMAX,
            STATUS: 'OPEN',
            PRICE: sr.INFOPRICE,
            PRICE_DISCOUNT: sr.INFOSALEPRICE,
            QTY: sr.INFOQTY,
            TOTAL_PRICE: sr.INFOTOTAL_PRICE,
            BARCODE_ID: sr.INFOBARCODE_ID,
            STOREDESC: storeDesc,
            EXPIRY_DATE: (sr.INFOEXPIRY_DATE).substring(6, 10) + '-' + (sr.INFOEXPIRY_DATE).substring(3, 5) + '-' + (sr.INFOEXPIRY_DATE).substring(0, 2),
            STORE: storeId,
            SUPPLIER: sr.INFOSUPPLIER,
            PURCHASE_NO: 0
        });
        window.location.href = ah.config.url.modalClose;
    };

    self.addItemfromPR = function (jsonData) {
        //ah.toggleAddMode();
        //  alert(JSON.stringify(jsonData));

        var storeDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.storeDetails));
        self.itemsReceive.splice(0, 5000);
        var storeId = parseInt($("#STORE").val());
        var storeDesc = null;
        $(ah.config.cls.liAddItem).hide();
        var sr = jsonData.AM_PURCHASEREQUEST;
        if (sr.length > 0) {
            $(ah.config.id.InfoText + ',' + ah.config.cls.divInfo).show();
            $(ah.config.id.contentSubHeader).hide();

            $("#divInfoText").text('Receiving from Purchase Order No:' + sr[0].PURCHASE_NO);
            $(ah.config.id.InfoText).html('Select Receiving Store and Complete each detail of receiving part(s). Click on <i class=" fa fa-pencil-square-o"></i>.  Then click on save button to proceed.');
            for (var o in sr) {

                if (sr[0].RECEIVING_STORE > 0) {
                    storeDetails = storeDetails.filter(function (item) { return item.STORE_ID === sr[0].RECEIVING_STORE });
                    if (storeDetails.length > 0) {
                        storeDesc = storeDetails[0].DESCRIPTION;
                        $("#STORE").val(sr[0].RECEIVING_STORE);
                    }
                }


                var subTotal = sr[o].PO_UNITPRICE * sr[o].QTY_PO_REQUEST;
                //alert(JSON.stringify(sr));
                //alert(sr[0].PURCHASE_NO);
                self.itemsReceive.push({
                    PRODUCT_DESC: sr[o].DESCRIPTION,
                    PRODUCT_CODE: sr[o].ID_PARTS,
                    ID_PARTS: sr[o].ID_PARTS,
                    TRANS_ID: 0,
                    MIN: 0,
                    MAX: 0,
                    STATUS: 'FOR POSTING',
                    PRICE: sr[o].PO_UNITPRICE,
                    PRICE_DISCOUNT: sr[o].PO_UNITPRICE,
                    QTY: sr[o].QTY_PO_REQUEST,
                    TOTAL_PRICE: subTotal,
                    BARCODE_ID: null,
                    STOREDESC: storeDesc,
                    EXPIRY_DATE: null,
                    STORE: null,
                    SUPPLIER: sr[o].SUPPLIER_ID,
                    PURCHASE_NO: sr[0].PURCHASE_NO,
                    QTY_RECEIVED: sr[o].QTY_PO_REQUEST,
                    QTY_RETURN: 0
                });
            }
            document.getElementById("STORE").disabled = false;
        }
        $("#btnReturn").hide();
        window.location.href = ah.config.url.modalClose;
    };
    self.checkExist = function (idPart) {

        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.itemsReceive);
        var partLists = JSON.parse(jsonObj);

        var partExist = partLists.filter(function (item) { return item.ID_PARTS === idPart });

        return partExist.length;
    };
    self.isValidDate = function (dateString, action) {
        // First check for the pattern
        var regex_date = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
        var regex_date2 = /^(\d{1,2})-(\d{1,2})-(\d{4})$/;
        if (!regex_date.test(dateString)) {
            if (!regex_date2.test(dateString)) {
                return 'Invalid';
            }
        }
        // Parse the date parts to integers
        var n = dateString.search("-");
        var parts = "";
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var newDateFormat = "";
        if (n <= 0) {
            parts = dateString.split("/");
            var day = parseInt(parts[0], 10);
            var month = parseInt(parts[1], 10);
            var year = parseInt(parts[2], 10);
            newDateFormat = dateString;
        } else {
            parts = dateString.split("-");
            var day = parseInt(parts[0], 10);
            var month = parseInt(parts[1], 10);
            var year = parseInt(parts[2], 10);
            newDateFormat = (format_two_digits(day) + '/' + format_two_digits(month) + '/' + year).trim();
            $("#INFOEXPIRY_DATE").val(newDateFormat);

        }

        // Check the ranges of month and year
        if (year < 1000 || year > 3000 || month == 0 || month > 12) {
            return 'Invalid';
        }

        var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        // Adjust for leap years
        if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
            monthLength[1] = 29;
        }

        // Check the range of the day
        if (day > 0 && day <= monthLength[month - 1]) {
            return newDateFormat;
        } else {
            return 'Invalid';
        }
    };
    self.removeItem = function (itemsReceive) {
        // alert(ko.utils.stringifyJson(self.itemsReceive));
        self.itemsReceive.remove(itemsReceive);
    };
    self.updateItemInfo = function (itemsReceive) {
        if ($("#INFOQTY").val() == "" || $("#INFOQTY").val() == "0") {
            return alert("Purchase QTY must have a value greater than 0");
        }
        var sr = ah.ConvertFormToJSON($(ah.config.id.frmitemInfo)[0]);


        if (sr.INFOPRICE === null || sr.INFOPRICE === "") {
            alert('Please provide price to proceed')
            return;
        }
        if (sr.INFOSUPPLIER === null || sr.INFOSUPPLIER === "") {
            alert('Please provide supplier to proceed')
            return;
        }
        if (sr.INFOSALEPRICE === null || sr.INFOSALEPRICE === "") {
            alert('Please provide sale price to proceed')
            return;
        }
        //if (sr.INFOBARCODE_ID === null || sr.INFOBARCODE_ID === "") {
        //    alert('Please provide batch no. to proceed')
        //    return;
        //}
        if (sr.INFOEXPIRY_DATE === null || sr.INFOEXPIRY_DATE === "") {
            sr.INFOEXPIRY_DATE = "01/01/1900"
        } else {
            returnStr = self.isValidDate(sr.INFOEXPIRY_DATE, 'INS');
            if (returnStr === 'Invalid') {
                alert('Unable to proceed, Please make sure that the date format provided in Expiry Date is correct (dd/mm/yyyy)');
                return;
            } else {

                sr.INFOEXPIRY_DATE = returnStr;
            }
        }
        var delRow = parseInt($("#delRow").val());
        var todelrow = parseInt(delRow) + 1;
        table = document.getElementById("itemsReceiveBatch");

        table.deleteRow(todelrow);
        self.itemsReceive.splice(delRow, 1);
        $("#delRow").val("");

        var storeDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.storeDetails));

        var storeId = parseInt($("#STORE").val());
        var storeDesc = "";
        storeDetails = storeDetails.filter(function (item) { return item.STORE_ID === storeId });
        if (storeDetails.length > 0) {
            storeDesc = storeDetails[0].DESCRIPTION;
        }


        self.itemsReceive.push({
            PRODUCT_DESC: sr.INFOPRODUCT_DESC,
            PRODUCT_CODE: sr.INFOPRODUCT_ID,
            ID_PARTS: sr.INFOPRODUCT_ID,
            BATCH_ID: sr.BATCH_ID,
            TRANS_ID: sr.TRANS_ID,
            MIN: sr.INFOMIN,
            MAX: sr.INFOMAX,
            STATUS: 'FOR POSTING',
            PRICE: sr.INFOPRICE,
            PRICE_DISCOUNT: sr.INFOSALEPRICE,
            QTY: sr.INFOQTY,
            TOTAL_PRICE: sr.INFOTOTAL_PRICE,
            BARCODE_ID: sr.INFOBARCODE_ID,
            STOREDESC: storeDesc,
            STORE: storeId,
            EXPIRY_DATE: (sr.INFOEXPIRY_DATE).substring(6, 10) + '-' + (sr.INFOEXPIRY_DATE).substring(3, 5) + '-' + (sr.INFOEXPIRY_DATE).substring(0, 2),
            SUPPLIER: sr.INFOSUPPLIER,
            PURCHASE_NO: sr.INFOPURCHASENO,
            QTY_RECEIVED: sr.INFOQTY_RECEIVED,
            QTY_RETURN: sr.INFOQTYRETURN
        });


        window.location.href = ah.config.url.modalClose;
    };
    self.modifyItem = function (itemsReceive) {
        var storeId = $(ah.config.id.storeCURR).val().toString();
        if (storeId === null || storeId === "") {
            alert('Please select receiving store to proceed.');
            return;
        }

        var i = self.itemsReceive.indexOf(itemsReceive);
        $(ah.config.id.addButton).hide();
        $(ah.config.id.updateButton).show();
        $("#delRow").val(i);

        self.selected(itemsReceive);
        var productId = self.selected().ID_PARTS;
        $(ah.config.id.frmitemInfo)[0].reset();
        $(ah.config.id.itemDescription).text(self.selected().PRODUCT_DESC);

        $(ah.config.id.infoProductId).val(self.selected().ID_PARTS);
        $(ah.config.id.infoProductDesc).val(self.selected().PRODUCT_DESC);

        $("#INFOPRICE").val(self.selected().PRICE);
        $("#BATCH_ID").val(self.selected().BATCH_ID);
        $("#TRANS_ID").val(self.selected().TRANS_ID);
        $("#INFOSALEPRICE").val(self.selected().PRICE_DISCOUNT);
        $("#INFOQTY").val(self.selected().QTY);
        $("#INFOTOTAL_PRICE").val(self.selected().TOTAL_PRICE);
        $("#INFOBARCODE_ID").val(self.selected().BARCODE_ID);
        var edate = null;

        $("#INFOEXPIRY_DATE").val(edate);
        $("#INFOMIN").val(self.selected().MIN);
        $("#INFOMAX").val(self.selected().MAX);
        $("#INFOSTORE").val(self.selected().STORE);
        $("#INFOSUPPLIER").val(self.selected().SUPPLIER);
        $("#INFOPURCHASENO").val(self.selected().PURCHASE_NO);
        $("#INFOQTY_RECEIVED").val(self.selected().QTY_RECEIVED);
        $("#INFOQTYRETURN").val(self.selected().QTY_RETURN);
        $("#INFORETURNREASON").val(self.selected().RETURN_REASON);
        ah.toggledisplayItemInfo();
        // self.itemsReceive.remove(itemsReceive);
        window.location.href = ah.config.url.modalAddItemsInfo;
    };
    self.returnItem = function (itemsReceive) {

        var i = self.itemsReceive.indexOf(itemsReceive);
        $("#delRow").val(i);
        document.getElementById("INFOSUPPLIER_R").disabled = true;
        document.getElementById("INFOBARCODE_ID_R").disabled = true;

        self.selected(itemsReceive);
        var productId = self.selected().ID_PARTS;
        $(ah.config.id.itemDescriptionReturn).text(self.selected().PRODUCT_DESC);

        $("#ID_PARTS").val(self.selected().ID_PARTS);
        $("#INFOPRODUCT_DESC_R").val(self.selected().PRODUCT_DESC);

        $("#INFOPRICE_R").val(self.selected().PRICE);
        $("#BATCH_ID_R").val(self.selected().BATCH_ID);
        $("#TRANS_ID_R").val(self.selected().TRANS_ID);
        $("#INFOSALEPRICE_R").val(self.selected().PRICE_DISCOUNT);
        $("#INFOQTY_R").val(self.selected().QTY);
        $("#INFOTOTAL_PRICE_R").val(self.selected().TOTAL_PRICE);
        $("#INFOBARCODE_ID_R").val(self.selected().BARCODE_ID);
        var edate = null;

        $("#INFOMIN_R").val(self.selected().MIN);
        $("#INFOMAX_R").val(self.selected().MAX);
        $("#STATUS_R").val(self.selected().STATUS);
        $("#STOREID").val(self.selected().STORE);
        $("#INFOSUPPLIER_R").val(self.selected().SUPPLIER);
        $("#INFOPURCHASENO_R").val(self.selected().PURCHASE_NO);
        $("#INFOQTY_RECEIVED").val(self.selected().QTY);
        $("#INFOQTY_RETURN").val(self.selected().QTY_RETURN);
        $("#INFORETURN_REASON").val(self.selected().RETURN_REASON);
        ah.toggledisplayItemInfo();
        window.location.href = ah.config.url.openModalReturn;

    };
    self.select = function (itemsReceive) {
        self.selected(itemsReceive);
    };
    self.selected = ko.observable(self.itemsReceive()[0]);
    self.validatePrivileges = function (privilegeId) {
        var groupPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));

        var groupPrivileges = groupPrivileges.filter(function (item) { return item.PRIVILEGES_ID === privilegeId });
        return groupPrivileges.length;
    };
    self.calculateTotPrice = function () {

        var recQty = $("#INFOQTY").val();
        var salePrice = $("#INFOSALEPRICE").val();


        if (recQty != null && recQty != "" && salePrice != null && salePrice != "") {
            var totalPrice = recQty * salePrice;
            $("#INFOTOTAL_PRICE").val(totalPrice);
        }

    };

    self.dateClick = function () {
        $("#dateClickAction").val("ACTIVE");
    };

    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.createNew = function () {
        self.itemsReceive.splice(0, 5000);
        $("#TRANS_NO").text("");
        $("#TRANS_ID").val(null);
        var jDate = new Date();
        var strDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();


        ah.toggleAddMode();
        $("#TRANS_DATE").val(strDate);


    };

    self.addItems = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        $("#INFOTOTAL_PRICE").attr("disabled", "disabled");
        var storeId = $(ah.config.id.storeCURR).val().toString();
        if (storeId === null || storeId === "") {
            alert('Please select receiving store to proceed.');
            return;
        }

        ah.toggleSearchMode();
        ah.toggleSearchItems();
        $(ah.config.cls.liAddItem).show();
        postData = { SEARCH: "", ACTION: "", STORE: storeId, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchStockItems, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = ah.config.url.modalAddItems;

    };

    self.searchPurchaseReq = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));


        ah.toggleSearchMode();
        ah.toggleSearchItems();
        postData = { SEARCH_FILTER: self.searchFilter, SEARCH: "", RETACTION: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchPurchaseReq, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = ah.config.url.modalOpenPurchase;

    };
    self.pageLoader = function () {
        var senderID = $(arguments[1].currentTarget).attr('id');
        var searchStr = $(ah.config.id.txtGlobalSearchOth).val().toString();
        var pageNum = parseInt($(ah.config.id.pageNum).val());

        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.productItems));
        var lastPage = Math.ceil(sr.length / 10);

        var pageCount = parseInt($(ah.config.id.pageCount).val());
        if (isNaN(pageCount)) {
            pageCount = 1;
        }
        if (isNaN(pageNum)) {
            pageNum = 0;
        }
        switch (senderID) {

            case ah.config.tagId.nextPage: {
                if (pageCount < lastPage) {
                    pageNum = pageNum + 10;
                    pageCount++;
                    self.searchItemsResult(searchStr, pageNum);
                }
                break;
            }
            case ah.config.tagId.priorPage: {

                if (pageNum === 0) {
                } else {
                    pageNum = pageNum - 10;
                    if (pageNum < 0) {
                        pageNum = 0;
                    }
                    pageCount = pageCount - 1;
                }

                self.searchItemsResult(searchStr, pageNum);
                break;
            }

            case ah.config.tagId.txtGlobalSearchOth: {
                pageCount = 1;
                pageNum = 0;
                self.searchItemsResult(searchStr, 0);
                break;
            }
        }

        $(ah.config.id.loadNumber).text(pageCount.toString());
        $(ah.config.id.pageCount).val(pageCount.toString());
        $(ah.config.id.pageNum).val(pageNum);

    };
    self.pageLoaderPurchase = function () {
        var senderID = $(arguments[1].currentTarget).attr('id');
        var searchStr = $(ah.config.id.txtGlobalSearchOth1).val().toString();
        var pageNum = parseInt($(ah.config.id.pageNum1).val());

        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.purchaseList));
        var lastPage = Math.ceil(sr.length / 15);

        var pageCount = parseInt($(ah.config.id.pageCount1).val());
        if (isNaN(pageCount)) {
            pageCount = 1;
        }
        if (isNaN(pageNum)) {
            pageNum = 0;
        }
        switch (senderID) {

            case ah.config.tagId.nextPage1: {
                if (pageCount < lastPage) {
                    pageNum = pageNum + 15;
                    pageCount++;
                    self.searchPurchaseResult(searchStr, pageNum);
                }
                break;
            }
            case ah.config.tagId.priorPage1: {

                if (pageNum === 0) {
                } else {
                    pageNum = pageNum - 15;
                    if (pageNum < 0) {
                        pageNum = 0;
                    }
                    pageCount = pageCount - 1;
                }

                self.searchPurchaseResult(searchStr, pageNum);
                break;
            }

            case ah.config.tagId.txtGlobalSearchOth1: {
                pageCount = 1;
                pageNum = 0;
                self.searchPurchaseResult(searchStr, 0);
                break;
            }
        }

        $(ah.config.id.loadNumber1).text(pageCount.toString());
        $(ah.config.id.pageCount1).val(pageCount.toString());
        $(ah.config.id.pageNum1).val(pageNum);

    };
    self.modifySetup = function () {

        ah.toggleEditMode();

    };

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.saveAllChanges = function () {

        $(ah.config.id.saveInfo).trigger('click');

    };
    self.postAllChanges = function () {
        $("#POSTID").val("POST");

        self.saveInfo();
    };

    self.cancelChanges = function () {
        //var refId = $("#TRANS_ID").val();
        //if (refId === null || refId === "" || refId ==="0") {
        ah.initializeLayout();
        //} else {
        //ah.toggleDisplayMode();
        //self.showDebtorDetails();
        //}

    };

    self.showDetails = function (isNotUpdate) {
        ah.toggleOpenDisplayMode();
        var storeDetails = sessionStorage.getItem(ah.config.skey.storeDetails);
        var pInfo = JSON.parse(sessionStorage.getItem(ah.config.skey.productStockBatch));
        if (pInfo.length > 0) {
            $("#TRANS_NO").text(pInfo[0].TRANS_ID);
            $("#TRANS_ID").val(pInfo[0].TRANS_ID);

            if (pInfo[0].STATUS === "RECEIVED") {
                $(ah.config.cls.liSave + ',' + ah.config.id.contentSubHeader + ',' + ah.config.id.btnModify + ',' + ah.config.id.btnRemove).hide();
            } else {
                $(ah.config.cls.liSave + ',' + ah.config.id.btnModify).show();
                $(ah.config.id.contentSubHeader).hide();
            }
        }
        $(ah.config.cls.liApiStatus).hide();

    };

    self.searchResult = function (jsonData) {
       
        var sr = jsonData.PRODUCT_STOCKBATCH_LIST;


        $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

        if (sr.length > 0) {
            var htmlstr = '';

            for (var o in sr) {
                var transDate = new Date(sr[o].TRANS_DATE);
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].TRANS_ID, 'Trans.ID ' + sr[o].TRANS_ID, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;         Trans. Date: ' + sr[o].TRANS_DATE, ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      Purchase No: ' + sr[o].PURCHASE_NO + ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      Total Items: ' + sr[o].NOITEMS );
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.statusName, sr[o].STATUS);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.refWO, sr[o].REF_WO);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.supplier, sr[o].SUPPLIER_DESC);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplateSpareParts, sr[o].TRANS_ID, 'Spare Parts ');
                htmlstr += '</li>';

            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
        }

        $(ah.config.cls.detailItem).bind('click', self.readtransID);
        $(ah.config.cls.detailParts).bind('click', self.getspareParts);
        ah.displaySearchResult();

    };
    self.getspareParts = function () {
        var reqID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        ah.toggleSearchMode();
        postData = { ID_PARTS: "", TRANS_ID: reqID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readTransIDList, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.searchResultItems = function (jsonData) {
        ah.toggleReadMode();

        ah.initializeLayout();
        ah.toggleAddMode();
        $(ah.config.id.frmitemInfo)[0].reset();
        ah.toggledisplayItemInfo();
        $(ah.config.id.addButton).show();
        $(ah.config.id.updateButton).hide();
        self.itemsReceive.splice(0, 5000);


        var sr = jsonData.PRODUCT_STOCKBATCH;


        if (sr.length > 0) {
            $("#STORE").val(sr[0].STORE)
            $("#TRANS_NO").text(sr[0].TRANS_ID);
            $("#TRANS_ID").val(sr[0].TRANS_ID);
            var transDate = ah.formatJSONDateToString(sr[0].TRANS_DATE)
            $("#TRANS_DATE").val(transDate);
            $(ah.config.cls.divInfo).show();
            if (sr[0].PURCHASE_NO > 0) {
                $(ah.config.cls.liAddItem).hide();
                $("#divInfoText").html(' Receive from Purchase Order No:' + sr[0].PURCHASE_NO + '&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;       Trasaction Number:');
            } else {
                $("#divInfoText").html('Trasaction Number:');
            }


            for (var o in sr) {
                if (sr[o].PURCHASE_NO === null) {
                    sr[o].PURCHASE_NO = 0;
                }

                var expiryDateCurr = ah.formatJSONDateToStringOth(sr[o].EXPIRY_DATE);
                var expiryDate = ah.formatJSONDateToString(sr[o].EXPIRY_DATE);
                if (sr[o].QTY_RETURN === null || sr[o].QTY_RETURN === "") {
                    sr[o].QTY_RETURN = 0;
                }
                self.itemsReceive.push({
                    BATCH_ID: sr[o].BATCH_ID,
                    PRODUCT_DESC: sr[o].PRODUCT_DESC,
                    PRODUCT_CODE: sr[o].PRODUCT_CODE,
                    ID_PARTS: sr[o].ID_PARTS,
                    TRANS_ID: sr[0].TRANS_ID,
                    MIN: sr[o].MIN,
                    MAX: sr[o].MAX,
                    STATUS: sr[o].STATUS,
                    PRICE: sr[o].PRICE,
                    PRICE_DISCOUNT: sr[o].PRICE_DISCOUNT,
                    QTY: sr[o].QTY,
                    TOTAL_PRICE: sr[o].TOTAL_PRICE,
                    BARCODE_ID: sr[o].BARCODE_ID,
                    STORE: sr[o].STORE,
                    EXPIRY_DATE: expiryDateCurr,
                    STOREDESC: sr[o].STOREDESC,
                    SUPPLIER: sr[o].SUPPLIER,
                    PURCHASE_NO: sr[o].PURCHASE_NO,
                    QTY_RECEIVED: sr[o].QTY,
                    QTY_RETURN: sr[o].QTY_RETURN,
                    RETURN_REASON: sr[o].RETURN_REASON
                });
               
            }
        }
       
        if (sr[0].STATUS === "RECEIVED") {
            $(ah.config.cls.liAddItem + ',' + ah.config.cls.liSave + ',' + ah.config.id.contentSubHeader + ',' + ah.config.id.btnModify + ',' + ah.config.id.btnRemove).hide();
            //+ ',' + ah.config.id.btnRemove

            $(ah.config.id.contentHeader).html(systemText.addModeHeader);


        } else {
            $(ah.config.cls.liSave + ',' + ah.config.id.btnModify).show();
            $(ah.config.id.contentSubHeader).hide();
            $("#btnReturn").hide();
        }
        if (self.validatePrivileges('28') === 0) {
            $("#btnReturn").hide();
        } else {
            $("#btnReturn").show();
        }

    };

    self.searchItemsResult = function (LovCategory, pageLoad) {

        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.productItems));

        if (LovCategory !== null && LovCategory !== "") {
            var arr = sr;
            var keywords = LovCategory.toUpperCase();
            var filtered = [];

            for (var i = 0; i < sr.length; i++) {
                for (var j = 0; j < keywords.length; j++) {
                    if ((sr[i].DESCRIPTION.toUpperCase()).indexOf(keywords) > -1 || (sr[i].ID_PARTS.toUpperCase()).indexOf(keywords) > -1) {
                        filtered.push(sr[i]);
                        break;
                    }
                }
            }
            sr = filtered;
        }

        $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).html('');
        if (sr.length > 0) {
            $("#noresult").hide();
            $("#pageSeparator").show();
            $("#page1").show();
            $("#page2").show();
            $("#nextPage").show();
            $("#priorPage").show();
            var totalrecords = sr.length;
            var pagenum = Math.ceil(totalrecords / 10);
            $(ah.config.id.totNumber).text(pagenum.toString());
            if (pagenum <= 1) {
                $("#page1").hide();
                $("#page2").hide();
                $("#pageSeparator").hide();
                $("#nextPage").hide();
                $("#priorPage").hide();
            }
            var htmlstr = '';
            var o = pageLoad;

            for (var i = 0; i < 10; i++) {
                htmlstr += '<li>';
                htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                if (sr[o].QTY !== null) {
                    htmlstr += ah.formatString(ah.config.html.searchExistRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].ID_PARTS, sr[o].DESCRIPTION, "Stock on Hand:" + sr[o].QTY);
                } else {
                    htmlstr += ah.formatString(ah.config.html.searchLOVRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].ID_PARTS, sr[o].DESCRIPTION, "");
                }
                htmlstr += '</span>';
                htmlstr += '</li>';
                o++
                if (totalrecords === o) {
                    break;
                }
            }
            //alert(htmlstr);
            $(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).append(htmlstr);
        }
        else {
            $("#page1").hide();
            $("#page2").hide();
            $("#pageSeparator").hide();
            $("#nextPage").hide();
            $("#priorPage").hide();
            $("#noresult").show();
        }
        $(ah.config.cls.productDetailItem).bind('click', self.addProduct);
        ah.toggleDisplayMode();
        ah.toggleAddItems();
    };
    self.searchResultPOPartsResult = function (jsonData) {
       
        var sr = jsonData.PRODUCT_STOCKBATCH;
        if (sr.length > 0) {
            self.partsWOList.splice(0, 5000);  
            for (var o in sr) {
                if (sr[o].PURCHASE_NO === null) {
                    sr[o].PURCHASE_NO = 0;
                }

                var expiryDateCurr = ah.formatJSONDateToStringOth(sr[o].EXPIRY_DATE);
                var expiryDate = ah.formatJSONDateToString(sr[o].EXPIRY_DATE);
                if (sr[o].QTY_RETURN === null || sr[o].QTY_RETURN === "") {
                    sr[o].QTY_RETURN = 0;
                }
                self.partsWOList.push({
                    BATCH_ID: sr[o].BATCH_ID,
                    PRODUCT_DESC: sr[o].PRODUCT_DESC,
                    PRODUCT_CODE: sr[o].PRODUCT_CODE,
                    ID_PARTS: sr[o].ID_PARTS,
                    TRANS_ID: sr[0].TRANS_ID,
                    MIN: sr[o].MIN,
                    MAX: sr[o].MAX,
                    STATUS: sr[o].STATUS,
                    PRICE: sr[o].PRICE,
                    PRICE_DISCOUNT: sr[o].PRICE_DISCOUNT,
                    QTY: sr[o].QTY,
                    TOTAL_PRICE: sr[o].TOTAL_PRICE,
                    BARCODE_ID: sr[o].BARCODE_ID,
                    STORE: sr[o].STORE,
                    EXPIRY_DATE: expiryDateCurr,
                    SUPPLIER: sr[o].SUPPLIER,
                    PURCHASE_NO: sr[o].PURCHASE_NO,
                    QTY_RECEIVED: sr[o].QTY_RECEIVED,
                    QTY_RETURN: sr[o].QTY_RETURN,
                    RETURN_REASON: sr[o].RETURN_REASON
                });
            }
            $(ah.config.cls.modalwidth3).show();
            window.location.href = ah.config.url.openModalPOParts;
            ah.displaySearchResult();
        }
        
    };
    self.searchPurchaseResult = function (LovCategory, pageLoad) {
        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.purchaseList));

        if (LovCategory !== null && LovCategory !== "") {
            var arr = sr;
            var keywords = LovCategory.toUpperCase();
            var filtered = [];

            for (var i = 0; i < sr.length; i++) {
                for (var j = 0; j < keywords.length; j++) {
                    if ((sr[i].PURCHASE_NO.toString()).indexOf(keywords) > -1) {
                        filtered.push(sr[i]);
                        break;
                    }
                }
            }
            sr = filtered;
        }

        $(ah.config.id.searchResult11 + ' ' + ah.config.tag.ul).html('');
        if (sr.length > 0) {
            $("#noresult1").hide();
            $("#pageSeparator1").show();
            $("#page11").show();
            $("#page21").show();
            $("#nextPage1").show();
            $("#priorPage1").show();
            var totalrecords = sr.length;
            var pagenum = Math.ceil(totalrecords / 15);
            $(ah.config.id.totNumber1).text(pagenum.toString());
            if (pagenum <= 1) {
                $("#page11").hide();
                $("#page21").hide();
                $("#pageSeparator1").hide();
                $("#nextPage1").hide();
                $("#priorPage1").hide();
            }
            var htmlstr = '';
            var o = pageLoad;

            for (var i = 0; i < 15; i++) {
                var transDate = new Date(sr[o].REQ_DATE);
                htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].PURCHASE_NO, 'Purchase Order No: ' + sr[o].PURCHASE_NO, 'Purchase Order Date: ' + sr[o].REQDATE, 'Total Items: ' + sr[o].ITEMSREQ);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.refWO, sr[o].REF_WO);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.supplier, sr[o].DESCRIPTION);
                htmlstr += '</li>';
                o++
                if (totalrecords === o) {
                    break;
                }
            }
            //alert(htmlstr);
            $(ah.config.id.searchResult11 + ' ' + ah.config.tag.ul).append(htmlstr);
        }
        else {
            $("#page11").hide();
            $("#page21").hide();
            $("#pageSeparator1").hide();
            $("#nextPage1").hide();
            $("#priorPage1").hide();
            $("#noresult1").show();
        }
        $(ah.config.cls.detailItem).bind('click', self.readPurchaseRequest);
        ah.toggleDisplayMode();
        ah.toggleAddItems();
    };

    self.readPurchaseRequest = function () {

        var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        postData = { PURCHASE_NO: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readPurchaseReq, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.addProduct = function () {
        var dataItemsInfo = JSON.parse(this.getAttribute(ah.config.attr.dataItems));
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        $(ah.config.id.frmitemInfo)[0].reset();
        ah.toggledisplayItemInfo();
        $(ah.config.id.addButton).show();
        $(ah.config.id.updateButton).hide();
        var storeId = $(ah.config.id.storeCURR).val().toString();
        $(ah.config.id.itemDescription).text(dataItemsInfo.DESCRIPTION);
        var existPart = self.checkExist(dataItemsInfo.ID_PARTS);
        if (existPart > 0) {
            alert('Unable to Proceed. The selected item was already exist.');
            window.location.href = ah.config.url.modalClose;
            return;
        }
        $(ah.config.id.infoStore).val(storeId);
        $(ah.config.id.infoProductId).val(dataItemsInfo.ID_PARTS);
        $(ah.config.id.infoProductDesc).val(dataItemsInfo.DESCRIPTION);
        if (dataItemsInfo.MIN === null || dataItemsInfo.MIN === "") {
            dataItemsInfo.MIN = 0;
        }
        if (dataItemsInfo.MAX === null || dataItemsInfo.MAX === "") {
            dataItemsInfo.MAX = 0;
        }
        $("#INFOMIN").val(dataItemsInfo.MIN);
        $("#INFOMAX").val(dataItemsInfo.MAX);
        document.getElementById("INFOSTORE").disabled = true;
        document.getElementById("INFOSUPPLIER").disabled = false;

        window.location.href = ah.config.url.modalAddItemsInfo;
        return;
        sessionStorage.removeItem("PRODUCT_STOCKBATCHPUSH");
        var appendData = JSON.parse(sessionStorage.getItem(ah.config.skey.productStockBatchPush));
        var initialTable = $("#initialTable").val();


        if (appendData === null || appendData === "") {
            var newAttribute = JSON.stringify({ "PRODUCT_DESC": dataItemsInfo.DESCRIPTION, "ID_PARTS": dataItemsInfo.ID_PARTS, "SUPPLIER": '', "STORE": storeId, "BARCODE_ID": '0', "QTY": '0', "PRICE": '0', "PRICE_DISCOUNT": '0', "STATUS": '', "TRANS_ID": '', "BARCODE_ID": '', "EXPIRY_DATE": '', "TRANS_DATE": '', "CREATED_BY": staffLogonSessions.STAFF_ID, "CREATED_DATE": '', "TOTAL_PRICE": '0' });
            var appendData = JSON.parse('[' + newAttribute + ']');
            sessionStorage.setItem(ah.config.skey.productStockBatchPush, JSON.stringify(appendData));
        }
        if (initialTable === 'N') {
            //var x = document.getElementById("itemBatchReceive").rows.length;
            self.addItemToReceive();
            //self.insertNewRow();
        } else {
            self.addItemToReceive();
            //self.populateData('INITIAL');
        }
        $("#initialTable").val("N");

    };
    self.uploadItem = function () {

    };
    self.updatePurchaseReturn = function () {
        var returnQty = $("#INFOQTY_RETURN").val();
        if (returnQty <= 0) {
            alert('Return quantity should not be zero...');
            return;
        }
        var receivedQty = $("#INFOQTY_R").val();
        if (returnQty > receivedQty) {
            alert('Return quantity should not be greater than received quantity.');
            return;
        }
        var returnReason = $("#INFORETURN_REASON").val();
        if (returnReason === "" || returnReason === null) {
            alert('Please provide return reason to proceed.');
            return;
        }
        table = document.getElementById("itemsReceiveBatch");
        var delRow = parseInt($("#delRow").val());
        var todelrow = parseInt(delRow) + 1;
        
        table.deleteRow(todelrow);
        self.itemsReceive.splice(delRow, 1);
        $("#delRow").val("");

        var storeDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.storeDetails));

        //var storeId = parseInt($("#STORE").val());
        //var storeDesc = "";
        //storeDetails = storeDetails.filter(function (item) { return item.STORE_ID === storeId });
        //if (storeDetails.length > 0) {
        //    storeDesc = storeDetails[0].DESCRIPTION;
        //}
        
        var sr = ah.ConvertFormToJSON($(ah.config.id.frmitemInfoReturn)[0]);
        self.itemsReceive.push({
            PRODUCT_DESC: sr.INFOPRODUCT_DESC_R,
            PRODUCT_CODE: sr.ID_PARTS,
            ID_PARTS: sr.ID_PARTS,
            BATCH_ID: sr.BATCH_ID,
            TRANS_ID: sr.TRANS_ID,
            MIN: sr.INFOMIN,
            MAX: sr.INFOMAX,
            PRICE: sr.INFOPRICE_R,
            PRICE_DISCOUNT: sr.INFOSALEPRICE_R,
            QTY: sr.INFOQTY_R,
            TOTAL_PRICE: sr.INFOTOTAL_PRICE_R,
            BARCODE_ID: sr.INFOBARCODE_ID_R,
            STORE: sr.STOREID,
            STATUS: sr.STATUS_R,
            SUPPLIER: sr.INFOSUPPLIER_R,
            PURCHASE_NO: sr.INFOPURCHASENO_R,
            QTY_RECEIVED: sr.INFOQTY_R,
            QTY_RETURN: sr.INFOQTY_RETURN
        });
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var itemDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        strDateStart = strDateStart + ' ' + hourmin;
        sr.RETURN_DATE = strDateStart;
        sr.PURCHASE_NO = sr.INFOPURCHASENO_R;
        sr.QTY_RETURN = sr.INFOQTY_RETURN;
        sr.RETURN_REASON = sr.INFORETURN_REASON;
        sr.RETURN_BY = staffLogonSessions.STAFF_ID
        postData = { PRODUCT_BATCH_V: sr, STAFF_LOGON_SESSIONS: staffLogonSessions };
        
       
        ah.toggleSaveMode();
        $.post(self.getApi() + ah.config.api.updatePurchaseReturn, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        window.location.href = ah.config.url.modalClose;
        $(ah.config.cls.liSave + ',' + ah.config.id.contentSubHeader + ',' + ah.config.id.btnModify + ',' + ah.config.id.btnRemove).hide();

    };
    self.insertNewRow = function () {
       
        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.productStockBatchPush));
       // alert(JSON.stringify(appendData));
        var tBatchReceive = document.getElementById('itemBatchReceive');       
        var len = tBatchReceive.rows.length;       
        var htmlstr = "";
        for (var o in sr) {
            
            htmlstr += "   <tr>";
            htmlstr += '       <td class="e"><span>' + len + '<input id="rowId" name="rowId" type ="hidden" class="required number input-oth a" value="' + len + '"></span></td>';
            htmlstr += '       <td class="c"><span>' + sr[o].PRODUCT_DESC + '</span><input id="PRODUCT_CODE" name="PRODUCT_CODE" type ="hidden" class="required number input-oth a" value="' + sr[o].ID_PARTS + '"></td>';
           
            htmlstr += '      <td><input data-bind="event: { change: searchNow } " id="PRICE" name="PRICE" type ="text" class="required number input-oth a" value="' + +sr[o].PRICE + '"></td>';
            htmlstr += '      <td><input id="PRICE_DISCOUNT" name="PRICE_DISCOUNT" type ="text" class="required number input-oth a" value="' + +sr[o].PRICE_DISCOUNT + '"></td>';
            htmlstr += '      <td><input id="QTY" name="QTY" type ="text" class="required number input-oth a" value="' + +sr[o].QTY + '"></td>';
            htmlstr += '       <td class="a"><span>' + 0 + '<input id="TOTAL_PRICE" name="TOTAL_PRICE" type ="hidden" class="required number input-oth a" value="' + sr[o].TOTAL_PRICE + '"></span></td>';
            htmlstr += '      <td class="c"><input id="BARCODE_ID" name="BARCODE_ID" type ="text" class="required" value="' + +sr[o].BARCODE_ID + '"></td>';
            htmlstr += '      <td><input class="datefield datefield tcal d" data-val="true" data-val-required="Date is required" pattern="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$"id="EXPIRY_DATE" name="EXPIRY_DATE" placeholder="dd/mm/yyyy" title="Date Format dd/mm/yyyy" /><br /></td>';
            htmlstr += '       <td><input id="CREATED_BY" name="CREATED_BY" type ="hidden" class="required" value="' + sr[o].CREATED_BY + '"><button class="button" onclick="deleteRow(this)"><i class="fa fa-trash"></button></td>';
            htmlstr += '       <td style="display:none;"><input id="STORE" name="STORE" type ="hidden" class="required number input-oth a" value="' + sr[o].STORE + '"></td>';
            htmlstr += "   </tr>";
        }
        $(ah.config.id.populateData + ' ' + ah.config.tag.table).append(htmlstr);
      //  $(ah.config.cls.productDetailItem).bind('click', self.deleteRow);
    };
    self.populateData = function (action) {
       
        var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.productStockBatchPush));
        //alert(JSON.stringify(sr));
        $(ah.config.id.populateData).html('');
        var htmlstr = '';
        htmlstr += '<table id="itemBatchReceive">';
        htmlstr += "<thead>";
        htmlstr += "   <tr>";
        htmlstr += "   <th class='e'>No.</th>";
        htmlstr += "   <th class='c'>Item Name</th>";
        
        htmlstr += "       <th class='a'>Price</th>";
        htmlstr += "       <th class='a'>Sale Price</th>";
        htmlstr += "       <th class='a'>QTY</th>";
        htmlstr += "       <th class='a'>Total Cost</th>";
        htmlstr += "       <th class='c'>Batch No.</th>";
        htmlstr += "       <th class='d'>Expiry Date</th>";
        htmlstr +="        <th />";
        htmlstr +="    </tr>";
        htmlstr +="</thead>";
        var rownum = 0;
        for (var o in sr) {
            rownum++
            htmlstr += "   <tr>";
            htmlstr += '        <td class="e"><span>' + rownum + '<input id="rowId" name="rowId" type ="hidden" class="required number input-oth a" value="' + rownum + '"></span></td>';
            htmlstr += '       <td class="c"><span>' + sr[o].PRODUCT_DESC + '</span><input id="PRODUCT_CODE" name="PRODUCT_CODE" type ="hidden" class="required number input-oth a" value="' + sr[o].ID_PARTS + '"></td>';
           
            htmlstr += '      <td><input data-bind="event: { change: searchNow } " id="PRICE" name="PRICE" type ="text" class="required number input-oth a" value="' + +sr[o].PRICE + '"></td>';
            htmlstr += '      <td><input id="PRICE_DISCOUNT" name="PRICE_DISCOUNT" type ="text" class="required number input-oth a" value="' + +sr[o].PRICE_DISCOUNT + '"></td>';
            htmlstr += '      <td><input id="QTY" name="QTY" type ="text" class="required number input-oth a" value="' + +sr[o].QTY + '"></td>';
            htmlstr += '       <td class="a"><span>' + 0 + '<input id="TOTAL_PRICE" name="TOTAL_PRICE" type ="hidden" class="required number input-oth a" value="' + sr[o].TOTAL_PRICE + '"></span></td>';
            htmlstr += '      <td class="c"><input id="BARCODE_ID" name="BARCODE_ID" type ="text" class="required" value="' + +sr[o].BARCODE_ID + '"></td>';
            htmlstr += '      <td><input class="datefield datefield tcal d" data-val="true" data-val-required="Date is required" pattern="^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$" id="EXPIRY_DATE" name="EXPIRY_DATE" placeholder="dd/mm/yyyy" title="Date Format dd/mm/yyyy" /><br /></td>';
            htmlstr += '       <td><input id="CREATED_BY" name="CREATED_BY" type ="hidden" class="required" value="' + sr[o].CREATED_BY + '"><button class="button" onclick="deleteRow(this)"><i class="fa fa-trash"></button></td>';
            htmlstr += '       <td style="display:none;"><input id="STORE" name="STORE" type ="hidden" class="required number input-oth a" value="' + sr[o].STORE + '"></td>';
            htmlstr += "   </tr>";
            htmlstr += "</table>";
        }
        $(ah.config.id.populateData).append(htmlstr);
       // $(ah.config.cls.productDetailItem).bind('click', self.deleteRow);

    };

    self.isValidDate = function (dateString, action) {
        // First check for the pattern
        var regex_date = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
        var regex_date2 = /^(\d{1,2})-(\d{1,2})-(\d{4})$/;
        if (!regex_date.test(dateString)) {
            if (!regex_date2.test(dateString)) {
                return 'Invalid';
            }
        }
        // Parse the date parts to integers
        var n = dateString.search("-");
        var parts = "";
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var newDateFormat = "";
        if (n <= 0) {
            parts = dateString.split("/");
            var day = parseInt(parts[0], 10);
            var month = parseInt(parts[1], 10);
            var year = parseInt(parts[2], 10);
            newDateFormat = dateString;
        } else {
            parts = dateString.split("-");
            var day = parseInt(parts[0], 10);
            var month = parseInt(parts[1], 10);
            var year = parseInt(parts[2], 10);
            newDateFormat = (format_two_digits(day) + '/' + format_two_digits(month) + '/' + year).trim();
            if (action === 'TRANS_DATE') {
                $("#TRANS_DATE").val(newDateFormat);
            } else if (action === 'INS') {
                $("#INSURANCE_EXPIRY").val(newDateFormat);
            } else if (action === 'ID') {
                $("#ID_EXPIRATION").val(newDateFormat);
            }
        }

        // Check the ranges of month and year
        if (year < 1000 || year > 3000 || month == 0 || month > 12) {
            return 'Invalid';
        }

        var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        // Adjust for leap years
        if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
            monthLength[1] = 29;
        }

        // Check the range of the day
        if (day > 0 && day <= monthLength[month - 1]) {
            return newDateFormat;
        } else {
            return 'Invalid';
        }
    };
    self.readtransID = function () {

        var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        postData = { ID_PARTS: "", TRANS_ID: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readTransIDList, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();


        var fromDate = $('#requestFromDate').val().split('/');
        if (fromDate.length == 3) {
            self.searchFilter.fromDate(fromDate[2] + "/" + fromDate[1] + "/" + fromDate[0]);
        }
        var toDate = $('#requestStartDate').val().split('/');
        if (toDate.length == 3) {
            self.searchFilter.toDate(toDate[2] + "/" + toDate[1] + "/" + toDate[0] + "  23:00:00.000");
        }

        postData = { SEARCH: $("#txtGlobalSearch").val(), STAFF_LOGON_SESSIONS: staffLogonSessions, SEARCH_FILTER: self.searchFilter };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };

    self.saveInfo = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var itemDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;
        
        var jsonObj = [];
        jsonObj = ko.utils.stringifyJson(self.itemsReceive);
        

		var receviceLists = JSON.parse(jsonObj);

		var storeId = $("#STORE").val();
		
		if (storeId === null || storeId === ""){
			alert('Unable to proceed. Please select Receiving Store.');
			return;
		}
		for (var x in receviceLists) {

			receviceLists[x].STORE = storeId;
		}

        if (receviceLists.length <= 0) {
            alert('Unable to proceed. No item to receive. Please click on Receive from Purchase or Add item then select parts to receive.')
            return;
        }

        var checkDet = receviceLists.filter(function (item) { return item.PRICE === null });
        if (checkDet.length > 0) {
            var infoA = "";
            infoA += 'Please Complete the details of the following parts: \n\n'
            for (var o in checkDet) {
                
                infoA += checkDet[o].PRODUCT_DESC +'\n';
            }
            alert(infoA);
            return;
        }

        var transDate = $("#TRANS_DATE").val();

        

        var jDate = new Date();
		var todayDate = new Date();
		var d = new Date();
		
		
		
        if (transDate !== null && transDate !== "") {
            var returnStr = self.isValidDate(transDate, 'TRANS_DATE');
            if (returnStr === 'Invalid') {
                alert('Unable to proceed, Please make sure that the date format provided in trasaction date is correct (dd/mm/yyyy)');
                return;
            }
            var tDate = transDate;
            tDate = tDate.substring(6, 10) + '-' + tDate.substring(3, 5) + '-' + tDate.substring(0, 2);
			jDate = new Date(tDate);
			var transDate = tDate.substring(6, 10) + '-' + tDate.substring(3, 5) + '-' + tDate.substring(0, 2);
			
		
        }
      
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        strDateStart = strDateStart + ' ' + hourmin;


        actionStr = $("#POSTID").val();
        var transId = $("#TRANS_ID").val();
       // alert(transId);
        if (transId === null || transId === "") {
            transId = 0;
        }
  
  
        postData = { TRANS_ID:transId, ACTION:actionStr, CREATE_DATE: strDateStart, PRODUCT_STOCKBATCH: receviceLists, STAFF_LOGON_SESSIONS: staffLogonSessions };
		//alert(JSON.stringify(postData));
		//return;
            ah.toggleSaveMode();
            $.post(self.getApi() + ah.config.api.createStockBatchReceive, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    //    }

        
    };

    self.webApiCallbackStatus = function (jsonData) {
         //alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            //window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.SUCCESSRETURN || jsonData.AM_PURCHASEREQUEST || jsonData.PRODUCT_STOCKBATCH_LIST || jsonData.PRODUCT_STOCKBATCH || jsonData.PRODUCT_SUPPLIERS || jsonData.PRODUCT_STOCKONHAND || jsonData.STORES || jsonData.STAFF || jsonData.SUCCESS) {
            // alert(ah.CurrentMode);
           
            if (ah.CurrentMode == ah.config.mode.save) {
               // alert(JSON.stringify(jsonData.PRODUCT_STOCKBATCH));
                if (jsonData.STORES) {
                    sessionStorage.setItem(ah.config.skey.storeDetails, JSON.stringify(jsonData.STORES));
                }
                if (jsonData.PRODUCT_STOCKBATCH) {
                    sessionStorage.setItem(ah.config.skey.productStockBatch, JSON.stringify(jsonData.PRODUCT_STOCKBATCH));
                }
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();

                if (jsonData.SUCCESS) {
                    
                    self.showDetails(false);
                } else if (jsonData.SUCCESSRETURN) {
                    ah.toggleOpenDisplayMode();
                    $(ah.config.cls.liSave + ',' + ah.config.id.contentSubHeader + ',' + ah.config.id.btnModify + ',' + ah.config.id.btnRemove).hide();
                }
                else {
                    
                   
                    self.showDetails(true);
                    
					//$("#TRANS_NO").text(jsonData.PRODUCT_STOCKBATCH[0].TRANS_ID);
                    //$(".liSaveAll ").hide();					
                }
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                //alert(JSON.stringify(jsonData));
                self.searchResultItems(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.add) {
                
                self.addItemfromPR(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.search) {
                if (jsonData.PRODUCT_STOCKONHAND) {
                    sessionStorage.setItem(ah.config.skey.productItems, JSON.stringify(jsonData.PRODUCT_STOCKONHAND));
                    $(ah.config.id.loadNumber).text("1");
                    $(ah.config.id.pageCount).val("1");
                    $(ah.config.id.pageNum).val(0);
                    self.searchItemsResult("", 0);
                } else if (jsonData.AM_PURCHASEREQUEST) {
                    sessionStorage.setItem(ah.config.skey.purchaseList, JSON.stringify(jsonData.AM_PURCHASEREQUEST));
                    $(ah.config.id.loadNumber1).text("1");
                    $(ah.config.id.pageCount1).val("1");
                    $(ah.config.id.pageNum1).val(0);
                    self.searchPurchaseResult("", 0);
                } else if (jsonData.PRODUCT_STOCKBATCH) {
                    self.searchResultPOPartsResult(jsonData);
                } else {
                    self.searchResult(jsonData);
                }
                
            }
            else if (ah.CurrentMode == ah.config.mode.searchResult) {
                self.searchResultPOPartsResult(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {
                var store = jsonData.STORES;
                sessionStorage.setItem(ah.config.skey.storeDetails, JSON.stringify(jsonData.STORES));
                $.each(store, function (item) {
                    $(ah.config.id.storeCURR)
                        .append($("<option></option>")
                        .attr("value", store[item].STORE_ID)
                        .text(store[item].DESCRIPTION));
                });

                $.each(store, function (item) {
                    $(ah.config.id.infoStore)
                        .append($("<option></option>")
                        .attr("value", store[item].STORE_ID)
                        .text(store[item].DESCRIPTION));
                });

                var supplier = jsonData.PRODUCT_SUPPLIERS;
                $.each(supplier, function (item) {
                    $(ah.config.id.supplierCURR)
                        .append($("<option></option>")
                        .attr("value", supplier[item].SUPPLIER_ID)
                        .text(supplier[item].DESCRIPTION));
                });
                $.each(supplier, function (item) {
                    $(ah.config.id.infoSupplier_r)
                        .append($("<option></option>")
                            .attr("value", supplier[item].SUPPLIER_ID)
                            .text(supplier[item].DESCRIPTION));
                });
                

                $.each(supplier, function (item) {
                    $(ah.config.id.infoSupplier)
                        .append($("<option></option>")
                        .attr("value", supplier[item].SUPPLIER_ID)
                        .text(supplier[item].DESCRIPTION));
                });
                

            }

        }
        else {

            alert(systemText.errorTwo);
            //window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};