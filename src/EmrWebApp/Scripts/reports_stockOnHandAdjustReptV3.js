﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            statusText: '.status-text',
            detailItem: '.detailItem',
            liPrint: '.liPrint'
        },
        tag: {

            textArea: 'textarea',
            select: 'select',
            tbody: 'tbody',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7,
            print: 8
        },
        tagId: {
            tcCode: 'tcCode',
            tcInService: 'tcInService',
            tcDescription: 'tcDescription',
            tcActive: 'tcActive',
            tcMissing: 'tcMissing',
            tcInActive: 'tcInActive',
            tcOtService: 'tcOtService',
            tcRetired: 'tcRetired',
            tcTransfer: 'tcTransfer',
            tcUndifined: 'tcUndifined'
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveInfo: '#saveInfo',
            searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            searchResultSummaryCount: "#searchResultSummaryCount",
            txtGlobalSearch: '#txtGlobalSearch',
            staffID: '#STAFF_ID',
            serviceID: '#SERVICE_ID',
            successMessage: '#successMessage',
            printSummary: '#printSummary',
            printSearchResultList: '#printSearchResultList',
            loadingBackground: '#loadingBackground'
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfoId: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            searchResult: 'AM_ASSETSUMMARY',

        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalClose: '#',
            modalFilter: '#openModalFilter'
        },
        api: {
            searchAll: '/Reports/SearchSOHAdjustRept',
            readClientInfo: '/Setup/ReadClientInfo'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
            searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(
            thisApp.config.cls.liPrint + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };

    thisApp.toggleSearchMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.search;

        $(thisApp.config.cls.statusText).text(systemText.searchStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();

    };

    thisApp.displaySearchResult = function () {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;

        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        $(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave).hide();

    };

    thisApp.ConvertFormToJSON = function (form) {
        $(form).find("input:disabled").removeAttr("disabled");
        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name),
                type = $el.attr('type');

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var stockOnHandReptViewModel = function (systemText) {

    var self = this;

    var ah = new appHelper(systemText);

    self.assetCategory = {
        count: ko.observable()
    };
    self.clientDetails = {
        CLIENTNAME: ko.observable(),
        CLIENT_LOGO: ko.observable(),
        COMPANY_LOGO: ko.observable(),
        HEADING: ko.observable(),
        HEADING2: ko.observable(),
        HEADING3: ko.observable(),
        HEADING_OTH: ko.observable(),
        HEADING_OTH2: ko.observable(),
        HEADING_OTH3: ko.observable(),
        CLIENTADDRESS: ko.observable(),
        CLIENTCONTACT: ko.observable()
    };
    self.searchFilter = {
        fromDate: ko.observable(''),
        toDate: ko.observable('')

    }
    self.showLoadingBackground = function () {
        var loadingBackground = $(ah.config.id.loadingBackground);
        loadingBackground.addClass('initializing').removeClass('done');
        var offset = loadingBackground.offset();
        var height = loadingBackground.height();
        var centerY = (offset.top + height / 2) - 120;
        $('.sk-circle').css({
            'margin-top': centerY + 'px',
            'display': 'block'
        });
    };
    self.getPageData = function () {
        
    };
    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());
        
        ah.initializeLayout();
        var today = moment(new Date()).format("DD/MM/YYYY");

        $('#requestFromDate').val(today);
        $('#requestStartDate').val(today);

        self.showLoadingBackground();

        $.when(self.getPageData()).done(function () {
            $(ah.config.id.loadingBackground).removeClass('initializing').addClass('done');
            $('.dark-bg, ' + ah.config.id.loadingBackground).css({
                'top': '90px',
                'z-index': '0'
            });
        });
    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };

    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.createNew = function () {

        ah.toggleAddMode();

    };
    self.searchFilterNow = function () {
        window.location.href = ah.config.url.modalClose;
        self.searchNow();
    }
    self.showModalFilter = function () {
        window.location.href = ah.config.url.modalFilter;
    };
    self.clearFilter = function () {

        $('#requestFromDate').val('');
        $('#requestStartDate').val('');
        self.searchFilter.fromDate = '';
        self.searchFilter.toDate = '';
    }
    self.modifySetup = function () {

        ah.toggleEditMode();

    };

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };
    
    self.printSummary = function () {
        ah.CurrentMode = ah.config.mode.print;
        document.getElementById("printSummary").classList.add('printSummary');
        var jsonObj = ko.observableArray();


        if (self.clientDetails.CLIENTNAME() != null) {
            self.print();

        } else {
            var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var postData;

            var staffInfo = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
            postData = { CLIENTID: staffInfo.CLIENT_CODE, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.readClientInfo, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
    };
    self.print = function () {

        setTimeout(function () {
            window.print();

        }, 2000);
    };
    self.sortDataTable = function (tableReference, sortColumnIndex, ascORdesc) {
        $(tableReference)
            .unbind('appendCache applyWidgetId applyWidgets sorton update updateCell')
            .removeClass('tablesorter')
            .find('thead th')
            .unbind('click mousedown')
            .removeClass('header headerSortDown headerSortUp');

        var sortedAt = 0;
        if (ascORdesc == "asc" || ascORdesc == "ASC") {
            sortedAt = 0;
        } else {
            sortedAt = 1;
        }

        $(tableReference).tablesorter({
            sortList: [[sortColumnIndex, sortedAt]],
            headers: {
                10: { sorter: "customDate" }
            }
        });

        $('.not-sorted').removeClass('header').removeClass('headerSortUp').removeClass('headerSortDown');
    }
    self.searchResult = function (jsonData) {


        var sr = jsonData.PURCHASEREQ_REPTLIST

        $(ah.config.id.searchResultList + ' ' + ah.config.tag.tbody).html('');

        var searchCat = $("#sumCategory").val();
        
        $(ah.config.id.searchResultSummaryCount).text("Number of Records: " + sr.length);
        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString()));
        
        self.assetCategory.count("No. of Records: " + sr.length.toString());

        $(ah.config.cls.liPrint).hide();
        if (sr.length > 0) {
            $(ah.config.cls.liPrint).show();
            var htmlstr = '';

            for (var o in sr) {
                htmlstr += "<tr>";
                if (sr[o].PO_STATUS === null || sr[o].PO_STATUS === "") {
                    sr[o].PO_STATUS = '-';
                }
                if (sr[o].PO_STATUS === null || sr[o].PO_STATUS === "") {
                    sr[o].PO_STATUS = '-';
                }
                if (sr[o].PURCHASE_NO === null || sr[o].PURCHASE_NO === 0) {
                    sr[o].PURCHASE_NO = '-';
                    sr[o].PURCHASE_QTY = '-';
                    sr[o].PURCHASE_UNITPRICE = '-';
                    sr[o].SUPPLIERDESC = '-';
                    sr[o].QTY_RECEIVED = '-';
                }
                if (sr[o].STATUS_ACTUAL === 'Y') {
                    sr[o].STATUS_ACTUAL = 'Active';
                } else {
                    sr[o].STATUS_ACTUAL = 'Inactive';
                }
                if (sr[o].STATUS_ADJUST === 'Y') {
                    sr[o].STATUS_ADJUST = 'Active';
                } else {
                    sr[o].STATUS_ADJUST = 'Inactive';
                }
                htmlstr += '<td class="d">' + sr[o].ID_PARTS + '</td>';
                htmlstr += '<td class="h">' + sr[o].DESCRIPTION + '</td>';
                htmlstr += '<td class="a">' + sr[o].QTY_ACTUAL + '</td>';
                htmlstr += '<td class="a">' + sr[o].QTY_ADJUST + '</td>';
                htmlstr += '<td class="a">' + sr[o].MIN_ACTUAL + '</td>';
                htmlstr += '<td class="a">' + sr[o].MIN_ADJUST + '</td>';
                htmlstr += '<td class="a">' + sr[o].MAX_ACTUAL + '</td>';
                htmlstr += '<td class="a">' + sr[o].MAX_ADJUST + '</td>';
                htmlstr += '<td class="a">' + sr[o].STATUS_ACTUAL + '</td>';
                htmlstr += '<td class="a">' + sr[o].STATUS_ADJUST + '</td>';
                htmlstr += '<td class="a">' + sr[o].CREATED_DATE + '</td>';
                htmlstr += '<td class="h">' + sr[o].DONEDESC + '</td>';
                htmlstr += '<td class="h">' + sr[o].REASON + '</td>';
                htmlstr += "</tr>";
            }

            $(ah.config.id.searchResultList + ' ' + ah.config.tag.tbody).append(htmlstr);
            $(ah.config.id.printSearchResultList).html('');
            $(ah.config.id.printSearchResultList).append($(ah.config.id.searchResultList).html());

            var tableReference = ah.config.id.searchResultList + ' table';
            self.sortDataTable(tableReference, 1, 'asc');
        }


        ah.displaySearchResult();

    };

    sortArray = function () {

    }

    self.readSetupID = function () {

        var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();

        //postData = { SUPPLIER_ID: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        //$.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.dateClick = function () {
        $("#dateClickAction").val("ACTIVE");
    };
    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var postData;

        ah.toggleSearchMode();

        var fromDate = $('#requestFromDate').val();

        fromDate = fromDate.split('/');
        if (fromDate.length == 3) {
            self.searchFilter.fromDate(fromDate[2] + "/" + fromDate[1] + "/" + fromDate[0]);
        }
        var toDate = $('#requestStartDate').val().split('/');
        if (toDate.length == 3) {
            self.searchFilter.toDate(toDate[2] + "/" + toDate[1] + "/" + toDate[0] + "  23:00:00.000");
        }

        self.showLoadingBackground();
        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), SEARCH_FILTER: self.searchFilter, RETACTION: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.when($.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus)).done(function () {
            $(ah.config.id.loadingBackground).removeClass('initializing').addClass('done');
        });
    };


    self.webApiCallbackStatus = function (jsonData) {

        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));


        }
        else if (jsonData.AM_CLIENT_INFO || jsonData.PURCHASEREQ_REPTLIST || jsonData.STAFF || jsonData.SUCCESS) {
            if (ah.CurrentMode == ah.config.mode.search) {

                self.searchResult(jsonData);
            }
            else if (ah.CurrentMode == ah.config.mode.print) {

                var clientInfo = jsonData.AM_CLIENT_INFO;

                self.clientDetails.CLIENTNAME(clientInfo.DESCRIPTION);
                self.clientDetails.CLIENT_LOGO(clientInfo.CLIENT_LOGO);
                self.clientDetails.COMPANY_LOGO(clientInfo.COMPANY_LOGO);
                self.clientDetails.HEADING(clientInfo.HEADING);
                self.clientDetails.HEADING2(clientInfo.HEADING2);
                self.clientDetails.HEADING3(clientInfo.HEADING3);
                self.clientDetails.HEADING_OTH(clientInfo.HEADING_OTH);
                self.clientDetails.HEADING_OTH2(clientInfo.HEADING_OTH2);
                self.clientDetails.HEADING_OTH3(clientInfo.HEADING_OTH3);
                self.clientDetails.CLIENTADDRESS(clientInfo.ADDRESS + ', ' + clientInfo.CITY + ', ' + clientInfo.ZIPCODE + ', ' + clientInfo.STATE);
                self.clientDetails.CLIENTCONTACT('Tel.No: ' + clientInfo.CONTACT_NO1 + ' Fax No:' + clientInfo.CONTACT_NO2);

                self.print();
            }

        }
        else {

            alert(systemText.errorTwo);
            //	window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};