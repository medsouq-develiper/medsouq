﻿var appHelper = function (systemText) {

	var thisApp = this;

	thisApp.config = {
		cls: {
			contentField: '.field-content-detail',
			contentSearch: '.search-content-detail',
			contentHome: '.home-content-detail',
			liModify: '.liModify',
			liSave: '.liSaveAll',
			liAddNew: '.liAddNew',
			divProgress: '.divProgressScreen',
			liApiStatus: '.liApiStatus',
			notifySuccess: '.notify-success',
			toolBoxRight: '.toolbox-right',
			statusText: '.status-text',
            detailItem: '.detailItem',
            liPrint: '.liPrint'
		},
		tag: {
			
			textArea: 'textarea',
			select: 'select',
			tbody: 'tbody',
			ul: 'ul'
		},
		mode: {
			idle: 0,
			add: 1,
			search: 2,
			searchResult: 3,
			save: 4,
			read: 5,
			display: 6,
            edit: 7,
            advanceSearch: 8,
            print: 9
		},
		tagId: {
			tcCode: 'tcCode',
			tcInService: 'tcInService',
			tcDescription: 'tcDescription',
			//tcActive: 'tcActive',
			tcMissing: 'tcMissing',
			tcInActive: 'tcInActive',
			tcOtService: 'tcOtService',
			tcRetired: 'tcRetired',
			tcTransfer: 'tcTransfer',
			tcUndifined: 'tcUndifined'
		},
		id: {
			spanUser: '#spanUser',
			contentHeader: '#contentHeader',
			contentSubHeader: '#contentSubHeader',
			saveInfo: '#saveInfo',
			searchResultList: '#searchResultList',
            searchResultSummary: '#searchResultSummary',
            searchResultSummaryCount: "#searchResultSummaryCount",
			txtGlobalSearch: '#txtGlobalSearch',
			staffID: '#STAFF_ID',
			serviceID: '#SERVICE_ID',
            successMessage: '#successMessage',
            costcenterStrList: '#costcenterStrList',
            manufacturerStrList: '#manufacturerStrList',
            supplierStrList: '#supplierStrList',
            modalClose: '#',
            assetCondition: '#filterCondition',
            printSummary: '#printSummary',
            printSearchResultList: '#printSearchResultList',
            loadingBackground: '#loadingBackground'
        },
        fld: {
            assetNoInfo: 'ASSETNO'
        },
		cssCls: {
			viewMode: 'view-mode'
		},
		attr: {
			readOnly: 'readonly',
			disabled: 'disabled',
			selectedIndex: 'selectedIndex',
			datainfoId: 'data-infoID'
		},
		skey: {
			staff: 'STAFF',
			webApiUrl: 'webApiURL',
			staffLogonSessions: 'STAFF_LOGON_SESSIONS',
			searchResult: 'AM_ASSETSUMMARY',
			
		},
		url: {
			homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalFilter: '#openModalFilter'
		},
		api: {
			searchDeviceTypeSum: '/Reports/SearchAssetByDeviceTypeSum',
			searchACostCenterSum: '/Reports/SearchAssetByCostCenterSum',
			searchResponsibleSum: '/Reports/SearchAssetByResponsibleCenterSum',
			searchManufacturerSum: '/Reports/SearchAssetByManufacturerSum',
            searchSupplierSum: '/Reports/SearchAssetBySupplierSum',
            searchLov: '/AIMS/SearchAimsLOV',
            readClientInfo: '/Setup/ReadClientInfo'
		},
		html: {
			searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
			searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>",
		}
	};

	thisApp.CurrentMode = thisApp.config.mode.idle;

	thisApp.ResetControls = function () {

		$(thisApp.config.tag.input).val('');
		$(thisApp.config.tag.textArea).val('');
		$(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

	};

	thisApp.initializeLayout = function () {

		thisApp.ResetControls();

		$(
            thisApp.config.cls.liPrint + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
			thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
			thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess
		).hide();

		$(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

		thisApp.CurrentMode = thisApp.config.mode.idle;

	};
	
	thisApp.toggleSearchMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.search;

		$(thisApp.config.cls.statusText).text(systemText.searchStatusText);
		$(thisApp.config.cls.liApiStatus).show();

		$(
			thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
			thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
		).hide();

	};

	thisApp.displaySearchResult = function () {

		thisApp.CurrentMode = thisApp.config.mode.searchResult;

		$(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
		$(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave).hide();

	};
	
	thisApp.ConvertFormToJSON = function (form) {
		$(form).find("input:disabled").removeAttr("disabled");
		var array = jQuery(form).serializeArray();
		var json = {};

		jQuery.each(array, function () {
			json[this.name] = this.value || '';
		});

		return json;

	};

	thisApp.formatString = function () {

		var stringToFormat = arguments[0];

		for (var i = 0; i <= arguments.length - 2; i++) {
			stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
		}

		return stringToFormat;
	};

	thisApp.LoadJSON = function (jsonData) {

		$.each(jsonData, function (name, val) {
			var $el = $('#' + name),
				type = $el.attr('type');

			switch (type) {
				case 'checkbox':
					$el.attr('checked', 'checked');
					break;
				case 'radio':
					$el.filter('[value="' + val + '"]').attr('checked', 'checked');
					break;
				default:
					var tagNameLCase = String($el.prop('tagName')).toLowerCase();
					if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
						$el.text(val);
					} else {
						$el.val(val);
					}
			}
		});

    };

    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var assetReptSummaryViewModel = function (systemText) {

	var self = this;

    var ah = new appHelper(systemText);

    var lovCostCenterArray = ko.observableArray([]);
    var lovManufacturerArray = ko.observableArray([]);
    var lovSupplierArray = ko.observableArray([]);
    var resultHtml = null;

    self.showLoadingBackground = function () {
        var loadingBackground = $(ah.config.id.loadingBackground);
        loadingBackground.addClass('initializing').removeClass('done');
        var offset = loadingBackground.offset();
        var height = loadingBackground.height();
        var centerY = (offset.top + height / 2) - 120;
        $('.sk-circle').css({
            'margin-top': centerY + 'px',
            'display': 'block'
        });
    };
    self.getPageData = function () {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
        var postData;
        
        postData = { LOV: "'AIMS_COSTCENTER','AIMS-CONDITION'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        return $.post(self.getApi() + ah.config.api.searchLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
	self.initialize = function () {

		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

		ah.initializeLayout();

        self.showLoadingBackground();

        $.when(self.getPageData()).done(function () {
            $(ah.config.id.loadingBackground).removeClass('initializing').addClass('done');
            $('.dark-bg, ' + ah.config.id.loadingBackground).css({
                'top': '90px',
                'z-index': '0'
            });
        });
	};
	self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
	};

	self.returnHome = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		window.location.href = staffDetails.HOME_DEFAULT;
	};
	self.createNew = function () {

		ah.toggleAddMode();

    };


    self.getLastSearch = function () {
        SsearchTxt = $("#txtGlobalSearch").val();
        Scondition = $("#filterCondition").val();
        Sstatus = $("#filterStatus").val();
        Scloneid = $("#cloneid").val();
        Scostcenter = $("#costcenterStr").val();

    };

    self.assetCategory = {
        title: ko.observable(),
        count: ko.observable()
    };
    self.clientDetails = {
        CLIENTNAME: ko.observable(),
        CLIENT_LOGO: ko.observable(),
        COMPANY_LOGO: ko.observable(),
        HEADING: ko.observable(),
        HEADING2: ko.observable(),
        HEADING3: ko.observable(),
        HEADING_OTH: ko.observable(),
        HEADING_OTH2: ko.observable(),
        HEADING_OTH3: ko.observable(),
        CLIENTADDRESS: ko.observable(),
        CLIENTCONTACT: ko.observable()
    };
    self.clearFilter = function () {
        self.searchFilter.AssetFilterCondition('');
        self.searchFilter.toDate('');
        self.searchFilter.status('');
        self.searchFilter.CostCenterStr('');
        self.searchFilter.ManufacturerStr('');
        self.searchFilter.SupplierStr('');
    };
    self.searchFilterNow = function () {
        //window.location.href = ah.config.url.modalClose;
        window.location.href = '#';
        self.searchNow();
    };
    self.searchFilter = {
        AssetFilterCondition: ko.observable(''),
        toDate: ko.observable(''),
        status: ko.observable(''),
        staffId: ko.observable(''),
        CostCenterStr: ko.observable(''),
        ManufacturerStr: ko.observable(''),
        SupplierStr: ko.observable('')
    };
    self.showModalFilter = function () {
        ah.CurrentMode = ah.config.mode.advanceSearch;

        var element2 = document.getElementById("costcenterStr");
        element2.classList.remove("view-mode");
        element2.disabled = false;

        var element3 = document.getElementById("filterStatus");
        element3.classList.remove("view-mode");
        element3.disabled = false;

        var element4 = document.getElementById("filterCondition");
        element4.classList.remove("view-mode");
        element4.disabled = false;

        var element5 = document.getElementById("manufacturerStr");
        element5.classList.remove("view-mode");
        element5.disabled = false;

        var element6 = document.getElementById("supplierStr");
        element6.classList.remove("view-mode");
        element6.disabled = false;

        window.location.href = ah.config.url.modalFilter;
    };
    self.printSummary = function () {
        ah.CurrentMode = ah.config.mode.print;
        document.getElementById("printSummary").classList.add('printSummary');
        var jsonObj = ko.observableArray();


        if (self.clientDetails.CLIENTNAME() != null) {
            self.print();

        } else {
            var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
            var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
            var postData;

            var staffInfo = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
            postData = { CLIENTID: staffInfo.CLIENT_CODE, STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.readClientInfo, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
    };
    self.print = function () {

        setTimeout(function () {
            window.print();

        }, 2000);
    };


	self.modifySetup = function () {

		ah.toggleEditMode();

	};

	self.signOut = function () {

		window.location.href = ah.config.url.homeIndex;

	};

	
	self.sortTable = function () {


		var senderID = $(arguments[1].currentTarget).attr('id');

		switch (senderID) {

			
			case ah.config.tagId.tcInService: {
				self.searchResult('INSERVICES');			
				break;
			}
			case ah.config.tagId.tcDescription: {
				self.searchResult('DESCRIPTION');
				break;
			}
			//case ah.config.tagId.tcActive: {
			//	self.searchResult('ACTIVE');
			//	break;
			//}
			case ah.config.tagId.tcMissing: {
				self.searchResult('MISSING');
				break;
			}
			case ah.config.tagId.tcInActive: {
				self.searchResult('INACTIVE');
				break;
			}
			case ah.config.tagId.tcOtService: {
				self.searchResult('OUTSERVICE');
				break;
			}
			case ah.config.tagId.tcRetired: {
				self.searchResult('RETIRED');
				break;
			}
			case ah.config.tagId.tcTransfer: {
				self.searchResult('TRANSFERED');
				break;
			}
			case ah.config.tagId.tcUndifined: {
				self.searchResult('UNDIFINED');
				break;
			}

		}

	
    };

	self.searchResult = function (sortDet) {
		var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.searchResult));
		
        $(ah.config.id.searchResultList + ' ' + ah.config.tag.tbody).html('');
        $(ah.config.id.printSearchResultList).html('');

		var searchCat = $("#sumCategory").val();
		
		if (searchCat === "CC") {
			systemText.searchModeHeader = "Summary Asset Report by Cost Center";
		} else if (searchCat === "RC") {
            systemText.searchModeHeader = "Summary Asset Report by Responsible Center";
		} else if (searchCat === "DT") {
            systemText.searchModeHeader = "Summary Asset Report by Device Type";
		} else if (searchCat === "M") {
            systemText.searchModeHeader = "Summary Asset Report by Manufacturer";
		} else if (searchCat === "S") {
            systemText.searchModeHeader = "Summary Asset Report by Supplier";
		}

        $(ah.config.id.searchResultSummaryCount).text("Number of Records: " + sr.length);
        $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString()));

        self.assetCategory.title(systemText.searchModeHeader.toString());
        self.assetCategory.count("No. of Records: " + sr.length.toString());
        $(ah.config.cls.liPrint).hide();

		if (sortDet === 'DESCRIPTION') {
			sr.sort(function (a, b) {

				if (a[sortDet] < b[sortDet])
					return -1;
				if (a[sortDet] > b[sortDet])
					return 1;
				return 0;
			});
		} else {
			sr.sort(function (a, b) {

				if (a[sortDet] > b[sortDet])
					return -1;
				if (a[sortDet] < b[sortDet])
					return 1;
				return 0;
			});
		}
		

        if (sr.length > 0) {
            $(ah.config.cls.liPrint).show();
			var htmlstr = '';

            for (var o in sr) {
                htmlstr += "<tr>";
                htmlstr += '<td class="d">' + sr[o].CODE + '</td>';
                htmlstr += '<td class="h">' + sr[o].DESCRIPTION + '</td>';
                //htmlstr += '<td class="a">' + sr[o].ACTIVE + '</td>';
                htmlstr += '<td class="a">' + sr[o].INSERVICES + '</td>';
                htmlstr += '<td class="a">' + sr[o].MISSING + '</td>';
                htmlstr += '<td class="a">' + sr[o].INACTIVE + '</td>';
                htmlstr += '<td class="a">' + sr[o].OUTSERVICE + '</td>';
                htmlstr += '<td class="a">' + sr[o].RETIRED + '</td>';
                htmlstr += '<td class="a">' + sr[o].TRANSFERED + '</td>';
                htmlstr += '<td class="a">' + sr[o].UNDIFINED + '</td>';
                htmlstr += "</tr>";
            }
            
            $(ah.config.id.searchResultList + ' ' + ah.config.tag.tbody).append(htmlstr);
            $(ah.config.id.printSearchResultList).html('');
            $(ah.config.id.printSearchResultList).append($(ah.config.id.searchResultList).html());
            
		}
		ah.displaySearchResult();
    };

	sortArray = function () {

	};

	self.readSetupID = function () {

		var infoID = this.getAttribute(ah.config.attr.datainfoId).toString();
		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var postData;

		ah.toggleReadMode();

		//postData = { SUPPLIER_ID: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
		//$.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

	};

	self.searchNow = function () {

		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		var postData;

		ah.toggleSearchMode();
		var searchCat = $("#sumCategory").val();
		var apiSearch = "";
		if (searchCat === "CC") {
			apiSearc = ah.config.api.searchACostCenterSum;
		} else if (searchCat === "RC") {
			apiSearc = ah.config.api.searchResponsibleSum;
		} else if (searchCat === "DT") {
			apiSearc = ah.config.api.searchDeviceTypeSum;
		} else if (searchCat === "M") {
			apiSearc = ah.config.api.searchManufacturerSum;
		} else if (searchCat === "S") {
			apiSearc = ah.config.api.searchSupplierSum;
        }

        self.showLoadingBackground();

        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), SEARCH_FILTER: self.searchFilter, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.when($.post(self.getApi() + apiSearc, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus)).done(function () {
            $(ah.config.id.loadingBackground).removeClass('initializing').addClass('done');
        });
	};


	self.webApiCallbackStatus = function (jsonData) {
        //alert(JSON.stringify(jsonData));
        //console.log(jsonData);
		if (jsonData.ERROR) {
			
				alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
		
        }
        else if (jsonData.AM_CLIENT_INFO || jsonData.AM_ASSETSUMMARY || jsonData.STAFF || jsonData.SUCCESS || jsonData.LOV_LOOKUPS || jsonData.AM_MANUFACTURER || jsonData.AM_SUPPLIER) {
            if (ah.CurrentMode == ah.config.mode.search) {
				sessionStorage.setItem(ah.config.skey.searchResult, JSON.stringify(jsonData.AM_ASSETSUMMARY));
				self.searchResult('DESCRIPTION');
			}
            else if (ah.CurrentMode == ah.config.mode.idle) {
                lovCostCenterArray = jsonData.LOV_LOOKUPS;

                var LOV_CostCenter = jsonData.LOV_LOOKUPS;
                LOV_CostCenter = LOV_CostCenter.filter(function (item) { return item.CATEGORY === 'AIMS_COSTCENTER' });
                $.each(LOV_CostCenter, function (item) {

                    $(ah.config.id.costcenterStrList)
                        .append($("<option>")
                            .attr("value", LOV_CostCenter[item].DESCRIPTION.trim())
                            .attr("id", LOV_CostCenter[item].LOV_LOOKUP_ID));
                });

                var assetConditionLOV = jsonData.LOV_LOOKUPS;
                assetConditionLOV = assetConditionLOV.filter(function (item) { return item.CATEGORY === 'AIMS-CONDITION' });

                $.each(assetConditionLOV, function (item) {

                    $(ah.config.id.assetCondition)
                        .append($("<option></option>")
                            .attr("value", assetConditionLOV[item].LOV_LOOKUP_ID)
                            .text(assetConditionLOV[item].DESCRIPTION));
                });

                lovManufacturerArray = jsonData.AM_MANUFACTURER;
                $.each(lovManufacturerArray, function (item) {

                    $(ah.config.id.manufacturerStrList)
                        .append($("<option>")
                            .attr("value", lovManufacturerArray[item].DESCRIPTION.trim())
                            .attr("id", lovManufacturerArray[item].MANUFACTURER_ID));
                });

                lovSupplierArray = jsonData.AM_SUPPLIER;
                $.each(lovSupplierArray, function (item) {

                    $(ah.config.id.supplierStrList)
                        .append($("<option>")
                            .attr("value", lovSupplierArray[item].DESCRIPTION.trim())
                            .attr("id", lovSupplierArray[item].SUPPLIER_ID));
                });
            }
            else if (ah.CurrentMode == ah.config.mode.print) {

                var clientInfo = jsonData.AM_CLIENT_INFO;

                self.clientDetails.CLIENTNAME(clientInfo.DESCRIPTION);
                self.clientDetails.CLIENT_LOGO(clientInfo.CLIENT_LOGO);
                self.clientDetails.COMPANY_LOGO(clientInfo.COMPANY_LOGO);
                self.clientDetails.HEADING(clientInfo.HEADING);
                self.clientDetails.HEADING2(clientInfo.HEADING2);
                self.clientDetails.HEADING3(clientInfo.HEADING3);
                self.clientDetails.HEADING_OTH(clientInfo.HEADING_OTH);
                self.clientDetails.HEADING_OTH2(clientInfo.HEADING_OTH2);
                self.clientDetails.HEADING_OTH3(clientInfo.HEADING_OTH3);
                self.clientDetails.CLIENTADDRESS(clientInfo.ADDRESS + ', ' + clientInfo.CITY + ', ' + clientInfo.ZIPCODE + ', ' + clientInfo.STATE);
                self.clientDetails.CLIENTCONTACT('Tel.No: ' + clientInfo.CONTACT_NO1 + ' Fax No:' + clientInfo.CONTACT_NO2);

                self.print();
            }
        }
		else {

			alert(systemText.errorTwo);
		//	window.location.href = ah.config.url.homeIndex;
		}

	};

	self.initialize();
};