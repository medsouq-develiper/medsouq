﻿var appHelper = function (systemText) {
    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            inputTypeNumber: 'input[type=number]',
        },
        mode: {
            idle: 0,

            homePage: 1,
            searchPage: 2,
            addOrEditPage: 3,

            modalGetStockOnHand: 4,
            modalSearchStockOnHand: 5,
            viewCompanyPR: 6,
            addOrEditCompanyPR: 7
        },
        id: {
            spanUser: '#spanUser',
            prSubmitBtn: '#prSubmitBtn',
            divNotify: '#divNotify',
        },

        cssCls: {
            viewMode: 'view-mode'
        },
        tagId: {
            txtGlobalSearchOth: 'txtGlobalSearchOth',
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datainfo: 'data-info',
            datainfoID: 'data-infoID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex',
            modalClose: '#',
            openModalFilter: '#openModalFilter',
            openStockOnHandModal: '#openStockOnHandModal',
            openReasonCompanyPRModal: '#openReasonCompanyPRModal',
            openBillingOrVendorInfoModal: '#openBillingOrVendorInfoModal',
            openNewMedicalSupplyModal: '#openNewMedicalSupplyModal',
        },
        fld: {
            prNo: 'PRNO'
        },
        api: {
            readPurchaseReq: '/AIMS/ReadPRDetails',
            searchLookup: '/Inventory/SearchStores',
            searchStockOnhand: '/Inventory/SearchStockOnhandFromStore',
            createOrUpdateCompanyPR: '/AIMS/CreateOrUpdateCompanyPR',
            searchCompanyPR: '/AIMS/SearchCompanyPR',
            getCompanyPRByRequest: '/AIMS/GetCompanyPRByRequest',
            updatePRStatus: '/AIMS/UpdatePRStatus',
            searchBillAddress: '/Search/SearchPOBillAddressActData',
            searchMSVLookUp: '/AIMS/SearchMSVLookUp',
            searchAmPartsActive2: '/AIMS/SearchAmPartsActive2',
            getClientRuleById: '/ClientRules/GetClientRuleById',
            searchSetupLov: '/Setup/SearchInventoryLOV',
            createNewAmPart: '/AIMS/CreateNewAmPart',
            readClientInfo: '/Setup/ReadClientInfo'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
        }
    };

    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };
};

/*Knockout MVVM Namespace - Controls form functionality*/
var purchaseRequestViewModel = function (systemText) {
    var ah = new appHelper(systemText);
    var self = this;
    var createFlag = false;
    var updateCompanyPRStatusFlag = false;
    var companyPRReasonStatus = null;

    self.modes = ah.config.mode;
    self.pageMode = ko.observable();
    self.currentMode = ko.observable();

    self.showProgress = ko.observable(false);
    self.showProgressScreen = ko.observable(false);
    self.showModalProgress = ko.observable(false);

    self.progressText = ko.observable();
    self.contentHeader = ko.observable();
    self.contentSubHeader = ko.observable();

    self.modalQuickSearch = ko.observable();
    self.modalReason = ko.observable();
    self.filter = {
        pageQuickSearch: ko.observable(''),
        FromDate: ko.observable(''),
        ToDate: ko.observable(''),
        Status: ko.observable('')
    };

    self.requestingStores = ko.observableArray([]);
    self.requestingStoreSOH = ko.observableArray([]);
    self.assignedSOH = ko.observableArray([]);
    self.purchaseRequests = ko.observableArray([]);
    self.purchaseRequestData = {
        AM_PRREQITEMID: ko.observable(),
        REMARKS: ko.observable(),
        REF_WO: ko.observable(),
        STATUS: ko.observable(),
        REF_ASSETNO: ko.observable(),
        REQUEST_ID: ko.observable(),
        REQ_BY: ko.observable(),
        REQ_CONTACTNO: ko.observable(),
        REQ_DEPT: ko.observable(),
        REQ_DATE: ko.observable(),
        CREATED_BY: ko.observable(),
        CREATED_DATE: ko.observable(),
        STORE_ID: ko.observable(),
        APPROVED_BY: ko.observable(),
        APPROVED_DATE: ko.observable(),
        CANCELLED_REASON: ko.observable(),
        CANCELLED_BY: ko.observable(),
        CANCELLED_DATE: ko.observable(),
        RETURNSTATUS_REASON: ko.observable(),
        RETURNSTATUS_BY: ko.observable(),
        RETURNSTATUS_DATE: ko.observable(),
        PURCHASEID: ko.observable(),
        PURCHASE_NO: ko.observable(),
        QTY: ko.observable(),
        SUPPLIER_ID: ko.observable(),
        SUPPLIERDESC: ko.observable(),
        PURCHASE_DATE: ko.observable(),
        BILL_ADDRESS: ko.observable(),
        BILL_EMAIL_ADDRESS: ko.observable(),
        BILL_CONTACT_NO1: ko.observable(),
        BILL_CONTACT_NO2: ko.observable(),
        BILL_ZIPCODE: ko.observable(),
        BILL_CITY: ko.observable(),
        BILL_STATE: ko.observable(),
        BILL_SHIPTO: ko.observable(),
        FOREIGN_CURRENCY: ko.observable(),
        SHIP_VIA: ko.observable(),
        TOTAL_AMOUNT: ko.observable(),
        TOTAL_NOITEMS: ko.observable(),
        SUPPLIER_ADDRESS1: ko.observable(),
        SUPPLIER_CITY: ko.observable(),
        SUPPLIER_STATE: ko.observable(),
        SUPPLIER_ZIPCODE: ko.observable(),
        SUPPLIER_CONTACT_NO1: ko.observable(),
        SUPPLIER_CONTACT_NO2: ko.observable(),
        SUPPLIER_CONTACT: ko.observable(),
        SUPPLIER_EMAIL_ADDRESS: ko.observable(),
        OTHER_INFO: ko.observable(),
        SHIP_DATE: ko.observable(),
        EST_SHIP_DATE: ko.observable(),
        HANDLING_FEE: ko.observable(),
        TRACKING_NO: ko.observable(),
        EST_FREIGHT: ko.observable(),
        FREIGHT: ko.observable(),
        FREIGHT_PAIDBY: ko.observable(),
        FREIGHT_CHARGETO: ko.observable(),
        TAXABLE: ko.observable(),
        TAX_CODE: ko.observable(),
        TAX_AMOUNT: ko.observable(),
        FREIGHT_POLINE: ko.observable(),
        FREIGHT_SEPARATE_INV: ko.observable(),
        INSURANCE_VALUE: ko.observable(),
        PO_BILLCODE_ADDRESS: ko.observable()
    };

    self.signOut = function () {
        window.location.href = ah.config.url.homeIndex;
    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.returnHome = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        window.location.href = staffDetails.HOME_DEFAULT;
    };
    self.toggleBoolean = function (property, passValue, mode) {
        var value = typeof passValue !== 'undefined' ? passValue : !self[property]();
        self[property](value);

        if (self[property](value) && mode) {
            switch (mode) {
                case 'search':
                    self.progressText('Please wait while searching . . .');
                    break;
                case 'save':
                    self.progressText('Please wait while saving . . .');
                    break;
                case 'get':
                    self.progressText('Please wait while getting Company Purchase Request Information . . .');
                    break;
            }
        }
    };
    self.setObservableValue = function (_object1, _object2) {
        Object.keys(_object2).forEach(function (key) {
            if (ko.isObservable(_object1[key]))
                _object1[key](_object2[key])
            else
                _object1[key] = _object2[key];
        });
    };
    self.getObservableValue = function (data) {
        if (Array.isArray(data)) {
            var returnData = [];
            for (var i = 0; i < data.length; i++) {
                var _object = data[i];
                returnData.push({});
                Object.keys(_object).forEach(function (key) {
                    if (ko.isObservable(_object[key]))
                        returnData[i][key] = _object[key]();
                    else
                        returnData[i][key] = _object[key];
                });
            }
        }
        else if (data instanceof Object) {
            var returnData = {};
            var _object = data;
            Object.keys(_object).forEach(function (key) {
                if (ko.isObservable(_object[key]))
                    returnData[key] = _object[key]();
                else
                    returnData[key] = _object[key];
            });
        }
        else
            returnData = data;

        return returnData;
    };
    self.setObservableValueEmpty = function (object) {
        Object.keys(object).forEach(function (key) {
            if (ko.isObservable(object[key]))
                object[key]('')
            else
                object[key] = '';
        });
    }
    self.checkViewMode = function () {
        return self.currentMode() != self.modes.viewCompanyPR
    };

    self.createNew = function () {
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData = { LOV: "'AIMS_COSTCENTER'", SEARCH: '', STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchBillAddress, postData).done(function (jsonData) {
            createFlag = true;
            self.pageMode(self.modes.addOrEditPage);
            self.currentMode(self.modes.idle);
            self.contentHeader(systemText.addModeHeader)
            self.contentSubHeader(systemText.addModeSubHeader);

            self.setObservableValueEmpty(self.purchaseRequestData);

            self.purchaseRequestData.STORE_ID(self.clientRuleStoreId);
            self.purchaseRequestData.REQ_DATE(moment().format('DD/MM/YYYY HH:mm'));
            self.purchaseRequestData.PURCHASE_DATE(moment().format('DD/MM/YYYY'));
            self.purchaseRequestData.STATUS('REQUEST');

            self.resetAssignedSOH();

            if (jsonData.AM_PURCHASE_BILLADDRESS && jsonData.AM_PURCHASE_BILLADDRESS.length == 1) {
                self.modalType('billing');
                self.assignBillingOrVendorInfo(jsonData.AM_PURCHASE_BILLADDRESS[0]);
            }
        }).fail(self.webApiCallbackStatus);
    };
    self.editCompanyPR = function () {
        createFlag = false;
        self.currentMode(self.modes.idle);
        self.contentSubHeader(systemText.addModeSubHeader);
    };
    self.openReasonCompanyPRModal = function (status) {
        companyPRReasonStatus = status;
        self.modalReason('');
        window.location.href = ah.config.url.openReasonCompanyPRModal;
    };
    self.saveCompanyPRModal = function () {
        self.updateCompanyPRStatus(companyPRReasonStatus);
    };
    self.updateCompanyPRStatus = function (status) {
        self.currentMode(self.modes.viewCompanyPR);
        self.toggleBoolean('showProgress', true, 'save');
        self.toggleBoolean('showProgressScreen', true);
        updateCompanyPRStatusFlag = true;

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var AM_PURCHASEREQUEST = self.getObservableValue(self.purchaseRequestData);

        AM_PURCHASEREQUEST.STATUS = status;
        if (AM_PURCHASEREQUEST.STATUS === "APPROVED") {
            AM_PURCHASEREQUEST.APPROVED_BY = staffLogonSessions.STAFF_ID;
            AM_PURCHASEREQUEST.APPROVED_DATE = moment().format();
        }
        else if (AM_PURCHASEREQUEST.STATUS === "CANCELLED") {
            AM_PURCHASEREQUEST.CANCELLED_BY = staffLogonSessions.STAFF_ID;
            AM_PURCHASEREQUEST.CANCELLED_DATE = moment().format();
            AM_PURCHASEREQUEST.CANCELLED_REASON = self.modalReason();
        }
        else if (AM_PURCHASEREQUEST.STATUS === "REOPEN") {
            AM_PURCHASEREQUEST.RETURNSTATUS_REASON = self.modalReason();
            AM_PURCHASEREQUEST.RETURNSTATUS_BY = staffLogonSessions.STAFF_ID;
            AM_PURCHASEREQUEST.RETURNSTATUS_DATE = moment().format();
            AM_PURCHASEREQUEST.STATUS = "REQUEST";
        }
        AM_PURCHASEREQUEST.REQ_DATE = moment(AM_PURCHASEREQUEST.REQ_DATE, 'DD/MM/YYYY HH:mm').format();
        AM_PURCHASEREQUEST.PURCHASE_DATE = moment(AM_PURCHASEREQUEST.PURCHASE_DATE, 'DD/MM/YYYY HH:mm').format();

        var postData = { AM_PURCHASEREQUEST: AM_PURCHASEREQUEST, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.updatePRStatus, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

        window.location.href = ah.config.url.modalClose;
    };
    self.clearModalFilter = function () {
        self.filter.FromDate('');
        self.filter.ToDate('');
        self.filter.Status('');
        $('#modalFromDate').val('');
        $('#modalToDate').val('');
    };
    self.openModalFilter = function () {
        window.location.href = ah.config.url.openModalFilter;
    };
    self.filterSearch = function () {
        self.pageMode(self.modes.searchPage);
        self.toggleBoolean('showProgress', true, 'search');
        self.filter.pageQuickSearch('');
        var FromDate = $('#modalFromDate').val();
        var ToDate = $('#modalToDate').val();
        if (moment(FromDate, 'DD/MM/YYYY', true).isValid())
            self.filter.FromDate(moment(FromDate, 'DD/MM/YYYY').format())
        if (moment(ToDate, 'DD/MM/YYYY', true).isValid())
            self.filter.ToDate(moment(ToDate, 'DD/MM/YYYY').format())

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        var postData = { SEARCH: "", SEARCH_FILTER: self.filter, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchCompanyPR, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.quickSearch = function () {
        self.pageMode(self.modes.searchPage);
        self.toggleBoolean('showProgress', true, 'search');

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        var postData = { SEARCH: self.filter.pageQuickSearch, SEARCH_FILTER: { FromDate: "" }, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchCompanyPR, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.selectPurchaseRequest = function (data) {
        var selectedPurchaseRequest = data;
        self.pageMode(self.modes.addOrEditPage);
        self.currentMode(self.modes.viewCompanyPR);
        self.toggleBoolean('showProgress', true);
        self.contentHeader(systemText.updateModeHeader)
        self.contentSubHeader(systemText.updateModeSubHeader);

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        var postData = { REQUEST_ID: selectedPurchaseRequest.REQUEST_ID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.getCompanyPRByRequest, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.openStockOnHandModal = function () {
        if (!self.purchaseRequestData.STORE_ID())
            return alert('Unable to Proceed. Please select Requesting Store to proceed.');

        self.currentMode(self.modes.modalGetStockOnHand);
        self.toggleBoolean('showProgress', true, 'search');

        self.modalQuickSearch('');
        self.modalActiveSearching(1);

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData = { SEARCH: "", ACTION: "", STORE_FROM: self.purchaseRequestData.STORE_ID(), STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchStockOnhand, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.modalSearchStock = function (val) {
        self.modalActiveSearching(val); 
        self.currentMode(self.modes.modalSearchStockOnHand);
        self.toggleBoolean('showModalProgress', true);

        self.requestingStoreSOH([]);

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        if (self.modalActiveSearching() == 1) {
            var postData = { SEARCH: self.modalQuickSearch(), ACTION: "", STORE_FROM: self.purchaseRequestData.STORE_ID(), STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchStockOnhand, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {
            var postData = { SEARCH: self.modalQuickSearch(), STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchAmPartsActive2, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
    };
    self.resetAssignedSOH = function () {
        self.assignedSOH([]);
    };
    self.assignStock = function () {
        var selectedItem = this;
        var found = self.assignedSOH().find(function (item) { return item.ID_PARTS == selectedItem.ID_PARTS });
        if (found) return alert('Unable to Proceed. The selected item was already exist.');
        var item = {
            DESCRIPTION: selectedItem.DESCRIPTION,
            ID_PARTS: selectedItem.ID_PARTS,
            PART_NUMBERS: selectedItem.PART_NUMBERS,
            AM_UOM: selectedItem.AM_UOM,
            STOCK_MIN: selectedItem.MIN || 0,
            STOCK_MAX: selectedItem.MAX || 0,
            PR_REQUEST_QTY: ko.observable(1),
            REQ_DATE: moment(self.purchaseRequestData.REQ_DATE(), 'DD/MM/YYYY HH:mm').format(),
            PR_REQUEST_ID: self.purchaseRequestData.REQUEST_ID,
            REF_WO: self.purchaseRequestData.REF_WO(),
            REF_ASSETNO: '',
            STORE_ID: self.purchaseRequestData.STORE_ID(),
            STATUS: "REQUEST",
            AMPRREQITEMID: 0,
            REQ_DEPT: self.purchaseRequestData.REQ_DEPT,
            REMARKS: self.purchaseRequestData.REMARKS,
            CURRSTATUS: "REQUEST",
            REQ_DETFLAG: 'Y',
            PRICE: ko.observable(0),
            TOTAL_PRICE: ko.observable(),
        };
        self.calculateTotalPrice(item);
        self.assignedSOH.push(item);
    };
    self.removeAssignStock = function () {
        var selectedItem = this;
        self.assignedSOH.remove(selectedItem);
    };
    self.cancelChanges = function () {
        if (createFlag)
            self.pageMode(self.modes.homePage);
        else
            self.selectPurchaseRequest(self.getObservableValue(self.purchaseRequestData));
    };
    self.triggerFormSubmit = function () {
        $(ah.config.id.prSubmitBtn).trigger('click');
    };
    self.saveAllChanges = function () {
        if (!self.purchaseRequestData.SUPPLIERDESC())
            return alert('Unable to proceed. Please select vendor information.');

        if (!self.purchaseRequestData.BILL_SHIPTO())
            return alert('Unable to proceed. Please select ship to/billing information.');

        if (!self.assignedSOH().length)
            return alert('Unable to proceed. No item to request. Please click on Add item button and select stock you wish to request.');

        var found = self.assignedSOH().find(function (item) { return !item.PR_REQUEST_QTY() });
        if (found)
            return alert('Please make sure that the ' + found.DESCRIPTION + ' qty value is greater than 0.');

        var found = self.assignedSOH().find(function (item) { return Number(item.PR_REQUEST_QTY()) < 0});
        if (found)
            return alert('Unable to proceed. Item name: ' + found.DESCRIPTION + ', have negative request qty. Please make sure that there is no negative qty.');

        self.currentMode(self.modes.addOrEditCompanyPR);
        self.toggleBoolean('showProgress', true, 'save');
        self.toggleBoolean('showProgressScreen', true);

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

        var AM_PURCHASEREQUEST = self.getObservableValue(self.purchaseRequestData);
        AM_PURCHASEREQUEST.REQ_DATE = moment(AM_PURCHASEREQUEST.REQ_DATE, 'DD/MM/YYYY HH:mm').format();
        AM_PURCHASEREQUEST.PURCHASE_DATE = moment(AM_PURCHASEREQUEST.PURCHASE_DATE, 'DD/MM/YYYY HH:mm').format();
        AM_PURCHASEREQUEST.EST_SHIP_DATE = AM_PURCHASEREQUEST.EST_SHIP_DATE ? moment(AM_PURCHASEREQUEST.EST_SHIP_DATE, 'DD/MM/YYYY HH:mm').format() : '';
        AM_PURCHASEREQUEST.CREATED_BY = staffLogonSessions.STAFF_ID;
        AM_PURCHASEREQUEST.CREATED_DATE = AM_PURCHASEREQUEST.REQ_DATE;
        var AM_PARTSREQUEST = self.assignedSOH();

        var postData = { MODE: createFlag ? 'Create' : 'Modify', AM_PURCHASEREQUEST: AM_PURCHASEREQUEST, AM_PARTSREQUEST: AM_PARTSREQUEST, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.createOrUpdateCompanyPR, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
    };
    self.initialize = function () {
        window.location.href = ah.config.url.modalClose;

        self.pageMode(self.modes.homePage);
        self.toggleBoolean('showProgress', true, 'search');
        self.toggleBoolean('showProgressScreen', true);

        $(ah.config.id.divNotify).css('display', 'none');

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        var postData = { LOV: "'SHIP_VIA','FOREIGN_CURRENCY','AIMS_MSTYPE','AIMS_MSUOM'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.when($.post(self.getApi() + ah.config.api.searchSetupLov, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus)).done(function () {
            var postData = { ruleId: 'STOREID', STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.getClientRuleById, postData).done(function (jsonData) {
                self.clientRuleStoreId = jsonData.CLIENTRULE.RULE_VALUE;
            }).fail(self.webApiCallbackStatus);
        });
    };

    self.medicalSupply = {
        DESCRIPTION: ko.observable(),
        TYPE_ID: ko.observable(),
        UOM_ID: ko.observable()
    };
    self.modalActiveSearching = ko.observable(1);
    self.shipVias = ko.observableArray([]);
    self.foreignCurrencies = ko.observableArray([]);
    self.types = ko.observableArray([]);
    self.uoms = ko.observableArray([]);
    self.clientRuleStoreId = '';
    self.modalQuickSearch2 = ko.observable();
    self.modalType = ko.observable();
    self.billingOrVendorList = ko.observableArray([]);
    self.openBillingOrVendorInfoModal = function (type) {
        self.modalQuickSearch2('');
        self.modalType(type);

        self.modalSearchBillingOrVendor();
        window.location.href = ah.config.url.openBillingOrVendorInfoModal;
    };
    self.modalSearchBillingOrVendor = function () {
        self.billingOrVendorList.removeAll();
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        self.toggleBoolean('showModalProgress', true);

        if (self.modalType() == 'billing') {
            var postData = { LOV: "'AIMS_COSTCENTER'", SEARCH: self.modalQuickSearch2(), STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchBillAddress, postData).done(function (jsonData) {
                self.billingOrVendorList(jsonData.AM_PURCHASE_BILLADDRESS);
                self.toggleBoolean('showModalProgress', false);
            }).fail(self.webApiCallbackStatus);
        }
        else {
            var postData = { SEARCH: self.modalQuickSearch2(), TYPE: 'SV', STAFF_LOGON_SESSIONS: staffLogonSessions };
            $.post(self.getApi() + ah.config.api.searchMSVLookUp, postData).done(function (jsonData) {
                self.billingOrVendorList(jsonData.AM_MANUFACTURER);
                self.toggleBoolean('showModalProgress', false);
            }).fail(self.webApiCallbackStatus);
        }
    };
    self.assignBillingOrVendorInfo = function (item) {
        if (self.modalType() == 'billing') {
            self.purchaseRequestData.BILL_SHIPTO(item.SHIP_TO);
            self.purchaseRequestData.PO_BILLCODE_ADDRESS(item.PO_BILLCODE);
            self.purchaseRequestData.BILL_ADDRESS(item.BILL_ADDRESS);
            self.purchaseRequestData.BILL_CITY(item.BILL_CITY);
            self.purchaseRequestData.BILL_ZIPCODE(item.BILL_ZIPCODE);
            self.purchaseRequestData.BILL_STATE(item.BILL_STATE);
            self.purchaseRequestData.BILL_CONTACT_NO1(item.BILL_CONTACT_NO1);
            self.purchaseRequestData.BILL_CONTACT_NO2(item.BILL_CONTACT_NO1);
            self.purchaseRequestData.BILL_EMAIL_ADDRESS(item.BILL_EMAIL_ADDRESS);
        }
        else {
            self.purchaseRequestData.SUPPLIER_ID(item.MANUFACTURER_ID);
            self.purchaseRequestData.SUPPLIERDESC(item.DESCRIPTION);
            self.purchaseRequestData.SUPPLIER_ADDRESS1(item.ADDRESS1);
            self.purchaseRequestData.SUPPLIER_CITY(item.CITY);
            self.purchaseRequestData.SUPPLIER_ZIPCODE(item.ZIP);
            self.purchaseRequestData.SUPPLIER_STATE(item.STATE);
            self.purchaseRequestData.SUPPLIER_CONTACT_NO1(item.CONTACT_NO1);
            self.purchaseRequestData.SUPPLIER_CONTACT_NO2(item.CONTACT_NO2);
            self.purchaseRequestData.SUPPLIER_EMAIL_ADDRESS(item.EMAIL_ADDRESS);
        }
        window.location.href = ah.config.url.modalClose;
    };
    self.calculateTotalPrice = function (item, isPrice) {
        item.TOTAL_PRICE((item.PRICE() || 0) * item.PR_REQUEST_QTY());

        item.TOTAL_PRICE(Math.round(item.TOTAL_PRICE() * 100) / 100);

        self.calculateTotal();
    };

    self.calculateTotal = function () {
        var totalAmountPrice = 0;
        totalAmountPrice = self.assignedSOH().reduce(function (acc, val) { return acc + val.TOTAL_PRICE(); }, 0);
        var inctoPO = self.purchaseRequestData.FREIGHT_POLINE();
        var freightCharge = self.purchaseRequestData.FREIGHT_CHARGETO();
        if (inctoPO == "YES" && freightCharge == "YES") {
            totalAmountPrice = totalAmountPrice + Number(self.purchaseRequestData.FREIGHT());
        } else if (freightCharge == "NO") {
            self.purchaseRequestData.FREIGHT_POLINE(null);
            self.purchaseRequestData.FREIGHT(0);
        }


            

        totalAmountPrice = Math.round(totalAmountPrice * 100) / 100;
        self.purchaseRequestData.TOTAL_AMOUNT(totalAmountPrice);
    }

    self.openNewMedicalSupplyModal = function () {
        self.setObservableValueEmpty(self.medicalSupply);
        window.location.href = ah.config.url.openNewMedicalSupplyModal;
    }
    self.addNewMedicalSupplyModal = function () {
        var jDate = new Date();
        var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        self.medicalSupply.STATUS = 'Active';
        self.medicalSupply.CREATED_DATE = strDateStart + ' ' + hourmin;

        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData = { AM_PARTS: self.medicalSupply, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.createNewAmPart, postData).done(function (jsonData) {
            if (jsonData.ERROR && jsonData.ERROR.ErrorText.includes("Duplicate")) {
                return alert('Description already in use');
            }

            var selectedItem = jsonData.AM_PARTS;
            var item = {
                DESCRIPTION: selectedItem.DESCRIPTION,
                ID_PARTS: selectedItem.ID_PARTS,
                PART_NUMBERS: selectedItem.PART_NUMBERS,
                AM_UOM: selectedItem.AM_UOM,
                STOCK_MIN: selectedItem.MIN || 0,
                STOCK_MAX: selectedItem.MAX || 0,
                PR_REQUEST_QTY: ko.observable(1),
                REQ_DATE: moment(self.purchaseRequestData.REQ_DATE(), 'DD/MM/YYYY HH:mm').format(),
                PR_REQUEST_ID: self.purchaseRequestData.REQUEST_ID,
                REF_WO: self.purchaseRequestData.REF_WO(),
                REF_ASSETNO: '',
                STORE_ID: self.purchaseRequestData.STORE_ID(),
                STATUS: "REQUEST",
                AMPRREQITEMID: 0,
                REQ_DEPT: self.purchaseRequestData.REQ_DEPT,
                REMARKS: self.purchaseRequestData.REMARKS,
                CURRSTATUS: "REQUEST",
                REQ_DETFLAG: 'Y',
                PRICE: ko.observable(0),
                TOTAL_PRICE: ko.observable(),
            };
            self.calculateTotalPrice(item);
            self.assignedSOH.push(item);

            self.modalSearchStock(self.modalActiveSearching());
            window.location.href = ah.config.url.openStockOnHandModal;
        }).fail(self.webApiCallbackStatus);
    };

    self.assignedSOHPrint = ko.observableArray([]);
    self.clientDetails = {
        CLIENTNAME: ko.observable(),
        CLIENT_LOGO: ko.observable(),
        COMPANY_LOGO: ko.observable(),
        HEADING: ko.observable(),
        HEADING2: ko.observable(),
        HEADING3: ko.observable(),
        HEADING_OTH: ko.observable(),
        HEADING_OTH2: ko.observable(),
        HEADING_OTH3: ko.observable(),
        CLIENTADDRESS: ko.observable(),
        CLIENTCONTACT: ko.observable()
    };
    self.printPurchaseRequest = function () {
        self.clientDetails.HEADING('Business Schema Trading Company');
        self.clientDetails.COMPANY_LOGO(window.location.origin + '/images/companylogo.png');

        self.assignedSOHPrint.removeAll();
        self.assignedSOHPrint(self.assignedSOH.slice(0));
        if (self.purchaseRequestData.FREIGHT_POLINE() == 'YES') {
            self.assignedSOHPrint.push({
                DESCRIPTION: 'FREIGHT CHARGES',
                PR_REQUEST_QTY: 1,
                PRICE: ko.observable(self.purchaseRequestData.FREIGHT() || 0),
                TOTAL_PRICE: ko.observable(self.purchaseRequestData.FREIGHT() || 0),
            });
        }

        setTimeout(function () {
            window.print();
        }, 1000)



        //var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        //var staffInfo = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        //var postData = { CLIENTID: staffInfo.CLIENT_CODE, STAFF_LOGON_SESSIONS: staffLogonSessions };
        //$.post(self.getApi() + ah.config.api.readClientInfo, postData).done(function (jsonData) {

        //    var clientInfo = jsonData.AM_CLIENT_INFO;

        //    self.clientDetails.CLIENTNAME(clientInfo.DESCRIPTION);
        //    self.clientDetails.CLIENT_LOGO(window.location.origin + (self.clientDetails.CLIENT_LOGO() || '/images/nophotok.png'));
        //    self.clientDetails.COMPANY_LOGO(window.location.origin + (self.clientDetails.COMPANY_LOGO() || '/images/nophotok.png'));
        //    self.clientDetails.HEADING(clientInfo.DESCRIPTION);
        //    self.clientDetails.HEADING2(clientInfo.DESCRIPTION_OTH);
        //    self.clientDetails.HEADING3(clientInfo.DESCRIPTION_SHORT);
        //    //self.clientDetails.HEADING_OTH(clientInfo.HEADING_OTH);
        //    //self.clientDetails.HEADING_OTH2(clientInfo.HEADING_OTH2);
        //    //self.clientDetails.HEADING_OTH3(clientInfo.HEADING_OTH3);
        //    self.clientDetails.CLIENTADDRESS(clientInfo.ADDRESS + ', ' + clientInfo.CITY + ', ' + clientInfo.ZIPCODE + ', ' + clientInfo.STATE);
        //    self.clientDetails.CLIENTCONTACT('Tel.No: ' + clientInfo.CONTACT_NO1 + ' Fax No:' + clientInfo.CONTACT_NO2);


        //    self.assignedSOHPrint.removeAll();
        //    self.assignedSOHPrint(self.assignedSOH.slice(0));
        //    if (self.purchaseRequestData.FREIGHT_POLINE() == 'YES') {
        //        self.assignedSOHPrint.push({
        //            DESCRIPTION: 'FREIGHT CHARGES',
        //            PR_REQUEST_QTY: 1,
        //            PRICE: ko.observable(self.purchaseRequestData.FREIGHT() || 0),
        //            TOTAL_PRICE: ko.observable(self.purchaseRequestData.FREIGHT() || 0),
        //        });
        //    }

        //    setTimeout(function () {
        //        window.print();
        //    }, 3000)
        //}).fail(self.webApiCallbackStatus);
    };
    self.getLookupDescription = function (value, array) {
        return value && array.length && array.find(function (x) { return x.LOV_LOOKUP_ID == value }).DESCRIPTION;
    };
    self.getTotalPrice = function () {
        var itemTotalPrice = self.assignedSOHPrint().reduce(function (acc, val) { return acc + val.TOTAL_PRICE(); }, 0);

        return itemTotalPrice + Number(self.purchaseRequestData.TAX_AMOUNT() || 0);
    };
    self.getPODate = function () {
        return moment(self.purchaseRequestData.PURCHASE_DATE(), 'DD/MM/YYYY').format('DD-MMM-YYYY');
    };

    self.webApiCallbackStatus = function (jsonData) {
        if (jsonData.ERROR) {
            if (jsonData.ERROR.ErrorText === "Work Order not found.") {
                alert(jsonData.ERROR.ErrorText);
            } else if (jsonData.ERROR.ErrorText === "Asset not found.") {
                alert(jsonData.ERROR.ErrorText);
            } else {
                alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            }

            self.toggleBoolean('showProgress', false);
            self.toggleBoolean('showProgressScreen', false);

            $(ah.config.id.divNotify).css('display', 'none');
        }
        else if (!jsonData.ERROR) {
            switch (self.pageMode()) {
                case self.modes.searchPage:
                    self.toggleBoolean('showProgress', false);
                    self.purchaseRequests(jsonData.AM_PURCHASEREQUEST);

                    window.location.href = ah.config.url.modalClose;
                    break;

                case self.modes.homePage:
                    self.toggleBoolean('showProgress', false);
                    self.toggleBoolean('showProgressScreen', false);

                    var shipVias = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'SHIP_VIA' });;
                    shipVias.unshift({
                        LOV_LOOKUP_ID: null,
                        DESCRIPTION: '- select -'
                    });
                    self.shipVias(shipVias);

                    var foreignCurrencies = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'FOREIGN_CURRENCY' });;
                    foreignCurrencies.unshift({
                        LOV_LOOKUP_ID: null,
                        DESCRIPTION: '- select -'
                    });
                    self.foreignCurrencies(foreignCurrencies);

                    var types = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'AIMS_MSTYPE' });;
                    types.unshift({
                        LOV_LOOKUP_ID: null,
                        DESCRIPTION: '- select -'
                    });
                    self.types(types);

                    var uoms = (jsonData.LOV_LOOKUPS || []).filter(function (item) { return item.CATEGORY == 'AIMS_MSUOM' });;
                    uoms.unshift({
                        LOV_LOOKUP_ID: null,
                        DESCRIPTION: '- select -'
                    });
                    self.uoms(uoms);
                    break;

                case self.modes.addOrEditPage:
                        
                    switch (self.currentMode()) {
                        case self.modes.modalGetStockOnHand:
                            self.toggleBoolean('showProgress', false);
                            self.requestingStoreSOH(jsonData.PRODUCT_STOCKONHAND);

                            window.location.href = ah.config.url.openStockOnHandModal;
                            break;

                        case self.modes.modalSearchStockOnHand:
                            self.toggleBoolean('showModalProgress', false);
                            self.requestingStoreSOH(jsonData.PRODUCT_STOCKONHAND || jsonData.AM_PARTS);
                            break;

                        case self.modes.addOrEditCompanyPR:
                            self.currentMode(self.modes.viewCompanyPR);
                            self.toggleBoolean('showProgress', false);
                            self.toggleBoolean('showProgressScreen', false);

                            $(ah.config.id.divNotify).css('display', 'block');
                            $(ah.config.id.divNotify + ' p').html(systemText.saveSuccessMessage);
                            setTimeout(function () {
                                $(ah.config.id.divNotify).fadeOut(1000);
                            }, 3000);

                            self.setObservableValue(self.purchaseRequestData, jsonData.AM_PURCHASEREQUEST);
                            self.purchaseRequestData.REQ_DATE(moment(self.purchaseRequestData.REQ_DATE()).format('DD/MM/YYYY HH:mm'));
                            self.purchaseRequestData.PURCHASE_DATE(moment(self.purchaseRequestData.PURCHASE_DATE()).format('DD/MM/YYYY'));
                            self.purchaseRequestData.EST_SHIP_DATE(self.purchaseRequestData.EST_SHIP_DATE() ? moment(self.purchaseRequestData.EST_SHIP_DATE()).format('DD/MM/YYYY') : '');
                            break;

                        case self.modes.viewCompanyPR:
                            if (updateCompanyPRStatusFlag) {
                                updateCompanyPRStatusFlag = false;
                                self.toggleBoolean('showProgressScreen', false);

                                $(ah.config.id.divNotify).css('display', 'block');
                                $(ah.config.id.divNotify + ' p').html(systemText.saveSuccessMessage);
                                setTimeout(function () {
                                    $(ah.config.id.divNotify).fadeOut(1000);
                                }, 3000);

                                self.selectPurchaseRequest(self.getObservableValue(self.purchaseRequestData));
                            }
                            else {
                                self.toggleBoolean('showProgress', false);
                                jsonData.AM_PURCHASEREQUESITEMS = jsonData.AM_PURCHASEREQUESITEMS || [];
                                for (var i = 0; i < jsonData.AM_PURCHASEREQUESITEMS.length; i++) {
                                    jsonData.AM_PURCHASEREQUESITEMS[i].PR_REQUEST_QTY = ko.observable(jsonData.AM_PURCHASEREQUESITEMS[i].PR_REQUEST_QTY);
                                    jsonData.AM_PURCHASEREQUESITEMS[i].PRICE = ko.observable(jsonData.AM_PURCHASEREQUESITEMS[i].PRICE);
                                    jsonData.AM_PURCHASEREQUESITEMS[i].TOTAL_PRICE = ko.observable(0);
                                    self.calculateTotalPrice(jsonData.AM_PURCHASEREQUESITEMS[i]);
                                }

                                self.setObservableValue(self.purchaseRequestData, jsonData.AM_PURCHASEREQUEST);
                                self.purchaseRequestData.REQ_DATE(moment(self.purchaseRequestData.REQ_DATE()).format('DD/MM/YYYY HH:mm'));
                                self.purchaseRequestData.PURCHASE_DATE(moment(self.purchaseRequestData.PURCHASE_DATE()).format('DD/MM/YYYY'));
                                self.purchaseRequestData.EST_SHIP_DATE(self.purchaseRequestData.EST_SHIP_DATE() ? moment(self.purchaseRequestData.EST_SHIP_DATE()).format('DD/MM/YYYY') : '');
                                self.assignedSOH(jsonData.AM_PURCHASEREQUESITEMS);
                            }
                            break;
                    }
                    break;
            }
        }
        else {
            alert(systemText.errorTwo);

            self.toggleBoolean('showProgress', false);
            self.toggleBoolean('showProgressScreen', false);

            $(ah.config.id.divNotify).css('display', 'none');
        }
    };

    self.initialize();
};