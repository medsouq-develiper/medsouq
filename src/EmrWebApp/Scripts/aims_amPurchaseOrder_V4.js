﻿var appHelper = function (systemText) {
	openModalLookup
	var thisApp = this;

	thisApp.config = {
		cls: {
			contentField: '.field-content-detail',
			contentSearch: '.search-content-detail',
			contentHome: '.home-content-detail',
			liModify: '.liModify',
			liSave: '.liSaveAll',
			liAddNew: '.liAddNew',
			divProgress: '.divProgressScreen',
			liApiStatus: '.liApiStatus',
			notifySuccess: '.notify-success',
			toolBoxRight: '.toolbox-right',
			divClinicDoctorID: '.divClinicDoctorID',
			statusText: '.status-text',
			detailItem: '.detailItem',
			liassetTag: '.liassetTag',
			wsAsset: '.wsAsset',
			itemRequest: '.itemRequest',
			lookupDetailItem: '.lookupDetailItem',
			liApprove: '.liApprove'
		},
		tag: {
			input: 'input',
			textArea: 'textarea',
			select: 'select',
			inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            inputTypeNumber: 'input[type=number]',
			ul: 'ul',
		},
		mode: {
			idle: 0,
			add: 1,
			search: 2,
			searchResult: 3,
			save: 4,
			read: 5,
			display: 6,
			edit: 7,
			deleted: 8
		},
		tagId: {
			btnSupplier: 'btnSupplier',
			btnBillAddress: 'btnBillAddress',
			txtGlobalSearchOth: 'txtGlobalSearchOth',
			priorPage: 'priorPage',
			nextPage: 'nextPage',
			lookUpnextPage: 'lookUpnextPage',
			lookUppriorPage: 'lookUppriorPage',
			lookUptxtGlobalSearchOth: 'lookUptxtGlobalSearchOth'
		},
		id: {
			spanUser: '#spanUser',
			contentHeader: '#contentHeader',
			contentSubHeader: '#contentSubHeader',
			saveInfo: '#saveInfo',
			searchResultList: '#searchResultList',
			searchResultSummary: '#searchResultSummary',
			txtGlobalSearch: '#txtGlobalSearch',
			frmInfo: '#frmInfo',
			supplierID: '#SUPPLIER_ID',
			frmInfoInspection: '#frmInfoInspection',
			clinicdoctorID: '#CLINIC_DOCTOR_ID',
			clinicID: '#CLINIC_ID',
			frmwsAsset: '#frmwsAsset',
			frmInfoReading: '#frmInfoReading',
			staffID: '#STAFF_ID',
			saveInfoLog: '#saveInfoLog',
			serviceID: '#SERVICE_ID',
			searchResult1: '#searchResult1',
			successMessage: '#successMessage',
			saveInfoReading: '#saveInfoReading',
			txtGlobalSearchOth: '#txtGlobalSearchOth',
			pageNum: '#pageNum',
			pageCount: '#pageCount',
			loadNumber: '#loadNumber',
			btnRemove: '#btnRemove',
			itemsReceiveBatch: '#itemsReceiveBatch',
			searchResultLookUp: '#searchResultLookUp',
			lookUptxtGlobalSearchOth: '#lookUptxtGlobalSearchOth',
			lookUppageNum: '#lookUppageNum',
			lookUppageCount: '#lookUppageCount',
			lovAction: '#LOVACTION'
		},

		cssCls: {
			viewMode: 'view-mode'
		},
		
		attr: {
			readOnly: 'readonly',
			disabled: 'disabled',
			selectedIndex: 'selectedIndex',
			datainfo: 'data-info',
			datainfoID: 'data-infoID',
			datalookUp: 'data-lookUp'
		},
		skey: {
			staff: 'STAFF',
			webApiUrl: 'webApiURL',
			staffLogonSessions: 'STAFF_LOGON_SESSIONS',
			searchResult: 'searchResult',
			infoDetails: 'AM_PURCHASEREQUEST',
			poItemsList: 'AM_PURCHASEORDERITEMS',
			prApprovedList: 'AM_PURCHASEREQUEST_APPROVEDLIST',
			wsassetList: 'AM_INSPECTIONASSET',
			supplierInfo: 'AM_SUPPLIER',
			supplierListLov: 'AM_SUPPLIERLIST',
			billAddress: 'AM_PURCHASE_BILLADDRESS',
			groupPrivileges: 'GROUP_PRIVILEGES',
			clientInfo: 'AM_CLIENT_INFO'
        },
        fld: {
            poNo: 'PONO'
        },
		url: {
			homeIndex: '/Home/Index',
			aimsHomepage: '/Menuapps/aimsIndex',
			modalLookUp: 'openModalLookup',
			modalClose: '#',
			modalLookUpSupBill: '#openModalLookupSupBill',
			modalAddItems: '#openModal',
			modalReading: 'openModalReading'
		},
		api: {

			readPurchaseReq: '/AIMS/ReadPODetails',
			searchAll: '/AIMS/SearchPO',
			createOrder: '/AIMS/CreateNewPurchaseOrder',
            updateStatusPO: '/AIMS/UpdateStatusPurchaseOrder',
            updatePO: '/AIMS/UpdatePurchaseOrderReq',
			readWOrder: '/AIMS/ReadWO',
			readAssetInfo: '/AIMS/ReadAssetInfo',
			searchSupplier: '/AIMS/SearchSuppierLOVData',
			searchApprovedPR: '/AIMS/SearchPRApproved',
			getClientInfo: '/Setup/ReadClientInfo',
			searchSupplierProd:  '/AIMS/SearchPartSP',
			searchPRFORPO: '/AIMS/SearchPurchaseApproveByProdId',
			searchBillAddress: '/Search/SearchPOBillAddressActData',
            approvedPO: '/AIMS/ApprovePurchaseOrder',
            getprItemInfo: '/AIMS/SearchPurchaseRequestItemInfo'
		},
		html: {
			searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem' data-infoID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} - {2} {3}</a>",
			searchRowHeaderTemplateLOV: "<a href='#' class='fs-medium-normal lookupDetailItem fc-slate-blue' data-lookUp='{0}' >&nbsp;&nbsp;&nbsp;&nbsp;  <i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1} </span></a>",
            searchRowTemplate: "<span class='search-row-label fc-light-grey-2'>{0}: <span class='search-row-data'>{1}</span></span>&nbsp;&nbsp;&nbsp;&nbsp;",
			searchRowHeaderTemplateList: "<a href='#openModal' class='fs-medium-normal detailItem' data-infoAssetID='{0}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} {3}</a>",
			searchLOVRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem fc-greendark' data-info ='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} <span>{3}</span></a>",
			searchExistRowHeaderTemplate: "<a href='#' class='fs-medium-normal detailItem fc-slate-blue' data-info ='{0}' ><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}  {2} <span class='fc-green'> {3} </span></a>"
		}
	};

	thisApp.CurrentMode = thisApp.config.mode.idle;

	thisApp.ResetControls = function () {

		$(thisApp.config.tag.input).val('');
		$(thisApp.config.tag.textArea).val('');
		$(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

	};

	thisApp.initializeLayout = function () {

		thisApp.ResetControls();

		$(
			thisApp.config.cls.liApprove + ',' +thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
			thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
			thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.notifySuccess + ',' + thisApp.config.cls.liassetTag + ',' + thisApp.config.cls.itemRequest
		).hide();

		$(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

		thisApp.CurrentMode = thisApp.config.mode.idle;

    };
    thisApp.getParameterValueByName = function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
	thisApp.toggleDisplayModereq = function () {

		thisApp.CurrentMode = thisApp.config.mode.add;

		//$(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
		//$(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

		$(thisApp.config.cls.contentField + ',' + thisApp.config.cls.searchAsset).show();
		//$(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.liApiStatus).hide();

		$(
			thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
			thisApp.config.tag.textArea
		).removeClass(thisApp.config.cssCls.viewMode);

		//$(
		//    thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
		//).prop(thisApp.config.attr.readOnly, false);

		//$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

	};
	thisApp.toggleSearchItems = function () {

		thisApp.CurrentMode = thisApp.config.mode.search;

		$(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
		$(thisApp.config.cls.liApiStatus).show();

		$(
			thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liSave +
			thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.liSave
		).hide();

	};
	thisApp.toggleAddMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.add;

		thisApp.ResetControls();
		$(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
		$(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

		$(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.id.contentSubHeader).show();

		$(
			thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
			thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.liassetTag + ',' + thisApp.config.cls.itemRequest
		).hide();


		$(
			thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
			thisApp.config.tag.textArea
		).removeClass(thisApp.config.cssCls.viewMode);

		$(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea + ',' + thisApp.config.tag.inputTypeNumber
		).prop(thisApp.config.attr.readOnly, false);

		$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
		document.getElementById("PURCHASE_DATE").disabled = false;
		$("#btnSupplier").show();
		$("#btnBillAddress").show();


	};
	thisApp.toggleEditModeModal = function () {


		$(
			thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
			thisApp.config.tag.textArea
		).removeClass(thisApp.config.cssCls.viewMode);

		$(
			thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
		).prop(thisApp.config.attr.readOnly, false);

		$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
	};
	thisApp.toggleSearchMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.search;

		$(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
		$(thisApp.config.cls.liApiStatus).show();

		$(
			thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
			thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.liassetTag
		).hide();

	};
	thisApp.toggleSearchModalMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.search;


	};
	thisApp.displaySearchResult = function () {

		thisApp.CurrentMode = thisApp.config.mode.searchResult;

		$(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
		$(thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave).hide();

	};

	thisApp.toggleEditMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.edit;
		
		$(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
		$(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

		$(thisApp.config.id.btnBillAddress + ',' + thisApp.config.id.btnSupplier + ',' +thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField + ',' + thisApp.config.cls.itemRequest + ',' + thisApp.config.id.contentSubHeader).show();
		$(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.liassetTag).hide();

		$(
			thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
			thisApp.config.tag.textArea
		).removeClass(thisApp.config.cssCls.viewMode);

		$(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea + ',' + thisApp.config.tag.inputTypeNumber
		).prop(thisApp.config.attr.readOnly, false);

		$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);

		document.getElementById("PURCHASE_DATE").disabled = false;
		$("#btnSupplier").show();
		$("#btnBillAddress").show();
	};

	thisApp.toggleSaveMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.save;

		$(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
		$(thisApp.config.cls.liSave).hide();
		$(thisApp.config.cls.divProgress).show();
		$(thisApp.config.cls.liApiStatus).show();

	};

	thisApp.toggleReadMode = function () {
		thisApp.CurrentMode = thisApp.config.mode.read;

		$(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
		$(thisApp.config.cls.liApiStatus).show();

		$(
			thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
			thisApp.config.cls.liModify + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch + ',' +
			thisApp.config.cls.contentField
		).hide();
	};

	thisApp.toggleDisplayMode = function () {

		thisApp.CurrentMode = thisApp.config.mode.display;
		
		$(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
		$(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

		$(
			thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
			thisApp.config.cls.divClinicDoctorID + ',' + thisApp.config.cls.contentField
		).show();

		$(
			thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
			thisApp.config.cls.contentHome
		).hide();

		$(
			thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
			thisApp.config.tag.textArea
		).addClass(thisApp.config.cssCls.viewMode);

		$(
            thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea + ',' + thisApp.config.tag.inputTypeNumber
		).prop(thisApp.config.attr.readOnly, true);

		$(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);
		document.getElementById("PURCHASE_DATE").disabled = true;
		$("#btnSupplier").hide();
		$("#btnBillAddress").hide();

	};

	thisApp.showSavingStatusSuccess = function () {

		$(thisApp.config.cls.notifySuccess).show();
		$(thisApp.config.cls.divProgress).hide();

		setTimeout(function () {
			$(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
		}, 3000);


	};

	thisApp.ConvertFormToJSON = function (form) {

		var array = jQuery(form).serializeArray();
		var json = {};

		jQuery.each(array, function () {
			json[this.name] = this.value || '';
		});

		return json;

	};



	thisApp.formatString = function () {

		var stringToFormat = arguments[0];

		for (var i = 0; i <= arguments.length - 2; i++) {
			stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
		}

		return stringToFormat;
	};
	thisApp.formatJSONDateToStringOth = function () {


		var dateToFormat = arguments[0];
		var strDate;
		var strTime;

		if (dateToFormat === null) return "";
		if (dateToFormat.length < 16) return "";

		strDate = String(dateToFormat).split('-', 3);
		strTime = String(dateToFormat).split('T');

		dateToFormat = strDate[0] + '-' + strDate[1] + '-' + strDate[2].substring(0, 2);


		return dateToFormat;
	};
	thisApp.formatJSONDateToString = function () {

		var dateToFormat = arguments[0];
		var strDate;

		if (dateToFormat === null) return "";
		strDate = String(dateToFormat).split('-', 3);

		dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0];


		return dateToFormat;
	};
	thisApp.formatJSONDateTimeToString = function () {


		var dateToFormat = arguments[0];
		var strDate;
		var strTime;

		if (dateToFormat === null) return "";
		if (dateToFormat.length < 16) return "-";

		strDate = String(dateToFormat).split('-', 3);
		strTime = String(dateToFormat).split('T');

		dateToFormat = strDate[2].substring(0, 2) + '/' + strDate[1] + '/' + strDate[0] + ' ' + strTime[1].substring(0, 5);


		return dateToFormat;
	};
	thisApp.formatJSONTimeToString = function () {

		var dateToFormat = arguments[0];
		var strDate;
		var strTime;

		if (dateToFormat === null) return "-";
		if (dateToFormat.length < 16) return "-";

		strDate = String(dateToFormat).split('-', 3);
		strTime = String(dateToFormat).split('T');

		var timeToFormat = strTime[1].substring(0, 5);


		return timeToFormat;
	};
	thisApp.LoadJSON = function (jsonData) {

		$.each(jsonData, function (name, val) {
			var $el = $('#' + name), type = $el.attr('type'), isDateTime = $el.attr('data-isdatetime'), isDate = $el.attr('data-isdate');


			if (isDateTime) {
				val = thisApp.formatJSONDateTimeToString(val);
			}

			if (isDate) {
				val = thisApp.formatJSONDateToString(val);
			}

			var $el = $('#' + name), type = $el.attr('type'), isTime = $el.attr('data-istime');
			if (isTime) {
				if (val === null || val === "") {
					val = "00:00";
				} else {
					val = thisApp.formatJSONTimeToString(val);
				}

			}

			switch (type) {
				case 'checkbox':
					$el.attr('checked', 'checked');
					break;
				case 'radio':
					$el.filter('[value="' + val + '"]').attr('checked', 'checked');
					break;
				default:
					var tagNameLCase = String($el.prop('tagName')).toLowerCase();
					if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
						$el.text(val);
					} else {
						$el.val(val);
					}
			}
		});

	};

};

/*Knockout MVVM Namespace - Controls form functionality*/
var amPurchaseOrderViewModel = function (systemText) {

	var self = this;

	var ah = new appHelper(systemText);
	
	self.initialize = function () {

		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		var postData;

		$(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

		ah.toggleReadMode();

		ah.initializeLayout();
        //if (self.validatePrivileges('36') === 0) {
        //    $(ah.config.cls.liAddNew).hide();
        //}

		var postData;
		postData = { CLIENTID: staffDetails.CLIENT_CODE, STAFF_LOGON_SESSIONS: staffLogonSessions };
		$.post(self.getApi() + ah.config.api.getClientInfo, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

	};
	self.poDetails = {
		CLIENTNAME: ko.observable(),
		CLIENTADDRESS: ko.observable(),
		CLIENTCONTACT: ko.observable(),
		POPURCHASENO: ko.observable(),
		POPURCHASEDATE: ko.observable(),
		POBILLSHIPTO: ko.observable(),
		POBILLADDRESS: ko.observable(),
		POBILLCONTACT: ko.observable(),
		POVENDOR: ko.observable(),
		POVENDORADDRESS: ko.observable(),
		POVENDORCONTACT: ko.observable(),
		POTOTALAMOUNT: ko.observable(),
		POTAXABLEAMOUNT: ko.observable(),
		POAMOUNTTAXTOTAL: ko.observable(),
		
		
	};
	self.getApi = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		//alert(JSON.stringify(staffDetails));
		var api = "";
		if (staffDetails.API === null || staffDetails.API === "") {
			api = webApiURL + "api";
		} else {
			api = staffDetails.API + "api";
		}

		return api
	};
    self.reqList = ko.observableArray([{ ID_PARTS: ko.observable(), DESCRIPTION: ko.observable(), QTY: ko.observable(), QTY_PO_REQUEST: ko.observable(), PO_UNITPRICE: ko.observable(), PO_TOTALUNITPRICEITEM: ko.observable()}]);
	self.poList = ko.observableArray([]);
	self.empty = ko.observableArray([]);
	self.returnHome = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		window.location.href = staffDetails.HOME_DEFAULT;
	};
	self.createNew = function () {
		self.reqList.splice(0, 5000);

		ah.toggleAddMode();

		var jDate = new Date();
		var strDate = ("0" + jDate.getDate()).slice(-2) + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + jDate.getFullYear();
		function time_format(d) {
			hours = format_two_digits(d.getHours());
			minutes = format_two_digits(d.getMinutes());
			return hours + ":" + minutes;
		}
		function format_two_digits(n) {
			return n < 10 ? '0' + n : n;
		}
		var hourmin = time_format(jDate);

		var reqDate = strDate + ' ' + hourmin;


		$("#PURCHASE_DATE").val(reqDate);
		$("#STATUS").val("ORDER");
        $("#STATUSDESC").val("ORDER");
		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));

		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		var postData;


		//var postData;
		//postData = { CLIENTID: staffDetails.CLIENT_CODE, STAFF_LOGON_SESSIONS: staffLogonSessions };
		//$.post(self.getApi() + ah.config.api.getClientInfo, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

	};
	self.searchWONow = function () {
		var refId = $("#REF_WO").val();
		if (refId === null || refId === "0" || refId === " ") {
			alert('Unable to proceed. Please provide a valid Work Order to Proceed.')
			return;
		}
		self.readWOId(refId);
	};
	self.pageLoader = function () {
		var senderID = $(arguments[1].currentTarget).attr('id');

		var searchStr = $(ah.config.id.txtGlobalSearchOth).val().toString();

		//var pageNum = parseInt($(ah.config.id.pageNum).val());
		//var pageCount = parseInt($(ah.config.id.pageCount).val());


		//var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.stockList));
		//var lastPage = Math.ceil(sr.length / 10);

		//if (isNaN(pageCount)) {
		//	pageCount = 1;
		//}
		//if (isNaN(pageNum)) {
		//	pageNum = 0;
		//}

		switch (senderID) {

			case ah.config.tagId.nextPage: {

				if (pageCount < lastPage) {
					pageNum = pageNum + 10;
					pageCount++;
					self.searchItemsResult(searchStr, pageNum);
				}
				break;
			}
			case ah.config.tagId.priorPage: {

				if (pageNum === 0) {
				} else {
					pageNum = pageNum - 10;
					if (pageNum < 0) {
						pageNum = 0;
					}
					pageCount = pageCount - 1;
				}

				self.searchItemsResult(searchStr, pageNum);
				break;
			}

			case ah.config.tagId.txtGlobalSearchOth: {
				//pageCount = 1;
				//pageNum = 0;
				//alert('here');
				self.searchItemsResult(searchStr, 0);
				break;
			}
		}

		//$(ah.config.id.loadNumber).text(pageCount.toString());
		//$(ah.config.id.pageCount).val(pageCount.toString());
		//$(ah.config.id.pageNum).val(pageNum);

	};
	self.modifySetup = function () {

		ah.toggleEditMode();

		$("#itemsReceiveBatch *").removeAttr('disabled', 'disabled');
		$(".button-ext").each(function (i) { $(this).show() });
		//$("#itemsReceiveBatch").closest('tr').find('input').removeAttr('readonly');
	};
	self.cancelChangesModal = function () {

		if (ah.CurrentMode == ah.config.mode.add) {
			ah.initializeLayout();
		} else {
			ah.toggleDisplayMode();
			var refId = $("#AM_INSPECTION_ID").val();
			self.readSetupExistID(refId);

		}

		window.location.href = ah.config.url.modalClose;

	};
    self.printPO = function () {
        document.getElementById("printPO").classList.add('printSummary');
        var clientInformation = JSON.parse(sessionStorage.getItem(ah.config.skey.clientInfo));
        document.getElementById("CLIENT_IMAGE").src = clientInformation.CLIENT_LOGO;
		self.poDetails.CLIENTNAME(clientInformation.DESCRIPTION);
		self.poDetails.CLIENTADDRESS(clientInformation.ADDRESS + ', ' + clientInformation.CITY + ', ' + clientInformation.ZIPCODE + ', ' + clientInformation.STATE);
		self.poDetails.CLIENTCONTACT('Tel.No: ' + clientInformation.CONTACT_NO1 + ' Fax No:' + clientInformation.CONTACT_NO2);
		self.poDetails.POPURCHASENO('Purchase No. : ' + $("#REQUEST_NO").val());
		self.poDetails.POPURCHASEDATE('Purchase Date : ' + $("#PURCHASE_DATE").val());
		self.poDetails.POBILLSHIPTO('Ship To : ' + $("#BILL_SHIPTO").val());
		self.poDetails.POBILLADDRESS('Address : ' + $("#BILL_ADDRESS").val() + ', ' + $("#BILL_CITY").val() + ', Zip Code :' + $("#BILL_ZIPCODE").val() + ', ' + $("#BILL_STATE").val());
		self.poDetails.POBILLCONTACT(' Tel No: ' + $("#BILL_CONTACT_NO1").val() + ' Fax No.: ' + $("#BILL_CONTACT_NO2").val() + ' Email Address: ' + $("#BILL_EMAIL_ADDRESS").val());
		self.poDetails.POVENDORADDRESS('Address : ' + $("#SUPPLIER_ADDRESS1").val() + ', ' + $("#SUPPLIER_CITY").val() + ', Zip Code :' + $("#SUPPLIER_ZIPCODE").val() + ', ' + $("#SUPPLIER_STATE").val());
		self.poDetails.POVENDORCONTACT(' Tel No: ' + $("#SUPPLIER_CONTACT_NO1").val() + ' Fax No.: ' + $("#SUPPLIER_CONTACT_NO2").val() + ' Email Address: ' + $("#SUPPLIER_EMAIL_ADDRESS").val());
		self.poDetails.POVENDOR('Vendor Name:' + $("#SUPPLIERDESC").val());

		var poTaxable = $("#TAXABLE").val();
		if (poTaxable === null || poTaxable === "") {

			poTaxable = 'NO';
		}
		if (poTaxable === 'YES') {
			var potaxAmount = $("#TAX_AMOUNT").val();
			self.poDetails.POTOTALAMOUNT('Sub Total : ' + $("#TOTAL_AMT").text() + ' ');
			self.poDetails.POTAXABLEAMOUNT('Tax : ' + potaxAmount);
			
			var poTotalA = +parseFloat($("#TOTAL_AMT").text()).toFixed(2) + +parseFloat(potaxAmount).toFixed(2);
			$("#taxAmt").show();
			$("#totalAmt").show();
			self.poDetails.POAMOUNTTAXTOTAL('Total : ' + poTotalA.toFixed(2)+ ' ');
		} else {
			self.poDetails.POTOTALAMOUNT('Total : ' + $("#TOTAL_AMT").text() + ' ');
			$("#taxAmt").hide();
			$("#totalAmt").hide();
		}

		var jsonObj = ko.observableArray([]);

		jsonObj = ko.utils.stringifyJson(self.reqList);
		var poListsInfo = JSON.parse(jsonObj);
		
		//
		if (poListsInfo.length > 0) {
			self.poList([]);
		
			for (var o in poListsInfo) {
				var comPrice = poListsInfo[o].PO_UNITPRICE * poListsInfo[o].QTY_PO_REQUEST;
				self.poList.push({
					PODESCRIPTION: poListsInfo[o].DESCRIPTION,	
					POQTY: poListsInfo[o].QTY_PO_REQUEST,
					POUNITPRICE: poListsInfo[o].PO_UNITPRICE,
					POSUBTOTAL: comPrice.toFixed(2)


				});
			}
			
		}
		
        setTimeout(function () { window.print(); }, 1000);
	};
    self.getPurchaseReqByProd = function (reqList) {
     
		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var postData;

		var partId = JSON.parse(ko.utils.stringifyJson(reqList.ID_PARTS));
        var partIdDesc = JSON.parse(ko.utils.stringifyJson(reqList.DESCRIPTION));
        var prNo = JSON.parse(ko.utils.stringifyJson(reqList.PR_REQUEST_ID));
		var purchaseNo = $("#PURCHASE_NO").val();
		$("#ID_PARTSDESC").val(partIdDesc);
        postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), ID_PARTS: partId, REQUEST_ID: prNo , STAFF_LOGON_SESSIONS: staffLogonSessions };

            $.post(self.getApi() + ah.config.api.getprItemInfo, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
	};
	
	self.getSupplierByProd = function (reqList) {
		
		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var postData;
		
		var partId = JSON.parse(ko.utils.stringifyJson(reqList.ID_PARTS));
		var partIdDesc = JSON.parse(ko.utils.stringifyJson(reqList.DESCRIPTION));
		
		$("#ID_PARTSDESC").val(partIdDesc);
		
		postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), ID_PARTS: partId, STAFF_LOGON_SESSIONS: staffLogonSessions };
		
		$.post(self.getApi() + ah.config.api.searchSupplierProd, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
	};
	self.readWOId = function () {

		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var postData;


		var refID = $("#REF_WO").val();
		postData = { REQ_NO: refID, STAFF_LOGON_SESSIONS: staffLogonSessions };
		$.post(self.getApi() + ah.config.api.readWOrder, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

	};

	self.signOut = function () {

		window.location.href = ah.config.url.homeIndex;

	};
	self.searchApprovedPR = function () {
		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var storeId = "";

		////ah.toggleSearchMode();
		ah.toggleSearchItems();
		postData = { SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
		$.post(self.getApi() + ah.config.api.searchApprovedPR, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
		window.location.href = ah.config.url.modalAddItems;

	};
	self.saveAllChanges = function () {
        $("#SAVEACTION").val('SAVEINFO');
		$(ah.config.id.saveInfo).trigger('click');

	};
	self.calculatePrice = function (reqList,idx) {
		
		
        var comPrice = reqList.PO_UNITPRICE * reqList.QTY_PO_REQUEST;
		if (comPrice === null) {
			comPrice = 0;
		}
		document.getElementById("itemsReceiveBatch").rows[idx + 1].cells[5].innerHTML = comPrice.toFixed(2);
		
        self.reqList()[idx].PO_TOTALUNITPRICEITEM = comPrice;
		var jsonObj = ko.observableArray([]);

		jsonObj = ko.utils.stringifyJson(self.reqList);
		var poLists = JSON.parse(jsonObj);
		if (poLists.length > 0) {
			var totalAmountPrice = 0;
			for (var o in poLists) {
                var comUnitPrice = poLists[o].PO_UNITPRICE * poLists[o].QTY_PO_REQUEST
				totalAmountPrice = comUnitPrice + totalAmountPrice
            }
           
			$("#TOTAL_AMT").text(totalAmountPrice.toFixed(2));
		}
		
	};
	
	self.addItemReq = function () {

		var infoID = JSON.parse(this.getAttribute(ah.config.attr.datainfo));
		
        var existPart = self.checkExist(infoID.ID_PARTS);
        var refWO = $("#REF_WO").val();
        if (refWO !== "" && refWO !== 0 && refWO !== null) {
           
            if (infoID.REF_WO != refWO) {
                alert('Unable to Proceed. You cannot add Spare Part requested with different Work Order.');
                return;
            }
        }
		if (existPart > 0) {
			alert('Unable to Proceed. The selected item was already exist.');
			return;
		}
        $("#REF_WO").val(infoID.REF_WO);
        $("#REF_WOID").val(infoID.REF_WO);
		self.reqList.push({
			DESCRIPTION: infoID.DESCRIPTION,
			ID_PARTS: infoID.ID_PARTS,
            QTY_PO_REQUEST: infoID.PR_REQUEST_QTY,
            QTY: infoID.PR_REQUEST_QTY,
            PR_REQUEST_ID: infoID.REQUEST_ID,
            PO_UNITPRICE: 0,
            PO_TOTALUNITPRICEITEM:0
			

        });



		

	};
	self.removeItem = function (reqList) {
		
		self.reqList.remove(reqList);

	};
	self.populateClientInfo = function (jsonData) {
      
        ah.LoadJSON(jsonData.AM_CLIENT_INFO);
        //alert(jsonData.AM_CLIENT_INFO.CLIENT_LOGO);
        document.getElementById("CLIENT_IMAGE").src = jsonData.AM_CLIENT_INFO.CLIENT_LOGO;
	};

	self.checkExist = function (idPart) {

		var jsonObj = [];
		jsonObj = ko.utils.stringifyJson(self.reqList);
		var partLists = JSON.parse(jsonObj);

		var partExist = partLists.filter(function (item) { return item.ID_PARTS === idPart });

		return partExist.length;
	};
	self.cancelChanges = function () {
		var refId = $("#REQUEST_NO").val();
		if (refId === null || refId === "" || refId === "0") {
			ah.initializeLayout();
		} else {
			ah.toggleDisplayMode();

			self.readPurchaseRequestExist(refId);
		}
		$("#itemsReceiveBatch *").attr('disabled', 'disabled');

	};
	
	self.showDetails = function (isNotUpdate, saveAction) {
        ah.toggleDisplayMode();
      
		if (isNotUpdate) {
           
			var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.poItemsList));
			var poInfo = JSON.parse(sessionStorage.getItem(ah.config.skey.infoDetails));
			var gPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));
			var supplierDet = JSON.parse(sessionStorage.getItem(ah.config.skey.supplierInfo));
			poInfo.TAX_AMOUNT = poInfo.TAX_AMOUNT.toFixed(2);
			//var jsetupDetails = JSON.parse(poInfo);
			//alert(JSON.stringify(jsetupDetails));
            if (saveAction !== 'NEW') {
                ah.ResetControls();
            }
			ah.LoadJSON(poInfo);
            $("#REF_WOID").val(poInfo.REF_WO);
			ah.toggleDisplayMode();
			
			$("#REQUEST_NO").val(poInfo.PURCHASE_NO);
			
			var suppierDesc = supplierDet[0].DESCRIPTION;
			$("#SUPPLIERDESC").val(suppierDesc);
			var currStatus = "ORDER";
			//alert(JSON.stringify(poInfo));
			var currStatus = poInfo.STATUS;
			if (poInfo.PURCHASE_NO > 0) {
				$("#STATUSDESC").val(poInfo.STATUS);
				
				if (currStatus === 'APPROVED' || currStatus === 'CANCELLED' || currStatus === 'RECEIVED') {
                    if (self.validatePrivileges('9') === 0) {
                        $(ah.config.cls.liModify + ',' + ah.config.id.contentSubHeader + ',' + ah.config.id.liApprove).hide();

                    };
					

                } else if (currStatus === 'ORDER') {
                    if (self.validatePrivileges('10') === 0) {
                        $(ah.config.cls.liApprove + ',' + ah.config.id.contentSubHeader).hide();

                    } else {
                        $(ah.config.cls.liApprove + ',' + ah.config.id.contentSubHeader).show();
                    }
					

				}
				
			}
			
			if (currStatus === 'RECEIVED' || currStatus === 'PARTIAL RECEIVED') {
				
				$(ah.config.cls.liModify + ',' + ah.config.cls.liApprove).hide();

			}

			//alert(JSON.stringify(prInfo));
       
            if (saveAction !=='NEW') {          
                if (sr.length > 0) {
                    self.reqList.splice(0, 5000);
                    $("#TOTAL_AMT").text('0');
                    for (var o in sr) {

                        self.reqList.push({

                            DESCRIPTION: sr[o].DESCRIPTION,
                            ID_PARTS: sr[o].ID_PARTS,
                            QTY_PO_REQUEST: sr[o].QTY_PO_REQUEST,
                            QTY: sr[o].PR_REQUEST_QTY,
                            PR_REQUEST_ID: sr[o].PR_REQUEST_ID,
                            PO_UNITPRICE: sr[o].PO_UNITPRICE,
                            PO_TOTALUNITPRICEITEM: sr[o].PO_TOTALUNITPRICEITEM


                        });
                    }
                }
            }

            //if (self.validatePrivileges('36') === 0) {
            //    $(ah.config.cls.liAddNew).hide();
            //}
            //if (self.validatePrivileges('37') === 0) {
            //    $(ah.config.id.liModify).hide();
            //}
			// $(ah.config.id.btnRemove).hide();

		}
		
		$(".button-ext").each(function (i) { $(this).hide() });
		$("#itemsReceiveBatch *").attr('disabled', 'disabled');
		$(ah.config.cls.itemRequest).show();
		
		$("#btnSupplier *").hide();

	};
	self.searchItemsResult = function (LovCategory, pageLoad) {

		var sr = JSON.parse(sessionStorage.getItem(ah.config.skey.prApprovedList));
		if (LovCategory !== null && LovCategory !== "") {
			var arr = sr;
			var keywords = LovCategory.toUpperCase();
			var filtered = [];

			for (var i = 0; i < sr.length; i++) {
				for (var j = 0; j < keywords.length; j++) {
					if ((sr[i].DESCRIPTION.toUpperCase()).indexOf(keywords) > -1 || (sr[i].ID_PARTS.toUpperCase()).indexOf(keywords) > -1 || (sr[i].REQUEST_ID.toUpperCase()).indexOf(keywords) > -1) {
						filtered.push(sr[i]);
						break;
					}
				}
			}
			sr = filtered;
		}

		$(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).html('');

		if (sr.length > 0) {
			//$("#noresult").hide();
			//$("#pageSeparator").show();
			//$("#page1").show();
			//$("#page2").show();
			//$("#nextPage").show();
			//$("#priorPage").show();
			//var totalrecords = sr.length;
			//var pagenum = Math.ceil(totalrecords / 10);
			//if (pagenum < 2 && pagenum > 1) {
			//	pagenum = 2;
			//}
			//$(ah.config.id.totNumber).text(pagenum.toString());

			//if (pagenum <= 1) {
			//	$("#page1").hide();
			//	$("#page2").hide();
			//	$("#pageSeparator").hide();
			//	$("#nextPage").hide();
			//	$("#priorPage").hide();

			//}
			var htmlstr = '';
			var o = pageLoad;

			for (var o in sr) {
				htmlstr += '<li>';
                htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                htmlstr += ah.formatString(ah.config.html.searchExistRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].ID_PARTS, sr[o].DESCRIPTION, "REQUEST QTY:" + sr[o].PR_REQUEST_QTY);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.prReqno, sr[o].REQUEST_ID);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.prReqDate, sr[o].REQDATE);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.workorderno, sr[o].REF_WO);
                htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.workorderasset, sr[o].REF_ASSETNO);
				//if (sr[o].QTY !== null) {
				//	htmlstr += ah.formatString(ah.config.html.searchExistRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].ID_PARTS, sr[o].DESCRIPTION, " Over all Purchase Request:" + sr[o].QTY);
				//} else {
				//	htmlstr += ah.formatString(ah.config.html.searchLOVRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].ID_PARTS, sr[o].DESCRIPTION, "");
				//}
				htmlstr += '</span>';
				htmlstr += '</li>';
				//o++
				//if (totalrecords === o) {
				//	break;
				//}
			}
			// alert(JSON.stringify(sr));
			//alert(htmlstr);
			$(ah.config.id.searchResult1 + ' ' + ah.config.tag.ul).append(htmlstr);
		}
		else {
			$("#page1").hide();
			$("#page2").hide();
			$("#pageSeparator").hide();
			$("#nextPage").hide();
			$("#priorPage").hide();
			$("#noresult").show();
		}
		$(ah.config.cls.detailItem).bind('click', self.addItemReq);
		ah.toggleDisplayModereq();
		//ah.toggleAddItems();
	};
	self.searchResult = function (jsonData) {


		var sr = jsonData.AM_PURCHASEORDERLIST;
		 
		$(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).html('');

		$(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));

		if (sr.length > 0) {
			var htmlstr = '';

			for (var o in sr) {
				
				htmlstr += '<li>';
                htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].PURCHASE_NO, sr[o].PURCHASE_NO, 'Purchase Date: ' + sr[o].PURCHASE_DATE, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total Items: ' + sr[o].TOTAL_NOITEMS + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Amount:&nbsp;&nbsp;' + parseFloat(sr[o].TOTAL_AMOUNT.toFixed(2)) + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Work Order No.:&nbsp;&nbsp;' + sr[o].REF_WO + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Status:&nbsp;&nbsp;' + sr[o].STATUS);
				
				htmlstr += '</li>';

			}

			$(ah.config.id.searchResultList + ' ' + ah.config.tag.ul).append(htmlstr);
		}

		$(ah.config.cls.detailItem).bind('click', self.readPurchaseRequest);
        ah.displaySearchResult();
        $(ah.config.cls.liApprove).hide();

	};
	self.searchResultSupplierProd = function (jsonData) {
		var sr = jsonData.AM_PARTSUPPLIERDETAILS;
		
		$(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');
		if (sr.length > 0) {
			var htmlstr = "";
			for (var o in sr) {

				if (sr[o].DESCRIPTION.length > 50)
					sr[o].DESCRIPTION = sr[o].DESCRIPTION.substring(0, 49) + "...";

				htmlstr += '<li>';
				htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
				htmlstr += ah.formatString(ah.config.html.searchExistRowHeaderTemplate, JSON.stringify(sr[o]), sr[o].DESCRIPTION, '          &nbsp;&nbsp;&nbsp;&nbsp;Preference: ' + sr[o].PREFERENCE,'');



				htmlstr += '</span>';
				htmlstr += '</li>';

			}

		
			$(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).append(htmlstr);
		}
		$("#lookUpTitle").text("Recommended Suppliers and Preference for "  + $("#ID_PARTSDESC").val());
		window.location.href = window.location.href + ah.config.url.modalLookUp;

	};
    self.validatePrivileges = function (privilegeId) {
        var groupPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));

         groupPrivileges = groupPrivileges.filter(function (item) { return item.PRIVILEGES_ID === privilegeId });
        return groupPrivileges.length;
    };
	self.searchResultPRProd = function (jsonData) {
		var sr = jsonData.AM_PURCHASEREQUESTFORPOLIST;
		
		$(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');
		if (sr.length > 0) {
			var htmlstr = "";
			for (var o in sr) {
				htmlstr += '<li>';
				htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
                htmlstr += ah.formatString(ah.config.html.searchExistRowHeaderTemplate, JSON.stringify(sr[o]), 'Purchase Req: ' + sr[o].REQUEST_ID, '          &nbsp;&nbsp;&nbsp;&nbsp;Req. Date: ' + sr[o].REQ_DATE, '&nbsp;&nbsp;&nbsp;&nbsp; QTY: ' + sr[o].PR_REQUEST_QTY);
				htmlstr += '</span>';
				htmlstr += '</li>';
			}


			$(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).append(htmlstr);
		}
		$("#lookUpTitle").text("Approved Purchase Request for " + $("#ID_PARTSDESC").val());
		window.location.href = window.location.href + ah.config.url.modalLookUp;

	};
	self.readPurchaseRequestExist = function (refId) {


		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var postData;

		ah.toggleReadMode();
		
		postData = { PURCHASE_NO: refId, STAFF_LOGON_SESSIONS: staffLogonSessions };
		$.post(self.getApi() + ah.config.api.readPurchaseReq, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

	};
	self.readPurchaseRequest = function () {

		var infoID = this.getAttribute(ah.config.attr.datainfoID).toString();
		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var postData;

		ah.toggleReadMode();
	
		postData = { PURCHASE_NO: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
		$.post(self.getApi() + ah.config.api.readPurchaseReq, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

	};
	self.readAssetID = function () {

		var infoID = $("#REF_ASSETNO").val();

		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var postData;



		postData = { ASSET_NO: infoID, STAFF_LOGON_SESSIONS: staffLogonSessions };
		$.post(self.getApi() + ah.config.api.readAssetInfo, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

	};
	self.readSetupExistID = function (refId) {


		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var postData;

		ah.toggleReadMode();

		postData = { AM_INSPECTION_ID: refId, STAFF_LOGON_SESSIONS: staffLogonSessions };
		$.post(self.getApi() + ah.config.api.readSetup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

	};
	self.statusApprove = function () {
		var jsonObj = [];
		jsonObj = ko.utils.stringifyJson(self.reqList);
		var receviceLists = JSON.parse(jsonObj);
		if (receviceLists.length <= 0) {
			alert('Unable to proceed. No item to approve.')
			return;
		}
		var lThanzero = receviceLists.filter(function (item) { return item.QTY.toString() === "-0" || item.QTY <= -1 });

		if (lThanzero.length > 0) {

			alert('Unable to proceed. Item name: ' + lThanzero[0].DESCRIPTION + ', have negative request qty.')
			return;
		}

		self.approvePurchaseOrder();
	};
	self.statusCancel = function () {
        self.updateStatus("CANCELLED")
	};
	self.searchNow = function () {

		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		var postData;
		self.reqList.splice(0, 5000);
		// self.clearTableData();
		ah.toggleSearchMode();

		postData = { SEARCH: $(ah.config.id.txtGlobalSearch).val().toString(), RETACTION: "Y", STAFF_LOGON_SESSIONS: staffLogonSessions };
		$.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
	};
	self.infoLovLoader = function () {
		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var senderID = $(arguments[1].currentTarget).attr('id');
		
		$(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');
		$("#lookUploadNumber").text("1");
		$("#lookUppageCount").val("1");
		$("#lookUppageNum").val(0);
		
		switch (senderID) {			
			case ah.config.tagId.btnBillAddress: {
				$("#lookUpTitle").text("Bill Address List");
				$(ah.config.id.lovAction).val("BILLADDRESS");
				//self.displayLookupManufacturer();

				postData = { LOV: "'AIMS_COSTCENTER'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
				$.post(self.getApi() + ah.config.api.searchBillAddress, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
				window.location.href = ah.config.url.modalLookUpSupBill;
				break;
			}
			case ah.config.tagId.btnSupplier: {
			
				$("#lookUpTitle").text("Supplier List");
				$(ah.config.id.lovAction).val("SUPPLIER");

				postData = { LOV: "'AIMS_COSTCENTER'", SEARCH: "", STAFF_LOGON_SESSIONS: staffLogonSessions };
				$.post(self.getApi() + ah.config.api.searchSupplier, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);				
				window.location.href = ah.config.url.modalLookUpSupBill;
		
			}
		}



	};
	self.lookUppageLoader = function () {


		var senderID = $(arguments[1].currentTarget).attr('id');

		var searchStr = $(ah.config.id.lookUptxtGlobalSearchOth).val().toString();

		var pageNum = parseInt($(ah.config.id.lookUppageNum).val());
		var pageCount = parseInt($(ah.config.id.lookUppageCount).val());

		if ($(ah.config.id.lovAction).val() === "SUPPLIER") {
			sr = JSON.parse(sessionStorage.getItem(ah.config.skey.supplierListLov));
		
		} else if ($(ah.config.id.lovAction).val() === "BILLADDRESS") {
			sr = JSON.parse(sessionStorage.getItem(ah.config.skey.billAddress));
		}
	
		var lastPage = Math.ceil(sr.length / 15);

		if (isNaN(pageCount)) {
			pageCount = 1;
		}
		if (isNaN(pageNum)) {
			pageNum = 0;
		}

		switch (senderID) {

			case ah.config.tagId.lookUpnextPage: {

				if (pageCount < lastPage) {
					pageNum = pageNum + 15;
					pageCount++;

					self.searchItemsLookUpResult(searchStr, pageNum);
				}
				break;
			}
			case ah.config.tagId.lookUppriorPage: {

				if (pageNum === 0) {
				} else {
					pageNum = pageNum - 15;
					if (pageNum < 0) {
						pageNum = 0;
					}
					pageCount = pageCount - 1;
				}

				self.searchItemsLookUpResult(searchStr, pageNum);
				break;
			}

			case ah.config.tagId.lookUptxtGlobalSearchOth: {
				pageCount = 1;
				pageNum = 0;

				self.searchItemsLookUpResult(searchStr, 0);
				break;
			}
		}

		$("#lookUploadNumber").text(pageCount.toString());
		$("#lookUppageCount").val(pageCount.toString());
		$("#lookUppageNum").val(pageNum);

	};
	self.searchItemsLookUpResult = function (LovCategory, pageLoad) {
		var sr = "";

		if ($(ah.config.id.lovAction).val() === "SUPPLIER") {
			sr = JSON.parse(sessionStorage.getItem(ah.config.skey.supplierListLov));
		
		} else if ($(ah.config.id.lovAction).val() === "BILLADDRESS") {
			sr = JSON.parse(sessionStorage.getItem(ah.config.skey.billAddress));
		}
		//alert(JSON.stringify(sr));

		if (LovCategory !== null && LovCategory !== "") {
			var arr = sr;

			var keywords = LovCategory.toUpperCase();
			var filtered = [];

			for (var i = 0; i < sr.length; i++) {
				for (var j = 0; j < keywords.length; j++) {
					if (sr[i].DESCRIPTION === null) sr[i].DESCRIPTION = "";
					if ((sr[i].DESCRIPTION.toUpperCase()).indexOf(keywords) > -1) {
						filtered.push(sr[i]);
						break;
					}
				}
			}
			sr = filtered;
		}

		$(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).html('');

		if (sr.length > 0) {
			$("#noresult").hide();
			$("#lookUppageSeparator").show();
			$("#lookUppage1").show();
			$("#lookUppage2").show();
			$("#lookUpnextPage").show();
			$("#lookUppriorPage").show();
			var totalrecords = sr.length;
			var pagenum = Math.ceil(totalrecords / 15);

			if (pagenum < 2 && pagenum > 1) {

				pagenum = 2;
			}

			$("#lookUptotNumber").text(pagenum.toString());


			if (pagenum <= 1) {
				$("#lookUppage1").hide();
				$("#lookUppage2").hide();
				$("#lookUppageSeparator").hide();
				$("#lookUpnextPage").hide();
				$("#lookUppriorPage").hide();

			}
			var htmlstr = '';
			var o = pageLoad;

			for (var i = 0; i < 15; i++) {
				htmlstr += '<li>';
				htmlstr += '<span class="' + ah.config.cssCls.searchRowDivider + ' ' + ah.config.cssCls.alignLeft + '">';
				htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplateLOV, JSON.stringify(sr[o]), sr[o].DESCRIPTION,'','');

				htmlstr += '</span>';
				htmlstr += '</li>';
				o++
				if (totalrecords === o) {
					break;
				}
			}
			// alert(JSON.stringify(sr));
			//alert(htmlstr);
			$(ah.config.id.searchResultLookUp + ' ' + ah.config.tag.ul).append(htmlstr);
		}
		else {
			$("#lookUppage1").hide();
			$("#lookUppage2").hide();
			$("#lookUppageSeparator").hide();
			$("#lookUpnextPage").hide();
			$("#lookUppriorPage").hide();
			$("#lookUpnoresult").show();
		}
		$(ah.config.cls.lookupDetailItem).bind('click', self.assignLookup);
		//ah.toggleDisplayModereq();
		//ah.toggleAddItems();
	};

	self.assignLookup = function () {
		
		var lookUpInfo = JSON.parse(this.getAttribute(ah.config.attr.datalookUp));
		var lovData = $(ah.config.id.lovAction).val();
		
		switch (lovData) {

			
			case "SUPPLIER": {

				
				$("#SUPPLIERDESC").val(lookUpInfo.DESCRIPTION);
				$("#SUPPLIER_ID").val(lookUpInfo.SUPPLIER_ID);
				$("#SUPPLIER_ADDRESS1").val(lookUpInfo.ADDRESS1);
				$("#SUPPLIER_CITY").val(lookUpInfo.CITY);
				$("#SUPPLIER_ZIP").val(lookUpInfo.ZIP);
				$("#SUPPIER_STATE").val(lookUpInfo.STATE);
				$("#SUPPLIER_CONTACT_NO1").val(lookUpInfo.CONTACT_NO1);
				$("#SUPPLIER_CONTACT_NO2").val(lookUpInfo.CONTACT_NO2);
				$("#SUPPLIER_EMAIL_ADDRESS").val(lookUpInfo.EMAIL_ADDRESS);
				break;
			}
			case "BILLADDRESS": {
				$("#BILL_SHIPTO").val(lookUpInfo.SHIP_TO);
				$("#PO_BILLCODE_ADDRESS").val(lookUpInfo.PO_BILLCODE);
				//$("#BILLADDRESSLOV").val(lookUpInfo.DESCRIPTION);
				$("#BILL_ADDRESS").val(lookUpInfo.BILL_ADDRESS);
				$("#BILL_CITY").val(lookUpInfo.BILL_CITY);
				$("#BILL_ZIPCODE").val(lookUpInfo.BILL_ZIPCODE);
				$("#BILL_STATE").val(lookUpInfo.BILL_STATE);
				$("#BILL_CONTACT_NO1").val(lookUpInfo.BILL_CONTACT_NO1);
				$("#BILL_CONTACT_NO2").val(lookUpInfo.BILL_CONTACT_NO2);
				$("#BILL_EMAIL_ADDRESS").val(lookUpInfo.BILL_EMAIL_ADDRESS);
				
				
				break;
			}




		}
	};
	self.clearTableData = function () {
		var new_tbody = document.createElement('tbody');
		populate_with_new_rows(new_tbody);
		old_tbody.parentNode.replaceChild(new_tbody, old_tbody)
	};
	self.updateStatus = function (StatusAction) {
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);

        //$("#STATUSID").val(StatusAction);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var poDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
        
        var postData;

        $(ah.config.cls.liModify + ',' + ah.config.id.contentSubHeader).hide();

        var jDate = new Date();
        var statusDate = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
        function time_format(d) {
            hours = format_two_digits(d.getHours());
            minutes = format_two_digits(d.getMinutes());
            return hours + ":" + minutes;
        }
        function format_two_digits(n) {
            return n < 10 ? '0' + n : n;
        }
        var hourmin = time_format(jDate);

        statusDate = statusDate + ' ' + hourmin;
     
        poDetails.STATUS = StatusAction;
        if (StatusAction == "APPROVED") {
           
            poDetails.APPROVED_BY = staffLogonSessions.STAFF_ID;
            poDetails.APPROVED_DATE = statusDate;
        } else if (StatusAction == "CANCELLED") {
            poDetails.CANCELLED_BY = staffLogonSessions.STAFF_ID;
            poDetails.CANCELLED_DATE = statusDate;
        }

        //alert(JSON.stringify(poDetails));
        postData = { AM_PURCHASEORDER: poDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
        //alert(JSON.stringify(postData));
        ah.toggleSaveMode();
        $.post(self.getApi() + ah.config.api.updateStatusPO, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

	};
    self.approvePurchaseOrder = function () {
        self.updateStatus("APPROVED")
  
	};
    self.saveInfo = function () {
        var saveAction = $("#SAVEACTION").val();
        if (saveAction != 'SAVEINFO') {
            return;
        }
        if (window.event.keyCode == 13) return;
		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var itemDetails = ah.ConvertFormToJSON($(ah.config.id.frmInfo)[0]);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var postData;



		var jsonObj = ko.observableArray([]);
		
		jsonObj = ko.utils.stringifyJson(self.reqList);

		
		var poLists = JSON.parse(jsonObj);

		if (poLists.length <= 0) {
			alert('Unable to proceed. No item to be order. Please click on Add item button and select parts you wish to purchase.')
			return;
		}

		if (poLists.length > 0) {
			var totalAmountPrice = 0;
			for (var o in poLists) {
				if (poLists[o].QTY_PO_REQUEST < 1) {
					alert("Please make sure that the " + poLists[o].DESCRIPTION + " purchase qty value is greater than 0.");
					return;
				}
				var comUnitPrice = poLists[o].PO_UNITPRICE * poLists[o].QTY_PO_REQUEST
				totalAmountPrice = comUnitPrice + totalAmountPrice
			}
			$("#TOTAL_AMT").text(totalAmountPrice.toFixed(2));
		}

		if (itemDetails.PURCHASE_NO === null || itemDetails.PURCHASE_NO === "") {
			itemDetails.PURCHASE_NO = 0;
		}

		if (itemDetails.PURCHASEID === null || itemDetails.PURCHASEID === "") {
			itemDetails.PURCHASEID = 0;
		}

		if (itemDetails.HANDLING_FEE === null || itemDetails.HANDLING_FEE === "") {
			itemDetails.HANDLING_FEE = 0;
		}
		if (itemDetails.EST_FREIGHT === null || itemDetails.EST_FREIGHT === "") {
			itemDetails.EST_FREIGHT = 0;
		}
		if (itemDetails.FREIGHT === null || itemDetails.FREIGHT === "") {
			itemDetails.FREIGHT = 0;
		}
		if (itemDetails.TAX_AMOUNT === null || itemDetails.TAX_AMOUNT === "") {
			itemDetails.TAX_AMOUNT = 0;
		}
		if (itemDetails.FREIGHT_POLINE === null || itemDetails.FREIGHT_POLINE === "") {
			itemDetails.FREIGHT_POLINE = 0;
		}
		if (itemDetails.FREIGHT_SEPARATE_INV === null || itemDetails.FREIGHT_SEPARATE_INV === "") {
			itemDetails.FREIGHT_SEPARATE_INV = 0;
		}
		if (itemDetails.INSURANCE_VALUE === null || itemDetails.INSURANCE_VALUE === "") {
			itemDetails.INSURANCE_VALUE = 0;
		}
		var reqDate = $("#PURCHASE_DATE").val();

		var strTime = '';
		var apptDate = reqDate;
		reqDate = apptDate.substring(6, 10) + '-' + apptDate.substring(3, 5) + '-' + apptDate.substring(0, 2);

		strTime = String(apptDate).split(' ');
		strTime = strTime[1].substring(0, 5);
		reqDate = reqDate + ' ' + strTime;
		
		

		var jDate = new Date();
		var strDateStart = jDate.getFullYear() + '/' + ("0" + (jDate.getMonth() + 1)).slice(-2) + '/' + ("0" + jDate.getDate()).slice(-2);
		function time_format(d) {
			hours = format_two_digits(d.getHours());
			minutes = format_two_digits(d.getMinutes());
			return hours + ":" + minutes;
		}
		function format_two_digits(n) {
			return n < 10 ? '0' + n : n;
		}
		var hourmin = time_format(jDate);

		strDateStart = strDateStart + ' ' + hourmin;
		itemDetails.PURCHASE_DATE = strDateStart;
		
		itemDetails.TOTAL_NOITEMS = poLists.length;
		var totalAmount = $("#TOTAL_AMT").text();
        
		itemDetails.TOTAL_AMOUNT = totalAmount;
		if (itemDetails.PURCHASE_NO === null || itemDetails.PURCHASE_NO === "") {
			itemDetails.PURCHASE_NO = "0";
        }
        postData = { AM_PURCHASEORDER_ITEMS: poLists, AM_PURCHASEORDER: itemDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
   
        if (ah.CurrentMode == ah.config.mode.add) {
            itemDetails.CREATED_BY = staffLogonSessions.STAFF_ID;
            itemDetails.CREATED_DATE = strDateStart;
            $.post(self.getApi() + ah.config.api.createOrder, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        } else {
            $.post(self.getApi() + ah.config.api.updatePO, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
            
        }
        ah.toggleSaveMode();
	};
	self.readPurchaseRequestRefId = function (refId) {


		var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
		var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
		var postData;

		ah.toggleReadMode();

		postData = { REQUEST_ID: refId, STAFF_LOGON_SESSIONS: staffLogonSessions };
		$.post(self.getApi() + ah.config.api.readPurchaseReq, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

	};
	self.webApiCallbackStatus = function (jsonData) {


		// alert(JSON.stringify(jsonData));
		if (jsonData.ERROR) {
			
				alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
			

			//window.location.href = ah.config.url.homeIndex;
			//alert(jsonData.ERROR.ErrorText);
		}
		else if (jsonData.AM_PURCHASE_BILLADDRESS || jsonData.AM_SUPPLIERLIST || jsonData.AM_PURCHASEREQUESTFORPOLIST || jsonData.AM_PARTSUPPLIERDETAILS || jsonData.AM_PURCHASEORDERITEMS || jsonData.AM_PURCHASEORDERLIST || jsonData.AM_CLIENT_INFO || jsonData.AM_SUPPLIER || jsonData.AM_ASSET_DETAILS || jsonData.AM_WORK_ORDERS || jsonData.AM_PURCHASEREQUEST_APPROVEDLIST || jsonData.AM_PURCHASEORDER || jsonData.STAFF || jsonData.SUCCESS) {
			//alert(ah.CurrentMode);
			if (ah.CurrentMode == ah.config.mode.save) {
				$(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
				ah.showSavingStatusSuccess();
                $("#SAVEACTION").val('N/A');
				sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_PURCHASEORDER));
               
				if (jsonData.SUCCESS) {
					
					self.showDetails(false,"MODIFY");
				}
				else {
					
					self.showDetails(true,"NEW");

				}

			}
			else if (ah.CurrentMode == ah.config.mode.deleted) {

				$(ah.config.id.successMessage).html(systemText.deletedSuccessMessage);
				ah.showSavingStatusSuccess();
				ah.toggleDisplayMode();
			}
			else if (ah.CurrentMode == ah.config.mode.add || ah.CurrentMode == ah.config.mode.edit) {
				if (jsonData.AM_CLIENT_INFO) {
					self.populateClientInfo(jsonData);
				} else if (jsonData.AM_PARTSUPPLIERDETAILS) {
					self.searchResultSupplierProd(jsonData);

				} else if (jsonData.AM_PURCHASEREQUESTFORPOLIST) {
					self.searchResultPRProd(jsonData);
				} else if (jsonData.AM_SUPPLIERLIST) {
					sessionStorage.setItem(ah.config.skey.supplierListLov, JSON.stringify(jsonData.AM_SUPPLIERLIST));
					$("#lookUploadNumber").text("1");
					$("#lookUppageCount").val("1");
					$("#lookUppageNum").val(0);
					self.searchItemsLookUpResult("", 0);
				}
				else if (jsonData.AM_PURCHASE_BILLADDRESS) {
					sessionStorage.setItem(ah.config.skey.billAddress, JSON.stringify(jsonData.AM_PURCHASE_BILLADDRESS));
					$("#lookUploadNumber").text("1");
					$("#lookUppageCount").val("1");
					$("#lookUppageNum").val(0);
					self.searchItemsLookUpResult("", 0);
				}
				
			}
			else if (ah.CurrentMode == ah.config.mode.read) {
				// alert(JSON.stringify(jsonData));
				sessionStorage.setItem(ah.config.skey.poItemsList, JSON.stringify(jsonData.AM_PURCHASEORDERITEMS));
				sessionStorage.setItem(ah.config.skey.infoDetails, JSON.stringify(jsonData.AM_PURCHASEORDER));
				sessionStorage.setItem(ah.config.skey.supplierInfo, JSON.stringify(jsonData.AM_SUPPLIER));
				self.showDetails(true);
			}
			else if (ah.CurrentMode == ah.config.mode.search) {
				if (jsonData.AM_PURCHASEREQUEST_APPROVEDLIST) {
					sessionStorage.setItem(ah.config.skey.prApprovedList, JSON.stringify(jsonData.AM_PURCHASEREQUEST_APPROVEDLIST));
					$(ah.config.id.loadNumber).text("1");
					$(ah.config.id.pageCount).val("1");
					$(ah.config.id.pageNum).val(0);
					self.searchItemsResult("", 0);
					$(ah.config.cls.liApiStatus).hide();
					$(ah.config.cls.liSave).show();
				} else {
					self.searchResult(jsonData);
				}


			}
			else if (ah.CurrentMode == ah.config.mode.idle) {
				//var suppier = jsonData.AM_SUPPLIER;
				sessionStorage.setItem(ah.config.skey.clientInfo, JSON.stringify(jsonData.AM_CLIENT_INFO));
				
                var reqNO = ah.getParameterValueByName(ah.config.fld.poNo);
                if (reqNO !== null && reqNO !== "") {
                    self.readPurchaseRequestExist(reqNO);
                } 
			}

		}
		else {

			alert(systemText.errorTwo);
			//window.location.href = ah.config.url.homeIndex;
		}

	};

	self.initialize();
};