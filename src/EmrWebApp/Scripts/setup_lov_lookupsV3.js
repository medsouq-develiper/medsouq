﻿var appHelper = function (systemText) {

    var thisApp = this;

    thisApp.config = {
        cls: {
            contentField: '.field-content-detail',
            contentSearch: '.search-content-detail',
            contentHome: '.home-content-detail',
            liModify: '.liModify',
            liSave: '.liSaveAll',
            liAddNew: '.liAddNew',
            divProgress: '.divProgressScreen',
            liApiStatus: '.liApiStatus',
            notifySuccess: '.notify-success',
            toolBoxRight: '.toolbox-right',
            divLovLookupID: '.divLovLookupID',
            statusText: '.status-text',
            lovlookupDetailItem: '.lovlookupDetailItem'
        },
        tag: {
            input: 'input',
            textArea: 'textarea',
            select: 'select',
            inputTypeText: 'input[type=text]',
            inputTypeEmail: 'input[type=email]',
            inputTypeNumber: 'input[type=number]',
            ul: 'ul'
        },
        mode: {
            idle: 0,
            add: 1,
            search: 2,
            searchResult: 3,
            save: 4,
            read: 5,
            display: 6,
            edit: 7,
            searchCategory: 8
        },
        id: {
            spanUser: '#spanUser',
            contentHeader: '#contentHeader',
            contentSubHeader: '#contentSubHeader',
            saveLovLookup: '#saveLovLookup',
            searchResultList: '#searchResultList',
            searchResultCategoryList1: '#searchResultCategoryList1',
            searchResultCategoryList2: '#searchResultCategoryList2',
            searchResultList1: '#searchResultList1',
            searchResultList2: '#searchResultList2',
            searchResultList3: '#searchResultList3',
            searchResultCategoryList: '#searchResultCategoryList',
            
            searchResultcontainer: 'searchResultcontainer',
           
            searchResultSummary: '#searchResultSummary',
            txtGlobalSearch: '#txtGlobalSearch',
            frmLovLookup: '#frmLovLookup',
            lovlookupID: '#LOV_LOOKUP_ID',
            lovlookupCategory: '#CATEGORY',
            lovCategoryDescription: '#CATEGORY_DESCRIPTION',
            successMessage: '#successMessage',
            woPriority: '#WO_PRIORITY'
        },
        cssCls: {
            viewMode: 'view-mode'
        },
        attr: {
            readOnly: 'readonly',
            disabled: 'disabled',
            selectedIndex: 'selectedIndex',
            datalovlookupCategoryDescription: 'data-lovlookupCategoryDescription',
            dataLovLookupID: 'data-lovlookupID'
        },
        skey: {
            staff: 'STAFF',
            webApiUrl: 'webApiURL',
            staffLogonSessions: 'STAFF_LOGON_SESSIONS',
            lovlookupDetails: 'LOV_LOOKUPS',
            searchResult: 'searchResult',
            groupPrivileges: 'GROUP_PRIVILEGES'

        },
        url: {
            homeIndex: '/Home/Index',
            aimsHomepage: '/Menuapps/aimsIndex'
        },
        api: {
            readLovLookup: '/Setup/ReadLovLookup',
            searchAll: '/Search/SearchLovLookups',
            createLovLookup: '/Setup/CreateLovLookup',
            updateLovLookup: '/Setup/UpdateLovLookup',
            getListLOV: '/Search/SearchListOfValue'
        },
        html: {
            searchRowHeaderTemplate: "<a href='#' class='fs-medium-normal lovlookupDetailItem' data-lovlookupID='{0}' data-lovlookupCategoryDescription='{1}'><i class='fa fa-arrow-circle-o-right'></i>&nbsp;&nbsp;{1}{2}{3}</a>",
            searchRowTemplate: '<span class="search-row-label fc-light-grey-2">{0}: <span class="search-row-data">{1}</span></span>'
        }
    };

    thisApp.CurrentMode = thisApp.config.mode.idle;

    thisApp.ResetControls = function () {

        $(thisApp.config.tag.input).val('');
        $(thisApp.config.tag.textArea).val('');
        $(thisApp.config.tag.select).prop(thisApp.config.attr.selectedIndex, 0);

    };

    thisApp.initializeLayout = function () {

        thisApp.ResetControls();

        $(thisApp.config.cls.liAddNew + ',' +
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.divProgress + ',' +
            thisApp.config.cls.contentField + ',' + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.liApiStatus + ',' +
            thisApp.config.cls.divLovLookupID + ',' + thisApp.config.cls.notifySuccess
        ).hide();

        $(thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentHome).show();

        thisApp.CurrentMode = thisApp.config.mode.idle;

    };

    thisApp.toggleAddMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.add;

        thisApp.ResetControls();
        $(thisApp.config.id.contentHeader).html(systemText.addModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.addModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();

        $(
            thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.divLovLookupID
        ).hide();


        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode); 

        $(
            thisApp.config.tag.inputTypeNumber + ',' + thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
        document.getElementById("LOV_LOOKUP_ID").disabled = false;
    };

    thisApp.toggleSearchMode = function (searchAction) {

        if (searchAction == 'CATEGORY') {
            thisApp.CurrentMode = thisApp.config.mode.searchCategory;

        } else {
            thisApp.CurrentMode = thisApp.config.mode.search;
        }
        
        
        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify + ',' +
            + thisApp.config.cls.contentSearch + ',' + thisApp.config.cls.contentField
        ).hide();
        $(thisApp.config.cls.statusText).text(systemText.searchModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();


    };

    thisApp.displaySearchResult = function (searchaction) {

        thisApp.CurrentMode = thisApp.config.mode.searchResult;
        if (searchaction == 'CATEGORY') {
            $(thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();

        } else {
            $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' + thisApp.config.cls.contentSearch).show();
        }
        
        $(thisApp.config.cls.liApiStatus).hide();

    };

    thisApp.toggleEditMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.edit;

        $(thisApp.config.id.contentHeader).html(systemText.updateModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.updateModeSubHeader);

        $(thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentField).show();
        $(thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liModify).hide();

        $(
            thisApp.config.tag.inputTypeNumber + ',' +  thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
            thisApp.config.tag.textArea
        ).removeClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeNumber + ',' +  thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, false);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, false);
       
        document.getElementById("LOV_LOOKUP_ID").disabled = true;
    };

    thisApp.toggleSaveMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.save;

        $(thisApp.config.cls.statusText).text(systemText.saveModeStatusText);
        $(thisApp.config.cls.liSave).hide();
        $(thisApp.config.cls.divProgress).show();
        $(thisApp.config.cls.liApiStatus).show();

    };

    thisApp.toggleReadMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.read;

        $(thisApp.config.cls.statusText).text(systemText.readModeStatusText);
        $(thisApp.config.cls.liApiStatus).show();

        $(
            thisApp.config.cls.contentHome + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.liSave + ',' +
            thisApp.config.cls.liModify + ','  + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentField
        ).hide();
    };

    thisApp.toggleDisplayMode = function () {

        thisApp.CurrentMode = thisApp.config.mode.display;

        $(thisApp.config.id.contentHeader).html(systemText.displayModeHeader);
        $(thisApp.config.id.contentSubHeader).html(systemText.displayModeSubHeader);

        $(
            thisApp.config.cls.liModify + ',' + thisApp.config.cls.liAddNew + ',' + thisApp.config.cls.toolBoxRight + ',' +
            thisApp.config.cls.divLovLookupID + ',' + thisApp.config.cls.contentField
        ).show();

        $(
            thisApp.config.cls.liApiStatus + ',' + thisApp.config.cls.liSave + ',' + thisApp.config.cls.contentSearch + ',' +
            thisApp.config.cls.contentHome
        ).hide();

        $(
            thisApp.config.tag.inputTypeNumber + ',' +  thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.select + ',' +
           thisApp.config.tag.textArea
        ).addClass(thisApp.config.cssCls.viewMode);

        $(
            thisApp.config.tag.inputTypeNumber + ',' +  thisApp.config.tag.inputTypeText + ',' + thisApp.config.tag.inputTypeEmail + ',' + thisApp.config.tag.textArea
        ).prop(thisApp.config.attr.disabled, true);

        $(thisApp.config.tag.select).prop(thisApp.config.attr.disabled, true);
        document.getElementById("LOV_LOOKUP_ID").disabled = true;
    };

    thisApp.showSavingStatusSuccess = function () {

        $(thisApp.config.cls.notifySuccess).show();
        $(thisApp.config.cls.divProgress).hide();

        setTimeout(function () {
            $(thisApp.config.cls.notifySuccess).fadeOut(1000, function () { });
        }, 3000);


    };

    thisApp.ConvertFormToJSON = function (form) {
        $(form).find(":disabled").removeAttr("disabled");
        var array = jQuery(form).serializeArray();
        var json = {};

        jQuery.each(array, function () {
            json[this.name] = this.value || '';
        });

        return json;

    };



    thisApp.formatString = function () {

        var stringToFormat = arguments[0];

        for (var i = 0; i <= arguments.length - 2 ; i++) {
            stringToFormat = stringToFormat.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i + 1]);
        }

        return stringToFormat;
    };

    thisApp.LoadJSON = function (jsonData) {

        $.each(jsonData, function (name, val) {
            var $el = $('#' + name),
                type = $el.attr('type');

            switch (type) {
                case 'checkbox':
                    $el.attr('checked', 'checked');
                    break;
                case 'radio':
                    $el.filter('[value="' + val + '"]').attr('checked', 'checked');
                    break;
                default:
                    var tagNameLCase = String($el.prop('tagName')).toLowerCase();
                    if (tagNameLCase == 'span' || tagNameLCase == 'p' || tagNameLCase == 'div') {
                        $el.text(val);
                    } else {
                        $el.val(val);
                    }
            }
        });

    };

};

/*Knockout MVVM Namespace - Controls form functionality*/
var lovlookupsViewModel = function (systemText) {

    var self = this;
    self.enable = false;
    self.category = ko.observable("");
    var ah = new appHelper(systemText);
    var srLovList = ko.observableArray([]);
    self.initialize = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));

        var postData;

        $(ah.config.id.spanUser).text(staffDetails.FIRST_NAME.toLowerCase() + ' ' + staffDetails.LAST_NAME.toLowerCase());

        postData = { LOV: "'AIMS_PRIORITY'", STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.getListLOV, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        ah.initializeLayout();

    };

    self.returnHome = function () {
		var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
		window.location.href = staffDetails.HOME_DEFAULT;
    };

    self.createNewLovLookup = function () {
        
        
        var lovlookupCagetory = $(ah.config.id.lovlookupCategory).val().toString();
        ah.toggleAddMode();
        document.getElementById("CATEGORY").value = lovlookupCagetory;
        $("#CATEGORY").attr("disabled", "disabled");

    };

    self.modifyLovLookupDetails = function () {

        ah.toggleEditMode();
        $("#CATEGORY").attr("disabled", "disabled");
    };

    self.signOut = function () {

        window.location.href = ah.config.url.homeIndex;

    };

    self.saveAllChanges = function () {

        $(ah.config.id.saveLovLookup).trigger('click');

    };

    self.cancelChanges = function () {

        if (ah.CurrentMode == ah.config.mode.add) {
            ah.initializeLayout();
        } else {
            ah.toggleDisplayMode();
            self.showLovLookupDetails();
        }

    };

    self.showLovLookupDetails = function (isNotUpdate) {

        if (isNotUpdate) {

            var lovlookupDetails = sessionStorage.getItem(ah.config.skey.lovlookupDetails);

            var jLovLookup = JSON.parse(lovlookupDetails);

            ah.ResetControls();
            ah.LoadJSON(jLovLookup);
            if (jLovLookup.CATEGORY == "AIMS_DTYPE") {
                $("#DTYPESELECT").show();
            } else {
                $("#DTYPESELECT").hide();
            }


            if (jLovLookup.CATEGORY == "AIMS_PRIORITY") {
                $("#WODURATION").show();
            } else {
                $("#WODURATION").hide();
            }

            if (jLovLookup.CATEGORY == "AIMS_PROBLEMTYPE") {
                $("#WOPRIORITY").show();
                $("#WO_PRIORITY").val(jLovLookup.WO_DURATION);
            } else {
                $("#WOPRIORITY").hide();
            }

            
        }
       
        ah.toggleDisplayMode();

    };

    self.searchResult = function (jsonData,searchaction) {

        sessionStorage.setItem(ah.config.skey.searchResult, JSON.stringify(jsonData.LOV_LOOKUPS));
        var sr = jsonData.LOV_LOOKUPS;
        
        
        $(ah.config.id.searchResultCategoryList1 + ' ' + ah.config.tag.ul).html('');
        $(ah.config.id.searchResultCategoryList2 + ' ' + ah.config.tag.ul).html('');

        $(ah.config.id.searchResultList1 + ' ' + ah.config.tag.ul).html('');
        $(ah.config.id.searchResultList2 + ' ' + ah.config.tag.ul).html('');
        $(ah.config.id.searchResultList3 + ' ' + ah.config.tag.ul).html('');
        
        var categoryDescription = $(ah.config.id.lovCategoryDescription).val().toString();
        
        if (sr.length > 0) {
            var htmlstr = '';

            var htmlStr1 = '';
            var htmlStr2 = '';
            var htmlStr3 = '';

            var totalrecords = sr.length;
            var itemCount = totalrecords
            var flag = 0;
            var o = 1;

            if (searchaction == 'CATEGORY') {
                if (itemCount % 2 !== 0) {
                    itemCount++;
                }
                var subtot = itemCount / 2;


                for (o in sr) {

                    if (sr[o]) {
                        htmlstr += '<li>';
                        htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].LOV_LOOKUP_ID, sr[o].DESCRIPTION, ' - ', sr[o].LOV_LOOKUP_ID);
                        htmlstr += '</li>';
                    }

                    if (o >= subtot - 1 && flag == 0) {
                        htmlStr1 += htmlstr;
                        htmlstr = '';
                        flag = 1;
                    } else if (o >= sr.length - 1 && flag == 1) {
                        htmlStr2 += htmlstr;
                        htmlstr = '';
                        flag = 2
                    }
                }

                $(ah.config.id.searchResultCategoryList1 + ' ' + ah.config.tag.ul).append(htmlStr1);
                $(ah.config.id.searchResultCategoryList2 + ' ' + ah.config.tag.ul).append(htmlStr2);

                $(ah.config.id.txtGlobalSearch).val('');

                $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length));
                $(ah.config.cls.lovlookupDetailItem).bind('click', self.SearchLovLookupByCategoryID);
            } else {

                if (itemCount % 3 !== 0) {
                    itemCount++;
                }
                var subtot = itemCount / 3;
                flag = 0;
                for (o in sr) {
					if (sr[o].STATUS === 'Y') {
						sr[o].STATUS = 'Active';
					} else {
						sr[o].STATUS = 'In Active';
					}
                    if (sr[o]) {
                        htmlstr += '<li>';
                        htmlstr += ah.formatString(ah.config.html.searchRowHeaderTemplate, sr[o].LOV_LOOKUP_ID, sr[o].DESCRIPTION, ' - ', sr[o].LOV_LOOKUP_ID);
						htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.descriptionothname, sr[o].DESCRIPTION_OTH);
						htmlstr += ah.formatString(ah.config.html.searchRowTemplate, systemText.searchModeLabels.statusname, sr[o].STATUS);
                        htmlstr += '</li>';
                    }

                    if (o >= subtot - 1 && flag == 0) {
                        htmlStr1 += htmlstr;
                        htmlstr = '';
                        flag = 1;
                    } else if (o >= (subtot * 2) - 1 && flag == 1) {
                        htmlStr2 += htmlstr;
                        htmlstr = '';
                        flag = 2;
                    } else if (o >= sr.length - 1 && flag == 2) {
                        htmlStr3 += htmlstr;
                        htmlstr = '';
                        flag = 3
                    }
                }

                $(ah.config.id.searchResultList1 + ' ' + ah.config.tag.ul).append(htmlStr1);
                $(ah.config.id.searchResultList2 + ' ' + ah.config.tag.ul).append(htmlStr2);
                $(ah.config.id.searchResultList3 + ' ' + ah.config.tag.ul).append(htmlStr3);

                $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length) + ' - ' + self.category());

                $(ah.config.cls.lovlookupDetailItem).bind('click', self.readLovLookupByLovLookupID);

            }

        }
        else {
            $(ah.config.id.searchResultSummary).text(ah.formatString(systemText.searchModeHeader.toString(), $(ah.config.id.txtGlobalSearch).val().toString(), sr.length) + ' - ' + self.category());
        }
        
        ah.displaySearchResult(searchaction);

    };

    self.SearchLovLookupByCategoryID = function () {

        var lovlookupCagetoryID = this.getAttribute(ah.config.attr.dataLovLookupID).toString();
        var lovCategoryDescription = this.getAttribute(ah.config.attr.datalovlookupCategoryDescription).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleSearchMode('ITEM');

        var searchQuery = $(ah.config.id.txtGlobalSearch).val().toString();

        $(ah.config.id.lovlookupCategory).val(lovlookupCagetoryID);
        $(ah.config.id.lovCategoryDescription).val(lovCategoryDescription);
        self.category(lovCategoryDescription);
        postData = { SEARCH: searchQuery, CATEGORY: lovlookupCagetoryID, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi()+ ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);


    };
    self.getApi = function () {
        var staffDetails = JSON.parse(sessionStorage.getItem(ah.config.skey.staff));
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        //alert(JSON.stringify(staffDetails));
        var api = "";
        if (staffDetails.API === null || staffDetails.API === "") {
            api = webApiURL + "api";
        } else {
            api = staffDetails.API + "api";
        }

        return api
    };
    self.readLovLookupByLovLookupID = function () {

        var lovlookupID = this.getAttribute(ah.config.attr.dataLovLookupID).toString();
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        ah.toggleReadMode();
        var lovlookupCagetory = $(ah.config.id.lovlookupCategory).val().toString();
        postData = { LOV_LOOKUP_ID: lovlookupID, CATEGORY: lovlookupCagetory, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.readLovLookup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    
    self.searchCagetoryList = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        $(ah.config.id.txtGlobalSearch).val('');
        var searchQuery = '';
        
        $(ah.config.id.lovlookupCategory).val('');
       
        ah.toggleSearchMode('CATEGORY');
        searchQuery2 = 'LOV';
    

        postData = { SEARCH: searchQuery, CATEGORY: searchQuery2, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };

    self.searchNow = function () {

        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;

        
        var searchQuery = $(ah.config.id.txtGlobalSearch).val().toString();
        searchQuery2 = $(ah.config.id.lovlookupCategory).val().toString()
 
        if (searchQuery2.length == 0) {
            ah.toggleSearchMode('CATEGORY');
            searchQuery2 = 'LOV';
        } else {
            ah.toggleSearchMode('ITEM');
        }
      

        postData = { SEARCH: searchQuery, CATEGORY: searchQuery2, STAFF_LOGON_SESSIONS: staffLogonSessions };
        $.post(self.getApi() + ah.config.api.searchAll, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);

    };
    self.validatePrivileges = function (privilegeId) {
        var groupPrivileges = JSON.parse(sessionStorage.getItem(ah.config.skey.groupPrivileges));

        var groupPrivileges = groupPrivileges.filter(function (item) { return item.PRIVILEGES_ID === privilegeId });
        return groupPrivileges.length;
    };
    self.saveLovLookupDetails = function () {
        document.getElementById("LOV_LOOKUP_ID").disabled = false;
        var webApiURL = sessionStorage.getItem(ah.config.skey.webApiUrl);
        var lovlookupDetails = ah.ConvertFormToJSON($(ah.config.id.frmLovLookup)[0]);
        var staffLogonSessions = JSON.parse(sessionStorage.getItem(ah.config.skey.staffLogonSessions));
        var postData;


        if (lovlookupDetails.CATEGORY == "AIMS_PROBLEMTYPE") {
            var wopriority = $("#WO_PRIORITY").val();
            lovlookupDetails.WO_DURATION = wopriority;
        }


        postData = { LOV_LOOKUPS: lovlookupDetails, STAFF_LOGON_SESSIONS: staffLogonSessions };
        if (ah.CurrentMode == ah.config.mode.add) {
            ah.toggleSaveMode();
            
            $.post(self.getApi() + ah.config.api.createLovLookup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }
        else {
            ah.toggleSaveMode();
           
            $.post(self.getApi() + ah.config.api.updateLovLookup, postData).done(self.webApiCallbackStatus).fail(self.webApiCallbackStatus);
        }

    };

    self.webApiCallbackStatus = function (jsonData) {
       // alert(JSON.stringify(jsonData));
        if (jsonData.ERROR) {

            alert(ah.formatString(systemText.errorOne, jsonData.ERROR.ErrorSource + ' - ' + jsonData.ERROR.ErrorText));
            window.location.href = ah.config.url.homeIndex;

        }
        else if (jsonData.LOV_LOOKUP_ID || jsonData.CATEGORY || jsonData.LOV_LOOKUPS || jsonData.SUCCESS || jsonData.LOV_LOOKUPS) {

            if (ah.CurrentMode == ah.config.mode.save) {

                sessionStorage.setItem(ah.config.skey.lovlookupDetails, JSON.stringify(jsonData.LOV_LOOKUPS));
                $(ah.config.id.successMessage).html(systemText.saveSuccessMessage);
                ah.showSavingStatusSuccess();
                
                if (jsonData.SUCCESS) {
                    
                    self.showLovLookupDetails(false);
                }
                else {
                    self.showLovLookupDetails(true);
                }
            }
            else if (ah.CurrentMode == ah.config.mode.read) {
                sessionStorage.setItem(ah.config.skey.lovlookupDetails, JSON.stringify(jsonData.LOV_LOOKUPS));
                self.showLovLookupDetails(true);
            }
            else if (ah.CurrentMode == ah.config.mode.search) {

                self.searchResult(jsonData,'ITEM');
            }
            else if (ah.CurrentMode == ah.config.mode.searchCategory) {

                self.searchResult(jsonData, 'CATEGORY');
            }
            else if (ah.CurrentMode == ah.config.mode.idle) {
                srLovList = jsonData.LOV_LOOKUPS;
                $.each(srLovList, function (item) {

                    $(ah.config.id.woPriority)
                        .append($("<option></option>")
                            .attr("value", srLovList[item].LOV_LOOKUP_ID)
                            .text(srLovList[item].DESCRIPTION));
                });

            }

        }
        else {

            alert(systemText.errorTwo);
            window.location.href = ah.config.url.homeIndex;
        }

    };

    self.initialize();
};