//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "12.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class services {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal services() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.services", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 2014 Technologies Incorporated. All Rights Reserved..
        /// </summary>
        internal static string footer_main_p_text {
            get {
                return ResourceManager.GetString("footer_main_p_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Other Description:.
        /// </summary>
        internal static string frmService_label_DESCRIPTION_OTH_text {
            get {
                return ResourceManager.GetString("frmService_label_DESCRIPTION_OTH_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Short Description:.
        /// </summary>
        internal static string frmService_label_DESCRIPTION_SHORT_text {
            get {
                return ResourceManager.GetString("frmService_label_DESCRIPTION_SHORT_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Description:.
        /// </summary>
        internal static string frmService_label_DESCRIPTION_text {
            get {
                return ResourceManager.GetString("frmService_label_DESCRIPTION_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hello, Welcome to Security and Settings.
        /// </summary>
        internal static string homeMessage_h1_text {
            get {
                return ResourceManager.GetString("homeMessage_h1_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create new Service.
        /// </summary>
        internal static string homeMessage_h1_ul_li_1_text {
            get {
                return ResourceManager.GetString("homeMessage_h1_ul_li_1_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Modify existing Service informaion.
        /// </summary>
        internal static string homeMessage_h1__ul_li_2_text {
            get {
                return ResourceManager.GetString("homeMessage_h1__ul_li_2_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hospital Information System.
        /// </summary>
        internal static string nav_site_map_ul_li_1_text {
            get {
                return ResourceManager.GetString("nav_site_map_ul_li_1_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Security and Settings / Services.
        /// </summary>
        internal static string nav_site_map_ul_li_2_text {
            get {
                return ResourceManager.GetString("nav_site_map_ul_li_2_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sign Out.
        /// </summary>
        internal static string nav_user_info_ul_li_signOut_text {
            get {
                return ResourceManager.GetString("nav_user_info_ul_li_signOut_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please wait while saving all changes ....
        /// </summary>
        internal static string readModeStatusText {
            get {
                return ResourceManager.GetString("readModeStatusText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Service search results for &apos;PED&apos;. 6 Items found..
        /// </summary>
        internal static string search_content_detail_searchResultSummary_text {
            get {
                return ResourceManager.GetString("search_content_detail_searchResultSummary_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to /Content/site.css.
        /// </summary>
        internal static string service_cssMainUrl {
            get {
                return ResourceManager.GetString("service_cssMainUrl", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Security and Settings - Hospital Information System.
        /// </summary>
        internal static string service_title {
            get {
                return ResourceManager.GetString("service_title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New Service.
        /// </summary>
        internal static string textInfo_addModeHeader_text {
            get {
                return ResourceManager.GetString("textInfo_addModeHeader_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please provide all required information before &lt;i class=&quot; fa fa-floppy-o&quot;&gt;&lt;/i&gt; saving. Click the &lt;i class=&quot; fa fa-undo&quot;&gt;&lt;/i&gt; cancel button to abort..
        /// </summary>
        internal static string textInfo_addModeSubHeader_text {
            get {
                return ResourceManager.GetString("textInfo_addModeSubHeader_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Service Information.
        /// </summary>
        internal static string textInfo_displayModeHeader_text {
            get {
                return ResourceManager.GetString("textInfo_displayModeHeader_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click the &lt;i class=&quot; fa fa-edit&quot;&gt;&lt;/i&gt;edit button to modify location details.
        /// </summary>
        internal static string textInfo_displayModeSubHeader_text {
            get {
                return ResourceManager.GetString("textInfo_displayModeSubHeader_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There seems to ba a problem in the system. We will now redirect you to the logon page to avoid further issues.\n\r&quot; {0} &quot;\n\rSorry for the inconvenience..
        /// </summary>
        internal static string textInfo_errorOne_text {
            get {
                return ResourceManager.GetString("textInfo_errorOne_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Server not available, please try again later.
        /// </summary>
        internal static string textInfo_errorTwo_text {
            get {
                return ResourceManager.GetString("textInfo_errorTwo_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please wait while getting clinicsession information . . ..
        /// </summary>
        internal static string textInfo_readModeStatusText_text {
            get {
                return ResourceManager.GetString("textInfo_readModeStatusText_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please wait while saving . . ..
        /// </summary>
        internal static string textInfo_saveModeStatusText_text {
            get {
                return ResourceManager.GetString("textInfo_saveModeStatusText_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Record Successfully Saved!.
        /// </summary>
        internal static string textInfo_saveSuccessMessage_text {
            get {
                return ResourceManager.GetString("textInfo_saveSuccessMessage_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Services search results for &apos;{0}&apos;. {1} Items found..
        /// </summary>
        internal static string textInfo_searchModeHeader_text {
            get {
                return ResourceManager.GetString("textInfo_searchModeHeader_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Description.
        /// </summary>
        internal static string textInfo_searchModeLabels_descriptionname_text {
            get {
                return ResourceManager.GetString("textInfo_searchModeLabels_descriptionname_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Other Description.
        /// </summary>
        internal static string textInfo_searchModeLabels_descriptionothname_text {
            get {
                return ResourceManager.GetString("textInfo_searchModeLabels_descriptionothname_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Short Description.
        /// </summary>
        internal static string textInfo_searchModeLabels_descriptionshortname_text {
            get {
                return ResourceManager.GetString("textInfo_searchModeLabels_descriptionshortname_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please wait while searching . . ..
        /// </summary>
        internal static string textInfo_searchModeStatusText_text {
            get {
                return ResourceManager.GetString("textInfo_searchModeStatusText_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update Service Information.
        /// </summary>
        internal static string textInfo_updateModeHeader_text {
            get {
                return ResourceManager.GetString("textInfo_updateModeHeader_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please provide all required information before &lt;i class=&quot; fa fa-floppy-o&quot;&gt;&lt;/i&gt; saving. Click the &lt;i class=&quot; fa fa-undo&quot;&gt;&lt;/i&gt; cancel button to abort..
        /// </summary>
        internal static string textInfo_updateModeSubHeader_text {
            get {
                return ResourceManager.GetString("textInfo_updateModeSubHeader_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New Entry.
        /// </summary>
        internal static string toolbox_left_ul_li_linkBtnAddNew_text {
            get {
                return ResourceManager.GetString("toolbox_left_ul_li_linkBtnAddNew_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Modify.
        /// </summary>
        internal static string toolbox_left_ul_li_linkBtnModify_text {
            get {
                return ResourceManager.GetString("toolbox_left_ul_li_linkBtnModify_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save.
        /// </summary>
        internal static string toolbox_left_ul_li_linkBtnSaveAll_text {
            get {
                return ResourceManager.GetString("toolbox_left_ul_li_linkBtnSaveAll_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        internal static string toolbox_left_ul_li_linkBtnSaveCancel_text {
            get {
                return ResourceManager.GetString("toolbox_left_ul_li_linkBtnSaveCancel_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please wait while saving all changes ....
        /// </summary>
        internal static string toolbox_left_ul_li_status_text_text {
            get {
                return ResourceManager.GetString("toolbox_left_ul_li_status_text_text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search Anything ....
        /// </summary>
        internal static string toolbox_rightul_li_txtGlobalSearch_text {
            get {
                return ResourceManager.GetString("toolbox_rightul_li_txtGlobalSearch_text", resourceCulture);
            }
        }
    }
}
