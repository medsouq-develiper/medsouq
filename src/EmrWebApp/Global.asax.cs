﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Globalization;
using System.Threading;

namespace EmrWebApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        internal protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            if (sessionManager.locale == null)
            {
                sessionManager.locale = "en";
            }

            Thread.CurrentThread.CurrentUICulture = (new CultureInfo(sessionManager.locale));
        }
    }

    public static class sessionManager
    {
        private const string LANGUAGE = "language";

        public static string locale
        {
            get
            {
                //var LANGUAGE_EN = "EN";
                if (HttpContext.Current.Session != null)
                {
                    return (string)HttpContext.Current.Session[LANGUAGE];
                }
                else
                {
                    return "";
                }
            }
            set
            {
                HttpContext.Current.Session[LANGUAGE] = value;
            }
        }
    }

}
