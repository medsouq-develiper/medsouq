﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmrSys
{
    public class Converter
    {
        public static string IntToBase32(int numConvert){
            string ret = "";
            char[] charArray = { 'G', 'R', '4', '1', 'A', 'B', '7', 'C', '3', '9', 'N', '8', 'Q', 'I', 'D', '0', 'T', 'L', 'S', 'E', 'P', '6', 'V', 'M', 'F', 'U', '5', 'H', '2', 'J', 'K', 'O' };
            
            string numBinary = System.Convert.ToString(numConvert, 2);
            string[] fiveBitBinaryArray = binaryToFiveBitArray(numBinary);
            foreach (string str in fiveBitBinaryArray)
            {
                ret += charArray[(int)System.Convert.ToInt32(str, 2)]; 
            }
            return ret;
        }
        private static string[] binaryToFiveBitArray(string numBinary){
            int binaryLength = numBinary.Length;
            int arrayLength = (int)Math.Ceiling(System.Convert.ToDouble(binaryLength/5));
            string[] ret = new string[arrayLength];

            for (int i = 0; i < arrayLength; i++) {
                int startSub = binaryLength - ((i+1)*5);
                if (startSub>=0){
                    ret[i] = numBinary.Substring(startSub, 5).PadLeft(5, char.Parse("0"));
                }
                else{
                    ret[i] = numBinary.Substring(0, 5+startSub).PadLeft(5, char.Parse("0"));
                }
            }
            return ret;
        }
    }
}
